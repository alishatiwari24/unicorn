const chai = require('chai');
const sinon = require('sinon');
const Game = require('../helpers/gamefunction');
const whowapi = require('../api/whowapi');
const _ = require('lodash');
const testData = require('./test-data');

const expect = chai.expect;
class TotalClass {
  getTotal(...args) {
    if (args == null || args.length == 0) {
      return 0;
    } else if (args.length == 1) {
      return args[0];
    }
    let total = 0;
    args.forEach((arg) => {
      total = this.calculateSum(total, arg);
    });
    return total;
  }

  calculateSum(total, value) {
    console.log('calling');
    return total + value;
  }
}

describe('TotalTest', () => {
  const total = new TotalClass();

  beforeEach(() => {
    sinon.stub(total, 'calculateSum').callsFake((arg1, arg2) => arg1 + arg2);
    sinon.stub(whowapi, 'get').callsFake(token => new Promise((resolve, reject) => {
      if (token === testData.USER_TOKEN) {
        resolve(testData.WHOW_VALID_TOKEN_RESPONSE);
      }
      resolve(testData.WHOW_INVALID_TOKEN_RESPONSE);
    }));
    sinon.stub(Game, 'getGame').callsFake((gameId) => {
      if (gameId === testData.VALID_GAME_ID) {
        return JSON.parse(testData.GAME_DATA);
      }
      return undefined;
    });
  });

  afterEach(() => {
    total.calculateSum.restore();
    whowapi.get.restore();
    Game.getGame.restore();
  });

  it('getTotal() should return 0 when no arguments are passed to it', () => {
    expect(total.getTotal()).to.equal(0);
  });
});
