const chai = require('chai');
const sinon = require('sinon');
const dragonPhoenixData = require('./test-data');
const dragonPhoenix = require('../../../util/games/dragon-phoenix');
const gameSession = require('../../../util/gamesession');
const expect = chai.expect;
const nj = require('numjs');

/**
* Dragon & Phoenix game's test cases are written here
*/
describe('Dragon & Phoenix Tests', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
  beforeEach(() => {
    /**
    * stub to mock getting user session
    */
    sinon.stub(gameSession, 'getUserSession').callsFake((token, gameId) => {
      console.log(token, 'tokenn getUserSession');
      if (gameId === dragonPhoenixData.VALID_GAME_ID && token === dragonPhoenixData.VALID_USER_TOKEN) {
        return Promise.resolve(dragonPhoenixData.VALID_USER_SESSION);
      }
      return Promise.reject();
    });

    /**
    * stub to mock updating user session, and call whow
    */
    sinon.stub(dragonPhoenix, 'updateSession').callsFake((session, gameId) => Promise.resolve());
  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    gameSession.getUserSession.restore();
    dragonPhoenix.updateSession.restore();
  });


  /**
  * select reel configuration success tests
  */
  it('select reel configuration from session - success test case', (done) => {
    const gameData = JSON.parse(JSON.stringify(dragonPhoenixData.VALID_GAME));
    const userSession = JSON.parse(JSON.stringify(dragonPhoenixData.VALID_USER_SESSION));
    dragonPhoenix.selectReelConfig(userSession, gameData);
    for (let i = 1; i <= gameData.game.default.gameConfig.specialFeature.reelConfig.NumberOfReels.length; i += 1) {
      expect(gameData.game.default.gameConfig.specialFeature.reelConfig).to.have.a.property(`Reel${i}`);
    }
    done();
  });

  /**
  * generate view zone test cases
  */
  it('generate view zone test case', (done) => {
    const gameData = JSON.parse(JSON.stringify(dragonPhoenixData.VALID_GAME));
    const noofReels = 5;
    const columnSize = 3;
    const specialSymbols = [];
    const heldSymbols = { symbolList: [], positions: {} };
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const isFeatureTriggered = dragonPhoenix.generateViewZone(
      [1, 2, 3, 4, 5], noofReels, columnSize, [], gameData.game,
      arrays, specialSymbols, heldSymbols, 0, 2
    );
    expect(isFeatureTriggered).to.be.eql(false);
    done();
  });

  it('generate view zone test case when respin is triggered', (done) => {
    const gameData = JSON.parse(JSON.stringify(dragonPhoenixData.VALID_GAME));
    const noofReels = 5;
    const columnSize = 3;
    const specialSymbols = [];
    const heldSymbols = { symbolList: [], positions: {} };
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const isFeatureTriggered = dragonPhoenix.generateViewZone(
      [1, 2, 2, 4, 5], noofReels, columnSize, [], gameData.game,
      arrays, specialSymbols, heldSymbols, 0, 2
    );
    expect(isFeatureTriggered).to.be.eql(true);
    done();
  });

  it('generate view zone test case when current respin is 1', (done) => {
    const gameData = JSON.parse(JSON.stringify(dragonPhoenixData.VALID_GAME));
    const noofReels = 5;
    const columnSize = 3;
    const specialSymbols = [];
    const heldSymbols = { symbolList: [], positions: {} };
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const isFeatureTriggered = dragonPhoenix.generateViewZone(
      [1, 2, 2, 4, 5], noofReels, columnSize, [], gameData.game,
      arrays, specialSymbols, heldSymbols, 1, 2
    );
    expect(isFeatureTriggered).to.be.eql(true);
    for (let i = 0; i < columnSize; i += 1) {
      expect(arrays.get(i, 2)).to.be.eql(gameData.game.default.gameConfig.specialFeature.triggeringSymbolId);
      expect(arrays.get(i, 2)).to.be.eql(gameData.game.default.gameConfig.specialFeature.triggeringSymbolId);
      expect(arrays.get(i, 2)).to.be.eql(gameData.game.default.gameConfig.specialFeature.triggeringSymbolId);
    }
    expect(heldSymbols.symbolList).to.have.property('length').to.be.eql(3);
    done();
  });

  it('generate view zone test case when current respin is 2', (done) => {
    const gameData = JSON.parse(JSON.stringify(dragonPhoenixData.VALID_GAME));
    const noofReels = 5;
    const columnSize = 3;
    const specialSymbols = [];
    const heldSymbols = { symbolList: [], positions: {} };
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const isFeatureTriggered = dragonPhoenix.generateViewZone(
      [1, 2, 2, 4, 5], noofReels, columnSize, [], gameData.game,
      arrays, specialSymbols, heldSymbols, 2, 2
    );
    expect(isFeatureTriggered).to.be.eql(false);
    for (let i = 0; i < columnSize; i += 1) {
      expect(arrays.get(i, 2)).to.be.eql(gameData.game.default.gameConfig.specialFeature.triggeringSymbolId);
      expect(arrays.get(i, 2)).to.be.eql(gameData.game.default.gameConfig.specialFeature.triggeringSymbolId);
      expect(arrays.get(i, 2)).to.be.eql(gameData.game.default.gameConfig.specialFeature.triggeringSymbolId);
    }
    expect(heldSymbols.symbolList).to.have.property('length').to.be.eql(3);
    done();
  });

  it('generate (view zone - response) test case', (done) => {
    const gameData = JSON.parse(JSON.stringify(dragonPhoenixData.VALID_GAME));
    const noofReels = 5;
    const columnSize = 3;
    const specialSymbols = [];
    const heldSymbols = { symbolList: [], positions: {} };
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const isFeatureTriggered = dragonPhoenix.generateViewZone(
      [1, 2, 2, 4, 5], noofReels, columnSize, [], gameData.game,
      arrays, specialSymbols, heldSymbols, 2, 2
    );
    const viewZone = dragonPhoenix.generateViewZoneResponse(noofReels, columnSize, arrays, gameData);
    expect(isFeatureTriggered).to.be.eql(false);
    for (let i = 0; i < columnSize; i += 1) {
      expect(arrays.get(i, 2)).to.be.eql(gameData.game.default.gameConfig.specialFeature.triggeringSymbolId);
      expect(arrays.get(i, 2)).to.be.eql(gameData.game.default.gameConfig.specialFeature.triggeringSymbolId);
      expect(arrays.get(i, 2)).to.be.eql(gameData.game.default.gameConfig.specialFeature.triggeringSymbolId);
    }
    for (let i = 1; i <= noofReels; i += 1) {
      expect(viewZone).to.have.a.property(`Reel${i}`);
      expect(viewZone[`Reel${i}`]).to.have.a.property('length').to.be.eql(columnSize);
      viewZone[`Reel${i}`].forEach((reelArray) => {
        expect(reelArray).to.have.a.property('symbolId');
        expect(reelArray).to.have.a.property('symbolType');
        expect(reelArray).to.have.a.property('symbolName');
      });
    }
    expect(heldSymbols.symbolList).to.have.property('length').to.be.eql(3);
    done();
  });

});
