const chai = require('chai');
const sinon = require('sinon');
const qinDynastyData = require('./test-data');
const qinDynasty = require('../../../util/games/qin-dynasty');
const nj = require('numjs');
const gameSession = require('../../../util/gamesession');
const rng = require('../../../util/rng');
const expect = chai.expect;

/**
* Qin Dynasty game's test cases are written here
* Qin Dynasty tests include following tests
* - get user session
*/
describe('Qin Dynasty Tests', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
  beforeEach(() => {
    /**
    * stub to mock getting user session
    */
    sinon.stub(gameSession, 'getUserSession').callsFake((token, gameId) => {
      if (gameId === qinDynastyData.VALID_GAME_ID && token === qinDynastyData.VALID_USER_TOKEN) {
        return Promise.resolve(qinDynastyData.VALID_USER_SESSION);
      }
      return Promise.reject();
    });
  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    gameSession.getUserSession.restore();
  });

  /**
  * Getting current reel set during the spin (main game)
  */
  it('Getting current reel set during any spin (main game), for valid session data', (done) => {
    const currentReelSet = qinDynasty.getCurrentReelSet(qinDynastyData.VALID_USER_SESSION, 5);
    expect(currentReelSet).to.be.eql(4);
    done();
  });

  /**
  * Getting current reel set during the spin
  */
  it('Getting current reel set during any spin (free game), for valid session data', (done) => {
    const currentReelSet = qinDynasty.getCurrentReelSet(qinDynastyData.VALID_USER_SESSION_FREE_GAME, 5);
    expect(currentReelSet).to.be.eql(1);
    done();
  });

  /**
  * Getting current reel set during the spin
  */
  it('Getting current reel set during any spin (free game), for current reel set not defined', (done) => {
    const session = JSON.parse(JSON.stringify(qinDynastyData.VALID_USER_SESSION_FREE_GAME));
    session.eventData.freeSpinData.currentReelSet = 5;
    const currentReelSet = qinDynasty.getCurrentReelSet(session, 5);
    expect(currentReelSet).to.be.eql(5);
    done();
  });

  /**
  * updating currentReelSet, for the next spin, (main game), success test case
  */
  it('Updating currentReelSet, for the next spin, (main game), success test case', (done) => {
    const session = JSON.parse(JSON.stringify(qinDynastyData.VALID_USER_SESSION));
    qinDynasty.updateReelSetForNextSpin(session, 5);
    expect(session.eventData.currentReelSet).to.be.eql(3);
    done();
  });

  /**
  * updating currentReelSet, for the next spin, (free game), success test case
  */
  it('Updating currentReelSet, for the next spin, (free game), success test case', (done) => {
    const session = JSON.parse(JSON.stringify(qinDynastyData.VALID_USER_SESSION_FREE_GAME));
    qinDynasty.updateReelSetForNextSpin(session, 5);
    expect(session.eventData.freeSpinData.currentReelSet).to.be.eql(5);
    done();
  });

  /**
  * updating currentReelSet, for the next spin, (free game), success test case - 2
  */
  it('Updating currentReelSet, for the next spin, (free game), success test case - 2 for valid dataset - 2', (done) => {
    const session = JSON.parse(JSON.stringify(qinDynastyData.VALID_USER_SESSION_FREE_GAME));
    session.eventData.freeSpinData.currentReelSet = 3;
    qinDynasty.updateReelSetForNextSpin(session, 5);
    expect(session.eventData.freeSpinData.currentReelSet).to.be.eql(2);
    done();
  });

  /**
  * adding roaming reel to reel sets
  */
  it('Adding roaming reels, to specified position, in the given reel set', (done) => {
    const game = JSON.parse(JSON.stringify(qinDynastyData.VALID_GAME_DATA));
    qinDynasty.addRoamingReel(game, qinDynastyData.VALID_USER_SESSION_FREE_GAME.eventData, 3);
    expect(game.default.gameConfig.freeSpin.reelConfig.Reel3).to.have.a.property('NumberOfRows').to.be.eql(15);
    expect(game.default.gameConfig.freeSpin.reelConfig.Reel3).to.have.a.property('SymbolDistribution').to.be.a('array');
    expect(game.default.gameConfig.freeSpin.reelConfig.Reel3.SymbolDistribution.length).to.be.eql(game.default.gameConfig.freeSpin.reelConfig.Reel3.NumberOfRows);
    done();
  });

  /**
  * wild pay compute test cases
  */
  it('Wild pay compute function success test case', (done) => {
    const game = JSON.parse(JSON.stringify(qinDynastyData.VALID_GAME_DATA));
    const session = JSON.parse(JSON.stringify(qinDynastyData.VALID_USER_SESSION_FREE_GAME));
    qinDynasty.addRoamingReel(game, session.eventData, 1);
    const arrays = nj.arange(5 * 3).reshape(3, 5);
    const results = [];
    const randomNumber = [0, 5, 6, 8, 13];
    qinDynasty.generateViewZone(randomNumber, 5, 3, [], [], game, arrays, session);
    qinDynasty.wildPayCompute(arrays, 3, game.default.gameConfig.payLines.normalizedPayArray, game, results);
    expect(results).to.be.a('array');
    expect(results.length).to.be.eql(5);
    done();
  });

  /**
  * normal pay compute test cases
  */
  it('Normal pay compute function success test case', (done) => {
    const game = JSON.parse(JSON.stringify(qinDynastyData.VALID_GAME_DATA));
    const session = JSON.parse(JSON.stringify(qinDynastyData.VALID_USER_SESSION_FREE_GAME));
    qinDynasty.addRoamingReel(game, session.eventData, 4);
    const arrays = nj.arange(5 * 3).reshape(3, 5);
    const results = [];
    const randomNumber = [0, 5, 6, 8, 13];
    qinDynasty.generateViewZone(randomNumber, 5, 3, [], [], game, arrays, session);
    qinDynasty.computeWins(arrays, 3, game.default.gameConfig.payLines.normalizedPayArray, game, results);
    expect(results).to.be.a('array');
    expect(results.length).to.be.eql(6);
    done();
  });

  /**
  * update session data
  */
  it('Update user session data success test case', (done) => {
    const session = JSON.parse(JSON.stringify(qinDynastyData.VALID_USER_SESSION));
    const sessionRequest = qinDynasty.updateSessionData(session, qinDynastyData.VALID_SPIN_RESULT);
    expect(sessionRequest).to.have.a.property('_type').to.be.eql('sessionRequest');
    expect(sessionRequest).to.have.a.property('status').to.be.eql('ready');
    expect(sessionRequest).to.have.a.property('spinIniTimestamp');
    expect(sessionRequest).to.have.a.property('spinCompletedTimestamp');
    done();
  });
});
