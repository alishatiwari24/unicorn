const chai = require('chai');
const sinon = require('sinon');
const gatsbyData = require('./test-data');
const gatsby = require('../../../util/games/gatsby');
const gameSession = require('../../../util/gamesession');
const expect = chai.expect;
const nj = require('numjs');

/**
* Gatsby game's test cases are written here
* Gatsby tests include following tests
* - get user session
*/
describe('The Great Gatsby Tests', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
  beforeEach(() => {
    /**
    * stub to mock getting user session
    */
    sinon.stub(gameSession, 'getUserSession').callsFake((token, gameId) => {
      if (gameId === gatsbyData.VALID_GAME_ID && token === gatsbyData.VALID_USER_TOKEN) {
        return Promise.resolve(gatsbyData.VALID_USER_SESSION);
      }
      return Promise.reject();
    });
  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    gameSession.getUserSession.restore();
  });

  /**
  * Get reel config to be used for current spin
  */
  it('Get reel config to be used for current spin', (done) => {
    const gameData = JSON.parse(JSON.stringify(gatsbyData.VALID_GAME_DATA.gameConfig));
    const reel = gatsby.getReelConfig(gameData, gatsbyData.VALID_USER_SESSION);
    expect(gameData).to.have.a.property('reelConfig');
    expect(gameData.reelConfig).to.have.a.property('NumberOfReels');
    expect(gameData.freeSpin).to.have.a.property('reelConfig');
    expect(gameData.freeSpin.reelConfig).to.have.a.property('NumberOfReels');
    for (let i = 1; i <= gameData.reelConfig.NumberOfReels; i += 1) {
      expect(gameData.reelConfig).to.have.a.property(`Reel${i}`);
      expect(gameData.freeSpin.reelConfig).to.have.a.property(`Reel${i}`);
    }
    expect(['15', '24']).to.contain.members([reel]);
    done();
  });

  /**
  * calculate followed wild path when special feature is triggered
  */
  it('calculate followed wild path when special feature is triggered', (done) => {
    const wildTrigger = {};
    const game = { default: JSON.parse(JSON.stringify(gatsbyData.VALID_GAME_DATA)) };
    const wildSymbols = [];
    const arrays = nj.arange(5 * 3).reshape(3, 5);
    const reelSet = '15';
    game.default.gameConfig.reelConfig = game.default.gameConfig.reelConfigSet.Reel15;
    const randomNumber = [16, 0, 0, 0, 16];
    gatsby.generateViewZone(randomNumber, 5, 3, [], game, arrays, gatsbyData.VALID_USER_SESSION, wildSymbols);
    gatsby.calculateWildPath(wildTrigger, game, arrays, wildSymbols, reelSet);
    expect(wildTrigger).to.have.a.property('wildSymbols').to.be.a('array');
    wildTrigger.wildSymbols.forEach((position) => {
      expect(arrays.get(position[0], position[1])).to.be.eql(wildTrigger.replacingSymbol);
    });
    expect(wildTrigger).to.have.a.property('replacingSymbol').to.be.a('string');
    done();
  });


  /**
  * calculate followed wild path when special feature is triggered
  */
  it('calculate followed wild path when special feature is triggered, for wrong reel set given', (done) => {
    const wildTrigger = {};
    const game = { default: JSON.parse(JSON.stringify(gatsbyData.VALID_GAME_DATA)) };
    const wildSymbols = [];
    const arrays = nj.arange(5 * 3).reshape(3, 5);
    // wrong reel option
    const reelSet = '12';
    game.default.gameConfig.reelConfig = game.default.gameConfig.reelConfigSet.Reel15;
    const randomNumber = [16, 0, 0, 0, 16];
    gatsby.generateViewZone(randomNumber, 5, 3, [], game, arrays, gatsbyData.VALID_USER_SESSION, wildSymbols);
    gatsby.calculateWildPath(wildTrigger, game, arrays, wildSymbols, reelSet);
    expect(wildTrigger).to.have.a.property('wildSymbols').to.be.eql([]);
    expect(wildTrigger).to.have.a.property('replacingSymbol').to.be.a('string');
    done();
  });


  /**
  * Generate game click response for gatsby game
  */
  it('Generate game click response for gatsby game', (done) => {
    const gameData = { game: { default: JSON.parse(JSON.stringify(gatsbyData.VALID_GAME_DATA)) }, userDetails: JSON.parse(JSON.stringify(gatsbyData.VALID_USER_DETAILS)) };
    gameData._id = gatsbyData.VALID_VERSION_ID;
    gatsby.generateGameClickResponse(gameData, gatsbyData.VALID_USER_TOKEN, gatsbyData.VALID_GAME_ID).then((spinResponse) => {
      expect(spinResponse).to.have.a.property('game');
      expect(spinResponse.game).to.have.a.property('gameId');
      expect(spinResponse.game).to.have.a.property('viewZone').to.be.a('number');
      expect(spinResponse.game).to.have.a.property('reels');
      expect(spinResponse.game).to.have.a.property('payArray');
      expect(spinResponse.game).to.have.a.property('payTable');
      expect(spinResponse.game).to.have.a.property('bigWins');
      expect(spinResponse.game).to.have.a.property('symbols');
      expect(spinResponse).to.have.a.property('userDetails');
      expect(spinResponse.userDetails).to.have.a.property('user');
      expect(spinResponse.userDetails.user).to.have.a.property('birthday');
      expect(spinResponse.userDetails.user).to.have.a.property('gender');
      expect(spinResponse.userDetails.user).to.have.a.property('id');
      expect(spinResponse.userDetails.user).to.have.a.property('level');
      expect(spinResponse.userDetails.user).to.have.a.property('levelProgress');
      expect(spinResponse.userDetails.user).to.have.a.property('multiplier');
      expect(spinResponse.userDetails.user).to.have.a.property('name');
      expect(spinResponse.userDetails.user).to.have.a.property('wallet');
      expect(spinResponse.userDetails).to.have.a.property('game');
      expect(spinResponse.userDetails).to.have.a.property('selectablePaylines');
      expect(spinResponse.userDetails).to.have.a.property('currentBet');
      expect(spinResponse.userDetails).to.have.a.property('currentLines');
      expect(spinResponse.userDetails).to.have.a.property('totalBet');
      expect(spinResponse.userDetails).to.have.a.property('freeSpinData');
      done();
    });
  });
});
