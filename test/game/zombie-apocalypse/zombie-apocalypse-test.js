const chai = require('chai');
const sinon = require('sinon');
const gameSession = require('../../../util/gamesession');
const constants = require('../../../response/constants');
const zombieApoc = require('../../../util/games/zombie-apocalypse');
const zaTestData = require('./test-data');
const rng = require('../../../util/rng');
const gameFunction = require('../../../helpers/gamefunction');
const zombieApocController = require('../../../controller/zombie-apocalypse');
const testData = require('../../test-data');

const expect = chai.expect;


/**
* Unicron slot related test cases are performed
* Unicron slote tests include following tests
* - Test User session computation for game click
* - Fetching user session with required reel set
* - Fetching reel configuration for subsequent spin in main game and free game
*/
describe('ZombieApocalypseTest', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
  beforeEach(() => {
    /**
    * stub to mock updating session request doc
    */
    sinon.stub(zombieApoc, 'updateSessionRequestDocForBonus').callsFake((sessionRequestKey, sessionRequectDoc, cas, status) => {
      if (cas === zaTestData.VALID_CAS_VALUE) {
        return Promise.resolve(cas);
      }

      return Promise.reject();
    });


    /**
    * stub to mock updating session doc
    */
    sinon.stub(zombieApoc, 'updateSession').callsFake((session, gameId) => {});


    /**
    * stub to mock updating bonus wins with whow api
    */
    sinon.stub(zombieApoc, 'updateBonusWins').callsFake((token, betAmount, winCoins, virtualAmount) => {});


    /**
    * stub to mock getting random numbers
    */
    sinon.stub(rng, 'generateRandomNumber').callsFake(reelConfig => [0, 4, 17, 11, 5]);


    /**
    * stub to mock getting user session and session request
    */
    sinon.stub(gameFunction, 'getUserSessionAndSessionRequest').callsFake((token, gameId) => Promise.resolve(zaTestData.VALID_USER_SESSION_AND_REQUEST_WITH_BONUS));


    /**
    * stub to mock updating session request doc
    */
    sinon.stub(gameFunction, 'getGame').callsFake((gameId) => {
      if (gameId === zaTestData.ALTERNATE_VALID_GAME_ID) {
        return { game: zaTestData.VALID_GAME_DATA };
      }
      return zaTestData.VALID_GAME_DATA;
    });


    /**
    * stub to mock getting user session and request doc from game session
    */
    sinon.stub(gameSession, 'getUserSessionAndSessionRequest').callsFake((token, gameId) => Promise.resolve(zaTestData.VALID_USER_SESSION_AND_REQUEST_WITH_BONUS));


    /**
     * stub to mock a call to whow bet api
     */
    sinon.stub(zombieApoc, 'whowBetApi').callsFake((roundId, token, zero, winAmount, betAmount, session) => Promise.resolve(zaTestData.WHOW_BET_API_RESPONSE));


     /**
      * stub to mock a call to whow close api
      */
    sinon.stub(zombieApoc, 'whowCloseApi').callsFake((roundId, token, winAmount, session) => Promise.resolve(zaTestData.WHOW_CLOSE_API_RESPONSE));


    /**
    * stub to mock getting user session
    */
    sinon.stub(gameSession, 'getUserSession').callsFake((token, gameId) => {
      if (gameId === zaTestData.VALID_GAME_ID && token === zaTestData.VALID_USER_TOKEN) {
        return Promise.resolve(zaTestData.USER_SESSION_FOR_GAMBLE);
      } else if (gameId === zaTestData.VALID_GAME_ID && token === zaTestData.VALID_USER_TOKEN) {
        return Promise.resolve(zaTestData.USER_SESSION_AVAILABLE_GAMBLE);
      } else if (gameId === zaTestData.ALTERNATE_VALID_GAME_ID && token === zaTestData.VALID_USER_TOKEN) {
        return Promise.resolve(zaTestData.USER_SESSION_COLLECT_WINNINGS);
      } else if (gameId === zaTestData.PICK_GAMBLE_GAME_ID && token === zaTestData.VALID_USER_TOKEN) {
        return Promise.resolve(zaTestData.USER_SESSION_FOR_PICK_GAMBLE);
      }

      return Promise.reject({ error: constants.RES_MESSAGES.NO_PAYTABLE });
    });
  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    zombieApoc.updateSessionRequestDocForBonus.restore();
    zombieApoc.updateSession.restore();
    zombieApoc.updateBonusWins.restore();
    rng.generateRandomNumber.restore();
    gameFunction.getUserSessionAndSessionRequest.restore();
    gameFunction.getGame.restore();
    gameSession.getUserSessionAndSessionRequest.restore();
    zombieApoc.whowBetApi.restore();
    zombieApoc.whowCloseApi.restore();
    gameSession.getUserSession.restore();
  });


  /**
  * Test case to check that a reel does not expand
  */
  it('Test case to check that a reel does not expand', (done) => {
    const game = zaTestData.VALID_GAME_DATA;
    const arrays = zaTestData.NO_WILD_SYMBOL_VIEW_ZONE();
    const result = zombieApoc.getReeslIfFrozenWild(game, arrays, zaTestData.COLUMN_SIZE, zaTestData.NOOF_REELS, []);

    /**
     * Expect that there are no expanding forzen wilds
     */
    expect(result[2]).to.be.eql(false);

    /**
     * Expect that expanding positions are empty
     */
    expect(result[1]).to.have.length(0);

    /**
     * Expect that there are no forzen wild symbols in view zone
     */
    const resultArrays = result[0];
    for (let i = 0; i < zaTestData.NOOF_REELS; i += 1) {
      for (let j = 0; j < zaTestData.COLUMN_SIZE; j += 1) {
        expect(resultArrays.get(j, i)).to.not.eql(zaTestData.WILD_SYMBOLD_ID);
      }
    }
    done();
  });


  /**
  * Test case to check that a reel does expand for frozen wild
  */
  it('Test case to check that a reel does expand for frozen wild', (done) => {
    const game = zaTestData.VALID_GAME_DATA;
    const arrays = zaTestData.WILD_SYMBOL_VIEW_ZONE();
    const result = zombieApoc.getReeslIfFrozenWild(game, arrays, zaTestData.COLUMN_SIZE, zaTestData.NOOF_REELS, []);

    /**
     * Expect that there is expanding forzen wilds
     */
    expect(result[2]).to.be.eql(true);

    /**
     * Expect that expanding positions are not empty
     */
    expect(result[1]).to.have.length.gt(0);

    /**
     * Expect that there are forzen wild symbols in view zone
     */
    const resultArrays = result[0];
    for (let i = 0; i < 1; i += 1) {
      for (let j = 0; j < zaTestData.COLUMN_SIZE; j += 1) {
        expect(resultArrays.get(j, i)).to.eql(zaTestData.WILD_SYMBOLD_ID);
      }
    }
    done();
  });


  /**
  * Test case to check that a reel expands correctly from expanding positions
  */
  it('Test case to check that a reel expands correctly from expanding positions', (done) => {
    const game = zaTestData.VALID_GAME_DATA;
    const arrays = zaTestData.WILD_SYMBOL_VIEW_ZONE();
    const expandingPositions = zaTestData.EXPANDING_POSITIONS;
    const resultArrays = zombieApoc.expandReelFromExpandingPosition(game, expandingPositions, zaTestData.COLUMN_SIZE, arrays);

    /**
     * Expect that there are forzen wild symbols in view zone
     */
    const i = expandingPositions[0].i;
    for (let j = 0; j < zaTestData.COLUMN_SIZE; j += 1) {
      expect(resultArrays.get(j, i)).to.eql(zaTestData.WILD_SYMBOLD_ID);
    }

    done();
  });


  /**
  * Check that there is no bonus won based on view zone
  */
  it('Test case to check that there is no bonus won based on view zone', (done) => {
    const game = zaTestData.VALID_GAME_DATA;
    const arrays = zaTestData.NO_WILD_SYMBOL_VIEW_ZONE();
    const bonusSymbolId = zaTestData.BONUS_SYMBOL_ID;
    const result = zombieApoc.checkIfBonusWon(game, arrays, zaTestData.COLUMN_SIZE, zaTestData.NOOF_REELS, bonusSymbolId);

    /**
     * Expect that bonus is not won
     */
    expect(result).to.eql(false);

    done();
  });


  /**
  * Check that bonus is not won when bonus symbol is present, but bonus condition is not satisfied
  */
  it('Test case to check that bonus is not won when bonus symbol is present, but bonus condition is not satisfied', (done) => {
    const game = zaTestData.VALID_GAME_DATA;
    const arrays = zaTestData.SINGLE_BONUS_SYMBOL_VIEW_ZONE();
    const bonusSymbolId = zaTestData.BONUS_SYMBOL_ID;
    const result = zombieApoc.checkIfBonusWon(game, arrays, zaTestData.COLUMN_SIZE, zaTestData.NOOF_REELS, bonusSymbolId);

    /**
     * Expect that bonus is not won
     */
    expect(result).to.eql(false);

    done();
  });


  /**
  * Check that bonus is awarded when bonus condition is satisfied
  */
  it('Test case to check that bonus is awarded when bonus condition is satisfied', (done) => {
    const game = zaTestData.VALID_GAME_DATA;
    const arrays = zaTestData.THREE_BONUS_SYMBOL_VIEW_ZONE();
    const bonusSymbolId = zaTestData.BONUS_SYMBOL_ID;
    const result = zombieApoc.checkIfBonusWon(game, arrays, zaTestData.COLUMN_SIZE, zaTestData.NOOF_REELS, bonusSymbolId);

    /**
     * Expect that bonus is awarded
     */
    expect(result).to.eql(true);

    done();
  });


  /**
  * Check that bonus object for session contains required fields
  */
  it('Check that bonus object for session contains required fields', (done) => {
    const game = zaTestData.VALID_GAME_DATA;

    const result = zombieApoc.initiateBonus(game, null);

    /**
     * Expect required fields are present in bonus object
     */
    expect(result).to.have.property('isActive');
    expect(result).to.have.property('activeStage');
    expect(result).to.have.property('totalStage');
    expect(result).to.have.property('stage');

    /**
     * Expect required properties in stage
     */
    expect(result.stage).to.have.property('stageName');
    expect(result.stage).to.have.property('noofMaps');
    expect(result.stage).to.have.property('noofPicks');
    expect(result.stage).to.have.property('noofZombies');
    expect(result.stage).to.have.property('currentPick');
    expect(result.stage).to.have.property('currentStage');
    expect(result.stage).to.have.property('shuffledZombies');


    /**
     * Expect current stage to have required count along with shuffled zombies
     */
    const currentStageKeys = Object.keys(result.stage.currentStage);
    expect(currentStageKeys).to.be.an('array').to.have.length(result.stage.noofZombies);
    expect(result.stage.shuffledZombies).to.be.an('array').to.have.length(result.stage.noofZombies);


    /**
     * Expect number of maps present in stage as expected
     */
    let mapsInArray = 0;
    currentStageKeys.forEach((stageKey) => {
      if (result.stage.currentStage[stageKey].hasMap) {
        mapsInArray += 1;
      }
    });

    expect(mapsInArray).to.be.eql(result.stage.noofMaps);
    done();
  });


  /**
  * Check for expected behaviour when a zombie in bonus is opened
  */
  it('Test case to check for expected behaviour when a zombie in bonus is opened with parallel requests', (done) => {
    const game = zaTestData.VALID_GAME_DATA;

    const session = zaTestData.VALID_USER_SESSION;


    zombieApoc.moveBonusToNextStage(session, zaTestData.VALID_NON_MAP_ZOMBIE_ID, 0, game, zaTestData.VALID_USER_TOKEN, 0, 10, false, zaTestData.VALID_GAME_ID, null, null, zaTestData.INVALID_CAS_VALUE)
    .then((result) => {}).catch(() => {
      /**
       * Expect that bonus play fails for parallel request with invalid CAS
       */
      done();
    });
  });


  /**
  * Check for expected behaviour when a zombie in bonus is opened
  */
  it('Test case to check for expected behaviour when a non map zombie in bonus is opened', (done) => {
    const game = zaTestData.VALID_GAME_DATA;

    const session = zaTestData.VALID_USER_SESSION;

    zombieApoc.moveBonusToNextStage(session, zaTestData.VALID_NON_MAP_ZOMBIE_ID, 0, game, zaTestData.VALID_USER_TOKEN, 0, 10, false, zaTestData.VALID_GAME_ID, null, null, zaTestData.VALID_CAS_VALUE)
    .then((result) => {
      /**
       * Expect that result includes data as expected
       */
      expect(result).to.have.property('hasBonusEnded').to.be.eql(false);
      expect(result).to.have.property('activeStage').to.be.eql(1);
      expect(result).to.have.property('currentPick').to.be.eql(2);
      expect(result).to.have.property('noofPicks').to.be.eql(5);
      done();
    });
  });


  /**
  * Check for expected behaviour when a zombie in bonus is opened
  */
  it('Test case to check for expected behaviour when a zombie with map in bonus is opened', (done) => {
    const game = zaTestData.VALID_GAME_DATA;

    const session = zaTestData.VALID_USER_SESSION;

    zombieApoc.moveBonusToNextStage(session, zaTestData.VALID_MAP_ZOMBIE_ID, 0, game, zaTestData.VALID_USER_TOKEN, 0, 10, false, zaTestData.VALID_GAME_ID, null, null, zaTestData.VALID_CAS_VALUE)
    .then((result) => {
      /**
       * Expect that result includes data as expected
       */
      expect(result).to.have.property('hasBonusEnded').to.be.eql(false);
      expect(result).to.have.property('activeStage').to.be.eql(1);
      expect(result).to.have.property('currentPick').to.be.eql(3);
      expect(result).to.have.property('noofPicks').to.be.eql(5);
      done();
    });
  });


  /**
  * Check for expected behaviour when a zombie in bonus is opened
  */
  it('Test case to check for expected behaviour when a zombie in bonus is opened to progress to next stage', (done) => {
    const game = zaTestData.VALID_GAME_DATA;

    const session = zaTestData.VALID_USER_SESSION_FOR_BOUS_PROGRESS;

    zombieApoc.moveBonusToNextStage(session, zaTestData.VALID_NON_MAP_ZOMBIE_ID, 0, { game }, zaTestData.VALID_USER_TOKEN, 0, 10, false, zaTestData.VALID_GAME_ID, null, null, zaTestData.VALID_CAS_VALUE)
    .then((result) => {
      /**
       * Expect that result includes data as expected
       */
      expect(result).to.have.property('hasBonusEnded').to.be.eql(false);
      expect(result).to.have.property('hasMovedtoNextStage').to.be.eql(true);
      expect(result).to.have.property('bonus');

      /**
       * Expect bonus object to have data as expected
       */
      expect(result.bonus).to.have.property('activeStage').to.be.eql(session.eventData.bonus.activeStage);
      expect(result.bonus).to.have.property('totalStage').to.be.eql(session.eventData.bonus.totalStage);
      expect(result.bonus).to.have.property('stage');

      /**
       * Expect stage to have valid data
       */
      expect(result.bonus.stage).to.have.property('stageName').to.be.eql(game.default.gameConfig.bonus.stages[session.eventData.bonus.activeStage].stageName);
      expect(result.bonus.stage).to.have.property('noofPicks').to.be.eql(game.default.gameConfig.bonus.stages[session.eventData.bonus.activeStage].noofPicks);
      expect(result.bonus.stage).to.have.property('noofZombies').to.be.eql(game.default.gameConfig.bonus.stages[session.eventData.bonus.activeStage].noofZombies);
      expect(result.bonus.stage).to.have.property('shuffledZombies').to.have.length(game.default.gameConfig.bonus.stages[session.eventData.bonus.activeStage].noofZombies);

      /**
       * Expect that shuffled zombies do not have ids outside of current stage zombies
       */
      const shuffledZombies = result.bonus.stage.shuffledZombies;
      shuffledZombies.forEach((zombieId) => {
        expect(session.eventData.bonus.stage.currentStage[zombieId]).to.not.eql(null);
      });
      done();
    });
  });


  /**
  * Check for expected behaviour when a zombie in bonus is opened
  */
  it('Test case to check for expected behaviour when a zombie in bonus is opened to end bonus game', (done) => {
    const game = zaTestData.VALID_GAME_DATA;

    const session = zaTestData.VALID_USER_SESSION_TO_END_BOUNS_PROGRESS;

    zombieApoc.moveBonusToNextStage(session, zaTestData.VALID_NON_MAP_ZOMBIE_ID, 0, { game }, zaTestData.VALID_USER_TOKEN, 0, 10, false, zaTestData.VALID_GAME_ID, null, null, zaTestData.VALID_CAS_VALUE)
    .then((result) => {
      /**
       * Expect that bonus ends
       */
      expect(result).to.have.property('hasBonusEnded').to.eql(true);
      expect(result).to.have.property('currentPick');
      expect(result).to.have.property('noofPicks');

      done();
    });
  });


  /**
  * Check for expected behaviour when a zombie in bonus is opened
  */
  it('Test case to check for expected behaviour when a zombie in bonus for normal game', (done) => {
    const game = zaTestData.VALID_GAME_DATA;

    const session = zaTestData.VALID_USER_SESSION;

    zombieApoc.moveBonusToNextStage(session, zaTestData.VALID_NON_MAP_ZOMBIE_ID, 0, { game }, zaTestData.VALID_USER_TOKEN, 0, 10, false, zaTestData.VALID_GAME_ID, null, null, zaTestData.VALID_CAS_VALUE)
    .then((result) => {
      /**
       * Expect that bonus response has required flags
       */
      expect(result).to.have.property('hasBonusEnded').to.eql(false);
      expect(result).to.have.property('currentPick');
      expect(result).to.have.property('activeStage');
      expect(result).to.have.property('noofPicks');
      done();
    });
  });


  /**
  * Check that free spin is triggered even when frozen wild expands in same reel
  */
  it('Test case to check that free spin is triggered even when frozen wild expands in same reel', (done) => {
    const game = zaTestData.VALID_GAME_DATA;
    const gameId = zaTestData.VALID_GAME_ID;
    const token = zaTestData.VALID_USER_TOKEN;
    const session = zaTestData.VALID_USER_SESSION;

    zombieApoc.compute(game, gameId, token, session)
    .then((result) => {
      /**
       * Expect results to contain data
       */
      expect(result).to.have.property('results').to.be.an('array');
      expect(result.results.freeSpins).to.be.eql(10);
      done();
    });
  });


  /**
  * Check that spin is not allowed when bonus is active
  */
  it('Test case to check that spin is not allowed when bonus is active', async () => {
    const gameId = zaTestData.VALID_GAME_ID;
    const token = zaTestData.VALID_USER_TOKEN;
    const clientID = zaTestData.VALID_CLIENT_ID;
    const result = await zombieApocController.handleSpin(gameId, token, clientID);

    /**
     * Expect a error response
     */
    expect(result).to.have.property('error').to.eql(constants.RES_MESSAGES.SPIN_NOT_ALLOWED_BONUS_ACTIVE);
  });


  /**
  * Check that when a zombie is already open in bonus, it does not count
  */
  it('Test case to check that when a zombie is already open in bonus, it does not count', async () => {
    const gameId = zaTestData.ALTERNATE_VALID_GAME_ID;
    const token = zaTestData.VALID_USER_TOKEN;
    const zombieId = zaTestData.OPENED_ZOMBIE_ID;

    const result = await zombieApoc.handleBonusRequest(token, gameId, zombieId);

    /**
     * Expect a error response
     */
    expect(result).to.have.property('error').to.eql(constants.RES_MESSAGES.ZOMBIE_BONUS_ALREADY_OPEN);
  });


  /**
  * Check that when a zombie is open in bonus
  */
  it('Test case to check that when a zombie is open in bonus', async () => {
    const gameId = zaTestData.ALTERNATE_VALID_GAME_ID;
    const token = zaTestData.VALID_USER_TOKEN;
    const zombieId = zaTestData.CLOSED_ZOMBIE_ID;


    const result = await zombieApoc.handleBonusRequest(token, gameId, zombieId);

    /**
     * Expect required properties to be present in response
     */
    expect(result).to.have.property('hasBonusEnded').to.eql(false);
    expect(result).to.have.property('activeStage').to.eql(1);
    expect(result).to.have.property('currentPick').to.eql(1);
    expect(result).to.have.property('noofPicks').to.eql(5);
    expect(result).to.have.property('isOpened').to.eql(true);
    expect(result).to.have.property('winCoins');
  });


  /**
  * Check that when a zombie is open in bonus that has a map
  */
  it('Test case to check when a zombie is open in bonus that has a map', async () => {
    const gameId = zaTestData.ALTERNATE_VALID_GAME_ID;
    const token = zaTestData.VALID_USER_TOKEN;
    const zombieId = zaTestData.ALTERNATE_VALID_NON_MAP_ZOMBIE_ID;

    const result = await zombieApoc.handleBonusRequest(token, gameId, zombieId);

    /**
     * Expect required properties to be present in response
     */
    expect(result).to.have.property('hasBonusEnded').to.eql(false);
    expect(result).to.have.property('activeStage').to.eql(1);
    expect(result).to.have.property('currentPick').to.eql(2);
    expect(result).to.have.property('noofPicks').to.eql(5);
    expect(result).to.have.property('isOpened').to.eql(true);
    expect(result).to.have.property('winCoins');
    expect(result).to.have.property('isMap').to.be.eql(true);
  });


  /**
  * Check that when a zombie is open in bonus that has a map which has was opened previously
  */
  it('Test case to check when a zombie is open in bonus that has a map which was opened previously', async () => {
    const gameId = zaTestData.ALTERNATE_VALID_GAME_ID;
    const token = zaTestData.VALID_USER_TOKEN;
    const zombieId = zaTestData.ALTERNATE_VALID_OPNEDED_MAP_ZOMBIE_ID;

    const result = await zombieApoc.handleBonusRequest(token, gameId, zombieId);

    /**
     * Expect error response
     */
    expect(result).to.have.property('error').to.eql(constants.RES_MESSAGES.ZOMBIE_BONUS_ALREADY_OPEN);
  });


  /**
  * Test case for gamble request with invalid token
  */
  it('Error Test case for gamble request with invalid token', async () => {
    const response = await zombieApoc.handleGambleRequest(zaTestData.INVALID_TOKEN, zaTestData.VALID_GAME_ID, zaTestData.VALID_CLIENT_ID, zaTestData.GAMBLE_RED_CARD);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Test case for gamble request with invalid game id
  */
  it('Error Test case for gamble request with invalid game id', async () => {
    const response = await zombieApoc.handleGambleRequest(zaTestData.VALID_USER_TOKEN, testData.INVALID_GAME_ID, zaTestData.VALID_CLIENT_ID, zaTestData.GAMBLE_RED_CARD);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Test case for gamble request with invalid client id
  */
  it('Error Test case for gamble request with invalid client id', async () => {
    const response = await zombieApoc.handleGambleRequest(zaTestData.VALID_USER_TOKEN, zaTestData.VALID_GAME_ID, zaTestData.INVALID_CLIENT_ID, zaTestData.GAMBLE_RED_CARD);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Test case for gamble request with invalid card
  */
  it('Error Test case for gamble request with invalid card', async () => {
    const response = await zombieApoc.handleGambleRequest(zaTestData.VALID_USER_TOKEN, zaTestData.VALID_GAME_ID, zaTestData.VALID_CLIENT_ID, zaTestData.GAMBLE_INVALID_CARD);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Test case for gamble request with null game parameters
  */
  it('Error Test case for gamble request with invalid card', async () => {
    const response = await zombieApoc.handleGambleRequest(null, null, null, null);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Test case for gamble request with all valid parameters to check gamble availability
  */
  it('Test case to check gamble availability', async () => {
    const token = zaTestData.VALID_USER_TOKEN;
    const gameId = zaTestData.VALID_GAME_ID;
    const clientID = zaTestData.VALID_CLIENT_ID;
    const card = zaTestData.GAMBLE_RED_CARD;
    const response = await zombieApoc.handleGambleRequest(token, gameId, clientID, card);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Test case for gamble request with all valid parameters
  */
  it('Successful Test case to check gamble, for required fields in response', async () => {
    const token = zaTestData.VALID_USER_TOKEN;
    const gameId = zaTestData.ALTERNATE_VALID_GAME_ID;
    const clientID = zaTestData.VALID_CLIENT_ID;
    const card = zaTestData.GAMBLE_RED_CARD;
    const response = await zombieApoc.handleGambleRequest(token, gameId, clientID, card);

      // expect required fields to be present with validity
    if (response.isGambleAvailable === true) {
        // when user wins
      expect(response).to.have.property('gambleWon').to.be.eql(true);
      expect(response).to.have.property('gambleCount').to.be.eql(1);
      expect(response).to.have.property('gambleTotalAmount').to.be.eql(500);
      expect(response).to.have.property('gambleBetAmount').to.be.eql(500);
      expect(response).to.have.property('gamblePotentialWinAmount').to.be.eql(response.gambleTotalAmount * 2);
      expect(response).to.have.property('gambleHistory');
      expect(response).to.have.property('wallet');
      expect(response).to.have.property('isGambleAvailable').to.be.eql(true);
    } else {
        // when user looses
      expect(response).to.have.property('gambleCount').to.be.eql(0);
      expect(response).to.have.property('gambleWon').to.be.eql(false);
      expect(response).to.have.property('gambleTotalAmount');
      expect(response).to.have.property('gambleHistory');
      expect(response).to.have.property('wallet');
      expect(response).to.have.property('isGambleAvailable').to.be.eql(false);
    }

    expect(response.gambleHistory).to.have.length.lt(6);
  });


  /**
  * Collect winnings test case for invalid token
  */
  it('Error Test case to collect winnings, invalid token', async () => {
    const token = zaTestData.INVALID_USER_TOKEN;
    const gameId = zaTestData.VALID_GAME_ID;
    const clientID = zaTestData.VALID_CLIENT_ID;
    const response = await zombieApoc.pickGamble(token, gameId, clientID);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Collect winnings test case for invalid game id
  */
  it('Error Test case to collect winnings, invalid game id', async () => {
    const token = zaTestData.VALID_USER_TOKEN;
    const gameId = testData.INVALID_GAME_ID;
    const clientID = zaTestData.VALID_CLIENT_ID;
    const response = await zombieApoc.pickGamble(token, gameId, clientID);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Collect winnings test case for invalid client id
  */
  it('Error Test case to collect winnings, invalid client id', async () => {
    const token = zaTestData.VALID_USER_TOKEN;
    const gameId = zaTestData.VALID_GAME_ID;
    const clientID = zaTestData.INVALID_CLIENT_ID;
    const response = await zombieApoc.pickGamble(token, gameId, clientID);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Collect winnings test case for valid parameters
  */
  it('Successful Test case to decline collect winnings, when there is no win', async () => {
    const token = zaTestData.VALID_USER_TOKEN;
    const gameId = testData.VALID_GAME_ID;
    const clientID = zaTestData.VALID_CLIENT_ID;
    const response = await zombieApoc.pickGamble(token, gameId, clientID);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Collect winnings test case for valid parameters
  */
  it('Successful Test case to accept collect winnings, when there is win', async () => {
    const token = zaTestData.VALID_USER_TOKEN;
    const gameId = zaTestData.PICK_GAMBLE_GAME_ID;
    const clientID = zaTestData.VALID_CLIENT_ID;
    const response = await zombieApoc.pickGamble(token, gameId, clientID);

    /**
     * Expect response to have valid properties
     */
    expect(response).to.have.property('collectAmount');
    expect(response).to.have.property('wallet');
  });
});
