const sinon = require('sinon');
const deepSeasData = require('./test-data');
const deepSeas = require('../../../util/games/deep-seas');
const gameSession = require('../../../util/gamesession');
const expect = require('chai').expect;
const nj = require('numjs');
const whowapi = require('../../../api/whowapi');

/**
* Deep Seas game's test cases are written here
* Deep Seas tests include following tests
* - get user session
*/
describe('Deep Seas Tests', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
  beforeEach(() => {
    /**
    * stub to mock getting user session
    */
    sinon.stub(gameSession, 'getUserSession').callsFake((token, gameId) => {
      if (gameId === deepSeasData.VALID_GAME_ID && token === deepSeasData.VALID_USER_TOKEN) {
        return Promise.resolve(deepSeasData.VALID_USER_SESSION);
      }
      return Promise.reject();
    });

    /**
    * stub to mock updating user session, and call whow
    */
    sinon.stub(deepSeas, 'updateSession').callsFake(() => Promise.resolve());
    /**
    * stub to mock whow api call
    */
    sinon.stub(whowapi, 'play').callsFake(() => Promise.resolve(deepSeasData.VALID_WHOW_RESPONSE));
  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    gameSession.getUserSession.restore();
    deepSeas.updateSession.restore();
    whowapi.play.restore();
  });


  /**
  * compute function test cases
  */
  it('compute function success test case', (done) => {
    deepSeas.compute(JSON.parse(JSON.stringify(deepSeasData.VALID_GAME)), deepSeasData.VALID_GAME_ID, deepSeasData.VALID_USER_TOKEN, deepSeasData.VALID_USER_SESSION)
    .then((result) => {
      expect(result).to.have.a.property('viewZone');
      expect(result).to.have.a.property('viewZoneR');
      expect(result).to.have.a.property('results').to.be.a('array');
      expect(result).to.have.a.property('resultsR').to.be.a('array');
      done();
    });
  });

  /**
  * compute function test cases for free spin
  */
  it('compute function success test case during free spin', (done) => {
    const session = JSON.parse(JSON.stringify(deepSeasData.VALID_USER_SESSION));
    session.eventData.freeSpinData = {
      currentFreeSpin: 1,
      freeSpins: 8
    };
    deepSeas.compute(JSON.parse(JSON.stringify(deepSeasData.VALID_GAME)), deepSeasData.VALID_GAME_ID, deepSeasData.VALID_USER_TOKEN, session)
    .then((result) => {
      expect(result).to.have.a.property('viewZone');
      expect(result).to.have.a.property('results').to.be.a('array');
      done();
    });
  });

  /**
  * generate combined reel config test cases
  */
  it('generate combined reel config success test cases', (done) => {
    const game = JSON.parse(JSON.stringify(deepSeasData.VALID_GAME));
    const reelConfig = deepSeas.generateCombinedReelConfig(game.default.gameConfig.reelConfigSet);
    expect(reelConfig).to.have.a.property('NumberOfReels').to.be.a('number').to.be.eql(6);
    const reels = ['Reel1', 'Reel2', 'Reel3', 'Reel4', 'Reel5', 'Reel6'];
    for (let i = 0; i < reels.length; i += 1) {
      expect(reelConfig).to.have.a.property(reels[i]);
      expect(reelConfig[reels[i]]).to.have.a.property('NumberOfRows').to.be.a('number');
      expect(reelConfig[reels[i]]).to.have.a.property('SymbolDistribution').to.be.a('array');
    }
    done();
  });

  /**
  * generate combined result test cases
  */
  it('calculate combined result when feature is triggered - success test case', (done) => {
    const game = JSON.parse(JSON.stringify(deepSeasData.VALID_GAME));
    deepSeas.calculateWinForCombinedViewZone([1, 2, 3], [4, 5, 6], game, [], 3, deepSeasData.VALID_USER_SESSION)
    .then((result) => {
      expect(result).to.have.a.property('viewZone');
      expect(result).to.have.a.property('results').to.be.a('array');
      done();
    });
  });

  /**
  * refine result test cases
  */
  it('refine result success test case', (done) => {
    const winResult = JSON.parse(JSON.stringify(deepSeasData.VALID_DUPLICATE_WIN_RESULT));
    const results = deepSeas.refineResults(winResult);
    expect(results).to.be.a('array').to.have.a.property('length').to.be.eql(1);
    done();
  });

  /**
  * generate view zone test case
  */
  it('generate view zone test case', (done) => {
    const arrays = nj.arange(3 * 3).reshape(3, 3);
    deepSeas.generateViewZone([1, 2, 3], 3, 3, [], deepSeasData.VALID_GAME, arrays, deepSeasData.VALID_USER_SESSION, 'ReelL');
    expect(arrays).to.have.a.property('get').to.be.a('function');
    expect(arrays).to.have.a.property('set').to.be.a('function');
    expect(arrays.get(0, 0)).to.be.eql('9e057a7c-404c-4a9f-9f81-5bba447b4f0f');
    expect(arrays.get(0, 1)).to.be.eql('90e0f0ab-e0a7-4832-a1f2-124c2a2f81f1');
    expect(arrays.get(0, 2)).to.be.eql('7be5dafd-c92c-47ba-a9eb-21fc4c39a433');
    done();
  });

  /**
  * get symbol function success test case
  */
  it('get symbol function success test case', (done) => {
    const symbolId = deepSeas.getSymbol(0, 0, deepSeasData.VALID_GAME.default.gameConfig.reelConfigSet.ReelL, [0, 0, 0]);
    expect(symbolId).to.be.eql('0ffd2d28-aa7e-43de-8824-b9cd32bb1ef9');
    done();
  });

  /**
  * generate game click response success test case
  */
  it('generate game click response success test case', (done) => {
    deepSeas.generateGameClickResponse({
      _id: deepSeasData.VALID_GAME_ID,
      game: deepSeasData.VALID_GAME,
      userDetails: deepSeasData.VALID_USER_DETAILS
    }, deepSeasData.VALID_USER_TOKEN, deepSeasData.VALID_GAME_ID).then((response) => {
      expect(response).to.have.a.property('game');
      expect(response).to.have.a.property('userDetails');
      expect(response.game).to.have.a.property('gameId');
      expect(response.game).to.have.a.property('viewZone');
      expect(response.game).to.have.a.property('reels');
      expect(response.game).to.have.a.property('reels2');
      expect(response.game).to.have.a.property('payArray');
      expect(response.game).to.have.a.property('payArray66');
      expect(response.game).to.have.a.property('payTable');
      expect(response.game).to.have.a.property('bigWins');
      expect(response.game).to.have.a.property('symbols');
      expect(gameSession.getUserSession.called).to.be.eql(true);
      done();
    });
  });

  /**
  * generate game spin response success test case
  */
  it('generate game spin response success test case', (done) => {
    const viewZone = nj.arange(3 * 3).reshape(3, 3);
    deepSeas.generateViewZone([1, 2, 3], 3, 3, [], deepSeasData.VALID_GAME, viewZone, deepSeasData.VALID_USER_SESSION, 'ReelL');
    const computeData = {
      results: [],
      resultsR: [],
      combinedResults: [],
      viewZone,
      viewZoneR: viewZone
    };
    deepSeas.generateSpinResponse(
      {
        _id: deepSeasData.VALID_GAME_ID,
        game: deepSeasData.VALID_GAME,
        userDetails: deepSeasData.VALID_USER_DETAILS
      }, computeData,
      deepSeasData.VALID_USER_TOKEN,
      deepSeasData.VALID_SESSION_REQUEST_KEY,
      deepSeasData.VALID_SESSION_DATA,
      deepSeasData.VALID_USER_SESSION
    ).then((response) => {
      expect(response).to.have.a.property('viewZoneL');
      expect(response).to.have.a.property('viewZoneR');
      expect(response).to.have.a.property('wins');
      expect(response).to.have.a.property('freeSpinData');
      expect(response).to.have.a.property('levelData');
      expect(response).to.have.a.property('session');
      done();
    });
  });

});
