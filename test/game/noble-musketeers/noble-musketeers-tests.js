const chai = require('chai');
const sinon = require('sinon');
const nobleMusketeersData = require('./test-data');
const nobleMusketeers = require('../../../util/games/noble-musketeers');
const gameSession = require('../../../util/gamesession');
const expect = chai.expect;
const nj = require('numjs');

/**
* Noble Musketeers game's test cases are written here
* Noble Musketeers tests include following tests
* - get user session
*/
describe('Noble Musketeers Tests', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
  beforeEach(() => {
    /**
    * stub to mock getting user session
    */
    sinon.stub(gameSession, 'getUserSession').callsFake((token, gameId) => {
      if (gameId === nobleMusketeersData.VALID_GAME_ID && token === nobleMusketeersData.VALID_USER_TOKEN) {
        return Promise.resolve(nobleMusketeersData.VALID_USER_SESSION);
      }
      return Promise.reject();
    });

    /**
    * stub to mock updating user session, and call whow
    */
    sinon.stub(nobleMusketeers, 'updateSession').callsFake((session, gameId) => Promise.resolve());
    sinon.stub(nobleMusketeers, 'callWhowForBonus').callsFake(() => Promise.resolve());
  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    gameSession.getUserSession.restore();
    nobleMusketeers.updateSession.restore();
    nobleMusketeers.callWhowForBonus.restore();
  });


  /**
  * free spin compute success tests
  */
  it('free spin compute success tests when fs is awarded by 2 musketeers', (done) => {
    const gameData = JSON.parse(JSON.stringify(nobleMusketeersData.VALID_GAME));
    nobleMusketeers.freeSpinCompute([0, 4], [], gameData.game).then((freeSpinData) => {
      expect(freeSpinData).to.have.a.property('freeSpinMultipliers').to.be.a('array');
      expect(freeSpinData).to.have.a.property('isSymbolPicked').to.be.eql(false);
      done();
    });
  });


  /**
  * free spin compute success tests
  */
  it('free spin compute success tests', (done) => {
    const gameData = JSON.parse(JSON.stringify(nobleMusketeersData.VALID_GAME));
    nobleMusketeers.freeSpinCompute([0, 2, 4], [], gameData.game).then((freeSpinData) => {
      expect(freeSpinData).to.have.a.property('freeSpins').to.be.eql(gameData.game.default.gameConfig.freeSpin.freeSpinWins[3].noofFS);
      expect(freeSpinData).to.have.a.property('freeSpinMultipliers').to.be.a('array');
      expect(freeSpinData).to.have.a.property('isSymbolPicked').to.be.eql(false);
      done();
    });
  });


  /**
  * compute function test cases
  */
  it('compute function success test case', (done) => {
    const noofReels = 5;
    const columnSize = 4;
    const gameData = JSON.parse(JSON.stringify(nobleMusketeersData.VALID_GAME));
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const randomNumber = [1, 0, 11, 5, 1];
    const results = [];
    nobleMusketeers.generateViewZone(randomNumber, noofReels, columnSize, [], [], gameData.game, arrays, nobleMusketeersData.VALID_USER_SESSION);
    nobleMusketeers.computeWins(arrays, columnSize, gameData.game, results);
    expect(results).to.be.a('array').to.have.a.property('length').to.be.eql(2);
    results.forEach((result) => {
      expect(result).to.have.a.property('winPosition').to.be.a('array').to.have.a.property('length').to.be.eql(noofReels);
      expect(result).to.have.a.property('symbolId').to.be.a('string');
      expect(result).to.have.a.property('multiplier').to.be.a('number');
      expect(result).to.have.a.property('winType').to.be.a('string');
    });
    done();
  });


  /**
  * compute function test case, for more than 3 same symbols on same reel
  */
  it('compute function success test case, for more than 3 same symbols on same reel', (done) => {
    const noofReels = 5;
    const columnSize = 4;
    const gameData = JSON.parse(JSON.stringify(nobleMusketeersData.VALID_GAME));
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const randomNumber = [1, 36, 11, 37, 29];
    const results = [];
    nobleMusketeers.generateViewZone(randomNumber, noofReels, columnSize, [], [], gameData.game, arrays, nobleMusketeersData.VALID_USER_SESSION);
    nobleMusketeers.computeWins(arrays, columnSize, gameData.game, results);
    expect(results).to.be.a('array').to.have.a.property('length').to.be.eql(3);
    results.forEach((result) => {
      expect(result).to.have.a.property('winPosition').to.be.a('array').to.have.a.property('length').to.be.eql(noofReels);
      expect(result).to.have.a.property('symbolId').to.be.a('string');
      expect(result).to.have.a.property('multiplier').to.be.a('number');
      expect(result).to.have.a.property('winType').to.be.a('string');
    });
    done();
  });


  /**
  * compute function test case, for more wilds on reel
  */
  it('compute function success test case, for more wilds on reel', (done) => {
    const noofReels = 5;
    const columnSize = 4;
    const gameData = JSON.parse(JSON.stringify(nobleMusketeersData.VALID_GAME));
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const randomNumber = [0, 9, 33, 36, 21];
    const results = [];
    nobleMusketeers.generateViewZone(randomNumber, noofReels, columnSize, [], [], gameData.game, arrays, nobleMusketeersData.VALID_USER_SESSION);
    nobleMusketeers.computeWins(arrays, columnSize, gameData.game, results);
    expect(results).to.be.a('array').to.have.a.property('length').to.be.eql(12);
    results.forEach((result) => {
      expect(result).to.have.a.property('winPosition').to.be.a('array').to.have.a.property('length').to.be.eql(noofReels);
      expect(result).to.have.a.property('symbolId').to.be.a('string');
      expect(result).to.have.a.property('multiplier').to.be.a('number');
      expect(result).to.have.a.property('winType').to.be.a('string');
    });
    done();
  });


  /**
  * handle bonus success test cases
  */
  it('handle bonus success test case for 3 musketeer pick', (done) => {
    const userSession = JSON.parse(JSON.stringify(nobleMusketeersData.VALID_USER_SESSION));
    userSession.eventData.bonusData = {
      freeSpinMultipliers: [2, 3, 4],
      isSymbolPicked: false
    };
    nobleMusketeers.handleBonusRequest(nobleMusketeersData.VALID_USER_TOKEN, nobleMusketeersData.VALID_GAME_ID, 1, userSession)
    .then((result) => {
      expect(result).to.have.a.property('response');
      expect(result.response).to.have.a.property('multiplier').to.be.eql(3);
      expect(result.response).to.have.a.property('currentMultipliers').to.be.a('array').to.have.a.property('length').to.be.eql(1);
      expect(result.response).to.have.a.property('actualMultipliers').to.be.a('array');
      expect(result).to.have.a.property('bonusObj');
      done();
    });
  });


  it('handle bonus success test case for 3 musketeer pick, when freespins are going on', (done) => {
    const userSession = JSON.parse(JSON.stringify(nobleMusketeersData.VALID_USER_SESSION));
    userSession.eventData.bonusData = {
      freeSpinMultipliers: [2, 3, 4],
      isSymbolPicked: false,
      multipliers: [2]
    };
    userSession.eventData.freeSpinData = {
      currentFreeSpin: 3,
      freeSpins: 7
    };
    nobleMusketeers.handleBonusRequest(nobleMusketeersData.VALID_USER_TOKEN, nobleMusketeersData.VALID_GAME_ID, 2, userSession)
    .then((result) => {
      expect(result).to.have.a.property('response');
      expect(result.response).to.have.a.property('multiplier').to.be.eql(4);
      expect(result.response).to.have.a.property('currentMultipliers').to.be.a('array').to.have.a.property('length').to.be.eql(2);
      expect(result.response).to.have.a.property('actualMultipliers').to.be.a('array');
      expect(result).to.have.a.property('bonusObj');
      done();
    });
  });

  it('handle bonus success test cases for two musketeer pick, and picked option to be bonus', (done) => {
    const userSession = JSON.parse(JSON.stringify(nobleMusketeersData.VALID_USER_SESSION));
    userSession.eventData.bonusData = {
      freeSpinMultipliers: [52, 2],
      isSymbolPicked: false
    };
    nobleMusketeers.handleBonusRequest(nobleMusketeersData.VALID_USER_TOKEN, nobleMusketeersData.VALID_GAME_ID, 1, userSession)
    .then((result) => {
      expect(result).to.have.a.property('response');
      expect(result.response).to.have.a.property('multiplier').to.be.eql(2);
      expect(result.response).to.have.a.property('currentMultipliers').to.be.a('array').to.have.a.property('length').to.be.eql(0);
      expect(result.response).to.have.a.property('actualMultipliers').to.be.a('array');
      expect(result.response).to.have.a.property('wins');
      expect(result.response.wins).to.have.a.property('winCoins').to.be.a('number');
      expect(result).to.have.a.property('bonusObj');
      expect(result.bonusObj).to.have.a.property('isNobelMusketeerBonus').to.be.eql(true);
      expect(result.bonusObj).to.have.a.property('isNobelMusketeerBonusAwarded').to.be.eql(true);
      expect(result.bonusObj).to.have.a.property('freeSpinData');
      expect(result.bonusObj).to.have.a.property('wins');
      expect(result.bonusObj.wins).to.have.a.property('winCoins').to.be.a('number');
      done();
    });
  });

  it('handle bonus success test cases for two musketeer pick, and picked option to be freespins', (done) => {
    const userSession = JSON.parse(JSON.stringify(nobleMusketeersData.VALID_USER_SESSION));
    userSession.eventData.bonusData = {
      freeSpinMultipliers: [52, 2],
      isSymbolPicked: false
    };
    nobleMusketeers.handleBonusRequest(nobleMusketeersData.VALID_USER_TOKEN, nobleMusketeersData.VALID_GAME_ID, 0, userSession)
    .then((result) => {
      expect(result).to.have.a.property('response');
      expect(result.response).to.have.a.property('multiplier').to.be.eql(2);
      expect(result.response).to.have.a.property('currentMultipliers').to.be.a('array').to.have.a.property('length').to.be.eql(1);
      expect(result.response).to.have.a.property('actualMultipliers').to.be.a('array');
      expect(result.response).to.have.a.property('wins');
      expect(result.response.wins).to.have.a.property('winCoins').to.be.a('number');
      expect(result).to.have.a.property('bonusObj');
      expect(result.bonusObj).to.have.a.property('isNobelMusketeerBonus').to.be.eql(true);
      expect(result.bonusObj).to.have.a.property('isNobelMusketeerBonusAwarded').to.be.eql(false);
      expect(result.bonusObj).to.have.a.property('freeSpinData');
      expect(result.bonusObj.freeSpinData).to.have.a.property('isFreeSpinAwarded').to.be.eql(true);
      expect(result.bonusObj.freeSpinData).to.have.a.property('freeSpins').to.be.eql(5);
      expect(result.bonusObj.freeSpinData).to.have.a.property('currentFreeSpin').to.be.eql(0);
      expect(result.bonusObj.freeSpinData).to.have.a.property('noofFreeSpinsAwarded').to.be.eql(5);
      expect(result.bonusObj.freeSpinData).to.have.a.property('winAmount').to.be.eql(0);
      expect(result.bonusObj).to.have.a.property('wins');
      expect(result.bonusObj.wins).to.have.a.property('winCoins').to.be.a('number');
      done();
    });
  });

  it('handle bonus success test cases for two musketeer pick, and picked option to be freespins, and freespins are going on', (done) => {
    const userSession = JSON.parse(JSON.stringify(nobleMusketeersData.VALID_USER_SESSION));
    userSession.eventData.bonusData = {
      freeSpinMultipliers: [52, 2],
      isSymbolPicked: false,
      multipliers: [2]
    };
    userSession.eventData.freeSpinData = {
      currentFreeSpin: 3,
      freeSpins: 7
    };
    nobleMusketeers.handleBonusRequest(nobleMusketeersData.VALID_USER_TOKEN, nobleMusketeersData.VALID_GAME_ID, 0, userSession)
    .then((result) => {
      expect(result).to.have.a.property('response');
      expect(result.response).to.have.a.property('multiplier').to.be.eql(2);
      expect(result.response).to.have.a.property('currentMultipliers').to.be.a('array').to.have.a.property('length').to.be.eql(2);
      expect(result.response).to.have.a.property('actualMultipliers').to.be.a('array');
      expect(result.response).to.have.a.property('wins');
      expect(result.response.wins).to.have.a.property('winCoins').to.be.a('number');
      expect(result).to.have.a.property('bonusObj');
      expect(result.bonusObj).to.have.a.property('isNobelMusketeerBonus').to.be.eql(true);
      expect(result.bonusObj).to.have.a.property('isNobelMusketeerBonusAwarded').to.be.eql(false);
      expect(result.bonusObj).to.have.a.property('freeSpinData');
      expect(result.bonusObj.freeSpinData).to.have.a.property('isFreeSpinAwarded').to.be.eql(true);
      expect(result.bonusObj.freeSpinData).to.have.a.property('freeSpins').to.be.eql(12);
      expect(result.bonusObj.freeSpinData).to.have.a.property('currentFreeSpin').to.be.eql(3);
      expect(result.bonusObj.freeSpinData).to.have.a.property('noofFreeSpinsAwarded').to.be.eql(5);
      expect(result.bonusObj).to.have.a.property('wins');
      expect(result.bonusObj.wins).to.have.a.property('winCoins').to.be.a('number');
      done();
    });
  });
});
