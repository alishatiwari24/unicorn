const chai = require('chai');
const sinon = require('sinon');
const pogData = require('./test-data');
const pacificRailRoad = require('../../../util/games/pacific-rail-road');
const gameSession = require('../../../util/gamesession');
const rng = require('../../../util/rng');
const expect = chai.expect;
const _ = require('lodash');
const nj = require('numjs');

describe('Pacific Rail Road Test', () => {

  beforeEach(() => {
    /**
     * it is used to mock gameSession method
     */
    sinon.stub(gameSession, 'getUserSession').callsFake((token, gameId) => {
      if (gameId === pogData.VALID_GAME_ID && token === pogData.VALID_USER_TOKEN) {
        return Promise.resolve(pogData.VALID_USER_SESSION);
      }
      return Promise.reject();
    });


    /**
     * getReelConfiguration method is used to get mocked reelConfig
     * @type {Object}
     */
    sinon.stub(pacificRailRoad, 'getReelConfiguration').callsFake((game, session) => {
      const gameData = { default: pogData.VALID_GAME_DATA };
      gameData.default.gameConfig.reelConfig = JSON.parse(JSON.stringify(gameData.default.gameConfig.reelConfigSet[0]));
      if (gameData.default.gameConfig.reelConfig) {
        return gameData.default.gameConfig;
      }
    });


    /**
     * generateRandomNumber method is used to get mock data for random number
     * @type {[type]}
     */
    sinon.stub(rng, 'generateRandomNumber').callsFake(game => pogData.WILD_REELS_1.rng);


    sinon.stub(pacificRailRoad, 'getRandomProbability').callsFake((game, gameType) => Math.floor(Math.random() * 3));
  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    gameSession.getUserSession.restore();
    pacificRailRoad.getReelConfiguration.restore();
    rng.generateRandomNumber.restore();
    pacificRailRoad.getRandomProbability.restore();
  });


  it('Get reel probability from specialFeature', (done) => {
    const gameData = JSON.parse(JSON.stringify({ default: pogData.VALID_GAME_DATA }));
    gameData.default.gameConfig.specialFeature = pogData.VALID_GAME_DATA.gameConfig.specialFeature;
    const probability = parseInt(pacificRailRoad.getRandomProbability(gameData, 'freeGame'));
    expect(probability).to.be.within(0, 1);
    done();
  });

  /**
  * Get reel config to be used for current spin object at zero index of reelConfigSet
  */
  it('Get reel config to be used for current spin from reelConfig 1', (done) => {
    const gameData = JSON.parse(JSON.stringify({ default: pogData.VALID_GAME_DATA }));
    gameData.default.gameConfig = pacificRailRoad.getReelConfiguration(gameData, pogData.VALID_USER_SESSION_FREE_GAME);
    expect(gameData.default.gameConfig).to.have.a.property('reelConfig');
    expect(gameData.default.gameConfig.reelConfig).to.have.a.property('NumberOfReels');
    const numberOfReel = gameData.default.gameConfig.reelConfig.NumberOfReels;
    for (let i = 1; i <= numberOfReel; i += 1) {
      expect(gameData.default.gameConfig.reelConfig).to.have.a.property(`Reel${i}`);
      expect(gameData.default.gameConfig.freeSpin.reelConfigSet[pogData.RANDOM_NUMBER_REELCONFIG1]).to.have.a.property(`Reel${i}`);
    }
    done();
  });


  /**
  * Get reel config to be used for current spin object at zero index of reelConfigSet
  */
  it('Get reel config to be used for current spin from reelConfig 2', (done) => {
    const gameData = { default: pogData.VALID_GAME_DATA };
    gameData.default.gameConfig = pacificRailRoad.getReelConfiguration(gameData, pogData.VALID_USER_SESSION_FREE_GAME);
    expect(gameData.default.gameConfig).to.have.a.property('reelConfig');
    expect(gameData.default.gameConfig.reelConfig).to.have.a.property('NumberOfReels');
    const numberOfReel = gameData.default.gameConfig.reelConfig.NumberOfReels;
    for (let i = 1; i <= numberOfReel; i += 1) {
      expect(gameData.default.gameConfig.reelConfig).to.have.a.property(`Reel${i}`);
      expect(gameData.default.gameConfig.freeSpin.reelConfigSet[pogData.RANDOM_NUMBER_REELCONFIG1]).to.have.a.property(`Reel${i}`);
    }
    done();
  });


  it('get check that freeSpin only count for stack of two (not single symblol allow only stake of two)', (done) => {
    const gameData = { default: pogData.VALID_GAME_DATA };
    gameData.default.gameConfig.reelConfig = JSON.parse(JSON.stringify(gameData.default.gameConfig.reelConfigSet[0]));
    const noofReels = gameData.default.gameConfig.reelConfigSet[0].NumberOfReels;
    const columnSize = parseInt(gameData.default.gameConfig.viewZone.toString().charAt(0));
    const rand = pogData.WILD_REELS_1.rng;
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const scatter = [];
    pacificRailRoad.generateViewZone(rand, noofReels, columnSize, scatter, gameData, arrays);
    expect(scatter.length).to.be.least(1);
    done();
  });
});
