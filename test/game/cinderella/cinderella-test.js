const chai = require('chai');
const sinon = require('sinon');
const gameSession = require('../../../util/gamesession');
const constants = require('../../../response/constants');
const cinderella = require('../../../util/games/cinderella');
const cinTestData = require('./test-data');
const rng = require('../../../util/rng');
const gameFunction = require('../../../helpers/gamefunction');
const testData = require('../../test-data');

const expect = chai.expect;


/**
* Cinderella test cases are performed
*/
describe('CinderellaTest', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
  beforeEach(() => {
    /**
    * stub to mock updating session doc
    */
    sinon.stub(cinderella, 'updateSession').callsFake((session, gameId) => {});


    // /**
    // * stub to mock getting random numbers
    // */
    // sinon.stub(rng, 'generateRandomNumber').callsFake(reelConfig => [0, 4, 17, 11, 5]);


    /**
    * stub to mock updating session request doc
    */
    sinon.stub(gameFunction, 'getGame').callsFake(gameId => cinderella.VALID_GAME_DATA);


    /**
    * stub to mock getting user session
    */
    sinon.stub(gameSession, 'getUserSession').callsFake((token, gameId) => {
      if (gameId === cinderella.VALID_GAME_ID && token === cinderella.VALID_USER_TOKEN) {
        return Promise.resolve(cinderella.USER_SESSION_FOR_GAMBLE);
      }

      return Promise.reject({ error: constants.RES_MESSAGES.NO_PAYTABLE });
    });
  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    cinderella.updateSession.restore();
    gameFunction.getGame.restore();
    gameSession.getUserSession.restore();
  });


  /**
  * Test case to wild reels yields expected output in main game
  */
  it('Test case to wild reels yields expected output in main game', (done) => {
    const session = cinTestData.USER_SESSION;
    const game = cinTestData.VALID_GAME_DATA;
    const result = cinderella.getWildReels(session, game.game);

    /**
    * Expect output to be either null or a string from wieght table
    */
    if (result !== null) {
      expect(result).to.be.a('string');
    }

    done();
  });


  /**
  * Test case to wild reels yields expected output in free game
  */
  it('Test case to wild reels yields expected output in free game', (done) => {
    const session = cinTestData.FREE_GAME_SESSION;
    const game = cinTestData.VALID_GAME_DATA;
    const result = cinderella.getWildReels(session, game.game);

    /**
    * Expect output to be either null or a string from wieght table
    */
    if (result !== null) {
      expect(result).to.be.a('string');
    }

    done();
  });


  /**
  * Test case to check that wild replaces in view zone when picked
  */
  it('Test case to check that wild replaces in view zone when picked for CASE 1', (done) => {
    const arrays = cinTestData.VIEW_ZONE();
    const wildSymbolId = cinTestData.WILD_SYMBOL_ID;
    const wildReels = cinTestData.WILDS_REELS_1;
    const game = cinTestData.VALID_GAME_DATA.game;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;

    const result = cinderella.replaceWildsInReel(arrays, wildReels, columnSize, wildSymbolId);
  /**
   * Expect result to be not empty
   */
    expect(result).to.have.length.gt(0);

    /**
     * Expect to have valid wild positions as per result
     */
    for (let i = 0; i < wildReels.length; i += 1) {
      if (wildReels[i] === '1') {
        for (let j = 0; j < columnSize; j += 1) {
          expect(arrays.get(j, i)).to.be.eql(wildSymbolId);
        }
      }
    }
    done();
  });


  /**
  * Test case to check that wild replaces in view zone when picked
  */
  it('Test case to check that wild replaces in view zone when picked for CASE 2', (done) => {
    const arrays = cinTestData.VIEW_ZONE();
    const wildSymbolId = cinTestData.WILD_SYMBOL_ID;
    const wildReels = cinTestData.WILDS_REELS_2;
    const game = cinTestData.VALID_GAME_DATA.game;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));

    const result = cinderella.replaceWildsInReel(arrays, wildReels, columnSize, wildSymbolId);
  /**
   * Expect result to be not empty
   */
    expect(result).to.have.length.gt(0);

    /**
     * Expect to have valid wild positions as per result
     */
    for (let i = 0; i < wildReels.length; i += 1) {
      if (wildReels[i] === '1') {
        for (let j = 0; j < columnSize; j += 1) {
          expect(arrays.get(j, i)).to.be.eql(wildSymbolId);
        }
      }
    }
    done();
  });


  /**
  * Test case to check that wild replaces in view zone when picked
  */
  it('Test case to check that wild replaces in view zone when picked for CASE 3', (done) => {
    const arrays = cinTestData.VIEW_ZONE();
    const wildSymbolId = cinTestData.WILD_SYMBOL_ID;
    const wildReels = cinTestData.WILDS_REELS_3;
    const game = cinTestData.VALID_GAME_DATA.game;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));

    const result = cinderella.replaceWildsInReel(arrays, wildReels, columnSize, wildSymbolId);
  /**
   * Expect result to be not empty
   */
    expect(result).to.have.length.gt(0);

    /**
     * Expect to have valid wild positions as per result
     */
    for (let i = 0; i < wildReels.length; i += 1) {
      if (wildReels[i] === '1') {
        for (let j = 0; j < columnSize; j += 1) {
          expect(arrays.get(j, i)).to.be.eql(wildSymbolId);
        }
      }
    }
    done();
  });


  /**
  * Test case to check Glass slipper payout when there is no glass slipper in view zone
  */
  it('Test case to check Glass slipper payout when there is no glass slipper in view zone', (done) => {
    const arrays = cinTestData.VIEW_ZONE();
    const game = cinTestData.VALID_GAME_DATA.game;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;

    const result = cinderella.determineBonus(noofReels, columnSize, game, arrays);

    /**
     * Expect there to be no result for glass slipper
     */
    expect(result).to.be.instanceOf(Array);
    expect(result).to.be.empty;

    done();
  });


  /**
  * Test case to check Glass slipper payout when there is single glass slipper in view zone
  */
  it('Test case to check Glass slipper payout when there single glass slipper in view zone', (done) => {
    const arrays = cinTestData.GLASS_SLIPPER_VIEW_ZONE_1();
    const game = cinTestData.VALID_GAME_DATA.game;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;

    const result = cinderella.determineBonus(noofReels, columnSize, game, arrays);

    /**
     * Expect there to be a non empty array
     */
    expect(result).to.be.instanceOf(Array);
    expect(result).to.have.length(1);

    done();
  });


  /**
  * Test case to check Glass slipper payout when there are multiple glass slipper in view zone
  */
  it('Test case to check Glass slipper payout when there are multiple glass slipper in view zone', (done) => {
    const arrays = cinTestData.GLASS_SLIPPER_VIEW_ZONE_2();
    const game = cinTestData.VALID_GAME_DATA.game;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;

    const result = cinderella.determineBonus(noofReels, columnSize, game, arrays);

    /**
     * Expect there to be a non empty array
     */
    expect(result).to.be.instanceOf(Array);
    expect(result).to.have.length.gt(1);

    done();
  });


  /**
  * Test case to check Glass slipper pays correctly when appears in view zone
  */
  it('Test case to check Glass slipper pays correctly when appears in view zone CASE 1', async () => {
    const arrays = cinTestData.GLASS_SLIPPER_VIEW_ZONE_1();
    const game = cinTestData.VALID_GAME_DATA.game;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;

    const bonus = cinderella.determineBonus(noofReels, columnSize, game, arrays);
    const result = await cinderella.scatterPayCompute(arrays, columnSize, [], game, [], bonus);
    /**
     * Expect result to to be non empty array
     */
    expect(result).to.be.instanceOf(Array);
    expect(result).to.have.length.gt(0);


    /**
     * Expect result to contain bonus object
     */
    expect(result[0]).to.have.property('bonusWin');

    /**
     * Expect bonus win to have required properties
     */
    const bonusWin = result[0].bonusWin;
    expect(bonusWin).to.have.property('multiplier');
    expect(bonusWin).to.have.property('winType');
  });


  /**
  * Test case to check Glass slipper pays correctly when appears in view zone
  */
  it('Test case to check Glass slipper pays correctly when appears in view zone CASE 2', async () => {
    const arrays = cinTestData.GLASS_SLIPPER_VIEW_ZONE_2();
    const game = cinTestData.VALID_GAME_DATA.game;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;

    const bonus = cinderella.determineBonus(noofReels, columnSize, game, arrays);
    const result = await cinderella.scatterPayCompute(arrays, columnSize, [], game, [], bonus);
    /**
     * Expect result to to be non empty array
     */
    expect(result).to.be.instanceOf(Array);
    expect(result).to.have.length.gt(0);


    /**
     * Expect result to contain bonus object
     */
    expect(result[0]).to.have.property('bonusWin');

    /**
     * Expect bonus win to have required properties
     */
    const bonusWin = result[0].bonusWin;
    expect(bonusWin).to.have.property('multiplier');
    expect(bonusWin).to.have.property('winType');
  });
});
