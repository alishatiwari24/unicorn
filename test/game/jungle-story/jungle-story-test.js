const chai = require('chai');
const sinon = require('sinon');
const jungleStoryData = require('./test-data');
const jungleStory = require('../../../util/games/jungle-story');
const gameSession = require('../../../util/gamesession');
const expect = chai.expect;
const nj = require('numjs');

/**
* Jungle Story game's test cases are written here
* Jungle Story tests include following tests
* - get user session
*/
describe('Jungle Story Tests', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
  beforeEach(() => {
    /**
    * stub to mock getting user session
    */
    sinon.stub(gameSession, 'getUserSession').callsFake((token, gameId) => {
      if (gameId === jungleStoryData.VALID_GAME_ID && token === jungleStoryData.VALID_USER_TOKEN) {
        return Promise.resolve(jungleStoryData.VALID_USER_SESSION);
      }
      return Promise.reject();
    });
  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    gameSession.getUserSession.restore();
  });


  /**
  * check if feature triggered or not success test case
  */
  it('check if feature triggered or not success test case', (done) => {
    const gameData = JSON.parse(JSON.stringify(jungleStoryData.VALID_GAME_DATA));
    const userSession = JSON.parse(JSON.stringify(jungleStoryData.VALID_USER_SESSION));
    const featureTriggered = jungleStory.checkIfFeatureTriggered(userSession, gameData.default.gameConfig.specialFeature);
    done();
  });

  /**
  * check if feature triggered or not error test case
  */
  it('check if feature triggered or not error test case', (done) => {
    const userSession = JSON.parse(JSON.stringify(jungleStoryData.VALID_USER_SESSION));
    const featureTriggered = jungleStory.checkIfFeatureTriggered(userSession, null);
    expect(featureTriggered).to.be.eql(false);
    done();
  });

  /**
  * check if feature triggered or not success test case, when fs is running
  */
  it('check if feature triggered or not success test case, when fs is running', (done) => {
    const gameData = JSON.parse(JSON.stringify(jungleStoryData.VALID_GAME_DATA));
    const userSession = JSON.parse(JSON.stringify(jungleStoryData.VALID_USER_SESSION));
    userSession.eventData.freeSpinData = { currentFreeSpin: 1, freeSpins: 5, winAmount: 1000 };
    const featureTriggered = jungleStory.checkIfFeatureTriggered(userSession, gameData.default.gameConfig.specialFeature);
    expect(featureTriggered).to.be.eql('1');
    done();
  });

  /**
  * generate view zone test case
  */
  it('generate view zone test case', (done) => {
    const gameData = JSON.parse(JSON.stringify(jungleStoryData.VALID_GAME_DATA));
    const randomNumber = [43, 7, 62, 10, 30];
    const noofReels = 5;
    const columnSize = 4;
    const scatter = [];
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const specialSymbolPositions = {};
    jungleStory.generateViewZone(randomNumber, noofReels, columnSize, scatter, gameData, arrays, specialSymbolPositions);
    expect(scatter).to.have.a.property('length').to.be.eql(2);
    expect(specialSymbolPositions).to.have.a.property('b10ebe2a-97dd-464d-bdc4-dfb218f9a5fe').to.be.a('array');
    expect(specialSymbolPositions['b10ebe2a-97dd-464d-bdc4-dfb218f9a5fe']).to.have.a.property('length').to.be.eql(5);
    expect(specialSymbolPositions).to.have.a.property('e23f72f4-a544-4c78-b922-b97dacd3d97a').to.be.a('array');
    expect(specialSymbolPositions['e23f72f4-a544-4c78-b922-b97dacd3d97a']).to.have.a.property('length').to.be.eql(2);
    done();
  });

  /**
  * calculate special feature winning test cases
  */
  it('calculate special feature winning test case for expandingWild', (done) => {
    const userSession = JSON.parse(JSON.stringify(jungleStoryData.VALID_USER_SESSION));
    const gameData = JSON.parse(JSON.stringify(jungleStoryData.VALID_GAME_DATA));
    const randomNumber = [81, 7, 62, 10, 30];
    const noofReels = 5;
    const columnSize = 4;
    const scatter = [];
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const specialSymbolPositions = {};
    jungleStory.generateViewZone(randomNumber, noofReels, columnSize, scatter, gameData, arrays, specialSymbolPositions);
    gameData.default.gameConfig.specialFeature.specialReel = ['A', 'A', 'A'];
    const featureData = jungleStory.calculateSpecialWinning(arrays, userSession, gameData.default.gameConfig.specialFeature, columnSize, specialSymbolPositions, scatter);
    expect(featureData).to.have.a.property('type').to.be.eql('expandingWild');
    expect(featureData).to.have.a.property('reels').to.be.eql([0]);
    expect(featureData).to.have.a.property('specialReel').to.be.eql(['A', 'A', 'A']);
    expect(featureData).to.have.a.property('selectedPosition').to.be.a('number');
    done();
  });

  /**
  * calculate special feature winning test cases
  */
  it('calculate special feature winning test cases for 5X winning', (done) => {
    const userSession = JSON.parse(JSON.stringify(jungleStoryData.VALID_USER_SESSION));
    const gameData = JSON.parse(JSON.stringify(jungleStoryData.VALID_GAME_DATA));
    const randomNumber = [81, 7, 62, 10, 30];
    const noofReels = 5;
    const columnSize = 4;
    const scatter = [];
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const specialSymbolPositions = {};
    jungleStory.generateViewZone(randomNumber, noofReels, columnSize, scatter, gameData, arrays, specialSymbolPositions);
    gameData.default.gameConfig.specialFeature.specialReel = ['B', 'B', 'B'];
    const featureData = jungleStory.calculateSpecialWinning(arrays, userSession, gameData.default.gameConfig.specialFeature, columnSize, specialSymbolPositions, scatter);
    expect(featureData).to.have.a.property('type').to.be.eql('5X');
    expect(featureData).to.have.a.property('multiplier').to.be.eql(5);
    expect(featureData).to.have.a.property('specialReel').to.be.eql(['B', 'B', 'B']);
    expect(featureData).to.have.a.property('selectedPosition').to.be.a('number');
    done();
  });

  /**
  * calculate special feature winning test cases
  */
  it('calculate special feature winning test cases for 5X winning', (done) => {
    const userSession = JSON.parse(JSON.stringify(jungleStoryData.VALID_USER_SESSION));
    const gameData = JSON.parse(JSON.stringify(jungleStoryData.VALID_GAME_DATA));
    const randomNumber = [81, 7, 62, 10, 30];
    const noofReels = 5;
    const columnSize = 4;
    const scatter = [];
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const specialSymbolPositions = {};
    jungleStory.generateViewZone(randomNumber, noofReels, columnSize, scatter, gameData, arrays, specialSymbolPositions);
    gameData.default.gameConfig.specialFeature.specialReel = ['C', 'C', 'C'];
    const featureData = jungleStory.calculateSpecialWinning(arrays, userSession, gameData.default.gameConfig.specialFeature, columnSize, specialSymbolPositions, scatter);
    expect(featureData).to.have.a.property('type').to.be.eql('ReplaceWithWild');
    expect(featureData).to.have.a.property('positions').to.have.length(6);
    expect(featureData).to.have.a.property('specialReel').to.be.eql(['C', 'C', 'C']);
    expect(featureData).to.have.a.property('selectedPosition').to.be.a('number');
    done();
  });

  /**
  * calculate special feature winning test cases
  */
  it('calculate special feature winning test cases for 5X winning', (done) => {
    const userSession = JSON.parse(JSON.stringify(jungleStoryData.VALID_USER_SESSION));
    const gameData = JSON.parse(JSON.stringify(jungleStoryData.VALID_GAME_DATA));
    const randomNumber = [81, 7, 62, 10, 30];
    const noofReels = 5;
    const columnSize = 4;
    const scatter = [];
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const specialSymbolPositions = {};
    jungleStory.generateViewZone(randomNumber, noofReels, columnSize, scatter, gameData, arrays, specialSymbolPositions);
    gameData.default.gameConfig.specialFeature.specialReel = ['D', 'D', 'D'];
    const featureData = jungleStory.calculateSpecialWinning(arrays, userSession, gameData.default.gameConfig.specialFeature, columnSize, specialSymbolPositions, scatter);
    expect(featureData).to.have.a.property('type').to.be.eql('ReplaceWithScatter');
    expect(featureData).to.have.a.property('positions').to.have.length(2);
    expect(featureData).to.have.a.property('specialReel').to.be.eql(['D', 'D', 'D']);
    expect(featureData).to.have.a.property('selectedPosition').to.be.a('number');
    done();
  });
});
