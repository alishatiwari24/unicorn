import * as chai from 'chai';
import * as sinon from 'sinon';
import { casanovaData } from './test-data';
import casanova from './../../../../util/games/q219/casanova';
import gameSession = require('../../../../util/gamesession');
import rng = require('../../../../util/rng');
const expect = chai.expect;
import _ from 'lodash';
import nj = require('numjs');
import { CasanovaGame, Session } from '../../../../interface/q219/cassanova-type';
import { WildOfKind, Wild } from '../../../../interface/Game';

describe('Casanova Test', () => {

    /**
     * before each test stubs are assigned value
     */
    beforeEach(() => {
        /**
         * it is used to mock gameSession method
         */
        sinon.stub(gameSession, 'getUserSession').callsFake((token: string, gameId: string) => {
            if (gameId === casanovaData.VALID_GAME_ID && token === casanovaData.VALID_USER_TOKEN) {
                return Promise.resolve(casanovaData.VALID_USER_SESSION);
            }
            return Promise.reject();
        });

        /**
         * generateRandomNumber method is used to get mock data for random number
         * @type {[type]}
         */
        sinon.stub(rng, 'generateRandomNumber').callsFake(game => {
            const num = Math.floor(Math.random() * casanovaData.RANDOM_NUMBERS.length);
            return casanovaData.RANDOM_NUMBERS[num];
        });
    });

    /**
     * after each test stubs are restored
     * restoration happpens for other tests to create stubs
     */
    afterEach(() => {
        gameSession.getUserSession.restore();
        rng.generateRandomNumber.restore();
    });

    it('Get reel probability from specialFeature for main game', done => {
        const gameData = JSON.parse(JSON.stringify({ default: casanovaData.VALID_GAME_DATA }));
        gameData.default.gameConfig.specialFeature.reelSwitching = casanovaData.DUMMY_SPECIAL_FEATURE.reelSwitching;
        const reelSet = parseInt(gameData.default.gameConfig.specialFeature.
            reelSwitching.mainGame[Math.floor(Math.random() * gameData.default.gameConfig.specialFeature.reelSwitching.mainGame.length)]
            , 10) as number;
        expect(reelSet).to.be.within(0, (gameData.default.gameConfig.reelConfigSet.length as number) - 1);
        done();
    });

    it('probability of random reel must be in reelConfigSet array index ', done => {
        const gameData = JSON.parse(JSON.stringify({ default: casanovaData.VALID_GAME_DATA }));
        gameData.default.gameConfig.specialFeature.reelSwitching = casanovaData.DUMMY_SPECIAL_FEATURE.reelSwitching;
        const probability = parseInt(gameData.default.gameConfig.specialFeature.reelSwitching.mainGame[
            Math.floor(Math.random() * gameData.default.gameConfig.specialFeature.reelSwitching.mainGame.length)], 10);
        expect(probability).to.be.within(0, gameData.default.gameConfig.reelConfigSet.length as number);
        done();
    });

    it('get check that wild multiplier is picked valid or not in free game', done => {
        const gameData: CasanovaGame['game'] = { default: casanovaData.VALID_GAME_DATA };
        gameData.default.gameConfig.reelConfig = JSON.parse(JSON.stringify(gameData.default.gameConfig.freeSpin.reelConfigSet[0]));
        const noofReels = gameData.default.gameConfig.reelConfigSet[1].NumberOfReels as number;
        const columnSize = parseInt(gameData.default.gameConfig.viewZone.toString().charAt(0), 10);
        const rand: number[] = rng.generateRandomNumber(gameData);
        const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
        const scatter: number[] = [];
        const wildOfKind: WildOfKind = {};
        casanova.generateViewZone(rand, noofReels, columnSize, scatter, gameData, arrays,
            casanovaData.VALID_USER_SESSION_FREE_GAME as Session, wildOfKind);
        if (_.isEmpty(wildOfKind)) {
            Object.keys(wildOfKind).map((key: string) => {
                const wild: Wild = wildOfKind[key] as Wild;
                expect(wild.multiplier).to.be.oneOf([5, 5, 5, 5]);
            });
        }
        done();
    });

    it('get check that wild multiplier is picked valid or not in main game', done => {
        const gameData: CasanovaGame['game'] = { default: casanovaData.VALID_GAME_DATA };
        gameData.default.gameConfig.reelConfig = JSON.parse(
            JSON.stringify(gameData.default.gameConfig.reelConfigSet[0]),
        );
        const noofReels = gameData.default.gameConfig.reelConfigSet[1].NumberOfReels as number;
        const columnSize = parseInt(gameData.default.gameConfig.viewZone.toString().charAt(0), 10);
        const rand: number[] = rng.generateRandomNumber(gameData);
        const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
        const scatter: number[] = [];
        const wildOfKind: WildOfKind = {};
        casanova.generateViewZone(rand, noofReels, columnSize, scatter, gameData, arrays, casanovaData.VALID_USER_SESSION, wildOfKind);
        if (_.isEmpty(wildOfKind)) {
            Object.keys(wildOfKind).map((key: string) => {
                const wild: Wild = wildOfKind[key] as Wild;
                expect(wild.multiplier).to.be.oneOf([2, 3, 4, 5]);
            });
        }
        done();
    });

    it('should check that total wild multiplier should be sum of wild multiplier on different reel on same payline', done => {
        const gameData = { default: casanovaData.VALID_GAME_DATA };
        casanova.compute(gameData, casanovaData.VALID_USER_SESSION)
            .then(data => {
                if (data.results.mainGameResult.length > 0) {
                    data.results.mainGameResult.forEach(resultObj => {
                        if (resultObj.totalWildMultiplier > 0) {
                            let wildMultiplierInOnePayline = 0;
                            Object.keys(resultObj.wildMultiplierPosition).forEach(reel => {
                                wildMultiplierInOnePayline +=
                                    resultObj.wildMultiplierPosition[reel].multiplier;
                            });
                            expect(resultObj.totalWildMultiplier).to.be.eql(
                                wildMultiplierInOnePayline,
                            );
                        }
                    });
                }
            });
        done();
    });
});
