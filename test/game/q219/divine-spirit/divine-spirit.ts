import * as chai from 'chai';
import * as sinon from 'sinon';
import { testData } from './test-data';
import divineSpirit from './../../../../util/games/q219/divine-spirit';
import rng = require('../../../../util/rng');
import { Compute } from '../../../../util/compute';
const expect = chai.expect;
import _ from 'lodash';
import gameEvents = require('../../../../helpers/game-events');
import {
    SessionResult, SessionObject, GameObject, ComputeData,
    GameDetail, MainGameReelConfig, Reels, SpinFinalResult, Result, SessionRequestValue,
    ViewZone, SpinResponseWins, FreeSpinDataSchema, LevelData,
} from '../../../../interface/q219/divine-spirit';

/**
 * here we are testing multiple behaviour of game
 */
describe('Divine Spirit Test Cases', () => {

    beforeEach(() => {
        sinon.stub(rng, 'generateRandomNumber').callsFake((reelConfig: MainGameReelConfig | Reels) => {
            const randomNumberArray = [testData.VALID_FREE_GAME_RANDOM, [12, 12, 12, 13, 14]];
            return randomNumberArray[Math.floor(Math.random() * randomNumberArray.length)];
        });

        sinon.stub(Compute.prototype, 'updateSession').callsFake((sessionClone: SessionObject, id: string) => {
            return Promise.resolve(true);
        });

        sinon.stub(gameEvents, 'notifyGameEvents').callsFake((obj: GameDetail, token: string,
                                                              session: SessionObject, result: Result,
                                                              freeSpins: number) => {
            return true;
        });

        sinon.stub(Compute.prototype, 'maskViewZone').callsFake((viewZone, obj) => {
            return testData.MASKED_VIEW_ZONE;
        });

        sinon.stub(Compute.prototype, 'callWhowBluehare').callsFake(() => {
            const spinResponseData = [testData.BLUEHARE_DATA, testData.BLUEHARE_DATA_WITH_FREE_SPIN];
            return Promise.resolve(spinResponseData[Math.floor(Math.random() * spinResponseData.length)]);
        });
    });

    afterEach(() => {
        rng.generateRandomNumber.restore();
        this.compute = new Compute();
        this.compute.updateSession.restore();
        gameEvents.notifyGameEvents.restore();
        this.compute.maskViewZone.restore();
        this.compute.callWhowBluehare.restore();
    });

    /**
     * it should check that whenever free spin occurs total bet multiply with current freeSpin multiplier
     */
    it('should check that when free game occurs freeSpinwin trigger according to multiplier and total bet', done => {
        const gameObject: GameObject = { default: testData.VALID_GAME_DATA };
        const sessionResult: SessionResult = testData.VALID_SESSION;
        const session: SessionObject = sessionResult[sessionResult.sessionKey].value;
        const obj: GameDetail = testData.VALID_GAME_OBJECT;
        const spinResponse: SpinFinalResult = testData.FREE_SPIN_RESULT_OF_3_KIND;

        divineSpirit.compute(gameObject, testData.VALID_USER_SESSION).then((computeData: ComputeData) => {
            if (computeData.scatter.length > 2) {
                expect(spinResponse.wins.freeSpinTriggerWin).to.be.eql((session.eventData.currentBet * session.eventData.currentLines) *
                    (obj.game.default.gameConfig.freeSpin.freeSpinWins[`${computeData.scatter.length}`].winMultiplier));
                done();
            }
        }).catch(err => {
            console.log(err);
        });
    });


    /**
     * it should check that when free spin is running and normal spin is done no free spin awarded again in freeSpin
     * than noofFreeSpinsAwarded field is not pressent
     */
    it('should check that when free spin is not awarded in free spin', done => {
        const gameObject: GameObject = { default: testData.VALID_GAME_DATA };
        const sessionResult: SessionResult = testData.VALID_SESSION;
        const session: SessionObject = sessionResult[sessionResult.sessionKey].value;
        const sessionRequest: SessionRequestValue = sessionResult[sessionResult.sessionRequestKey].value;
        const obj: GameDetail = testData.VALID_GAME_OBJECT;
        divineSpirit.compute(gameObject, testData.VALID_USER_FREE_SPIN_SESSION).then((computeData: ComputeData) => {
            divineSpirit.generateSpinResponse(obj, computeData, testData.VALID_USER_TOKEN, sessionResult.sessionRequestKey,
                sessionRequest, session).then((result: SpinFinalResult) => {
                    expect(result.freeSpinData.hasOwnProperty('noofFreeSpinsAwarded')).to.be.equal(false);
                    done();
                });
        }).catch(err => {
            console.log(err);
        });
    });


    /**
     * it should check that when free spin is running and free spin is done free spin awarded again in freeSpin
     * than noofFreeSpinsAwarded field is pressent
     */
    it('should check that when free spin is awarded in free spin', done => {
        const gameObject: GameObject = { default: testData.VALID_GAME_DATA };
        const sessionResult: SessionResult = testData.VALID_SESSION;
        const session: SessionObject = sessionResult[sessionResult.sessionKey].value;
        const sessionRequest: SessionRequestValue = sessionResult[sessionResult.sessionRequestKey].value;
        const obj: GameDetail = testData.VALID_GAME_OBJECT;
        divineSpirit.compute(gameObject, testData.VALID_USER_FREE_SPIN_SESSION).then((computeData: ComputeData) => {
            divineSpirit.generateSpinResponse(obj, computeData, testData.VALID_USER_TOKEN, sessionResult.sessionRequestKey,
                sessionRequest, session).then((result: SpinFinalResult) => {
                    expect(result.freeSpinData.hasOwnProperty('noofFreeSpinsAwarded')).to.be.equal(true);
                    done();
                });
        }).catch(err => {
            console.log(err);
        });
    });


    /**
     * it should check that  when free spin comes than what will be result object
     * it contains field noofFreeSpinsAwarded in it or not
     */
    it('should check that what will be wins object when free spin comes', done => {
        const gameObject: GameObject = { default: testData.VALID_GAME_DATA };
        const sessionResult: SessionResult = testData.VALID_SESSION;
        const session: SessionObject = sessionResult[sessionResult.sessionKey].value;
        const obj: GameDetail = testData.VALID_GAME_OBJECT;
        const noofReels: number = testData.NO_OF_REELS;
        const viewZone: ViewZone = testData.VIEWZONE;
        const result: Result = {} as Result;
        result.viewZone = viewZone;
        result.wins = {} as SpinResponseWins;
        result.freeSpinData = {} as FreeSpinDataSchema;
        result.levelData = {} as LevelData;
        result.noofReels = noofReels;
        divineSpirit.compute(gameObject, testData.VALID_USER_FREE_SPIN_SESSION).then((computeData: ComputeData) => {
            if (computeData.results.freeSpins > 0) {
                divineSpirit.buildFreeSpins(obj, computeData, session, result);
                expect(result.freeSpinData.hasOwnProperty('noofFreeSpinsAwarded')).to.be.equal(true);
                done();
            } else {
                expect(result.freeSpinData.hasOwnProperty('noofFreeSpinsAwarded')).to.be.equal(true);
                done();
            }
        }).catch(err => {
            console.log(err);
        });
    });


    /**
     * it should check that when freeSpin occurs win will be occurs and that win is updated in
     * database
     */
    it('should check that what will be response object when free spin comes', done => {
        const gameObject: GameObject = { default: testData.VALID_GAME_DATA };
        const sessionResult: SessionResult = testData.VALID_SESSION;
        const session: SessionObject = sessionResult[sessionResult.sessionKey].value;
        const obj: GameDetail = testData.VALID_GAME_OBJECT;
        const noofReels: number = testData.NO_OF_REELS;
        const viewZone: ViewZone = testData.VIEWZONE;
        const result: Result = {} as Result;
        result.viewZone = viewZone;
        result.wins = {} as SpinResponseWins;
        result.freeSpinData = {} as FreeSpinDataSchema;
        result.levelData = {} as LevelData;
        result.noofReels = noofReels;
        divineSpirit.compute(gameObject, testData.VALID_USER_FREE_SPIN_SESSION).then((computeData: ComputeData) => {
            if (computeData.results.freeSpins > 0) {
                divineSpirit.buildFreeSpins(obj, computeData, session, result);
            }
            let total: number = 0;
            computeData.results.mainGameResult.forEach(resultObj => {
                resultObj.winAmount = resultObj.multiplier * (session.eventData.currentBet);
                total += resultObj.winAmount;
                delete resultObj.multiplier;
            });
            divineSpirit.buildResponse(obj, computeData, testData.VALID_USER_TOKEN,
                sessionResult.sessionRequestKey, sessionResult[sessionResult.sessionRequestKey].value
                , session, result, total).then((data: SpinFinalResult) => {
                    expect(data.wins.winCoins).to.be.eql(4000);
                    done();
                });
        }).catch(err => {
            console.log(err);
        });
    });


    /**
     * it should check that if free spin comes in main game than win will be calculate
     * and than freeSpin will be trigger
     */
    it('should check that what will be response object when free spin comes or not in main game', done => {
        const gameObject: GameObject = { default: testData.VALID_GAME_DATA };
        const sessionResult: SessionResult = testData.VALID_SESSION;
        const session: SessionObject = sessionResult[sessionResult.sessionKey].value;
        const obj: GameDetail = testData.VALID_GAME_OBJECT;
        const noofReels: number = testData.NO_OF_REELS;
        const viewZone: ViewZone = testData.VIEWZONE;
        const result: Result = {} as Result;
        result.viewZone = viewZone;
        result.wins = {} as SpinResponseWins;
        result.freeSpinData = {} as FreeSpinDataSchema;
        result.levelData = {} as LevelData;
        result.noofReels = noofReels;
        divineSpirit.compute(gameObject, testData.VALID_USER_SESSION).then((computeData: ComputeData) => {
            if (computeData.results.freeSpins > 0) {
                divineSpirit.buildFreeSpins(obj, computeData, session, result);
            }
            let total: number = 0;
            computeData.results.mainGameResult.forEach(resultObj => {
                resultObj.winAmount = resultObj.multiplier * (session.eventData.currentBet);
                total += resultObj.winAmount;
                delete resultObj.multiplier;
            });
            divineSpirit.buildResponse(obj, computeData, testData.VALID_USER_TOKEN,
                sessionResult.sessionRequestKey, sessionResult[sessionResult.sessionRequestKey].value
                , session, result, total).then((data: SpinFinalResult) => {
                    expect(data.freeSpinData.isFreeSpinAwarded).to.be.eql(true);
                    done();
                });
        }).catch(err => {
            console.log(err);
        });
    });
});
