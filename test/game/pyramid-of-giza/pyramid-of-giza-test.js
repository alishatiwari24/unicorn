const chai = require('chai');
const sinon = require('sinon');
const pogData = require('./test-data');
const pyramidOfGiza = require('../../../util/games/pyramids-of-giza');
const gameSession = require('../../../util/gamesession');
const rng = require('../../../util/rng');
const expect = chai.expect;
const _ = require('lodash');
const nj = require('numjs');

describe('Pyramid of Giza Test', () => {

  beforeEach(() => {
    /**
     * it is used to mock gameSession method
     */
    sinon.stub(gameSession, 'getUserSession').callsFake((token, gameId) => {
      if (gameId === pogData.VALID_GAME_ID && token === pogData.VALID_USER_TOKEN) {
        return Promise.resolve(pogData.VALID_USER_SESSION);
      }
      return Promise.reject();
    });


    /**
     * getReelConfiguration method is used to get mocked reelConfig
     * @type {Object}
     */
    sinon.stub(pyramidOfGiza, 'getReelConfiguration').callsFake((game, session) => {
      const gameData = { default: pogData.VALID_GAME_DATA };
      gameData.default.gameConfig.reelConfig = JSON.parse(JSON.stringify(gameData.default.gameConfig.reelConfigSet[0]));
      if (gameData.default.gameConfig.reelConfig) {
        return gameData.default.gameConfig;
      }
    });


    /**
     * generateRandomNumber method is used to get mock data for random number
     * @type {[type]}
     */
    sinon.stub(rng, 'generateRandomNumber').callsFake(game => pogData.WILD_REELS_1.rng);


    sinon.stub(pyramidOfGiza, 'getRandomProbability').callsFake((game, gameType) => Math.floor(Math.random() * 3));
  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    gameSession.getUserSession.restore();
    pyramidOfGiza.getReelConfiguration.restore();
    rng.generateRandomNumber.restore();
    pyramidOfGiza.getRandomProbability.restore();
  });


  it('Get reel probability from specialFeature', (done) => {
    const gameData = JSON.parse(JSON.stringify({ default: pogData.VALID_GAME_DATA }));
    gameData.default.gameConfig.specialFeature = pogData.DUMMY_SPECIAL_FEATURE;
    const probability = parseInt(pyramidOfGiza.getRandomProbability(gameData, 'freeGame'));
    expect(probability).to.be.within(0, 1);
    done();
  });

  /**
  * Get reel config to be used for current spin object at zero index of reelConfigSet
  */
  it('Get reel config to be used for current spin from reelConfig 1', (done) => {
    const gameData = JSON.parse(JSON.stringify({ default: pogData.VALID_GAME_DATA }));
    gameData.default.gameConfig = pyramidOfGiza.getReelConfiguration(gameData, pogData.VALID_USER_SESSION);
    expect(gameData.default.gameConfig).to.have.a.property('reelConfig');
    expect(gameData.default.gameConfig.reelConfig).to.have.a.property('NumberOfReels');
    const numberOfReels = gameData.default.gameConfig.reelConfig.NumberOfReels;
      for (let i = 1; i <= numberOfReels; i += 1) {
        expect(gameData.default.gameConfig.reelConfig).to.have.a.property(`Reel${i}`);
        expect(gameData.default.gameConfig.freeSpin.reelConfigSet[pogData.RANDOM_NUMBER_REELCONFIG2]).to.have.a.property(`Reel${i}`);
      }
    done();
  });


  /**
  * Get reel config to be used for current spin object at zero index of reelConfigSet
  */
  it('Get reel config to be used for current spin from reelConfig 2', (done) => {
    const gameData = { default: pogData.VALID_GAME_DATA };
    gameData.default.gameConfig = pyramidOfGiza.getReelConfiguration(gameData, pogData.VALID_USER_SESSION_FREE_GAME);
    expect(gameData.default.gameConfig).to.have.a.property('reelConfig');
    expect(gameData.default.gameConfig.reelConfig).to.have.a.property('NumberOfReels');
    const numberOfReels = gameData.default.gameConfig.reelConfig.NumberOfReels;
      for (let i = 1; i <= numberOfReels; i += 1) {
        expect(gameData.default.gameConfig.reelConfig).to.have.a.property(`Reel${i}`);
        expect(gameData.default.gameConfig.freeSpin.reelConfigSet[pogData.RANDOM_NUMBER_REELCONFIG2]).to.have.a.property(`Reel${i}`);
      }
    done();
  });


  it('get check that wild is expanding or not in free game', (done) => {
    const gameData = { default: pogData.VALID_GAME_DATA };
    gameData.default.gameConfig.reelConfig = JSON.parse(JSON.stringify(gameData.default.gameConfig.freeSpin.reelConfigSet[0]));
    const noofReels = gameData.default.gameConfig.reelConfigSet[1].NumberOfReels;
    const columnSize = parseInt(gameData.default.gameConfig.viewZone.toString().charAt(0));
    const rand = pogData.WILD_REELS_1.rng;
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const scatter = [];
    const pos = {};
    pyramidOfGiza.generateViewZone(rand, noofReels, columnSize, scatter, gameData, arrays, pogData.VALID_USER_SESSION_FREE_GAME, pos);
    expect(Object.keys(pos).length).to.be.least(1);
    done();
  });

  it('get check that wild is not expanding in mainGame', (done) => {
    const gameData = { default: pogData.VALID_GAME_DATA };
    gameData.default.gameConfig.reelConfig = JSON.parse(JSON.stringify(gameData.default.gameConfig.reelConfigSet[0]));
    const noofReels = gameData.default.gameConfig.reelConfigSet[1].NumberOfReels;
    const columnSize = parseInt(gameData.default.gameConfig.viewZone.toString().charAt(0));
    const rand = pogData.WILD_REELS_1.rng;
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const scatter = [];
    const pos = {};
    pyramidOfGiza.generateViewZone(rand, noofReels, columnSize, scatter, gameData, arrays, pogData.VALID_USER_SESSION, pos);
    expect(Object.keys(pos).length).to.be.least(1);
    done();
  });


  it('should check that reel is expanding wildin free game and win is correct', (done) => {
    const gameData = { default: pogData.VALID_GAME_DATA };
    pyramidOfGiza.compute(gameData, pogData.VALID_GAME_ID, pogData.VALID_USER_TOKEN, pogData.VALID_USER_SESSION_FREE_GAME).then((data) => {
    expect(data.results.length).to.be.eql(pogData.WILD_REELS_1.wins);
    });
    done();
  });
});
