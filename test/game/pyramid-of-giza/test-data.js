module.exports = {
    VALID_USER_SESSION: JSON.parse('{"_type":"userSession","_updatedTimestamp":"2019-03-01T07:38:39.847Z","clientID":"xQEPNoytcC1Ud1330i4vdeIVAepS9t2r","event":"spin","eventData":{"currentBet":50,"currentLines":40,"freeSpinData":{},"userDetails":{"id":"59aebc7bf3869b5be10ec66d","name":"BillyRacoon","gender":"na","wallet":{"chips":843012193},"locale":"en_GB","birthday":"01-01-1970","level":25,"levelProgress":0.93,"multiplier":1}},"game":{"settings":{"bets":[50,150,250,500,750,1000,1250,1500,1750,2000,2250,2500]},"betAmount":2000 }}'),

    VALID_USER_SESSION_FREE_GAME: JSON.parse('{"_type":"userSession","_updatedTimestamp":"2019-03-04T08:55:32.503Z","clientID":"3zq6HW3xcA7rMPzRFz4phak5a9iEW92E","event":"spin","eventData":{"currentBet":50,"currentLines":40,"freeSpinData":{"freeSpins":10,"currentFreeSpin":0,"winAmount":0},"userDetails":{"id":"59aebc7bf3869b5be10ec66d","name":"BillyRacoon","gender":"na","wallet":{"chips":843010193},"locale":"en_GB","birthday":"01-01-1970","level":25,"levelProgress":0.93,"multiplier":1}},"game":{"settings":{"bets":[50,150,250,500,750,1000,1250,1500,1750,2000,2250,2500]},"betAmount":2000}}'),

    VALID_VERSION_ID: '9e4f8cd2-8601-4967-b309-32e932c37167',

    VALID_GAME_DATA: {
	'_type': 'gameConfig',
	'baseGameId': 'd3401dc4-8a61-43b0-9764-50c8c7daef3d',
	'createdAt': 1552312712343,
	'environment': 'completed',
	'gameConfig': {
	'bigWins': {
		'bigWin': {
			'maxBetMultiplier': 20,
			'minBetMultiplier': 10
		},
		'legendaryWin': {
			'maxBetMultiplier': null,
			'minBetMultiplier': 50
		},
		'monsterWin': {
			'maxBetMultiplier': 50,
			'minBetMultiplier': 20
		}
	},
	'freeSpin': {
		'SymbolName': 'FreeSpins',
		'SymbolType': 'Scatter',
		'freeSpinWins': {
			'3': {
				'noofFS': 10,
				'winMultiplier': 1
			},
			'4': {
				'noofFS': 20,
				'winMultiplier': 1
			},
			'5': {
				'noofFS': 30,
				'winMultiplier': 1
			}
		},
		'isFSReelDiff': true,
		'keys': ['3', '4', '5'],
		'minimumRequiredCount': 3,
		'reelConfigSet': [{
			'NumberOfReels': 5,
			'Reel1': {
				'NumberOfRows': 82,
				'SymbolDistribution': [{
					'RowNumber': 0,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 1,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 2,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 3,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 4,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 5,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 6,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 7,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 8,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 9,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 10,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 11,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 12,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 13,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 14,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 15,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 16,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 17,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 18,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 19,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 20,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 21,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 22,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 23,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 24,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 25,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 26,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 27,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 28,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 29,
					'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
					'SymbolName': 'FreeSpins',
					'SymbolType': 'Scatter',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
				}, {
					'RowNumber': 30,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 31,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 32,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 33,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 34,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 35,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 36,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 37,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 38,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 39,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 40,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 41,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 42,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 43,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 44,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 45,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 46,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 47,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 48,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 49,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 50,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 51,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 52,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 53,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 54,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 55,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 56,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 57,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 58,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 59,
					'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
					'SymbolName': 'FreeSpins',
					'SymbolType': 'Scatter',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
				}, {
					'RowNumber': 60,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 61,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 62,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 63,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 64,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 65,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 66,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 67,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 68,
					'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
					'SymbolName': 'FreeSpins',
					'SymbolType': 'Scatter',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
				}, {
					'RowNumber': 69,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 70,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 71,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 72,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 73,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 74,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 75,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 76,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 77,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 78,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 79,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 80,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 81,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}]
			},
			'Reel2': {
				'NumberOfRows': 82,
				'SymbolDistribution': [{
					'RowNumber': 0,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 1,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 2,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 3,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 4,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 5,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 6,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 7,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 8,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 9,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 10,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 11,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 12,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 13,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 14,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 15,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 16,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 17,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 18,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 19,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 20,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 21,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 22,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 23,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 24,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 25,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 26,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 27,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 28,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 29,
					'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
					'SymbolName': 'FreeSpins',
					'SymbolType': 'Scatter',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
				}, {
					'RowNumber': 30,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 31,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 32,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 33,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 34,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 35,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 36,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 37,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 38,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 39,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 40,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 41,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 42,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 43,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 44,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 45,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 46,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 47,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 48,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 49,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 50,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 51,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 52,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 53,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 54,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 55,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 56,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 57,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 58,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 59,
					'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
					'SymbolName': 'FreeSpins',
					'SymbolType': 'Scatter',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
				}, {
					'RowNumber': 60,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 61,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 62,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 63,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 64,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 65,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 66,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 67,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 68,
					'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
					'SymbolName': 'FreeSpins',
					'SymbolType': 'Scatter',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
				}, {
					'RowNumber': 69,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 70,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 71,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 72,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 73,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 74,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 75,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 76,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 77,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 78,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 79,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 80,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 81,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}]
			},
			'Reel3': {
				'NumberOfRows': 82,
				'SymbolDistribution': [{
					'RowNumber': 0,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 1,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 2,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 3,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 4,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 5,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 6,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 7,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 8,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 9,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 10,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 11,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 12,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 13,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 14,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 15,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 16,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 17,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 18,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 19,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 20,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 21,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 22,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 23,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 24,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 25,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 26,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 27,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 28,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 29,
					'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
					'SymbolName': 'FreeSpins',
					'SymbolType': 'Scatter',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
				}, {
					'RowNumber': 30,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 31,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 32,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 33,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 34,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 35,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 36,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 37,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 38,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 39,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 40,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 41,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 42,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 43,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 44,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 45,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 46,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 47,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 48,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 49,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 50,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 51,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 52,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 53,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 54,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 55,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 56,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 57,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 58,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 59,
					'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
					'SymbolName': 'FreeSpins',
					'SymbolType': 'Scatter',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
				}, {
					'RowNumber': 60,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 61,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 62,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 63,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 64,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 65,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 66,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 67,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 68,
					'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
					'SymbolName': 'FreeSpins',
					'SymbolType': 'Scatter',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
				}, {
					'RowNumber': 69,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 70,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 71,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 72,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 73,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 74,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 75,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 76,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 77,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 78,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 79,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 80,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 81,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}]
			},
			'Reel4': {
				'NumberOfRows': 82,
				'SymbolDistribution': [{
					'RowNumber': 0,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 1,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 2,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 3,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 4,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 5,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 6,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 7,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 8,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 9,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 10,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 11,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 12,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 13,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 14,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 15,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 16,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 17,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 18,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 19,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 20,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 21,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 22,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 23,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 24,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 25,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 26,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 27,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 28,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 29,
					'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
					'SymbolName': 'FreeSpins',
					'SymbolType': 'Scatter',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
				}, {
					'RowNumber': 30,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 31,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 32,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 33,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 34,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 35,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 36,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 37,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 38,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 39,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 40,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 41,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 42,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 43,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 44,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 45,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 46,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 47,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 48,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 49,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 50,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 51,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 52,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 53,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 54,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 55,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 56,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 57,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 58,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 59,
					'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
					'SymbolName': 'FreeSpins',
					'SymbolType': 'Scatter',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
				}, {
					'RowNumber': 60,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 61,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 62,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 63,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 64,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 65,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 66,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 67,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 68,
					'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
					'SymbolName': 'FreeSpins',
					'SymbolType': 'Scatter',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
				}, {
					'RowNumber': 69,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 70,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 71,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 72,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 73,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 74,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 75,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 76,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 77,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 78,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 79,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 80,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 81,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}]
			},
			'Reel5': {
				'NumberOfRows': 82,
				'SymbolDistribution': [{
					'RowNumber': 0,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 1,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 2,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 3,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 4,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 5,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 6,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 7,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 8,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 9,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 10,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 11,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 12,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 13,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 14,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 15,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 16,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 17,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 18,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 19,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 20,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 21,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 22,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 23,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 24,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 25,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 26,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 27,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 28,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 29,
					'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
					'SymbolName': 'FreeSpins',
					'SymbolType': 'Scatter',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
				}, {
					'RowNumber': 30,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 31,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 32,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 33,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 34,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 35,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 36,
					'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
					'SymbolName': 'Wild',
					'SymbolType': 'Wild',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
				}, {
					'RowNumber': 37,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 38,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 39,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 40,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 41,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 42,
					'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
					'SymbolName': 'FreeSpins',
					'SymbolType': 'Scatter',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
				}, {
					'RowNumber': 43,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 44,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 45,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 46,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 47,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 48,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 49,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 50,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 51,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 52,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 53,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 54,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 55,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 56,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 57,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 58,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 59,
					'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
					'SymbolName': 'FreeSpins',
					'SymbolType': 'Scatter',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
				}, {
					'RowNumber': 60,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 61,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 62,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 63,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 64,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 65,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 66,
					'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
					'SymbolName': 'Low1',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
				}, {
					'RowNumber': 67,
					'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
					'SymbolName': 'Hi2',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
				}, {
					'RowNumber': 68,
					'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
					'SymbolName': 'FreeSpins',
					'SymbolType': 'Scatter',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
				}, {
					'RowNumber': 69,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 70,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 71,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 72,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}, {
					'RowNumber': 73,
					'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
					'SymbolName': 'Hi4',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
				}, {
					'RowNumber': 74,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 75,
					'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
					'SymbolName': 'Hi6',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
				}, {
					'RowNumber': 76,
					'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
					'SymbolName': 'Low3',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
				}, {
					'RowNumber': 77,
					'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
					'SymbolName': 'Hi3',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
				}, {
					'RowNumber': 78,
					'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
					'SymbolName': 'Hi5',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
				}, {
					'RowNumber': 79,
					'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
					'SymbolName': 'Low2',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
				}, {
					'RowNumber': 80,
					'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
					'SymbolName': 'Hi1',
					'SymbolType': 'High Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
				}, {
					'RowNumber': 81,
					'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
					'SymbolName': 'Low4',
					'SymbolType': 'Low Paying',
					'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
				}]
			}
		}],
		'symbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1'
	},
	'payLines': {
		'PayArray': [{
			'_id': '827bbd13-fb61-4ff4-9e54-7ce4503fe0a4',
			'five': 1,
			'four': 1,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 1,
			'three': 1,
			'two': 1
		}, {
			'_id': '2535c660-4536-4ef7-aeb5-fed87bdca374',
			'five': 0,
			'four': 0,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 0,
			'three': 0,
			'two': 0
		}, {
			'_id': 'c4644256-74c4-46f5-b4b7-8761ffebf945',
			'five': 2,
			'four': 2,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 2,
			'three': 2,
			'two': 2
		}, {
			'_id': 'bcb20a5d-d832-411f-af38-4cfd59c21125',
			'five': 0,
			'four': 1,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 0,
			'three': 2,
			'two': 1
		}, {
			'_id': '52af41f7-299f-469f-811c-baafb7cd3ce4',
			'five': 2,
			'four': 1,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 2,
			'three': 0,
			'two': 1
		}, {
			'_id': '09daf507-fb02-4991-a817-1e074a148373',
			'five': 1,
			'four': 0,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 1,
			'three': 0,
			'two': 0
		}, {
			'_id': '7020059d-492c-4d16-ae23-e64b8b95b483',
			'five': 1,
			'four': 2,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 1,
			'three': 2,
			'two': 2
		}, {
			'_id': '9a7d2080-01fd-4be4-9f92-59e62ce76023',
			'five': 2,
			'four': 2,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 0,
			'three': 1,
			'two': 0
		}, {
			'_id': 'cbdc55af-9755-4693-9362-17a462fa29d6',
			'five': 0,
			'four': 0,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 2,
			'three': 1,
			'two': 2
		}, {
			'_id': 'c94e0dcd-1ca7-4fca-a209-3c9896b83c3c',
			'five': 1,
			'four': 0,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 1,
			'three': 1,
			'two': 2
		}, {
			'_id': '8e9df29f-0c42-42ad-b283-f466ecb7de0e',
			'five': 1,
			'four': 2,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 1,
			'three': 1,
			'two': 0
		}, {
			'_id': '2af36175-d939-4299-98b3-602cf20f215e',
			'five': 0,
			'four': 1,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 0,
			'three': 1,
			'two': 1
		}, {
			'_id': '150e5282-c17c-4877-914c-76be150ef77d',
			'five': 2,
			'four': 1,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 2,
			'three': 1,
			'two': 1
		}, {
			'_id': '631cde62-6f97-4f21-8f25-f2a0124f8ab9',
			'five': 0,
			'four': 1,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 0,
			'three': 0,
			'two': 1
		}, {
			'_id': '44b67650-2572-419b-971f-574d052dd3cd',
			'five': 2,
			'four': 1,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 2,
			'three': 2,
			'two': 1
		}, {
			'_id': 'aba1765e-664c-4c48-8ded-9308dfb96be1',
			'five': 1,
			'four': 1,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 1,
			'three': 0,
			'two': 1
		}, {
			'_id': 'd98c00c2-12cc-4959-b4bb-1ec1fd1b60fd',
			'five': 1,
			'four': 1,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 1,
			'three': 2,
			'two': 1
		}, {
			'_id': '4f7e9886-1bf1-44e8-abb9-45379ff28e27',
			'five': 0,
			'four': 0,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 0,
			'three': 2,
			'two': 0
		}, {
			'_id': '27f0e358-32ab-4ab9-9a53-1b702077b687',
			'five': 2,
			'four': 2,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 2,
			'three': 0,
			'two': 2
		}, {
			'_id': 'cd9e7c52-389c-490f-af65-be3f2df70d5a',
			'five': 0,
			'four': 2,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 0,
			'three': 2,
			'two': 2
		}, {
			'_id': '77ebf8c4-76af-4e40-9f00-35b574ecd8fe',
			'five': 2,
			'four': 0,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 2,
			'three': 0,
			'two': 0
		}, {
			'_id': 'ce1007c6-a480-45af-9c27-6d51e4adeb68',
			'five': 1,
			'four': 2,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 1,
			'three': 0,
			'two': 2
		}, {
			'_id': '9afd2ad3-f1f5-4a6d-b1e2-2cfe2bfb6172',
			'five': 1,
			'four': 0,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 1,
			'three': 2,
			'two': 0
		}, {
			'_id': '0582086d-bfc1-4ee1-bde1-522ae53a9b06',
			'five': 0,
			'four': 2,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 0,
			'three': 0,
			'two': 2
		}, {
			'_id': '2482f844-ef8a-422d-86d6-8c581b24bbb5',
			'five': 2,
			'four': 0,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 2,
			'three': 2,
			'two': 0
		}, {
			'_id': 'f2b45047-6e07-423c-803c-77c3966dee29',
			'five': 0,
			'four': 2,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 2,
			'three': 1,
			'two': 0
		}, {
			'_id': '17da128a-e282-4ba3-b94d-dfafe4fc3c03',
			'five': 2,
			'four': 0,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 0,
			'three': 1,
			'two': 2
		}, {
			'_id': '364d22e9-39fe-4e6b-a5e6-833152faa30d',
			'five': 0,
			'four': 2,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 0,
			'three': 1,
			'two': 2
		}, {
			'_id': '95f5c8fa-3669-4e0a-a84d-69d58523e5a6',
			'five': 2,
			'four': 0,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 2,
			'three': 1,
			'two': 0
		}, {
			'_id': 'aa1b058f-2c80-4c11-b452-13c098965809',
			'five': 1,
			'four': 0,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 2,
			'three': 0,
			'two': 1
		}, {
			'_id': '0a1e990f-39d8-4222-80dc-6183f8ec8032',
			'five': 1,
			'four': 2,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 0,
			'three': 2,
			'two': 1
		}, {
			'_id': '9c9c81eb-1a51-4f6c-b1de-35817508a4d7',
			'five': 2,
			'four': 2,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 0,
			'three': 2,
			'two': 0
		}, {
			'_id': '7d321d9f-9387-4e9d-953a-eb4b95434396',
			'five': 0,
			'four': 0,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 2,
			'three': 0,
			'two': 2
		}, {
			'_id': 'fef4a6e7-c754-423c-90e5-e48ad40af836',
			'five': 2,
			'four': 1,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 1,
			'three': 2,
			'two': 0
		}, {
			'_id': 'b318b438-3152-4b04-b9bb-0208e543743e',
			'five': 0,
			'four': 1,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 1,
			'three': 0,
			'two': 2
		}, {
			'_id': 'b78c43f6-7ac6-4162-8e94-90140637cd41',
			'five': 2,
			'four': 1,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 0,
			'three': 0,
			'two': 1
		}, {
			'_id': 'b6e6b104-8d84-40d2-a639-1d401589c077',
			'five': 0,
			'four': 1,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 2,
			'three': 2,
			'two': 1
		}, {
			'_id': 'a6a8b0bc-0a7b-4dd4-9521-b81b53afcc00',
			'five': 0,
			'four': 0,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 1,
			'three': 2,
			'two': 2
		}, {
			'_id': '7421c2bc-16e6-4b0c-ad90-fe742c91e024',
			'five': 2,
			'four': 1,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 0,
			'three': 1,
			'two': 0
		}, {
			'_id': '68e4723a-aa38-41d2-b2e4-65947f0611e8',
			'five': 0,
			'four': 1,
			'keys': ['one', 'two', 'three', 'four', 'five'],
			'one': 2,
			'three': 1,
			'two': 2
		}],
		'normalizedPayArray': [{
			'_id': '827bbd13-fb61-4ff4-9e54-7ce4503fe0a4',
			'data': [1, 1, 1, 1, 1]
		}, {
			'_id': '2535c660-4536-4ef7-aeb5-fed87bdca374',
			'data': [0, 0, 0, 0, 0]
		}, {
			'_id': 'c4644256-74c4-46f5-b4b7-8761ffebf945',
			'data': [2, 2, 2, 2, 2]
		}, {
			'_id': 'bcb20a5d-d832-411f-af38-4cfd59c21125',
			'data': [0, 1, 2, 1, 0]
		}, {
			'_id': '52af41f7-299f-469f-811c-baafb7cd3ce4',
			'data': [2, 1, 0, 1, 2]
		}, {
			'_id': '09daf507-fb02-4991-a817-1e074a148373',
			'data': [1, 0, 0, 0, 1]
		}, {
			'_id': '7020059d-492c-4d16-ae23-e64b8b95b483',
			'data': [1, 2, 2, 2, 1]
		}, {
			'_id': '9a7d2080-01fd-4be4-9f92-59e62ce76023',
			'data': [0, 0, 1, 2, 2]
		}, {
			'_id': 'cbdc55af-9755-4693-9362-17a462fa29d6',
			'data': [2, 2, 1, 0, 0]
		}, {
			'_id': 'c94e0dcd-1ca7-4fca-a209-3c9896b83c3c',
			'data': [1, 2, 1, 0, 1]
		}, {
			'_id': '8e9df29f-0c42-42ad-b283-f466ecb7de0e',
			'data': [1, 0, 1, 2, 1]
		}, {
			'_id': '2af36175-d939-4299-98b3-602cf20f215e',
			'data': [0, 1, 1, 1, 0]
		}, {
			'_id': '150e5282-c17c-4877-914c-76be150ef77d',
			'data': [2, 1, 1, 1, 2]
		}, {
			'_id': '631cde62-6f97-4f21-8f25-f2a0124f8ab9',
			'data': [0, 1, 0, 1, 0]
		}, {
			'_id': '44b67650-2572-419b-971f-574d052dd3cd',
			'data': [2, 1, 2, 1, 2]
		}, {
			'_id': 'aba1765e-664c-4c48-8ded-9308dfb96be1',
			'data': [1, 1, 0, 1, 1]
		}, {
			'_id': 'd98c00c2-12cc-4959-b4bb-1ec1fd1b60fd',
			'data': [1, 1, 2, 1, 1]
		}, {
			'_id': '4f7e9886-1bf1-44e8-abb9-45379ff28e27',
			'data': [0, 0, 2, 0, 0]
		}, {
			'_id': '27f0e358-32ab-4ab9-9a53-1b702077b687',
			'data': [2, 2, 0, 2, 2]
		}, {
			'_id': 'cd9e7c52-389c-490f-af65-be3f2df70d5a',
			'data': [0, 2, 2, 2, 0]
		}, {
			'_id': '77ebf8c4-76af-4e40-9f00-35b574ecd8fe',
			'data': [2, 0, 0, 0, 2]
		}, {
			'_id': 'ce1007c6-a480-45af-9c27-6d51e4adeb68',
			'data': [1, 2, 0, 2, 1]
		}, {
			'_id': '9afd2ad3-f1f5-4a6d-b1e2-2cfe2bfb6172',
			'data': [1, 0, 2, 0, 1]
		}, {
			'_id': '0582086d-bfc1-4ee1-bde1-522ae53a9b06',
			'data': [0, 2, 0, 2, 0]
		}, {
			'_id': '2482f844-ef8a-422d-86d6-8c581b24bbb5',
			'data': [2, 0, 2, 0, 2]
		}, {
			'_id': 'f2b45047-6e07-423c-803c-77c3966dee29',
			'data': [2, 0, 1, 2, 0]
		}, {
			'_id': '17da128a-e282-4ba3-b94d-dfafe4fc3c03',
			'data': [0, 2, 1, 0, 2]
		}, {
			'_id': '364d22e9-39fe-4e6b-a5e6-833152faa30d',
			'data': [0, 2, 1, 2, 0]
		}, {
			'_id': '95f5c8fa-3669-4e0a-a84d-69d58523e5a6',
			'data': [2, 0, 1, 0, 2]
		}, {
			'_id': 'aa1b058f-2c80-4c11-b452-13c098965809',
			'data': [2, 1, 0, 0, 1]
		}, {
			'_id': '0a1e990f-39d8-4222-80dc-6183f8ec8032',
			'data': [0, 1, 2, 2, 1]
		}, {
			'_id': '9c9c81eb-1a51-4f6c-b1de-35817508a4d7',
			'data': [0, 0, 2, 2, 2]
		}, {
			'_id': '7d321d9f-9387-4e9d-953a-eb4b95434396',
			'data': [2, 2, 0, 0, 0]
		}, {
			'_id': 'fef4a6e7-c754-423c-90e5-e48ad40af836',
			'data': [1, 0, 2, 1, 2]
		}, {
			'_id': 'b318b438-3152-4b04-b9bb-0208e543743e',
			'data': [1, 2, 0, 1, 0]
		}, {
			'_id': 'b78c43f6-7ac6-4162-8e94-90140637cd41',
			'data': [0, 1, 0, 1, 2]
		}, {
			'_id': 'b6e6b104-8d84-40d2-a639-1d401589c077',
			'data': [2, 1, 2, 1, 0]
		}, {
			'_id': 'a6a8b0bc-0a7b-4dd4-9521-b81b53afcc00',
			'data': [1, 2, 2, 0, 0]
		}, {
			'_id': '7421c2bc-16e6-4b0c-ad90-fe742c91e024',
			'data': [0, 0, 1, 1, 2]
		}, {
			'_id': '68e4723a-aa38-41d2-b2e4-65947f0611e8',
			'data': [2, 2, 1, 1, 0]
		}]
	},
	'payTable': {
		'1ed95855-19f2-4bab-a37d-a8b5ddb88e81': {
			'3': {
				'Amount': 0,
				'Multiplier': 10
			},
			'4': {
				'Amount': 0,
				'Multiplier': 40
			},
			'5': {
				'Amount': 0,
				'Multiplier': 100
			}
		},
		'42e36df6-0c74-40b1-ab05-c49d3514b4ef': {
			'3': {
				'Amount': 0,
				'Multiplier': 30
			},
			'4': {
				'Amount': 0,
				'Multiplier': 100
			},
			'5': {
				'Amount': 0,
				'Multiplier': 200
			}
		},
		'4eb13a7a-9765-4e8c-8c36-403e43cbe47c': {
			'3': {
				'Amount': 0,
				'Multiplier': 4
			},
			'4': {
				'Amount': 0,
				'Multiplier': 10
			},
			'5': {
				'Amount': 0,
				'Multiplier': 50
			}
		},
		'56de42d7-1bec-413b-9355-992b88970644': {
			'3': {
				'Amount': 0,
				'Multiplier': 5
			},
			'4': {
				'Amount': 0,
				'Multiplier': 15
			},
			'5': {
				'Amount': 0,
				'Multiplier': 60
			}
		},
		'731b08d4-c19e-4081-ad3a-3dc83a8887c2': {
			'3': {
				'Amount': 0,
				'Multiplier': 8
			},
			'4': {
				'Amount': 0,
				'Multiplier': 30
			},
			'5': {
				'Amount': 0,
				'Multiplier': 80
			}
		},
		'78262d74-d750-4116-aa41-15a3b0154011': {
			'3': {
				'Amount': 0,
				'Multiplier': 15
			},
			'4': {
				'Amount': 0,
				'Multiplier': 50
			},
			'5': {
				'Amount': 0,
				'Multiplier': 120
			}
		},
		'8d08ecf9-841f-48c0-ad29-da3098ba1f91': {
			'3': {
				'Amount': 0,
				'Multiplier': 50
			},
			'4': {
				'Amount': 0,
				'Multiplier': 200
			},
			'5': {
				'Amount': 0,
				'Multiplier': 800
			}
		},
		'903f1f1a-3c0f-49c4-8782-ed133e4acee1': {},
		'b5a812b1-1c21-4b85-a172-530f35f991ef': {
			'3': {
				'Amount': 0,
				'Multiplier': 5
			},
			'4': {
				'Amount': 0,
				'Multiplier': 15
			},
			'5': {
				'Amount': 0,
				'Multiplier': 60
			}
		},
		'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9': {
			'3': {
				'Amount': 0,
				'Multiplier': 20
			},
			'4': {
				'Amount': 0,
				'Multiplier': 75
			},
			'5': {
				'Amount': 0,
				'Multiplier': 150
			}
		},
		'bc53bbeb-976d-407a-a07c-d31ad721e174': {
			'3': {
				'Amount': 0,
				'Multiplier': 4
			},
			'4': {
				'Amount': 0,
				'Multiplier': 10
			},
			'5': {
				'Amount': 0,
				'Multiplier': 50
			}
		},
		'f5ee797d-e0ac-416f-bf07-c3da32be14ae': {
			'3': {
				'Amount': 0,
				'Multiplier': 40
			},
			'4': {
				'Amount': 0,
				'Multiplier': 150
			},
			'5': {
				'Amount': 0,
				'Multiplier': 300
			}
		}
	},
	'reelConfigSet': [{
		'NumberOfReels': 5,
		'Reel1': {
			'NumberOfRows': 82,
			'SymbolDistribution': [{
				'RowNumber': 0,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 1,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 2,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 3,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 4,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 5,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 6,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 7,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 8,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 9,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 10,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 11,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 12,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 13,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 14,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 15,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 16,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 17,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 18,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 19,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 20,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 21,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 22,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 23,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 24,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 25,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 26,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 27,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 28,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 29,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 30,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 31,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 32,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 33,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 34,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 35,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 36,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 37,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 38,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 39,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 40,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 41,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 42,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 43,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 44,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 45,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 46,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 47,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 48,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 49,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 50,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 51,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 52,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 53,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 54,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 55,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 56,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 57,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 58,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 59,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 60,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 61,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 62,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 63,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 64,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 65,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 66,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 67,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 68,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 69,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 70,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 71,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 72,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 73,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 74,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 75,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 76,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 77,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 78,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 79,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 80,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 81,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}]
		},
		'Reel2': {
			'NumberOfRows': 82,
			'SymbolDistribution': [{
				'RowNumber': 0,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 1,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 2,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 3,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 4,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 5,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 6,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 7,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 8,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 9,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 10,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 11,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 12,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 13,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 14,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 15,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 16,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 17,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 18,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 19,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 20,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 21,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 22,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 23,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 24,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 25,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 26,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 27,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 28,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 29,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 30,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 31,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 32,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 33,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 34,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 35,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 36,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 37,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 38,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 39,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 40,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 41,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 42,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 43,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 44,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 45,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 46,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 47,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 48,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 49,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 50,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 51,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 52,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 53,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 54,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 55,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 56,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 57,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 58,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 59,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 60,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 61,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 62,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 63,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 64,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 65,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 66,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 67,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 68,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 69,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 70,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 71,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 72,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 73,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 74,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 75,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 76,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 77,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 78,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 79,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 80,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 81,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}]
		},
		'Reel3': {
			'NumberOfRows': 82,
			'SymbolDistribution': [{
				'RowNumber': 0,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 1,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 2,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 3,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 4,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 5,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 6,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 7,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 8,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 9,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 10,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 11,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 12,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 13,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 14,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 15,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 16,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 17,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 18,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 19,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 20,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 21,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 22,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 23,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 24,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 25,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 26,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 27,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 28,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 29,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 30,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 31,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 32,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 33,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 34,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 35,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 36,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 37,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 38,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 39,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 40,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 41,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 42,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 43,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 44,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 45,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 46,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 47,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 48,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 49,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 50,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 51,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 52,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 53,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 54,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 55,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 56,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 57,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 58,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 59,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 60,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 61,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 62,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 63,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 64,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 65,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 66,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 67,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 68,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 69,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 70,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 71,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 72,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 73,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 74,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 75,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 76,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 77,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 78,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 79,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 80,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 81,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}]
		},
		'Reel4': {
			'NumberOfRows': 82,
			'SymbolDistribution': [{
				'RowNumber': 0,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 1,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 2,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 3,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 4,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 5,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 6,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 7,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 8,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 9,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 10,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 11,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 12,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 13,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 14,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 15,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 16,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 17,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 18,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 19,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 20,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 21,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 22,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 23,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 24,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 25,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 26,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 27,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 28,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 29,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 30,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 31,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 32,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 33,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 34,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 35,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 36,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 37,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 38,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 39,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 40,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 41,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 42,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 43,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 44,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 45,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 46,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 47,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 48,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 49,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 50,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 51,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 52,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 53,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 54,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 55,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 56,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 57,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 58,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 59,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 60,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 61,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 62,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 63,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 64,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 65,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 66,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 67,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 68,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 69,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 70,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 71,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 72,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 73,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 74,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 75,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 76,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 77,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 78,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 79,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 80,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 81,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}]
		},
		'Reel5': {
			'NumberOfRows': 82,
			'SymbolDistribution': [{
				'RowNumber': 0,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 1,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 2,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 3,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 4,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 5,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 6,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 7,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 8,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 9,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 10,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 11,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 12,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 13,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 14,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 15,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 16,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 17,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 18,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 19,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 20,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 21,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 22,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 23,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 24,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 25,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 26,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 27,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 28,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 29,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 30,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 31,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 32,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 33,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 34,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 35,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 36,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 37,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 38,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 39,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 40,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 41,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 42,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 43,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 44,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 45,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 46,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 47,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 48,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 49,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 50,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 51,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 52,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 53,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 54,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 55,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 56,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 57,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 58,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 59,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 60,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 61,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 62,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 63,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 64,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 65,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 66,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 67,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 68,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 69,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 70,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 71,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 72,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 73,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 74,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 75,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 76,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 77,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 78,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 79,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 80,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 81,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}]
		}
	}, {
		'NumberOfReels': 5,
		'Reel1': {
			'NumberOfRows': 82,
			'SymbolDistribution': [{
				'RowNumber': 0,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 1,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 2,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 3,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 4,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 5,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 6,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 7,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 8,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 9,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 10,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 11,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 12,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 13,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 14,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 15,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 16,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 17,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 18,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 19,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 20,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 21,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 22,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 23,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 24,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 25,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 26,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 27,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 28,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 29,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 30,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 31,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 32,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 33,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 34,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 35,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 36,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 37,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 38,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 39,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 40,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 41,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 42,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 43,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 44,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 45,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 46,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 47,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 48,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 49,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 50,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 51,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 52,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 53,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 54,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 55,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 56,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 57,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 58,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 59,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 60,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 61,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 62,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 63,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 64,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 65,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 66,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 67,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 68,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 69,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 70,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 71,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 72,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 73,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 74,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 75,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 76,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 77,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 78,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 79,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 80,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 81,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}]
		},
		'Reel2': {
			'NumberOfRows': 82,
			'SymbolDistribution': [{
				'RowNumber': 0,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 1,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 2,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 3,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 4,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 5,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 6,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 7,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 8,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 9,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 10,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 11,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 12,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 13,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 14,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 15,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 16,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 17,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 18,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 19,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 20,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 21,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 22,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 23,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 24,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 25,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 26,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 27,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 28,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 29,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 30,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 31,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 32,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 33,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 34,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 35,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 36,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 37,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 38,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 39,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 40,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 41,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 42,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 43,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 44,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 45,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 46,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 47,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 48,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 49,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 50,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 51,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 52,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 53,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 54,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 55,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 56,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 57,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 58,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 59,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 60,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 61,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 62,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 63,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 64,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 65,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 66,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 67,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 68,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 69,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 70,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 71,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 72,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 73,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 74,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 75,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 76,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 77,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 78,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 79,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 80,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 81,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}]
		},
		'Reel3': {
			'NumberOfRows': 82,
			'SymbolDistribution': [{
				'RowNumber': 0,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 1,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 2,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 3,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 4,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 5,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 6,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 7,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 8,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 9,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 10,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 11,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 12,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 13,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 14,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 15,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 16,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 17,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 18,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 19,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 20,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 21,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 22,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 23,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 24,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 25,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 26,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 27,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 28,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 29,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 30,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 31,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 32,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 33,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 34,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 35,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 36,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 37,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 38,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 39,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 40,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 41,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 42,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 43,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 44,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 45,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 46,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 47,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 48,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 49,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 50,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 51,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 52,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 53,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 54,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 55,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 56,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 57,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 58,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 59,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 60,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 61,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 62,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 63,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 64,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 65,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 66,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 67,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 68,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 69,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 70,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 71,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 72,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 73,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 74,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 75,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 76,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 77,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 78,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 79,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 80,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 81,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}]
		},
		'Reel4': {
			'NumberOfRows': 82,
			'SymbolDistribution': [{
				'RowNumber': 0,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 1,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 2,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 3,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 4,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 5,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 6,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 7,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 8,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 9,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 10,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 11,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 12,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 13,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 14,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 15,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 16,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 17,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 18,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 19,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 20,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 21,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 22,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 23,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 24,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 25,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 26,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 27,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 28,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 29,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 30,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 31,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 32,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 33,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 34,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 35,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 36,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 37,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 38,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 39,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 40,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 41,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 42,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 43,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 44,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 45,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 46,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 47,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 48,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 49,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 50,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 51,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 52,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 53,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 54,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 55,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 56,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 57,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 58,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 59,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 60,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 61,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 62,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 63,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 64,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 65,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 66,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 67,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 68,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 69,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 70,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 71,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 72,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 73,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 74,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 75,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 76,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 77,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 78,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 79,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 80,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 81,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}]
		},
		'Reel5': {
			'NumberOfRows': 82,
			'SymbolDistribution': [{
				'RowNumber': 0,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 1,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 2,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 3,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 4,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 5,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 6,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 7,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 8,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 9,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 10,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 11,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 12,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 13,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 14,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 15,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 16,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 17,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 18,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 19,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 20,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 21,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 22,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 23,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 24,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 25,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 26,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 27,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 28,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 29,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 30,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 31,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 32,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 33,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 34,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 35,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 36,
				'SymbolId': '8d08ecf9-841f-48c0-ad29-da3098ba1f91',
				'SymbolName': 'Wild',
				'SymbolType': 'Wild',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild'
			}, {
				'RowNumber': 37,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 38,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 39,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 40,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 41,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 42,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 43,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 44,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 45,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 46,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 47,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 48,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 49,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 50,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 51,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 52,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 53,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 54,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 55,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 56,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 57,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 58,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 59,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 60,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 61,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 62,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 63,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 64,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 65,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 66,
				'SymbolId': '56de42d7-1bec-413b-9355-992b88970644',
				'SymbolName': 'Low1',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
			}, {
				'RowNumber': 67,
				'SymbolId': '42e36df6-0c74-40b1-ab05-c49d3514b4ef',
				'SymbolName': 'Hi2',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
			}, {
				'RowNumber': 68,
				'SymbolId': '903f1f1a-3c0f-49c4-8782-ed133e4acee1',
				'SymbolName': 'FreeSpins',
				'SymbolType': 'Scatter',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
			}, {
				'RowNumber': 69,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 70,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 71,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 72,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}, {
				'RowNumber': 73,
				'SymbolId': '78262d74-d750-4116-aa41-15a3b0154011',
				'SymbolName': 'Hi4',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
			}, {
				'RowNumber': 74,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 75,
				'SymbolId': '731b08d4-c19e-4081-ad3a-3dc83a8887c2',
				'SymbolName': 'Hi6',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
			}, {
				'RowNumber': 76,
				'SymbolId': 'bc53bbeb-976d-407a-a07c-d31ad721e174',
				'SymbolName': 'Low3',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
			}, {
				'RowNumber': 77,
				'SymbolId': 'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9',
				'SymbolName': 'Hi3',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
			}, {
				'RowNumber': 78,
				'SymbolId': '1ed95855-19f2-4bab-a37d-a8b5ddb88e81',
				'SymbolName': 'Hi5',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
			}, {
				'RowNumber': 79,
				'SymbolId': 'b5a812b1-1c21-4b85-a172-530f35f991ef',
				'SymbolName': 'Low2',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
			}, {
				'RowNumber': 80,
				'SymbolId': 'f5ee797d-e0ac-416f-bf07-c3da32be14ae',
				'SymbolName': 'Hi1',
				'SymbolType': 'High Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
			}, {
				'RowNumber': 81,
				'SymbolId': '4eb13a7a-9765-4e8c-8c36-403e43cbe47c',
				'SymbolName': 'Low4',
				'SymbolType': 'Low Paying',
				'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
			}]
		}
	}],
	'selectablePaylines': {
		'40': ['827bbd13-fb61-4ff4-9e54-7ce4503fe0a4', '2535c660-4536-4ef7-aeb5-fed87bdca374', 'c4644256-74c4-46f5-b4b7-8761ffebf945', 'bcb20a5d-d832-411f-af38-4cfd59c21125', '52af41f7-299f-469f-811c-baafb7cd3ce4', '09daf507-fb02-4991-a817-1e074a148373', '7020059d-492c-4d16-ae23-e64b8b95b483', '9a7d2080-01fd-4be4-9f92-59e62ce76023', 'cbdc55af-9755-4693-9362-17a462fa29d6', 'c94e0dcd-1ca7-4fca-a209-3c9896b83c3c', '8e9df29f-0c42-42ad-b283-f466ecb7de0e', '2af36175-d939-4299-98b3-602cf20f215e', '150e5282-c17c-4877-914c-76be150ef77d', '631cde62-6f97-4f21-8f25-f2a0124f8ab9', '44b67650-2572-419b-971f-574d052dd3cd', 'aba1765e-664c-4c48-8ded-9308dfb96be1', 'd98c00c2-12cc-4959-b4bb-1ec1fd1b60fd', '4f7e9886-1bf1-44e8-abb9-45379ff28e27', '27f0e358-32ab-4ab9-9a53-1b702077b687', 'cd9e7c52-389c-490f-af65-be3f2df70d5a', '77ebf8c4-76af-4e40-9f00-35b574ecd8fe', 'ce1007c6-a480-45af-9c27-6d51e4adeb68', '9afd2ad3-f1f5-4a6d-b1e2-2cfe2bfb6172', '0582086d-bfc1-4ee1-bde1-522ae53a9b06', '2482f844-ef8a-422d-86d6-8c581b24bbb5', 'f2b45047-6e07-423c-803c-77c3966dee29', '17da128a-e282-4ba3-b94d-dfafe4fc3c03', '364d22e9-39fe-4e6b-a5e6-833152faa30d', '95f5c8fa-3669-4e0a-a84d-69d58523e5a6', 'aa1b058f-2c80-4c11-b452-13c098965809', '0a1e990f-39d8-4222-80dc-6183f8ec8032', '9c9c81eb-1a51-4f6c-b1de-35817508a4d7', '7d321d9f-9387-4e9d-953a-eb4b95434396', 'fef4a6e7-c754-423c-90e5-e48ad40af836', 'b318b438-3152-4b04-b9bb-0208e543743e', 'b78c43f6-7ac6-4162-8e94-90140637cd41', 'b6e6b104-8d84-40d2-a639-1d401589c077', 'a6a8b0bc-0a7b-4dd4-9521-b81b53afcc00', '7421c2bc-16e6-4b0c-ad90-fe742c91e024', '68e4723a-aa38-41d2-b2e4-65947f0611e8'],
		'options': ['40']
	},
	'specialFeature': {
		'reelSwitching': {
			'freeGame': ['0', '0'],
			'mainGame': ['1', '0', '1', '1', '1', '1', '1', '1', '1', '0', '1', '0', '1', '1', '0', '1', '0', '1', '0', '1', '1', '1', '1', '0', '1']
		}
	},
	'symbols': {
		'1ed95855-19f2-4bab-a37d-a8b5ddb88e81': {
			'SymbolName': 'Hi5',
			'SymbolNumber': 6,
			'SymbolType': 'High Paying',
			'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi5'
		},
		'42e36df6-0c74-40b1-ab05-c49d3514b4ef': {
			'SymbolName': 'Hi2',
			'SymbolNumber': 3,
			'SymbolType': 'High Paying',
			'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi2'
		},
		'4eb13a7a-9765-4e8c-8c36-403e43cbe47c': {
			'SymbolName': 'Low4',
			'SymbolNumber': 11,
			'SymbolType': 'Low Paying',
			'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low4'
		},
		'56de42d7-1bec-413b-9355-992b88970644': {
			'SymbolName': 'Low1',
			'SymbolNumber': 8,
			'SymbolType': 'Low Paying',
			'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low1'
		},
		'731b08d4-c19e-4081-ad3a-3dc83a8887c2': {
			'SymbolName': 'Hi6',
			'SymbolNumber': 7,
			'SymbolType': 'High Paying',
			'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi6'
		},
		'78262d74-d750-4116-aa41-15a3b0154011': {
			'SymbolName': 'Hi4',
			'SymbolNumber': 5,
			'SymbolType': 'High Paying',
			'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi4'
		},
		'8d08ecf9-841f-48c0-ad29-da3098ba1f91': {
			'SymbolName': 'Wild',
			'SymbolNumber': 1,
			'SymbolType': 'Wild',
			'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/wild',
			'doesWildPay': true,
			'isExpandingWild': true
		},
		'903f1f1a-3c0f-49c4-8782-ed133e4acee1': {
			'SymbolName': 'FreeSpins',
			'SymbolNumber': 12,
			'SymbolType': 'Scatter',
			'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/freespins'
		},
		'b5a812b1-1c21-4b85-a172-530f35f991ef': {
			'SymbolName': 'Low2',
			'SymbolNumber': 9,
			'SymbolType': 'Low Paying',
			'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low2'
		},
		'bbcfd4cd-90da-4f24-a8e1-89c84c85cde9': {
			'SymbolName': 'Hi3',
			'SymbolNumber': 4,
			'SymbolType': 'High Paying',
			'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi3'
		},
		'bc53bbeb-976d-407a-a07c-d31ad721e174': {
			'SymbolName': 'Low3',
			'SymbolNumber': 10,
			'SymbolType': 'Low Paying',
			'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/low3'
		},
		'f5ee797d-e0ac-416f-bf07-c3da32be14ae': {
			'SymbolName': 'Hi1',
			'SymbolNumber': 2,
			'SymbolType': 'High Paying',
			'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d3401dc4-8a61-43b0-9764-50c8c7daef3d/hi1'
		}
	},
	'viewZone': 35
},
	'isDeleted': false,
	'status': 'start',
	'updatedAt': 1552365159934,
	'version': 3,
	'versionCode': '331231',
	'versionName': 'v3'
},

    VALID_GAME_ID: 'd3401dc4-8a61-43b0-9764-50c8c7daef3d',
    VALID_USER_TOKEN: '59b8e8d3502b2f25995438f5',
    INVALID_USER_TOKEN: '97b2ghkskrfa',
    VALID_USER_DETAILS: { 'user': { 'birthday': '1-1-1970', 'gender': 'na', 'id': '597b1378502b2f09f265a1f0', 'level': 36, 'levelProgress': 0.23, 'locale': 'de_DE', 'multiplier': 1, 'name': 'rinkal', 'wallet': { 'chips': 11183327193099 } }, 'game': { 'settings': { 'bets': [120, 200, 400, 800, 1600, 2400, 3200, 4800, 6400, 8000, 10000, 12000, 16000, 24000, 28000, 32000, 60000, 80000, 100000, 170000] } } },
    VALID_CLIENT_ID: 'k7uACEZBTfiJ8cIqb3LIfKNYPhRdWZ4i',
    RANDOM_NUMBER_REELCONFIG1: 0,
    RANDOM_NUMBER_REELCONFIG2: 0,
    INVALID_CLIENT_ID: 'rtwafmeNKndflwnGI',
    DUMMY_SPECIAL_FEATURE: { 'reelSwitching': { 'freeGame': ['0', '1', '2'], 'mainGame': ['0', '1', '2'] } },
    WILD_REELS_1: { rng: [13, 13, 13, 13, 13], wins: 40 },
    WILD_REELS_2: [12, 13, 2, 15, 15],
    WILD_REELS_3: [19, 16, 13, 15, 15],
    WILD_REELS_4: [18, 17, 20, 13, 15],
    WILD_REELS_5: [18, 1, 2, 15, 13],
    WILD_SYMBOL_ID: '8d08ecf9-841f-48c0-ad29-da3098ba1f91'
};
