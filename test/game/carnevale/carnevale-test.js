const chai = require('chai');
const sinon = require('sinon');
const carnevaleData = require('./test-data');
const carnevale = require('../../../util/games/carnevale');
const gameSession = require('../../../util/gamesession');
const rng = require('../../../util/rng');
const expect = chai.expect;
const _ = require('lodash');
const nj = require('numjs');

describe('Carnevale Test', () => {

  beforeEach(() => {
    /**
     * it is used to mock gameSession method
     */
    sinon.stub(gameSession, 'getUserSession').callsFake((token, gameId) => {
      if (gameId === carnevaleData.VALID_GAME_ID && token === carnevaleData.VALID_USER_TOKEN) {
        return Promise.resolve(carnevaleData.VALID_USER_SESSION);
      }
      return Promise.reject();
    });


    /**
     * generateRandomNumber method is used to get mock data for random number
     * @type {[type]}
     */
    sinon.stub(rng, 'generateRandomNumber').callsFake(game => carnevaleData.RANDOM_NUMBERS[Math.floor(Math.random() * carnevaleData.RANDOM_NUMBERS.length)]);


    sinon.stub(carnevale, 'getRandomProbability').callsFake((game, gameType) => Math.floor(Math.random() * 2));
  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    gameSession.getUserSession.restore();
    rng.generateRandomNumber.restore();
    carnevale.getRandomProbability.restore();
  });


  it('Get reel probability from specialFeature for free game', (done) => {
    const gameData = JSON.parse(JSON.stringify({ default: carnevaleData.VALID_GAME_DATA }));
    gameData.default.gameConfig.specialFeature.reelSwitching = carnevaleData.DUMMY_SPECIAL_FEATURE;
    const probability = parseInt(carnevale.getRandomProbability(gameData, 'freeGame'));
    expect(probability).to.be.eql(0);
    done();
  });


  it('Get reel probability from specialFeature for main game', (done) => {
    const gameData = JSON.parse(JSON.stringify({ default: carnevaleData.VALID_GAME_DATA }));
    gameData.default.gameConfig.specialFeature.reelSwitching = carnevaleData.DUMMY_SPECIAL_FEATURE;
    const probability = parseInt(carnevale.getRandomProbability(gameData, 'mainGame'));
    expect(probability).to.be.within(0, gameData.default.gameConfig.reelConfigSet.length);
    done();
  });


  it('probability of random reel must be in reelConfigSet array index ', (done) => {
    const gameData = JSON.parse(JSON.stringify({ default: carnevaleData.VALID_GAME_DATA }));
    gameData.default.gameConfig.specialFeature.reelSwitching = carnevaleData.DUMMY_SPECIAL_FEATURE;
    const probability = parseInt(carnevale.getRandomProbability(gameData, 'mainGame'));
    expect(probability).to.be.within(0, gameData.default.gameConfig.reelConfigSet.length);
    done();
  });


  it('Get reel configuration from  main game by using specialFeature mainGame probability', (done) => {
    const gameData = JSON.parse(JSON.stringify({ default: carnevaleData.VALID_GAME_DATA }));
    gameData.default.gameConfig.specialFeature.reelSwitching = carnevaleData.DUMMY_SPECIAL_FEATURE;
    const gameConfig = carnevale.getReelConfiguration(gameData, carnevaleData.VALID_USER_SESSION);
      expect(gameConfig.reelConfig).to.have.a.property('NumberOfReels');
    done();
  });


  it('Get reel configuration from  free game by using specialFeature freeGame probability', (done) => {
    const gameData = JSON.parse(JSON.stringify({ default: carnevaleData.VALID_GAME_DATA }));
    gameData.default.gameConfig.specialFeature.reelSwitching = carnevaleData.DUMMY_SPECIAL_FEATURE;
    const gameConfig = carnevale.getReelConfiguration(gameData, carnevaleData.VALID_USER_SESSION_FREE_GAME);
      expect(gameConfig.reelConfig).to.have.a.property('NumberOfReels');
    done();
  });


  it('get check that wild multiplier is picked valid or not in free game', (done) => {
    const gameData = { default: carnevaleData.VALID_GAME_DATA };
    gameData.default.gameConfig.reelConfig = JSON.parse(JSON.stringify(gameData.default.gameConfig.freeSpin.reelConfigSet[0]));
    const noofReels = gameData.default.gameConfig.reelConfigSet[1].NumberOfReels;
    const columnSize = parseInt(gameData.default.gameConfig.viewZone.toString().charAt(0));
    const rand = rng.generateRandomNumber(gameData);
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const scatter = [];
    const wildOfKind = [];
    carnevale.generateViewZone(rand, noofReels, columnSize, scatter, gameData, arrays, carnevaleData.VALID_USER_SESSION_FREE_GAME, wildOfKind);
    if (wildOfKind.length > 0) {
      wildOfKind.forEach((wild) => {
        expect(wild.multiplier).to.be.oneOf([2, 5, 10, 100]);
      });
    }
    done();
  });


  it('get check that wild multiplier is picked valid or not in main game', (done) => {
    const gameData = { default: carnevaleData.VALID_GAME_DATA };
    gameData.default.gameConfig.reelConfig = JSON.parse(JSON.stringify(gameData.default.gameConfig.reelConfigSet[0]));
    const noofReels = gameData.default.gameConfig.reelConfigSet[1].NumberOfReels;
    const columnSize = parseInt(gameData.default.gameConfig.viewZone.toString().charAt(0));
    const rand = rng.generateRandomNumber(gameData);
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const scatter = [];
    const wildOfKind = [];
    carnevale.generateViewZone(rand, noofReels, columnSize, scatter, gameData, arrays, carnevaleData.VALID_USER_SESSION, wildOfKind);
    if (wildOfKind.length > 0) {
      wildOfKind.forEach((wild) => {
        expect(wild.multiplier).to.be.oneOf([2, 5, 10, 50]);
      });
    }
    done();
  });


  it('should check that total wild multiplier should be sum of wild multiplier on different reel on same payline', (done) => {
    const gameData = { default: carnevaleData.VALID_GAME_DATA };
    carnevale.compute(gameData, carnevaleData.VALID_GAME_ID, carnevaleData.VALID_USER_TOKEN, carnevaleData.VALID_USER_SESSION).then((data) => {
      if (data.results.length > 0) {
        data.results.forEach((resultObj) => {
          if (resultObj.totalWildMultiplier > 0) {
            let wildMultiplierInOnePayline = 0;
            Object.keys(resultObj.wildMultiplierPosition).forEach((reel) => {
              wildMultiplierInOnePayline += resultObj.wildMultiplierPosition[reel].multiplier;
            });
            expect(resultObj.totalWildMultiplier).to.be.eql(wildMultiplierInOnePayline);
          }
        });
      }
    });
    done();
  });
});


describe('Carnevale Test for Computing win', () => {
  beforeEach(() => {
    /**
     * it is used to mock gameSession method
     */
    sinon.stub(gameSession, 'getUserSession').callsFake((token, gameId) => {
      if (gameId === carnevaleData.VALID_GAME_ID && token === carnevaleData.VALID_USER_TOKEN) {
        return Promise.resolve(carnevaleData.VALID_USER_SESSION);
      }
      return Promise.reject();
    });


    /**
     * generateRandomNumber method is used to get mock data for random number
     * @type {[type]}
     */
    sinon.stub(rng, 'generateRandomNumber').callsFake(game => carnevaleData.RANDOM_NUMBERS[Math.floor(Math.random() * carnevaleData.RANDOM_NUMBERS.length)]);


    sinon.stub(carnevale, 'getRandomProbability').callsFake((game, gameType) => Math.floor(Math.random() * 2));


    sinon.stub(carnevale, 'getWildMultiplier').callsFake((session, specialFeature, line) => carnevaleData.WILD_MULTIPLIER2[Math.floor(Math.random() * 2)]);
  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    gameSession.getUserSession.restore();
    carnevale.getWildMultiplier.restore();
    rng.generateRandomNumber.restore();
    carnevale.getRandomProbability.restore();
  });


  it('should check that win amount is multiply according to totalWildMultiplier if it exist', (done) => {
    const gameData = { default: carnevaleData.VALID_GAME_DATA };
    carnevale.compute(gameData, carnevaleData.VALID_GAME_ID, carnevaleData.VALID_USER_TOKEN, carnevaleData.VALID_USER_SESSION).then((data) => {
      if (data.results.length > 0) {
        data.results.forEach((resultObj) => {
          if (resultObj.totalWildMultiplier > 0) {
            let wildMultiplierInOnePayline = 0;
            Object.keys(resultObj.wildMultiplierPosition).forEach((reel) => {
              wildMultiplierInOnePayline += resultObj.wildMultiplierPosition[reel].multiplier;
            });
            expect((resultObj.totalWildMultiplier || 1) * resultObj.multiplier * carnevaleData.VALID_USER_SESSION.eventData.currentBet).to.be.eql((wildMultiplierInOnePayline || 1) * carnevaleData.VALID_USER_SESSION.eventData.currentBet * resultObj.multiplier);
          }
        });
      }
    });
    done();
  });
});
