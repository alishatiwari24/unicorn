module.exports = {
    VALID_USER_SESSION: JSON.parse('{"_type":"userSession","_updatedTimestamp":"2019-03-01T07:38:39.847Z","clientID":"xQEPNoytcC1Ud1330i4vdeIVAepS9t2r","event":"spin","eventData":{"currentBet":50,"currentLines":40,"freeSpinData":{},"userDetails":{"id":"59aebc7bf3869b5be10ec66d","name":"BillyRacoon","gender":"na","wallet":{"chips":843012193},"locale":"en_GB","birthday":"01-01-1970","level":25,"levelProgress":0.93,"multiplier":1}},"game":{"settings":{"bets":[50,150,250,500,750,1000,1250,1500,1750,2000,2250,2500]},"betAmount":2000 }}'),

    VALID_USER_SESSION_FREE_GAME: JSON.parse('{"_type":"userSession","_updatedTimestamp":"2019-03-04T08:55:32.503Z","clientID":"3zq6HW3xcA7rMPzRFz4phak5a9iEW92E","event":"spin","eventData":{"currentBet":50,"currentLines":40,"freeSpinData":{"freeSpins":10,"currentFreeSpin":0,"winAmount":0},"userDetails":{"id":"59aebc7bf3869b5be10ec66d","name":"BillyRacoon","gender":"na","wallet":{"chips":843010193},"locale":"en_GB","birthday":"01-01-1970","level":25,"levelProgress":0.93,"multiplier":1}},"game":{"settings":{"bets":[50,150,250,500,750,1000,1250,1500,1750,2000,2250,2500]},"betAmount":2000}}'),

    VALID_VERSION_ID: 'badf3700-1bd0-46e2-b49d-e0727a8bd220',

    VALID_GAME_DATA: {
			'_type': 'gameConfig',
			'baseGameId': 'd94ad420-486a-43ff-8284-a8791a6b6d5c',
			'createdAt': 1553684585514,
			'environment': 'completed',
			'gameConfig': {
				'bigWins': {
					'bigWin': {
						'maxBetMultiplier': 20,
						'minBetMultiplier': 10
					},
					'legendaryWin': {
						'maxBetMultiplier': null,
						'minBetMultiplier': 50
					},
					'monsterWin': {
						'maxBetMultiplier': 50,
						'minBetMultiplier': 20
					}
				},
				'freeSpin': {
					'SymbolName': 'Bonus',
					'SymbolType': 'Scatter',
					'freeSpinWins': {
						'3': {
							'noofFS': 10,
							'winMultiplier': 0
						},
						'4': {
							'noofFS': 15,
							'winMultiplier': 0
						},
						'5': {
							'noofFS': 25,
							'winMultiplier': 0
						}
					},
					'isFSReelDiff': true,
					'keys': ['3', '4', '5'],
					'minimumRequiredCount': 3,
					'reelConfigSet': [{
						'NumberOfReels': 5,
						'Reel1': {
							'NumberOfRows': 64,
							'SymbolDistribution': [{
								'RowNumber': 0,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 1,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 2,
								'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
								'SymbolName': 'Bonus',
								'SymbolType': 'Scatter',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
							}, {
								'RowNumber': 3,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 4,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 5,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 6,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 7,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 8,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 9,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 10,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 11,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 12,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 13,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 14,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 15,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 16,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 17,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 18,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 19,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 20,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 21,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 22,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 23,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 24,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 25,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 26,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 27,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 28,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 29,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 30,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 31,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 32,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 33,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 34,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 35,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 36,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 37,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 38,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 39,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 40,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 41,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 42,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 43,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 44,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 45,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 46,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 47,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 48,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 49,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 50,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 51,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 52,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 53,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 54,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 55,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 56,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 57,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 58,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 59,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 60,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 61,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 62,
								'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
								'SymbolName': 'Bonus',
								'SymbolType': 'Scatter',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
							}, {
								'RowNumber': 63,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}]
						},
						'Reel2': {
							'NumberOfRows': 64,
							'SymbolDistribution': [{
								'RowNumber': 0,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 1,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 2,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 3,
								'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
								'SymbolName': 'Bonus',
								'SymbolType': 'Scatter',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
							}, {
								'RowNumber': 4,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 5,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 6,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 7,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 8,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 9,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 10,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 11,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 12,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 13,
								'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
								'SymbolName': 'Wild',
								'SymbolType': 'Wild',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
							}, {
								'RowNumber': 14,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 15,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 16,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 17,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 18,
								'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
								'SymbolName': 'Bonus',
								'SymbolType': 'Scatter',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
							}, {
								'RowNumber': 19,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 20,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 21,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 22,
								'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
								'SymbolName': 'Wild',
								'SymbolType': 'Wild',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
							}, {
								'RowNumber': 23,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 24,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 25,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 26,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 27,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 28,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 29,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 30,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 31,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 32,
								'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
								'SymbolName': 'Wild',
								'SymbolType': 'Wild',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
							}, {
								'RowNumber': 33,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 34,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 35,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 36,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 37,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 38,
								'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
								'SymbolName': 'Bonus',
								'SymbolType': 'Scatter',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
							}, {
								'RowNumber': 39,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 40,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 41,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 42,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 43,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 44,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 45,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 46,
								'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
								'SymbolName': 'Wild',
								'SymbolType': 'Wild',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
							}, {
								'RowNumber': 47,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 48,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 49,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 50,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 51,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 52,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 53,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 54,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 55,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 56,
								'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
								'SymbolName': 'Wild',
								'SymbolType': 'Wild',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
							}, {
								'RowNumber': 57,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 58,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 59,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 60,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 61,
								'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
								'SymbolName': 'Wild',
								'SymbolType': 'Wild',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
							}, {
								'RowNumber': 62,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 63,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}]
						},
						'Reel3': {
							'NumberOfRows': 64,
							'SymbolDistribution': [{
								'RowNumber': 0,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 1,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 2,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 3,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 4,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 5,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 6,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 7,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 8,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 9,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 10,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 11,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 12,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 13,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 14,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 15,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 16,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 17,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 18,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 19,
								'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
								'SymbolName': 'Bonus',
								'SymbolType': 'Scatter',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
							}, {
								'RowNumber': 20,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 21,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 22,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 23,
								'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
								'SymbolName': 'Wild',
								'SymbolType': 'Wild',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
							}, {
								'RowNumber': 24,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 25,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 26,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 27,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 28,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 29,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 30,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 31,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 32,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 33,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 34,
								'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
								'SymbolName': 'Wild',
								'SymbolType': 'Wild',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
							}, {
								'RowNumber': 35,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 36,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 37,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 38,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 39,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 40,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 41,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 42,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 43,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 44,
								'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
								'SymbolName': 'Wild',
								'SymbolType': 'Wild',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
							}, {
								'RowNumber': 45,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 46,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 47,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 48,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 49,
								'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
								'SymbolName': 'Bonus',
								'SymbolType': 'Scatter',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
							}, {
								'RowNumber': 50,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 51,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 52,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 53,
								'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
								'SymbolName': 'Wild',
								'SymbolType': 'Wild',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
							}, {
								'RowNumber': 54,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 55,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 56,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 57,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 58,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 59,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 60,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 61,
								'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
								'SymbolName': 'Wild',
								'SymbolType': 'Wild',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
							}, {
								'RowNumber': 62,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 63,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}]
						},
						'Reel4': {
							'NumberOfRows': 64,
							'SymbolDistribution': [{
								'RowNumber': 0,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 1,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 2,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 3,
								'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
								'SymbolName': 'Bonus',
								'SymbolType': 'Scatter',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
							}, {
								'RowNumber': 4,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 5,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 6,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 7,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 8,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 9,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 10,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 11,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 12,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 13,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 14,
								'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
								'SymbolName': 'Wild',
								'SymbolType': 'Wild',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
							}, {
								'RowNumber': 15,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 16,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 17,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 18,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 19,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 20,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 21,
								'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
								'SymbolName': 'Wild',
								'SymbolType': 'Wild',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
							}, {
								'RowNumber': 22,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 23,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 24,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 25,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 26,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 27,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 28,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 29,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 30,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 31,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 32,
								'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
								'SymbolName': 'Wild',
								'SymbolType': 'Wild',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
							}, {
								'RowNumber': 33,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 34,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 35,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 36,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 37,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 38,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 39,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 40,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 41,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 42,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 43,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 44,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 45,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 46,
								'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
								'SymbolName': 'Wild',
								'SymbolType': 'Wild',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
							}, {
								'RowNumber': 47,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 48,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 49,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 50,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 51,
								'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
								'SymbolName': 'Bonus',
								'SymbolType': 'Scatter',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
							}, {
								'RowNumber': 52,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 53,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 54,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 55,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 56,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 57,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 58,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 59,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 60,
								'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
								'SymbolName': 'Wild',
								'SymbolType': 'Wild',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
							}, {
								'RowNumber': 61,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 62,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 63,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}]
						},
						'Reel5': {
							'NumberOfRows': 64,
							'SymbolDistribution': [{
								'RowNumber': 0,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 1,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 2,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 3,
								'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
								'SymbolName': 'Bonus',
								'SymbolType': 'Scatter',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
							}, {
								'RowNumber': 4,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 5,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 6,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 7,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 8,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 9,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 10,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 11,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 12,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 13,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 14,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 15,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 16,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 17,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 18,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 19,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 20,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 21,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 22,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 23,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 24,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 25,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 26,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 27,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 28,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 29,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 30,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 31,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 32,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 33,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 34,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 35,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 36,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 37,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 38,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 39,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 40,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 41,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 42,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 43,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 44,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 45,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 46,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 47,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 48,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 49,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 50,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 51,
								'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
								'SymbolName': 'Bonus',
								'SymbolType': 'Scatter',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
							}, {
								'RowNumber': 52,
								'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
								'SymbolName': 'Low1',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
							}, {
								'RowNumber': 53,
								'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
								'SymbolName': 'Hi2',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
							}, {
								'RowNumber': 54,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 55,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}, {
								'RowNumber': 56,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 57,
								'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
								'SymbolName': 'Low3',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
							}, {
								'RowNumber': 58,
								'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
								'SymbolName': 'Hi3',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
							}, {
								'RowNumber': 59,
								'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
								'SymbolName': 'Hi5',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
							}, {
								'RowNumber': 60,
								'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
								'SymbolName': 'Hi1',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
							}, {
								'RowNumber': 61,
								'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
								'SymbolName': 'Low2',
								'SymbolType': 'Low Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
							}, {
								'RowNumber': 62,
								'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
								'SymbolName': 'Hi4',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
							}, {
								'RowNumber': 63,
								'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
								'SymbolName': 'Hi6',
								'SymbolType': 'High Paying',
								'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
							}]
						}
					}],
					'symbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c'
				},
				'payLines': {
					'PayArray': [{
						'_id': 'db32dcf0-e625-4034-98fd-212181868246',
						'five': 1,
						'four': 1,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 1,
						'three': 1,
						'two': 1
					}, {
						'_id': 'bb47a6a3-816f-4f09-9b41-0f5a12f6ee8c',
						'five': 0,
						'four': 0,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 0,
						'three': 0,
						'two': 0
					}, {
						'_id': 'faddebcf-c15b-4332-bb1d-4d8eb27be8c1',
						'five': 2,
						'four': 2,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 2,
						'three': 2,
						'two': 2
					}, {
						'_id': '38c35e4e-33b1-4237-82ed-5a2293d80cd5',
						'five': 0,
						'four': 1,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 0,
						'three': 2,
						'two': 1
					}, {
						'_id': '711a9e1a-e4a4-4cf2-ae0f-30c12e450408',
						'five': 2,
						'four': 1,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 2,
						'three': 0,
						'two': 1
					}, {
						'_id': '2d61b0e0-84bc-402c-9326-c738bebbeb31',
						'five': 1,
						'four': 0,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 1,
						'three': 0,
						'two': 0
					}, {
						'_id': '74fdb327-1ef6-45b9-929d-c1afa067c57d',
						'five': 1,
						'four': 2,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 1,
						'three': 2,
						'two': 2
					}, {
						'_id': '3afe6d5d-70b8-45bf-91d2-9a54e5b956f3',
						'five': 2,
						'four': 2,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 0,
						'three': 1,
						'two': 0
					}, {
						'_id': '8576c23c-c8dc-4f9c-b993-b53bf66efba2',
						'five': 0,
						'four': 0,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 2,
						'three': 1,
						'two': 2
					}, {
						'_id': '4f787027-9f68-4b71-9d69-4d019a0773e2',
						'five': 1,
						'four': 0,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 1,
						'three': 1,
						'two': 2
					}, {
						'_id': '9e9e87b8-0d22-4395-a894-74bcbdc2de79',
						'five': 1,
						'four': 2,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 1,
						'three': 1,
						'two': 0
					}, {
						'_id': '10e214cc-42f5-4461-bfd3-a81081767835',
						'five': 0,
						'four': 1,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 0,
						'three': 1,
						'two': 1
					}, {
						'_id': '4e0c9290-f86a-4550-9aa9-211fd353c441',
						'five': 2,
						'four': 1,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 2,
						'three': 1,
						'two': 1
					}, {
						'_id': '65aff733-0f47-41fe-9f5c-9e645814fbdd',
						'five': 0,
						'four': 1,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 0,
						'three': 0,
						'two': 1
					}, {
						'_id': '57ba9290-60e9-4747-99a5-77e0e4db92d6',
						'five': 2,
						'four': 1,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 2,
						'three': 2,
						'two': 1
					}, {
						'_id': 'b7cc86ba-fecf-417d-82b6-eed72e48137f',
						'five': 1,
						'four': 1,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 1,
						'three': 0,
						'two': 1
					}, {
						'_id': 'f00676a9-3621-4594-9a5d-4d029c0fa45b',
						'five': 1,
						'four': 1,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 1,
						'three': 2,
						'two': 1
					}, {
						'_id': '68d97b61-c722-4de9-9fe3-1108a6a9fe70',
						'five': 0,
						'four': 0,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 0,
						'three': 2,
						'two': 0
					}, {
						'_id': '002a8929-b0e9-4af4-bee2-61d96bac1711',
						'five': 2,
						'four': 2,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 2,
						'three': 0,
						'two': 2
					}, {
						'_id': '9471ce8d-fea8-468c-96d8-b4cb50bdb027',
						'five': 0,
						'four': 2,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 0,
						'three': 2,
						'two': 2
					}, {
						'_id': 'ab8ac917-4c32-411b-910e-13430b8cf8ac',
						'five': 2,
						'four': 0,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 2,
						'three': 0,
						'two': 0
					}, {
						'_id': '40421e71-0f29-4618-8132-2608a7ddf8ce',
						'five': 1,
						'four': 2,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 1,
						'three': 0,
						'two': 2
					}, {
						'_id': '4f7416db-8c91-4a19-9bc8-0743810312e9',
						'five': 1,
						'four': 0,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 1,
						'three': 2,
						'two': 0
					}, {
						'_id': '7db16c74-a968-48f9-b1f9-d4a8a29bcee3',
						'five': 0,
						'four': 2,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 0,
						'three': 0,
						'two': 2
					}, {
						'_id': '67b5fb65-604b-4803-9fb0-25c89a94ab0d',
						'five': 2,
						'four': 0,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 2,
						'three': 2,
						'two': 0
					}, {
						'_id': '646deb91-d1da-4297-b9c5-dbb00378dd94',
						'five': 0,
						'four': 2,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 2,
						'three': 1,
						'two': 0
					}, {
						'_id': '1420763f-ed44-4db5-a753-b346269616de',
						'five': 2,
						'four': 0,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 0,
						'three': 1,
						'two': 2
					}, {
						'_id': '6cafc562-13de-4c30-9b80-74f0b3179f07',
						'five': 0,
						'four': 2,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 0,
						'three': 1,
						'two': 2
					}, {
						'_id': 'd7f8172c-62e4-439d-9c52-1fe1ab2e557d',
						'five': 2,
						'four': 0,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 2,
						'three': 1,
						'two': 0
					}, {
						'_id': 'e7e2636d-d8ec-4171-8239-4d242970073e',
						'five': 1,
						'four': 0,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 2,
						'three': 0,
						'two': 1
					}, {
						'_id': '78ceeb90-719d-4c56-abd3-ff361339de1f',
						'five': 1,
						'four': 2,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 0,
						'three': 2,
						'two': 1
					}, {
						'_id': 'ccdc01c2-f98a-4557-ab41-ab5319f5990b',
						'five': 2,
						'four': 2,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 0,
						'three': 2,
						'two': 0
					}, {
						'_id': '438f9d9f-9f3f-4062-bd47-5f961944922e',
						'five': 0,
						'four': 0,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 2,
						'three': 0,
						'two': 2
					}, {
						'_id': '1cd965ba-9814-40e9-af75-b3b62c9b7856',
						'five': 2,
						'four': 1,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 1,
						'three': 2,
						'two': 0
					}, {
						'_id': '6bcf4d52-2235-48d1-bd4d-0e55d875c90a',
						'five': 0,
						'four': 1,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 1,
						'three': 0,
						'two': 2
					}, {
						'_id': 'f2c0c1f7-1257-4e65-8aa2-9bcf411a0c75',
						'five': 2,
						'four': 1,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 0,
						'three': 0,
						'two': 1
					}, {
						'_id': '64c56879-9c10-45f1-8867-617082f131ea',
						'five': 0,
						'four': 1,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 2,
						'three': 2,
						'two': 1
					}, {
						'_id': '4cbf05a1-e633-461b-9371-a4b2c9737aea',
						'five': 0,
						'four': 0,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 1,
						'three': 2,
						'two': 2
					}, {
						'_id': '6bf23e34-dccc-4776-a126-477cd6fbd902',
						'five': 2,
						'four': 1,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 0,
						'three': 1,
						'two': 0
					}, {
						'_id': '72d6b6a0-ef12-4791-b76e-08d121aceb8f',
						'five': 0,
						'four': 1,
						'keys': ['one', 'two', 'three', 'four', 'five'],
						'one': 2,
						'three': 1,
						'two': 2
					}],
					'normalizedPayArray': [{
						'_id': 'db32dcf0-e625-4034-98fd-212181868246',
						'data': [1, 1, 1, 1, 1]
					}, {
						'_id': 'bb47a6a3-816f-4f09-9b41-0f5a12f6ee8c',
						'data': [0, 0, 0, 0, 0]
					}, {
						'_id': 'faddebcf-c15b-4332-bb1d-4d8eb27be8c1',
						'data': [2, 2, 2, 2, 2]
					}, {
						'_id': '38c35e4e-33b1-4237-82ed-5a2293d80cd5',
						'data': [0, 1, 2, 1, 0]
					}, {
						'_id': '711a9e1a-e4a4-4cf2-ae0f-30c12e450408',
						'data': [2, 1, 0, 1, 2]
					}, {
						'_id': '2d61b0e0-84bc-402c-9326-c738bebbeb31',
						'data': [1, 0, 0, 0, 1]
					}, {
						'_id': '74fdb327-1ef6-45b9-929d-c1afa067c57d',
						'data': [1, 2, 2, 2, 1]
					}, {
						'_id': '3afe6d5d-70b8-45bf-91d2-9a54e5b956f3',
						'data': [0, 0, 1, 2, 2]
					}, {
						'_id': '8576c23c-c8dc-4f9c-b993-b53bf66efba2',
						'data': [2, 2, 1, 0, 0]
					}, {
						'_id': '4f787027-9f68-4b71-9d69-4d019a0773e2',
						'data': [1, 2, 1, 0, 1]
					}, {
						'_id': '9e9e87b8-0d22-4395-a894-74bcbdc2de79',
						'data': [1, 0, 1, 2, 1]
					}, {
						'_id': '10e214cc-42f5-4461-bfd3-a81081767835',
						'data': [0, 1, 1, 1, 0]
					}, {
						'_id': '4e0c9290-f86a-4550-9aa9-211fd353c441',
						'data': [2, 1, 1, 1, 2]
					}, {
						'_id': '65aff733-0f47-41fe-9f5c-9e645814fbdd',
						'data': [0, 1, 0, 1, 0]
					}, {
						'_id': '57ba9290-60e9-4747-99a5-77e0e4db92d6',
						'data': [2, 1, 2, 1, 2]
					}, {
						'_id': 'b7cc86ba-fecf-417d-82b6-eed72e48137f',
						'data': [1, 1, 0, 1, 1]
					}, {
						'_id': 'f00676a9-3621-4594-9a5d-4d029c0fa45b',
						'data': [1, 1, 2, 1, 1]
					}, {
						'_id': '68d97b61-c722-4de9-9fe3-1108a6a9fe70',
						'data': [0, 0, 2, 0, 0]
					}, {
						'_id': '002a8929-b0e9-4af4-bee2-61d96bac1711',
						'data': [2, 2, 0, 2, 2]
					}, {
						'_id': '9471ce8d-fea8-468c-96d8-b4cb50bdb027',
						'data': [0, 2, 2, 2, 0]
					}, {
						'_id': 'ab8ac917-4c32-411b-910e-13430b8cf8ac',
						'data': [2, 0, 0, 0, 2]
					}, {
						'_id': '40421e71-0f29-4618-8132-2608a7ddf8ce',
						'data': [1, 2, 0, 2, 1]
					}, {
						'_id': '4f7416db-8c91-4a19-9bc8-0743810312e9',
						'data': [1, 0, 2, 0, 1]
					}, {
						'_id': '7db16c74-a968-48f9-b1f9-d4a8a29bcee3',
						'data': [0, 2, 0, 2, 0]
					}, {
						'_id': '67b5fb65-604b-4803-9fb0-25c89a94ab0d',
						'data': [2, 0, 2, 0, 2]
					}, {
						'_id': '646deb91-d1da-4297-b9c5-dbb00378dd94',
						'data': [2, 0, 1, 2, 0]
					}, {
						'_id': '1420763f-ed44-4db5-a753-b346269616de',
						'data': [0, 2, 1, 0, 2]
					}, {
						'_id': '6cafc562-13de-4c30-9b80-74f0b3179f07',
						'data': [0, 2, 1, 2, 0]
					}, {
						'_id': 'd7f8172c-62e4-439d-9c52-1fe1ab2e557d',
						'data': [2, 0, 1, 0, 2]
					}, {
						'_id': 'e7e2636d-d8ec-4171-8239-4d242970073e',
						'data': [2, 1, 0, 0, 1]
					}, {
						'_id': '78ceeb90-719d-4c56-abd3-ff361339de1f',
						'data': [0, 1, 2, 2, 1]
					}, {
						'_id': 'ccdc01c2-f98a-4557-ab41-ab5319f5990b',
						'data': [0, 0, 2, 2, 2]
					}, {
						'_id': '438f9d9f-9f3f-4062-bd47-5f961944922e',
						'data': [2, 2, 0, 0, 0]
					}, {
						'_id': '1cd965ba-9814-40e9-af75-b3b62c9b7856',
						'data': [1, 0, 2, 1, 2]
					}, {
						'_id': '6bcf4d52-2235-48d1-bd4d-0e55d875c90a',
						'data': [1, 2, 0, 1, 0]
					}, {
						'_id': 'f2c0c1f7-1257-4e65-8aa2-9bcf411a0c75',
						'data': [0, 1, 0, 1, 2]
					}, {
						'_id': '64c56879-9c10-45f1-8867-617082f131ea',
						'data': [2, 1, 2, 1, 0]
					}, {
						'_id': '4cbf05a1-e633-461b-9371-a4b2c9737aea',
						'data': [1, 2, 2, 0, 0]
					}, {
						'_id': '6bf23e34-dccc-4776-a126-477cd6fbd902',
						'data': [0, 0, 1, 1, 2]
					}, {
						'_id': '72d6b6a0-ef12-4791-b76e-08d121aceb8f',
						'data': [2, 2, 1, 1, 0]
					}]
				},
				'payTable': {
					'13481430-c234-4c17-8769-ad8174f89ab1': {
						'3': {
							'Amount': 0,
							'Multiplier': 8
						},
						'4': {
							'Amount': 0,
							'Multiplier': 15
						},
						'5': {
							'Amount': 0,
							'Multiplier': 100
						}
					},
					'35de97a3-fa13-4b97-a3b1-493ecb90b413': {},
					'5063dff2-cef5-4d99-897b-a627e4e78036': {
						'3': {
							'Amount': 0,
							'Multiplier': 12
						},
						'4': {
							'Amount': 0,
							'Multiplier': 25
						},
						'5': {
							'Amount': 0,
							'Multiplier': 150
						}
					},
					'538eea5d-0a59-4bce-b2ea-06a68673df64': {
						'3': {
							'Amount': 0,
							'Multiplier': 5
						},
						'4': {
							'Amount': 0,
							'Multiplier': 10
						},
						'5': {
							'Amount': 0,
							'Multiplier': 50
						}
					},
					'61adce3b-47c1-48d7-9a56-3eeda8474a03': {
						'3': {
							'Amount': 0,
							'Multiplier': 4
						},
						'4': {
							'Amount': 0,
							'Multiplier': 8
						},
						'5': {
							'Amount': 0,
							'Multiplier': 40
						}
					},
					'8231a0c4-8b0b-4926-96ee-6fd56fc25ac9': {
						'3': {
							'Amount': 0,
							'Multiplier': 15
						},
						'4': {
							'Amount': 0,
							'Multiplier': 50
						},
						'5': {
							'Amount': 0,
							'Multiplier': 250
						}
					},
					'87d49ab3-32d5-44c7-8d41-0371f2eb00df': {
						'3': {
							'Amount': 0,
							'Multiplier': 6
						},
						'4': {
							'Amount': 0,
							'Multiplier': 12
						},
						'5': {
							'Amount': 0,
							'Multiplier': 70
						}
					},
					'b2594c1e-d3fd-481a-9248-296c14db282c': {},
					'bc54ff34-478a-45bd-a501-d7ff6428d5a9': {
						'3': {
							'Amount': 0,
							'Multiplier': 4
						},
						'4': {
							'Amount': 0,
							'Multiplier': 8
						},
						'5': {
							'Amount': 0,
							'Multiplier': 40
						}
					},
					'd12dcd92-ece9-440e-8197-729016eb4dcd': {
						'3': {
							'Amount': 0,
							'Multiplier': 10
						},
						'4': {
							'Amount': 0,
							'Multiplier': 20
						},
						'5': {
							'Amount': 0,
							'Multiplier': 125
						}
					},
					'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0': {
						'3': {
							'Amount': 0,
							'Multiplier': 4
						},
						'4': {
							'Amount': 0,
							'Multiplier': 8
						},
						'5': {
							'Amount': 0,
							'Multiplier': 40
						}
					}
				},
				'reelConfigSet': [{
					'NumberOfReels': 5,
					'Reel1': {
						'NumberOfRows': 64,
						'SymbolDistribution': [{
							'RowNumber': 0,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 1,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 2,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 3,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 4,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 5,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 6,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 7,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 8,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 9,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 10,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 11,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 12,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 13,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 14,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 15,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 16,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 17,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 18,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 19,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 20,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 21,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 22,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 23,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 24,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 25,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 26,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 27,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 28,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 29,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 30,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 31,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 32,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 33,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 34,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 35,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 36,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 37,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 38,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 39,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 40,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 41,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 42,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 43,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 44,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 45,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 46,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 47,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 48,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 49,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 50,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 51,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 52,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 53,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 54,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 55,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 56,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 57,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 58,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 59,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 60,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 61,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 62,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 63,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}]
					},
					'Reel2': {
						'NumberOfRows': 64,
						'SymbolDistribution': [{
							'RowNumber': 0,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 1,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 2,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 3,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 4,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 5,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 6,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 7,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 8,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 9,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 10,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 11,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 12,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 13,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 14,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 15,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 16,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 17,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 18,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 19,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 20,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 21,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 22,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 23,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 24,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 25,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 26,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 27,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 28,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 29,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 30,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 31,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 32,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 33,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 34,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 35,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 36,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 37,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 38,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 39,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 40,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 41,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 42,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 43,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 44,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 45,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 46,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 47,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 48,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 49,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 50,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 51,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 52,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 53,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 54,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 55,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 56,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 57,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 58,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 59,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 60,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 61,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 62,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 63,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}]
					},
					'Reel3': {
						'NumberOfRows': 64,
						'SymbolDistribution': [{
							'RowNumber': 0,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 1,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 2,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 3,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 4,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 5,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 6,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 7,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 8,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 9,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 10,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 11,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 12,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 13,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 14,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 15,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 16,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 17,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 18,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 19,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 20,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 21,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 22,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 23,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 24,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 25,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 26,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 27,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 28,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 29,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 30,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 31,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 32,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 33,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 34,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 35,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 36,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 37,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 38,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 39,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 40,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 41,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 42,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 43,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 44,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 45,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 46,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 47,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 48,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 49,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 50,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 51,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 52,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 53,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 54,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 55,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 56,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 57,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 58,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 59,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 60,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 61,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 62,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 63,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}]
					},
					'Reel4': {
						'NumberOfRows': 64,
						'SymbolDistribution': [{
							'RowNumber': 0,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 1,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 2,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 3,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 4,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 5,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 6,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 7,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 8,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 9,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 10,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 11,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 12,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 13,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 14,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 15,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 16,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 17,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 18,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 19,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 20,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 21,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 22,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 23,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 24,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 25,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 26,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 27,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 28,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 29,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 30,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 31,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 32,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 33,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 34,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 35,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 36,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 37,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 38,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 39,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 40,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 41,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 42,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 43,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 44,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 45,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 46,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 47,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 48,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 49,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 50,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 51,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 52,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 53,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 54,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 55,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 56,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 57,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 58,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 59,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 60,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 61,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 62,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 63,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}]
					},
					'Reel5': {
						'NumberOfRows': 64,
						'SymbolDistribution': [{
							'RowNumber': 0,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 1,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 2,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 3,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 4,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 5,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 6,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 7,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 8,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 9,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 10,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 11,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 12,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 13,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 14,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 15,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 16,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 17,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 18,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 19,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 20,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 21,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 22,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 23,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 24,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 25,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 26,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 27,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 28,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 29,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 30,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 31,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 32,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 33,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 34,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 35,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 36,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 37,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 38,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 39,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 40,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 41,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 42,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 43,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 44,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 45,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 46,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 47,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 48,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 49,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 50,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 51,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 52,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 53,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 54,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 55,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 56,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 57,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 58,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 59,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 60,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 61,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 62,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 63,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}]
					}
				}, {
					'NumberOfReels': 5,
					'Reel1': {
						'NumberOfRows': 64,
						'SymbolDistribution': [{
							'RowNumber': 0,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 1,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 2,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 3,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 4,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 5,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 6,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 7,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 8,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 9,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 10,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 11,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 12,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 13,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 14,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 15,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 16,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 17,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 18,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 19,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 20,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 21,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 22,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 23,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 24,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 25,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 26,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 27,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 28,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 29,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 30,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 31,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 32,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 33,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 34,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 35,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 36,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 37,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 38,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 39,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 40,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 41,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 42,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 43,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 44,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 45,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 46,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 47,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 48,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 49,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 50,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 51,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 52,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 53,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 54,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 55,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 56,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 57,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 58,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 59,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 60,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 61,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 62,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 63,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}]
					},
					'Reel2': {
						'NumberOfRows': 64,
						'SymbolDistribution': [{
							'RowNumber': 0,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 1,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 2,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 3,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 4,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 5,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 6,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 7,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 8,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 9,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 10,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 11,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 12,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 13,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 14,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 15,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 16,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 17,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 18,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 19,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 20,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 21,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 22,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 23,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 24,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 25,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 26,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 27,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 28,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 29,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 30,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 31,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 32,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 33,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 34,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 35,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 36,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 37,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 38,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 39,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 40,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 41,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 42,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 43,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 44,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 45,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 46,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 47,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 48,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 49,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 50,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 51,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 52,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 53,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 54,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 55,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 56,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 57,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 58,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 59,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 60,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 61,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 62,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 63,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}]
					},
					'Reel3': {
						'NumberOfRows': 64,
						'SymbolDistribution': [{
							'RowNumber': 0,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 1,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 2,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 3,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 4,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 5,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 6,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 7,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 8,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 9,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 10,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 11,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 12,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 13,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 14,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 15,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 16,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 17,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 18,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 19,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 20,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 21,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 22,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 23,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 24,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 25,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 26,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 27,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 28,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 29,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 30,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 31,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 32,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 33,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 34,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 35,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 36,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 37,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 38,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 39,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 40,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 41,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 42,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 43,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 44,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 45,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 46,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 47,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 48,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 49,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 50,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 51,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 52,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 53,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 54,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 55,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 56,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 57,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 58,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 59,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 60,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 61,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 62,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 63,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}]
					},
					'Reel4': {
						'NumberOfRows': 64,
						'SymbolDistribution': [{
							'RowNumber': 0,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 1,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 2,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 3,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 4,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 5,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 6,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 7,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 8,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 9,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 10,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 11,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 12,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 13,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 14,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 15,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 16,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 17,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 18,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 19,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 20,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 21,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 22,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 23,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 24,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 25,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 26,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 27,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 28,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 29,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 30,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 31,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 32,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 33,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 34,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 35,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 36,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 37,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 38,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 39,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 40,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 41,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 42,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 43,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 44,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 45,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 46,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 47,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 48,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 49,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 50,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 51,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 52,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 53,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 54,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 55,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 56,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 57,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 58,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 59,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 60,
							'SymbolId': '35de97a3-fa13-4b97-a3b1-493ecb90b413',
							'SymbolName': 'Wild',
							'SymbolType': 'Wild',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild'
						}, {
							'RowNumber': 61,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 62,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 63,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}]
					},
					'Reel5': {
						'NumberOfRows': 64,
						'SymbolDistribution': [{
							'RowNumber': 0,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 1,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 2,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 3,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 4,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 5,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 6,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 7,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 8,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 9,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 10,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 11,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 12,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 13,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 14,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 15,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 16,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 17,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 18,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 19,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 20,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 21,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 22,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 23,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 24,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 25,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 26,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 27,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 28,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 29,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 30,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 31,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 32,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 33,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 34,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 35,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 36,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 37,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 38,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 39,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 40,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 41,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 42,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 43,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 44,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 45,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 46,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 47,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 48,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 49,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 50,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 51,
							'SymbolId': 'b2594c1e-d3fd-481a-9248-296c14db282c',
							'SymbolName': 'Bonus',
							'SymbolType': 'Scatter',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
						}, {
							'RowNumber': 52,
							'SymbolId': 'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0',
							'SymbolName': 'Low1',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
						}, {
							'RowNumber': 53,
							'SymbolId': '5063dff2-cef5-4d99-897b-a627e4e78036',
							'SymbolName': 'Hi2',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
						}, {
							'RowNumber': 54,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 55,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}, {
							'RowNumber': 56,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 57,
							'SymbolId': 'bc54ff34-478a-45bd-a501-d7ff6428d5a9',
							'SymbolName': 'Low3',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
						}, {
							'RowNumber': 58,
							'SymbolId': 'd12dcd92-ece9-440e-8197-729016eb4dcd',
							'SymbolName': 'Hi3',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
						}, {
							'RowNumber': 59,
							'SymbolId': '87d49ab3-32d5-44c7-8d41-0371f2eb00df',
							'SymbolName': 'Hi5',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
						}, {
							'RowNumber': 60,
							'SymbolId': '8231a0c4-8b0b-4926-96ee-6fd56fc25ac9',
							'SymbolName': 'Hi1',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
						}, {
							'RowNumber': 61,
							'SymbolId': '61adce3b-47c1-48d7-9a56-3eeda8474a03',
							'SymbolName': 'Low2',
							'SymbolType': 'Low Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
						}, {
							'RowNumber': 62,
							'SymbolId': '13481430-c234-4c17-8769-ad8174f89ab1',
							'SymbolName': 'Hi4',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
						}, {
							'RowNumber': 63,
							'SymbolId': '538eea5d-0a59-4bce-b2ea-06a68673df64',
							'SymbolName': 'Hi6',
							'SymbolType': 'High Paying',
							'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
						}]
					}
				}],
				'selectablePaylines': {
					'40': ['db32dcf0-e625-4034-98fd-212181868246', 'bb47a6a3-816f-4f09-9b41-0f5a12f6ee8c', 'faddebcf-c15b-4332-bb1d-4d8eb27be8c1', '38c35e4e-33b1-4237-82ed-5a2293d80cd5', '711a9e1a-e4a4-4cf2-ae0f-30c12e450408', '2d61b0e0-84bc-402c-9326-c738bebbeb31', '74fdb327-1ef6-45b9-929d-c1afa067c57d', '3afe6d5d-70b8-45bf-91d2-9a54e5b956f3', '8576c23c-c8dc-4f9c-b993-b53bf66efba2', '4f787027-9f68-4b71-9d69-4d019a0773e2', '9e9e87b8-0d22-4395-a894-74bcbdc2de79', '10e214cc-42f5-4461-bfd3-a81081767835', '4e0c9290-f86a-4550-9aa9-211fd353c441', '65aff733-0f47-41fe-9f5c-9e645814fbdd', '57ba9290-60e9-4747-99a5-77e0e4db92d6', 'b7cc86ba-fecf-417d-82b6-eed72e48137f', 'f00676a9-3621-4594-9a5d-4d029c0fa45b', '68d97b61-c722-4de9-9fe3-1108a6a9fe70', '002a8929-b0e9-4af4-bee2-61d96bac1711', '9471ce8d-fea8-468c-96d8-b4cb50bdb027', 'ab8ac917-4c32-411b-910e-13430b8cf8ac', '40421e71-0f29-4618-8132-2608a7ddf8ce', '4f7416db-8c91-4a19-9bc8-0743810312e9', '7db16c74-a968-48f9-b1f9-d4a8a29bcee3', '67b5fb65-604b-4803-9fb0-25c89a94ab0d', '646deb91-d1da-4297-b9c5-dbb00378dd94', '1420763f-ed44-4db5-a753-b346269616de', '6cafc562-13de-4c30-9b80-74f0b3179f07', 'd7f8172c-62e4-439d-9c52-1fe1ab2e557d', 'e7e2636d-d8ec-4171-8239-4d242970073e', '78ceeb90-719d-4c56-abd3-ff361339de1f', 'ccdc01c2-f98a-4557-ab41-ab5319f5990b', '438f9d9f-9f3f-4062-bd47-5f961944922e', '1cd965ba-9814-40e9-af75-b3b62c9b7856', '6bcf4d52-2235-48d1-bd4d-0e55d875c90a', 'f2c0c1f7-1257-4e65-8aa2-9bcf411a0c75', '64c56879-9c10-45f1-8867-617082f131ea', '4cbf05a1-e633-461b-9371-a4b2c9737aea', '6bf23e34-dccc-4776-a126-477cd6fbd902', '72d6b6a0-ef12-4791-b76e-08d121aceb8f'],
					'options': ['40']
				},
				'specialFeature': {
					'reelSwitching': {
						'freeGame': ['0', '0'],
						'mainGame': ['0', '0', '0', '1', '1', '1', '0', '1', '1', '1', '1', '1', '0', '0', '1', '1', '1', '0', '0', '1', '1', '0', '0', '1', '0']
					},
					'wildMultiplierProb': {
						'freeGame': {
							'reel2': ['2', '2', '2', '2', '5', '2', '2', '2', '2', '2', '5', '2', '2', '2', '2', '5', '2', '2', '10', '2', '2', '2', '100', '2', '2', '2', '2', '2', '2', '2', '2', '5', '2', '10', '2', '2', '2', '5', '2', '2', '2', '2', '2', '10', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '5', '2', '2', '2', '2', '2', '2', '5', '2', '2', '2', '5', '2', '2', '2', '2', '2', '5', '5', '5', '2', '2', '2', '2', '2', '2', '2', '2', '5', '2', '2', '5', '100', '5', '2', '2', '5', '2', '2', '2', '2', '2', '2', '2', '2', '2'],
							'reel3': ['2', '5', '5', '2', '2', '2', '2', '5', '2', '2', '2', '2', '5', '2', '5', '2', '2', '5', '2', '5', '2', '100', '5', '10', '2', '2', '2', '2', '2', '10', '5', '2', '2', '2', '2', '2', '2', '2', '2', '2', '5', '2', '5', '5', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '5', '2', '2', '2', '2', '2', '2', '5', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '10', '100', '2', '5', '2', '2', '5', '2', '5', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '5', '2', '5', '2', '5'],
							'reel4': ['100', '5', '2', '5', '2', '2', '2', '5', '2', '2', '2', '2', '10', '2', '2', '2', '2', '5', '2', '2', '2', '10', '2', '2', '2', '2', '2', '2', '2', '5', '2', '2', '2', '2', '2', '5', '100', '2', '2', '2', '5', '2', '2', '2', '2', '2', '2', '5', '2', '5', '2', '2', '2', '2', '100', '2', '5', '5', '10', '2', '2', '100', '2', '2', '2', '2', '2', '2', '2', '10', '2', '5', '5', '2', '2', '2', '5', '2', '2', '2', '2', '10', '2', '2', '2', '2', '2', '5', '10', '2', '2', '2', '5', '5', '2', '5', '2', '5', '5', '2']
						},
						'mainGame': {
							'reel2': ['50', '2', '2', '2', '2', '2', '10', '5', '2', '2', '2', '2', '2', '5', '2', '10', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '5', '2', '2', '2', '2', '2', '2', '5', '5', '2', '2', '2', '2', '2', '2', '2', '5', '2', '2', '2', '2', '2', '2', '2', '2', '2', '5', '5', '2', '2', '2', '10', '5', '2', '2', '2', '2', '2', '2', '5', '2', '2', '2', '2', '2', '2', '5', '2', '2', '2', '2', '2', '5', '2', '5', '2', '2', '2', '2', '2', '2', '5', '5', '2', '2', '2', '10', '2'],
							'reel3': ['2', '5', '10', '2', '5', '10', '2', '5', '2', '10', '2', '5', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '5', '2', '2', '2', '2', '5', '2', '2', '2', '2', '2', '2', '50', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '5', '2', '5', '2', '2', '2', '2', '5', '2', '2', '2', '5', '2', '2', '5', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '5', '2', '2', '10', '2', '2', '2', '2', '2', '2', '2', '2', '5', '5', '5', '2', '2', '5', '5', '5', '5', '2', '5', '2', '2'],
							'reel4': ['5', '2', '2', '2', '2', '2', '10', '2', '2', '10', '2', '50', '2', '2', '10', '5', '2', '2', '10', '2', '2', '2', '5', '2', '2', '2', '5', '5', '10', '5', '2', '5', '50', '2', '2', '5', '2', '2', '2', '5', '2', '5', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '5', '2', '2', '2', '5', '2', '2', '2', '5', '10', '2', '2', '5', '2', '5', '2', '5', '2', '10', '2', '2', '2', '2', '5', '2', '2', '5', '2', '2', '2', '10', '2', '2', '5', '2', '2', '2', '2', '2', '2', '2', '5', '2', '2', '2', '2', '2']
						}
					}
				},
				'symbols': {
					'13481430-c234-4c17-8769-ad8174f89ab1': {
						'SymbolName': 'Hi4',
						'SymbolNumber': 5,
						'SymbolType': 'High Paying',
						'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi4'
					},
					'35de97a3-fa13-4b97-a3b1-493ecb90b413': {
						'SymbolName': 'Wild',
						'SymbolNumber': 1,
						'SymbolType': 'Wild',
						'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/wild',
						'doesWildPay': false,
						'isExpandingWild': false
					},
					'5063dff2-cef5-4d99-897b-a627e4e78036': {
						'SymbolName': 'Hi2',
						'SymbolNumber': 3,
						'SymbolType': 'High Paying',
						'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi2'
					},
					'538eea5d-0a59-4bce-b2ea-06a68673df64': {
						'SymbolName': 'Hi6',
						'SymbolNumber': 7,
						'SymbolType': 'High Paying',
						'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi6'
					},
					'61adce3b-47c1-48d7-9a56-3eeda8474a03': {
						'SymbolName': 'Low2',
						'SymbolNumber': 9,
						'SymbolType': 'Low Paying',
						'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low2'
					},
					'8231a0c4-8b0b-4926-96ee-6fd56fc25ac9': {
						'SymbolName': 'Hi1',
						'SymbolNumber': 2,
						'SymbolType': 'High Paying',
						'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi1'
					},
					'87d49ab3-32d5-44c7-8d41-0371f2eb00df': {
						'SymbolName': 'Hi5',
						'SymbolNumber': 6,
						'SymbolType': 'High Paying',
						'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi5'
					},
					'b2594c1e-d3fd-481a-9248-296c14db282c': {
						'SymbolName': 'Bonus',
						'SymbolNumber': 11,
						'SymbolType': 'Scatter',
						'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/bonus'
					},
					'bc54ff34-478a-45bd-a501-d7ff6428d5a9': {
						'SymbolName': 'Low3',
						'SymbolNumber': 10,
						'SymbolType': 'Low Paying',
						'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low3'
					},
					'd12dcd92-ece9-440e-8197-729016eb4dcd': {
						'SymbolName': 'Hi3',
						'SymbolNumber': 4,
						'SymbolType': 'High Paying',
						'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/hi3'
					},
					'd2042d7f-c6ee-46b4-99bc-cf6a66e15ec0': {
						'SymbolName': 'Low1',
						'SymbolNumber': 8,
						'SymbolType': 'Low Paying',
						'SymbolUrl': 'https://slot-game-symbols.s3.amazonaws.com/d94ad420-486a-43ff-8284-a8791a6b6d5c/low1'
					}
				},
				'viewZone': 35
			},
			'isDeleted': false,
			'status': 'start',
			'updatedAt': 1553776596964,
			'version': 5,
			'versionCode': 'v6',
			'versionName': 'V6'
		},

    VALID_GAME_ID: 'd94ad420-486a-43ff-8284-a8791a6b6d5c',
    VALID_USER_TOKEN: '59b8e8d3502b2f25995438f5',
    INVALID_USER_TOKEN: '97b2ghkskrfa',
    VALID_USER_DETAILS: { 'user': { 'birthday': '1-1-1970', 'gender': 'na', 'id': '597b1378502b2f09f265a1f0', 'level': 36, 'levelProgress': 0.23, 'locale': 'de_DE', 'multiplier': 1, 'name': 'rinkal', 'wallet': { 'chips': 11183327193099 } }, 'game': { 'settings': { 'bets': [120, 200, 400, 800, 1600, 2400, 3200, 4800, 6400, 8000, 10000, 12000, 16000, 24000, 28000, 32000, 60000, 80000, 100000, 170000] } } },
    VALID_CLIENT_ID: 'k7uACEZBTfiJ8cIqb3LIfKNYPhRdWZ4i',
    RANDOM_NUMBER_REELCONFIG1: 0,
    RANDOM_NUMBER_REELCONFIG2: 0,
		FREE_GAME_WRONG_RANDOM_REEL_PICKUP: [0, 1],
		WILD_ON_REEL2: [10, 13, 26, 19, 10],
		RANDOM_NUMBERS: [[10, 15, 16, 18, 20], [15, 18, 18, 20, 22], [17, 19, 20, 24, 28], [18, 21, 22, 26, 30], [20, 23, 24, 28, 32], [10, 12, 23, 10, 8]],
    INVALID_CLIENT_ID: 'rtwafmeNKndflwnGI',
    DUMMY_SPECIAL_FEATURE: { 'reelSwitching': { 'freeGame': ['0'], 'mainGame': ['0', '1'] } },
		WILD_MULTIPLIER2: [2, 2, 2],
		WILD_MULTIPLIER5: [5, 5, 5],
		WILD_MULTIPLIER10: [10, 10, 10],

    WILD_SYMBOL_ID: '35de97a3-fa13-4b97-a3b1-493ecb90b413'
};
