const chai = require('chai');
const sinon = require('sinon');
const soaTestData = require('./test-data');
const testData = require('../../test-data');
const secretOfAmun = require('../../../util/games/secret-of-amun');
const gameSession = require('../../../util/gamesession');
const constants = require('../../../response/constants');

const expect = chai.expect;


/**
* Unicron slot related test cases are performed
* Unicron slote tests include following tests
* - Test User session computation for game click
* - Fetching user session with required reel set
* - Fetching reel configuration for subsequent spin in main game and free game
*/
describe('SecretOfAmunTest', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
  beforeEach(() => {
    /**
    * stub to mock getting user session
    */
    sinon.stub(gameSession, 'getUserSession').callsFake((token, gameId) => {
      if (gameId === testData.VALID_GAME_ID && token === testData.VALID_USER_TOKEN) {
        return Promise.resolve(soaTestData.USER_SESSION_FOR_GAMBLE);
      } else if (gameId === soaTestData.VALID_GAME_ID && token === testData.VALID_USER_TOKEN) {
        return Promise.resolve(soaTestData.USER_SESSION_AVAILABLE_GAMBLE);
      } else if (gameId === soaTestData.VALID_GAME_ID_TO_COLLECT && token === testData.VALID_USER_TOKEN) {
        return Promise.resolve(soaTestData.USER_SESSION_COLLECT_WINNINGS);
      }

      return Promise.reject({ error: constants.RES_MESSAGES.NO_PAYTABLE });
    });


    /**
     * stub to mock a call to whow bet api
     */
    sinon.stub(secretOfAmun, 'whowBetApi').callsFake((roundId, token, zero, winAmount, betAmount, session) => Promise.resolve(soaTestData.WHOW_BET_API_RESPONSE));


     /**
      * stub to mock a call to whow close api
      */
    sinon.stub(secretOfAmun, 'whowCloseApi').callsFake((roundId, token, winAmount, session) => Promise.resolve(soaTestData.WHOW_CLOSE_API_RESPONSE));


      /**
       * stub to mock a call to update session
       */
    sinon.stub(secretOfAmun, 'updateSession').callsFake((roundId, token, winAmount, session) => {});
  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    gameSession.getUserSession.restore();
    secretOfAmun.whowBetApi.restore();
    secretOfAmun.whowCloseApi.restore();
    secretOfAmun.updateSession.restore();
  });


  /**
  * Test case to check that wild/scatter is not picked for expanding
  */
  it('Test case to check that wild/scatter is not picked for expanding', (done) => {
    const game = soaTestData.VALID_GAME_DATA;

    const symbolId = secretOfAmun.pickFreeSpinExpandingSymbol(game);

    /**
     * Expect that any other symbol, other than scatter/wild is choosen
     */
    expect(symbolId).to.not.eql(soaTestData.WILD_SCATTER_SYMBOL_ID);
    done();
  });


  /**
  * Test case to check that symbol does not expand
  */
  it('Test case to check that symbol does not expand', (done) => {
    const game = soaTestData.VALID_GAME_DATA;
    const pickedSymbolId = soaTestData.PICKED_SYMBOL_ID;
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const array = soaTestData.ONE_WIN_VIEW_ZONE();

    const result = secretOfAmun.expandSymbolIfQualified(array, pickedSymbolId, noofReels, columnSize, game);

    /**
     * Expect that picked symbol's length is less than the paytable multiplier
     */
    expect(result[2]).to.have.length.below(3);

    /**
     * Expect the expanding result returned to be false
     */
    expect(result[3]).to.be.eql(false);
    done();
  });


  /**
  * Test case to check that symbol does expand
  */
  it('Test case to check that symbol does expand', (done) => {
    const game = soaTestData.VALID_GAME_DATA;
    const pickedSymbolId = soaTestData.PICKED_SYMBOL_ID;
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const array = soaTestData.NO_WIN_VIEW_ZONE();

    const result = secretOfAmun.expandSymbolIfQualified(array, pickedSymbolId, noofReels, columnSize, game);

    /**
     * Expect that picked symbol presens to be greater than or equls to the paytable multiplier
     */
    expect(result[2]).to.have.length.above(2);

    /**
     * Expect the expanding result returned to be true
     */
    expect(result[3]).to.be.eql(true);
    done();
  });


  /**
  * Test case for reels when symbol expands
  */
  it('Test case to validate reels when symbol expands', (done) => {
    const game = soaTestData.VALID_GAME_DATA;
    const pickedSymbolId = soaTestData.PICKED_SYMBOL_ID;
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const expandingPositions = soaTestData.FREE_SPIN_EXPANDING_POS.expandingPositions;
    const result = secretOfAmun.fetchReelsForExpandedViewZone(game, expandingPositions, pickedSymbolId, noofReels, columnSize);

    /**
     * Expect that picked symbol is present in full of the reel
     */
    for (let i = 0; i < noofReels - expandingPositions.length; i += 1) {
      for (let j = 0; j < columnSize; j += 1) {
        expect(result.get(j, i)).to.be.eql(pickedSymbolId);
      }
    }

    done();
  });


  /**
  * Test case for win computation during free spins
  */
  it('Test case for win computation during free spins, no wins', (done) => {
    const game = soaTestData.VALID_GAME_DATA;
    const pickedSymbolId = soaTestData.PICKED_SYMBOL_ID;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const arrays = soaTestData.NO_WIN_VIEW_ZONE();
    const payArray = game.default.gameConfig.payLines.normalizedPayArray;
    secretOfAmun.computeWinsForFreeSpin(arrays, columnSize, payArray, game, [], pickedSymbolId)
    .then((result) => {
      /**
      * Expect that free spin results in no win
      */
      expect(result).to.have.length(0);
      done();
    });
  });


  /**
  * Test case for win computation during free spins
  */
  it('Test case for win computation during free spins, win results', (done) => {
    const game = soaTestData.VALID_GAME_DATA;
    const pickedSymbolId = soaTestData.PICKED_SYMBOL_ID;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const arrays = soaTestData.ONE_WIN_VIEW_ZONE();
    const payArray = game.default.gameConfig.payLines.normalizedPayArray;
    secretOfAmun.computeWinsForFreeSpin(arrays, columnSize, payArray, game, [], pickedSymbolId)
    .then((result) => {
      /**
      * Expect that free spin results when user wins
      */
      expect(result).to.have.length(1);
      done();
    });
  });


  /**
  * Error test case for no of freespin awards based on view zone
  */
  it('Test case for no free spins awards based on view zone', (done) => {
    const game = soaTestData.VALID_GAME_DATA;

    // various free spins awarded for number of scatter symbols
    Promise.all([
      secretOfAmun.freeSpinCompute([0, 1], [], game),
      secretOfAmun.freeSpinCompute([0], [], game),
      secretOfAmun.freeSpinCompute([0, 1, 2, 3, 4, 5], [], game)
    ])
    .then((result) => {
      expect(result[0]).to.be.length(0);
      expect(result[1]).to.be.length(0);
      expect(result[2]).to.be.length(0);
    })
    .then(() => done(), done);
  });


  /**
  * Successful test case for no of freespin awards based on view zone
  */
  it('Test case to check correct awarded free spins, based on view zone', (done) => {
    const game = soaTestData.VALID_GAME_DATA;

    // various free spins awarded for number of scatter symbols
    Promise.all([
      secretOfAmun.freeSpinCompute([53, 19, 13], [], game),
      secretOfAmun.freeSpinCompute([53, 19, 13, 3], [], game),
      secretOfAmun.freeSpinCompute([53, 19, 13, 3, 4], [], game)
    ])
    .then((result) => {
      expect(result[0].freeSpins).to.be.eql(10);
      expect(result[1].freeSpins).to.be.eql(10);
      expect(result[2].freeSpins).to.be.eql(10);
    })
    .then(() => done(), done);
  });


  /**
  * Test case to spin computation results when free spins are not active
  */
  it('Test case of spin computation results when free spins are not active, for no win', (done) => {
    const game = soaTestData.VALID_GAME_DATA;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const arrays = soaTestData.NO_WIN_VIEW_ZONE();
    const deepCopyArray = arrays.clone();
    const payArray = game.default.gameConfig.payLines.normalizedPayArray;

    const pickedSymbol = soaTestData.PICKED_SYMBOL_ID;
    secretOfAmun.computeForNoActiveFreeSpins(game, arrays, deepCopyArray, [], payArray, columnSize, pickedSymbol)
    .then((result) => {
      /**
       * checkout that result is as per expectations
       */
      expect(result.results).to.have.length(0);
      done();
    });
  });


  /**
  * Test case to spin computation results when free spins are not active
  */
  it('Test case of spin computation results when free spins are not active, for win', (done) => {
    const game = soaTestData.VALID_GAME_DATA;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const arrays = soaTestData.ONE_WIN_VIEW_ZONE();
    const deepCopyArray = arrays.clone();
    const payArray = game.default.gameConfig.payLines.normalizedPayArray;

    const pickedSymbol = soaTestData.PICKED_SYMBOL_ID;
    secretOfAmun.computeForNoActiveFreeSpins(game, arrays, deepCopyArray, [], payArray, columnSize, pickedSymbol)
    .then((result) => {
      /**
       * checkout that result is as per expectations
       */
      expect(result.results).to.have.length(1);
      done();
    });
  });


  /**
  * Test case to spin computation results when free spins are active
  */
  it('Test case of spin computation results when free spins are active, for no win', (done) => {
    const game = soaTestData.VALID_GAME_DATA;
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const arrays = soaTestData.NO_WIN_VIEW_ZONE();
    const deepCopyArray = arrays.clone();
    const payArray = game.default.gameConfig.payLines.normalizedPayArray;

    const session = soaTestData.FREE_SPIN_SESSION;
    secretOfAmun.computeForFreeSpin(game, session, arrays, deepCopyArray, [], payArray, noofReels, columnSize)
    .then((result) => {
      /**
       * Expect the required fields
       */
      expect(result).to.have.property('viewZone');
      expect(result).to.have.property('results');
      expect(result).to.have.property('expanding');
      expect(result.expanding).to.have.property('isPickedSymbolExpanding');
      expect(result.expanding).to.have.property('expandingPositions');

      /**
       * Check if the values for the fields are as expected
       */
      expect(result.expanding.isPickedSymbolExpanding).to.be.eql(false);
      expect(result.results).to.have.length(0);

      done();
    });
  });

  /**
  * Test case to spin computation results when free spins are active
  */
  it('Test case of spin computation results when free spins are active, for win', (done) => {
    const game = soaTestData.VALID_GAME_DATA;
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const arrays = soaTestData.ONE_WIN_VIEW_ZONE();
    const deepCopyArray = arrays.clone();
    const payArray = game.default.gameConfig.payLines.normalizedPayArray;

    const session = soaTestData.FREE_SPIN_SESSION;
    secretOfAmun.computeForFreeSpin(game, session, arrays, deepCopyArray, [], payArray, noofReels, columnSize)
    .then((result) => {
      /**
       * Expect the required fields
       */
      expect(result).to.have.property('viewZone');
      expect(result).to.have.property('results');
      expect(result).to.have.property('expanding');
      expect(result.expanding).to.have.property('isPickedSymbolExpanding');
      expect(result.expanding).to.have.property('expandingPositions');

      /**
       * Check if the values for the fields are as expected
       */
      expect(result.expanding.isPickedSymbolExpanding).to.be.eql(false);
      expect(result.results).to.have.length(1);

      /**
       * Expect that all the properties are present in result object
       */
      const results = result.results;
      results.forEach((res) => {
        expect(res).to.have.property('symbolId');
        expect(res).to.have.property('winType');
        expect(res).to.have.property('winLineId');
        expect(res).to.have.property('multiplier');
      });
      done();
    });
  });


  /**
  * Test case for gamble request with invalid token
  */
  it('Error Test case for gamble request with invalid token', (done) => {
    secretOfAmun.handleGambleRequest(testData.INVALID_TOKEN, testData.VALID_GAME_ID, soaTestData.VALID_CLIENT_ID, '', soaTestData.USER_SESSION_COLLECT_WINNINGS)
    .then((response) => {
      expect(response).to.have.property('error');
      done();
    });
  });


  /**
  * Test case for gamble request with invalid game id
  */
  it('Error Test case for gamble request with invalid game id', (done) => {
    secretOfAmun.handleGambleRequest(testData.VALID_USER_TOKEN, testData.INVALID_GAME_ID, soaTestData.VALID_CLIENT_ID, '', soaTestData.USER_SESSION_COLLECT_WINNINGS)
    .then((response) => {
      expect(response).to.have.property('error');
      done();
    });
  });


  /**
  * Test case for gamble request with invalid client id
  */
  it('Error Test case for gamble request with invalid client id', (done) => {
    secretOfAmun.handleGambleRequest(testData.VALID_USER_TOKEN, testData.VALID_GAME_ID, testData.INVALID_CLIENT_ID, '', soaTestData.USER_SESSION_COLLECT_WINNINGS)
    .then((response) => {
      expect(response).to.have.property('error');
      done();
    });
  });


  /**
  * Test case for gamble request with invalid card
  */
  it('Error Test case for gamble request with invalid card', (done) => {
    secretOfAmun.handleGambleRequest(testData.VALID_USER_TOKEN, testData.VALID_GAME_ID, soaTestData.VALID_CLIENT_ID, '', soaTestData.USER_SESSION_COLLECT_WINNINGS)
    .then((response) => {
      expect(response).to.have.property('error');
      done();
    });
  });


  /**
  * Test case for gamble request with null game parameters
  */
  it('Error Test case for gamble request with invalid card', (done) => {
    secretOfAmun.handleGambleRequest(null, null, null, null, soaTestData.USER_SESSION_COLLECT_WINNINGS)
    .then((response) => {
      expect(response).to.have.property('error');
      done();
    });
  });


  /**
  * Test case for gamble request with all valid parameters to check gamble unvailability
  */
  it('Test case to check gamble unvailability', (done) => {
    const token = testData.VALID_USER_TOKEN;
    const gameId = testData.VALID_GAME_ID;
    const clientID = soaTestData.VALID_CLIENT_ID;
    secretOfAmun.handleGambleRequest(token, gameId, clientID, '', soaTestData.USER_SESSION_COLLECT_WINNINGS)
    .then((response) => {
      expect(response).to.have.property('error');
      done();
    });
  });


  /**
  * Test case for gamble request with all valid parameters to check gamble availability
  */
  it('Test case to check gamble availability', (done) => {
    const token = testData.VALID_USER_TOKEN;
    const gameId = testData.VALID_GAME_ID;
    const card = soaTestData.GAMBLE_RED_CARD;
    secretOfAmun.handleGambleRequest(token, gameId, '', card, soaTestData.USER_SESSION_COLLECT_WINNINGS)
    .then((response) => {
      expect(response).to.have.property('error');
      done();
    });
  });


  /**
  * Test case for gamble request with all valid parameters
  */
  it('Successful Test case to check gamble, for required fields in response', (done) => {
    const token = testData.VALID_USER_TOKEN;
    const gameId = soaTestData.VALID_GAME_ID;
    const clientID = soaTestData.VALID_CLIENT_ID;
    const card = soaTestData.GAMBLE_RED_CARD;
    secretOfAmun.handleGambleRequest(token, gameId, clientID, card, soaTestData.USER_SESSION_AVAILABLE_GAMBLE)
    .then((response) => {
      // expect required fields to be present with validity
      if (response.isGambleAvailable === true) {
        // when user wins
        expect(response).to.have.property('gambleWon').to.be.eql(true);
        expect(response).to.have.property('gambleCount').to.be.eql(1);
        expect(response).to.have.property('gambleTotalAmount').to.be.eql(500);
        expect(response).to.have.property('gambleBetAmount').to.be.eql(500);
        expect(response).to.have.property('gamblePotentialWinAmount').to.be.eql(response.gambleTotalAmount * 2);
        expect(response).to.have.property('gambleHistory');
        expect(response).to.have.property('wallet');
        expect(response).to.have.property('isGambleAvailable').to.be.eql(true);
      } else {
        // when user looses
        expect(response).to.have.property('gambleCount').to.be.eql(0);
        expect(response).to.have.property('gambleWon').to.be.eql(false);
        expect(response).to.have.property('gambleTotalAmount');
        expect(response).to.have.property('gambleHistory');
        expect(response).to.have.property('wallet');
        expect(response).to.have.property('isGambleAvailable').to.be.eql(false);
      }

      expect(response.gambleHistory).to.have.length.lt(6);
      done();
    });
  });


  /**
  * Collect winnings test case for invalid client id
  */
  it('Error Test case to collect winnings, invalid client id', (done) => {
    const token = testData.VALID_USER_TOKEN;
    const gameId = testData.VALID_GAME_ID;
    const clientID = testData.INVALID_CLIENT_ID;
    secretOfAmun.pickGamble(token, gameId, clientID, soaTestData.USER_SESSION_COLLECT_WINNINGS)
    .then((response) => {
      expect(response).to.have.property('error');
      done();
    });
  });


  /**
  * Collect winnings test case for valid parameters
  */
  it('Successful Test case to decline collect winnings, when there is no win', (done) => {
    const token = testData.VALID_USER_TOKEN;
    const gameId = testData.VALID_GAME_ID;
    const clientID = soaTestData.VALID_CLIENT_ID;
    secretOfAmun.pickGamble(token, gameId, clientID, soaTestData.USER_SESSION_FOR_GAMBLE)
    .then((response) => {
      expect(response).to.have.property('error');
      done();
    });
  });


  /**
  * Collect winnings test case for valid parameters
  */
  it('Successful Test case to accept collect winnings, when there is win', (done) => {
    const token = testData.VALID_USER_TOKEN;
    const gameId = soaTestData.VALID_GAME_ID_TO_COLLECT;
    const clientID = soaTestData.VALID_CLIENT_ID;
    secretOfAmun.pickGamble(token, gameId, clientID, soaTestData.USER_SESSION_COLLECT_WINNINGS)
    .then((response) => {
      expect(response).to.have.property('collectAmount');
      expect(response).to.have.property('wallet');
      expect(response.wallet).to.have.property('chips');
      done();
    });
  });
});
