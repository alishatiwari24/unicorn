const chai = require('chai');
const sinon = require('sinon');
const testData = require('../../test-data');
const rng = require('../../../util/rng');

const expect = chai.expect;


/**
* Game random number generator related tests are performed
* Random numbers are generated
* Game logger tests include following tests
* - Test log data validity for properties that needs to be present
* - Win line/symbol counter test
* both negative and positive test cases are covered
*/
describe('GameRNGTest', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
  beforeEach(() => {

  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {

  });

  /**
  * Error test case when reel configuration is not valid
  */
  it('rng error test case when reel configuration is undefined/null', (done) => {
    const result = rng.generateRandomNumber(null);
    expect(result).to.be.a('null');
    done();
  });

  /**
   * Error test case when reel configuration is valid but random numbers are not valid
   */
  it('rng error test case when reel configuration is valid and random numbers are not', (done) => {
    const reelConfig = JSON.parse(testData.RNG_INVALID_REEL_CONFIG);

    const result = rng.generateRandomNumber(reelConfig);

    /**
     * Expect no random numbers are generated
     */
    expect(result).to.not.have.length.gt(0);
    done();
  });


  /**
  * Successful test case when reel configuration is valid
  */
  it('rng successful test case when reel configuration valid', (done) => {
    const game = JSON.parse(testData.GAME_DATA).game;
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;

    const result = rng.generateRandomNumber(game.default.gameConfig.reelConfig);

    /**
     * Expect to have required random numbers generated as per reel configuration
     */
    expect(result).to.have.length(noofReels);

    /**
     * Expect each number
     */
    result.forEach((number) => {
      expect(number).to.be.a('number');
      expect(number).to.be.gte(0);
    });
    done();
  });
});
