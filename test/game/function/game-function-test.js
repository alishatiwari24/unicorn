  const chai = require('chai');
  const sinon = require('sinon');
  const Game = require('../../../helpers/gamefunction');
  const testData = require('../../test-data');
  const successCases = require('./success-response');
  const compute = require('../../../util/compute');
  const constants = require('../../../response/constants');
  const whowapi = require('../../../api/whowapi');
  const failCases = require('./failure-response');
  const gameSession = require('../../../util/gamesession');

  const expect = chai.expect;


/**
* Game spin releted tests are performed
* includes error test cases such as
* - Invalid game id
* - Invalid user token
* - Invalid clientID
* and success case where game spin happens successfully
*/
  describe('GameFunctionTest', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
    beforeEach(() => {
    /**
    * stub to mock getting a game
    */
      sinon.stub(Game, 'getGame').callsFake((gameId) => {
        if (gameId === testData.VALID_GAME_ID) {
          return JSON.parse(testData.GAME_DATA);
        }
        return undefined;
      });

    /**
    * stub to mock getting user session
    */
      sinon.stub(Game, 'getUserSessionAndSessionRequest').callsFake((token, gameId) => new Promise((resolve, reject) => {
        if (gameId === testData.VALID_GAME_ID && token == testData.VALID_USER_TOKEN) {
          resolve(successCases.VALID_USER_SESSION);
        } else {
          reject();
        }
      }));

    /**
    * stub to mock updating user session
    */
      sinon.stub(Game, 'updateSessionRequestDoc').callsFake((sessionRequestKey, sessionRequest, cas) => {
        if (!sessionRequestKey || !sessionRequest || !cas) {
          return Promise.reject({});
        }
        return Promise.resolve();
      });

    /**
    * stub to mock computation of results
    */
      sinon.stub(compute, 'compute').callsFake((game, gameId, token, session) => {
        if (!game || !gameId || !token || !session) {
          return Promise.reject({});
        }
        return Promise.resolve(JSON.parse(testData.SPIN_RESULT_DATA_FOR_WHOW_API_1));
      });

    /**
    * stub to generate game spin response
    */
      sinon.stub(compute, 'generateSpinResponse').callsFake((obj, computeData, token, sessionRequestKey, sessionRequest, session) => Promise.resolve(testData.SUCCESSFUL_SPIN_RESULT));

    /**
    * stub to mock computation of results
    */
      sinon.stub(Game, 'getUserSession').callsFake((token, gameId) => {
        if (gameId == testData.VALID_GAME_ID && token == testData.VALID_USER_TOKEN) {
          const session = successCases.VALID_USER_SESSION[successCases.VALID_USER_SESSION.sessionKey].value;
          return Promise.resolve(session);
        }
        return Promise.reject({ error: constants.RES_MESSAGES.NO_PAYTABLE });
      });

    /**
    * stub to mock computation of results
    */
      sinon.stub(gameSession, 'getUserSession').callsFake((token, gameId) => {
        if (gameId == testData.VALID_GAME_ID && token == testData.VALID_USER_TOKEN) {
          const session = successCases.VALID_USER_SESSION[successCases.VALID_USER_SESSION.sessionKey].value;
          return Promise.resolve(session);
        }
        return Promise.reject({ error: constants.RES_MESSAGES.NO_PAYTABLE });
      });

    /**
    * stub to mock whow api to get user details
    */
      sinon.stub(whowapi, 'get').callsFake(token => new Promise((resolve, reject) => {
        if (token == testData.VALID_USER_TOKEN) {
          resolve(successCases.WHOW_VALID_TOKEN_RESPONSE);
        } else {
          resolve(failCases.WHOW_INVALID_TOKEN_RESPONSE);
        }
      }));

    /**
    * stub to mock updating user session with bet lines
    */
      sinon.stub(gameSession, 'updateBetLines').callsFake((id, gameId, line) => {});

    /**
    * stub to mock getting playable games
    */
      sinon.stub(Game, 'getAllPlayableGames').callsFake((token, gameId) => Promise.resolve({ test: 'test' }));
    });

  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
    afterEach(() => {
      Game.getGame.restore();
      Game.getUserSessionAndSessionRequest.restore();
      Game.updateSessionRequestDoc.restore();
      compute.compute.restore();
      compute.generateSpinResponse.restore();
      Game.getUserSession.restore();
      gameSession.getUserSession.restore();
      whowapi.get.restore();
      gameSession.updateBetLines.restore();
      Game.getAllPlayableGames.restore();
    });

  /**
  * game function getting play table, error test case for invalid token
  */
    it('game function getting pay table, error test case for invalid token', (done) => {
      Game.getPaytble(testData.INVALID_USER_TOKEN, testData.VALID_GAME_ID)
    .then((data) => {
      /**
       * Expect that function is not resolved
       */
    })
    .catch((err) => {
      /**
       * Expect function resolve to error object
       */
      expect(err).to.have.property('error');
      done();
    });
    });


  /**
  * game function getting play table, error test case for null token
  */
    it('game function getting pay table, error test case for null token', (done) => {
      Game.getPaytble(null, testData.VALID_GAME_ID)
    .then((data) => {
      /**
       * Expect that function is not resolved
       */
    })
    .catch((err) => {
      /**
       * Expect function resolve to error object
       */
      expect(err).to.have.property('error');
      done();
    });
    });


  /**
  * game function getting play table, error test case for invalid game id
  */
    it('game function getting pay table, error test case for invalid game id', (done) => {
      Game.getPaytble(testData.VALID_USER_TOKEN, testData.INVALID_GAME_ID)
    .then((data) => {
      /**
       * Expect that function is not resolved
       */
    })
    .catch((err) => {
      /**
       * Expect function resolve to error object
       */
      expect(err).to.have.property('error');
      done();
    });
    });


  /**
  * game function getting play table, error test case for null game id
  */
    it('game function getting pay table, error test case for null game id', (done) => {
      Game.getPaytble(testData.VALID_USER_TOKEN, null)
    .then((data) => {
      /**
       * Expect that function is not resolved
       */
    })
    .catch((err) => {
      /**
       * Expect function resolve to error object
       */
      expect(err).to.have.property('error');
      done();
    });
    });


  /**
  * game function getting play table, successful test case
  */
    it('game function getting pay table, successful test case', (done) => {
      Game.getPaytble(testData.VALID_USER_TOKEN, testData.VALID_GAME_ID)
    .then((data) => {
      /**
       * Expect function resolve pay table data
       */
      expect(data).to.have.nested.property('2081cabb-d41e-460e-803c-f5a1d943ccba');
      expect(data).to.have.nested.property('2081cabb-d41e-460e-803c-f5a1d943ccba.3');
      expect(data).to.have.nested.property('2081cabb-d41e-460e-803c-f5a1d943ccba.3.amount');
      expect(data).to.have.nested.property('2081cabb-d41e-460e-803c-f5a1d943ccba.4');
      expect(data).to.have.nested.property('2081cabb-d41e-460e-803c-f5a1d943ccba.4.amount');
      expect(data).to.have.nested.property('2081cabb-d41e-460e-803c-f5a1d943ccba.5');
      expect(data).to.have.nested.property('2081cabb-d41e-460e-803c-f5a1d943ccba.5.amount');
      done();
    });
    });


  /**
  * game function getting user session, error test case for invalid token
  */
    it('game function getting user session, error test case for invalid token', (done) => {
      Game.getUserSession(testData.INVALID_USER_TOKEN, testData.VALID_GAME_ID)
    .then((data) => {
      /**
       * Expect that function is not resolved
       */
    })
    .catch((err) => {
      /**
       * Expect function resolve to error object
       */
      expect(err).to.have.property('error');
      done();
    });
    });


  /**
  * game function getting user session, error test case for null token
  */
    it('game function getting user session, error test case for invalid token', (done) => {
      Game.getUserSession(null, testData.VALID_GAME_ID)
    .then((data) => {
      /**
       * Expect that function is not resolved
       */
    })
    .catch((err) => {
      /**
       * Expect function resolve to error object
       */
      expect(err).to.have.property('error');
      done();
    });
    });


  /**
  * game function getting user session, error test case for invalid token
  */
    it('game function getting user session, error test case for invalid game id', (done) => {
      Game.getUserSession(testData.VALID_USER_TOKEN, testData.INVALID_GAME_ID)
    .then((data) => {
      /**
       * Expect that function is not resolved
       */
    })
    .catch((err) => {
      /**
       * Expect function resolve to error object
       */
      expect(err).to.have.property('error');
      done();
    });
    });


  /**
  * game function getting user session, error test case for null game id
  */
    it('game function getting user session, error test case for null game id', (done) => {
      Game.getUserSession(testData.VALID_USER_TOKEN, null)
    .then((data) => {
      /**
       * Expect that function is not resolved
       */
    })
    .catch((err) => {
      /**
       * Expect function resolve to error object
       */
      expect(err).to.have.property('error');
      done();
    });
    });


  /**
  * game function getting user session, successful test case
  */
    it('game function getting user session, successful test case', (done) => {
      Game.getUserSession(testData.VALID_USER_TOKEN, testData.VALID_GAME_ID)
    .then((data) => {
      /**
       * Expect that function to resolve valid session data
       */
      expect(data).to.have.nested.property('clientID');
      expect(data).to.have.nested.property('eventData');

      /**
       * Expect session data to be present in response
       */
      expect(data).to.have.nested.property('eventData.currentBet');
      expect(data).to.have.nested.property('eventData.currentLines');
      expect(data).to.have.nested.property('eventData.userDetails');
      expect(data).to.have.nested.property('eventData.freeSpinData');
      // expect(data).to.have.nested.property('eventData.userDetails.id');
      // expect(data).to.have.nested.property('eventData.userDetails.name');
      // expect(data).to.have.nested.property('eventData.userDetails.gender');
      // expect(data).to.have.nested.property('eventData.userDetails.locale');
      // expect(data).to.have.nested.property('eventData.userDetails.birthday');
      // expect(data).to.have.nested.property('eventData.userDetails.level');
      // expect(data).to.have.nested.property('eventData.userDetails.levelProgress');
      // expect(data).to.have.nested.property('eventData.userDetails.multiplier');
      // expect(data).to.have.nested.property('eventData.userDetails.id');

      /**
       * Expect there to be game settings
       */
      expect(data).to.have.nested.property('game');
      expect(data).to.have.nested.property('_updatedTimestamp');
      expect(data).to.have.nested.property('currentBet');
      expect(data).to.have.nested.property('game.betAmount');
      expect(data).to.have.nested.property('game.settings');
      expect(data).to.have.nested.property('game.settings.bets').to.have.length.gt(0);
      expect(data).to.have.nested.property('game.settings.maxBetMultiplier');
      expect(data).to.have.nested.property('game.settings.minBet');
      expect(data).to.have.nested.property('game.settings.minBetMultiplier');

      done();
    });
    });


  /**
  * game function getting playable games, successful test case
  */
    it('getting all playable games, successful test case', (done) => {
      Game.getAllPlayableGames()
    .then((data) => {
      /**
       * Expect that function is resolved to updated bet line number
       */
      expect(data).to.not.be.empty;
      done();
    });
    });
  });
