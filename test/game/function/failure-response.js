module.exports = {
  NO_GAME: JSON.parse('{"error":"could not find any game from given id"}'),
  WHOW_INVALID_TOKEN_RESPONSE: JSON.parse('{"status":404,"errors":["resource not found"],"payload":[]}'),
  UNRECOGNIZED_USER: JSON.parse('{"error": "could not recognize user"}'),
  GAME_SPIN_INVALID_CLIENT_ID: JSON.parse('{ "error": "invalid client" }'),
  GAME_SPIN_NO_GAME_CLICK: JSON.parse('{"error":"spin not allowed right now as game click did not happen"}'),
  GAME_SPIN_ACTIVE_SPIN: JSON.parse('{"error":"spin not allowed right now"}'),
};
