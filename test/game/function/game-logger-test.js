const chai = require('chai');
const sinon = require('sinon');
const Log = require('../../../util/logger');
const testData = require('../../test-data');
const failCases = require('./failure-response');
const successCases = require('./success-response');
const gameSession = require('../../../util/gamesession');
const Game = require('../../../helpers/gamefunction');
const logger = require('../../../helpers/firehose');

const expect = chai.expect;


/**
* Game logger related tests are performed
* logger logs the data to Elasticsearch using firehose
* Game logger tests include following tests
* - Test log data validity for properties that needs to be present
* - Win line/symbol counter test
* both negative and positive test cases are covered
*/
describe('GameLogTest', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
  beforeEach(() => {
    /**
    * Stub to get user session from database
    */
    sinon.stub(gameSession, 'getUserSession').callsFake((token, gameId) => Promise.resolve(successCases.VALID_USER_SESSION[successCases.VALID_USER_SESSION.sessionKey].value));

    /**
     * Stub to get game from given game id
     */
    sinon.stub(Game, 'getGame').callsFake(gameId => JSON.parse(testData.GAME_DATA));

    /**
     * Stub to export data to Elasticsearch using firehose
     */
    sinon.stub(logger, 'info').callsFake((result) => {});
  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    gameSession.getUserSession.restore();
    Game.getGame.restore();
    logger.info.restore();
  });


  /**
  * Error test case for undefined/null token
  */
  it('game log test for undefined/null token', (done) => {
    const result = JSON.parse(testData.VALID_RESULT_DATA_FOR_LOGGER);
    Log.logSpinResult(result, null, testData.VALID_GAME_ID);

    /**
     * Expect there to be properties which are not removed in case logging does not happen
     */
    expect(result).to.have.nested.property('game');
    expect(result).to.have.nested.property('jackpot');
    done();
  });


  /**
  * Error test case for undefined/null game id
  */
  it('game log test for undefined/null game id', (done) => {
    const result = JSON.parse(testData.VALID_RESULT_DATA_FOR_LOGGER);
    Log.logSpinResult(result, testData.VALID_GAME_ID, null);

    /**
     * Expect there to be properties which are not removed in case logging does not happen
     */
    expect(result).to.have.nested.property('game');
    expect(result).to.have.nested.property('jackpot');
    done();
  });

  /**
   * Successful test case when logs are shipped to Elasticsearch
   */
  it('game log test for successful game spin result', (done) => {
    const result = JSON.parse(testData.VALID_RESULT_DATA_FOR_LOGGER);
    Log.logSpinResult(result, testData.VALID_GAME_ID, testData.VALID_GAME_ID);

     /**
      * Expect there to be absence properties which are removed in case of successful logging
      */
    expect(result).to.not.have.nested.property('game');
    expect(result).to.not.have.nested.property('jackpot');


    /**
    * Expect not to have keys in view zone if logging is successful
    */
    const noofReels = parseInt(result.viewZone.viewZone.toString().charAt(1));
    for (let i = 0; i < noofReels; i++) {
      result.viewZone[`Reel${i + 1}`].forEach((line) => {
        expect(line).to.deep.include.property('symbolId');
      });
    }
    done();
  });
});
