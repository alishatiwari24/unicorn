const chai = require('chai');
const sinon = require('sinon');
const whowapi = require('../../../api/whowapi');
const testData = require('../../test-data');
const failCases = require('./failure-response');
const successCases = require('./success-response');
const whow = require('../../../api/whowapi');
const Api = require('../../../api/api');

const expect = chai.expect;

/**
* Whow api call resolution related tests are performed
*/
describe('WHOWApiCall', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
  beforeEach(() => {
    /**
    * stub to whow api call
    */
    sinon.stub(whow.api, 'call').callsFake((method, endpoint, [], callback) => {
      callback('{"data":"data"}', '');
    });
  });

  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    whow.api.call.restore();
  });


  /**
  * Error test case for empty token when calling whow api get method
  */
  it('whow GET api call empty token', (done) => {
    whow.get('')
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Error test case for null token when calling whow api get method
  */
  it('whow GET api call null token', (done) => {
    whow.get(null)
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Successful test case when calling whow api get method
  */
  it('whow GET api call valid token', (done) => {
    whow.get(testData.VALID_USER_TOKEN)
    .then((result) => {
      expect(result).to.have.nested.property('data');
      done();
    });
  });


  /**
  * Error test case for empty token when calling whow api wallet method
  */
  it('whow WALLET api call invalid token', (done) => {
    whow.wallet('')
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Error test case for null token when calling whow api wallet method
  */
  it('whow WALLET api call invalid token', (done) => {
    whow.wallet(null)
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Successful test case when calling whow api wallet method
  */
  it('whow WALLET api call valid token', (done) => {
    whow.wallet(testData.VALID_USER_TOKEN)
    .then((result) => {
      expect(result).to.have.nested.property('data');
      done();
    });
  });


  /**
  * Error test case for empty token when calling whow api play method
  */
  it('whow PLAY api call empty token', (done) => {
    whow.wallet('', [])
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Error test case for null token when calling whow api play method
  */
  it('whow PLAY api call null token', (done) => {
    whow.play(null, [])
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Error test case for empty payload when calling whow api play method
  */
  it('whow PLAY api call empty payload', (done) => {
    whow.play(testData.VALID_USER_TOKEN, '')
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Error test case for null payload when calling whow api play method
  */
  it('whow PLAY api call null payload', (done) => {
    whow.play(testData.VALID_USER_TOKEN, null)
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Successful test case when calling whow api play method
  */
  it('whow PLAY api call valid token and valid payload', (done) => {
    whow.play(testData.VALID_USER_TOKEN, [])
    .then((result) => {
      result = JSON.parse(result);
      expect(result).to.have.nested.property('data');
      done();
    });
  });


  /**
  * Error test case for empty token when calling whow api bet method
  */
  it('whow BET api call empty token', (done) => {
    whow.bet('', [])
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Error test case for null token when calling whow api bet method
  */
  it('whow BET api call null token', (done) => {
    whow.bet(null, [])
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Error test case for empty payload when calling whow api bet method
  */
  it('whow BET api call empty payload', (done) => {
    whow.bet(testData.VALID_USER_TOKEN, '')
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Error test case for null payload when calling whow api bet method
  */
  it('whow BET api call null payload', (done) => {
    whow.bet(testData.VALID_USER_TOKEN, null)
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Successful test case when calling whow api bet method
  */
  it('whow BET api call valid token and valid payload', (done) => {
    whow.bet(testData.VALID_USER_TOKEN, [])
    .then((result) => {
      result = JSON.parse(result);
      expect(result).to.have.nested.property('data');
      done();
    });
  });


  /**
  * Error test case for empty token when calling whow api close method
  */
  it('whow CLOSE api call empty token', (done) => {
    whow.close('', [])
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Error test case for null token when calling whow api close method
  */
  it('whow CLOSE api call null token', (done) => {
    whow.close(null, [])
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Error test case for empty payload when calling whow api close method
  */
  it('whow CLOSE api call empty payload', (done) => {
    whow.close(testData.VALID_USER_TOKEN, '')
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Error test case for null payload when calling whow api close method
  */
  it('whow CLOSE api call null payload', (done) => {
    whow.close(testData.VALID_USER_TOKEN, null)
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Successful test case when calling whow api close method
  */
  it('whow CLOSE api call valid token and valid payload', (done) => {
    whow.close(testData.VALID_USER_TOKEN, [])
    .then((result) => {
      result = JSON.parse(result);
      expect(result).to.have.nested.property('data');
      done();
    });
  });


  /**
  * Error test case for empty token when calling whow api cancel method
  */
  it('whow CANCEL api call empty token', (done) => {
    whow.cancel('', [])
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Error test case for null token when calling whow api cancel method
  */
  it('whow CANCEL api call null token', (done) => {
    whow.cancel(null, [])
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Error test case for empty payload when calling whow api cancel method
  */
  it('whow CANCEL api call empty payload', (done) => {
    whow.cancel(testData.VALID_USER_TOKEN, '')
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Error test case for null payload when calling whow api cancel method
  */
  it('whow CANCEL api call null payload', (done) => {
    whow.cancel(testData.VALID_USER_TOKEN, null)
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Successful test case when calling whow api cancel method
  */
  it('whow CANCEL api call valid token and valid payload', (done) => {
    whow.cancel(testData.VALID_USER_TOKEN, [])
    .then((result) => {
      result = JSON.parse(result);
      expect(result).to.have.nested.property('data');
      done();
    });
  });


  /**
  * Error test case for empty token when calling whow api triggerEvents method
  */
  it('whow TRIGER EVENTS api call empty token', (done) => {
    whow.triggerEvents('', [])
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Error test case for null token when calling whow api triggerEvents method
  */
  it('whow TRIGER EVENTS api call null token', (done) => {
    whow.triggerEvents(null, [])
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Error test case for empty payload when calling whow api triggerEvents method
  */
  it('whow TRIGER EVENTS api call empty payload', (done) => {
    whow.triggerEvents(testData.VALID_USER_TOKEN, '')
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Error test case for null payload when calling whow api triggerEvents method
  */
  it('whow TRIGER EVENTS api call null payload', (done) => {
    whow.triggerEvents(testData.VALID_USER_TOKEN, null)
    .then((result) => {
      expect(result).to.deep.equal({});
      done();
    });
  });


  /**
  * Successful test case when calling whow api triggerEvents method
  */
  it('whow TRIGER EVENTS api call valid token and valid payload', (done) => {
    whow.triggerEvents(testData.VALID_USER_TOKEN, [])
    .then((result) => {
      expect(result).to.have.nested.property('data');
      done();
    });
  });
});
