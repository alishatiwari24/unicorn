const chai = require('chai');
const sinon = require('sinon');
const Game = require('../../../helpers/gamefunction');
const testData = require('../../test-data');
const failCases = require('./failure-response');
const successCases = require('./success-response');
const compute = require('../../../util/compute');

const expect = chai.expect;


/**
* Game click related validations
* includes error test cases such as
* - Invalid game id
* - Invalid user token
* and success case where game click happens successfully
*/
describe('GameSpinTest', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
  beforeEach(() => {
    /**
    * stub to fetch user session data from database
    */
    sinon.stub(Game, 'getUserSessionAndSessionRequest').callsFake((token, gameId) => new Promise((resolve, reject) => {
      if (gameId === testData.VALID_GAME_ID && token === testData.VALID_USER_TOKEN) {
        resolve(successCases.VALID_USER_SESSION);
      } else if (gameId === testData.ACTIVE_SPIN_CLICK_VALID_GAME_ID) {
        resolve(successCases.VALID_USER_SESSION_ACTIVE_SPIN);
      } else {
        resolve(undefined);
      }
    }));

    /**
    * stub to update user session
    */
    sinon.stub(Game, 'updateSessionRequestDoc').callsFake((token, gameId) => new Promise((resolve, reject) => {
      if (gameId === testData.VALID_GAME_ID && token === testData.VALID_USER_TOKEN) {
        resolve(successCases.VALID_USER_SESSION);
      } else if (gameId === testData.NO_GAME_CLICK_VALID_GAME_ID) {
        resolve({ undefined });
      } else {
        resolve({ undefined });
      }
    }));

    /**
    * stub to get a valid game data from game id
    */
    sinon.stub(Game, 'getGame').callsFake((gameId) => {
      if (gameId === testData.VALID_GAME_ID || gameId === testData.NO_GAME_CLICK_VALID_GAME_ID || gameId === testData.ACTIVE_SPIN_CLICK_VALID_GAME_ID) {
        return JSON.parse(testData.GAME_DATA);
      }
      return undefined;
    });


    /**
    * stub for successful game spin
    * it resolves a valid game spin response promise
    */
    sinon.stub(compute, 'generateSpinResponse').callsFake(gameId => Promise.resolve(successCases.GAME_SPIN_SUCCESS));
  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    Game.getUserSessionAndSessionRequest.restore();
    Game.updateSessionRequestDoc.restore();
    Game.getGame.restore();
    compute.generateSpinResponse.restore();
  });


  /**
  * Error test case for invalid game ID
  */
  it('game spin invalid game id', (done) => {
    Game.handleSpin(testData.INVALID_GAME_ID, testData.VALID_USER_TOKEN, testData.VALID_CLIENT_ID)
    .then((data) => {
      expect(data).to.deep.include(failCases.NO_GAME);
    })
    .then(() => done(), done);
  });


  /**
  * Error test case for null/undefined game ID
  */
  it('game spin null/undefined game id', (done) => {
    Game.handleSpin(null, testData.VALID_USER_TOKEN, testData.VALID_CLIENT_ID)
    .then((data) => {
      expect(data).to.deep.include(failCases.NO_GAME);
    })
    .then(() => done(), done);
  });


  /**
  * Error test case for invalid client ID
  */
  it('game spin invalid client id', (done) => {
    Game.handleSpin(testData.VALID_GAME_ID, testData.VALID_USER_TOKEN, testData.INVALID_CLIENT_ID)
    .then((data) => {
      expect(data).to.deep.include(failCases.GAME_SPIN_INVALID_CLIENT_ID);
    })
    .then(() => done(), done);
  });


  /**
  * Error test case for null/undefined client ID
  */
  it('game spin null/undefined client id', (done) => {
    Game.handleSpin(testData.VALID_GAME_ID, testData.VALID_USER_TOKEN, null)
    .then((data) => {
      expect(data).to.deep.include(failCases.GAME_SPIN_INVALID_CLIENT_ID);
    })
    .then(() => done(), done);
  });

  /**
  * Error test case for when game click did not happend and spin is attempted
  */
  it('game spin when game click did not happen', (done) => {
    Game.handleSpin(testData.NO_GAME_CLICK_VALID_GAME_ID, testData.VALID_USER_TOKEN, testData.VALID_CLIENT_ID)
    .then((data) => {
      expect(data).to.deep.include(failCases.GAME_SPIN_NO_GAME_CLICK);
    })
    .then(() => done(), done);
  });


  /**
  * Error test case for when a spin is going on and another spin is attempted
  */
  it('game spin when another spin is active', (done) => {
    Game.handleSpin(testData.ACTIVE_SPIN_CLICK_VALID_GAME_ID, testData.VALID_USER_TOKEN, testData.VALID_CLIENT_ID)
    .then((data) => {
      expect(data).to.deep.include(failCases.GAME_SPIN_ACTIVE_SPIN);
    })
    .then(() => done(), done);
  });


  /**
  * Successfull test case for a spin to happen
  */
  it('game spin successfull spin', (done) => {
    Game.handleSpin(testData.VALID_GAME_ID, testData.VALID_USER_TOKEN, testData.VALID_CLIENT_ID)
    .then((data) => {
      /**
      * Check if required properties are present in spin response
      */
      expect(data).to.have.nested.property('data.viewZone');
      expect(data).to.have.nested.property('data.wins');
      expect(data).to.have.nested.property('data.freeSpinData');
      expect(data).to.have.nested.property('data.levelData');
      expect(data).to.have.nested.property('data.wallet');
      expect(data).to.have.nested.property('data.game');
      expect(data).to.have.nested.property('data.jackpot');

      /**
      * Check if required number of reels are present
      */
      expect(parseInt(data.data.viewZone.viewZone.toString().charAt(1))).to.be.eql(Object.keys(data.data.viewZone).length - 1);
    })
    .then(() => done(), done);
  });
});
