const chai = require('chai');
const sinon = require('sinon');
const Game = require('../../../helpers/gamefunction');
const whowapi = require('../../../api/whowapi');
const testData = require('../../test-data');
const failCases = require('./failure-response');
const successCases = require('./success-response');
const compute = require('../../../util/compute');

const expect = chai.expect;


/**
* Game spin releted tests are performed
* includes error test cases such as
* - Invalid game id
* - Invalid user token
* - Invalid clientID
* and success case where game spin happens successfully
*/
describe('GameClickTest', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
  beforeEach(() => {
    /**
    * stub to mock whow api to get user details
    */
    sinon.stub(whowapi, 'get').callsFake(token => new Promise((resolve, reject) => {
      if (token == testData.VALID_USER_TOKEN) {
        resolve(successCases.WHOW_VALID_TOKEN_RESPONSE);
      } else {
        resolve(failCases.WHOW_INVALID_TOKEN_RESPONSE);
      }
    }));

    /**
    * stub to mock getting a game
    */
    sinon.stub(Game, 'getGame').callsFake((gameId) => {
      if (gameId === testData.VALID_GAME_ID) {
        return JSON.parse(testData.GAME_DATA);
      }
      return undefined;
    });

    /**
    * stub to mock geeting valid game click data
    */
    sinon.stub(compute, 'generateGameClickResponse').callsFake((obj, token, gameId) => new Promise((resolve, reject) => {
      if (gameId === testData.VALID_GAME_ID) {
        resolve(testData.VALID_GAME_CLICK_REPONSE);
      } else {
        reject();
      }
    }));
  });

  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    whowapi.get.restore();
    Game.getGame.restore();
    compute.generateGameClickResponse.restore();
  });

  /**
  * Error test case for invalid game id
  */
  it('game click invalid game id', () => {
    Game.handleGameClick(testData.INVALID_GAME_ID, testData.VALID_USER_TOKEN)
    .then((data) => {
      expect(data).to.deep.include(failCases.NO_GAME);
    });
  });


  /**
  * Error test case for null game id
  */
  it('game click undefined/null game id', () => {
    Game.handleGameClick(null, testData.VALID_USER_TOKEN)
    .then((data) => {
      expect(data).to.deep.include(failCases.NO_GAME);
    });
  });


  /**
  * Error test case for undefined/null user token
  */
  it('game click undefined/null user token', () => {
    Game.handleGameClick(testData.VALID_GAME_ID, null)
    .then((data) => {
      expect(data).to.deep.include(failCases.UNRECOGNIZED_USER);
    });
  });


  /**
  * successfull test case of game click
  */
  it('game click success', (done) => {
    Game.handleGameClick(testData.VALID_GAME_ID, testData.VALID_USER_TOKEN)
    .then((data) => {
      /**
      * Check if required properties are present in game click response
      */
      expect(data).to.deep.include.keys('game');
      expect(data).to.have.nested.property('game.gameId');
      expect(data).to.have.nested.property('game.viewZone');
      expect(data).to.have.nested.property('game.reels');
      expect(data).to.have.nested.property('game.payArray');
      expect(data).to.have.nested.property('game.payTable');
      expect(data).to.have.nested.property('game.symbols');
      expect(data).to.have.nested.property('game.reels');


      /**
      * Check if required number of reels are present
      */
      expect(data.game.reels.noofReels).to.be.eql(Object.keys(data.game.reels).length - 1);

      /**
      * Check if required number of symbols are present as per reel size
      */
      const reels = Object.keys(data.game.reels);
      const column = parseInt(data.game.viewZone.toString().charAt(0));
      for (let i = 1; i < reels.length; i++) {
        expect(data.game.reels[reels[i]].length).to.be.eql(column);
      }

      done();
    });
  });
});
