const sinon = require('sinon');
const chai = require('chai');
const testData = require('../../test-data');
const Game = require('../../../helpers/gamefunction');
const gameSession = require('../../../util/gamesession');
const responseDispatcher = require('../../../util/responsedispatcher');
const logger = require('../../../util/logger');
const successResponse = require('./success-response');
const _ = require('lodash');

const expect = chai.expect;

const gameController = require('../../../controller/game');

/**
* Game controler releted tests
* test cases related to game click, various game events, game spin are performed
* game controller is the entry point of various game routes
*/
describe('GameControllerTest', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
  beforeEach(() => {
    /**
    * stub to mock handling game click event
    */
    sinon.stub(Game, 'handleGameClick').callsFake((gameId, token) => {
      if (gameId == testData.VALID_GAME_ID && token == testData.VALID_USER_TOKEN) {
        return Promise.resolve({ userDetails: 'userDetails' });
      }
      return Promise.resolve({});
    });

    /**
    * stub to mock sending api response
    */
    sinon.stub(responseDispatcher, 'dispatchError').callsFake((res, data) => {});

    /**
    * stub to mock sending error response
    */
    sinon.stub(responseDispatcher, 'dispatch').callsFake((res, data) => {});

    /**
    * stub to get a valid game data from game id
    */
    sinon.stub(Game, 'getGame').callsFake((gameId) => {
      if (gameId === testData.VALID_GAME_ID || gameId === testData.NO_GAME_CLICK_VALID_GAME_ID || gameId === testData.ACTIVE_SPIN_CLICK_VALID_GAME_ID) {
        return JSON.parse(testData.GAME_DATA);
      }
      return undefined;
    });

    /**
     * stub to mock game click database operation
     */
    sinon.stub(gameSession, 'gameClick').callsFake((gameId, token, clientID, data, gameData) => {});

    /**
     * stub to mock game spin database operation
     */
    sinon.stub(gameSession, 'gameSpin').callsFake((gameId, token, clientID, data, gameData) => {});

    /**
     * stub to mock sping event on game
     */
    sinon.stub(Game, 'handleSpin').callsFake((gameId, token, clientId) => {
      if (gameId == testData.VALID_GAME_ID && token == testData.VALID_USER_TOKEN && clientId == testData.VALID_CLIENT_ID) {
        return Promise.resolve({});
      }
      return Promise.resolve({ error: 'error' });
    });

    /**
     * stub result logging
     */
    sinon.stub(logger, 'logSpinResult').callsFake((logResult, token, gameId) => {});

    /**
     * stub to mock getting user game session
     */
    sinon.stub(gameSession, 'getUserSession').callsFake((token, gameId) => {
      if (token == testData.VALID_USER_TOKEN && gameId == testData.VALID_GAME_ID) {
        const session = successResponse.VALID_USER_SESSION[successResponse.VALID_USER_SESSION.sessionKey].value;
        return Promise.resolve(session);
      }
      return Promise.reject({});
    });

    /**
     * stub to mock getting game paytable
     */
    sinon.stub(Game, 'getPaytble').callsFake((token, gameId) => {
      if (token == testData.VALID_USER_TOKEN && gameId == testData.VALID_GAME_ID) {
        return Promise.resolve({});
      }
      return Promise.reject({});
    });

    /**
     * stub to mock updating user's current bet
     */
    sinon.stub(gameSession, 'updateUserCurrentBet').callsFake((id, gameId, currentBet) => {});

    /**
     * stub to mock increasing bet line
     */
    sinon.stub(Game, 'increaseBetLine').callsFake((token, gameId) => {
      if (token == testData.VALID_USER_TOKEN && gameId == testData.VALID_GAME_ID) {
        return Promise.resolve({});
      }
      return Promise.reject({});
    });

    /**
     * stub to mock decreasing bet line
     */
    sinon.stub(Game, 'decreaseBetLine').callsFake((token, gameId) => {
      if (token == testData.VALID_USER_TOKEN && gameId == testData.VALID_GAME_ID) {
        return Promise.resolve({});
      }
      return Promise.reject({});
    });

    /**
     * stub to mock setting bet line
     */
    sinon.stub(Game, 'setBetLine').callsFake((token, gameId) => {
      if (token == testData.VALID_USER_TOKEN && gameId == testData.VALID_GAME_ID) {
        return Promise.resolve({});
      }
      return Promise.reject({});
    });

    /**
     * stub to mock getting playable games
     */
    sinon.stub(Game, 'getAllPlayableGames').callsFake(() => Promise.resolve({}));
  });

  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    Game.handleGameClick.restore();
    responseDispatcher.dispatch.restore();
    responseDispatcher.dispatchError.restore();
    Game.getGame.restore();
    gameSession.gameClick.restore();
    Game.handleSpin.restore();
    logger.logSpinResult.restore();
    gameSession.gameSpin.restore();
    gameSession.getUserSession.restore();
    Game.getPaytble.restore();
    gameSession.updateUserCurrentBet.restore();
    Game.increaseBetLine.restore();
    Game.decreaseBetLine.restore();
    Game.setBetLine.restore();
    Game.getAllPlayableGames.restore();
  });


  /**
   * Error test case for invalid game click, with no request body parameters
   */
  it('game click error test case incase of in invalid game click, no required parameters', (done) => {
    let req = {},
      res = {};

    req.body = {};

    gameController.gameClick(req, res);

    setTimeout(() => {
      /**
      * Expect that game click user session database is not perfromed
      * in case of invalid game click response
      */
      expect(gameSession.gameClick.calledOnce).to.not.be.eql(true);

      /**
      * Expect that response is sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.be.eql(true);

      done();
    }, 10);
  });


  /**
   * Error test case for invalid game click, with null game id
   */
  it('game click error test case incase of in invalid game click, null game id', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = null;
    req.body.token = testData.VALID_USER_TOKEN;

    gameController.gameClick(req, res);

    setTimeout(() => {
      /**
      * Expect that game click user session database is not perfromed
      * in case of invalid game click response
      */
      expect(gameSession.gameClick.calledOnce).to.not.be.eql(true);

      /**
      * Expect that response is sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.be.eql(true);

      done();
    }, 10);
  });


  /**
   * Error test case for invalid game click, with null user token
   */
  it('game click error test case incase of in invalid game click, null user token', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = null;

    gameController.gameClick(req, res);

    setTimeout(() => {
      /**
      * Expect that game click user session database is not perfromed
      * in case of invalid game click response
      */
      expect(gameSession.gameClick.calledOnce).to.not.be.eql(true);

      /**
      * Expect that response is sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.be.eql(true);

      done();
    }, 10);
  });


  /**
   * Error test case for invalid game click, with wrong game id
   */
  it('game click error test case incase of in invalid game id', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;

    gameController.gameClick(req, res);

    setTimeout(() => {
      /**
      * Expect that game click user session database is not perfromed
      * in case of invalid game click response
      */
      expect(gameSession.gameClick.calledOnce).to.not.be.eql(true);

      /**
      * Expect that response is sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.be.eql(true);

      done();
    }, 10);
  });


  /**
   * Error test case for invalid game click, with wrong user token
   */
  it('game click error test case incase of in invalid user token', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.INVALID_USER_TOKEN;

    gameController.gameClick(req, res);

    setTimeout(() => {
      /**
      * Expect that game click user session database is not perfromed
      * in case of invalid game click response
      */
      expect(gameSession.gameClick.calledOnce).to.not.be.eql(true);

      /**
      * Expect that response is sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.be.eql(true);

      done();
    }, 10);
  });


  /**
   * Successful test case for valid game click
   */
  it('game click Successful test case incase of in valid game click ', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;

    gameController.gameClick(req, res);

    setTimeout(() => {
      /**
      * Expect that game click user session database is not perfromed
      * in case of invalid game click response
      */
      expect(gameSession.gameClick.calledOnce).to.be.eql(true);

      /**
      * Expect that response is sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.be.eql(true);

      done();
    }, 10);
  });


  /**
   * Error test case for invalid game spin response for null game id
   */
  it('game spin error test case incase of null game id ', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = null;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.spin(req, res);

    setTimeout(() => {
      /**
       * Expct that user game session is not updated
       */
      expect(gameSession.gameSpin.calledOnce).to.not.be.eql(true);

      /**
       * Expect that spin result is not loggged
       */
      expect(logger.logSpinResult.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);

      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      done();
    }, 10);
  });


  /**
   * Error test case for invalid game spin response for null user token
   */
  it('game spin error test case incase of null user token', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = null;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.spin(req, res);

    setTimeout(() => {
      /**
       * Expct that user game session is not updated
       */
      expect(gameSession.gameSpin.calledOnce).to.not.be.eql(true);

      /**
       * Expect that spin result is not loggged
       */
      expect(logger.logSpinResult.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);

      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      done();
    }, 10);
  });


  /**
   * Error test case for invalid game spin response for null client id
   */
  it('game spin error test case incase of null client id', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = null;

    gameController.spin(req, res);

    setTimeout(() => {
      /**
       * Expct that user game session is not updated
       */
      expect(gameSession.gameSpin.calledOnce).to.not.be.eql(true);

      /**
       * Expect that spin result is not loggged
       */
      expect(logger.logSpinResult.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);

      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      done();
    }, 10);
  });


  /**
   * Error test case for invalid game spin response for all null body parameters
   */
  it('game spin error test case incase of all null body parameters', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = null;
    req.body.token = null;
    req.body.clientID = null;

    gameController.spin(req, res);

    setTimeout(() => {
      /**
       * Expct that user game session is not updated
       */
      expect(gameSession.gameSpin.calledOnce).to.not.be.eql(true);

      /**
       * Expect that spin result is not loggged
       */
      expect(logger.logSpinResult.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);

      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      done();
    }, 10);
  });


  /**
   * Error test case for invalid game spin response for invalid game id
   */
  it('game spin error test case incase of invalid game id ', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.spin(req, res);

    setTimeout(() => {
      /**
       * Expct that user game session is not updated
       */
      expect(gameSession.gameSpin.calledOnce).to.not.be.eql(true);

      /**
       * Expect that spin result is not loggged
       */
      expect(logger.logSpinResult.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);

      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      done();
    }, 10);
  });


  /**
   * Error test case for invalid game spin response for invalid user token
   */
  it('game spin error test case incase of invalid user token ', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.INVALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.spin(req, res);

    setTimeout(() => {
      /**
       * Expct that user game session is not updated
       */
      expect(gameSession.gameSpin.calledOnce).to.not.be.eql(true);

      /**
       * Expect that spin result is not loggged
       */
      expect(logger.logSpinResult.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);

      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      done();
    }, 10);
  });


  /**
   * Error test case for invalid game spin response for invalid client id
   */
  it('game spin error test case incase of invalid client id ', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.VLID_USER_TOKEN;
    req.body.clientID = testData.INVALID_CLIENT_ID;

    gameController.spin(req, res);

    setTimeout(() => {
      /**
       * Expct that user game session is not updated
       */
      expect(gameSession.gameSpin.calledOnce).to.not.be.eql(true);

      /**
       * Expect that spin result is not loggged
       */
      expect(logger.logSpinResult.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);

      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      done();
    }, 10);
  });


  /**
   * Error test case for invalid game spin response for all invalid body parameters
   */
  it('game spin error test case incase of all invalid body parameters', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.INVALID_USER_TOKEN;
    req.body.clientID = testData.INVALID_CLIENT_ID;

    gameController.spin(req, res);

    setTimeout(() => {
      /**
       * Expct that user game session is not updated
       */
      expect(gameSession.gameSpin.calledOnce).to.not.be.eql(true);

      /**
       * Expect that spin result is not loggged
       */
      expect(logger.logSpinResult.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);

      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      done();
    }, 10);
  });


  /**
   * Successfult test case for valid game spin response
   */
  it('game spin successfult test case ', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.spin(req, res);

    setTimeout(() => {
      /**
       * Expct that user game session is not updated
       */
      expect(gameSession.gameSpin.calledOnce).to.be.eql(true);

      /**
       * Expect that spin result is not loggged
       */
      expect(logger.logSpinResult.calledOnce).to.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.be.eql(true);

      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.not.be.eql(true);

      done();
    }, 10);
  });


  /**
   * Error test case for invalid game session response, for null game id
   */
  it('game session error test case for null game id ', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = null;
    req.body.token = testData.VALID_USER_TOKEN;

    gameController.getSession(req, res);

    setTimeout(() => {
      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid game session response, incase of null user token
   */
  it('game session error test case for null user token', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = null;

    gameController.getSession(req, res);

    setTimeout(() => {
      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid game session response, incase of all null body parameters
   */
  it('game session error test case for null body parameters', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = null;
    req.body.token = null;

    gameController.getSession(req, res);

    setTimeout(() => {
      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid game session response
   */
  it('game session error test case for invalid game id ', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;

    gameController.getSession(req, res);

    setTimeout(() => {
      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid game session response
   */
  it('game session error test case for invalid user token ', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.INVALID_USER_TOKEN;

    gameController.getSession(req, res);

    setTimeout(() => {
      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid game session response, incase of invalid body parameters
   */
  it('game session error test case for all invalid body parameters', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.INVALID_USER_TOKEN;

    gameController.getSession(req, res);

    setTimeout(() => {
      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Successful test case for valid game session response
   */
  it('game session successful test case', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;

    gameController.getSession(req, res);

    setTimeout(() => {
      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid game paytable response, null game id
   */
  it('game paytable error test case for null game id ', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = null;
    req.body.token = testData.VALID_USER_TOKEN;

    gameController.getSession(req, res);

    setTimeout(() => {
      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid game paytable response, incase of null user token
   */
  it('game paytable error test case for invalid game id ', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = null;

    gameController.getSession(req, res);

    setTimeout(() => {
      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid game paytable response, incase of all null body parameters
   */
  it('game paytable error test case for all null body parameters ', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = null;
    req.body.token = null;

    gameController.getSession(req, res);

    setTimeout(() => {
      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid game paytable response
   */
  it('game paytable error test case for invalid game id ', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;

    gameController.getSession(req, res);

    setTimeout(() => {
      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid game paytable response
   */
  it('game paytable error test case for invalid user token ', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.INVALID_USER_TOKEN;

    gameController.getSession(req, res);

    setTimeout(() => {
      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid game paytable response, incase of all invalid body parameters
   */
  it('game paytable error test case for all invalid body parameters', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.INVALID_USER_TOKEN;

    gameController.getSession(req, res);

    setTimeout(() => {
      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Successful test case for valid game paytable response
   */
  it('game paytable successful test case', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;

    gameController.getSession(req, res);

    setTimeout(() => {
      /**
      * Expect that error response is sent
      */
      expect(responseDispatcher.dispatchError.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid increase bet response, null game id
   */
  it('increase bet error test case for null gameId', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = null;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.increaseBet(req, res);

    setTimeout(() => {
      /**
       * Expect that user session update, updating bet is not called
       */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid increase bet response, null user token
   */
  it('increase bet error test case for null user token', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = null;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.increaseBet(req, res);

    setTimeout(() => {
      /**
       * Expect that user session update, updating bet is not called
       */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid increase bet response, all null body parameters
   */
  it('increase bet error test case for all null body parameters', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = null;
    req.body.token = null;
    req.body.clientID = null;

    gameController.increaseBet(req, res);

    setTimeout(() => {
      /**
       * Expect that user session update, updating bet is not called
       */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid increase bet response
   */
  it('increase bet error test case for invalid gameId', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.increaseBet(req, res);

    setTimeout(() => {
      /**
       * Expect that user session update, updating bet is not called
       */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid increase bet response
   */
  it('increase bet error test case for invalid user token', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.INVALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.increaseBet(req, res);

    setTimeout(() => {
      /**
       * Expect that user session update, updating bet is not called
       */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid increase bet response, all invalid body parameters
   */
  it('increase bet error test case for all invalid body parameters', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.INVALID_USER_TOKEN;
    req.body.clientID = testData.INVALID_CLIENT_ID;

    gameController.increaseBet(req, res);

    setTimeout(() => {
      /**
       * Expect that user session update, updating bet is not called
       */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Successfult test case for valid increase bet response
   */
  it('increase bet successful test case', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.increaseBet(req, res);

    setTimeout(() => {
      /**
     * Expect that user session update, updating bet is called
     */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.be.eql(true);

      /**
      * Expect that successful response is sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid decrease bet response, null game id
   */
  it('decrease bet error test case for null gameId', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = null;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.decreaseBet(req, res);

    setTimeout(() => {
        /**
         * Expect that user session update, updating bet is not called
         */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid decrease bet response, null user token
   */
  it('decrease bet error test case for null user token', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = null;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.decreaseBet(req, res);

    setTimeout(() => {
        /**
         * Expect that user session update, updating bet is not called
         */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid decrease bet response, all null body parameters
   */
  it('decrease bet error test case for all null body parameters', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = null;
    req.body.token = null;
    req.body.clientID = null;

    gameController.decreaseBet(req, res);

    setTimeout(() => {
        /**
         * Expect that user session update, updating bet is not called
         */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid decrease bet response
   */
  it('decrease bet error test case for invalid gameId', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.decreaseBet(req, res);

    setTimeout(() => {
        /**
         * Expect that user session update, updating bet is not called
         */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid decrease bet response
   */
  it('decrease bet error test case for invalid user token', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.INVALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.decreaseBet(req, res);

    setTimeout(() => {
      /**
       * Expect that user session update, updating bet is not called
       */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid decrease bet response, all invalid body parameters
   */
  it('decrease bet error test case for all invalid body parameters', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.INVALID_USER_TOKEN;
    req.body.clientID = testData.INVALID_CLIENT_ID;

    gameController.decreaseBet(req, res);

    setTimeout(() => {
      /**
       * Expect that user session update, updating bet is not called
       */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Successfult test case for valid decrease bet response
   */
  it('decrease bet successful test case', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.decreaseBet(req, res);

    setTimeout(() => {
      /**
       * Expect that user session update, updating bet is called
       */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.be.eql(true);

      /**
      * Expect that successful response is sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid max bet response, null game id
   */
  it('max bet error test case for null gameId', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = null;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.maxBet(req, res);

    setTimeout(() => {
        /**
         * Expect that user session update, updating bet is not called
         */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid max bet response, null user token
   */
  it('max bet error test case for null user token', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = null;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.maxBet(req, res);

    setTimeout(() => {
        /**
         * Expect that user session update, updating bet is not called
         */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid max bet response, all null body parameters
   */
  it('max bet error test case for all null body parameters', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = null;
    req.body.token = null;
    req.body.clientID = null;

    gameController.maxBet(req, res);

    setTimeout(() => {
        /**
         * Expect that user session update, updating bet is not called
         */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid max bet response
   */
  it('max bet error test case for invalid gameId', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.maxBet(req, res);

    setTimeout(() => {
        /**
         * Expect that user session update, updating bet is not called
         */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid max bet response
   */
  it('max bet error test case for invalid user token', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.INVALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.maxBet(req, res);

    setTimeout(() => {
      /**
       * Expect that user session update, updating bet is not called
       */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid max bet response, all invalid body parameters
   */
  it('max bet error test case for all invalid body parameters', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.INVALID_USER_TOKEN;
    req.body.clientID = testData.INVALID_CLIENT_ID;

    gameController.maxBet(req, res);

    setTimeout(() => {
      /**
       * Expect that user session update, updating bet is not called
       */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.not.be.eql(true);

      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Successfult test case for valid max bet response
   */
  it('max bet successful test case', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.maxBet(req, res);

    setTimeout(() => {
      /**
       * Expect that user session update, updating bet is called
       */
      expect(gameSession.updateUserCurrentBet.calledOnce).to.be.eql(false);

      /**
      * Expect that successful response is sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid increase bet line response, null game id
   */
  it('increase bet line error test case for null game id', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = null;
    req.body.token = testData.VALID_USER_TOKEN;

    gameController.increaseBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid increase bet line response, null user token
   */
  it('increase bet line error test case for null user token', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = null;

    gameController.increaseBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid increase bet line response, all null body parameters
   */
  it('increase bet line error test case for all null body parameters', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = null;
    req.body.token = null;

    gameController.increaseBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid increase bet line response
   */
  it('increase bet line error test case for invalid game id', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;

    gameController.increaseBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid increase bet line response
   */
  it('increase bet line error test case for invalid user token', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.INVALID_USER_TOKEN;

    gameController.increaseBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid increase bet line response, all invalid body parameters
   */
  it('increase bet line error test case for all invalid body parameters', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.INVALID_USER_TOKEN;

    gameController.increaseBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Successful test case for valid increase bet line response
   */
  it('increase bet line successful test case', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.increaseBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid decrease bet line response, null game id
   */
  it('decrease bet line error test case for null game id', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = null;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.decreaseBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid decrease bet line response, null user token
   */
  it('decrease bet line error test case for null user token', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = null;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.decreaseBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid decrease bet line response, all null body parameters
   */
  it('decrease bet line error test case for all null body parameters', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = null;
    req.body.token = null;
    req.body.clientID = null;

    gameController.decreaseBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid decrease bet line response
   */
  it('decrease bet line error test case for invalid game id', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.decreaseBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid decrease bet line response
   */
  it('decrease bet line error test case for invalid user token', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.INVALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.decreaseBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid decrease bet line response, all invalid body parameters
   */
  it('decrease bet line error test case for all invalid body parameters', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.INVALID_USER_TOKEN;
    req.body.clientID = testData.INVALID_CLIENT_ID;

    gameController.decreaseBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Successful test case for valid decrease bet line response
   */
  it('decrease bet line successful test case', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.decreaseBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid set bet line response, null game id
   */
  it('set bet line error test case for invalid game id', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = null;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.setBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid set bet line response, null user token
   */
  it('set bet line error test case for null user token', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = null;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.setBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid set bet line response, all null body parameters
   */
  it('set bet line error test case for all null body parameters', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = null;
    req.body.token = null;
    req.body.clientID = null;

    gameController.setBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid set bet line response
   */
  it('set bet line error test case for invalid game id', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.setBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid set bet line response
   */
  it('set bet line error test case for invalid user token', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.INVALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.setBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Error test case for invalid set bet line response, all invalid body parameters
   */
  it('set bet line error test case for all invalid body parameters', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.INVALID_GAME_ID;
    req.body.token = testData.INVALID_USER_TOKEN;
    req.body.clientID = testData.INVALID_CLIENT_ID;

    gameController.setBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is not sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.not.be.eql(true);
      done();
    }, 10);
  });


  /**
   * Successful test case for valid set bet line response
   */
  it('set bet line successful test case', (done) => {
    let req = {},
      res = {};

    req.body = {};
    req.body.gameId = testData.VALID_GAME_ID;
    req.body.token = testData.VALID_USER_TOKEN;
    req.body.clientID = testData.VALID_CLIENT_ID;

    gameController.setBetLine(req, res);

    setTimeout(() => {
      /**
      * Expect that successful response is sent
      */
      expect(responseDispatcher.dispatch.calledOnce).to.be.eql(true);
      done();
    }, 10);
  });
});
