const chai = require('chai');
const sinon = require('sinon');
const compute = require('../../../util/compute');
const testData = require('../../test-data');
const successCases = require('./success-response');
const nj = require('numjs');
const rng = require('../../../util/rng');
const whowapi = require('../../../api/whowapi');
const reelMask = require('../../../config/reel-mask-config');
const gameSession = require('../../../util/gamesession');
const constants = require('../../../response/constants');

const expect = chai.expect;


/**
* Game Compute releted tests are performed
* Game computation tests includes following tests
* - Test view zone created from random numbers
* - Test game spin compuatation from random numbers
* - Test game spin win computations
* - Test game spin free spin win
* - Test game spin scatter pay
* - Test game spin wild pay
* - Test game spin response builder
* - Success case everything above is calculated as expected
*/
describe('GameComputationTest', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
  beforeEach(() => {
    /**
    * Stub to generate random numbers
    */
    sinon.stub(rng, 'generateRandomNumber').callsFake((reelConfig) => {
      if (reelConfig.getNoWin === true) {
        return testData.RANDOM_NUMBER_NO_WINS;
      }
      return [29, 1, 10, 20, 10];
    });

    /**
    * Stub to mock function responsilble for whow api call
    */
    sinon.stub(compute, 'callWhow').callsFake((token, result, computeData, betAmount, virtualAmount, total, session) => Promise.resolve(result));

    /**
     * Stub to mock whow api call of bet/play when a game round is played
     */
    sinon.stub(whowapi, 'play').callsFake((token, {}) => Promise.resolve(JSON.parse(testData.SPIN_DATA_FOR_WHOW_API_2)));
    sinon.stub(whowapi, 'bet').callsFake((token, {}) => Promise.resolve(JSON.parse(testData.SPIN_DATA_FOR_WHOW_API_1)));
    sinon.stub(whowapi, 'close').callsFake((token, {}) => {});

    /**
    * stub to mock computation of results
    */
    sinon.stub(gameSession, 'getUserSession').callsFake((token, gameId) => {
      if (gameId == testData.VALID_GAME_ID && token == testData.VALID_USER_TOKEN) {
        const session = successCases.VALID_USER_SESSION[successCases.VALID_USER_SESSION.sessionKey].value;
        return Promise.resolve(session);
      }
      return Promise.reject({ error: constants.RES_MESSAGES.NO_PAYTABLE });
    });
  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    rng.generateRandomNumber.restore();
    compute.callWhow.restore();
    whowapi.play.restore();
    whowapi.bet.restore();
    whowapi.close.restore();
    gameSession.getUserSession.restore();
  });


  /**
  * Error test case for wrong view zone from random numbers
  */
  it('game view zone computation, wrong view zone from random numbers', (done) => {
    // random numbers [6, 18, 29, 9, 7]
    const game = JSON.parse(testData.GAME_DATA).game;
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    let arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const expanding = [];
    const scatter = [];
    const randomNumber = testData.VALID_RANDOM_NUMBERS_FOR_TEST;
    const session = successCases.VALID_USER_SESSION[successCases.VALID_USER_SESSION.sessionKey].value;

    compute.generateViewZone(randomNumber, noofReels, columnSize, expanding, scatter, game, arrays, session);

    arrays = JSON.stringify(arrays);

    expect(arrays).to.not.include(testData.WRONG_VIEW_ZONE_SYMBOL_ID);
    done();
  });


  /**
  * Error test case for wrong number of scatter symbols in view zone
  */
  it('game view zone computation, wrong number of scatter symbols', (done) => {
    // random numbers [0, 20, 5, 15, 17]
    const game = JSON.parse(testData.GAME_DATA).game;
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    let arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    let expanding = [];
    let scatter = [];
    const session = successCases.VALID_USER_SESSION[successCases.VALID_USER_SESSION.sessionKey].value;

    // set 1 test case
    let randomNumber = testData.VALID_RANDOM_NUMBERS_FOR_SCATTER_TEST_1;
    compute.generateViewZone(randomNumber, noofReels, columnSize, expanding, scatter, game, arrays, session);
    expect(scatter).to.not.be.length(1);

    // set 2 test case
    randomNumber = testData.VALID_RANDOM_NUMBERS_FOR_SCATTER_TEST_2;
    arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    expanding = []; scatter = [];
    compute.generateViewZone(randomNumber, noofReels, columnSize, expanding, scatter, game, arrays, session);
    expect(scatter).to.not.be.length(2);
    done();
  });


  /**
  * Successful test case for right number of scatter symbols in view zone
  */
  it('game view zone computation, right number of scatter symbols', (done) => {
    // random numbers [0, 20, 5, 15, 17]
    const game = JSON.parse(testData.GAME_DATA).game;
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const expanding = [];
    const scatter = [];
    const session = successCases.VALID_USER_SESSION[successCases.VALID_USER_SESSION.sessionKey].value;

    // set 1 test case
    const randomNumber = testData.VALID_RANDOM_NUMBERS_FOR_SCATTER_TEST_1;
    compute.generateViewZone(randomNumber, noofReels, columnSize, expanding, scatter, game, arrays, session);
    expect(scatter).to.be.length(2);
    done();
  });


  /**
  * Error test case for wild not being present
  */
  it('game view zone computation, no scatter symbols', (done) => {
    const game = JSON.parse(testData.GAME_DATA).game;
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    let arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const expanding = [];
    const scatter = [];
    const session = successCases.VALID_USER_SESSION[successCases.VALID_USER_SESSION.sessionKey].value;

    // set 1 test case
    const randomNumber = testData.VALID_RANDOM_NUMBERS_FOR_WILD_NOT_PRESENT_TEST;
    compute.generateViewZone(randomNumber, noofReels, columnSize, expanding, scatter, game, arrays, session);

    arrays = JSON.parse(JSON.stringify(arrays));
    expect(arrays).to.not.include(testData.WILD_SYMBOL_ID);
    done();
  });


  /**
  * Successful test case for wild being present
  */
  it('game view zone computation, expected scatter symbol presence', (done) => {
    const game = JSON.parse(testData.GAME_DATA).game;
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    let arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const expanding = [];
    const scatter = [];
    const session = successCases.VALID_USER_SESSION[successCases.VALID_USER_SESSION.sessionKey].value;

    const randomNumber = testData.VALID_RANDOM_NUMBERS_FOR_WILD_PRESENT_TEST;
    compute.generateViewZone(randomNumber, noofReels, columnSize, expanding, scatter, game, arrays, session);

    arrays = JSON.parse(JSON.stringify(arrays));
    expect(arrays).to.include(testData.WILD_SYMBOL_ID);
    done();
  });


  /**
  * Error test case of no wins based on view zone
  */
  it('game win computation, no wins', (done) => {
    const game = JSON.parse(testData.GAME_DATA).game;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const arrays = testData.NO_WIN_VIEW_ZONE();
    const payArray = game.default.gameConfig.payLines.normalizedPayArray;
    const spinResult = [];
    compute.computeWins(arrays, columnSize, payArray, game, spinResult)
    .then((data) => {
      expect(data).to.be.length(0);
    })
    .then(() => done(), done);
  });


  /**
  * Successful test case of win based on view zone
  */
  it('game win computation for different view zone wins', (done) => {
    const game = JSON.parse(testData.GAME_DATA).game;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const payArray = game.default.gameConfig.payLines.normalizedPayArray;

    // various wins for different view zones is determined
    Promise.all([
      compute.computeWins(testData.NO_WIN_VIEW_ZONE(), columnSize, payArray, game, []),
      compute.computeWins(testData.ONE_WIN_VIEW_ZONE(), columnSize, payArray, game, []),
      compute.computeWins(testData.THREE_WIN_VIEW_ZONE(), columnSize, payArray, game, []),
      compute.computeWins(testData.FOUR_WIN_VIEW_ZONE(), columnSize, payArray, game, []),
    ])
    .then((values) => {
      expect(values[0]).to.be.length(0);
      expect(values[1]).to.be.length(1);
      expect(values[2]).to.be.length(3);
      expect(values[3]).to.be.length(4);
    })
    .then(() => done(), done);
  });

  /**
  * Error test case for no of freespin awards based on view zone
  */
  it('game win computation, no free spins awards based on view zone', (done) => {
    const game = JSON.parse(testData.GAME_DATA).game;

    // various free spins awarded for number of scatter symbols
    Promise.all([
      compute.freeSpinCompute([0, 1], [], game),
      compute.freeSpinCompute([0], [], game),
      compute.freeSpinCompute([0, 1, 2, 3, 4, 5], [], game),
    ])
    .then((result) => {
      expect(result[0]).to.be.length(0);
      expect(result[1]).to.be.length(0);
      expect(result[2]).to.be.length(0);
    })
    .then(() => done(), done);
  });


  /**
  * Successful test case for no of freespin awards based on view zone
  */
  it('game win computation, correct free spins awards based on view zone', (done) => {
    const game = JSON.parse(testData.GAME_DATA).game;

    // various free spins awarded for number of scatter symbols
    Promise.all([
      compute.freeSpinCompute([0, 1, 2], [], game),
      compute.freeSpinCompute([0, 1, 2, 3], [], game),
      compute.freeSpinCompute([0, 1, 2, 3, 4], [], game),
    ])
    .then((result) => {
      expect(result[0].freeSpins).to.be.eql(10);
      expect(result[1].freeSpins).to.be.eql(10);
      expect(result[2].freeSpins).to.be.eql(10);
    })
    .then(() => done(), done);
  });


  /**
  * Error test case for no of wins from scatter
  */
  it('game win computation, no scatter wins based on view zone', (done) => {
    const game = JSON.parse(testData.GAME_DATA).game;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const payArray = game.default.gameConfig.payLines.normalizedPayArray;
    // various free spins awarded for number of scatter symbols
    Promise.all([
      compute.scatterPayCompute(testData.ONE_WIN_VIEW_ZONE(), columnSize, payArray, game, []),
      compute.scatterPayCompute(testData.THREE_WIN_VIEW_ZONE(), columnSize, payArray, game, []),
    ])
    .then((result) => {
      expect(result[0]).to.be.length(0);
      expect(result[1]).to.be.length(0);
    })
    .then(() => done(), done);
  });


  /**
  * Successful test case for no of wins from scatter
  */
  it('game win computation, correct scatter wins based on view zone', (done) => {
    const game = JSON.parse(testData.GAME_DATA).game;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const payArray = game.default.gameConfig.payLines.normalizedPayArray;
    // various free spins awarded for number of scatter symbols
    Promise.all([
      compute.scatterPayCompute(testData.ONE_WIN_VIEW_ZONE(), columnSize, payArray, game, []),
      compute.scatterPayCompute(testData.ONE_SCATTER_WIN_VIEW_ZONE(), columnSize, payArray, game, []),
    ])
    .then((result) => {
      expect(result[0]).to.be.length(0);
      expect(result[1]).to.be.length(1);
    })
    .then(() => done(), done);
  });


  /**
  * Error test case for no of wins from wilds
  */
  it('game win computation, no wild wins based on view zone', (done) => {
    const game = JSON.parse(testData.GAME_DATA).game;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const payArray = game.default.gameConfig.payLines.normalizedPayArray;
    // various free spins awarded for number of scatter symbols
    Promise.all([
      compute.wildPayCompute(testData.ONE_WIN_VIEW_ZONE(), columnSize, payArray, game, []),
      compute.wildPayCompute(testData.TWO_WILD_SYMBOLS_NO_WIN_VIEW_ZONE(), columnSize, payArray, game, []),
    ])
    .then((result) => {
      expect(result[0]).to.be.length(0);
      expect(result[1]).to.be.length(0);
    })
    .then(() => done(), done);
  });


  /**
  * Successful test case for correct no of wins from wilds
  */
  it('game win computation, correct no of wild wins based on view zone', (done) => {
    const game = JSON.parse(testData.GAME_DATA).game;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const payArray = game.default.gameConfig.payLines.normalizedPayArray;
    // various free spins awarded for number of scatter symbols
    Promise.all([
      compute.wildPayCompute(testData.THREE_WILD_SYMBOLS_WIN_VIEW_ZONE(), columnSize, payArray, game, []),
    ])
    .then((result) => {
      expect(result[0]).to.be.length(3);
    })
    .then(() => done(), done);
  });


  /**
  * Error test case for no wins from view zone
  */
  it('game computation, no computed wins from view zone', (done) => {
    const game = JSON.parse(testData.GAME_DATA).game;
    const gameId = testData.VALID_GAME_ID;
    const token = testData.VALID_USER_TOKEN;
    const session = successCases.VALID_USER_SESSION[successCases.VALID_USER_SESSION.sessionKey].value;
    game.default.gameConfig.reelConfig.getNoWin = true;
    Promise.all([
      compute.compute(game, gameId, token, session),
    ])
    .then((result) => {
      expect(result[0].results).to.be.length(0);
    })
    .then(() => done(), done);
  });


  /**
  * Successful test case for no of wins from view zone
  */
  it('game computation, correct computed wins from view zone', (done) => {
    const game = JSON.parse(testData.GAME_DATA).game;
    const gameId = testData.VALID_GAME_ID;
    const token = testData.VALID_USER_TOKEN;
    const session = successCases.VALID_USER_SESSION[successCases.VALID_USER_SESSION.sessionKey].value;
    game.default.gameConfig.reelConfig.getNoWin = false;
    Promise.all([
      compute.compute(game, gameId, token, session),
    ])
    .then((result) => {
      expect(result[0].results).to.have.length.above(0);
    })
    .then(() => done(), done);
  });


  /**
  * Error test case for compute response for no wins
  */
  it('game computation, wrong compute response from response builder', (done) => {
    const obj = JSON.parse(testData.GAME_DATA);
    const token = testData.VALID_USER_TOKEN;
    const session = successCases.VALID_USER_SESSION[successCases.VALID_USER_SESSION.sessionKey].value;
    const sessionRequest = successCases.VALID_USER_SESSION[successCases.VALID_USER_SESSION.sessionRequestKey].value;
    const computeData = testData.COMPUTE_RESULT_SET_1();

    Promise.all([
      compute.generateSpinResponse(obj, computeData, token, successCases.VALID_USER_SESSION.sessionRequestKey, sessionRequest, session),
    ])
    .then((result) => {
      expect(result[0].wins.winLines).to.be.length(0);
      expect(result[0].wins.winCoins).to.be.eql(0);
      expect(result[0].wins.winLinesCount).to.be.eql(0);
    })
    .then(() => done(), done);
  });


  /**
  * Success test case for correct compute response
  */
  it('game computation, correct compute response from response builder', (done) => {
    const obj = JSON.parse(testData.GAME_DATA);
    const token = testData.VALID_USER_TOKEN;
    const session = successCases.VALID_USER_SESSION[successCases.VALID_USER_SESSION.sessionKey].value;
    const sessionRequest = successCases.VALID_USER_SESSION[successCases.VALID_USER_SESSION.sessionRequestKey].value;
    const computeData = testData.COMPUTE_RESULT_SET_1();


    compute.generateSpinResponse(obj, computeData, token, successCases.VALID_USER_SESSION.sessionRequestKey, sessionRequest, session)
    .then((result) => {
      /**
       * expect properties to be present in result
       */
      expect(result).to.have.nested.property('viewZone');
      expect(result).to.have.nested.property('viewZone.viewZone');
      expect(result.viewZone.viewZone).to.be.a('number');

      /**
       * expect there to be a result property to display user wins
       */
      expect(result).to.have.nested.property('wins');
      expect(result).to.have.nested.property('wins.winCoins');
      expect(result).to.have.nested.property('wins.winLinesCount');
      expect(result).to.have.nested.property('wins.winLines');

      /**
       * expect free spin property to be present, to inform user is any free spins are present
       */
      expect(result).to.have.nested.property('freeSpinData');

      /**
       * expect level propertub in spin result, to inform user of the progress made
       */
      expect(result).to.have.nested.property('levelData');

      /**
       * expect view zone to have expected number of reels
       */
      expect(parseInt(result.viewZone.viewZone.toString().charAt(1))).to.be.eql(Object.keys(result.viewZone).length - 1);

      /**
       * Expect all reels in view zone to be of expected lenght based on view port
       */
      const reels = Object.keys(result.viewZone);
      let reelSize = reelMask.INITIAL_REEL_SIZE;
      for (let i = 0; i < reels.length - 1; i++) {
        if (i > 0) {
          reelSize += reelMask.REEL_SIZE_INCREMENT;
        }
        expect(result.viewZone[reels[i]].symbols.length).to.be.eql(reelSize);
      }
    })
    .then(() => done(), done);
  });


  /**
   * Error test case for game click response
   */
  it('invalid game click response test', (done) => {
    compute.generateGameClickResponse(JSON.parse(testData.GAME_DATA))
    .then((result) => {
      /**
      * Expect number of reels to not be greater than expectation
      */
      expect(result.game.reels.noofReels).to.not.be.gt(Object.keys(result.game.reels).length);

      /**
      * Expect reel size to not exceed more than expectation
      */
      const reels = Object.keys(result.game.reels);
      const column = parseInt(result.game.viewZone.toString().charAt(0));
      for (let i = 1; i < reels.length; i++) {
        expect(result.game.reels[reels[i]].length).to.be.eql(column);
      }

      done();
    });
  });

  /**
   * Successful test case for game click response
   */
  it('successful game click response test', (done) => {
    compute.generateGameClickResponse(JSON.parse(testData.GAME_DATA))
    .then((result) => {
      /**
      * Expect game configuration properties to be present in click response
      */
      expect(result).to.have.nested.property('game');
      expect(result).to.have.nested.property('game.gameId');
      expect(result).to.have.nested.property('game.viewZone');
      expect(result.game.viewZone).to.be.a('number');
      expect(result).to.have.nested.property('game.reels');
      expect(result).to.have.nested.property('game.payArray');
      expect(result).to.have.nested.property('game.payTable');
      expect(result).to.have.nested.property('game.symbols');
      expect(result).to.have.nested.property('userDetails');

      /**
      * Check if required number of reels are present
      */
      expect(result.game.reels.noofReels).to.be.eql(Object.keys(result.game.reels).length - 1);

      /**
      * Expect reel size is as per expectations
      */
      const reels = Object.keys(result.game.reels);
      const column = parseInt(result.game.viewZone.toString().charAt(0));
      for (let i = 1; i < reels.length; i++) {
        expect(result.game.reels[reels[i]].length).to.be.eql(column);
      }

      done();
    });
  });


  /**
   * Successful test case for whow api call
   */
  it('valid whow api call', (done) => {
    const token = testData.VALID_USER_TOKEN;
    const session = successCases.VALID_USER_SESSION[successCases.VALID_USER_SESSION.sessionKey].value;
    const computeData = testData.COMPUTE_RESULT_SET_1();
    const betAmount = session.eventData.currentBet * session.eventData.currentLines;

    Promise.all([
      compute.callWhow(token, JSON.parse(testData.SPIN_RESULT_DATA_FOR_WHOW_API_1), computeData, betAmount, 0, 0, session),
      compute.callWhow(token, JSON.parse(testData.SPIN_RESULT_DATA_FOR_WHOW_API_2), computeData, betAmount, 0, 0, session),
    ])
    .then((result) => {
      const noWin = result[0];
      const win = result[1];

      /**
       * Expect there to be keys in case when there is no win
       */
      expect(noWin).to.have.nested.property('levelData');
      expect(noWin).to.have.nested.property('levelData.level');
      expect(noWin).to.have.nested.property('levelData.levelProgress');
      expect(noWin).to.have.nested.property('wallet');
      expect(noWin).to.have.nested.property('wallet.chips');
      expect(noWin).to.have.nested.property('game');
      expect(noWin).to.have.nested.property('game.settings');
      expect(noWin).to.have.nested.property('game.settings.bets');
      expect(noWin).to.have.nested.property('jackpot');
      expect(noWin).to.have.nested.property('jackpot.jackpots');

      /**
       * Expect there to be keys in case when there is  win
       */
      expect(win).to.have.nested.property('levelData');
      expect(win).to.have.nested.property('levelData.level');
      expect(win).to.have.nested.property('levelData.levelProgress');
      expect(win).to.have.nested.property('wallet');
      expect(win).to.have.nested.property('wallet.chips');
      expect(win).to.have.nested.property('game');
      expect(win).to.have.nested.property('game.settings');
      expect(win).to.have.nested.property('game.settings.bets');
      expect(win).to.have.nested.property('jackpot');
      expect(win).to.have.nested.property('jackpot.jackpots');
    }).then(() => done(), done);
  });


  /**
   * Error test case of failed whow api call
   */
  it('failed whow api call test', (done) => {
    const token = testData.VALID_USER_TOKEN;
    const session = successCases.VALID_USER_SESSION[successCases.VALID_USER_SESSION.sessionKey].value;
    const computeData = testData.COMPUTE_RESULT_SET_1();
    const betAmount = session.eventData.currentBet * session.eventData.currentLines;

    Promise.all([
      compute.callWhow(token, JSON.parse(testData.FAILED_WHOW_API_CALL_DATA_1), computeData, betAmount, 0, 0, session),
      compute.callWhow(token, JSON.parse(testData.FAILED_WHOW_API_CALL_DATA_2), computeData, betAmount, 0, 0, session),
    ])
    .then((result) => {
      const noWin = result[0];
      const win = result[1];

      /**
       * Expect there to be keys in case of failed whow api call and user wins nothing
       */
      expect(noWin).to.have.nested.property('levelData');

      /**
       * Expect absence of keys in case of failed whow api when user wins nothing
       */
      expect(noWin).not.to.have.nested.property('levelData.level');
      expect(noWin).not.to.have.nested.property('levelData.levelProgress');
      expect(noWin).not.to.have.nested.property('wallet');
      expect(noWin).not.to.have.nested.property('wallet.chips');
      expect(noWin).not.to.have.nested.property('game');
      expect(noWin).not.to.have.nested.property('game.settings');
      expect(noWin).not.to.have.nested.property('game.settings.bets');
      expect(noWin).not.to.have.nested.property('jackpot');
      expect(noWin).not.to.have.nested.property('jackpot.jackpots');


      /**
       * Expect there to be keys in case of failed whow api call and user wins
       */
      expect(win).to.have.nested.property('levelData');

      /**
       * Expect absence of keys in case of failed whow api when user wins
       */
      expect(win).not.to.have.nested.property('levelData.level');
      expect(win).not.to.have.nested.property('levelData.levelProgress');
      expect(win).not.to.have.nested.property('wallet');
      expect(win).not.to.have.nested.property('wallet.chips');
      expect(win).not.to.have.nested.property('game');
      expect(win).not.to.have.nested.property('game.settings');
      expect(win).not.to.have.nested.property('game.settings.bets');
      expect(win).not.to.have.nested.property('jackpot');
      expect(win).not.to.have.nested.property('jackpot.jackpots');
    }).then(() => done(), done);
  });
});
