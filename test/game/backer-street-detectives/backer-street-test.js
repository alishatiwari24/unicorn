const chai = require('chai');
const sinon = require('sinon');
const testData = require('./test-data');
const backerStreet = require('../../../util/games/backer-street-detective');
const gameSession = require('../../../util/gamesession');
const constants = require('../../../response/constants');

const expect = chai.expect;


/**
* Backer street detectives related test cases are performed
*/
describe('BackerStreetTest', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
  beforeEach(() => {
    /**
    * stub to mock getting user session
    */
    sinon.stub(gameSession, 'getUserSession').callsFake((token, gameId) => {
      if (token === testData.VALID_USER_TOKEN && gameId === testData.VALID_GAME_ID_FREE_SPIN) {
        return Promise.resolve(testData.FRESS_SPIN_USER_SESSION);
      } else if (token === testData.INVALID_USER_TOKEN || gameId === testData.INVALID_GAME_ID) {
        return Promise.reject({ error: constants.RES_MESSAGES.NO_PAYTABLE });
      }
      return Promise.resolve(testData.MAIN_GAME_SESSION);
    });

    /**
     * stub to mock updating user session
     */
    sinon.stub(backerStreet, 'updateSession').callsFake((session, gameId) => {});
  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    gameSession.getUserSession.restore();
    backerStreet.updateSession.restore();
  });


  /**
  * Cheking for roaming wild positions on spin
  */
  it('Cheking for roaming wild positions on spin, when free spins are not active', (done) => {
    const game = testData.VALID_GAME;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const session = testData.MAIN_GAME_SESSION;
    const result = backerStreet.getRoamingWildPositions(session, game.default.gameConfig.specialFeature, columnSize);
    /**
     * Expect that result has appropriate length
     */
     expect(result).to.have.a.property('roamingWildsPositions');
     expect(result).to.have.a.property('wildPositions');
     expect(result.roamingWildsPositions).to.have.length(2);
    done();
  });


  /**
  * Cheking for roaming wild positions on spin
  */
  it('Cheking for roaming wild positions on spin, when free spins are active', (done) => {
    const game = testData.VALID_GAME;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const session = testData.FREE_GAME_SESSION;
    const result = backerStreet.getRoamingWildPositions(session, game.default.gameConfig.specialFeature, columnSize);
    /**
     * Expect that result has appropriate length
     */
    expect(result).to.have.a.property('roamingWildsPositions');
    expect(result).to.have.a.property('wildPositions');
    expect(result.roamingWildsPositions).to.have.length(5);
    done();
  });


  /**
  * Cheking for bonus game
  */
  it('Cheking for card pick when free spin is awarded', (done) => {
    const gameId = testData.VALID_GAME_ID;
    const validSession = testData.FREE_GAME_SESSION;
    backerStreet.handleBonusRequest(gameId, 0, validSession)
    .then((result) => {
      /**
      * Expect that result to return error
      */
      expect(result).to.have.property('response');
      expect(result.response).to.have.property('actualCards').to.have.length(3);
      expect(result.response).to.have.property('selectedCard').to.be.eql(3);
      done();
    });
  });
});
