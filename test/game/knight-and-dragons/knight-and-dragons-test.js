const chai = require('chai');
const sinon = require('sinon');
const gameSession = require('../../../util/gamesession');
const constants = require('../../../response/constants');
const kad = require('../../../util/games/knight-dragons');
const kadTestData = require('./test-data');
const rng = require('../../../util/rng');
const gameFunction = require('../../../helpers/gamefunction');
const testData = require('../../test-data');

const expect = chai.expect;


/**
* Unicron slot related test cases are performed
* Unicron slote tests include following tests
* - Test User session computation for game click
* - Fetching user session with required reel set
* - Fetching reel configuration for subsequent spin in main game and free game
*/
describe('KnightAndDragonTest', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
  beforeEach(() => {
    /**
    * stub to mock updating session request doc
    */
    sinon.stub(kad, 'updateSessionRequestDocForBonus').callsFake((sessionRequestKey, sessionRequectDoc, cas, status) => {
      if (cas === kadTestData.VALID_CAS_VALUE) {
        return Promise.resolve(cas);
      }

      return Promise.reject();
    });


    /**
    * stub to mock updating session doc
    */
    sinon.stub(kad, 'updateSession').callsFake((session, gameId) => {});


    /**
    * stub to mock updating bonus wins with whow api
    */
    sinon.stub(kad, 'updateBonusWins').callsFake((token, betAmount, winCoins, virtualAmount) => {});


    /**
    * stub to mock getting random numbers
    */
    sinon.stub(rng, 'generateRandomNumber').callsFake(reelConfig => [0, 4, 17, 11, 5]);


    /**
    * stub to mock getting user session and session request
    */
    sinon.stub(gameFunction, 'getUserSessionAndSessionRequest').callsFake((token, gameId) => Promise.resolve(kadTestData.VALID_USER_SESSION_AND_REQUEST_WITH_BONUS));


    /**
    * stub to mock updating session request doc
    */
    sinon.stub(gameFunction, 'getGame').callsFake((gameId) => {
      if (gameId === kad.ALTERNATE_VALID_GAME_ID) {
        return { game: kad.VALID_GAME_DATA };
      }
      return kad.VALID_GAME_DATA;
    });


    /**
    * stub to mock getting user session and request doc from game session
    */
    sinon.stub(gameSession, 'getUserSessionAndSessionRequest').callsFake((token, gameId) => Promise.resolve(kadTestData.VALID_USER_SESSION_AND_REQUEST_WITH_BONUS));


    /**
     * stub to mock a call to whow bet api
     */
    sinon.stub(kad, 'whowBetApi').callsFake((roundId, token, zero, winAmount, betAmount, session) => Promise.resolve(kadTestData.WHOW_BET_API_RESPONSE));


     /**
      * stub to mock a call to whow close api
      */
    sinon.stub(kad, 'whowCloseApi').callsFake((roundId, token, winAmount, session) => Promise.resolve(kadTestData.WHOW_CLOSE_API_RESPONSE));


    /**
    * stub to mock getting user session
    */
    sinon.stub(gameSession, 'getUserSession').callsFake((token, gameId) => {
      if (gameId === kadTestData.VALID_GAME_ID && token === kadTestData.VALID_USER_TOKEN) {
        return Promise.resolve(kadTestData.USER_SESSION_FOR_GAMBLE);
      } else if (gameId === kadTestData.ALTERNATE_VALID_GAME_ID && token === kadTestData.VALID_USER_TOKEN) {
        return Promise.resolve(kadTestData.USER_SESSION_AVAILABLE_GAMBLE);
      } else if (gameId === kadTestData.PICK_GAMBLE_GAME_ID && token === kadTestData.VALID_USER_TOKEN) {
        return Promise.resolve(kadTestData.USER_SESSION_FOR_PICK_GAMBLE);
      }

      return Promise.reject({ error: constants.RES_MESSAGES.NO_PAYTABLE });
    });
  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    kad.updateSessionRequestDocForBonus.restore();
    kad.updateSession.restore();
    kad.updateBonusWins.restore();
    rng.generateRandomNumber.restore();
    gameFunction.getUserSessionAndSessionRequest.restore();
    gameFunction.getGame.restore();
    gameSession.getUserSessionAndSessionRequest.restore();
    kad.whowBetApi.restore();
    kad.whowCloseApi.restore();
    gameSession.getUserSession.restore();
  });


  /**
  * Test case to check if there is evil symbol or damsel adjacent to good knight
  */
  it('Test case to check if there is evil symbol or damsel adjacent to good knight, when not present', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const arrays = kadTestData.NO_ADJACENT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;

    const result = kad.checkIfAdjacentKnights(arrays, noofReels, columnSize, game);

    /**
     * Expect a emplty result
     */
    expect(result).to.be.an('array').to.have.length(0);
    done();
  });


  /**
  * Test case to check if there is evil symbol or damsel adjacent to good knight
  */
  it('Test case to check if there is evil symbol or damsel adjacent to good knight, when Damsel on left', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const arrays = kadTestData.DAMSEL_ADJACENT_LEFT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;
    const result = kad.checkIfAdjacentKnights(arrays, noofReels, columnSize, game);

    /**
     * Expect there to be a result
     */
    expect(result).to.be.an('array').to.have.length.gt(0);

    /**
     * Expecrt required fields to be present in result
     */
    result.forEach((res) => {
      expect(res).to.have.property('currentPosition');
      expect(res).to.have.property('currentSymbol');
      expect(res).to.have.property('position').eql('left');
      expect(res).to.have.property('symbolPoistion');
      expect(res).to.have.property('symbolName').eql('Damsel');
    });
    done();
  });


  /**
  * Test case to check if there is evil symbol or damsel adjacent to good knight
  */
  it('Test case to check if there is evil symbol or damsel adjacent to good knight, when Damsel on right', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const arrays = kadTestData.DAMSEL_ADJACENT_RIGHT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;
    const result = kad.checkIfAdjacentKnights(arrays, noofReels, columnSize, game);

    /**
     * Expect there to be a result
     */
    expect(result).to.be.an('array').to.have.length.gt(0);

    /**
     * Expecrt required fields to be present in result
     */
    result.forEach((res) => {
      expect(res).to.have.property('currentPosition');
      expect(res).to.have.property('currentSymbol');
      expect(res).to.have.property('position').eql('right');
      expect(res).to.have.property('symbolPoistion');
      expect(res).to.have.property('symbolName').eql('Damsel');
    });
    done();
  });


  /**
  * Test case to check if there is evil symbol or damsel adjacent to good knight
  */
  it('Test case to check if there is evil symbol or damsel adjacent to good knight, when Damsel is present on either side multiple times', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const arrays = kadTestData.DAMSEL_MULTIPLE_ADJACENT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;
    const result = kad.checkIfAdjacentKnights(arrays, noofReels, columnSize, game);

    /**
     * Expect there to be a result
     */
    expect(result).to.be.an('array').to.have.length.gt(0);

    /**
     * Expecrt required fields to be present in result
     */
    result.forEach((res) => {
      expect(res).to.have.property('currentPosition');
      expect(res).to.have.property('currentSymbol');
      expect(res).to.have.property('position');
      expect(res).to.have.property('symbolPoistion');
      expect(res).to.have.property('symbolName').eql('Damsel');
    });
    done();
  });


  /**
  * Test case to check if there is evil symbol or damsel adjacent to good knight
  */
  it('Test case to check if there is evil symbol or damsel adjacent to good knight, when Evil Knight on left', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const arrays = kadTestData.EVIL_KNIGHT_ADJACENT_LEFT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;
    const result = kad.checkIfAdjacentKnights(arrays, noofReels, columnSize, game);
    /**
     * Expect there to be a result
     */
    expect(result).to.be.an('array').to.have.length.gt(0);

    /**
     * Expecrt required fields to be present in result
     */
    result.forEach((res) => {
      expect(res).to.have.property('currentPosition');
      expect(res).to.have.property('currentSymbol');
      expect(res).to.have.property('position').eql('left');
      expect(res).to.have.property('symbolPoistion');
      expect(res).to.have.property('symbolName').eql('Evil Knight');
    });
    done();
  });


  /**
  * Test case to check if there is evil symbol or damsel adjacent to good knight
  */
  it('Test case to check if there is evil symbol or damsel adjacent to good knight, when Evil Knight on right', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const arrays = kadTestData.EVIL_KNIGHT_ADJACENT_RIGHT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;
    const result = kad.checkIfAdjacentKnights(arrays, noofReels, columnSize, game);

    /**
     * Expect there to be a result
     */
    expect(result).to.be.an('array').to.have.length.gt(0);

    /**
     * Expecrt required fields to be present in result
     */
    result.forEach((res) => {
      expect(res).to.have.property('currentPosition');
      expect(res).to.have.property('currentSymbol');
      expect(res).to.have.property('position').eql('right');
      expect(res).to.have.property('symbolPoistion');
      expect(res).to.have.property('symbolName').eql('Evil Knight');
    });
    done();
  });


  /**
  * Test case to check if there is evil symbol or damsel adjacent to good knight
  */
  it('Test case to check if there is evil symbol or damsel adjacent to good knight, when Evil Knight is present on either side multiple times', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const arrays = kadTestData.EVIL_KNIGHT_MULTIPLE_ADJACENT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;
    const result = kad.checkIfAdjacentKnights(arrays, noofReels, columnSize, game);

    /**
     * Expect there to be a result
     */
    expect(result).to.be.an('array').to.have.length.gt(0);

    /**
     * Expecrt required fields to be present in result
     */
    result.forEach((res) => {
      expect(res).to.have.property('currentPosition');
      expect(res).to.have.property('currentSymbol');
      expect(res).to.have.property('position');
      expect(res).to.have.property('symbolPoistion');
      expect(res).to.have.property('symbolName').eql('Evil Knight');
    });
    done();
  });


  /**
  * Test case to check if there is evil symbol or damsel adjacent to good knight
  */
  it('Test case to check if there is evil symbol or damsel adjacent to good knight, when both Damsel and Evil Knight is present on either sides', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const arrays = kadTestData.EVIL_KNIGHT_DAMSEL_ADJACENT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;
    const result = kad.checkIfAdjacentKnights(arrays, noofReels, columnSize, game);

    /**
     * Expect there to be a result
     */
    expect(result).to.be.an('array').to.have.length.gt(0);

    /**
     * Expecrt required fields to be present in result
     */
    result.forEach((res) => {
      expect(res).to.have.property('currentPosition');
      expect(res).to.have.property('currentSymbol');
      expect(res).to.have.property('position');
      expect(res).to.have.property('symbolPoistion');
      expect(res).to.have.property('symbolName');
    });
    done();
  });


  /**
  * Test case to determine adjacent symbol winnings
  */
  it('Test case to determine adjacent symbol winnings, when Damsel is left', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const session = kadTestData.VALID_USER_SESSION;
    const arrays = kadTestData.DAMSEL_ADJACENT_LEFT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;
    const adjacents = kad.checkIfAdjacentKnights(arrays, noofReels, columnSize, game);
    const result = kad.fetchAdjacentWinnings(adjacents, session.eventData.currentBet, game.default.gameConfig.specialFeature);

    /**
     * Expect there to be a result
     */
    expect(result).to.be.an('array').to.have.length.gt(0);

    /**
     * Expecrt required fields to be present in result
     */
    result.forEach((res) => {
      expect(res).to.have.property('currentPosition');
      expect(res).to.have.property('currentSymbol');
      expect(res).to.have.property('position').eql('left');
      expect(res).to.have.property('symbolPoistion');
      expect(res).to.have.property('symbolName').eql('Damsel');
      expect(res).to.have.property('hasGoodKnightWon');

      /**
       * If Good knight wins expect winnings
       */
      if (res.hasGoodKnightWon) {
        expect(res).to.have.property('winAmount');
      }
    });
    done();
  });


  /**
  * Test case to determine adjacent symbol winnings
  */
  it('Test case to determine adjacent symbol winnings, when Damsel is right', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const session = kadTestData.VALID_USER_SESSION;
    const arrays = kadTestData.DAMSEL_ADJACENT_RIGHT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;
    const adjacents = kad.checkIfAdjacentKnights(arrays, noofReels, columnSize, game);
    const result = kad.fetchAdjacentWinnings(adjacents, session.eventData.currentBet, game.default.gameConfig.specialFeature);

    /**
     * Expect there to be a result
     */
    expect(result).to.be.an('array').to.have.length.gt(0);

    /**
     * Expecrt required fields to be present in result
     */
    result.forEach((res) => {
      expect(res).to.have.property('currentPosition');
      expect(res).to.have.property('currentSymbol');
      expect(res).to.have.property('position').eql('right');
      expect(res).to.have.property('symbolPoistion');
      expect(res).to.have.property('symbolName').eql('Damsel');
      expect(res).to.have.property('hasGoodKnightWon');

      /**
       * If Good knight wins expect winnings
       */
      if (res.hasGoodKnightWon) {
        expect(res).to.have.property('winAmount');
      }
    });
    done();
  });


  /**
  * Test case to determine adjacent symbol winnings
  */
  it('Test case to determine adjacent symbol winnings, when Damsel is present on either side multiple times', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const session = kadTestData.VALID_USER_SESSION;
    const arrays = kadTestData.DAMSEL_MULTIPLE_ADJACENT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;
    const adjacents = kad.checkIfAdjacentKnights(arrays, noofReels, columnSize, game);
    const result = kad.fetchAdjacentWinnings(adjacents, session.eventData.currentBet, game.default.gameConfig.specialFeature);

    /**
     * Expect there to be a result
     */
    expect(result).to.be.an('array').to.have.length.gt(0);

    /**
     * Expecrt required fields to be present in result
     */
    result.forEach((res) => {
      expect(res).to.have.property('currentPosition');
      expect(res).to.have.property('currentSymbol');
      expect(res).to.have.property('position');
      expect(res).to.have.property('symbolPoistion');
      expect(res).to.have.property('symbolName').eql('Damsel');
      expect(res).to.have.property('hasGoodKnightWon');

      /**
       * If Good knight wins expect winnings
       */
      if (res.hasGoodKnightWon) {
        expect(res).to.have.property('winAmount');
      }
    });
    done();
  });


  /**
  * Test case to determine adjacent symbol winnings
  */
  it('Test case to determine adjacent symbol winnings, when Evil Knight is left', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const session = kadTestData.VALID_USER_SESSION;
    const arrays = kadTestData.EVIL_KNIGHT_ADJACENT_LEFT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;
    const adjacents = kad.checkIfAdjacentKnights(arrays, noofReels, columnSize, game);
    const result = kad.fetchAdjacentWinnings(adjacents, session.eventData.currentBet, game.default.gameConfig.specialFeature);

    /**
     * Expect there to be a result
     */
    expect(result).to.be.an('array').to.have.length.gt(0);

    /**
     * Expecrt required fields to be present in result
     */
    result.forEach((res) => {
      expect(res).to.have.property('currentPosition');
      expect(res).to.have.property('currentSymbol');
      expect(res).to.have.property('position').eql('left');
      expect(res).to.have.property('symbolPoistion');
      expect(res).to.have.property('symbolName').eql('Evil Knight');
      expect(res).to.have.property('hasGoodKnightWon');

      /**
       * If Good knight wins expect winnings
       */
      if (res.hasGoodKnightWon) {
        expect(res).to.have.property('winAmount');
      } else {
        // expect win amount key to be absent
        expect(res).to.not.have.property('winAmount');
      }
    });
    done();
  });


  /**
  * Test case to determine adjacent symbol winnings
  */
  it('Test case to determine adjacent symbol winnings, when Evil Knight is right', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const session = kadTestData.VALID_USER_SESSION;
    const arrays = kadTestData.EVIL_KNIGHT_ADJACENT_RIGHT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;
    const adjacents = kad.checkIfAdjacentKnights(arrays, noofReels, columnSize, game);
    const result = kad.fetchAdjacentWinnings(adjacents, session.eventData.currentBet, game.default.gameConfig.specialFeature);

    /**
     * Expect there to be a result
     */
    expect(result).to.be.an('array').to.have.length.gt(0);

    /**
     * Expecrt required fields to be present in result
     */
    result.forEach((res) => {
      expect(res).to.have.property('currentPosition');
      expect(res).to.have.property('currentSymbol');
      expect(res).to.have.property('position').eql('right');
      expect(res).to.have.property('symbolPoistion');
      expect(res).to.have.property('symbolName').eql('Evil Knight');
      expect(res).to.have.property('hasGoodKnightWon');

      /**
       * If Good knight wins expect winnings
       */
      if (res.hasGoodKnightWon) {
        expect(res).to.have.property('winAmount');
      } else {
        // expect win amount key to be absent
        expect(res).to.not.have.property('winAmount');
      }
    });
    done();
  });


  /**
  * Test case to determine adjacent symbol winnings
  */
  it('Test case to determine adjacent symbol winnings, when Evil Knight is present on either side multiple times', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const session = kadTestData.VALID_USER_SESSION;
    const arrays = kadTestData.EVIL_KNIGHT_MULTIPLE_ADJACENT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;
    const adjacents = kad.checkIfAdjacentKnights(arrays, noofReels, columnSize, game);
    const result = kad.fetchAdjacentWinnings(adjacents, session.eventData.currentBet, game.default.gameConfig.specialFeature);

    /**
     * Expect there to be a result
     */
    expect(result).to.be.an('array').to.have.length.gt(0);

    /**
     * Expecrt required fields to be present in result
     */
    result.forEach((res) => {
      expect(res).to.have.property('currentPosition');
      expect(res).to.have.property('currentSymbol');
      expect(res).to.have.property('position');
      expect(res).to.have.property('symbolPoistion');
      expect(res).to.have.property('symbolName').eql('Evil Knight');
      expect(res).to.have.property('hasGoodKnightWon');

      /**
       * If Good knight wins expect winnings
       */
      if (res.hasGoodKnightWon) {
        expect(res).to.have.property('winAmount');
      } else {
        // expect win amount key to be absent
        expect(res).to.not.have.property('winAmount');
      }
    });
    done();
  });


  /**
  * Test case to determine adjacent symbol winnings
  */
  it('Test case to determine adjacent symbol winnings, when both Damsel and Evil Knight is present on either sides', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const session = kadTestData.VALID_USER_SESSION;
    const arrays = kadTestData.EVIL_KNIGHT_DAMSEL_ADJACENT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;
    const adjacents = kad.checkIfAdjacentKnights(arrays, noofReels, columnSize, game);
    const result = kad.fetchAdjacentWinnings(adjacents, session.eventData.currentBet, game.default.gameConfig.specialFeature);

    /**
     * Expect there to be a result
     */
    expect(result).to.be.an('array').to.have.length.gt(0);

    /**
     * Expecrt required fields to be present in result
     */
    result.forEach((res) => {
      expect(res).to.have.property('currentPosition');
      expect(res).to.have.property('currentSymbol');
      expect(res).to.have.property('position');
      expect(res).to.have.property('symbolPoistion');
      expect(res).to.have.property('symbolName');
      expect(res).to.have.property('hasGoodKnightWon');

      /**
       * If Good knight wins expect winnings
       */
      if (res.hasGoodKnightWon) {
        expect(res).to.have.property('winAmount');
      } else if (res.symbolName === 'Evil Knight') {
        // expect win amount key to be absent if evil knight is present in adjacent
        expect(res).to.not.have.property('winAmount');
      }
    });
    done();
  });


  /**
  * Test case to determine damsel is present in view zone
  */
  it('Test case to determine damsel present in view zone, when there is no damsel', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const arrays = kadTestData.NO_ADJACENT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;

    const result = kad.damselInFreeSpins(arrays, noofReels, columnSize, game);

    /**
     * Expect there to be an empty result
     */
    expect(result).to.be.an('array').to.have.length(0);

    done();
  });


  /**
  * Test case to determine damsel is present in view zone
  */
  it('Test case to determine damsel present in view zone, when there is single damsel', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const arrays = kadTestData.DAMSEL_ADJACENT_LEFT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;

    const result = kad.damselInFreeSpins(arrays, noofReels, columnSize, game);

    /**
     * Expect there to be an empty result
     */
    expect(result).to.be.an('array').to.have.length.gt(0);
    result.forEach((res) => {
      /**
       * Expect result to have position data
       */
      expect(res).to.have.property('i');
      expect(res).to.have.property('j');
    });

    done();
  });


  /**
  * Test case to determine damsel is present in view zone
  */
  it('Test case to determine damsel present in view zone, when there are multiple damsel symbols', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const arrays = kadTestData.DAMSEL_MULTIPLE_ADJACENT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;

    const result = kad.damselInFreeSpins(arrays, noofReels, columnSize, game);

    /**
     * Expect there to be an empty result
     */
    expect(result).to.be.an('array').to.have.length.gt(0);
    result.forEach((res) => {
      /**
       * Expect result to have position data
       */
      expect(res).to.have.property('i');
      expect(res).to.have.property('j');
    });

    done();
  });


  /**
  * Test case to determine damsel outcome in free spins
  */
  it('Test case to determine damsel outcome, when there is single damsel symbol', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const arrays = kadTestData.DAMSEL_ADJACENT_LEFT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;
    const session = kadTestData.VALID_USER_SESSION;

    const damselPositions = kad.damselInFreeSpins(arrays, noofReels, columnSize, game);
    const result = kad.damselFreeSpinOutput(damselPositions, game, session.eventData.currentBet);

    /**
     * Expect there to be an empty result
     */
    expect(result).to.be.an('array').to.have.length.gt(0);
    result.forEach((res) => {
      /**
       * Expect result to have requireb properties
       */
      expect(res).to.have.property('damselPosition');
      expect(res).to.have.property('winType');

      if (res.winType === 'freeSpin') {
        // expect free spin related property
        expect(res).to.have.property('noofFreeSpinWon');
      } else {
        // expect property related to prizes won
        expect(res).to.have.property('winCoins');
      }
    });

    done();
  });


  /**
  * Test case to determine damsel outcome in free spins
  */
  it('Test case to determine damsel outcome, when there are multiple damsel symbols', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const arrays = kadTestData.DAMSEL_MULTIPLE_ADJACENT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;
    const session = kadTestData.VALID_USER_SESSION;

    const damselPositions = kad.damselInFreeSpins(arrays, noofReels, columnSize, game);
    const result = kad.damselFreeSpinOutput(damselPositions, game, session.eventData.currentBet);

    /**
     * Expect there to be an empty result
     */
    expect(result).to.be.an('array').to.have.length.gt(0);
    result.forEach((res) => {
      /**
       * Expect result to have requireb properties
       */
      expect(res).to.have.property('damselPosition');
      expect(res).to.have.property('winType');

      if (res.winType === 'freeSpin') {
        // expect free spin related property
        expect(res).to.have.property('noofFreeSpinWon');
      } else {
        // expect property related to prizes won
        expect(res).to.have.property('winCoins');
      }
    });

    done();
  });


  /**
  * Test case if bonus is won or not
  */
  it('Test case if bonus is won or not from view zone, when bonus is not won', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const arrays = kadTestData.DAMSEL_MULTIPLE_ADJACENT_SYMBOL_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;
    const bonusSymbolId = kadTestData.BONUS_SYMBOL_ID;

    const result = kad.checkIfBonusWon(game, arrays, columnSize, noofReels, bonusSymbolId);

    /**
     * Expect result to acknowledge that bonus is not won
     */
    expect(result).to.have.property('bonusWon').to.be.eql(false);

    done();
  });


  /**
  * Test case if bonus is won or not
  */
  it('Test case if bonus is won or not from view zone, when bonus not won', (done) => {
    const game = kadTestData.VALID_GAME_DATA.game;
    const arrays = kadTestData.BONUS_WIN_VIEW_ZONE();
    const noofReels = kadTestData.NOOF_REELS;
    const columnSize = kadTestData.COLUMN_SIZE;
    const bonusSymbolId = kadTestData.BONUS_SYMBOL_ID;

    const result = kad.checkIfBonusWon(game, arrays, columnSize, noofReels, bonusSymbolId);
    /**
     * Expect result to acknowledge that bonus is won
     */
    expect(result).to.have.property('bonusWon').to.be.eql(true);

    done();
  });


  /**
  * Test case check bonus document built
  */
  it('Test case check bonus document built', (done) => {
    const game = kadTestData.VALID_GAME_DATA;

    const result = kad.initiateBonus({}, game);
    /**
     * Expect required
     */
    expect(result).to.have.property('bonusSession');
    expect(result).to.have.property('bonusResultDoc');

    /**
     * Expect bonus session doc to have requirebd properties
     */
    expect(result.bonusSession).to.have.property('bonus');
    expect(result.bonusSession.bonus).to.have.property('stage');
    expect(result.bonusSession.bonus.stage).to.have.property('currentStage');
    expect(result.bonusSession.bonus.stage).to.have.property('bonusSessionCards');

    /**
     * Expect bonus session cards to have required properties
     */
    const cards = Object.keys(result.bonusSession.bonus.stage.bonusSessionCards);

    cards.forEach((card) => {
      expect(result.bonusSession.bonus.stage.bonusSessionCards[card]).to.have.property('cardType');
      expect(result.bonusSession.bonus.stage.bonusSessionCards[card]).to.have.property('noofDragons');
    });


    /**
     * Expect response session doc to include required properties
     */
    expect(result.bonusResultDoc).to.have.property('bonus');
    expect(result.bonusResultDoc.bonus).to.have.property('stage');
    expect(result.bonusResultDoc.bonus.stage).to.have.property('currentStage');
    expect(result.bonusResultDoc.bonus.stage).to.have.property('cards').to.be.length.gt(0);

    done();
  });


  /**
  * Test case check moving bonus to next screen/stage
  */
  it('Test case check moving bonus to next screen/stage', async () => {
    const sessionCas = kadTestData.VALID_USER_SESSION_AND_REQUEST_WITH_BONUS;
    const sessionRequestKey = sessionCas.sessionRequestKey;
    const sessionRequectDoc = sessionCas[sessionCas.sessionRequestKey].value;
    const session = sessionCas[sessionCas.sessionKey].value;
    const gameId = kadTestData.VALID_GAME_ID;
    const game = kadTestData.VALID_GAME_DATA.game;
    const bonus = game.default.gameConfig.bonus;
    const cardId = kadTestData.STAGE1_CARD_ID;
    const card = session.eventData.bonus.stage.bonusSessionCards[cardId];
    const cas = kadTestData.VALID_CAS_VALUE;
    const result = await kad.moveBonusToNextStage(session, gameId, bonus, card, sessionRequestKey, sessionRequectDoc, cas);

    /**
     * Expect response session doc to include required properties
     */
    expect(result).to.have.property('result');
    expect(result).to.have.property('bonus');
    expect(result).to.have.property('bonus');
    expect(result.bonus).to.have.property('stage');
    expect(result.bonus.stage).to.have.property('currentStage');
    expect(result.bonus.stage).to.have.property('cards').to.be.length.gt(0);
  });


  /**
  * Test case check picking card in stage 2
  */
  it('Test case check picking card in stage/screen 2, when Damsel is picked', async () => {
    const sessionCas = kadTestData.VALID_USER_SESSION_AND_REQUEST_WITH_BONUS_SCREEN2;
    const sessionRequestKey = sessionCas.sessionRequestKey;
    const sessionRequectDoc = sessionCas[sessionCas.sessionRequestKey].value;
    const session = sessionCas[sessionCas.sessionKey].value;
    const gameId = kadTestData.VALID_GAME_ID;
    const game = kadTestData.VALID_GAME_DATA.game;
    const bonus = game.default.gameConfig.bonus;
    const cardId = kadTestData.STAGE2_CARD_ID;
    const token = kadTestData.VALID_USER_TOKEN;
    const cas = kadTestData.VALID_CAS_VALUE;

    const result = await kad.handlePickDamsel(session, gameId, bonus, cardId, sessionRequestKey, sessionRequectDoc, cas, token);

    /**
     * Expect result to have required properties
     */
    expect(result).to.have.property('isBonusAvailable').to.be.eql(true);
    expect(result).to.have.property('cardType').to.be.eql('Damsel');
    expect(result).to.have.property('winCoins');
    expect(result).to.have.property('currentDragonPicks');
    expect(result).to.have.property('currentPick');
    expect(result).to.have.property('dragonsAvailable');
    expect(result).to.have.property('toalPicks');
  });


  /**
  * Test case check picking card in stage 2
  */
  it('Test case check picking card in stage/screen 2, when Dragon is picked', async () => {
    const sessionCas = kadTestData.VALID_USER_SESSION_AND_REQUEST_WITH_BONUS_SCREEN2;
    const sessionRequestKey = sessionCas.sessionRequestKey;
    const sessionRequectDoc = sessionCas[sessionCas.sessionRequestKey].value;
    const session = sessionCas[sessionCas.sessionKey].value;
    const gameId = kadTestData.VALID_GAME_ID;
    const game = kadTestData.VALID_GAME_DATA.game;
    const bonus = game.default.gameConfig.bonus;
    const cardId = kadTestData.STAGE2_DRAGON_CARD_ID;
    const token = kadTestData.VALID_USER_TOKEN;
    const cas = kadTestData.VALID_CAS_VALUE;

    const result = await kad.handlePickDamsel(session, gameId, bonus, cardId, sessionRequestKey, sessionRequectDoc, cas, token);

    /**
     * Expect result to have required properties
     */
    expect(result).to.have.property('isBonusAvailable').to.be.eql(true);
    expect(result).to.have.property('cardType').to.be.eql('Dragon');
    expect(result).to.not.have.property('winCoins');
    expect(result).to.have.property('currentDragonPicks');
    expect(result).to.have.property('currentPick');
    expect(result).to.have.property('dragonsAvailable');
    expect(result).to.have.property('toalPicks');
  });


  /**
  * Test case check picking card in stage 2
  */
  it('Test case check picking card in stage/screen 2, when Damsel is picked and bonus must end', async () => {
    const sessionCas = kadTestData.VALID_USER_SESSION_TO_END_BONUS2;
    const sessionRequestKey = sessionCas.sessionRequestKey;
    const sessionRequectDoc = sessionCas[sessionCas.sessionRequestKey].value;
    const session = sessionCas[sessionCas.sessionKey].value;
    const gameId = kadTestData.VALID_GAME_ID;
    const game = kadTestData.VALID_GAME_DATA.game;
    const bonus = game.default.gameConfig.bonus;
    const cardId = kadTestData.STAGE2_CARD_ID;
    const token = kadTestData.VALID_USER_TOKEN;
    const cas = kadTestData.VALID_CAS_VALUE;

    const result = await kad.handlePickDamsel(session, gameId, bonus, cardId, sessionRequestKey, sessionRequectDoc, cas, token);
    /**
     * Expect result to have required properties
     */
    expect(result).to.have.property('isBonusAvailable').to.be.eql(false);
    expect(result).to.have.property('cardType').to.be.eql('Damsel');
    expect(result).to.have.property('winCoins');
    expect(result).to.have.property('currentDragonPicks');
    expect(result).to.have.property('currentPick');
    expect(result).to.have.property('dragonsAvailable');
    expect(result).to.have.property('toalPicks');
  });

  // TODO

  /**
  * Test case for gamble request with invalid token
  */
  it('Error Test case for gamble request with invalid token', async () => {
    const response = await kad.handleGambleRequest(kadTestData.INVALID_TOKEN, kadTestData.VALID_GAME_ID, kadTestData.VALID_CLIENT_ID, kadTestData.GAMBLE_RED_CARD);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Test case for gamble request with invalid game id
  */
  it('Error Test case for gamble request with invalid game id', async () => {
    const response = await kad.handleGambleRequest(kadTestData.VALID_USER_TOKEN, testData.INVALID_GAME_ID, kadTestData.VALID_CLIENT_ID, kadTestData.GAMBLE_RED_CARD);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Test case for gamble request with invalid client id
  */
  it('Error Test case for gamble request with invalid client id', async () => {
    const response = await kad.handleGambleRequest(kadTestData.VALID_USER_TOKEN, kadTestData.VALID_GAME_ID, kadTestData.INVALID_CLIENT_ID, kadTestData.GAMBLE_RED_CARD);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Test case for gamble request with invalid card
  */
  it('Error Test case for gamble request with invalid card', async () => {
    const response = await kad.handleGambleRequest(kadTestData.VALID_USER_TOKEN, kadTestData.VALID_GAME_ID, kadTestData.VALID_CLIENT_ID, kadTestData.GAMBLE_INVALID_CARD);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Test case for gamble request with null game parameters
  */
  it('Error Test case for gamble request with invalid card', async () => {
    const response = await kad.handleGambleRequest(null, null, null, null);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Test case for gamble request with all valid parameters to check gamble availability
  */
  it('Test case to check gamble availability', async () => {
    const token = kadTestData.VALID_USER_TOKEN;
    const gameId = kadTestData.VALID_GAME_ID;
    const clientID = kadTestData.VALID_CLIENT_ID;
    const card = kadTestData.GAMBLE_RED_CARD;
    const response = await kad.handleGambleRequest(token, gameId, clientID, card);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Test case for gamble request with all valid parameters
  */
  it('Successful Test case to check gamble, for required fields in response', async () => {
    const token = kadTestData.VALID_USER_TOKEN;
    const gameId = kadTestData.ALTERNATE_VALID_GAME_ID;
    const clientID = kadTestData.VALID_CLIENT_ID;
    const card = kadTestData.GAMBLE_RED_CARD;
    const response = await kad.handleGambleRequest(token, gameId, clientID, card);

      // expect required fields to be present with validity
    if (response.isGambleAvailable === true) {
        // when user wins
      expect(response).to.have.property('gambleWon').to.be.eql(true);
      expect(response).to.have.property('gambleCount').to.be.eql(1);
      expect(response).to.have.property('gambleTotalAmount').to.be.eql(500);
      expect(response).to.have.property('gambleBetAmount').to.be.eql(500);
      expect(response).to.have.property('gamblePotentialWinAmount').to.be.eql(response.gambleTotalAmount * 2);
      expect(response).to.have.property('gambleHistory');
      expect(response).to.have.property('wallet');
      expect(response).to.have.property('isGambleAvailable').to.be.eql(true);
    } else {
        // when user looses
      expect(response).to.have.property('gambleCount').to.be.eql(0);
      expect(response).to.have.property('gambleWon').to.be.eql(false);
      expect(response).to.have.property('gambleTotalAmount');
      expect(response).to.have.property('gambleHistory');
      expect(response).to.have.property('wallet');
      expect(response).to.have.property('isGambleAvailable').to.be.eql(false);
    }

    expect(response.gambleHistory).to.have.length.lt(6);
  });


  /**
  * Collect winnings test case for invalid token
  */
  it('Error Test case to collect winnings, invalid token', async () => {
    const token = kadTestData.INVALID_USER_TOKEN;
    const gameId = kadTestData.VALID_GAME_ID;
    const clientID = kadTestData.VALID_CLIENT_ID;
    const response = await kad.pickGamble(token, gameId, clientID);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Collect winnings test case for invalid game id
  */
  it('Error Test case to collect winnings, invalid game id', async () => {
    const token = kadTestData.VALID_USER_TOKEN;
    const gameId = testData.INVALID_GAME_ID;
    const clientID = kadTestData.VALID_CLIENT_ID;
    const response = await kad.pickGamble(token, gameId, clientID);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Collect winnings test case for invalid client id
  */
  it('Error Test case to collect winnings, invalid client id', async () => {
    const token = kadTestData.VALID_USER_TOKEN;
    const gameId = kadTestData.VALID_GAME_ID;
    const clientID = kadTestData.INVALID_CLIENT_ID;
    const response = await kad.pickGamble(token, gameId, clientID);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Collect winnings test case for valid parameters
  */
  it('Successful Test case to decline collect winnings, when there is no win', async () => {
    const token = kadTestData.VALID_USER_TOKEN;
    const gameId = testData.VALID_GAME_ID;
    const clientID = kadTestData.VALID_CLIENT_ID;
    const response = await kad.pickGamble(token, gameId, clientID);

    /**
     * Expect error response
     */
    expect(response).to.have.property('error');
  });


  /**
  * Collect winnings test case for valid parameters
  */
  it('Successful Test case to accept collect winnings, when there is win', async () => {
    const token = kadTestData.VALID_USER_TOKEN;
    const gameId = kadTestData.PICK_GAMBLE_GAME_ID;
    const clientID = kadTestData.VALID_CLIENT_ID;
    const response = await kad.pickGamble(token, gameId, clientID);

    /**
     * Expect response to have valid properties
     */
    expect(response).to.have.property('collectAmount');
    expect(response).to.have.property('wallet');
  });
});
