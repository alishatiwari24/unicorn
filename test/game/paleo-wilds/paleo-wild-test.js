const chai = require('chai');
const sinon = require('sinon');
const paleoData = require('./test-data');
const paleo = require('../../../util/games/paleo-wilds');

const expect = chai.expect;


/**
* Paloe Wilds related test cases are performed
* Paloe Wilds tests include following tests
* - Test User session computation for game click
* - Fetching user session with required reel set
* - Fetching reel configuration for subsequent spin in main game and free game
*/
describe('PaleoWildTest', () => {
  /**
  * before each tests required stubs are created using sinon library
  * stubs are creatd to eliminate external factors from participating in tests
  */
  beforeEach(() => {
    /**
    * stub to mock getting user session
    */
    sinon.stub(paleo, 'getUserSession').callsFake((token, gameId) => {
      if (gameId === paleoData.VALID_GAME_ID && token === paleoData.VALID_USER_TOKEN) {
        return Promise.resolve(paleoData.VALID_USER_SESSION);
      }
      return Promise.reject();
    });
  });


  /**
  * after each test stubs are restored
  * restoration happpens for other tests to create stubs
  */
  afterEach(() => {
    paleo.getUserSession.restore();
  });


  /**
  * Getting user session error test case when user token is not valid
  */
  it('Getting user session error test case, invalid user token', (done) => {
    paleo.getUserSession(paleoData.INVALID_USER_TOKEN, paleoData.VALID_GAME_ID)
    .then((result) => {})
    .catch(() => {
      /**
       * Expect that promise is rejected
       */
      done();
    });
  });


  /**
  * Getting user session error test case when user token is null
  */
  it('Getting user session error test case, null user token', (done) => {
    paleo.getUserSession(null, paleoData.VALID_GAME_ID)
    .then((result) => {})
    .catch(() => {
      /**
       * Expect that promise is rejected
       */
      done();
    });
  });


  /**
  * Getting user session error test case when game id is not valid
  */
  it('Getting user session error test case, invalid game id', (done) => {
    paleo.getUserSession(paleoData.VALID_USER_TOKEN, paleoData.INVALID_GAME_ID)
    .then((result) => {})
    .catch(() => {
      /**
       * Expect that promise is rejected
       */
      done();
    });
  });


  /**
  * Getting user session error test case when game id is null
  */
  it('Getting user session error test case, null game id', (done) => {
    paleo.getUserSession(paleoData.VALID_USER_TOKEN, null)
    .then((result) => {})
    .catch(() => {
      /**
       * Expect that promise is rejected
       */
      done();
    });
  });


  /**
  * successful test case when user session is fetched with valid inputs
  */
  it('Getting user session successful test case', (done) => {
    paleo.getUserSession(paleoData.VALID_USER_TOKEN, paleoData.VALID_GAME_ID)
    .then((data) => {
      /**
       * Expect that function to resolve valid session data
       */
      expect(data).to.have.nested.property('clientID');
      expect(data).to.have.nested.property('eventData');

      /**
       * Expect session data to be present in response
       */
      expect(data).to.have.nested.property('eventData.currentBet');
      expect(data).to.have.nested.property('eventData.currentLines');
      expect(data).to.have.nested.property('eventData.userDetails');
      expect(data).to.have.nested.property('eventData.freeSpinData');
      expect(data).to.have.nested.property('eventData.userDetails.id');
      expect(data).to.have.nested.property('eventData.userDetails.name');
      expect(data).to.have.nested.property('eventData.userDetails.gender');
      expect(data).to.have.nested.property('eventData.userDetails.locale');
      expect(data).to.have.nested.property('eventData.userDetails.birthday');
      expect(data).to.have.nested.property('eventData.userDetails.level');
      expect(data).to.have.nested.property('eventData.userDetails.levelProgress');
      expect(data).to.have.nested.property('eventData.userDetails.multiplier');
      expect(data).to.have.nested.property('eventData.userDetails.id');


      /**
       * Expect there to be game settings
       */
      expect(data).to.have.nested.property('game');
      expect(data).to.have.nested.property('_updatedTimestamp');
      expect(data).to.have.nested.property('game.betAmount');
      expect(data).to.have.nested.property('game.settings');
      expect(data).to.have.nested.property('game.settings.bets').to.have.length.gt(0);
      expect(data).to.have.nested.property('game.settings.maxBetMultiplier');
      expect(data).to.have.nested.property('game.settings.minBet');
      expect(data).to.have.nested.property('game.settings.minBetMultiplier');


      /**
       * Expect game specific properties to be present in session
       */
      expect(data).to.have.nested.property('eventData.currentReelSet');
      done();
    });
  });


  /**
  * Test case to get reel set for sub sequent spin
  */
  it('Test case to get reel set for sub sequent spin when there are no free spins', (done) => {
    const result = paleo.getSpinReelConfiguration(paleoData.VALID_USER_SESSION, paleoData.VALID_GAME_DATA);

    /**
     * Expect that result has required propoerties
     */
    expect(result.reelConfig).to.have.nested.property('NumberOfReels').which.is.a('number');
    /**
     * Expect that reel configuration is valid and has valid symbols according to number of rows
     */
    for (let i = 0; i < result.NumberOfReels; i += 1) {
      expect(result.reelConfig).to.have.nested.property(`Reel${i + 1}`);
      expect(result.reelConfig[`Reel${i + 1}`]).to.have.nested.property('NumberOfRows');
      expect(result.reelConfig[`Reel${i + 1}`]).to.have.nested.property('SymbolDistribution');
      expect(result.reelConfig[`Reel${i + 1}`].NumberOfRows).to.have.length.eql(result[`Reel${i + 1}`].SymbolDistribution.length);
    }
    done();
  });


  /**
  * Test case to get reel set for sub sequent free spin
  */
  it('Test case to get reel set for sub sequent spin when free spin is active', (done) => {
    const result = paleo.getSpinReelConfiguration(paleoData.VALID_USER_SESSION_FREE_GAME, paleoData.VALID_GAME_DATA);
    /**
     * Expect that reel configuration is valid and has valid symbols according to number of rows
     */
    for (let i = 0; i < result.NumberOfReels; i += 1) {
      expect(result).to.have.nested.property(`Reel${i + 1}`);
      expect(result[`Reel${i + 1}`]).to.have.nested.property('NumberOfRows');
      expect(result[`Reel${i + 1}`]).to.have.nested.property('SymbolDistribution');
      expect(result[`Reel${i + 1}`].NumberOfRows).to.have.length.eql(result[`Reel${i + 1}`].SymbolDistribution.length);
    }
    done();
  });


  /**
  * Test case to get the index of next reel set for subsequent spin when free spins are not active
  */
  it('Test case to get the index of next reel set for subsequent spin when free spins are not active', (done) => {
    const result = paleo.getMainGameReelSet(paleoData.VALID_USER_SESSION, paleoData.VALID_GAME_DATA);
    /**
     * Expect that result is required index of reel set in main game
     */
    let nextConfig = paleoData.VALID_USER_SESSION.eventData.currentReelSet - 1;
    if (nextConfig < 0) {
      nextConfig = paleoData.VALID_GAME_DATA.game.default.gameConfig.specialFeatures.mainGame.length - 1;
    }
    expect(result).which.is.a('number').to.be.eql(nextConfig);

    done();
  });


  /**
  * Test case to get the index of next reel set for subsequent spin when free spins are active
  */
  it('Test case to get the index of next reel set for subsequent spin when free spins are active', (done) => {
    let result = paleo.getFreeGameReelSet(paleoData.VALID_USER_SESSION_FREE_GAME.eventData.freeSpinData.currentReelSet1, paleoData.VALID_GAME_DATA);
    /**
     * Expect that result is required index of reel set in free game
     */
    let nextConfig = paleoData.VALID_USER_SESSION_FREE_GAME.eventData.freeSpinData.currentReelSet1 - 1;
    if (nextConfig < 0) {
      nextConfig = paleoData.VALID_GAME_DATA.game.default.gameConfig.specialFeatures.freeGame.length - 1;
    }
    expect(result).which.is.a('number').to.be.eql(nextConfig);

    result = paleo.getFreeGameReelSet(paleoData.VALID_USER_SESSION_FREE_GAME.eventData.freeSpinData.currentReelSet2, paleoData.VALID_GAME_DATA);
    nextConfig = paleoData.VALID_USER_SESSION_FREE_GAME.eventData.freeSpinData.currentReelSet2 - 1;
    if (nextConfig < 0) {
      nextConfig = paleoData.VALID_GAME_DATA.game.default.gameConfig.specialFeatures.freeGame.length - 1;
    }
    expect(result).which.is.a('number').to.be.eql(nextConfig);
    done();
  });
});
