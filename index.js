/* eslint-disable new-cap, no-console, max-params */

const finalhandler = require('finalhandler');
const http = require('http');
const Router = require('router');
const serveStatic = require('serve-static');
const bugsnag = require('bugsnag');

const index = require('./routes');
const config = require('./config');
const constants = require('./response/constants');
const bodyParser = require('body-parser');
const compression = require('compression');
const cors = require('cors');
const gamefunction = require('./helpers/gamefunction');
const responseDispatcher = require('./util/responsedispatcher');
const prometheus = require('./api/prometheus');

const router = Router();

/**
*enables cross origin request
*/
router.use(cors());
/**
* compress responses
*/
router.use(compression());

/**
* Use body parser to parse request body
*/
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));
/**
* Register bugsnag with apiKey
*/
bugsnag.register(config.bugsnag.apiKey);

/**
* Use bugsnag request handler
*/
router.use(bugsnag.requestHandler);

/**
* Use bugsnag error handler
*/
router.use(bugsnag.errorHandler);

router.route('/*').get(index).post(index);

/**
* Expose response time metrics for prometheus
*/
router.use('/metrics', (req, res) => {
  res.write(prometheus.register.metrics());
  res.end();
});

  /**
  *@api {get} /ping health check api
  *@apiName health check
  *@apiGroup health-check
  *@apiVersion 1.0.0
  *@apiSuccess (Success 200) {json} Success-Response:
    {
      "data":"pong"
    }
  */
router.use('/ping', (req, res) => {
  responseDispatcher.dispatchPing(res);
});

/**
* serve apidoc as static html using serve static
*/
router.use('/apidoc', (req, res) => {
  const serve = serveStatic(`${__dirname}/apidoc`, { index: ['index.html'] });
  serve(req, res, finalhandler(req, res));
});


/**
* handle 404 - not found
*/
router.get('/*', (req, res) => {
  responseDispatcher.dispatch404(res);
});

/**
* global exception handler
* does maintainance task
*/
router.use((err, req, res) => {
  console.log('unhandled error', err);
  responseDispatcher.dispatchError(res, { error: constants.RES_MESSAGES.SOMETHING_WENT_WRONG, erroCode: constants.RES_ERROR_CODES.SOMETHING_WENT_WRONG });
});
/**
* Create Http server
*/
const server = http.createServer((req, res) => {
	/**
	* attach final handler to route Object in order to route every request
	*/
  router(req, res, finalhandler(req, res));
});

/**
* Server listens on given httpPort
*/
server.listen(config.application.httpPort);
console.log(`server started on port number: ${config.application.httpPort}`);
gamefunction.gameLookupLoop();
