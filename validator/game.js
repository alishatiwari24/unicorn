const Joi = require('joi');
const responseDispatcher = require('../util/responsedispatcher');
const gameFunction = require('../helpers/gamefunction');
const constants = require('../response/constants');

class GameValidator {

  /**
   * validates game click api parameters
   * would check of all needed parameters
   * would pass an relevant error id some fields are missing
   * and if some fields are present that are not expected
   */
  validateGameClick(req, res, next) {
    const obj = {
      clientID: Joi.string().required(),
      token: Joi.string().required(),
      gameId: Joi.string().required()
    };
    const gameClickSchema = Joi.object().keys(obj).unknown();
    Joi.validate(req.body, gameClickSchema, (err) => {
      if (err === null) {
        return next();
      }
      return responseDispatcher.dispatchValidationError(res, err.message);
    });
  }


  /**
   * validates getting mobile version api parameters
   * would check all needed parameters
   * would pass an relevant error, if some fields are missing,
   * if some fields are present that are not expected
   */
  validateMobileVersion(req, res, next) {
    const bodyObj = {
      token: Joi.string().required(),
      gameId: Joi.string().required()
    };
    const queryObj = { platform: Joi.string().required() };

    Joi.validate(req.body, bodyObj, (err) => {
      if (err === null) {
        Joi.validate(req.query, queryObj, (inerr) => {
          if (inerr === null) {
            return next();
          }
          return responseDispatcher.dispatchValidationError(res, err.message);
        });
      } else {
        responseDispatcher.dispatchValidationError(res, err.message);
      }
    });
  }


  /**
   * validates noble musketeers gamble pick API
   * it must contain clientID, token, pickedCard and gameId
   */
  validateSOAGamble(req, res, next) {
    const obj = {
      clientID: Joi.string().required(),
      token: Joi.string().required(),
      gameId: Joi.string().required(),
      pickedCard: Joi.string().required()
    };
    const soaGambleSchema = Joi.object().keys(obj).unknown();
    Joi.validate(req.body, soaGambleSchema, (err) => {
      if (err === null) {
        return next();
      }
      return responseDispatcher.dispatchValidationError(res, err.message);
    });
  }


  /**
   * validates noble musketeers gamble pick API
   * it must contain clientID, token and gameId
   */
  validateSOAGamblePick(req, res, next) {
    const obj = {
      clientID: Joi.string().required(),
      token: Joi.string().required(),
      gameId: Joi.string().required()
    };
    const soaGambleSchema = Joi.object().keys(obj).unknown();
    Joi.validate(req.body, soaGambleSchema, (err) => {
      if (err === null) {
        return next();
      }
      return responseDispatcher.dispatchValidationError(res, err.message);
    });
  }


  /**
   * validates noble musketeers pick bonus api
   * it must contain clientID, token and gameId
   * along with cardId, cardId must be the index of the card, which is to be picked
   * say for example, user gets total 3 musketeers in any spin, so he gets to pick one from 3 options
   * and client should send cardId as one of [0, 1, 2] values, (indices of all cards)
   */
  validateNMBonusPick(req, res, next) {
    const obj = {
      clientID: Joi.string().required().error(new Error('client id is required, and it must be a string')),
      token: Joi.string().required().error(new Error('token is required, and it must be a string')),
      gameId: Joi.string().required().error(new Error('game id is required, and it must be a string')),
      cardId: Joi.number().required().error(new Error('cardId is required, and it must be a number'))
    };
    const soaGambleSchema = Joi.object().keys(obj).unknown();
    Joi.validate(req.body, soaGambleSchema, (err) => {
      if (err === null) {
        return next();
      }
      return responseDispatcher.dispatchValidationError(res, { error: err.message, erroCode: constants.RES_ERROR_CODES.VALIDATION_ERROR });
    });
  }


  /**
   * validates all requests, except *get all playable games*
   * it ensures that request body must contain baseGameId
   * it gets start version's ID for given baseGameId, and assigns it inside request body
   * if the baseGameId does not exist in baseGame-mapping, it returns *Invalid game* error
   */
  validateGameId(req, res, next) {
    const baseGameId = req.body.gameId;
    const gameId = gameFunction.getGameIdFromBaseGameId(baseGameId);
    req.body.gameId = gameId;
    req.body.baseGameId = baseGameId;
    if (!gameId && req.url !== '/game/all') {
      return responseDispatcher.dispatchValidationError(res, { error: 'Invalid game', errorCode: constants.RES_ERROR_CODES.INVALID_GAME });
    }
    return next();
  }
}
module.exports = new GameValidator();
