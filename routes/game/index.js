/* eslint-disable new-cap */
const Router = require('router');
const gameController = require('../../controller/game');
const validator = require('../../validator/game');
const unicorn = require('../../controller/q219/unicorn.js');
// const unicornSlotController = require('../../controller/unicorn-slot');
const secretOfAmunController = require('../../controller/secret-of-amun');
const constants = require('../../response/constants');
const parisTouristController = require('../../controller/paris-tourist');
const zombieApocController = require('../../controller/zombie-apocalypse');
const knightDragonsController = require('../../controller/knight-dragons');
const backStreet = require('../../controller/backer-street-detective');
const cinderella = require('../../controller/cinderella');
const pyramidsOfGiza = require('../../controller/pyramids-of-giza');
const pacificRailRoad = require('../../controller/pacific-rail-road');
const qinDynasty = require('../../controller/qin-dynasty');
const gatsby = require('../../controller/gatsby');
const nobleMusketeers = require('../../controller/noble-musketeers');
const jungleStory = require('../../controller/jungle-story');
const poseidon = require('../../controller/poseidon');
const zeus = require('../../controller/zeus');
const paleo = require('../../controller/paleo-wilds');
const deepSeas = require('../../controller/deep-seas');
const dragonPhoenix = require('../../controller/dragon-phoenix');
const casanova = require('../../controller/q219/casanova.js');
const divineSpirit = require('../../controller/q219/divine-spirit.js');
const carnevale = require('../../controller/carnevale');

const router = Router();
/**
  *@api {post} /game/game_click game click
  *@apiName game click
  *@apiGroup Game
  *@apiParamExample {json} Request-Example
  {
    "token":"597b2856502b2f09f265a1fa",
    "clientID":"qsGurtExxuz6LoLzMGKtHpGLpsLyMyIp",
    "gameId":"b5a2ce27-42c2-41f4-bbad-c5469649373a"
  }
  *@apiSuccess {Integer} Success-Code 200
  *@apiSuccessExample {json} Success-Response:
  *
  {
	"isError": false,
	"data": {
		"game": {
			"gameId": "cd83495b-1594-4100-99f6-9b68fa8d1b03",
			"viewZone": 35,
			"reels": {
				"noofReels": 5,
				"reel1": ["577a2b0c-c27d-4d11-ade3-f05a27821326", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "577a2b0c-c27d-4d11-ade3-f05a27821326"],
				"reel2": ["76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "9dc7d76c-0291-4ca4-b0de-c960d19af3a8"],
				"reel3": ["0511eae3-78b9-45c8-9355-ce6c333b0d07", "577a2b0c-c27d-4d11-ade3-f05a27821326", "31829440-b741-4c3d-98d2-ce3a4de3452f"],
				"reel4": ["76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "31b99cbb-7e07-482c-9a8e-b88cdd32f0fb", "03f7d9e3-288c-41e3-a641-197c2b08c7f4"],
				"reel5": ["31b99cbb-7e07-482c-9a8e-b88cdd32f0fb", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f"]
			},
			"payArray": [{
				"_id": "31444f64-98a1-4581-8b5d-39b256bc1659",
				"five": 1,
				"four": 1,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 1,
				"three": 1,
				"two": 1
			}, {
				"_id": "c57a5b16-029c-4309-b001-3f485d99ac0a",
				"five": 0,
				"four": 0,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 0,
				"three": 0,
				"two": 0
			}, {
				"_id": "fcd3cf66-c565-44a4-8bea-9aeef94da65c",
				"five": 2,
				"four": 2,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 2,
				"three": 2,
				"two": 2
			}, {
				"_id": "a0cf6662-6eda-45fe-b598-54e095ef8b42",
				"five": 0,
				"four": 1,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 0,
				"three": 2,
				"two": 1
			}, {
				"_id": "ecd88ed3-2f86-4c65-aedd-ce8cde062fce",
				"five": 2,
				"four": 1,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 2,
				"three": 0,
				"two": 1
			}, {
				"_id": "5213e43b-0e38-48f6-8395-2498d664bacd",
				"five": 1,
				"four": 0,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 1,
				"three": 0,
				"two": 0
			}, {
				"_id": "3d1512d7-5091-44dc-9236-900c35bbfe21",
				"five": 1,
				"four": 2,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 1,
				"three": 2,
				"two": 2
			}, {
				"_id": "6b762a7a-3195-4685-92f7-e9584e7ca689",
				"five": 2,
				"four": 2,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 0,
				"three": 1,
				"two": 0
			}, {
				"_id": "c3c6adcf-eb14-48ce-b8d2-1b43c7580b3e",
				"five": 0,
				"four": 0,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 2,
				"three": 1,
				"two": 2
			}, {
				"_id": "61fa94b6-b85e-48af-a8d9-58aef7e87c85",
				"five": 1,
				"four": 2,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 1,
				"three": 1,
				"two": 0
			}, {
				"_id": "1e3be6a1-b6d6-43c3-aa0d-7b09b4e4bc22",
				"five": 1,
				"four": 0,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 1,
				"three": 1,
				"two": 2
			}, {
				"_id": "06e2e796-4f4b-4529-b120-ffa901843283",
				"five": 0,
				"four": 1,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 0,
				"three": 1,
				"two": 1
			}, {
				"_id": "bd36e3f5-d88c-4cdc-918d-bbde8239481e",
				"five": 2,
				"four": 1,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 2,
				"three": 1,
				"two": 1
			}, {
				"_id": "854110c3-0357-4507-b539-644849124c7c",
				"five": 0,
				"four": 1,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 0,
				"three": 0,
				"two": 1
			}, {
				"_id": "f1761803-446c-4804-a955-65c88343467a",
				"five": 2,
				"four": 1,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 2,
				"three": 2,
				"two": 1
			}, {
				"_id": "292eb020-27c3-47ff-8f6e-bc513c0aa984",
				"five": 1,
				"four": 1,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 1,
				"three": 0,
				"two": 1
			}, {
				"_id": "090dd7a9-a284-4ff0-a98a-09c4784c4661",
				"five": 1,
				"four": 1,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 1,
				"three": 2,
				"two": 1
			}, {
				"_id": "a0b99dc8-5a96-4ab5-a762-14111d64df1f",
				"five": 0,
				"four": 0,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 0,
				"three": 2,
				"two": 0
			}, {
				"_id": "b1990053-2a8c-46d2-b41b-6b886a6d8150",
				"five": 2,
				"four": 2,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 2,
				"three": 0,
				"two": 2
			}, {
				"_id": "05a73120-02b2-4eef-8c92-b3f3382c84e1",
				"five": 0,
				"four": 2,
				"keys": ["one", "two", "three", "four", "five"],
				"one": 0,
				"three": 2,
				"two": 2
			}],
			"payTable": {
				"03f7d9e3-288c-41e3-a641-197c2b08c7f4": {
					"3": {
						"amount": 0,
						"multiplier": 0.54
					},
					"4": {
						"amount": 0,
						"multiplier": 1.44
					},
					"5": {
						"amount": 0,
						"multiplier": 3.6
					}
				},
				"0511eae3-78b9-45c8-9355-ce6c333b0d07": {
					"2": {
						"amount": 0,
						"multiplier": 0.9
					},
					"3": {
						"amount": 0,
						"multiplier": 1.8
					},
					"4": {
						"amount": 0,
						"multiplier": 36
					},
					"5": {
						"amount": 0,
						"multiplier": 90
					}
				},
				"21d29c3f-7d79-43a6-a4cb-cdae646cd625": {
					"2": {
						"amount": 0,
						"multiplier": 1.08
					},
					"3": {
						"amount": 0,
						"multiplier": 1.35
					},
					"4": {
						"amount": 0,
						"multiplier": 7.2
					},
					"5": {
						"amount": 0,
						"multiplier": 9
					}
				},
				"31829440-b741-4c3d-98d2-ce3a4de3452f": {
					"3": {
						"amount": 0,
						"multiplier": 0.4
					},
					"4": {
						"amount": 0,
						"multiplier": 0.72
					},
					"5": {
						"amount": 0,
						"multiplier": 1.8
					}
				},
				"31b99cbb-7e07-482c-9a8e-b88cdd32f0fb": {
					"3": {
						"amount": 0,
						"multiplier": 0.54
					},
					"4": {
						"amount": 0,
						"multiplier": 1.44
					},
					"5": {
						"amount": 0,
						"multiplier": 3.6
					}
				},
				"4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa": {
					"3": {
						"amount": 0,
						"multiplier": 0.135
					},
					"4": {
						"amount": 0,
						"multiplier": 0.72
					},
					"5": {
						"amount": 0,
						"multiplier": 1.8
					}
				},
				"4ed22cb7-5ad0-401a-b373-dc19c42b69a9": {
					"2": {
						"amount": 0,
						"multiplier": 0.67
					},
					"3": {
						"amount": 0,
						"multiplier": 0.9
					},
					"4": {
						"amount": 0,
						"multiplier": 2.16
					},
					"5": {
						"amount": 0,
						"multiplier": 5.4
					}
				},
				"577a2b0c-c27d-4d11-ade3-f05a27821326": {
					"3": {
						"amount": 0,
						"multiplier": 0.4
					},
					"4": {
						"amount": 0,
						"multiplier": 0.72
					},
					"5": {
						"amount": 0,
						"multiplier": 1.8
					}
				},
				"76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f": {
					"3": {
						"amount": 0,
						"multiplier": 0.27
					},
					"4": {
						"amount": 0,
						"multiplier": 0.36
					},
					"5": {
						"amount": 0,
						"multiplier": 0.9
					}
				},
				"8de45961-8f0f-4dec-924e-2873e25224ee": {
					"3": {
						"amount": 0,
						"multiplier": 0.27
					},
					"4": {
						"amount": 0,
						"multiplier": 0.36
					},
					"5": {
						"amount": 0,
						"multiplier": 0.9
					}
				},
				"9dc7d76c-0291-4ca4-b0de-c960d19af3a8": {
					"2": {
						"amount": 0,
						"multiplier": 0.81
					},
					"3": {
						"amount": 0,
						"multiplier": 0.9
					},
					"4": {
						"amount": 0,
						"multiplier": 3.6
					},
					"5": {
						"amount": 0,
						"multiplier": 9
					}
				}
			},
			"symbols": {
				"03f7d9e3-288c-41e3-a641-197c2b08c7f4": {
					"symbolName": "Zombie2",
					"symbolType": "High Paying",
					"symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/71d50423-3cad-499c-b9e2-8cbc3e21a149/zombie2",
					"freeGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					},
					"mainGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					}
				},
				"0511eae3-78b9-45c8-9355-ce6c333b0d07": {
					"symbolName": "Wild",
					"symbolType": "Wild",
					"symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/71d50423-3cad-499c-b9e2-8cbc3e21a149/wild",
					"doesWildPay": true,
					"freeGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					},
					"isExpandingWild": false,
					"mainGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					}
				},
				"21d29c3f-7d79-43a6-a4cb-cdae646cd625": {
					"symbolName": "Zombie1",
					"symbolType": "High Paying",
					"symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/71d50423-3cad-499c-b9e2-8cbc3e21a149/zombie1",
					"freeGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					},
					"mainGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					}
				},
				"31829440-b741-4c3d-98d2-ce3a4de3452f": {
					"symbolName": "Puddle",
					"symbolType": "Low Paying",
					"symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/71d50423-3cad-499c-b9e2-8cbc3e21a149/puddle",
					"freeGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					},
					"mainGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					}
				},
				"31b99cbb-7e07-482c-9a8e-b88cdd32f0fb": {
					"symbolName": "Cop",
					"symbolType": "High Paying",
					"symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/71d50423-3cad-499c-b9e2-8cbc3e21a149/cop",
					"freeGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					},
					"mainGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					}
				},
				"4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa": {
					"symbolName": "Free Spin",
					"symbolType": "Scatter",
					"symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/71d50423-3cad-499c-b9e2-8cbc3e21a149/free_spin",
					"freeGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					},
					"mainGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					}
				},
				"4ed22cb7-5ad0-401a-b373-dc19c42b69a9": {
					"symbolName": "Crow",
					"symbolType": "High Paying",
					"symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/71d50423-3cad-499c-b9e2-8cbc3e21a149/crow",
					"freeGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					},
					"mainGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					}
				},
				"577a2b0c-c27d-4d11-ade3-f05a27821326": {
					"symbolName": "Eye",
					"symbolType": "Low Paying",
					"symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/71d50423-3cad-499c-b9e2-8cbc3e21a149/eye",
					"freeGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					},
					"mainGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					}
				},
				"76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f": {
					"symbolName": "Jeep",
					"symbolType": "Low Paying",
					"symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/71d50423-3cad-499c-b9e2-8cbc3e21a149/jeep",
					"freeGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					},
					"mainGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					}
				},
				"8de45961-8f0f-4dec-924e-2873e25224ee": {
					"symbolName": "Shotgun",
					"symbolType": "Low Paying",
					"symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/71d50423-3cad-499c-b9e2-8cbc3e21a149/shotgun",
					"freeGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					},
					"mainGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					}
				},
				"9dc7d76c-0291-4ca4-b0de-c960d19af3a8": {
					"symbolName": "Lady",
					"symbolType": "High Paying",
					"symbolUrl": "https://slot-game-symbols.s3.amazonaws.com/71d50423-3cad-499c-b9e2-8cbc3e21a149/lady",
					"freeGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					},
					"mainGame": {
						"2 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"3 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"4 of a kind": {
							"combination": 0,
							"probability": 0
						},
						"5 of a kind": {
							"combination": 0,
							"probability": 0
						}
					}
				}
			}
		},
		"userDetails": {
			"user": {
				"id": "597b1378502b2f09f265a1f0",
				"name": "darhan",
				"gender": "na",
				"wallet": {
					"chips": 11182564636439
				},
				"locale": "de_DE",
				"birthday": "1-1-1970",
				"level": 30,
				"levelProgress": 0.14,
				"multiplier": 1
			},
			"game": {
				"settings": {
					"minBet": 100,
					"maxBetMultiplier": 10,
					"minBetMultiplier": 5,
					"bets": [80, 200, 400, 800, 1600, 2400, 3200, 4800, 5600, 6400, 8000, 10000, 12000, 16000, 24000, 28000, 32000, 40000, 60000, 92000]
				}
			}
		}
	}
}
*/
router.post('/game/game_click', validator.validateGameClick, (req, res) => {
  switch (req.body.baseGameId) {
    // case constants.UNICORN:
    //   unicornController.default.gameClick(req, res);
    //   break;

    // case constants.GAME_IDS.UNICORN_SLOT:
    //   unicornSlotController.gameClick(req, res);
    //   break;
    case constants.GAME_IDS.PARIS_TOURIST:
      parisTouristController.gameClick(req, res);
      break;
    case constants.GAME_IDS.ZOMBIE_APOCALYPSE:
      zombieApocController.gameClick(req, res);
      break;
    case constants.GAME_IDS.KNIGHT_DRAGONS:
      knightDragonsController.gameClick(req, res);
      break;
    case constants.GAME_IDS.SECRET_OF_AMUN:
      secretOfAmunController.gameClick(req, res);
      break;
    case constants.GAME_IDS.CINDERELLA:
      cinderella.gameClick(req, res);
      break;
    case constants.GAME_IDS.PALEO_WILDS:
      paleo.gameClick(req, res);
      break;
    case constants.GAME_IDS.GATSBY:
      gatsby.gameClick(req, res);
      break;
    case constants.GAME_IDS.NOBLE_MUSKETEERS:
      nobleMusketeers.gameClick(req, res);
      break;
    case constants.GAME_IDS.DEEP_SEAS:
      deepSeas.gameClick(req, res);
      break;
    case constants.GAME_IDS.DRAGON_PHOENIX:
      dragonPhoenix.gameClick(req, res);
      break;
    case constants.GAME_IDS.PYRAMIDS_OF_GIZA:
      pyramidsOfGiza.gameClick(req, res);
      break;
    case constants.GAME_IDS.PACIFIC_RAIL_ROAD:
      pacificRailRoad.gameClick(req, res);
      break;
    default:
      gameController.gameClick(req, res);
  }
});

/**
  *@api {post} /game/game_event?event=increaseBet increase bet
  *@apiName increase bet
  *@apiGroup Game Event
  *@apiVersion 1.0.0
  @apiParamExample {json} Request-Example
  {
    "token":"597b2856502b2f09f265a1fa",
    "gameId":"b5a2ce27-42c2-41f4-bbad-c5469649373a"
  }
  *@apiSuccessExample {json} Success-Response:
  *
    {
        "isError": false,
        "data":
        {
            "currentBet": 200
        }
    }

*/

/**
  *@api {post} /game/game_event?event=decreaseBet decrease bet
  *@apiName decrease bet
  *@apiGroup Game Event
  *@apiVersion 1.0.0
  @apiParamExample {json} Request-Example
  {
    "token":"597b2856502b2f09f265a1fa",
    "gameId":"b5a2ce27-42c2-41f4-bbad-c5469649373a"
  }
  *@apiSuccessExample {json} Success-Response:
  *
    {
        "isError": false,
        "data":
        {
            "currentBet": 100
        }
    }

*/

/**
  *@api {post} /game/game_event?event=increaseBetLine increase bet lines
  *@apiName increase bet line
  *@apiGroup Game Event
  *@apiVersion 1.0.0
  *@apiParamExample {json} Request-Example
  {
    "token":"597b2856502b2f09f265a1fa",
    "gameId":"b5a2ce27-42c2-41f4-bbad-c5469649373a"
  }
  *@apiSuccessExample {json} Success-Response:
    {
        "isError": false,
        "data":
        {
            "currentLines": 5
        }
    }
*/

/**
  *@api {post} /game/game_event?event=decreaseBetLine decrease bet lines
  *@apiName decrease bet line
  *@apiGroup Game Event
  *@apiVersion 1.0.0
  *@apiParamExample {json} Request-Example
  {
    "token":"597b2856502b2f09f265a1fa",
    "gameId":"b5a2ce27-42c2-41f4-bbad-c5469649373a"
  }
  *@apiSuccessExample {json} Success-Response:
    {
        "isError": false,
        "data":
        {
            "currentLines": 15
        }
    }
*/

/**
  *@api {post} /game/game_event?event=setBetLine set game lines
  *@apiName set game lines
  *@apiGroup Game Event
  *@apiVersion 1.0.0
  *@apiSuccess {Integer} Success-Code 200
  *@apiParamExample {json} Request-Example
  {
    "betLine":5,
    "token":"597339bc2b2ccb7485125914",
    "gameId":"b5a2ce27-42c2-41f4-bbad-c5469649373a"
  }
  *@apiSuccessExample {json} Success-Response:
    {
        "isError": false,
        "data":
        {
            "currentLines": 5
        }
    }
*/

/**
  *@api {post} /game/game_event?event=autoSpin auto spin on
  *@apiName auto spin on
  *@apiGroup Game Event
  *@apiVersion 1.0.0
  *@apiParamExample {json} Request-Example
  {
    "token":"597b2856502b2f09f265a1fa",
    "gameId":"b5a2ce27-42c2-41f4-bbad-c5469649373a"
  }
  *@apiSuccessExample {json} Success-Response:
  *  {
  *    "isError": false,
  *    "message": "auto spin activated",
  *    "data": {
  *      "field": "autospinon",
         "value": true,
         "wallet":14000
  *    }
  *  }
*/

/**
  *@api {post} /game/game_event?event=autoSpin auto spin off
  *@apiName auto spin off
  *@apiGroup Game Event
  *@apiVersion 1.0.0
  *@apiSuccess {Integer} Success-Code 200
  *@apiParamExample {json} Request-Example
  {
    "token":"597b2856502b2f09f265a1fa",
    "gameId":"b5a2ce27-42c2-41f4-bbad-c5469649373a"
  }
  *@apiSuccessExample {json} Success-Response:
  *  {
  *    "isError": false,
  *    "message": "auto spin deactivated",
  *    "data": {
  *      "field": "autospinoff",
         "value": true,
         "wallet":14000
  *    }
  *  }
*/

/**
  *@api {post} /game/game_event?event=maxBet max bet request
  *@apiName max bet request
  *@apiGroup Game Event
  *@apiVersion 1.0.0
  *@apiSuccess {Integer} Success-Code 200
  *@apiParamExample {json} Request-Example
  {
    "token":"597b2856502b2f09f265a1fa",
    "gameId":"b5a2ce27-42c2-41f4-bbad-c5469649373a"
  }
  *@apiSuccessExample {json} Success-Response:
    {
        "isError": false,
        "data":
        {
            "currentBet": 3200
        }
    }
*/

router.post('/game/game_event', (req, res) => {
  switch (req.body.baseGameId) {
    case constants.GAME_IDS.PALEO_WILDS:
      paleo.gameEvent(req, res);
      break;
    case constants.GAME_IDS.QIN_DYNASTY:
      qinDynasty.gameEvent(req, res);
      break;
    // case constants.GAME_IDS.UNICORN_SLOT:
    //   unicornSlotController.gameEvent(req, res);
    //   break;


    // case constants.GAME_IDS.UNICORN:
    //   unicornController.default.gameEvent(req, res);
    //   break;
    default:
      gameController.gameEvent(req, res);
  }
});

/**
  *@api {post} /game/spin spin
  *@apiName spin
  *@apiGroup Spin
  *@apiVersion 1.0.0
  *@apiParamExample {json} Request-Example
  {
    "token":"597b2856502b2f09f265a1fa",
    "clientID":"qsGurtExxuz6LoLzMGKtHpGLpsLyMyIp",
    "gameId":"b5a2ce27-42c2-41f4-bbad-c5469649373a"
  }
  *@apiSuccessExample {json} Success-Response:
  *

  {
"isError": false,
"data": {
  "viewZone": {
    "reel1": {
      "symbols": ["76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "0511eae3-78b9-45c8-9355-ce6c333b0d07", "31829440-b741-4c3d-98d2-ce3a4de3452f", "31829440-b741-4c3d-98d2-ce3a4de3452f", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "577a2b0c-c27d-4d11-ade3-f05a27821326", "21d29c3f-7d79-43a6-a4cb-cdae646cd625"],
      "stopAt": 5
    },
    "reel2": {
      "symbols": ["9dc7d76c-0291-4ca4-b0de-c960d19af3a8", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "31829440-b741-4c3d-98d2-ce3a4de3452f", "31b99cbb-7e07-482c-9a8e-b88cdd32f0fb", "8de45961-8f0f-4dec-924e-2873e25224ee", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "9dc7d76c-0291-4ca4-b0de-c960d19af3a8"],
      "stopAt": 9
    },
    "reel3": {
      "symbols": ["0511eae3-78b9-45c8-9355-ce6c333b0d07", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "0511eae3-78b9-45c8-9355-ce6c333b0d07", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "577a2b0c-c27d-4d11-ade3-f05a27821326", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "9dc7d76c-0291-4ca4-b0de-c960d19af3a8", "0511eae3-78b9-45c8-9355-ce6c333b0d07", "0511eae3-78b9-45c8-9355-ce6c333b0d07", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "31b99cbb-7e07-482c-9a8e-b88cdd32f0fb", "03f7d9e3-288c-41e3-a641-197c2b08c7f4"],
      "stopAt": 0
    },
    "reel4": {
      "symbols": ["4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "577a2b0c-c27d-4d11-ade3-f05a27821326", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "8de45961-8f0f-4dec-924e-2873e25224ee", "8de45961-8f0f-4dec-924e-2873e25224ee", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "8de45961-8f0f-4dec-924e-2873e25224ee", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "8de45961-8f0f-4dec-924e-2873e25224ee", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "31b99cbb-7e07-482c-9a8e-b88cdd32f0fb", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "31829440-b741-4c3d-98d2-ce3a4de3452f", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "9dc7d76c-0291-4ca4-b0de-c960d19af3a8"],
      "stopAt": 5
    },
    "reel5": {
      "symbols": ["4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "31b99cbb-7e07-482c-9a8e-b88cdd32f0fb", "577a2b0c-c27d-4d11-ade3-f05a27821326", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "31829440-b741-4c3d-98d2-ce3a4de3452f", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "31b99cbb-7e07-482c-9a8e-b88cdd32f0fb", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "9dc7d76c-0291-4ca4-b0de-c960d19af3a8", "31829440-b741-4c3d-98d2-ce3a4de3452f", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "8de45961-8f0f-4dec-924e-2873e25224ee", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "21d29c3f-7d79-43a6-a4cb-cdae646cd625"],
      "stopAt": 9
    },
    "viewZone": 35
  },
  "wins": {
    "winCoins": 2880,
    "winLinesCount": 2,
    "winLines": [{
      "symbolId": "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f",
      "winType": "5 of a kind",
      "winLineId": "a0cf6662-6eda-45fe-b598-54e095ef8b42",
      "winAmount": 1440
    }, {
      "symbolId": "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f",
      "winType": "5 of a kind",
      "winLineId": "854110c3-0357-4507-b539-644849124c7c",
      "winAmount": 1440
    }]
  },
  "freeSpinData": {},
  "levelData": {
    "level": 30,
    "levelProgress": 0.13
  },
  "wallet": {
    "chips": 11182564644111
  },
  "game": {
    "settings": {
      "minBet": 100,
      "maxBetMultiplier": 10,
      "minBetMultiplier": 5,
      "bets": [80, 200, 400, 800, 1600, 2400, 3200, 4800, 5600, 6400, 8000, 10000, 12000, 16000, 24000, 28000, 32000, 40000, 60000, 92000]
    },
    "betAmount": 1600
  },
  "jackpot": {
    "jackpots": [{
      "slug": "jackpot-silver",
      "amount": 1,
      "game": null,
      "progress": 0
    }, {
      "slug": "jackpot-bronze",
      "amount": 608349.96,
      "game": null,
      "progress": 20.14
    }, {
      "slug": "savanna-moon",
      "amount": 177604369.08,
      "game": "savanna-moon",
      "progress": 1.68
    }, {
      "slug": "jackpot-gold",
      "amount": 179326432.8,
      "game": null,
      "progress": 96.53
    }]
  }
}
}
*/


/**
  *@api {post} /game/spin free spin triggered
  *@apiName free spin triggered
  *@apiGroup Spin
  *@apiVersion 1.0.0
  *@apiParamExample {json} Request-Example
  {
    "token":"597b2856502b2f09f265a1fa",
    "gameId":"b5a2ce27-42c2-41f4-bbad-c5469649373a"
  }
  *@apiSuccessExample {json} Success-Response:
  *
  {
"isError": false,
"data": {
  "viewZone": {
    "reel1": {
      "symbols": ["76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "0511eae3-78b9-45c8-9355-ce6c333b0d07", "31829440-b741-4c3d-98d2-ce3a4de3452f", "31829440-b741-4c3d-98d2-ce3a4de3452f", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "577a2b0c-c27d-4d11-ade3-f05a27821326", "21d29c3f-7d79-43a6-a4cb-cdae646cd625"],
      "stopAt": 5
    },
    "reel2": {
      "symbols": ["9dc7d76c-0291-4ca4-b0de-c960d19af3a8", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "31829440-b741-4c3d-98d2-ce3a4de3452f", "31b99cbb-7e07-482c-9a8e-b88cdd32f0fb", "8de45961-8f0f-4dec-924e-2873e25224ee", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "9dc7d76c-0291-4ca4-b0de-c960d19af3a8"],
      "stopAt": 9
    },
    "reel3": {
      "symbols": ["0511eae3-78b9-45c8-9355-ce6c333b0d07", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "0511eae3-78b9-45c8-9355-ce6c333b0d07", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "577a2b0c-c27d-4d11-ade3-f05a27821326", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "9dc7d76c-0291-4ca4-b0de-c960d19af3a8", "0511eae3-78b9-45c8-9355-ce6c333b0d07", "0511eae3-78b9-45c8-9355-ce6c333b0d07", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "31b99cbb-7e07-482c-9a8e-b88cdd32f0fb", "03f7d9e3-288c-41e3-a641-197c2b08c7f4"],
      "stopAt": 0
    },
    "reel4": {
      "symbols": ["4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "577a2b0c-c27d-4d11-ade3-f05a27821326", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "8de45961-8f0f-4dec-924e-2873e25224ee", "8de45961-8f0f-4dec-924e-2873e25224ee", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "8de45961-8f0f-4dec-924e-2873e25224ee", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "8de45961-8f0f-4dec-924e-2873e25224ee", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "31b99cbb-7e07-482c-9a8e-b88cdd32f0fb", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "31829440-b741-4c3d-98d2-ce3a4de3452f", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "9dc7d76c-0291-4ca4-b0de-c960d19af3a8"],
      "stopAt": 5
    },
    "reel5": {
      "symbols": ["4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "31b99cbb-7e07-482c-9a8e-b88cdd32f0fb", "577a2b0c-c27d-4d11-ade3-f05a27821326", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "31829440-b741-4c3d-98d2-ce3a4de3452f", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "31b99cbb-7e07-482c-9a8e-b88cdd32f0fb", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "9dc7d76c-0291-4ca4-b0de-c960d19af3a8", "31829440-b741-4c3d-98d2-ce3a4de3452f", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "8de45961-8f0f-4dec-924e-2873e25224ee", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "21d29c3f-7d79-43a6-a4cb-cdae646cd625"],
      "stopAt": 9
    },
    "viewZone": 35
  },
  "wins": {
    "winCoins": 2880,
    "winLinesCount": 2,
    "winLines": [{
      "symbolId": "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f",
      "winType": "5 of a kind",
      "winLineId": "a0cf6662-6eda-45fe-b598-54e095ef8b42",
      "winAmount": 1440
    }, {
      "symbolId": "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f",
      "winType": "5 of a kind",
      "winLineId": "854110c3-0357-4507-b539-644849124c7c",
      "winAmount": 1440
    }]
  },
  "freeSpinData": {
    "freeSpins": 10,
    "currentFreeSpin": 0
  },
  "levelData": {
    "level": 0.01
  },
  "wallet": {
    "chips": 11182564644111
  },
  "game": {
    "settings": {
      "minBet": 100,
      "maxBetMultiplier": 10,
      "minBetMultiplier": 5,
      "bets": [80, 200, 400, 800, 1600, 2400, 3200, 4800, 5600, 6400, 8000, 10000, 12000, 16000, 24000, 28000, 32000, 40000, 60000, 92000]
    },
    "betAmount": 1600
  },
  "jackpot": {
    "jackpots": [{
      "slug": "jackpot-silver",
      "amount": 1,
      "game": null,
      "progress": 0
    }, {
      "slug": "jackpot-bronze",
      "amount": 608349.96,
      "game": null,
      "progress": 20.14
    }, {
      "slug": "savanna-moon",
      "amount": 177604369.08,
      "game": "savanna-moon",
      "progress": 1.68
    }, {
      "slug": "jackpot-gold",
      "amount": 179326432.8,
      "game": null,
      "progress": 96.53
    }]
  }
}
}
*/


/**
  *@api {post} /game/spin free spin
  *@apiName free spin
  *@apiGroup Spin
  *@apiVersion 1.0.0
  *@apiParamExample {json} Request-Example
  {
    "token":"597b2856502b2f09f265a1fa",
    "gameId":"b5a2ce27-42c2-41f4-bbad-c5469649373a"
  }
  *@apiSuccessExample {json} Success-Response:
  *
  {
"isError": false,
"data": {
  "viewZone": {
    "reel1": {
      "symbols": ["76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "0511eae3-78b9-45c8-9355-ce6c333b0d07", "31829440-b741-4c3d-98d2-ce3a4de3452f", "31829440-b741-4c3d-98d2-ce3a4de3452f", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "577a2b0c-c27d-4d11-ade3-f05a27821326", "21d29c3f-7d79-43a6-a4cb-cdae646cd625"],
      "stopAt": 5
    },
    "reel2": {
      "symbols": ["9dc7d76c-0291-4ca4-b0de-c960d19af3a8", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "31829440-b741-4c3d-98d2-ce3a4de3452f", "31b99cbb-7e07-482c-9a8e-b88cdd32f0fb", "8de45961-8f0f-4dec-924e-2873e25224ee", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "9dc7d76c-0291-4ca4-b0de-c960d19af3a8"],
      "stopAt": 9
    },
    "reel3": {
      "symbols": ["0511eae3-78b9-45c8-9355-ce6c333b0d07", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "0511eae3-78b9-45c8-9355-ce6c333b0d07", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "577a2b0c-c27d-4d11-ade3-f05a27821326", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "9dc7d76c-0291-4ca4-b0de-c960d19af3a8", "0511eae3-78b9-45c8-9355-ce6c333b0d07", "0511eae3-78b9-45c8-9355-ce6c333b0d07", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "31b99cbb-7e07-482c-9a8e-b88cdd32f0fb", "03f7d9e3-288c-41e3-a641-197c2b08c7f4"],
      "stopAt": 0
    },
    "reel4": {
      "symbols": ["4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "577a2b0c-c27d-4d11-ade3-f05a27821326", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "8de45961-8f0f-4dec-924e-2873e25224ee", "8de45961-8f0f-4dec-924e-2873e25224ee", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "8de45961-8f0f-4dec-924e-2873e25224ee", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "8de45961-8f0f-4dec-924e-2873e25224ee", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "31b99cbb-7e07-482c-9a8e-b88cdd32f0fb", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "31829440-b741-4c3d-98d2-ce3a4de3452f", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "9dc7d76c-0291-4ca4-b0de-c960d19af3a8"],
      "stopAt": 5
    },
    "reel5": {
      "symbols": ["4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "4cfca000-c9aa-4e7a-bf57-e5e7c8acecfa", "31b99cbb-7e07-482c-9a8e-b88cdd32f0fb", "577a2b0c-c27d-4d11-ade3-f05a27821326", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "31829440-b741-4c3d-98d2-ce3a4de3452f", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "31b99cbb-7e07-482c-9a8e-b88cdd32f0fb", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "9dc7d76c-0291-4ca4-b0de-c960d19af3a8", "31829440-b741-4c3d-98d2-ce3a4de3452f", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "8de45961-8f0f-4dec-924e-2873e25224ee", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "03f7d9e3-288c-41e3-a641-197c2b08c7f4", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "4ed22cb7-5ad0-401a-b373-dc19c42b69a9", "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f", "21d29c3f-7d79-43a6-a4cb-cdae646cd625", "21d29c3f-7d79-43a6-a4cb-cdae646cd625"],
      "stopAt": 9
    },
    "viewZone": 35
  },
  "wins": {
    "winCoins": 2880,
    "winLinesCount": 2,
    "winLines": [{
      "symbolId": "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f",
      "winType": "5 of a kind",
      "winLineId": "a0cf6662-6eda-45fe-b598-54e095ef8b42",
      "winAmount": 1440
    }, {
      "symbolId": "76c4b146-8a25-4ac7-b9ea-ba6e9ca48a6f",
      "winType": "5 of a kind",
      "winLineId": "854110c3-0357-4507-b539-644849124c7c",
      "winAmount": 1440
    }]
  },
  "freeSpinData": {
    "freeSpins": 10,
    "currentFreeSpin": 1
  },
  "levelData": {
    "level": 0.01
  },
  "wallet": {
    "chips": 11182564644111
  },
  "game": {
    "settings": {
      "minBet": 100,
      "maxBetMultiplier": 10,
      "minBetMultiplier": 5,
      "bets": [80, 200, 400, 800, 1600, 2400, 3200, 4800, 5600, 6400, 8000, 10000, 12000, 16000, 24000, 28000, 32000, 40000, 60000, 92000]
    },
    "betAmount": 1600
  },
  "jackpot": {
    "jackpots": [{
      "slug": "jackpot-silver",
      "amount": 1,
      "game": null,
      "progress": 0
    }, {
      "slug": "jackpot-bronze",
      "amount": 608349.96,
      "game": null,
      "progress": 20.14
    }, {
      "slug": "savanna-moon",
      "amount": 177604369.08,
      "game": "savanna-moon",
      "progress": 1.68
    }, {
      "slug": "jackpot-gold",
      "amount": 179326432.8,
      "game": null,
      "progress": 96.53
    }]
  }
}
}


*/
router.post('/game/spin', (req, res) => {
  switch (req.body.baseGameId) {
    case constants.GAME_IDS.SECRET_OF_AMUN:
      secretOfAmunController.spin(req, res);
      break;

    case constants.GAME_IDS.UNICORN:
    unicorn.default.spin(req, res);
      break;

    case constants.GAME_IDS.BACKER_STREET_DETECTIVES:
      backStreet.spin(req, res);
      break;
    case constants.GAME_IDS.CINDERELLA:
      cinderella.spin(req, res);
      break;
    case constants.GAME_IDS.PALEO_WILDS:
      paleo.spin(req, res);
      break;
    case constants.GAME_IDS.QIN_DYNASTY:
      qinDynasty.spin(req, res);
      break;
    case constants.GAME_IDS.GATSBY:
      gatsby.spin(req, res);
      break;
    case constants.GAME_IDS.NOBLE_MUSKETEERS:
      nobleMusketeers.spin(req, res);
      break;
    case constants.GAME_IDS.DEEP_SEAS:
      deepSeas.spin(req, res);
      break;
    case constants.GAME_IDS.DRAGON_PHOENIX:
      dragonPhoenix.spin(req, res);
      break;
    case constants.GAME_IDS.JUNGLE_STORY:
      jungleStory.spin(req, res);
      break;
    case constants.GAME_IDS.POSEIDON:
      poseidon.spin(req, res);
      break;
    case constants.GAME_IDS.ZEUS:
      zeus.spin(req, res);
      break;
    case constants.GAME_IDS.PYRAMIDS_OF_GIZA:
      pyramidsOfGiza.spin(req, res);
      break;
      case constants.GAME_IDS.CARNEVALE:
        carnevale.spin(req, res);
        break;
    // case constants.GAME_IDS.UNICORN_SLOT:
    //   unicornSlotController.spin(req, res);
    //   break;
    case constants.GAME_IDS.PARIS_TOURIST:
      parisTouristController.spin(req, res);
      break;
    case constants.GAME_IDS.ZOMBIE_APOCALYPSE:
      zombieApocController.spin(req, res);
      break;
    case constants.GAME_IDS.KNIGHT_DRAGONS:
      knightDragonsController.spin(req, res);
      break;
    case constants.GAME_IDS.PACIFIC_RAIL_ROAD:
      pacificRailRoad.spin(req, res);
      break;
    case constants.GAME_IDS.CASANOVA:
      casanova.default.spin(req, res);
      break;
    case constants.GAME_IDS.DIVINE_SPIRIT:
      divineSpirit.default.spin(req, res);
      break;
    default:
      gameController.spin(req, res);
  }
});


/**
  *@api {post} /game/session get user session
  *@apiName get user session
  *@apiGroup Game
  *@apiVersion 1.0.0
  *@apiSuccess {Integer} Success-Code 200
  *@apiParamExample {json} Request-Example
  {
      "token":"597b2856502b2f09f265a1fa",
      "gameId":"b5a2ce27-42c2-41f4-bbad-c5469649373a"
  }
  *@apiSuccessExample {json} Success-Response:
  *
    {
        "isError": false,
        "data":
        {
            "_type": "userSession",
            "_updatedTimestamp": "2017-08-12T07:56:00.742Z",
            "event": "spin",
            "eventData":
            {
                "currentBet": 100,
                "currentLines": 10,
                "freeSpinData":
                {
                    "freeSpins": 10,
                    "currentFreeSpin": 1
                },
                "userDetails":
                {
                    "birthday": "1-1-1970",
                    "gender": "na",
                    "id": "597b1378502b2f09f265a1f0",
                    "level": 7,
                    "levelProgress": 0.93,
                    "locale": "de_DE",
                    "multiplier": 1,
                    "name": "darhan",
                    "wallet":
                    {
                        "chips": 527539
                    }
                }
            },
            "game":
            {
                "settings":
                {
                    "minBet": 100,
                    "maxBetMultiplier": 10,
                    "minBetMultiplier": 5,
                    "bets":
                    [
                        20,
                        40,
                        100,
                        200,
                        400,
                        800,
                        1200,
                        1600,
                        2400,
                        3200
                    ]
                }
            }
        }
    }
*/
router.post('/game/session', (req, res) => {
  switch (req.body.baseGameId) {
    case constants.GAME_IDS.PALEO_WILDS:
      paleo.getSession(req, res);
      break;
    case constants.GAME_IDS.QIN_DYNASTY:
      qinDynasty.getSession(req, res);
      break;
    // case constants.GAME_IDS.UNICORN_SLOT:
    //   unicornSlotController.getSession(req, res);
    //   break;


    // case constants.GAME_IDS.UNICORN:
    //   unicornController.default.getSession(req, res);
    //   break;
    case constants.GAME_IDS.NOBLE_MUSKETEERS:
      nobleMusketeers.getSession(req, res);
      break;
    case constants.GAME_IDS.DRAGON_PHOENIX:
      dragonPhoenix.getSession(req, res);
      break;
    default:
      gameController.getSession(req, res);
  }
});


/**
  *@api {post} /game/paytable get game paytable
  *@apiName get game paytable
  *@apiGroup Game
  *@apiVersion 1.0.0
  *@apiSuccess {Integer} Success-Code 200
  *@apiParamExample {json} Request-Example
  {
      "token":"597b2856502b2f09f265a1fa",
      "gameId":"b5a2ce27-42c2-41f4-bbad-c5469649373a"
  }
  *@apiSuccessExample {json} Success-Response:
  *


  {
      "isError": false,
      "data":
      {
          "21c0a6be-23b9-46fa-84e9-db6d20652105":
          {
              "2":
              {
                  "amount": 2000
              },
              "3":
              {
                  "amount": 4000
              },
              "4":
              {
                  "amount": 8000
              },
              "5":
              {
                  "amount": 10000
              }
          },
          "319c5cc4-54a5-485b-ac98-0f06b2a0ba54":
          {
              "2":
              {
                  "amount": 0
              },
              "3":
              {
                  "amount": 1000
              },
              "4":
              {
                  "amount": 2000
              },
              "5":
              {
                  "amount": 4000
              }
          },
          "445b57e3-d699-4289-8d27-0c1ef89467bd":
          {
              "2":
              {
                  "amount": 1000
              },
              "3":
              {
                  "amount": 2000
              },
              "4":
              {
                  "amount": 4000
              },
              "5":
              {
                  "amount": 8000
              }
          },
          "61c0a31a-b7ae-4032-95ef-f818a589e0ce":
          {
              "2":
              {
                  "amount": 0
              },
              "3":
              {
                  "amount": 2000
              },
              "4":
              {
                  "amount": 2000
              },
              "5":
              {
                  "amount": 2000
              }
          },
          "80ef8a14-a2af-49db-8d0b-1f8d100bc5ae":
          {
              "2":
              {
                  "amount": 1000
              },
              "3":
              {
                  "amount": 2000
              },
              "4":
              {
                  "amount": 3000
              },
              "5":
              {
                  "amount": 8000
              }
          },
          "a6865e21-bd34-484d-80e0-5fbcba2a41e1":
          {
              "2":
              {
                  "amount": 2000
              },
              "3":
              {
                  "amount": 8000
              },
              "4":
              {
                  "amount": 16000
              },
              "5":
              {
                  "amount": 32000
              }
          },
          "a73186fa-6666-40df-8168-f31beeaee89c":
          {
              "2":
              {
                  "amount": 2000
              },
              "3":
              {
                  "amount": 4000
              },
              "4":
              {
                  "amount": 8000
              },
              "5":
              {
                  "amount": 10000
              }
          },
          "b2fdd1e0-5230-4aeb-bc7c-5e5cb301cede":
          {
              "2":
              {
                  "amount": 10000
              },
              "3":
              {
                  "amount": 20000
              },
              "4":
              {
                  "amount": 40000
              },
              "5":
              {
                  "amount": 80000
              }
          },
          "c318eac1-67dd-4cb0-8722-505588ecb4ca":
          {
              "2":
              {
                  "amount": 0
              },
              "3":
              {
                  "amount": 0
              },
              "4":
              {
                  "amount": 0
              },
              "5":
              {
                  "amount": 0
              }
          },
          "c56e0ad1-1423-45f2-ac74-988a0614ad95":
          {
              "2":
              {
                  "amount": 1000
              },
              "3":
              {
                  "amount": 2000
              },
              "4":
              {
                  "amount": 4000
              },
              "5":
              {
                  "amount": 8000
              }
          },
          "d5a23d50-cdab-4556-8798-c3fc7d783ab9":
          {
              "2":
              {
                  "amount": 0
              },
              "3":
              {
                  "amount": 0
              },
              "4":
              {
                  "amount": 0
              },
              "5":
              {
                  "amount": 0
              }
          },
          "df1d9952-68cc-43e3-9c31-fbea5c9614f3":
          {
              "2":
              {
                  "amount": 1000
              },
              "3":
              {
                  "amount": 2000
              },
              "4":
              {
                  "amount": 4000
              },
              "5":
              {
                  "amount": 8000
              }
          },
          "f03c9bb4-8469-44b9-a112-416250f5494b":
          {
              "2":
              {
                  "amount": 1000
              },
              "3":
              {
                  "amount": 1000
              },
              "4":
              {
                  "amount": 2000
              },
              "5":
              {
                  "amount": 2000
              }
          },
          "f625b74c-90d1-4d3a-82c6-f208ed376097":
          {
              "2":
              {
                  "amount": 1000
              },
              "3":
              {
                  "amount": 2000
              },
              "4":
              {
                  "amount": 4000
              },
              "5":
              {
                  "amount": 8000
              }
          }
      }
  }
*/
router.post('/game/paytable', gameController.getPayTable);


/**
  *@api {post} /game/all get all playable games
  *@apiName get all playable games
  *@apiGroup Game
  *@apiVersion 1.0.0
  *@apiSuccess {Integer} Success-Code 200
  *@apiSuccessExample {json} Success-Response:
  *{
    "isError": false,
    "data": {
      "playableGames": [
        [{
          "gameName": "unicorn slot",
          "versionName": "test version",
          "gameId": "2ce5747c-ecad-483d-8bad-c946776bf454"
        }],
        [{
          "gameName": "atkins diet",
          "versionName": "v1.0.0",
          "gameId": "12e6d223-5d7d-4516-906b-af14208f3902"
        }]
      ]
    }
  }
*/
router.get('/game/all', gameController.getAllPlayableGames);

router.post('/game/symbol/paytable', gameController.getSymbolPay);

router.post('/game/soa/gamble', validator.validateSOAGamble, secretOfAmunController.gamble);

router.post('/game/soa/pick', validator.validateSOAGamblePick, secretOfAmunController.pickGamble);

router.post('/game/paleo-wilds/do-not-show', paleo.doNotShowPopUp);
router.post('/game/qin-dynasty/do-not-show', qinDynasty.doNotShowPopUp);
// router.post('/game/unicorn-slot/do-not-show', unicornSlotController.doNotShowPopUp);

router.post('/game/pt/spin_wheel', parisTouristController.spinFortuneWheel);

router.post('/game/za/bonus', zombieApocController.handleBonusRequest);

router.post('/game/za/gamble', validator.validateSOAGamble, zombieApocController.gamble);

router.post('/game/za/pick', validator.validateSOAGamblePick, zombieApocController.pickGamble);

router.post('/game/kad/bonus', knightDragonsController.handleBonusRequest);

router.post('/game/noble-musketeers/bonus', validator.validateNMBonusPick, nobleMusketeers.handleBonusRequest);

router.post('/game/kad/gamble', validator.validateSOAGamble, knightDragonsController.gamble);

router.post('/game/kad/pick', knightDragonsController.pickGamble);

router.post('/game/pt/gamble', validator.validateSOAGamble, parisTouristController.gamble);

router.post('/game/pt/pick', validator.validateSOAGamblePick, parisTouristController.pickGamble);

router.post('/game/bsd/bonus', backStreet.handleBonusRequest);

module.exports = router;
