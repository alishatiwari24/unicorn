/* eslint-disable new-cap */
const Router = require('router');
const mobileController = require('../../controller/mobile');
const validator = require('../../validator/game');

const router = Router();

/**
 * mobile related routes
 */
router.post('/mobile/version', validator.validateMobileVersion, mobileController.getVersion);

module.exports = router;
