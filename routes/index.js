/* eslint-disable new-cap */
const game = require('./game');
const Router = require('router');
const mobile = require('./mobile');

const validator = require('../validator/game');

const router = Router();

/**
* registered route for game apis
*/
router.route('/game*').get(validator.validateGameId, game).post(validator.validateGameId, game);

/**
* registered route for mobile specific apis
*/
router.route('/mobile*').get(validator.validateGameId, mobile).post(validator.validateGameId, mobile);

module.exports = router;
