/* eslint-disable require-await */
const whowapi = require('../api/whowapi');
const _ = require('lodash');
const GameEvents = require('./game-events').GameEvents;
/**
 * Calls whow api and informs of the events that are handled
 * Symbols in view zone, wins, symbols participating in wins,
 * 5 of a kind and free spin awards are notified
 */
class GameEventsDeepSeas extends GameEvents {

  /**
   * All the tracked game events are notified from this point
   */
  async notifyGameEvents(obj, token, session, result, freeSpins) {
    const noofReels = obj.game.default.gameConfig.reelConfigSet.ReelL.NumberOfReels;
    /**
    * notify of all the symbols present in view zone
    */
   const masterPayload = [];
   this.notifySymbols(masterPayload, noofReels, (session.eventData.currentBet * session.eventData.currentLines), result.viewZoneL, result.viewZoneR, obj);

   const total = result.wins.winCoins;
   /**
   * notify of the wins
   */
   if (total > 0) {
     let isFreeSpinWin = false;
     if (!_.isEmpty(session.eventData.freeSpinData) && freeSpins && session.eventData.freeSpinData.currentFreeSpin == 0) {
       isFreeSpinWin = false;
     } else if (!_.isEmpty(session.eventData.freeSpinData)) {
       isFreeSpinWin = true;
     }
     super.notifyWins(masterPayload, (session.eventData.currentBet * session.eventData.currentLines), total, isFreeSpinWin, obj);

     /**
     * notify symbol that wins
     */
     this.notifySymbolWins(masterPayload, (session.eventData.currentBet * session.eventData.currentLines), result, result.viewZoneL, result.viewZoneR, obj);
    }

    /**
    * Notify of free spins
    */
    if (freeSpins) {
      let isFreeSpin = false;
      if (!_.isEmpty(session.eventData.freeSpinData) && session.eventData.freeSpinData.currentFreeSpin > 0) {
        isFreeSpin = true;
      }
      super.notifyFreeSpins(masterPayload, freeSpins, (session.eventData.currentBet * session.eventData.currentLines), isFreeSpin);
    }

    const events = {};
    events.events = masterPayload;

    whowapi.triggerEvents(token, events);
    return true;
  }

  /**
   * All the symbols participating in the view zone are notified
   * A integer Id of the symbol is passed in payload that represents a symbol
   */
  notifySymbols(masterPayload, noofReels, betAmount, viewZoneL, viewZoneR, obj) {
    const value = [];
    const symbolPool = obj.game.default.gameConfig.symbols;
    for (let i = 0; i < noofReels; i += 1) {
      const symbols = viewZoneL[`Reel${i + 1}`];
      symbols.forEach((symbol) => {
        value.push(symbolPool[symbol.symbolId].SymbolNumber);
      });
    }
    for (let i = 0; i < noofReels; i += 1) {
      const symbols = viewZoneR[`Reel${i + 1}`];
      symbols.forEach((symbol) => {
        value.push(symbolPool[symbol.symbolId].SymbolNumber);
      });
    }
    const payload = {};
    payload.type = 'symbols';
    payload.value = value;
    payload.betAmount = betAmount;
    masterPayload.push(payload);
  }


  /**
   * When a user wins, the symbols participating in a win is notified
   */
  notifySymbolWins(masterPayload, betAmount, results, viewZoneL, viewZoneR, obj) {
    const wins = results.wins;
    const normalizedPayArray = obj.game.default.gameConfig.payLines.normalizedPayArray;
    const normalizedPayArray66 = obj.game.default.gameConfig.payLines66.normalizedPayArray;
    const symbolPool = obj.game.default.gameConfig.symbols;
    const symbols = [];
    let trigger6Win = false;
    const sixWins = [];
    wins.winLines.forEach((win) => {
      const winLineId = win.winLineId;
      let payLine = _.find(normalizedPayArray, o => o._id === winLineId);
      if (!payLine) {
        payLine = _.find(normalizedPayArray66, o => o._id === winLineId);
      }
      const positions = payLine.data;
      const winLength = win.winType.charAt(0);
      for (let i = 0; i < winLength; i += 1) {
        const symbolId = (i < 3) ? viewZoneL[`Reel${i + 1}`][positions[i]].symbolId : viewZoneR[`Reel${i - 2}`][positions[i]].symbolId;
        symbols.push(symbolPool[symbolId].SymbolNumber);
      }
      if (win.winType === '6 of a kind') {
        trigger6Win = true;
        sixWins.push(win);
      }
    });
    const payload = {};
    payload.type = 'symbolsWin';
    payload.value = symbols;
    payload.betAmount = betAmount;

    masterPayload.push(payload);

    if (trigger6Win) {
      this.notifyFullRow(masterPayload, sixWins, viewZoneL, viewZoneR, obj);
    }
  }


  /**
   * If a user wins with a combination of 5 of a kind
   * such wins are notified
   */
  async notifyFullRow(masterPayload, wins, viewZoneL, viewZoneR, obj) {
    const symbols = [];
    const normalizedPayArray = obj.game.default.gameConfig.payLines66.normalizedPayArray;
    const symbolPool = obj.game.default.gameConfig.symbols;

    const win = wins[0];
    const winLineId = win.winLineId;
    const payLine = _.find(normalizedPayArray, o => o._id === winLineId);
    const positions = payLine.data;
    let wonWithWild = false;
    for (let i = 0; i < positions.length; i += 1) {
      const symbolId = (i < 3) ? viewZoneL[`Reel${i + 1}`][positions[i]].symbolId : viewZoneL[`Reel${i - 2}`][positions[i]].symbolId;
      if (symbolPool[symbolId].SymbolType === 'Wild' || symbolPool[symbolId].SymbolType === 'Wild/Scatter') {
        wonWithWild = true;
      }
      symbols.push(symbolPool[symbolId].SymbolNumber);
    }

    const payload = {};
    payload.type = 'fullRow';
    payload.value = 6;
    payload.wildcard = wonWithWild;
    payload.symbols = symbols;
    masterPayload.push(payload);
  }
}
module.exports = new GameEventsDeepSeas();
