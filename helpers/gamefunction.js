/*
eslint-disable no-console, no-underscore-dangle
*/
const { bucket } = require('./couchbase');
const _ = require('lodash');
const compute = require('../util/compute');
const gameSession = require('../util/gamesession');
const whowapi = require('../api/whowapi');
const Promise = require('bluebird');
const constants = require('../response/constants');
const moment = require('moment');

const gameConfig = [];
const baseGamesMapping = {};

/**
* Game handles all the game business logic
* Various computations and session updates wherever necessary happen through this class
*/
class Game {

  /*
  * This function handles spin request from controller
  * generates a random number based on game configuration
  * computes the result with the help of compute helper and generates the response
  * fallbacks in case of user spin being active
  * and checking valid spins happens in it.
  */
  handleSpin(gameId, token, clientID) {
    return new Promise((resolve) => {
      const obj = this.getGame(gameId);
      if (!obj) {
        resolve({ error: constants.RES_MESSAGES.GAME_NOT_FOUND, erroCode: constants.RES_ERROR_CODES.GAME_NOT_FOUND });
      } else {
        this.getUserSessionAndSessionRequest(token, gameId)
        .then((results) => {
          if (results.sessionRequestKey && results.sessionKey) {
            const sessionRequest = results[results.sessionRequestKey].value;
            if (sessionRequest.status == 'init' || sessionRequest.status == 'ready') {
              const session = results[results.sessionKey].value;

              // allow spin
              if (session.clientID === clientID) {
                // update session request doc
                this.updateSessionRequestDoc(results.sessionRequestKey, sessionRequest, results[results.sessionRequestKey].cas)
                .then(() => {
                  compute.compute(obj.game, gameId, token, session)
                  .then((computeData) => {
                    resolve(compute.generateSpinResponse(obj, computeData, token, results.sessionRequestKey, sessionRequest, session));
                  }).catch((err) => {
                    console.log(err);
                  });
                })
                .catch((error) => {
                  console.log(error);
                  resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED });
                });
              } else {
                resolve(compute.invalidClientIdResponse());
              }
            } else {
              resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED });
            }
          } else {
            // reject spin as no session request document is present
            resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK });
          }
        }).catch(() => {
          resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK });
        });
      }
    });
  }


  /**
  * This function returns game stored in memory based on gameId passed to it
  */
  getGame(gameId) {
    return _.find(gameConfig, o => o._id === gameId);
  }


  /**
  * This function returns base game id stored in memory based on gameId passed to it
  */
  getBaseGameId(gameId) {
    return _.find(gameConfig, o => o._id === gameId).game.default.baseGameId;
  }


  /**
  * This function returns paytable for user for current bet and lines
  */
  getPaytble(token, gameId) {
    return new Promise((resolve, reject) => {
      const game = this.getGame(gameId);
      const payTable = {};
      this.getUserSession(token, gameId)
      .then((session) => {
        const currentBet = session.eventData.currentBet;
        const currentLines = session.eventData.currentLines;
        Object.keys(game.game.default.gameConfig.payTable).forEach((key) => {
          const payArray = game.game.default.gameConfig.payTable[key];
          const pays = {};
          Object.keys(payArray).forEach((array) => {
            pays[array] = { amount: (currentBet * currentLines * payArray[array].Multiplier) };
          });
          payTable[key] = pays;
        });
        resolve(payTable);
      }).catch((err) => {
        reject({ error: constants.RES_MESSAGES.NO_PAYTABLE });
      });
    });
  }


  /**
  * This function handles game click event from controller
  * WHOW API is called to get user data from token
  * relevant user data along with game sessions are created using compute helper
  */
  handleGameClick(gameId, token) {
    return new Promise((resolve, reject) => {
      whowapi.get(token)
      .then((response) => {
        const obj = this.getGame(gameId);
        if (!obj) {
          resolve({ error: constants.RES_MESSAGES.GAME_NOT_FOUND, erroCode: constants.RES_ERROR_CODES.GAME_NOT_FOUND });
        } else if (response.status == 200) {
          obj.userDetails = response.payload;

          // checks if there is multiple reel config set
          if (obj.game.default.gameConfig.reelConfigSet) {
            obj.game.default.gameConfig.reelConfig = obj.game.default.gameConfig.reelConfigSet[obj.game.default.gameConfig.specialFeature.reelSwitching.mainGame[0]];
          }
          resolve(compute.generateGameClickResponse(obj, token, gameId));
        } else {
          resolve({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
        }
      }).catch((err) => {
        console.log(err);
        reject({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
      });
    });
  }


  /**
  * This function is used to fetched user game session data from game session helper
  */
  getUserSession(token, gameId) {
    return new Promise((resolve, reject) => {
      gameSession.getUserSession(token, gameId)
      .then((result) => {
        resolve(result);
      }).catch((err) => {
        console.log(err);
        reject(err);
      });
    });
  }


  /**
  * This function gets user session and session request documents
  * stored in database at the time of game click using game session
  */
  getUserSessionAndSessionRequest(token, gameId) {
    return gameSession.getUserSessionAndSessionRequest(token, gameId);
  }


  /**
  * This function updates user's session request doument allowing a spin to take place
  * game session helper is used to update session data
  */
  updateSessionRequestDoc(sessionRequestKey, sessionRequestDoc, cas) {
    sessionRequestDoc.status = 'spinning';
    sessionRequestDoc.spinIniTimestamp = moment().utc();
    return gameSession.updateSessionRequestDocWithCas(sessionRequestKey, sessionRequestDoc, cas);
  }


  /**
  * This function is used poll database for new games and versions if avialable
  * existing game changes are also polled
  * polling happens every 1 minute and results are store in memory
  */
  gameLookupLoop() {
    this.loadGames();
    setInterval(() => {
      this.loadGames();
    }, 60000);
  }


  /*
  * All games are loaded when this function is called for the first time
  * and game details are stored in local memory
  */
  loadGames() {
    bucket.get('646cab37-1bea-411c-a256-a6140bab16f2', (error, allGames) => {
      bucket.getMulti(['c67a1994-c076-4b04-ba3a-d08bfa460e54'], (err, allGamesData) => {
        if (allGamesData) {
          for (const gameId in allGamesData) {
            if (allGamesData[gameId].value) {
              const j = _.findIndex(gameConfig, game => game._id == gameId);
              if (j === -1) {
                gameConfig.push({ _id: gameId, game: { default: allGamesData[gameId].value } });
                baseGamesMapping[allGamesData[gameId].value.baseGameId] = gameId;
              } else {
                gameConfig.splice(j, 1);
                gameConfig.push({ _id: gameId, game: { default: allGamesData[gameId].value } });
                baseGamesMapping[allGamesData[gameId].value.baseGameId] = gameId;
              }
            }
          }
        } else {
          console.log(err, allGamesData, 'err');
        }
        console.log(err, gameConfig.length, new Date(), 'loaded');
      });
    });
  }


/**
 * This function fetches the game that are currently live and that can be played
 * Game can be made active or inactive via admin console by admin user
 */
  getAllPlayableGames() {
    return new Promise((resolve) => {
      const data = {};
      const promise = [];
      let sortId = 0;
      for (const baseGameId in baseGamesMapping) {
        promise.push(new Promise((resolve1) => {
          bucket.get(baseGameId, (err, baseGame) => {
            data[baseGameId] = {
              sortId,
              gameId: baseGameId,
              gameLogo: baseGame.value.game.gameLogo,
              gameName: baseGame.value.game.gameName,
              versionName: baseGame.value.game.latestVersion
            };
            resolve1();
          });
        }));
        sortId += 1;
      }
      Promise.all(promise)
      .then(() => {
        resolve(data);
      });
    });
  }


  /**
   * This function returns the symbol pay of the symbolId provided
   * User's session is polled for current bet and current lines in order to get symbol pay
   * @param  {[type]} token    user token
   * @param  {[type]} gameId   game id of the game for which symbol id is queried
   * @param  {[type]} symbolId symbol id of the symbol for which pay needs to be fetched
   */
  getSymbolPay(token, gameId, symbolId) {
    return new Promise((resolve, reject) => {
      const game = this.getGame(gameId);
      const payTable = {};
      this.getUserSession(token, gameId)
      .then((session) => {
        if (!session) {
          reject(constants.RES_MESSAGES.NO_USER_SESSION);
        } else {
          const currentBet = session.eventData.currentBet;
          const currentLines = session.eventData.currentLines;
          if (!game.game.default.gameConfig.payTable[symbolId]) {
            reject(constants.RES_MESSAGES.INVALID_SYMBOL);
          } else {
            const payArray = game.game.default.gameConfig.payTable[symbolId];
            const pays = {};
            Object.keys(payArray).forEach((array) => {
              pays[array] = { amount: (currentBet * currentLines * payArray[array].Multiplier) };
            });
            payTable[gameId] = pays;

            resolve(payTable);
          }
        }
      })
      .catch((err) => {
        console.log(err);
        reject(constants.RES_MESSAGES.NO_USER_SESSION);
      });
    });
  }


/**
 * This function would fetch assesst bundle url and version code of a platform
 * @param  gameId   game id of the game that client is playing
 * @param  token    user token
 * @param  platform ANDROID/IOS/MAC/WINDOWS - allowed user platforms
 */
  getVersion(gameId, token, platform) {
    return new Promise((resolve, reject) => {
      this.getUserSession(token, gameId)
      .then((session) => {
        bucket.get(`${platform}::version`, (err, result) => {
          if (err) {
            reject({ error: constants.RES_MESSAGES.NO_VERSION });
          } else {
            resolve(result.value);
          }
        });
      }).catch((err) => {
        reject({ error: constants.RES_MESSAGES.NO_SESSION_DATA, errorCode: constants.RES_ERROR_CODES.NO_SESSION_DATA });
      });
    });
  }


  /**
   * this function removes the unnecessary fields from session object
   * it builds the required response for client to parse
   */
  generateUserSessionResponse(userSession) {
    const session = {
      event: 'spin',
      eventData: {
        currentBet: userSession.eventData.currentBet,
        currentLines: userSession.eventData.currentLines,
        userDetails: {
          name: userSession.eventData.userDetails.name,
          gender: userSession.eventData.userDetails.gender,
          wallet: userSession.eventData.userDetails.wallet,
          locale: userSession.eventData.userDetails.locale,
          level: userSession.eventData.userDetails.level,
          levelProgress: userSession.eventData.userDetails.levelProgress
        },
        freeSpinData: userSession.eventData.freeSpinData,
        game: userSession.game
      }
    };
    session.eventData.totalBet = session.eventData.currentBet * session.eventData.currentLines;
    if (userSession.eventData.currentReelSet !== null && userSession.eventData.currentReelSet !== undefined) {
      let currentReel = 1;
      if (!_.isEmpty(userSession.eventData.freeSpinData) && userSession.eventData.freeSpinData.currentReelSet !== null) {
        currentReel = userSession.eventData.freeSpinData.currentReelSet;
      } else if (!isNaN(userSession.eventData.currentReelSet) && userSession.eventData.currentReelSet !== Infinity) {
        currentReel = userSession.eventData.currentReelSet;
      } else {
        currentReel = 1;
      }
      currentReel += 1;
      if (currentReel > 5) {
        currentReel = 1;
      }
      if (currentReel) {
        session.wildReel = `reel${currentReel}`;
      }
    }
    return session;
  }


  /**
   * This function returns the actual live version of the game from its base gameid
   * It is helper that resolves game id mutation problem when a new version of the game is live
   */
  getGameIdFromBaseGameId(baseGameId) {
    return baseGamesMapping[baseGameId];
  }
}

module.exports = new Game();
module.exports.GameFunction = Game;
