/* eslint-disable func-style, require-jsdoc, no-console, no-ternary, multiline-ternary */

const AWS = require('aws-sdk');
const moment = require('moment');
AWS.config.loadFromPath(`${__dirname}/../config/aws.json`);

/**
* initiate firehose Object with appropriate apiVersion
*/
const firehose = new AWS.Firehose({ apiVersion: '2015-08-04' });

function log(level, data) {
  try {
    data.level = level;
    data.createdTimestamp = moment().utc();
    const params = {
      // delivery stream -- required
      DeliveryStreamName: 'slot-staging',
      // required
      Records: [{
        /**
        * provide data in buffer form
        */
        Data: Buffer.from(JSON.stringify(data), 'utf8')
      }]
    };
    /**
    * firehose put record
    */
    firehose.putRecordBatch(params, (err, putRecordResult) => {
      if (err) {
        console.log(err, putRecordResult, 'err in putRecordBatch');
      }
    });
  } catch (e) {
    console.log(e, 'err in firehose.log');
  }
}

const logger = {
  /**
  * stream for debug
  */
  debug(data) {
    log('debug', (typeof data === String) ? { data } : data);
  },
  /**
  * stream for info - manual logs
  */
  info(data) {
    log('info', (typeof data === String) ? { data } : data);
  },
  /**
  * stream for warnings
  */
  warn(data) {
    log('warn', (typeof data === String) ? { data } : data);
  },
  /**
  * stream fro errors
  */
  error(error) {
    log('error', { message: error.message || error, stack: error.stack || error });
  }
};


/**
* export logger to make it available to other modules
*/
module.exports = logger;
