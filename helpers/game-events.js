/* eslint-disable require-await */
const whowapi = require('../api/whowapi');
const _ = require('lodash');

/**
 * Calls whow api and informs of the events that are handled
 * Symbols in view zone, wins, symbols participating in wins,
 * 5 of a kind and free spin awards are notified
 */
class GameEvents {

  /**
   * All the tracked game events are notified from this point
   */
  async notifyGameEvents(obj, token, session, result, freeSpins) {
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    /**
    * notify of all the symbols present in view zone
    */
   const masterPayload = [];
    this.notifySymbols(masterPayload, noofReels, (session.eventData.currentBet * session.eventData.currentLines), result.viewZone, obj);

    const total = result.wins.winCoins;
    /**
    * notify of the wins
    */
    if (total > 0) {
      let isFreeSpinWin = false;
      if (!_.isEmpty(session.eventData.freeSpinData) && freeSpins && session.eventData.freeSpinData.currentFreeSpin == 0) {
        isFreeSpinWin = false;
      } else if (!_.isEmpty(session.eventData.freeSpinData)) {
        isFreeSpinWin = true;
      }
      this.notifyWins(masterPayload, (session.eventData.currentBet * session.eventData.currentLines), total, isFreeSpinWin, obj);

      /**
      * notify symbol that wins
      */
      this.notifySymbolWins(masterPayload, (session.eventData.currentBet * session.eventData.currentLines), result, result.viewZone, obj);
    }

    /**
    * Notify of free spins
    */
    if (freeSpins) {
      let isFreeSpin = false;
      if (!_.isEmpty(session.eventData.freeSpinData) && session.eventData.freeSpinData.currentFreeSpin > 0) {
        isFreeSpin = true;
      }
      this.notifyFreeSpins(masterPayload, freeSpins, (session.eventData.currentBet * session.eventData.currentLines), isFreeSpin);
    }

    const events = {};
    events.events = masterPayload;

    whowapi.triggerEvents(token, events);
    return true;
  }

  /**
   * All the symbols participating in the view zone are notified
   * A integer Id of the symbol is passed in payload that represents a symbol
   */
  notifySymbols(masterPayload, noofReels, betAmount, viewZone, obj) {
    const value = [];
    const symbolPool = obj.game.default.gameConfig.symbols;
    for (let i = 0; i < noofReels; i += 1) {
      const symbols = viewZone[`Reel${i + 1}`];
      symbols.forEach((symbol) => {
        value.push(symbolPool[symbol.symbolId].SymbolNumber);
      });
    }
    const payload = {};
    payload.type = 'symbols';
    payload.value = value;
    payload.betAmount = betAmount;

    masterPayload.push(payload);
  }


  /**
   * When a user wins, the win amount is notified
   */
  notifyWins(masterPayload, betAmount, winAmount, isFreeGameWin, obj) {
    const payload = {};
    payload.type = 'win';
    payload.betAmount = betAmount;
    payload.value = winAmount;
    payload.isFreeGameWin = isFreeGameWin;

    let winType = 'normal';

    const bigWins = obj.game.default.gameConfig.bigWins;
    if (bigWins) {

      if (bigWins.bigWin && winAmount >= (betAmount * bigWins.bigWin.minBetMultiplier) && winAmount < (betAmount * bigWins.bigWin.maxBetMultiplier)) {
        winType = 'big';
      } else if (bigWins.monsterWin && winAmount >= (betAmount * bigWins.monsterWin.minBetMultiplier) && winAmount < (betAmount * bigWins.monsterWin.maxBetMultiplier)) {
        winType = 'monster';
      } else if (bigWins.legendaryWin && winAmount >= (betAmount * bigWins.legendaryWin.minBetMultiplier)) {
        winType = 'legendary';
      }
    }
    payload.winType = winType;

    masterPayload.push(payload);
  }


  /**
   * When a user wins, the symbols participating in a win is notified
   */
  notifySymbolWins(masterPayload, betAmount, results, viewZone, obj) {
    const wins = results.wins;
    const normalizedPayArray = obj.game.default.gameConfig.payLines.normalizedPayArray;
    const symbolPool = obj.game.default.gameConfig.symbols;
    const symbols = [];
    let trigger5Win = false;
    const fiveWins = [];
    wins.winLines.forEach((win) => {
      const winLineId = win.winLineId;
      let payLine = _.find(normalizedPayArray, o => o._id === winLineId);
      if (!winLineId && win.winPosition && win.winPosition.length) {
        payLine = { data: _.map(win.winPosition, '0') };
      }
      const positions = payLine.data;
      const winLength = win.winType.charAt(0);
      for (let i = 0; i < winLength; i += 1) {
        const symbolId = viewZone[`Reel${i + 1}`][positions[i]].symbolId;
        symbols.push(symbolPool[symbolId].SymbolNumber);
      }
      if (win.winType === '5 of a kind') {
        trigger5Win = true;
        fiveWins.push(win);
      }
    });

    if (results.wins.bonusWins) {
      const winLength = results.wins.bonusWins.winType.charAt(0);
      const bonusSymbolId = _.findKey(obj.game.default.gameConfig.symbols, { SymbolType: 'Bonus' });
      const bonusSymbolNumber = symbolPool[bonusSymbolId].SymbolNumber;
      for (let i = 0; i < winLength; i += 1) {
        symbols.push(bonusSymbolNumber);
      }
    }

    if (results.expandData) {
      const reel = Object.keys(results.expandData)[0];
      const columnNumber = results.expandData[reel].columnNumber;
      const reelNumber = reel.charAt(reel.length - 1);
      const symbolId = viewZone[`Reel${reelNumber}`][columnNumber].symbolId;

      const counter = Object.keys(results.expandData).length * 3;
      for (let i = 0; i < counter; i += 1) {
        symbols.push(symbolPool[symbolId].SymbolNumber);
      }
    }
    const payload = {};
    payload.type = 'symbolsWin';
    payload.value = symbols;
    payload.betAmount = betAmount;

    masterPayload.push(payload);

    if (trigger5Win) {
      this.notifyFullRow(masterPayload, fiveWins, viewZone, obj);
    }
  }


  /**
   * If a user wins with a combination of 5 of a kind
   * such wins are notified
   */
  async notifyFullRow(masterPayload, wins, viewZone, obj) {
    const symbols = [];
    const normalizedPayArray = obj.game.default.gameConfig.payLines.normalizedPayArray;
    const symbolPool = obj.game.default.gameConfig.symbols;

    const win = wins[0];
    const winLineId = win.winLineId;
    let payLine = _.find(normalizedPayArray, o => o._id === winLineId);
    if (!winLineId && win.winPosition && win.winPosition.length) {
      payLine = { data: _.map(win.winPosition, '0') };
    }
    const positions = payLine.data;
    let wonWithWild = false;
    for (let i = 0; i < positions.length; i += 1) {
      const symbolId = viewZone[`Reel${i + 1}`][positions[i]].symbolId;
      if (symbolPool[symbolId].SymbolType === 'Wild' || symbolPool[symbolId].SymbolType === 'Wild/Scatter') {
        wonWithWild = true;
      }
      symbols.push(symbolPool[symbolId].SymbolNumber);
    }

    const payload = {};
    payload.type = 'fullRow';
    payload.value = 5;
    payload.wildcard = wonWithWild;
    payload.symbols = symbols;

    masterPayload.push(payload);
  }


  /**
   * When user wins free spins
   * it is notified with the number of free spin that user has won
   */
  async notifyFreeSpins(masterPayload, value, betAmount, inFreespins) {
    const payload = {};
    payload.type = 'freespins';
    payload.value = value;
    payload.betAmount = betAmount;
    payload.inFreespins = inFreespins;

    masterPayload.push(payload);
  }


  /**
   * When free spins are awarded by the casino
   * it is validated with whow with relevant free spin id
   */
  async validateFreeSpins(token, freeSpinObj, betAmount, noofFs) {
    const payload = {};
    payload.ids = [];

    freeSpinObj.forEach((freeSpins) => {
      payload.ids.push(freeSpins.id);
    });

    whowapi.sessionCalls(token, payload);

    this.notifyCasinoFreeSpins(token, betAmount, noofFs);
    return true;
  }


  /**
   * When casino free spins are awarded
   * it is notified with to whow
   */
  async notifyCasinoFreeSpins(token, betAmount, nooFS) {
    const payload = {};
    payload.type = 'casinoFreespins';
    payload.value = nooFS;
    payload.betAmount = betAmount;

    const events = {};
    events.events = payload;
    whowapi.triggerEvents(token, events);
    return true;
  }

}
module.exports = new GameEvents();
module.exports.GameEvents = GameEvents;
