const couchbase = require('couchbase');

/**
connect to couchbase cluster running on amazon
*/
const cluster = new couchbase.Cluster('couchbase://cb-staging.sn.whowgames.net');
cluster.authenticate('admin', 'admin123');

/**
default is the bucket used for all data storage
*/
const bucket = cluster.openBucket('default');
bucket.operationTimeout = 30000;

/**
* simulation data bucket would be used to store simulation data when required
*/
const simulationBucket = cluster.openBucket('simulation');
// bucket.operationTimeout = 100000;

module.exports.bucket = bucket;
module.exports.simulationBucket = simulationBucket;
