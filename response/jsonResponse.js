/**
* Export a function to return a Response in JSON format
* and import it to all controllers to send JSON Response
*/
module.exports = (isError, message, data) => {
  /**
  * response is always bundled with isError flag
  * data flag contains either error message or a api call response
  */
  const response = { isError, message };
  if (data) {
    response.data = data;
  }
  return (JSON.stringify(response));
};
