module.exports = {
  TTL: { USER_TOKEN: (5 * 24 * 60 * 60) },
	/**
	* Response Header constants
	*/
  RES_HEADER: {
    CONTENT_TYPE: 'application/json',
    CHAR_SET: 'utf-8'
  },

  /**
	* Response Messages Constants
	*/
  RES_MESSAGES: {
    OK: 'OK',
    NOT_FOUND: 'Requested resource not found on server',
    PONG: 'pong',
    FAILED_AUTHENTICATION: 'failed to authenticate token',
    SOMETHING_WENT_WRONG: 'something went wrong, please try again.',
    USER_LOGIN: 'user logged in',
    USER_DETAILS: 'user detail in response',
    INVALID_GAME_EVENT: 'invalid game event',
    NO_SESSION_DATA: 'could not fetch session data, please try again.',
    NO_PAYTABLE: 'could not fetch pay table, please try again.',
    SPIN_NOT_ALLOWED: 'spin not allowed right now',
    SPIN_NOT_ALLOWED_NO_GAME_CLICK: 'spin not allowed right now as game click did not happen',
    SPIN_FAILED: 'could not spin right now, an unexpected error occured, please try again.',
    GAME_NOT_FOUND: 'could not find any game from given id',
    UNRECOGNIZED_USER: 'could not recognize user',
    INVALID_SYMBOL: 'this symbol id is invalid',
    NO_USER_SESSION: 'user session is not active',
    NO_VERSION: 'no version found for the given platform',
    NOT_ENOUGH_BALANCE: 'not enough balance',
    GAMBLE_NOT_AVAILABLE: 'gamble cannot be availed right now',
    GAMBLE_NOT_ACTIVE: 'gamble is not active right now',
    GAMBLE_BEING_PICKED: 'please wait while gamble is being processed',
    CARD_BEING_PICKED: 'please wait while card pick is being processed',
    NO_WINS_TO_COLLECT: 'there is no win amount to collect',
    INVALID_CLIENT: 'invalid client',
    INVALID_GAMBLE_CARD: 'invalid gamble card',
    SPIN_NOT_ALLOWED_BONUS_ACTIVE: 'spin not allowed right now, bonus is active',
    BONUS_NOT_ACTIVE: 'Bonus is not active right now',
    BONUS_CANNOT_BE_PLAYED: 'Bonus cannot be played right now',
    BONUS_NOT_ALLOWED_NO_GAME_CLICK: 'Bonus not allowed roght now, game click did not happen',
    ZOMBIE_BONUS_ALREADY_OPEN: 'This zombie is already open, please choose another zombie',
    INVALID_ZOMBIE_ID: 'Invalid Zombie Id',
    INVALID_CARD_ID: 'Invalid Card Id',
    CANNOT_INCREASE_BET_LINE: 'Cannot increase bet line',
    CANNOT_DECREASE_BET_LINE: 'Cannot decrease bet line',
    CANNOT_SET_BET_LINE: 'Cannot set bet line',
    KAD_BONUS_ALREADY_OPEN: 'This card is already open, please choose another card',
    BET_ALTER_DENIED_FREE_SPIN_ACTIVE: 'Cannot change bet free spins are active',
    BET_ALTER_DENIED_BONUS_ACTIVE: 'Cannot change bet bonus is active',
    PICK_NOT_FOUND: 'Selected option does not exist in given pick options'
  },


  /**
   * Error codes for error messages
   */
  RES_ERROR_CODES: {
    NOT_FOUND: 101,
    SOMETHING_WENT_WRONG: 102,
    INVALID_GAME_EVENT: 103,
    NO_SESSION_DATA: 104,
    SPIN_NOT_ALLOWED: 105,
    SPIN_NOT_ALLOWED_NO_GAME_CLICK: 106,
    SPIN_FAILED: 107,
    GAME_NOT_FOUND: 108,
    UNRECOGNIZED_USER: 109,
    NOT_ENOUGH_BALANCE: 110,
    GAMBLE_NOT_ACTIVE: 111,
    NO_WINS_TO_COLLECT: 112,
    INVALID_CLIENT: 113,
    INVALID_GAMBLE_CARD: 114,
    INVALID_GAME: 115,
    BONUS_NOT_ACTIVE: 116,
    PICK_NOT_FOUND: 117,
    SPIN_NOT_ALLOWED_BONUS_ACTIVE: 118,
    VALIDATION_ERROR: 119,
    GAMBLE_BEING_PICKED: 120
  },

  /**
	* HTTP Code constants
	*/
  HTTP_STATUS: {
    OK: 200,
    NOT_FOUND: 404,
    ERROR: 400,
    UNPROCESSABLE_ENTITY: 422
  },

  SPIN_RESULT: { SUCCESS_MESSAGE: 'spin result data' },

  /**
  * Constants for various game events
  */
  GAME_EVENT: {
    INCREASE_BET: 'increaseBet',
    DECREASE_BET: 'decreaseBet',
    MAX_BET: 'maxBet',
    INCREASE_BET_LINE: 'increaseBetLine',
    DECREASE_BET_LINE: 'decreaseBetLine',
    SET_BET_LINE: 'setBetLine'
  },

  /**
   * Game ids of games
   */
  GAME_IDS: {
    UNICORN: 'bc76d823-4a8e-429f-b06d-8ffa54137fe1',
    // UNICORN_SLOT: 'bc76d823-4a8e-429f-b06d-8ffa54137fe1',
    SECRET_OF_AMUN: '3f75d8e3-5367-4bfa-a728-8b220b49be1e',
    PARIS_TOURIST: 'cc87969a-b28f-4358-82bb-f77854500609',
    ZOMBIE_APOCALYPSE: '71d50423-3cad-499c-b9e2-8cbc3e21a149',
    KNIGHT_DRAGONS: '41a1bfe0-a99d-460e-a94c-f6c2c7aa0d4a',
    BACKER_STREET_DETECTIVES: '96a72a01-057c-4cc7-ad7a-3d50594a2879',
    CINDERELLA: '17ce4b1c-a3c5-46ed-85e5-b0baa0e57c76',
    PALEO_WILDS_OLD: '848752b5-0a66-488e-8b71-b2f8e773a11c',
    QIN_DYNASTY_OLD: '44b2826c-650c-4145-b18d-3b0973c6d0a5',
    QIN_DYNASTY: '466ec804-1aca-404f-a46a-d3807c34bc24',
    PALEO_WILDS: '774d6038-131f-4467-b4d6-6b3bd5df1fbf',
    GATSBY: '32453d99-16c6-4fde-b669-6bb041bfdcac',
    NOBLE_MUSKETEERS: 'cbd29eac-accf-46c4-90e5-7f6cdd58d0ea',
    DEEP_SEAS: 'd1d1bd17-61a1-4ed2-9c38-9e080b4d1c5c',
    DRAGON_PHOENIX: '302a088b-5596-4d12-8830-e85143e94509',
    JUNGLE_STORY: '8c2a7906-9aec-435e-a847-bb459b111fe6',
    POSEIDON: 'd3e4b9dd-bdc9-4e2a-99df-ad6725103136',
    ZEUS: 'e66188b0-2115-4696-ad71-39a0a0580f11',
    PYRAMIDS_OF_GIZA: 'd3401dc4-8a61-43b0-9764-50c8c7daef3d',
    PACIFIC_RAIL_ROAD: 'b397d20f-6fb1-45cd-9aee-62e561648f48',
    CASANOVA: 'c897cbc7-3654-473a-ba26-eea8300771ea',
    CARNEVALE: 'd94ad420-486a-43ff-8284-a8791a6b6d5c',
    DIVINE_SPIRIT: '83f629cb-e541-4ea0-8b73-de67ed534fb0'
  },


  /**
   * Mobile/standalone platforms of unity client
   */
  PLATFORMS: {
    AN: 'ANDROID',
    IOS: 'IOS',
    MAC: 'MACINTOSH',
    WIN: 'WINDOWS'
  },

  /**
   * Gamgle feature related constants
   */
  GAMBLE: {
    SECRET_OF_AMUN_RANDOM_RANGE: 52,
    SECRET_OF_AMUN_MAX_GAMBLE: 5,
    RED: 'red',
    BLACK: 'black'
  },


  /**
   * Cinderella game related constants
   */
   CINDERELLA: { NOOF_LINES: 1 },

   /**
    * Noble Musketeers game related constants
    */
   NOBLE_MUSKETEERS: { NOOF_LINES: 50 },

   /**
    * Deep Seas game related constants
    */
   DEEP_SEAS: { NOOF_LINES: 50 },

   /**
    * Dragon & Phoenix game related constants
    */
   DRAGON_PHOENIX: { NOOF_LINES: 60 }
};
