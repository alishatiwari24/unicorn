const logger = require('../helpers/firehose');
const Game = require('../helpers/gamefunction');
const gameSession = require('./gamesession');

/**
* LogExporter logs various log events from spin api
* ships logs to Amazon Elasticsearch and S3 using firehose
* primary elastic search index is used to store log data
*/
class LogExporter {

  /**
  * This function models the spin result document based on the analytics requirement
  * it deletes the fields not required and fetches the field required from database
  */
  logSpinResult(result, token, gameId) {
    if (result && token && gameId) {
      delete result.levelData;
      delete result.game;
      delete result.jackpot;
      result.gameId = gameId;
      const gameData = Game.getGame(gameId);
      result.baseGameId = gameData.game.default.baseGameId;
      result.gameVersion = gameData.game.default.versionName;
      result.stage = gameData.game.default.stage;

      /**
      * removes un necessary data from while logging spin results
      */
      const symbols = {};
      const types = {};
      if (result.wins.winLines.length > 0) {
        result.wins.winLines.forEach((line) => {
          if (!types[line.winType]) {
            types[line.winType] = 0;
          }
          if (!symbols[line.symbolId]) {
            symbols[line.symbolId] = 0;
          }
          types[line.winType]++;
          symbols[line.symbolId]++;
        });
        result.wins.types = types;
        result.wins.symbols = symbols;
      }
      gameSession.getUserSession(token, gameId)
      .then((session) => {
        result.user = {};
        result.user.currentBet = session.eventData.currentBet;
        result.user.currentLines = session.eventData.currentLines;
        result.user.totalBet = session.eventData.currentBet * session.eventData.currentLines;
        result.user.token = token;
        result.user.userId = session.eventData.userDetails.id;

        logger.info(result);
      }).catch((err) => {
        console.log(err);
      });
    }
  }
}
module.exports = new LogExporter();
