import * as nj from 'numjs';
import { Compute } from '../../compute';
import rng = require('../../rng.js');
import _ from 'lodash';
import gameEvents = require('../../../helpers/game-events');
import {
    ViewZone, Result, ComputeData, CasanovaGame, Session, SessionRequest, SymbolsDesc,
    NormalizedPayArray, CasanovaGameConfig, ComputeDataResult, Wins, ResultSession,
} from 'interface/q219/cassanova-type';
import { WildOfKind, FreeSpinData, LevelData } from 'interface/Game';
import moment from 'moment';
import * as ndarray from 'interface/nd-impl';

class Casanova extends Compute {

    /**
     * This function is responsible to calculate result for a spin event
     * taking into account
     * view zone, pay lines, pay table
     */
    public async compute(game: CasanovaGame['game'], session: Session): Promise<ComputeData> {
        // get reel config
        game.default.gameConfig.reelConfig = this.getReelConfig(game.default.gameConfig, session);

        const noofReels: number = game.default.gameConfig.reelConfig.NumberOfReels as number;
        const columnSize: number = parseInt(game.default.gameConfig.viewZone.toString().charAt(0), 0);


        // start calculating view zone
        const arrays: ndarray<number> = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels) as ndarray;
        // const arrays = nj.arange<string>(noofReels * columnSize).reshape(noofReels, columnSize);

        // random numbers are fetched from reel configuration
        const randomNumber: number[] = rng.generateRandomNumber(game.default.gameConfig.reelConfig);

        // view zone is calculated below
        const scatter: number[] = [];
        const wildOfKind: WildOfKind = {};
        this.generateViewZone(randomNumber, noofReels, columnSize, scatter, game, arrays, session, wildOfKind);

        // if expanding wild is detected complete reel is replaced with non expaning symbols each
        const deepCopyArray = _.cloneDeep(arrays);
        // load pay arrays
        const payArray = game.default.gameConfig.payLines.normalizedPayArray;
        const copyPayArray: NormalizedPayArray[] = _.cloneDeep(payArray);
        const results: ComputeDataResult = { mainGameResult: [] };
        try {
            await Promise.all([
                this.computeWins(arrays, columnSize, copyPayArray, game, results.mainGameResult, wildOfKind),
                super.freeSpinCompute(scatter, results, game),
            ]);
            return {
                viewZone: deepCopyArray,
                results: results,
                scatter: scatter,
            };
        } catch (error) {
            return { error: 'error in view zone' };
        }
    }


    /**
     * This function generates view zone base on random numbers
     * view zone is calculated to be shown to user
     */
    public generateViewZone(randomNumber: number[], noofReels: number, columnSize: number, scatter: number[],
                            game: CasanovaGame['game'], arrays: ndarray<number>, session: Session, wildOfKind: WildOfKind) {
        let lastPushed: number = -1;
        for (let i = 0; i < noofReels; i += 1) {
            for (let j = 0; j < columnSize; j += 1) {
                const symbolId = super.getSymbol(j, i, game.default.gameConfig.reelConfig, randomNumber);
                if (super.getSymbol(j + 1, i, game.default.gameConfig.reelConfig, randomNumber) as string === symbolId &&
                    game.default.gameConfig.symbols[symbolId].SymbolType === 'Scatter' && lastPushed !== i && j + 1 !== columnSize) {
                    scatter.push(i);
                    lastPushed = i;
                }

                if (game.default.gameConfig.symbols[symbolId].SymbolType === 'Wild') {
                    let wildMultiplier: number;
                    if (!_.isEmpty(session.eventData.freeSpinData) &&
                        (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0)) {
                        wildMultiplier = game.default.gameConfig.specialFeature.wildMultiplier.freeGame[`reel${i + 1}`] as number;
                    } else {
                        wildMultiplier = game.default.gameConfig.specialFeature.wildMultiplier.mainGame[`reel${i + 1}`] as number;
                    }

                    wildOfKind[`${i + 1}-${j}`] = { multiplier: wildMultiplier, reel: i + 1, row: j };
                }
                arrays.set(j, i, symbolId);
            }
        }
    }


    /**
     * This function computes wins based on view zone
     * win is calculated starting from left i.e. from 1st reel
     * Wilds and scatter are taken into consideration while computing wins
     */
    public async computeWins(arrays: ndarray<number>, columnSize: number, payArray: NormalizedPayArray[], game: CasanovaGame['game'],
                             results: ComputeDataResult['mainGameResult'], wildFromViewZone: WildOfKind) {
        const j = 0;
        for (let i = 0; i < columnSize; i += 1) {
            for (const [k, v] of payArray.entries()) {
                let symbolId = arrays.get(i, j);
                const payLine = v.data;
                let ofAKind = [];
                let totalWildMultiplier = 0;
                const wildPositionMultiplier = {};
                if (payLine[0] === i) {
                    ofAKind.push('test');
                    for (let l = 1; l < payLine.length; l += 1) {
                        if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType === 'Wild') {
                            // const currentWildMultiplier = _.find(wildFromViewZone, w => w.reel === (l + 1));
                            const currentWildMultiplier = wildFromViewZone[`${l + 1}-${payLine[l]}`];
                            totalWildMultiplier += currentWildMultiplier.multiplier;
                            wildPositionMultiplier[`reel${l + 1}`] = { row: payLine[l], multiplier: currentWildMultiplier.multiplier };
                        }
                        if (game.default.gameConfig.symbols[symbolId].SymbolType === 'Wild' &&
                            game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType !== 'Scatter') {
                            symbolId = arrays.get(payLine[l], l);
                            ofAKind.push('test');
                            continue;
                        }
                        if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType !== 'Wild' &&
                            arrays.get(payLine[l], l) !== symbolId) {
                            break;
                        }
                        ofAKind.push('test');
                    }
                    if (ofAKind.length > 0 && game.default.gameConfig.payTable[symbolId][ofAKind.length]) {
                        results.push({
                            symbolId: `${symbolId}`, winType: `${ofAKind.length} of a kind`, winLineId: v._id,
                            multiplier: game.default.gameConfig.payTable[symbolId][ofAKind.length].Multiplier,
                            totalWildMultiplier: totalWildMultiplier, wildMultiplierPosition: wildPositionMultiplier,
                        });
                    }
                }
                ofAKind = [];
            }
        }
        return results;
    }


    /**
     * getReelConfig method is used to get reelConfig for free game or main game
     * @param  {[game]} game    game contains game object
     * @param  {[session]} session session object user session and games session details
     */
    public getReelConfig(gameConfig: CasanovaGameConfig, session: Session) {
        if (!_.isEmpty(session.eventData.freeSpinData) &&
            (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0)) {
            return gameConfig.freeSpin.reelConfigSet[0];
        }
        const reelSet = gameConfig.specialFeature.reelSwitching.mainGame[
            Math.floor(Math.random() * gameConfig.specialFeature.reelSwitching.mainGame.length)
        ];
        return gameConfig.reelConfigSet[reelSet];
    }


    /**
     * generateSpinResponse method is used to generate spin response on every spin
     * and update user session and call callWhowBluehare api
     * @param obj   obj  contains game object
     * @param computeData computeData contains data that is computed on every spin
     * @param token   token contains user token
     * @param sessionRequestKey  its contains that sessionRequestKey such as cas
     * @param sessionRequest sessionRequest contains game session request
     */
    public generateSpinResponse(obj: CasanovaGame, computeData: ComputeData, token: string,
                                sessionRequestKey: string, sessionRequest: SessionRequest, session: Session) {
        const noofReels: number = obj.game.default.gameConfig.reelConfig.NumberOfReels as number;
        const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0), 10);
        const array = computeData.viewZone;
        const viewZone: ViewZone = {};
        for (let i = 0; i < noofReels; i += 1) {
            const line: SymbolsDesc[] = [];
            for (let j = 0; j < columnSize; j += 1) {
                line.push({
                    symbolId: `${array.get(j, i)}`,
                    symbolType: obj.game.default.gameConfig.symbols[`${array.get(j, i)}`].SymbolType,
                    symbolName: obj.game.default.gameConfig.symbols[`${array.get(j, i)}`].SymbolName,
                });
            }
            viewZone[`Reel${i + 1}`] = line;
        }
        viewZone.viewZone = obj.game.default.gameConfig.viewZone as number;
        const result: Result = {
            viewZone: viewZone,
            wins: {} as Wins,
            freeSpinData: {} as FreeSpinData,
            levelData: {} as LevelData,
            noofReels: noofReels,
            session: {} as ResultSession,
        };
        if (computeData.results.freeSpins) {
            if (!_.isEmpty(session)) {
                if (_.isEmpty(session.eventData.freeSpinData)) {
                    result.freeSpinData.freeSpins = computeData.results.freeSpins;
                    result.freeSpinData.currentFreeSpin = 0;
                    result.freeSpinData.winAmount = 0;
                    session.eventData.freeSpinData = result.freeSpinData;
                } else {
                    result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
                    result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin;
                    result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
                    session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
                    session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
                }
                result.freeSpinData.isFreeSpinAwarded = true;
                result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
                if (session.eventData.freeSpinData.isFreeSpinAwarded) {
                    delete session.eventData.freeSpinData.isFreeSpinAwarded;
                    delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
                }
                session.event = 'spin';
                session._updatedTimestamp = moment.utc();
                const sessionClone = JSON.parse(JSON.stringify(session));
                if (!_.isEmpty(sessionClone.eventData.freeSpinData)) {
                    computeData.results.mainGameResult.forEach(winLineResult => {
                        (sessionClone.eventData.freeSpinData.winAmount as number) += (winLineResult.multiplier
                            * (sessionClone.eventData.currentBet) * (winLineResult.totalWildMultiplier || 1));
                    });
                }
                super.updateSession(sessionClone, obj._id);
            }
            result.isSessionUpdated = true;
        }
        let total = 0;
        computeData.results.mainGameResult.forEach(resultObj => {
            resultObj.winAmount = resultObj.multiplier * (session.eventData.currentBet) * (resultObj.totalWildMultiplier || 1);
            total += resultObj.winAmount;
            delete resultObj.multiplier;
        });
        result.wins.winCoins = total;
        result.wins.winLinesCount = computeData.results.mainGameResult.length;
        result.wins.winLines = [];
        result.wins.winLines = computeData.results.mainGameResult;
        result.session.sessionRequestKey = sessionRequestKey;
        result.session.sessionRequest = sessionRequest;
        gameEvents.notifyGameEvents(obj, token, session, result, computeData.results.freeSpins);
        // mask view zone here
        result.viewZone = super.maskViewZone(result.viewZone, obj, _.isEmpty(session.eventData.freeSpinData));
        if (_.isEmpty(session.eventData.freeSpinData)) {
            return super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet
                * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id);
        } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
            result.freeSpinData = session.eventData.freeSpinData;
            if (computeData.results.freeSpins) {
                result.freeSpinData.isFreeSpinAwarded = true;
                result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
            }
            if (!computeData.results.freeSpins) {
                result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
            }

            if (result.wins.winCoins > 0) {
                result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
                session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
            } else {
                result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
            }
            // call whow api
            if (session.eventData.freeSpinData.currentFreeSpin !== 0) {
                return super.callWhowBluehare(token, result, computeData, 0, 10, total, session,
                    sessionRequestKey, sessionRequest, obj._id);
            } else {
                return super.callWhowBluehare(token, result, computeData,
                    (session.eventData.currentBet * session.eventData.currentLines), 0,
                    total, session, sessionRequestKey, sessionRequest, obj._id);
            }
        } else {
            return super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet
                * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id);
        }
    }

}
export default new Casanova();
