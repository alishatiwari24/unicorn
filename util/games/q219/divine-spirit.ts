import * as nj from 'numjs';
import * as BaseNdArray from '../../../interface/q219/index';
import { Compute } from '../../compute';
import {
    GameObject, ViewZone, Result, ComputeData,
    SpinResponseViewZone, SpinResponseWinLines, GameDetail,
    SessionObject, SessionRequestValue, SpinResponseWins,
    FreeSpinDataSchema, LevelData, SpinResultSessionObject, NormalizedPayArray, ComputeDataResult,
} from 'interface/q219/divine-spirit';
import rng = require('../../rng.js');
import _ from 'lodash';
import gameEvents = require('../../../helpers/game-events');
import moment from 'moment';

/**
 * DivineSpirit class contains all utils method which is used to
 * generate and compute on every spin of game
 */
class DivineSpirit extends Compute {

    /**
     * Compute Method is used to perform computation on every spin
     * @param  game    it contains game data such as game config
     * @param  gameId  it contains game id of DivineSpirit
     * @param  token   contains token of user
     * @param  session cotains session details of user
     */
    public async compute(game: GameObject, session: SessionObject) {        
        // get reel config
        const noofReels: number = game.default.gameConfig.reelConfig.NumberOfReels as number;
        const columnSize: number = parseInt(game.default.gameConfig.viewZone.toString().charAt(0), 0);

        // start calculating view zone
        const arrays: BaseNdArray<number> = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
        // random numbers are fetched from reel configuration
        
        
		const randomNumber: number[] = rng.generateRandomNumber(game.default.gameConfig.reelConfig);
        console.log(randomNumber);
		//const randomNumber: number[] = [24,35,30,33,35];
        
        
        // view zone is calculated below
        let scatter: number[] = [];
        this.generateViewZone(randomNumber, noofReels, columnSize, scatter, game, arrays, session);
        scatter = _.uniq(scatter);
        // load pay arrays
        const payArray: NormalizedPayArray[] = game.default.gameConfig.payLines.normalizedPayArray;
        const copyPayArray: NormalizedPayArray[] = _.cloneDeep(payArray);
        let results: ComputeDataResult = { mainGameResult: [] };
        try {
            
            await Promise.all([
                super.computeWins(arrays, columnSize, copyPayArray, game, results.mainGameResult, session),
                super.freeSpinCompute(scatter, results, game),
                super.wildPayCompute(arrays, columnSize, payArray, game, results.mainGameResult),
            ]);
            results = this.refineResults(results);
            return { viewZone: arrays, results: results, scatter: scatter };
        } catch (error) {
            console.log(error);
            return { error: 'error in view zone' };
        }
    }


    /**
     * refineResults - this function checks for wild pay and normal pay on same line, and considers the highest paying combination
     */
    public refineResults(results: ComputeDataResult) {
        const winLines = {};
        const highestResults: ComputeDataResult = { mainGameResult: [] };
        if (results.freeSpins) {
            highestResults.freeSpins = results.freeSpins;
        }
        results.mainGameResult.forEach(result => {
            if (result.winLineId) {
                // if given winLine is there in winLines object, check for maximum of thw wins, and replace it with max result
                if (winLines[result.winLineId]) {
                    highestResults[winLines[result.winLineId].index] = _.maxBy([winLines[result.winLineId].result, result], 'multiplier');
                } else {
                    // else, add it to winLine list
                    winLines[result.winLineId] = { result: result, index: highestResults.mainGameResult.length };
                    highestResults.mainGameResult.push(result);
                }
            }
        });
        return highestResults;
    }


    /**
     * This function generates view zone base on random numbers
     * view zone is calculated to be shown to user
     */
    public generateViewZone(randomNumber: number[], noofReels: number, columnSize: number,
                            scatter: number[], game: GameObject, arrays: BaseNdArray<number | string>, session: SessionObject) {
        if (!_.isEmpty(session.eventData.freeSpinData) &&
            (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0)) {
            for (let i = 0; i < noofReels; i += 1) {
                for (let j = 0; j < columnSize; j += 1) {
                    const symbolId: string = super.getSymbol(j, i, game.default.gameConfig.freeSpin.reelConfig, randomNumber);
                    if (game.default.gameConfig.symbols[symbolId].SymbolType === 'Scatter') {
                        scatter.push(i);
                    }
                    arrays.set(j, i, symbolId);
                }
            }
        } else {
            for (let i = 0; i < noofReels; i += 1) {
                for (let j = 0; j < columnSize; j += 1) {
                    const symbolId: string = super.getSymbol(j, i, game.default.gameConfig.reelConfig, randomNumber);
                    if (game.default.gameConfig.symbols[symbolId].SymbolType === 'Scatter') {
                        scatter.push(i);
                    }
                    arrays.set(j, i, symbolId);
                }
            }
        }
    }


    /**
     * generateSpinResponse method is used to generate spin response on every spin
     * and update user session and call callWhowBluehare api
     * @param obj obj  contains game object
     * @param computeData computeData contains data that is computed on every spin
     * @param token token contains user token
     * @param sessionRequestKey  its contains that sessionRequestKey such as cas
     * @param sessionRequest sessionRequest contains game session request
     */
    public generateSpinResponse(obj: GameDetail, computeData: ComputeData, token: string,
                                sessionRequestKey: string, sessionRequest: SessionRequestValue, session: SessionObject) {
        try {
            const noofReels: number = obj.game.default.gameConfig.reelConfig.NumberOfReels;
            const columnSize: number = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0), 10);
            const array: BaseNdArray<number | string> = computeData.viewZone;
            const viewZone: ViewZone = {};
            for (let i = 0; i < noofReels; i += 1) {
                const line: SpinResponseViewZone[] = [];
                for (let j = 0; j < columnSize; j += 1) {
                    line.push({
                        symbolId: array.get(j, i) as string,
                        symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType,
                        symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName,
                    });
                }
                viewZone[`Reel${i + 1}`] = line;
            }
            viewZone.viewZone = obj.game.default.gameConfig.viewZone as number;
            let result: Result = {} as Result;
            result.viewZone = viewZone;
            result.wins = {} as SpinResponseWins;
            result.freeSpinData = {} as FreeSpinDataSchema;
            result.levelData = {} as LevelData;
            result.noofReels = noofReels;

            if ((computeData.results as { freeSpins: number }).freeSpins) {
                result = this.buildFreeSpins(obj, computeData, session, result);
            }
            let total: number = 0;
            let freeSpinTriggerWin: number = 0;
            computeData.results.mainGameResult.forEach(resultObj => {
                resultObj.winAmount = resultObj.multiplier * (session.eventData.currentBet);
                total += resultObj.winAmount;
                delete resultObj.multiplier;
            });

            if (computeData.scatter.length > 2) {
                freeSpinTriggerWin += ((session.eventData.currentBet * session.eventData.currentLines) *
                    (obj.game.default.gameConfig.freeSpin.freeSpinWins[`${computeData.scatter.length}`].winMultiplier));
            }
            if (freeSpinTriggerWin > 0) {
                result.wins.freeSpinTriggerWin = freeSpinTriggerWin;
            }
            total += freeSpinTriggerWin;
            result.wins.winCoins = total;
            result.wins.winLinesCount = computeData.results.mainGameResult.length;
            result.wins.winLines = [];
            computeData.results.mainGameResult.forEach((row: SpinResponseWinLines) => result.wins.winLines.push(row));
            result.session = {} as SpinResultSessionObject;
            result.session.sessionRequestKey = sessionRequestKey;
            result.session.sessionRequest = sessionRequest;
            gameEvents.notifyGameEvents(obj, token, session, result, computeData.results.freeSpins);
            // mask view zone here
            return this.buildResponse(obj, computeData, token,
                sessionRequestKey, sessionRequest, session, result, total);
        } catch (error) {
            console.log(error, 'in spin genre');
        }

    }


    /**
     * generateFreeSpinAwardResponse method is used to generate spin response when free spin is awarded
     * @param  obj  contains game obj
     * @param  computeData contains computeData on every spin
     * @param  session  contains userSession
     * @param  result   contains result of every generated spin response object
     * @return  result contains result
     */
    public buildFreeSpins(obj: GameDetail, computeData: ComputeData,
                          session: SessionObject, result: Result) {
        if (!_.isEmpty(session)) {
            if (_.isEmpty(session.eventData.freeSpinData)) {
                result.freeSpinData.freeSpins = computeData.results.freeSpins;
                result.freeSpinData.currentFreeSpin = 0;
                result.freeSpinData.winAmount = 0;
                session.eventData.freeSpinData = result.freeSpinData;
            } else {
                result.freeSpinData.freeSpins = computeData.results.freeSpins +
                    session.eventData.freeSpinData.freeSpins;
                result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin;
                result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
                session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
                session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
            }
            result.freeSpinData.isFreeSpinAwarded = true;
            result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
            if (session.eventData.freeSpinData.isFreeSpinAwarded) {
                delete session.eventData.freeSpinData.isFreeSpinAwarded;
                delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
            }
            session.event = 'spin';
            session._updatedTimestamp = moment.utc();
            const sessionClone = JSON.parse(JSON.stringify(session));
            if (!_.isEmpty(sessionClone.eventData.freeSpinData)) {
                // sessionClone.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
                computeData.results.mainGameResult.forEach(winLineResult => {
                    sessionClone.eventData.freeSpinData.winAmount += (winLineResult.multiplier * (sessionClone.eventData.currentBet));
                });
                let freeSpinWin = 0;
                if (computeData.scatter.length > 2) {
                    freeSpinWin += ((sessionClone.eventData.currentBet * sessionClone.eventData.currentLines) *
                        (obj.game.default.gameConfig.freeSpin.freeSpinWins[`${computeData.scatter.length}`].winMultiplier));
                }
                sessionClone.eventData.freeSpinData.winAmount += freeSpinWin;
            }
            super.updateSession(sessionClone, obj._id);
        }
        result.isSessionUpdated = true;
        return result;
    }


    /**
     * generateMaskedViewZoneAndCallBlueWhare is used to generate maskViewZone and callWhowBluehare methods
     * according to follwing conditions
     * @param  obj  contains game obj
     * @param  computeData contains computeData on every spin
     * @param  session  contains userSession
     * @param  result   contains result of every generated spin response object
     * @return                   [description]
     */
    public buildResponse(obj: GameDetail, computeData: ComputeData, token: string,
                         sessionRequestKey: string, sessionRequest: SessionRequestValue,
                         session: SessionObject, result: Result, total: number) {
        result.viewZone = super.maskViewZone(result.viewZone, obj, _.isEmpty(session.eventData.freeSpinData));
        if (_.isEmpty(session.eventData.freeSpinData)) {
            return super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet
                * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id);
        } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
            result.freeSpinData = session.eventData.freeSpinData;
            if (computeData.results.freeSpins) {
                result.freeSpinData.isFreeSpinAwarded = true;
                result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
            }
            if (!computeData.results.freeSpins) {
                result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
            }

            if (result.wins.winCoins > 0) {
                result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
                session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
            } else {
                result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
            }
            // call whow api
            if (session.eventData.freeSpinData.currentFreeSpin !== 0) {
                return super.callWhowBluehare(token, result, computeData, 0,
                    10, total, session, sessionRequestKey, sessionRequest, obj._id);
            } else {
                return super.callWhowBluehare(
                    token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines),
                    0, total, session, sessionRequestKey, sessionRequest, obj._id,
                );
            }
        } else {
            return super.callWhowBluehare(
                token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines),
                0, total, session, sessionRequestKey, sessionRequest, obj._id,
            );
        }
    }

}

export default new DivineSpirit();
