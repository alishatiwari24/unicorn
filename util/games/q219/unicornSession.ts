const { bucket } = require('../../../helpers/couchbase');
const nj = require('numjs');
const rng = require('../../../util/rng');
const moment = require('moment');
const _ = require('lodash');
const { Compute } = require('../../compute');
const gameSession = require('../../gamesession');
const gameEvents = require('../../../helpers/game-events');
import { SpinResponseWins, Result, ViewZone, LevelData, FreeSpinDataSchema } from 'interface/q219/divine-spirit';

class Unicorn extends Compute {
    /**
    * This function is responsible to calculate result for a spin event
    * taking into account
    * view zone, pay lines, pay table
    */
  compute(game, gameId, token, session) {    
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));

    // start calculating view zone
    let arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);

    // random numbers are fetched from reel configuration
    const randomNumber = rng.generateRandomNumber(game.default.gameConfig.reelConfig);
    // view zone is calculated below
    let expanding = [];
    const scatter = [];
    this.generateViewZone([4, 0, 9, 20, 18], noofReels, columnSize, expanding, scatter, game, arrays, session);
        
    // if expanding wild is detected complete reel is replaced with non expaning symbols each
    const deepCopyArray = arrays.clone();
    
    expanding = _.uniq(expanding);
    if (expanding.length > 0) {
      const copyArray = arrays.clone();
      const wildSymbolId = _.findKey(game.default.gameConfig.symbols, { SymbolType: 'Wild', isExpandingWild: false });
      expanding.forEach((i) => {
        for (let j = 0; j < columnSize; j += 1) {
          copyArray.set(j, i, wildSymbolId);
        }
      });
      arrays = copyArray.clone();
    }

    // load pay arrays
    let payArray = game.default.gameConfig.payLines.normalizedPayArray;
    const copyPayArray = JSON.parse(JSON.stringify(payArray));


    // result based on view zone is calculated below
    const results = [];

    return new Promise((resolve) => {
      const currentLines = session.eventData.currentLines;
      const lineArrays = game.default.gameConfig.selectablePaylines[currentLines];
      payArray.forEach((pay) => {
        const object = _.find(lineArrays, o => o === pay._id);
        if (!object) {
          _.remove(copyPayArray, arr => arr._id === pay._id);
        }
      });
      payArray = copyPayArray;
      arrays = deepCopyArray.clone();
      /*
      * compute wins, for given viewzone
      * compute free spins,if user got any
      * compute scatter wins, if user got any
      * and compute for wild pay
      */
      Promise.all([
        this.computeWins(arrays, columnSize, payArray, game, results),
        this.freeSpinCompute(scatter, results, game)
        // this.scatterPayCompute(arrays, columnSize, payArray, game, results),
        // this.wildPayCompute(deepCopyArray, columnSize, payArray, game, results)
      ]).then(() => {
        resolve({ viewZone: arrays, results, wildReel: session.eventData.wildReel });
      }).catch((err) => {
        resolve({ error: 'error in view zone' });
      });
    });
  }

  generateViewZone(randomNumber, noofReels, columnSize, expanding, scatter, game, arrays, session) {
 
    /** Setting currentReel to display roaming stacked wild */
    let currentReel = (session.eventData.wildReel-1) || 5;
    const stackedWild = game.default.gameConfig.specialFeatures.data.Reel.SymbolDistribution[1].SymbolId;
    if (!_.isEmpty(session.eventData.freeSpinData) && (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0)) {
      for (let i = 0; i < noofReels; i += 1) {
        for (let j = 0; j < columnSize; j += 1) {
          const symbolId = this.getSymbol(j, i, game.default.gameConfig.freeSpin.reelConfig, randomNumber);
          if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Wild' && game.default.gameConfig.symbols[symbolId].isExpandingWild == true) {
            expanding.push(i);
          }
          if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Scatter') {
            scatter.push(i);
          }
          arrays.set(j, i, symbolId);
        }
      }
      session.eventData.wildReel = currentReel;
      arrays.set(0, currentReel-1, stackedWild);
      arrays.set(1, currentReel-1, stackedWild);
      arrays.set(2, currentReel-1, stackedWild);
    } else {
      // const stackedWild = game.default.gameConfig.specialFeatures.data.Reel.SymbolDistribution[1].SymbolId;
      for (let i = 0; i < noofReels; i += 1) {
        for (let j = 0; j < columnSize; j += 1) {
          const symbolId = this.getSymbol(j, i, game.default.gameConfig.reelConfig, randomNumber);          
          if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Wild' && game.default.gameConfig.symbols[symbolId].isExpandingWild == true) {
            expanding.push(i);
          }
          if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Scatter') {
            scatter.push(i);
          }
          arrays.set(j, i, symbolId);
        }
      }
        session.eventData.wildReel = currentReel;
        arrays.set(0, currentReel-1, stackedWild);
        arrays.set(1, currentReel-1, stackedWild);
        arrays.set(2, currentReel-1, stackedWild);
    }
  }

  /**
  * This function is responsible to generate spin response
  * it includes view zone, wins, user details like wallet, bet array etc
  * necessary render view for the user
  */
  generateSpinResponse(obj, computeData, token, sessionRequestKey, sessionRequest, session) {
    // determine view port
    try {
      console.log('in unicornsession generatespinresponse', computeData);
     
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));
    // create reels for view zone
    const array = computeData.viewZone;
    const viewZone: ViewZone = {};

    for (let i = 0; i < noofReels; i += 1) {
      const line = [];
      for (let j = 0; j < columnSize; j += 1) {
        line.push({ symbolId: array.get(j, i), symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType, symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName });
      }
      viewZone[`Reel${i + 1}`] = line;
    }
    viewZone.viewZone = obj.game.default.gameConfig.viewZone;


    // create win document
    const result: Result = {} as Result;
    result.viewZone = viewZone;
    result.wins = {} as SpinResponseWins;
    result.freeSpinData = {} as FreeSpinDataSchema;
    result.levelData = {} as LevelData;
    result.noofReels = noofReels;

    result.wildReel = computeData.wildReel;
    session.eventData.wildReel = result.wildReel;
    return new Promise((resolve) => {
        // if free spin is present
      if (computeData.results.freeSpins) {
        if (!_.isEmpty(session)) {
          if (_.isEmpty(session.eventData.freeSpinData)) {
            result.freeSpinData.freeSpins = computeData.results.freeSpins;
            result.freeSpinData.currentFreeSpin = 0;
            result.freeSpinData.winAmount = 0;

            session.eventData.freeSpinData = result.freeSpinData;
          } else {
            result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;

            session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
            session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
          }
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
          if (session.eventData.freeSpinData.isFreeSpinAwarded) {
            delete session.eventData.freeSpinData.isFreeSpinAwarded;
            delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
          }
          session.event = 'spin';
          session._updatedTimestamp = moment.utc();
          this.updateSessionAndSessionRequest(session, obj._id, sessionRequestKey, sessionRequest);
        }
        result.isSessionUpdated = true;
      }
      console.log('in the middle....');
      
        // total win amount calculated based on wins from pay line
      let total = 0;
      console.log(computeData.results);
      
      computeData.results.forEach((resultObj) => {
        resultObj.winAmount = resultObj.multiplier * (session.eventData.currentBet);
        total += (resultObj.multiplier * (session.eventData.currentBet));
        delete resultObj.multiplier;
      });
      console.log(total);
    
      result.wins.winCoins = total;
      result.wins.winLinesCount = computeData.results.length;

      result.wins.winLines = computeData.results;

        // put user session data
      result.session = {};
      result.session.sessionRequestKey = sessionRequestKey;
      result.session.sessionRequest = sessionRequest;
      
      // mask view zone here
      result.viewZone = this.maskViewZone(result.viewZone, obj);
                console.log(session , 'main');

      if (_.isEmpty(session.eventData.freeSpinData)) {
          console.log(session , 'main');
          
          // no free spins
        resolve(this.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session));
      } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
          // some free spins remaining
        result.freeSpinData = session.eventData.freeSpinData;

        if (computeData.results.freeSpins) {
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
        }

        if (!computeData.results.freeSpins) {
          result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
        }

        if (result.wins.winCoins > 0) {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
          session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
        } else {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
        }
        console.log('in the end.......');
        
          // call whow api
        resolve(this.callWhowBluehare(token, result, computeData, 0, 10, total, session));
      } else {
          // free spin object exists and there are no free spins remaining
        resolve(this.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session));
      }
    });
    } catch (error) {
      console.log(error);
    }
    
  }

}

export default new Unicorn();