/* eslint-disable max-statements, max-params, prefer-destructuring, no-ternary, multiline-ternary, no-console */
const nj = require('numjs');
const rng = require('../rng');
const moment = require('moment');
const _ = require('lodash');
const { Compute } = require('../compute');
const gameEvents = require('../../helpers/game-events');

/**
 * Zeus specific features are implemented in this class
 * This class would be reponsible to update game related user sessions
 * This class would be reponsible to compute for expanding wild
 */
class Zeus extends Compute {

  /**
  * This function is responsible to calculate result for a spin event
  * taking into account
  * view zone, pay lines, pay table
  */
  compute(game, gameId, token, session) {
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    // start calculating view zone
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    // random numbers are fetched from reel configuration
    const randomNumber = rng.generateRandomNumber(game.default.gameConfig.reelConfig);
    // view zone is calculated below
    const expanding = [];
    const scatter = [];
    this.generateViewZone(randomNumber, noofReels, columnSize, expanding, scatter, game, arrays, session);
    // if expanding wild is detected complete reel is replaced with non expaning symbols each
    const deepCopyArray = arrays.clone();
    if (expanding.length > 0) {
      const wildSymbolId = _.findKey(game.default.gameConfig.symbols, { SymbolType: 'Wild', isExpandingWild: true });
      expanding.forEach((expandingPosition) => {
        for (let j = 0; j < columnSize; j += 1) {
          if (game.default.gameConfig.symbols[arrays.get(j, expandingPosition.column)].SymbolType === 'scatter') {
            scatter.splice(scatter.indexOf(expandingPosition.column), 1);
          }
          arrays.set(j, expandingPosition.column, wildSymbolId);
        }
      });
    }
    // load pay arrays
    const payArray = game.default.gameConfig.payLines.normalizedPayArray;
    // result based on view zone is calculated below
    let results = [];
    return new Promise((resolve) => {
      Promise.all([
        super.computeWins(arrays, columnSize, payArray, game, results),
        super.freeSpinCompute(scatter, results, game),
        super.wildPayCompute(arrays, columnSize, payArray, game, results)
      ]).then(() => {
        results = super.refineResults(results);
        resolve({ viewZone: deepCopyArray, results, scatter, expanding });
      }).catch((err) => {
        console.log(err, 'err in cmpute');
        resolve({ error: 'error in view zone' });
      });
    });
  }


  /**
  * This function generates view zone base on random numbers
  * view zone is calculated to be shown to user
  */
  generateViewZone(randomNumber, noofReels, columnSize, expanding, scatter, game, arrays, session) {
    let reelConfig = game.default.gameConfig.reelConfig;
    if (!_.isEmpty(session.eventData.freeSpinData) && (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0)) {
      reelConfig = game.default.gameConfig.freeSpin.reelConfig;
    }
    for (let i = 0; i < noofReels; i += 1) {
      for (let j = 0; j < columnSize; j += 1) {
        const symbolId = super.getSymbol(j, i, reelConfig, randomNumber);
        if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Wild' && game.default.gameConfig.symbols[symbolId].isExpandingWild == true) {
          expanding.push({ column: i, row: j });
        }
        if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Scatter') {
          scatter.push(i);
        }
        arrays.set(j, i, symbolId);
      }
    }
  }


  /**
  * This function is responsible to generate spin response
  * it includes view zone, wins, user details like wallet, bet array etc
  * necessary render view for the user
  */
  generateSpinResponse(obj, computeData, token, sessionRequestKey, sessionRequest, session) {
    // determine view port
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));
    // create reels for view zone
    const array = computeData.viewZone;
    const viewZone = {};
    for (let i = 0; i < noofReels; i += 1) {
      const line = [];
      for (let j = 0; j < columnSize; j += 1) {
        line.push({ symbolId: array.get(j, i), symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType, symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName });
      }
      viewZone[`Reel${i + 1}`] = line;
    }
    viewZone.viewZone = obj.game.default.gameConfig.viewZone;
    // create win document
    const result = {};
    result.viewZone = viewZone;
    if (computeData.expanding.length) {
      result.expandingWild = computeData.expanding;
    }
    result.wins = {};
    result.freeSpinData = {};
    result.levelData = {};
    result.noofReels = noofReels;
    let isFsWinAdded = false;
    return new Promise((resolve) => {
      // if free spin is present
      if (computeData.results.freeSpins) {
        if (!_.isEmpty(session)) {
          if (_.isEmpty(session.eventData.freeSpinData)) {
            result.freeSpinData.freeSpins = computeData.results.freeSpins;
            result.freeSpinData.multiplier = parseInt(obj.game.default.gameConfig.freeSpin.freeSpinWins[computeData.scatter.length].winMultiplier) || 1;
            result.freeSpinData.currentFreeSpin = 0;
            result.freeSpinData.winAmount = 0;
            session.eventData.freeSpinData = JSON.parse(JSON.stringify(result.freeSpinData));
          } else {
            result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
            session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
            session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
            computeData.results.forEach((winLineResult) => {
              session.eventData.freeSpinData.winAmount += (winLineResult.multiplier * (session.eventData.currentBet) * session.eventData.freeSpinData.multiplier);
            });
            isFsWinAdded = true;
          }

          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;

          if (session.eventData.freeSpinData.isFreeSpinAwarded) {
            delete session.eventData.freeSpinData.isFreeSpinAwarded;
            delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
          }
          session.event = 'spin';
          session._updatedTimestamp = moment.utc();
          super.updateSession(session, obj._id);
        }
        result.isSessionUpdated = true;
      }

      // total win amount calculated based on wins from pay line
      let total = 0;
      computeData.results.forEach((resultObj) => {
        resultObj.winAmount = resultObj.multiplier * (session.eventData.currentBet);
        total += resultObj.winAmount;
        delete resultObj.multiplier;
      });
      result.wins.winCoins = total;
      result.wins.winLinesCount = computeData.results.length;
      result.wins.winLines = [];

      computeData.results.forEach(row => result.wins.winLines.push(row));

      // put user session data
      result.session = {};
      result.session.sessionRequestKey = sessionRequestKey;
      result.session.sessionRequest = sessionRequest;
      /**
       * notify of all the symbols present in view zone
       */
      gameEvents.notifyGameEvents(obj, token, session, result, computeData.results.freeSpins);
      // mask view zone here
      result.viewZone = super.maskViewZone(result.viewZone, obj, _.isEmpty(session.eventData.freeSpinData));
      if (_.isEmpty(session.eventData.freeSpinData)) {
        resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
        if (session.eventData.freeSpinData.currentFreeSpin !== 0) {
          total *= session.eventData.freeSpinData.multiplier;
          result.wins.winCoins = total;
        }
        // some free spins remaining
        result.freeSpinData = session.eventData.freeSpinData;
        if (computeData.results.freeSpins) {
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
        }
        if (!computeData.results.freeSpins) {
          result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
        }
        if (result.wins.winCoins > 0) {
          if (!isFsWinAdded) {
            result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
          }
          session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
        } else {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
        }
        // call whow api
        if (session.eventData.freeSpinData.currentFreeSpin !== 0) {
          resolve(super.callWhowBluehare(token, result, computeData, 0, 10, total, session, sessionRequestKey, sessionRequest, obj._id));
        } else {
          resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
        }
      } else {
        // free spin object exists and there are no free spins remaining
        resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      }
    });
  }
}

module.exports = new Zeus();
