const { Compute } = require('../compute');
const nj = require('numjs');
const rng = require('../rng');
const _ = require('lodash');
const moment = require('moment');
const gameSession = require('../gamesession');
const logger = require('../../helpers/firehose');
const Game = require('../../helpers/gamefunction');
const constants = require('../../response/constants');
const whowapi = require('../../api/whowapi');
const { bucket } = require('../../helpers/couchbase');
const uuid = require('uuid');
const shuffle = require('shuffle-array');
const gameEvents = require('../../helpers/game-events');

/**
 * Special features related to zombie apocalypse is implemented in this class
 * This class handles main game computation and logic for expanding feature during free spin
 */
class ZombieApocalypse extends Compute {

  /**
   * [Handles game click event for zombie apocalypse]
   * @param  gameId   zombie apocalypse game id
   * @param  token    user token
   * @param  clientID client id generated on the client side
   * @param  data     user related data fetched from whow
   * @param  gameData game configuratin related data
   */
  handleGameClick(gameId, token, clientID, data, gameData) {
    bucket.get(`${data.userDetails.user.id}::${gameId}`, (err, result) => {
      if (!err) {
        if (result == null) {
          gameSession.createGameSessionObject(gameId, clientID, data, gameData);
        } else {
          result.value.clientID = clientID;
          gameSession.buildUserSessionObject(result, data, gameData);
          this.updateSession(result.value, gameId);
        }
      } else {
        gameSession.createGameSessionObject(gameId, clientID, data, gameData);
      }
    });
    gameSession.createUserDataAndSessionRequest(data, gameId, token);
  }


  /**
  * This function is responsible to calculate result for a spin event
  * taking into account
  * view zone, pay lines, pay table
  */
  compute(game, gameId, token, session) {
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));

    // start calculating view zone
    let arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);

    // random numbers are fetched from reel configuration
    const randomNumber = rng.generateRandomNumber(game.default.gameConfig.reelConfig);

    // view zone is calculated below
    const expanding = [];
    const scatter = [];
    super.generateViewZone(randomNumber, noofReels, columnSize, expanding, scatter, game, arrays, session);

    // if expanding wild is detected complete reel is replaced with non expaning symbols each
    let deepCopyArray = arrays.clone();

    // load pay arrays
    let payArray = game.default.gameConfig.payLines.normalizedPayArray;
    const copyPayArray = JSON.parse(JSON.stringify(payArray));

    // result based on view zone is calculated below
    const results = [];

    return new Promise((resolve, reject) => {
      const currentLines = session.eventData.currentLines;
      const lineArrays = game.default.gameConfig.selectablePaylines[currentLines];
      payArray.forEach((pay) => {
        const object = _.find(lineArrays, o => o === pay._id);
        if (!object) {
          _.remove(copyPayArray, arr => arr._id === pay._id);
        }
      });
      payArray = copyPayArray;
      arrays = deepCopyArray.clone();

      // check if expanding positions is already there in session
      if (session.eventData.expandingPositions && session.eventData.expandingPositions.length > 0) {
        const expandingPositions = session.eventData.expandingPositions;
        deepCopyArray = this.expandReelFromExpandingPosition(game, expandingPositions, columnSize, arrays);
        arrays = deepCopyArray;
      }

      const scatterCount = this.determineScatterCount(game, arrays, columnSize, noofReels);

      // check if user wins a bonus game
      const bonusSymbolId = _.findKey(game.default.gameConfig.symbols, { SymbolType: 'Bonus' });
      const isBonusWon = this.checkIfBonusWon(game, arrays, columnSize, noofReels, bonusSymbolId);

      // expand reel if wild is present
      const data = this.getReeslIfFrozenWild(game, arrays, columnSize, noofReels, session.eventData.expandingPositions);
      if (data[2] == true) {
        // reels expanded
        arrays = data[0];
      }
      this.freeSpinCompute(scatterCount, results, game)
      .then((results) => {
        if (results.freeSpins) {
          // free spins are awarded
          if (_.isEmpty(session.eventData.freeSpinData)) {
            // there is no active free spin that is going on
            resolve(this.computeForNoActiveFreeSpins(game, arrays, deepCopyArray, results, payArray, columnSize, data[1], isBonusWon));
          } else {
            // there are active free spins and free spin is awarded

            resolve(this.computeForFreeSpin(game, session, arrays, deepCopyArray, results, payArray, noofReels, columnSize, data[1], isBonusWon));
          }
        } else if (!_.isEmpty(session.eventData.freeSpinData)) {
          // free spins are active but free spin is not awarded

          resolve(this.computeForFreeSpin(game, session, arrays, deepCopyArray, results, payArray, noofReels, columnSize, data[1], isBonusWon));
        } else {
          // free spin is not active and free spins are not awarded
          // normal proceedings

          resolve(this.computeForNoActiveFreeSpins(game, arrays, deepCopyArray, results, payArray, columnSize, data[1], isBonusWon));
        }
      });
    });
  }


  /**
   * Checks if wild is present in a reel
   * if wild is present the reel is expanded
   */
  getReeslIfFrozenWild(game, arrays, columnSize, noofReels, expandingPosition) {
    const ignoreValues = [];
    if (expandingPosition && expandingPosition.length > 0) {
      expandingPosition.forEach((position) => {
        ignoreValues.push(position.i);
      });
    }
    const copyArray = arrays.clone();
    const expandingPositions = [];
    for (let i = 0; i < noofReels; i += 1) {
      if (ignoreValues.indexOf(i) > -1) {
 continue;
}
      for (let j = 0; j < columnSize; j += 1) {
        if (game.default.gameConfig.symbols[copyArray.get(j, i)].SymbolType == 'Wild') {
          for (let k = 0; k < columnSize; k += 1) {
            copyArray.set(k, i, copyArray.get(j, i));
          }
          expandingPositions.push({ i, j });
          break;
        }
      }
    }

    if (expandingPositions.length > 0) {
      return [copyArray, expandingPositions, true];
    }
    return [arrays, expandingPositions, false];
  }


  /**
   * This function expands frozen wild symbols in reel
   * wilds are expanded based on expanding position provided to it
   * expanding positions are stored in user session
   */
  expandReelFromExpandingPosition(game, expandingPositions, columnSize, arrays) {
    const copyArray = arrays.clone();
    const wildSymbolId = _.findKey(game.default.gameConfig.symbols, { SymbolType: 'Wild' });
    expandingPositions.forEach((position) => {
      const i = position.i;
      for (let k = 0; k < columnSize; k += 1) {
        copyArray.set(k, i, wildSymbolId);
      }
    });
    return copyArray;
  }


  /**
  * This function is responsible to generate spin response
  * it includes view zone, wins, user details like wallet, bet array etc
  * necessary render view for the user
  */
  generateSpinResponse(obj, computeData, token, sessionRequestKey, sessionRequest, session) {
    // determine view port
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));

    // create reels for view zone
    const array = computeData.viewZone;
    const viewZone = {};

    for (let i = 0; i < noofReels; i++) {
      const line = [];
      for (let j = 0; j < columnSize; j++) {
        line.push({ symbolId: array.get(j, i), symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType, symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName });
      }
      viewZone[`Reel${i + 1}`] = line;
    }
    viewZone.viewZone = obj.game.default.gameConfig.viewZone;

    // create win document
    const result = {};
    result.viewZone = viewZone;
    result.wins = {};
    result.freeSpinData = {}, result.levelData = {};

    // develop bonus object
    if (computeData.isBonusWon) {
      result.bonus = this.initiateBonus(obj.game, null);
    }
    return new Promise((resolve, reject) => {
        // if free spin is present
      if (computeData.results.freeSpins) {
        if (!_.isEmpty(session)) {
          if (_.isEmpty(session.eventData.freeSpinData)) {
            result.freeSpinData.freeSpins = computeData.results.freeSpins;
            result.freeSpinData.pickedSymbol = computeData.pickedSymbol;
            result.freeSpinData.currentFreeSpin = 0;
            result.freeSpinData.winAmount = 0;

            session.eventData.freeSpinData = result.freeSpinData;
          } else {
            result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;

            session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
            session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
          }
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
          if (session.eventData.freeSpinData.isFreeSpinAwarded) {
            delete session.eventData.freeSpinData.isFreeSpinAwarded;
            delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
          }
          session.event = 'spin';
          session._updatedTimestamp = moment.utc();


          // if bonus is won construct bonus object for user session
          if (computeData.isBonusWon) {
            session.eventData.bonus = result.bonus;
          }
          super.updateSessionAndSessionRequest(session, obj._id, sessionRequestKey, sessionRequest);
        }
        result.isSessionUpdated = true;
      }

      // update for expanding symbols if they are expanding
      if (computeData.expandingPositions) {
        result.expandData = {};
        const expandingPositions = computeData.expandingPositions;
        expandingPositions.forEach((position) => {
          const reelNumber = position.i + 1;
          const columnNumber = position.j;

          result.expandData[`reel${reelNumber}`] = { columnNumber, isExpanding: true };
          result.expandingPositions = computeData.expandingPositions;
        });
      }
        // total win amount calculated based on wins from pay line
      let total = 0;
      computeData.results.forEach((result) => {
        result.winAmount = result.multiplier * session.eventData.currentBet;

        total += result.winAmount;
        delete result.multiplier;
      });


      if (!_.isEmpty(session.eventData.freeSpinData) && session.eventData.freeSpinData.currentFreeSpin > 0) {
        total *= (session.eventData.freeSpinData.currentFreeSpin + 1);
      }


      result.wins.winCoins = total;
      result.wins.winLinesCount = computeData.results.length;
      result.wins.winLines = JSON.parse(JSON.stringify(computeData.results));

      if (result.wins.winLines.freeSpins) {
        delete result.wins.winLines.freeSpins;
      }

        // put user session data
      result.session = {};
      result.session.sessionRequestKey = sessionRequestKey;
      result.session.sessionRequest = sessionRequest;
      /**
       * notify of all the symbols present in view zone
       */
      gameEvents.notifyGameEvents(obj, token, session, result, computeData.results.freeSpins);

      // mask view zone here
      result.viewZone = super.maskViewZone(result.viewZone, obj);
      if (_.isEmpty(session.eventData.freeSpinData)) {
          // no free spins
        resolve(this.callWhow(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
          // some free spins remaining
        result.freeSpinData = session.eventData.freeSpinData;

        if (computeData.results.freeSpins) {
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
        }

        if (!computeData.results.freeSpins) {
          result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
        }

        if (result.wins.winCoins > 0) {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
          session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
        } else {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
        }
          // call whow api
        if (session.eventData.freeSpinData.currentFreeSpin === 0) {
          // free spins just awarded
          result.isSessionUpdated = false;
          session.eventData.freeSpinData.winAmount = 0;
          resolve(this.callWhowForFirstFreeSpin(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
        } else {
          if (computeData.results.freeSpins && !_.isEmpty(session.eventData.freeSpinData)) {
            result.isSessionUpdated = false;
          }
          resolve(this.callBet(token, 0, total, 10, result, session, sessionRequestKey, sessionRequest, obj._id));
        }
      } else {
          // free spin object exists and there are no free spins remaining
        resolve(this.callWhow(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      }
    });
  }


  /**
   * This function computes various data points
   * It checks for wins after frozen wild has expanded
   * and computes wins for scatter and wild symbols if they pay
   */
  computeForFreeSpin(game, session, arrays, deepCopyArray, results, payArray, noofReels, columnSize, expandingPositions, isBonusWon) {
    return new Promise((resolve, reject) => {
      Promise.all(
        [
          super.computeWins(arrays, columnSize, payArray, game, results),
          super.scatterPayCompute(arrays, columnSize, payArray, game, results),
          super.wildPayCompute(arrays, columnSize, payArray, game, results)
        ]).then((values) => {
          if (expandingPositions.length > 0) {
            resolve({ viewZone: deepCopyArray, results, expandingPositions, isBonusWon });
          } else {
            resolve({ viewZone: deepCopyArray, results, isBonusWon });
          }
        }).catch((err) => {
          resolve({ error: 'error in view zone' });
        });
    });
  }


  /**
   * This function computes results when free spins are not active
   * Wild and scatter symbols are considered when computing
   */
  computeForNoActiveFreeSpins(game, arrays, deepCopyArray, results, payArray, columnSize, expandingPositions, isBonusWon) {
    return new Promise((resolve, reject) => {
      Promise.all(
        [
          super.computeWins(arrays, columnSize, payArray, game, results),
          super.scatterPayCompute(arrays, columnSize, payArray, game, results),
          super.wildPayCompute(arrays, columnSize, payArray, game, results)
        ]).then((values) => {
          if (expandingPositions.length > 0) {
            resolve({ viewZone: deepCopyArray, results, expandingPositions, isBonusWon });
          } else {
            resolve({ viewZone: deepCopyArray, results, isBonusWon });
          }
        }).catch((err) => {
          resolve({ error: 'error in view zone' });
        });
    });
  }


  /**
  * This function checks if there are any free spins to be awarded based on view zone
  * If there are free spins than the number of free spins are determined based on game configuration
  */
  freeSpinCompute(scatter, results, game) {
    return new Promise((resolve, reject) => {
      if (scatter >= game.default.gameConfig.freeSpin.minimumRequiredCount) {
        if (!game.default.gameConfig.freeSpin.freeSpinWins[scatter]) {
          results.freeSpins = game.default.gameConfig.freeSpin.freeSpinWins[game.default.gameConfig.freeSpin.keys[game.default.gameConfig.freeSpin.keys.length - 1]].noofFS;
        } else {
          results.freeSpins = game.default.gameConfig.freeSpin.freeSpinWins[scatter].noofFS;
        }
      }
      resolve(results);
    });
  }

  /**
  * This function models the spin result document based on the analytics requirement
  * it deletes the fields not required and fetches the field required from database
  * logging for secret of amun is customized
  */
  logSpinResult(result, token, gameId) {
    if (result && token && gameId) {
      delete result.levelData;
      delete result.game;
      delete result.jackpot;
      delete result.session;
      delete result.viewZone;
      result.gameId = gameId;
      const gameData = Game.getGame(gameId);
      result.baseGameId = gameData.game.default.baseGameId;
      result.gameVersion = gameData.game.default.versionName;
      result.stage = gameData.game.default.stage;

      /**
      * removes un necessary data from while logging spin results
      */
      const symbols = {};
      const types = {};
      if (result.wins.winLines.length > 0) {
        result.wins.winLines.forEach((line) => {
          if (!types[line.winType]) {
            types[line.winType] = 0;
          }
          if (!symbols[line.symbolId]) {
            symbols[line.symbolId] = 0;
          }
          types[line.winType]++;
          symbols[line.symbolId]++;
        });
        result.wins.types = types;
        result.wins.symbols = symbols;
      }
      if (result.wins.expandedWins && result.wins.expandedWins.expandedWinLines && result.wins.expandedWins.expandedWinLines.length > 0) {
        result.wins.expandedWins.expandedWinLines.forEach((line) => {
          if (!types[line.winType]) {
            types[line.winType] = 0;
          }
          if (!symbols[line.symbolId]) {
            symbols[line.symbolId] = 0;
          }
          types[line.winType]++;
          symbols[line.symbolId]++;
        });
        result.wins.types = types;
        result.wins.symbols = symbols;
      }
      gameSession.getUserSession(token, gameId)
      .then((session) => {
        result.user = {};
        result.user.currentBet = session.eventData.currentBet;
        result.user.currentLines = session.eventData.currentLines;
        result.user.totalBet = session.eventData.currentBet * session.eventData.currentLines;
        result.user.token = token;
        result.user.userId = session.eventData.userDetails.id;

        if (result.expandData) {
          delete result.expandData;
        }
        if (result.wins.expandedWins) {
          delete result.wins.expandedWins;
        }

        logger.info(result);
      }).catch((err) => {
        console.log(err);
      });
    }
  }


  /**
  * updates user game session with details of a spin result
  * it may include free spin if won in game spin
  * and updated wallet chips as a result of whow api call bet/play
  */
  gameSpin(gameId, token, docDetails) {
    docDetails._updatedTimestamp = moment.utc();
    gameSession.getUserSession(token, gameId)
        .then((session) => {
          session.game = docDetails.game;
          session.eventData.userDetails.wallet = docDetails.wallet;
          session.gamble = docDetails.gamble;
          if (docDetails.expandingPositions) {
            session.eventData.expandingPositions = docDetails.expandingPositions;
          } else {
            session.eventData.expandingPositions = [];
          }

          if (!_.isEmpty(session)) {
            session.event = 'spin';
            if (!_.isEmpty(docDetails.freeSpinData) && docDetails.freeSpinData.isFreeSpinAwarded) {
              delete docDetails.freeSpinData.isFreeSpinAwarded;
              delete docDetails.freeSpinData.noofFreeSpinsAwarded;
            }
            session.eventData.freeSpinData = docDetails.freeSpinData;
            if (!_.isEmpty(session.eventData.freeSpinData) && (session.eventData.freeSpinData.currentFreeSpin == session.eventData.freeSpinData.freeSpins)) {
              const totalWin = session.eventData.freeSpinData.winAmount;
              session.gamble.winAmount = totalWin;
              session.gamble.initialWin = totalWin;
              session.gamble.wasFreeSpin = true;
              session.eventData.freeSpinData = {};
            } else if (session.gamble) {
              session.gamble.wasFreeSpin = false;
            }
          }

          const sessionRequestKey = docDetails.session.sessionRequestKey;
          const sessionRequest = docDetails.session.sessionRequest;
          sessionRequest.status = 'ready',
          sessionRequest.spinCompletedTimestamp = moment().utc();

          // if bonus is won construct bonus object for user session
          if (docDetails.bonus) {
            session.eventData.bonus = docDetails.bonus;
          }
          this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);
          this.updateSession(session, gameId);
        }).catch((err) => {
          console.log(err);
        });
  }


  /**
  * updates user's session with free spin data, wallet chips etc.
  */
  updateSession(session, gameId) {
    bucket.upsert(`${session.eventData.userDetails.id}::${gameId}`, session, (err, result) => {
      if (err) {
        console.log(err);
      }
    });
  }


  /**
  * updates user's session and session requests
  * created when game click took place
  */
  updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest) {
    bucket.upsert(`${session.eventData.userDetails.id}::${gameId}`, session, (err, result) => {
      if (!err) {
        this.updateSessionRequestDoc(sessionRequestKey, sessionRequest);
      }
    });
  }


  /**
  * updates user's session request document with provided document
  * no optimistic database concurreny is used in this update
  */
  updateSessionRequestDoc(sessionRequestKey, sessionRequestDoc) {
    bucket.upsert(sessionRequestKey, sessionRequestDoc, (err, result) => {
      if (err) {
        console.log(err);
      }
    });
  }


  /**
   * This function hadles the gamble request for secret of amun
   * Validations related gamble is done and gamble winings are determined
   */
  handleGambleRequest(token, gameId, clientId, pickedCard) {
    return new Promise((resolve, reject) => {
      if (!pickedCard && pickedCard != null && pickedCard.toLowerCase() !== constants.GAMBLE.RED && pickedCard.toLowerCase() !== constants.GAMBLE.BLACK) {
        resolve({ error: constants.RES_MESSAGES.INVALID_GAMBLE_CARD, errorCode: constants.RES_ERROR_CODES.INVALID_GAMBLE_CARD });
      } else {
        /**
        * Step 1: Check if free spin is not active
        */
        gameSession.getUserSession(token, gameId)
        .then((session) => {
          if (session.clientID === clientId) {
            if (!_.isEmpty(session.eventData.freeSpinData)) {
              // free spin is active
              resolve({ error: constants.RES_MESSAGES.GAMBLE_NOT_ACTIVE, errorCode: constants.RES_ERROR_CODES.GAMBLE_NOT_ACTIVE });
            } else if (!session.gamble.winAmount > 0) {
              /**
              * Step 2: get the last win stored in session
              * if there is no last win, gamble cannot be availed
              */
              resolve({ error: constants.RES_MESSAGES.GAMBLE_NOT_ACTIVE, errorCode: constants.RES_ERROR_CODES.GAMBLE_NOT_ACTIVE });
            } else {
              const pickedNumber = Math.floor(Math.random() * constants.GAMBLE.SECRET_OF_AMUN_RANDOM_RANGE);
              let isRed = false;
              let card = constants.GAMBLE.BLACK;
              if ((pickedNumber >= 0 && pickedNumber <= 13) || (pickedNumber >= 27 && pickedNumber <= 39)) {
                isRed = true;
                card = constants.GAMBLE.RED;
              }
              /**
              * Step 3: User either wins or looses based on randomness in server
              * - If user wins, winning is doubled
              * - If user looses, he looses even the last win amount
              */
              session.gamble.history.push(card);
              if (session.gamble.history.length > 5) {
                session.gamble.history.shift();
              }

              if ((pickedCard.toLowerCase() === constants.GAMBLE.RED && isRed) || (pickedCard.toLowerCase() === constants.GAMBLE.BLACK && !isRed)) {
                // user wins
                session.gamble.winAmount = session.gamble.winAmount * 2;
                session.gamble.betAmount = session.gamble.winAmount;
                session.gamble.count += 1;
                // call bet whow api
                this.whowBetApi(session.gamble.id, token, 0, session.gamble.winAmount, session.gamble.betAmount, session)
                .then((response) => {
                  if (session.gamble.count === constants.GAMBLE.SECRET_OF_AMUN_MAX_GAMBLE) {
                    // max gabmle
                    this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
                    .then((data) => {
                      session.gamble.count = 0;
                      session.gamble.winAmount = 0;
                      this.updateSession(session, gameId);
                      resolve({ gambleCount: session.gamble.count, gambleWon: true, gambleTotalAmount: session.gamble.winAmount, gambleHistory: session.gamble.history, wallet: session.eventData.userDetails.wallet, isGambleAvailable: false });
                    });
                  } else {
                    this.updateSession(session, gameId);
                    resolve({ gambleCount: session.gamble.count, gambleWon: true, gambleTotalAmount: session.gamble.winAmount, gambleBetAmount: session.gamble.betAmount, gamblePotentialWinAmount: session.gamble.betAmount * 2, gambleHistory: session.gamble.history, wallet: session.eventData.userDetails.wallet, isGambleAvailable: true });
                  }
                });
              } else {
                // user looses
                // call close whow api
                this.whowCloseApi(session.gamble.id, token, 0, session)
                .then((data) => {
                  session.gamble.count = 0;
                  session.gamble.winAmount = 0;
                  this.updateSession(session, gameId);
                  resolve({ gambleCount: session.gamble.count, gambleWon: false, gambleTotalAmount: session.gamble.winAmount, gambleHistory: session.gamble.history, wallet: session.eventData.userDetails.wallet, isGambleAvailable: false });
                });
              }
            }
          } else {
            resolve({ error: constants.RES_MESSAGES.INVALID_CLIENT, errorCode: constants.RES_ERROR_CODES.INVALID_CLIENT });
          }
        })
        .catch((err) => {
          console.log(err);
          resolve({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
        });
      }
    });
  }


  /**
   * Whow bet api is called in case user wins
   */
  whowBetApi(roundId, token, betAmount, total, virtualAmount, session) {
    return new Promise((resolve, reject) => {
      whowapi.bet(token, {
        roundId,
        betAmount,
        winAmount: total,
        virtualAmount
      }).then((response) => {
        response = JSON.parse(response);
        session.eventData.userDetails.wallet = response.payload.user.wallet;
        session.game = response.payload.game;
        resolve(response);
      }).catch((err) => {
        resolve(err);
      });
    });
  }


  /**
   * Whow close api is called in case when user looses
   * when user looses previous round is close
   */
  whowCloseApi(roundId, token, total, session) {
    return new Promise((resolve, reject) => {
      whowapi.close(token, {
        winAmount: total,
        roundId
      })
      .then((response) => {
        response = JSON.parse(response);
        session.eventData.userDetails.wallet = response.payload.user.wallet;
        session.game = response.payload.game;

        resolve(response);
      })
      .catch((err) => {
        console.log(err);
        resolve(err);
      });
    });
  }


  /**
   * This function closes previous active round and calls a new bet api
   * This function is used when user wins in main game
   */
  callWhowBet(token, result, computeData, betAmount, virtualAmount, total, session) {
    return new Promise((resolve, reject) => {
      this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
      .then((response) => {
        resolve(this.callBet(token, betAmount, total, virtualAmount, result, session));
      });
    });
  }


  /**
   * This function is overriden from the super class
   * Whow api is called at the end of each spin
   * informing the wins to whow
   */
  callWhow(token, result, computeData, betAmount, virtualAmount, total, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve, reject) => {
      if (computeData.results.length > 0) {
        if (session.gamble && session.gamble.id) {
          this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
          .then((response) => {
            resolve(this.callBet(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
          });
        } else {
          resolve(this.callBet(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
        }
      } else if (session.gamble && session.gamble.id) {
        this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
        .then((response) => {
          resolve(this.callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
        });
      } else {
        resolve(this.callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
      }
    });
  }


  /**
   * This function is called when the first free spin is played
   * Purpose of this function is to close any winnings that was awarded before free spin was triggered
   */
  callWhowForFirstFreeSpin(token, result, computeData, betAmount, virtualAmount, total, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve, reject) => {
      if (computeData.results.length > 0) {
        this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
        .then((data) => {
          session.gamble.winAmount = 0;
          this.callBet(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId)
          .then((results) => {
            whowapi.close(token, {
              winAmount: total,
              roundId: results.gamble.id
            });
            resolve(results);
          });
        });
      } else {
        resolve(this.callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
      }
    });
  }


  /**
   * This is the actual whow api call function
   * bet api is called when user wins
   */
  callBet(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve, reject) => {
      whowapi.bet(token, {
        betAmount,
        winAmount: total,
        virtualAmount
      }).then((response) => {
        response = JSON.parse(response);

        if (response.status && response.status === 110) {
          sessionRequest.status = 'ready';
          session.gamble.winAmount = 0;
          session.gamble.count = 0;
          this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);
          resolve({ error: constants.RES_MESSAGES.NOT_ENOUGH_BALANCE, errorCode: constants.RES_ERROR_CODES.NOT_ENOUGH_BALANCE });
        } else {
          result.gamble = response.payload.round;
          if (session.gamble === undefined || session.gamble === 'undefined' || session.gamble.history === undefined || session.gamble.history === 'undefined') {
            result.gamble.history = [];
          } else {
            result.gamble.history = session.gamble.history;
          }
          result.gamble.winAmount = total;
          result.gamble.count = 0;
          result.levelData.level = response.payload.user.level;
          result.levelData.levelProgress = response.payload.user.levelProgress;
          result.wallet = response.payload.user.wallet;
          result.game = response.payload.game;
          result.game.betAmount = (session.eventData.currentBet * session.eventData.currentLines);
          result.jackpot = response.payload.user.jackpotData;
          resolve(result);
        }
      }).catch((err) => {
        console.log(err);
        resolve(result);
      });
    });
  }


  /**
   * This is the actual whow api call function
   * play api is called when user does not win
   */
  callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve, reject) => {
      whowapi.play(token, {
        betAmount,
        winAmount: total,
        virtualAmount
      }).then((response) => {
        response = JSON.parse(response);

        if (response.status && response.status === 110) {
          sessionRequest.status = 'ready';
          if (session.gamble && !_.isEmpty(session.gamble)) {
            session.gamble.winAmount = 0;
            session.gamble.count = 0;
          }
          this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);

          resolve({ error: constants.RES_MESSAGES.NOT_ENOUGH_BALANCE, errorCode: constants.RES_ERROR_CODES.NOT_ENOUGH_BALANCE });
        } else {
          result.gamble = response.payload.round;
          if (session.gamble === undefined || session.gamble === 'undefined' || session.gamble.history === undefined || session.gamble.history === 'undefined') {
            result.gamble.history = [];
          } else {
            result.gamble.history = session.gamble.history;
          }
          result.gamble.count = 0;
          result.levelData.level = response.payload.user.level;
          result.levelData.levelProgress = response.payload.user.levelProgress;
          result.wallet = response.payload.user.wallet;
          result.game = response.payload.game;
          result.game.betAmount = (session.eventData.currentBet * session.eventData.currentLines);
          result.jackpot = response.payload.user.jackpotData;
          resolve(result);
        }
      }).catch((err) => {
        console.log(err);
        resolve(result);
      });
    });
  }


  /**
   * This function collects the win amount of current spin
   * and also the current win amount when gamble is active
   */
  pickGamble(token, gameId, clientId) {
    return new Promise((resolve, reject) => {
      gameSession.getUserSession(token, gameId)
      .then((session) => {
        if (session.clientID === clientId) {
          if (!_.isEmpty(session.eventData.freeSpinData)) {
            resolve({ error: constants.RES_MESSAGES.GAMBLE_NOT_ACTIVE, errorCode: constants.RES_ERROR_CODES.GAMBLE_NOT_ACTIVE });
          } else if (!session.gamble.winAmount > 0) {
            resolve({ error: constants.RES_MESSAGES.NO_WINS_TO_COLLECT, errorCode: constants.RES_ERROR_CODES.NO_WINS_TO_COLLECT });
          } else {
            this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
            .then((data) => {
              if (session.gamble.count > 0) {
                session.gamble.count = 0;
                session.gamble.winAmount = 0;
                this.updateSession(session, gameId);
                resolve({ gambleCount: session.gamble.count, gambleWon: true, gambleTotalAmount: session.gamble.winAmount, gambleHistory: session.gamble.history, wallet: session.eventData.userDetails.wallet, isGambleAvailable: false });
              } else {
                const totalWin = session.gamble.winAmount;
                session.gamble.winAmount = 0;
                this.updateSession(session, gameId);
                resolve({ collectAmount: totalWin, wallet: session.eventData.userDetails.wallet });
              }
            });
          }
        } else {
          resolve({ error: constants.RES_MESSAGES.INVALID_CLIENT, errorCode: constants.RES_ERROR_CODES.INVALID_CLIENT });
        }
      }).catch((err) => {
        console.log(err);
        resolve({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
      });
    });
  }

  /**
   * This function determines if user has won bonus game
   * Bonus symbols in view zone is determined and checked with game configuration
   */
  checkIfBonusWon(game, arrays, columnSize, noofReels, bonusSymbolId) {
    let bonusCount = 0;
    let bonusDetected = false;
    for (let i = 0; i < noofReels; i += 1) {
      for (let j = 0; j < columnSize; j += 1) {
        if (game.default.gameConfig.symbols[arrays.get(j, i)].SymbolType == 'Bonus') {
          bonusCount += 1;
          bonusDetected = true;
          break;
        } else {
          bonusDetected = false;
        }
      }
      if (bonusDetected) {
        continue;
      }
    }
    if (game.default.gameConfig.bonus && game.default.gameConfig.bonus !== undefined && game.default.gameConfig.bonus !== 'undefined') {
      if (bonusCount >= game.default.gameConfig.bonus.bonusTriggerCount) {
        return true;
      }
    }
    return false;
  }


  /**
   * This function builds bonus object for user session
   * If bonus is won, it constructs:
   * various zombies and some having maps
   * and orders zombies in random order
   */
  initiateBonus(game, activeStage) {
    const bonus = {};
    bonus.isActive = true;
    bonus.activeStage = activeStage || 0;
    bonus.totalStage = game.default.gameConfig.bonus.stages.length;

    // stage configuration data
    bonus.stage = {};
    bonus.stage.stageName = game.default.gameConfig.bonus.stages[bonus.activeStage].stageName;
    bonus.stage.noofMaps = game.default.gameConfig.bonus.stages[bonus.activeStage].noofMaps;
    bonus.stage.noofPicks = game.default.gameConfig.bonus.stages[bonus.activeStage].noofPicks;
    bonus.stage.noofZombies = game.default.gameConfig.bonus.stages[bonus.activeStage].noofZombies;
    bonus.stage.currentPick = 0;

    bonus.stage.currentStage = {};

    // build zombies
    const zombies = [];
    for (let i = 0; i < bonus.stage.noofZombies; i += 1) {
      const zombieId = uuid();
      zombies.push(zombieId);
      bonus.stage.currentStage[zombieId] = { isOpened: false, hasMap: false, prizeWon: 0 };
    }

    // build maps
    for (let i = 0; i < bonus.stage.noofMaps; i++) {
      bonus.stage.currentStage[zombies[i]].hasMap = true;
    }

    // build order
    const shuffledZombies = shuffle(zombies);
    bonus.stage.shuffledZombies = shuffledZombies;
    return bonus;
  }


  /**
   * Filters bonus object for client side response
   */
  filterBonusResponse(result) {
    result.bonus.activeStage = result.bonus.activeStage + 1;
    delete result.bonus.isActive;
    delete result.bonus.stage.currentStage;
    delete result.bonus.stage.noofMaps;
    return result;
  }


/**
 * Handles bonus request from client
 * it checks if bonus is active and makes decision accordingly
 * bonus outcomes are determined by it
 */
  handleBonusRequest(token, gameId, zombieId) {
    return new Promise((resolve, reject) => {
      gameSession.getUserSessionAndSessionRequest(token, gameId)
      .then((sessionCas) => {
        if (sessionCas.sessionRequestKey && sessionCas.sessionKey) {
          const sessionRequest = sessionCas[sessionCas.sessionRequestKey].value;
          if (sessionRequest.bonusPlay === 'init' || sessionRequest.bonusPlay === 'ready') {
            const session = sessionCas[sessionCas.sessionKey].value;
            if (!session.eventData.bonus || !session.eventData.bonus.isActive) {
              resolve({ error: constants.RES_MESSAGES.BONUS_NOT_ACTIVE });
            } else {
              this.updateSessionRequestDocForBonus(sessionCas.sessionRequestKey, sessionRequest, sessionCas[sessionCas.sessionRequestKey].cas)
              .then((cas) => {
                const result = {};
                const gameData = Game.getGame(gameId);
                const bonus = gameData.game.default.gameConfig.bonus;
                const zombie = session.eventData.bonus.stage.currentStage[zombieId];
                if (!zombie || zombie === null || zombie === undefined || zombie === 'undefined') {
                  this.updateSessionRequestDocForBonus(sessionCas.sessionRequestKey, sessionRequest, cas, 'ready');
                  resolve({ error: constants.RES_MESSAGES.INVALID_ZOMBIE_ID });
                } else if (zombie.isOpened) {
                  this.updateSessionRequestDocForBonus(sessionCas.sessionRequestKey, sessionRequest, cas, 'ready');
                  resolve({ error: constants.RES_MESSAGES.ZOMBIE_BONUS_ALREADY_OPEN });
                } else if (zombie.hasMap) {
                  // open the map enable next stage, no coins won
                  session.eventData.bonus.stage.wasMapOpened = true;

                  // reset all maps to coins
                  const zombieArray = Object.keys(session.eventData.bonus.stage.currentStage);
                  zombieArray.forEach((zombieObjectId) => {
                    if (zombieId !== zombieObjectId && session.eventData.bonus.stage.currentStage[zombieObjectId].hasMap === true) {
                      session.eventData.bonus.stage.currentStage[zombieObjectId].hasMap = false;
                    }
                  });

                  this.moveBonusToNextStage(session, zombieId, 0, gameData, token, 0, 0, false, gameId, sessionCas.sessionRequestKey, sessionCas[sessionCas.sessionRequestKey].value, cas)
                  .then((result) => {
                    result.isOpened = true;
                    result.winCoins = 0;
                    result.isMap = true;

                    resolve(result);
                  }).catch((err) => {
                    console.log(err);
                    resolve({ error: constants.RES_MESSAGES.BONUS_CANNOT_BE_PLAYED });
                  });
                } else {
                  // open zombie and calculate price
                  const multiplier = bonus.betPrizeArray[Math.floor(Math.random() * bonus.betPrizeArray.length)];
                  const winCoins = session.eventData.currentBet * multiplier;

                  this.moveBonusToNextStage(session, zombieId, winCoins, gameData, token, 0, 10, true, gameId, sessionCas.sessionRequestKey, sessionCas[sessionCas.sessionRequestKey].value, cas)
                  .then((result) => {
                    result.isOpened = true;
                    result.winCoins = winCoins;
                    resolve(result);
                  }).catch((err) => {
                    console.log(err);
                    resolve({ error: constants.RES_MESSAGES.BONUS_CANNOT_BE_PLAYED });
                  });
                }
              })
              .catch((err) => {
                console.log(err);
                resolve({ error: constants.RES_MESSAGES.BONUS_CANNOT_BE_PLAYED });
              });
            }
          } else {
            // not ready
            resolve({ error: constants.RES_MESSAGES.BONUS_CANNOT_BE_PLAYED });
          }
        } else {
          // no game click
          resolve({ error: constants.RES_MESSAGES.BONUS_NOT_ALLOWED_NO_GAME_CLICK });
        }
      })
      .catch((err) => {
        console.log(err);
        resolve({ error: err });
      });
    });
  }


  /**
   * upgrades bonus stage if map has been opened
   * it updates session and calls whow api to inform of the winnings
   */
  moveBonusToNextStage(session, zombieId, winCoins, game, token, betAmount, virtualAmount, callWhow, gameId, sessionRequestKey, sessionRequectDoc, cas) {
    const result = {};
    result.hasBonusEnded = false;
    session.eventData.bonus.stage.currentStage[zombieId].isOpened = true;
    session.eventData.bonus.stage.currentStage[zombieId].prizeWon = winCoins;

    // increase pick count
    session.eventData.bonus.stage.currentPick = session.eventData.bonus.stage.currentPick + 1;

    // check if new stage can be activated
    if (session.eventData.bonus.stage.currentPick === session.eventData.bonus.stage.noofPicks && session.eventData.bonus.stage.wasMapOpened) {
      session.eventData.bonus.activeStage = session.eventData.bonus.activeStage + 1;
      if (session.eventData.bonus.activeStage >= session.eventData.bonus.totalStage) {
        // end bonus play
        result.hasBonusEnded = true;
        result.activeStage = session.eventData.bonus.activeStage + 1;
        result.currentPick = session.eventData.bonus.stage.currentPick;
        result.noofPicks = session.eventData.bonus.stage.noofPicks;
        session.eventData.bonus = null;
      } else {
        // move bonus to next stage
        const bonus = this.initiateBonus(game.game, session.eventData.bonus.activeStage);

        session.eventData.bonus = bonus;
        result.hasMovedtoNextStage = true;
        result.bonus = JSON.parse(JSON.stringify(bonus));
        delete result.bonus.isActive;
        delete result.bonus.stage.currentStage;
        delete result.bonus.stage.noofMaps;
      }
    } else if (session.eventData.bonus.stage.currentPick === session.eventData.bonus.stage.noofPicks) {
      // end bonus play
      result.hasBonusEnded = true;
      result.activeStage = session.eventData.bonus.activeStage + 1;
      result.currentPick = session.eventData.bonus.stage.currentPick;
      result.noofPicks = session.eventData.bonus.stage.noofPicks;
      session.eventData.bonus = null;
    } else {
      // continue as normal bonus game
      result.hasBonusEnded = false;
      result.activeStage = session.eventData.bonus.activeStage + 1;
      result.currentPick = session.eventData.bonus.stage.currentPick;
      result.noofPicks = session.eventData.bonus.stage.noofPicks;
    }

    return new Promise((resolve, reject) => {
      this.updateSessionRequestDocForBonus(sessionRequestKey, sessionRequectDoc, cas, 'ready')
      .then((data) => {
        if (callWhow) {
          this.updateBonusWins(token, betAmount, winCoins, virtualAmount);
        }
        this.updateSession(session, gameId);
        resolve(result);
      })
      .catch((err) => {
        reject(err);
      });
    });
  }


  /**
   * Whow api call to update bonus winnings
   */
  updateBonusWins(token, betAmount, total, virtualAmount) {
    whowapi.bet(token, {
      betAmount,
      winAmount: total,
      virtualAmount
    }).then((response) => {
      response = JSON.parse(response);
      whowapi.close(token, {
        winAmount: total,
        roundId: response.payload.round.id
      });
    });
  }


  /**
  * This function updates user's session request doument allowing a spin to take place
  * game session helper is used to update session data
  */
  updateSessionRequestDocForBonus(sessionRequestKey, sessionRequestDoc, cas, bonusPlay) {
    sessionRequestDoc.bonusPlay = bonusPlay || 'playing';
    sessionRequestDoc.bonusIniTimestamp = moment().utc();
    return this.updateSessionRequestDocWithCas(sessionRequestKey, sessionRequestDoc, cas);
  }


  /**
  * This updates user's session request document with provided document
  * Optimistic concurreny is handled with couchbase cas document
  * to ensure parallel spins for same game for a user does not happen unless previous spin is not finished
  */
  updateSessionRequestDocWithCas(sessionRequestKey, sessionRequestDoc, cas) {
    return new Promise((resolve, reject) => {
      bucket.upsert(sessionRequestKey, sessionRequestDoc, { cas }, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result.cas);
        }
      });
    });
  }


  /**
  * This function handles game click event from AZ controller
  * WHOW API is called to get user data from token
  * relevant user data along with game sessions are created using compute helper
  */
  handleGameClickZA(gameId, token) {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await whowapi.get(token);
        const obj = Game.getGame(gameId);
        if (!obj) {
          resolve({ error: constants.RES_MESSAGES.GAME_NOT_FOUND, erroCode: constants.RES_ERROR_CODES.GAME_NOT_FOUND });
        } else if (response.status === 200) {
          obj.userDetails = response.payload;
          resolve(this.generateGameClickResponse(obj, token, gameId));
        } else {
          resolve({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
        }
      } catch (err) {
        console.log(err);
        reject({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
      }
    });
  }


  /**
  * This function is used to build response for game click event
  * game data along with user data for subsequent  game events calls are passed to client
  */
  generateGameClickResponse(data, token, gameId) {
    const result = {};
    result.game = {};
    result.game.gameId = data._id;
    result.game.viewZone = data.game.default.gameConfig.viewZone;
    result.game.reels = {};
    result.game.reels.noofReels = data.game.default.gameConfig.reelConfig.NumberOfReels;

    // do not give away reel configuration, mask initiail view
    const symbolIndices = Object.keys(data.game.default.gameConfig.symbols);
    for (let i = 0; i < data.game.default.gameConfig.reelConfig.NumberOfReels; i++) {
      const columnSize = parseInt(data.game.default.gameConfig.viewZone.toString().charAt(0));
      const reelArray = [];
      for (let j = 0; j < columnSize; j += 1) {
        reelArray.push(symbolIndices[Math.floor(Math.random() * symbolIndices.length)]);
      }
      result.game.reels[`Reel${i + 1}`] = reelArray;
    }
    result.game.payArray = data.game.default.gameConfig.payLines.PayArray;
    result.game.payTable = data.game.default.gameConfig.payTable;

    result.game.bigWins = data.game.default.gameConfig.bigWins;
    result.game.symbols = data.game.default.gameConfig.symbols;
    result.game.stage = data.game.default.gameConfig.stage;
    result.game.version = data.game.default.gameConfig.versionName;
    result.userDetails = data.userDetails;
    result.userDetails.selectablePaylines = data.game.default.gameConfig.selectablePaylines.options;
    // console.log(JSON.stringify(data.userDetails));
    result.userDetails.currentBet = data.userDetails.game.settings.bets[0] || 5,
    result.userDetails.currentLines = parseInt(data.game.default.gameConfig.selectablePaylines.options[data.game.default.gameConfig.selectablePaylines.options.length - 1]) || 20;
    result.userDetails.totalBet = (result.userDetails.currentBet * result.userDetails.currentLines);

    return new Promise(async (resolve, reject) => {
      try {
        const session = await gameSession.getUserSession(token, gameId);
        result.userDetails.currentBet = session.eventData.currentBet;
        result.userDetails.currentLines = session.eventData.currentLines;
        result.userDetails.freeSpinData = session.eventData.freeSpinData;
        result.userDetails.totalBet = (result.userDetails.currentBet * result.userDetails.currentLines);

        if (session.eventData.bonus && session.eventData.bonus != null) {
          result.bonus = {};
          result.bonus.activeStage = session.eventData.bonus.activeStage;
          result.bonus.totalStage = session.eventData.bonus.totalStage;
          result.bonus.stage = {};
          result.bonus.stage.stageName = session.eventData.bonus.stage.stageName;
          result.bonus.stage.noofPicks = session.eventData.bonus.stage.noofPicks;
          result.bonus.stage.noofZombies = session.eventData.bonus.stage.noofZombies;
          result.bonus.stage.currentPick = session.eventData.bonus.stage.currentPick;
          result.bonus.stage.shuffledZombies = session.eventData.bonus.stage.shuffledZombies;

          const currentStageKeys = Object.keys(session.eventData.bonus.stage.currentStage);
          const currentStage = {};
          currentStageKeys.forEach((stageKey) => {
            currentStage[stageKey] = {};
            currentStage[stageKey].isOpened = session.eventData.bonus.stage.currentStage[stageKey].isOpened;
            currentStage[stageKey].prizeWon = session.eventData.bonus.stage.currentStage[stageKey].prizeWon;
          });
          result.bonus.stage.currentStage = currentStage;
        }
        resolve(result);
      } catch (e) {
        result.userDetails.currentBet = data.userDetails.game.settings.bets[0] || 5,
        result.userDetails.currentLines = parseInt(data.game.default.gameConfig.selectablePaylines.options[data.game.default.gameConfig.selectablePaylines.options.length - 1]) || 20;
        result.userDetails.freeSpinData = {};
        result.userDetails.totalBet = (result.userDetails.currentBet * result.userDetails.currentLines);
        resolve(result);
      }
    });
  }


/**
 * Determines free spin symbols present in view zone
 */
  determineScatterCount(game, arrays, columnSize, noofReels) {
    let scatterCount = 0;
    for (let i = 0; i < noofReels; i += 1) {
      for (let j = 0; j < columnSize; j += 1) {
        if (game.default.gameConfig.symbols[arrays.get(j, i)].SymbolType === 'Scatter') {
          scatterCount += 1;
        }
      }
    }
    return scatterCount;
  }
}

module.exports = new ZombieApocalypse();
