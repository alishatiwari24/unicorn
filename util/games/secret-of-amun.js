/* eslint-disable require-await */
const { Compute } = require('../compute');
const nj = require('numjs');
const rng = require('../rng');
const _ = require('lodash');
const moment = require('moment');
const gameSession = require('../gamesession');
const logger = require('../../helpers/firehose');
const Game = require('../../helpers/gamefunction');
const constants = require('../../response/constants');
const whowapi = require('../../api/whowapi');
const { bucket } = require('../../helpers/couchbase');
const gameEvents = require('../../helpers/game-events');

/**
 * Special features related to secret of amun is implemented in this class
 * This class handles main game computation and logic for expanding feature during free spin
 */
class SecretOfAmun extends Compute {

  /**
  * This function is responsible to calculate result for a spin event
  * taking into account
  * view zone, pay lines, pay table
  */
  compute(game, gameId, token, session) {
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));

    // start calculating view zone
    let arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);

    // random numbers are fetched from reel configuration
    const randomNumber = rng.generateRandomNumberOld(game.default.gameConfig.reelConfig);
    // view zone is calculated below
    let expanding = [];
    const scatter = [];
    this.generateViewZone(randomNumber, noofReels, columnSize, expanding, scatter, game, arrays, session);

    // if expanding wild is detected complete reel is replaced with non expaning symbols each
    const deepCopyArray = arrays.clone();
    expanding = _.uniq(expanding);
    if (expanding.length > 0) {
      const copyArray = arrays.clone();
      const wildSymbolId = _.findKey(game.default.gameConfig.symbols, { SymbolType: 'Wild/Scatter', isExpandingWild: false });
      expanding.forEach((i) => {
        for (let j = 0; j < columnSize; j++) {
          copyArray.set(j, i, wildSymbolId);
        }
      });
      arrays = copyArray.clone();
    }

    // load pay arrays
    let payArray = game.default.gameConfig.payLines.normalizedPayArray;
    const copyPayArray = JSON.parse(JSON.stringify(payArray));

    // result based on view zone is calculated below
    const results = [];

    return new Promise((resolve, reject) => {
      const currentLines = session.eventData.currentLines;
      const lineArrays = game.default.gameConfig.selectablePaylines[currentLines];
      payArray.forEach((pay) => {
        const object = _.find(lineArrays, o => o === pay._id);
        if (!object) {
          _.remove(copyPayArray, arr => arr._id === pay._id);
        }
      });
      payArray = copyPayArray;
      arrays = deepCopyArray.clone();

      this.freeSpinCompute(scatter, results, game)
      .then((results) => {
        if (results.freeSpins) {
          // free spins are awarded
          if (_.isEmpty(session.eventData.freeSpinData)) {
            // there is no active free spin that is going on

            // choose a symbol to replace
            const pickedSymbol = this.pickFreeSpinExpandingSymbol(game);

            resolve(this.computeForNoActiveFreeSpins(game, arrays, deepCopyArray, results, payArray, columnSize, pickedSymbol));
          } else {
            // there are active free spins and free spin is awarded

            resolve(this.computeForFreeSpin(game, session, arrays, deepCopyArray, results, payArray, noofReels, columnSize));
          }
        } else if (!_.isEmpty(session.eventData.freeSpinData)) {
          // free spins are active but free spin is not awarded

          resolve(this.computeForFreeSpin(game, session, arrays, deepCopyArray, results, payArray, noofReels, columnSize));
        } else {
          // free spin is not active and free spins are not awarded
          // normal proceedings
          resolve(this.computeForNoActiveFreeSpins(game, arrays, deepCopyArray, results, payArray, columnSize));
        }
      });
    });
  }


  /**
  * This function is responsible to generate spin response
  * it includes view zone, wins, user details like wallet, bet array etc
  * necessary render view for the user
  */
  generateSpinResponse(obj, computeData, token, sessionRequestKey, sessionRequest, session) {
    // determine view port
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));

    // create reels for view zone
    const array = computeData.viewZone;
    const viewZone = {};

    for (let i = 0; i < noofReels; i++) {
      const line = [];
      for (let j = 0; j < columnSize; j++) {
        line.push({ symbolId: array.get(j, i), symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType, symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName });
      }
      viewZone[`Reel${i + 1}`] = line;
    }
    viewZone.viewZone = obj.game.default.gameConfig.viewZone;


    // create win document
    const result = {};
    result.viewZone = viewZone;
    result.wins = {};
    result.freeSpinData = {}, result.levelData = {};

    return new Promise((resolve, reject) => {
        // if free spin is present
      if (computeData.results.freeSpins) {
        if (!_.isEmpty(session)) {
          if (_.isEmpty(session.eventData.freeSpinData)) {
            result.freeSpinData.freeSpins = computeData.results.freeSpins;
            result.freeSpinData.pickedSymbol = computeData.pickedSymbol;
            result.freeSpinData.currentFreeSpin = 0;
            result.freeSpinData.winAmount = 0;

            session.eventData.freeSpinData = result.freeSpinData;
          } else {
            result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;

            session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
            session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
          }
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
          if (session.eventData.freeSpinData.isFreeSpinAwarded) {
            delete session.eventData.freeSpinData.isFreeSpinAwarded;
            delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
          }
          session.event = 'spin';
          session._updatedTimestamp = moment.utc();

          // const cas = super.map.get(`${session.eventData.userDetails.id}::${obj._id}`) || null;
          super.updateSession(session, obj._id);
        }
        result.isSessionUpdated = true;
      }

      // update for expanding symbols if they are expanding
      if (computeData.expanding && computeData.expanding.isPickedSymbolExpanding && computeData.expanding.isPickedSymbolExpanding === true) {
        result.expandData = {};
        // expanding: { isPickedSymbolExpanding, expandingPositions: expandResult[2] }
        const expandingPositions = computeData.expanding.expandingPositions;
        expandingPositions.forEach((position) => {
          const reelNumber = position.i + 1;
          const columnNumber = position.j;

          result.expandData[`reel${reelNumber}`] = { columnNumber, isExpanding: true };
        });
      }
        // total win amount calculated based on wins from pay line
      let total = 0;
      computeData.results.forEach((result) => {
        result.winAmount = result.multiplier * session.eventData.currentBet;
        total += (result.multiplier * session.eventData.currentBet);
        delete result.multiplier;
      });

      if (computeData.expanding && computeData.expanding.isPickedSymbolExpanding && computeData.expanding.isPickedSymbolExpanding === true) {
        computeData.expanding.expandedResults.forEach((results) => {
          results.winAmount = results.multiplier * session.eventData.currentBet;
          total += (results.multiplier * session.eventData.currentBet);
          delete results.multiplier;
        });

        result.wins.expandedWins = {};
        result.wins.expandedWins.expandedWinLinesCount = computeData.expanding.expandedResults.length;
        result.wins.expandedWins.expandedWinLines = computeData.expanding.expandedResults;
      }
      result.wins.winCoins = total;
      if (result.wins.winCoins > 0) {
        result.pickStatus = 'active';
      } else {
        result.pickStatus = 'release';
      }
      result.wins.winLinesCount = computeData.results.length;
      result.wins.winLines = JSON.parse(JSON.stringify(computeData.results));

      if (result.wins.winLines.freeSpins) {
        delete result.wins.winLines.freeSpins;
      }

        // put user session data
      result.session = {};
      result.session.sessionRequestKey = sessionRequestKey;
      result.session.sessionRequest = sessionRequest;

      /**
       * notify of all the symbols present in view zone
       */
      gameEvents.notifyGameEvents(obj, token, session, result, computeData.results.freeSpins);

      // mask view zone here
      result.viewZone = super.maskViewZone(result.viewZone, obj);
      if (_.isEmpty(session.eventData.freeSpinData)) {
          // no free spins
        resolve(this.callWhow(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
          // some free spins remaining
        result.freeSpinData = session.eventData.freeSpinData;

        if (computeData.results.freeSpins) {
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
        }

        if (!computeData.results.freeSpins) {
          result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
        }

        if (result.wins.winCoins > 0) {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
          session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
        } else {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
        }
          // call whow api
        if (session.eventData.freeSpinData.currentFreeSpin === 0) {
          // free spins just awarded
          result.isSessionUpdated = false;
          session.eventData.freeSpinData.winAmount = 0;
          resolve(this.callWhowForFirstFreeSpin(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
        } else {
          if (computeData.results.freeSpins && !_.isEmpty(session.eventData.freeSpinData)) {
            result.isSessionUpdated = false;
          }
          resolve(this.callBet(token, 0, total, 10, result, session, sessionRequestKey, sessionRequest, obj._id));
        }
      } else {
          // free spin object exists and there are no free spins remaining
        resolve(this.callWhow(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      }
    });
  }


  /**
   * This function picks a random symbol other than wild/scatter
   * it is invoked when free spin is triggered in main game
   */
  pickFreeSpinExpandingSymbol(game) {
    const symbolIds = [];
    const symbolData = game.default.gameConfig.symbols;
    Object.keys(symbolData).forEach((symbolId) => {
      if (symbolData[symbolId].SymbolType !== 'Wild/Scatter') {
        symbolIds.push(symbolId);
      }
    });
    return symbolIds[Math.floor(Math.random() * symbolIds.length)];
  }


  /**
   * This function expands the picked symbol if qualified
   * If the frequency of picked symbol, then it is expanded in whol reel
   */
  expandSymbolIfQualified(array, pickedSymbolId, noofReels, columnSize, game) {
    const copyArray = array.clone();
    const expandingPositions = [];
    let pickedSymbolCount = 0;
    for (let i = 0; i < noofReels; i += 1) {
      for (let j = 0; j < columnSize; j += 1) {
        if (copyArray.get(j, i) == pickedSymbolId) {
          for (let k = 0; k < columnSize; k += 1) {
            copyArray.set(k, i, pickedSymbolId);
          }
          expandingPositions.push({ i, j });
          pickedSymbolCount += 1;
          break;
        }
      }
    }

    if (game.default.gameConfig.payTable[pickedSymbolId][pickedSymbolCount] && game.default.gameConfig.payTable[pickedSymbolId][pickedSymbolCount].Multiplier > 0) {
      return [copyArray, pickedSymbolCount, expandingPositions, true];
    }
    return [array, pickedSymbolCount, expandingPositions, false];
  }


  /**
   * This function computes various data points
   * It picks the symbol which was picked when free spin was triggered
   * It then checks should the symbol expand,
   * and then calculates wins
   */
  computeForFreeSpin(game, session, arrays, deepCopyArray, results, payArray, noofReels, columnSize) {
    return new Promise((resolve, reject) => {
      let isPickedSymbolExpanding = false;

      // Step 1 : pick existing symbol from user session
      const pickedSymbol = session.eventData.freeSpinData.pickedSymbol;

      // Step 2 : check if the symbol from step 1 is there in view zone more than 2 times
      const expandResult = this.expandSymbolIfQualified(arrays, pickedSymbol, noofReels, columnSize, game);
      if (expandResult[3] == true) {
        isPickedSymbolExpanding = true;
      }

      if (isPickedSymbolExpanding) {
        const expandedViewZoneReels = this.fetchReelsForExpandedViewZone(game, expandResult[2], pickedSymbol, noofReels, columnSize);
        Promise.all(
          [
            this.computeWinsForFreeSpin(arrays, columnSize, payArray, game, results, pickedSymbol),
            this.computeWinsForFreeSpinExpanded(expandedViewZoneReels, columnSize, payArray, game, results),
            this.wildAndScatterPayCompute(expandResult[0], columnSize, payArray, game, results)
          ]).then((values) => {
            resolve({ viewZone: arrays, results, expanding: { isPickedSymbolExpanding, expandingPositions: expandResult[2], expandedResults: values[1] } });
          }).catch((err) => {
            resolve({ error: 'error in view zone' });
          });
      } else {
        Promise.all(
          [
            this.computeWinsForFreeSpin(arrays, columnSize, payArray, game, results),
            this.wildAndScatterPayCompute(arrays, columnSize, payArray, game, results)
          ]).then((values) => {
            resolve({ viewZone: arrays, results, expanding: { isPickedSymbolExpanding, expandingPositions: expandResult[2] } });
          }).catch((err) => {
            resolve({ error: 'error in view zone' });
          });
      }
    });
  }


  /**
   * This function computes results when free spins are not active
   * Wild and scatter symbols are considered when computing
   */
  computeForNoActiveFreeSpins(game, arrays, deepCopyArray, results, payArray, columnSize, pickedSymbol) {
    return new Promise((resolve, reject) => {
      Promise.all(
        [
          this.computeWins(arrays, columnSize, payArray, game, results),
          this.wildAndScatterPayCompute(arrays, columnSize, payArray, game, results)
        ]).then((values) => {
          if (pickedSymbol) {
            resolve({ viewZone: arrays, results, pickedSymbol });
          } else {
            resolve({ viewZone: arrays, results });
          }
        }).catch((err) => {
          resolve({ error: 'error in view zone' });
        });
    });
  }


  /**
  * This function computes wins based on view zone
  * win is calculated starting from left i.e. from 1st reel
  * A special symbol i.e. Wilds and scatter both is taken into consideration while computing wins
  */
  computeWins(arrays, columnSize, payArray, game, results) {
    return new Promise((resolve, reject) => {
      const j = 0;
      for (let i = 0; i < columnSize; i++) {
        for (let k = 0; k < payArray.length; k++) {
          let symbolId = arrays.get(i, j);
          const payLine = payArray[k].data;
          let ofAKind = [];
          if (payLine[0] == i) {
            ofAKind.push('test');
            for (let l = 1; l < payLine.length; l++) {
              if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Wild/Scatter') {
                symbolId = arrays.get(payLine[l], l);
                ofAKind.push('test');
                continue;
              }
              if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType != 'Wild/Scatter' && arrays.get(payLine[l], l) != symbolId) {
                break;
              }
              ofAKind.push('test');
            }
            if (ofAKind.length > 0 && game.default.gameConfig.payTable[symbolId][ofAKind.length]) {
              results.push({ symbolId, winType: `${ofAKind.length} of a kind`, winLineId: payArray[k]._id, multiplier: game.default.gameConfig.payTable[symbolId][ofAKind.length].Multiplier });
            }
          }
          ofAKind = [];
        }
      }
      resolve(results);
    });
  }


  /**
  * This function computes win results when free spin is active
  * Wins are calculatd before symbol expands
  */
  computeWinsForFreeSpin(arrays, columnSize, payArray, game, results, pickedSymbol) {
    return new Promise((resolve, reject) => {
      const j = 0;
      for (let i = 0; i < columnSize; i++) {
        for (let k = 0; k < payArray.length; k++) {
          let symbolId = arrays.get(i, j);
          const payLine = payArray[k].data;
          let ofAKind = [];
          if (payLine[0] == i) {
            ofAKind.push('test');
            for (let l = 1; l < payLine.length; l++) {
              if (symbolId == pickedSymbol) {
                break;
              }
              if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Wild/Scatter') {
                symbolId = arrays.get(payLine[l], l);
                ofAKind.push('test');
                continue;
              }
              if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType != 'Wild/Scatter' && arrays.get(payLine[l], l) != symbolId) {
                break;
              }
              ofAKind.push('test');
            }

            if (ofAKind.length > 0 && game.default.gameConfig.payTable[symbolId][ofAKind.length] && symbolId !== pickedSymbol) {
              results.push({ symbolId, winType: `${ofAKind.length} of a kind`, winLineId: payArray[k]._id, multiplier: game.default.gameConfig.payTable[symbolId][ofAKind.length].Multiplier });
            }
          }
          ofAKind = [];
        }
      }
      resolve(results);
    });
  }


  /**
  * This function computes win results when free spin is active
  * Wins are calculatd after symbol expands
  */
  computeWinsForFreeSpinExpanded(arrays, columnSize, payArray, game, results) {
    const expandedResults = [];
    return new Promise((resolve, reject) => {
      const j = 0;
      for (let i = 0; i < columnSize; i++) {
        for (let k = 0; k < payArray.length; k++) {
          const symbolId = arrays.get(i, j);
          const payLine = payArray[k].data;
          let ofAKind = [];
          if (payLine[0] == i) {
            ofAKind.push('test');
            for (let l = 1; l < payLine.length; l++) {
              if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Wild/Scatter') {
                symbolId = arrays.get(payLine[l], l);
                ofAKind.push('test');
                continue;
              }
              if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType != 'Wild/Scatter' && arrays.get(payLine[l], l) != symbolId) {
                break;
              }
              ofAKind.push('test');
            }
            if (ofAKind.length > 0 && game.default.gameConfig.payTable[symbolId][ofAKind.length]) {
              expandedResults.push({ symbolId, winType: `${ofAKind.length} of a kind`, winLineId: payArray[k]._id, multiplier: game.default.gameConfig.payTable[symbolId][ofAKind.length].Multiplier });
            }
          }
          ofAKind = [];
        }
      }
      resolve(expandedResults);
    });
  }


  /**
  * This function checks if scatter is present in view zone pays or not
  * if scatters are present and scatter pays then wins are calculated subsequentaly
  */
  wildAndScatterPayCompute(arrays, columnSize, payArray, game, results) {
    return new Promise((resolve, reject) => {
      const j = 0;
      for (let i = 0; i < columnSize; i++) {
        const symbolId = arrays.get(i, j);
        for (let k = 0; k < payArray.length; k++) {
          const payLine = payArray[k].data;
          const ofAKind = [];
          if (payLine[0] == i) {
            ofAKind.push('test');
            for (let l = 1; l < payLine.length; l++) {
              if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType != 'Wild/Scatter' || arrays.get(payLine[l], l) != symbolId) {
                break;
              }
              ofAKind.push('test');
            }
            if (ofAKind.length > 0 && game.default.gameConfig.payTable[symbolId][ofAKind.length]) {
              results.push({ symbolId, winType: `${ofAKind.length} of a kind`, winLineId: payArray[k]._id, multiplier: game.default.gameConfig.payTable[symbolId][ofAKind.length].Multiplier });
            }
          }
        }
      }
      resolve(results);
    });
  }


  /**
  * This function generates view zone base on random numbers
  * view zone is calculated to be shown to user
  */
  generateViewZone(randomNumber, noofReels, columnSize, expanding, scatter, game, arrays, session) {
    if (!_.isEmpty(session.eventData.freeSpinData) && (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0)) {
      for (let i = 0; i < noofReels; i++) {
        for (let j = 0; j < columnSize; j++) {
          const symbolId = this.getSymbol(j, i, game.default.gameConfig.freeSpin.reelConfig, randomNumber);
          if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Wild/Scatter') {
            scatter.push(i);
          }
          arrays.set(j, i, symbolId);
        }
      }
    } else {
      for (let i = 0; i < noofReels; i++) {
        for (let j = 0; j < columnSize; j++) {
          const symbolId = this.getSymbol(j, i, game.default.gameConfig.reelConfig, randomNumber);
          if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Wild/Scatter') {
            scatter.push(i);
          }
          arrays.set(j, i, symbolId);
        }
      }
    }
  }


  /**
  * This function checks if there are any free spins to be awarded based on view zone
  * If there are free spins than the number of free spins are determined based on game configuration
  */
  freeSpinCompute(scatter, results, game) {
    return new Promise((resolve, reject) => {
      if (scatter.length >= game.default.gameConfig.freeSpin.minimumRequiredCount) {
        if (!game.default.gameConfig.freeSpin.freeSpinWins[scatter.length]) {
          results.freeSpins = game.default.gameConfig.freeSpin.freeSpinWins[game.default.gameConfig.freeSpin.keys[game.default.gameConfig.freeSpin.keys.length - 1]].noofFS;
        } else {
          results.freeSpins = game.default.gameConfig.freeSpin.freeSpinWins[scatter.length].noofFS;
        }
      }
      resolve(results);
    });
  }


  /**
   * View zone for expanded symbols is computed by this function
   * Expanded view zone is necessary to compute results
   */
  fetchReelsForExpandedViewZone(game, expandingPositions, pickedSymbol, noofReels, columnSize) {
    let maskedSymbol;
    Object.keys(game.default.gameConfig.symbols).forEach((symbolId) => {
      if (symbolId != pickedSymbol && game.default.gameConfig.symbols[symbolId].SymbolType != 'Wild/Scatter') {
        maskedSymbol = symbolId;
      }
    });
    const maskedArrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    for (let i = 0; i < expandingPositions.length; i++) {
      for (let j = 0; j < columnSize; j++) {
        maskedArrays.set(j, i, pickedSymbol);
      }
    }
    for (let i = expandingPositions.length; i < noofReels; i++) {
      for (let j = 0; j < columnSize; j++) {
        maskedArrays.set(j, i, maskedSymbol);
      }
    }
    return maskedArrays;
  }


  /**
  * This function models the spin result document based on the analytics requirement
  * it deletes the fields not required and fetches the field required from database
  * logging for secret of amun is customized
  */
  logSpinResult(result, token, gameId) {
    if (result && token && gameId) {
      delete result.levelData;
      delete result.game;
      delete result.jackpot;
      delete result.session;
      delete result.viewZone;
      result.gameId = gameId;
      const gameData = Game.getGame(gameId);
      result.baseGameId = gameData.game.default.baseGameId;
      result.gameVersion = gameData.game.default.versionName;
      result.stage = gameData.game.default.stage;

      /**
      * removes un necessary data from while logging spin results
      */
      const symbols = {};
      const types = {};
      if (result.wins.winLines.length > 0) {
        result.wins.winLines.forEach((line) => {
          if (!types[line.winType]) {
            types[line.winType] = 0;
          }
          if (!symbols[line.symbolId]) {
            symbols[line.symbolId] = 0;
          }
          types[line.winType]++;
          symbols[line.symbolId]++;
        });
        result.wins.types = types;
        result.wins.symbols = symbols;
      }
      if (result.wins.expandedWins && result.wins.expandedWins.expandedWinLines && result.wins.expandedWins.expandedWinLines.length > 0) {
        result.wins.expandedWins.expandedWinLines.forEach((line) => {
          if (!types[line.winType]) {
            types[line.winType] = 0;
          }
          if (!symbols[line.symbolId]) {
            symbols[line.symbolId] = 0;
          }
          types[line.winType]++;
          symbols[line.symbolId]++;
        });
        result.wins.types = types;
        result.wins.symbols = symbols;
      }
      gameSession.getUserSession(token, gameId)
      .then((session) => {
        result.user = {};
        result.user.currentBet = session.eventData.currentBet;
        result.user.currentLines = session.eventData.currentLines;
        result.user.totalBet = session.eventData.currentBet * session.eventData.currentLines;
        result.user.token = token;
        result.user.userId = session.eventData.userDetails.id;

        if (result.expandData) {
          delete result.expandData;
        }
        // if (result.wins.expandedWins) {
        //   delete result.wins.expandedWins;
        // }
        logger.info(result);
      }).catch((err) => {
        console.log(err);
      });
    }
  }


  /**
  * updates user game session with details of a spin result
  * it may include free spin if won in game spin
  * and updated wallet chips as a result of whow api call bet/play
  */
  gameSpin(gameId, token, docDetails) {
    docDetails._updatedTimestamp = moment.utc();
    gameSession.getUserSession(token, gameId)
        .then((session) => {
          let lastFS = false;
          session.game = docDetails.game;
          session.eventData.userDetails.wallet = docDetails.wallet;
          session.gamble = docDetails.gamble;
          if (!_.isEmpty(session)) {
            session.event = 'spin';
            if (!_.isEmpty(docDetails.freeSpinData) && docDetails.freeSpinData.isFreeSpinAwarded) {
              delete docDetails.freeSpinData.isFreeSpinAwarded;
              delete docDetails.freeSpinData.noofFreeSpinsAwarded;
            }
            session.eventData.freeSpinData = docDetails.freeSpinData;
            if (!_.isEmpty(session.eventData.freeSpinData) && (session.eventData.freeSpinData.currentFreeSpin == session.eventData.freeSpinData.freeSpins)) {
              const totalWin = session.eventData.freeSpinData.winAmount;
              session.gamble.winAmount = totalWin;
              session.gamble.initialWin = totalWin;
              session.gamble.wasFreeSpin = true;
              session.eventData.freeSpinData = {};
              lastFS = true;
            } else if (session.gamble) {
              session.gamble.wasFreeSpin = false;
            }
          }
          session.pickStatus = docDetails.pickStatus;
          if (lastFS) {
            session.pickStatus = 'active';
          }
          const sessionRequestKey = docDetails.session.sessionRequestKey;
          const sessionRequest = docDetails.session.sessionRequest;
          sessionRequest.status = 'ready',
          sessionRequest.spinCompletedTimestamp = moment().utc();
          this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);
          super.updateSession(session, gameId);
        }).catch((err) => {
          console.log(err);
        });
  }


  /**
  * updates user's session with pickStatus
  */
  updateSessionWithCas(session, gameId, cas) {
    return new Promise((resolve, reject) => {
      bucket.upsert(`${session.eventData.userDetails.id}::${gameId}`, session, { cas }, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }


  /**
  * This function fetches user's current session for a game
  * session data includes current bet amount, current pay lines
  * and user profile related informations
  */
  getUserSession(token, gameId) {
    return new Promise((resolve, reject) => {
      bucket.get(token, (err, result) => {
        if (!err) {
          bucket.get(`${result.value.id}::${gameId}`, result, (in_err, get_result) => {
            if (!in_err) {
              resolve(get_result);
            } else {
              reject();
            }
          });
        } else {
          reject(err);
        }
      });
    });
  }


  /**
   * This function hadles the gamble request for secret of amun
   * Validations related gamble is done and gamble winings are determined
   */
  handleGambleRequest(token, gameId, clientId, pickedCard, session) {
    return new Promise((resolve) => {
      if (!pickedCard && pickedCard != null && pickedCard.toLowerCase() !== constants.GAMBLE.RED && pickedCard.toLowerCase() !== constants.GAMBLE.BLACK) {
        resolve({ error: constants.RES_MESSAGES.INVALID_GAMBLE_CARD, errorCode: constants.RES_ERROR_CODES.INVALID_GAMBLE_CARD });
      } else {
        /**
        * Step 1: Check if free spin is not active
        */
        if (session.clientID === clientId) {
          if (!_.isEmpty(session.eventData.freeSpinData)) {
            // free spin is active
            resolve({ error: constants.RES_MESSAGES.GAMBLE_NOT_ACTIVE, errorCode: constants.RES_ERROR_CODES.GAMBLE_NOT_ACTIVE });
          } else if (!session.gamble.winAmount > 0) {
            /**
            * Step 2: get the last win stored in session
            * if there is no last win, gamble cannot be availed
            */
            resolve({ error: constants.RES_MESSAGES.GAMBLE_NOT_ACTIVE, errorCode: constants.RES_ERROR_CODES.GAMBLE_NOT_ACTIVE });
          } else {
            const pickedNumber = Math.floor(Math.random() * constants.GAMBLE.SECRET_OF_AMUN_RANDOM_RANGE);
            let isRed = false;
            let card = constants.GAMBLE.BLACK;
            if ((pickedNumber >= 0 && pickedNumber <= 13) || (pickedNumber >= 27 && pickedNumber <= 39)) {
              isRed = true;
              card = constants.GAMBLE.RED;
            }
            /**
            * Step 3: User either wins or looses based on randomness in server
            * - If user wins, winning is doubled
            * - If user looses, he looses even the last win amount
            */
            session.gamble.history.push(card);
            if (session.gamble.history.length > 5) {
              session.gamble.history.shift();
            }

            if ((pickedCard.toLowerCase() === constants.GAMBLE.RED && isRed) || (pickedCard.toLowerCase() === constants.GAMBLE.BLACK && !isRed)) {
              // user wins
              session.gamble.winAmount = session.gamble.winAmount * 2;
              session.gamble.betAmount = session.gamble.winAmount;
              session.gamble.count += 1;
              // call bet whow api
              this.whowBetApi(session.gamble.id, token, 0, session.gamble.winAmount, session.gamble.betAmount, session)
              .then((response) => {
                if (session.gamble.count === constants.GAMBLE.SECRET_OF_AMUN_MAX_GAMBLE) {
                  // max gabmle
                  this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
                  .then((data) => {
                    session.gamble.count = 0;
                    session.gamble.winAmount = 0;
                    session.pickStatus = 'release';
                    super.updateSession(session, gameId);
                    resolve({ gambleCount: session.gamble.count, gambleWon: true, gambleTotalAmount: session.gamble.winAmount, gambleHistory: session.gamble.history, wallet: session.eventData.userDetails.wallet, isGambleAvailable: false });
                  });
                } else {
                  session.pickStatus = 'active';
                  super.updateSession(session, gameId);
                  resolve({ gambleCount: session.gamble.count, gambleWon: true, gambleTotalAmount: session.gamble.winAmount, gambleBetAmount: session.gamble.betAmount, gamblePotentialWinAmount: session.gamble.betAmount * 2, gambleHistory: session.gamble.history, wallet: session.eventData.userDetails.wallet, isGambleAvailable: true });
                }
              });
            } else {
              // user looses
              // call close whow api
              this.whowCloseApi(session.gamble.id, token, 0, session)
              .then((data) => {
                session.gamble.count = 0;
                session.gamble.winAmount = 0;
                session.pickStatus = 'release';
                super.updateSession(session, gameId);
                resolve({ gambleCount: session.gamble.count, gambleWon: false, gambleTotalAmount: session.gamble.winAmount, gambleHistory: session.gamble.history, wallet: session.eventData.userDetails.wallet, isGambleAvailable: false });
              });
            }
          }
        } else {
          resolve({ error: constants.RES_MESSAGES.INVALID_CLIENT, errorCode: constants.RES_ERROR_CODES.INVALID_CLIENT });
        }
      }
    });
  }


  /**
   * Whow bet api is called in case user wins
   */
  whowBetApi(roundId, token, betAmount, total, virtualAmount, session) {
    return new Promise((resolve, reject) => {
      whowapi.bet(token, {
        roundId,
        betAmount,
        winAmount: total,
        virtualAmount
      }).then((response) => {
        response = JSON.parse(response);
        session.eventData.userDetails.wallet = response.payload.user.wallet;
        session.game = response.payload.game;
        resolve(response);
      }).catch((err) => {
        resolve(err);
      });
    });
  }


  /**
   * Whow close api is called in case when user looses
   * when user looses previous round is close
   */
  whowCloseApi(roundId, token, total, session) {
    return new Promise((resolve, reject) => {
      whowapi.close(token, {
        winAmount: total,
        roundId
      })
      .then((response) => {
        response = JSON.parse(response);
        session.eventData.userDetails.wallet = response.payload.user.wallet;
        session.game = response.payload.game;

        resolve(response);
      })
      .catch((err) => {
        console.log(err);
        resolve(err);
      });
    });
  }


  /**
   * This function closes previous active round and calls a new bet api
   * This function is used when user wins in main game
   */
  callWhowBet(token, result, computeData, betAmount, virtualAmount, total, session, sessionRequestKey, sessionRequest) {
    return new Promise((resolve, reject) => {
      this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
      .then((response) => {
        resolve(this.callBet(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest));
      });
    });
  }


  /**
   * This function is overriden from the super class
   * Whow api is called at the end of each spin
   * informing the wins to whow
   */
  callWhow(token, result, computeData, betAmount, virtualAmount, total, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve, reject) => {
      if (computeData.results.length > 0) {
        if (session.gamble && session.gamble.id && session.gamble.winAmount && session.gamble.winAmount > 0) {
          this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
          .then((response) => {
            resolve(this.callBet(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
          });
        } else {
          resolve(this.callBet(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
        }
      } else if (session.gamble && session.gamble.id && session.gamble.winAmount && session.gamble.winAmount > 0) {
        this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
        .then((response) => {
          resolve(this.callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
        });
      } else {
        resolve(this.callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
      }
    });
  }


  /**
   * This function is called when the first free spin is played
   * Purpose of this function is to close any winnings that was awarded before free spin was triggered
   */
  callWhowForFirstFreeSpin(token, result, computeData, betAmount, virtualAmount, total, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve, reject) => {
      if (computeData.results.length > 0) {
        this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
        .then((data) => {
          session.gamble.winAmount = 0;
          this.callBet(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId)
          .then((results) => {
            if (results.gamble) {
              whowapi.close(token, {
                winAmount: total,
                roundId: results.gamble.id
              });
            }
            resolve(results);
          });
        });
      } else if (session.gamble && session.gamble.winAmount && session.gamble.winAmount > 0) {
        this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
          .then((data) => {
            session.gamble.winAmount = 0;
            resolve(this.callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
          });
      } else {
        resolve(this.callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
      }
    });
  }


  /**
   * This is the actual whow api call function
   * bet api is called when user wins
   */
  callBet(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve, reject) => {
      whowapi.bet(token, {
        betAmount,
        winAmount: total,
        virtualAmount
      }).then((response) => {
        response = JSON.parse(response);

        // this signifies that wallet does not have enough chips to bet on
        if (response.status && response.status === 110) {
          if (session.gamble && !_.isEmpty(session.gamble)) {
            session.gamble.winAmount = 0;
            session.gamble.count = 0;
          }
          sessionRequest.status = 'ready';
          session.eventData.freeSpinData = {};
          this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);
          resolve({ error: constants.RES_MESSAGES.NOT_ENOUGH_BALANCE, errorCode: constants.RES_ERROR_CODES.NOT_ENOUGH_BALANCE });
        } else {
          result.gamble = response.payload.round;
          if (!session.gamble || !session.gamble.history) {
            result.gamble.history = [];
          } else {
            result.gamble.history = session.gamble.history;
          }
          result.gamble.winAmount = total;
          result.gamble.count = 0;
          result.levelData.level = response.payload.user.level;
          result.levelData.levelProgress = response.payload.user.levelProgress;
          result.wallet = response.payload.user.wallet;
          result.game = response.payload.game;
          result.game.betAmount = (session.eventData.currentBet * session.eventData.currentLines);
          result.jackpot = response.payload.user.jackpotData;
          resolve(result);
        }
      }).catch((err) => {
        console.log(err);
        resolve(result);
      });
    });
  }


  /**
   * This is the actual whow api call function
   * play api is called when user does not win
   */
  callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve, reject) => {
      whowapi.play(token, {
        betAmount,
        winAmount: total,
        virtualAmount
      }).then((response) => {
        response = JSON.parse(response);

        if (response.status && response.status === 110) {
          sessionRequest.status = 'ready';
          if (session.gamble && !_.isEmpty(session.gamble)) {
            session.gamble.winAmount = 0;
            session.gamble.count = 0;
          }
          session.eventData.freeSpinData = {};
          this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);
          resolve({ error: constants.RES_MESSAGES.NOT_ENOUGH_BALANCE, errorCode: constants.RES_ERROR_CODES.NOT_ENOUGH_BALANCE });
        } else {
          result.gamble = response.payload.round;
          if (!session.gamble || !session.gamble.history) {
            result.gamble.history = [];
          } else {
            result.gamble.history = session.gamble.history;
          }
          result.gamble.count = 0;
          result.levelData.level = response.payload.user.level;
          result.levelData.levelProgress = response.payload.user.levelProgress;
          result.wallet = response.payload.user.wallet;
          result.game = response.payload.game;
          result.game.betAmount = (session.eventData.currentBet * session.eventData.currentLines);
          result.jackpot = response.payload.user.jackpotData;
          resolve(result);
        }
      }).catch((err) => {
        console.log(err);
        resolve(result);
      });
    });
  }


  /**
   * This function collects the win amount of current spin
   * and also the current win amount when gamble is active
   */
  pickGamble(token, gameId, clientId, session) {
    return new Promise((resolve) => {
      if (session.clientID === clientId) {
        if (!_.isEmpty(session.eventData.freeSpinData)) {
          resolve({ error: constants.RES_MESSAGES.GAMBLE_NOT_ACTIVE, errorCode: constants.RES_ERROR_CODES.GAMBLE_NOT_ACTIVE });
        } else if (!session.gamble.winAmount > 0) {
          resolve({ error: constants.RES_MESSAGES.NO_WINS_TO_COLLECT, errorCode: constants.RES_ERROR_CODES.NO_WINS_TO_COLLECT });
        } else {
          this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
          .then((data) => {
            session.pickStatus = 'release';
            if (session.gamble.count > 0) {
              session.gamble.count = 0;
              session.gamble.winAmount = 0;
              super.updateSession(session, gameId);
              resolve({ gambleCount: session.gamble.count, gambleWon: true, gambleTotalAmount: session.gamble.winAmount, gambleHistory: session.gamble.history, wallet: session.eventData.userDetails.wallet, isGambleAvailable: false });
            } else {
              const totalWin = session.gamble.winAmount;
              session.gamble.winAmount = 0;
              super.updateSession(session, gameId);
              resolve({ collectAmount: totalWin, wallet: session.eventData.userDetails.wallet });
            }
          });
        }
      } else {
        resolve({ error: constants.RES_MESSAGES.INVALID_CLIENT, errorCode: constants.RES_ERROR_CODES.INVALID_CLIENT });
      }
    });
  }


  /**
  * This function is used to build response for game click event
  * game data along with user data for subsequent  game events calls are passed to client
  */
  generateGameClickResponse(data, token, gameId, casinoFreeSpins, freespinObj) {
    const result = {};
    result.game = {};
    result.game.gameId = data._id;
    result.game.viewZone = data.game.default.gameConfig.viewZone;
    result.game.reels = {};
    result.game.reels.noofReels = data.game.default.gameConfig.reelConfig.NumberOfReels;

    // do not give away reel configuration, mask initiail view
    const symbolIndices = Object.keys(data.game.default.gameConfig.symbols);
    for (let i = 0; i < data.game.default.gameConfig.reelConfig.NumberOfReels; i += 1) {
      const columnSize = parseInt(data.game.default.gameConfig.viewZone.toString().charAt(0));
      const reelArray = [];
      for (let j = 0; j < columnSize; j += 1) {
        reelArray.push(symbolIndices[Math.floor(Math.random() * symbolIndices.length)]);
      }
      result.game.reels[`Reel${i + 1}`] = reelArray;
    }
    result.game.payArray = data.game.default.gameConfig.payLines.PayArray;
    result.game.payTable = data.game.default.gameConfig.payTable;

    result.game.bigWins = data.game.default.gameConfig.bigWins;
    result.game.symbols = data.game.default.gameConfig.symbols;
    result.game.stage = data.game.default.gameConfig.stage;
    result.game.version = data.game.default.gameConfig.versionName;
    result.userDetails = data.userDetails;
    result.userDetails.selectablePaylines = data.game.default.gameConfig.selectablePaylines.options;
    // console.log(JSON.stringify(data.userDetails));
    result.userDetails.currentBet = data.userDetails.game.settings.bets[0] || 5,
    result.userDetails.currentLines = parseInt(data.game.default.gameConfig.selectablePaylines.options[data.game.default.gameConfig.selectablePaylines.options.length - 1]) || 20;
    result.userDetails.totalBet = (result.userDetails.currentBet * result.userDetails.currentLines);

    return new Promise((resolve) => {
      gameSession.getUserSession(token, gameId)
      .then((session) => {
        result.userDetails.currentBet = session.eventData.currentBet;
        result.userDetails.currentLines = session.eventData.currentLines;
        if (casinoFreeSpins && _.isEmpty(session.eventData.freeSpinData)) {
          const noofFs = data.game.default.gameConfig.freeSpin.freeSpinWins[data.game.default.gameConfig.freeSpin.keys[0]].noofFS;

          session.eventData.freeSpinData = {};

          session.eventData.freeSpinData.freeSpins = noofFs;
          session.eventData.freeSpinData.pickedSymbol = this.pickFreeSpinExpandingSymbol(data.game);
          session.eventData.freeSpinData.currentFreeSpin = 0;
          session.eventData.freeSpinData.winAmount = 0;

          this.updateSession(session, gameId);

          // update game events
          gameEvents.validateFreeSpins(token, freespinObj, (session.eventData.currentBet * session.eventData.currentLines), noofFs);

        }
        result.userDetails.freeSpinData = session.eventData.freeSpinData;
        result.userDetails.totalBet = (result.userDetails.currentBet * result.userDetails.currentLines);
        resolve(result);
      }).catch(() => {
        result.userDetails.currentBet = data.userDetails.game.settings.bets[0] || 5,
        result.userDetails.currentLines = parseInt(data.game.default.gameConfig.selectablePaylines.options[data.game.default.gameConfig.selectablePaylines.options.length - 1]) || 20;
        result.userDetails.freeSpinData = {};
        result.userDetails.totalBet = (result.userDetails.currentBet * result.userDetails.currentLines);
        resolve(result);
      });
    });
  }
}

module.exports = new SecretOfAmun();
