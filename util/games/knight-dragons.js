const { Compute } = require('../compute');
const nj = require('numjs');
const rng = require('../rng');
const _ = require('lodash');
const moment = require('moment');
const gameSession = require('../gamesession');
const logger = require('../../helpers/firehose');
const Game = require('../../helpers/gamefunction');
const constants = require('../../response/constants');
const whowapi = require('../../api/whowapi');
const { bucket } = require('../../helpers/couchbase');
const uuid = require('uuid');
const shuffle = require('shuffle-array');
const gameEvents = require('../../helpers/game-events');

/**
 * Special features related to zombie apocalypse is implemented in this class
 * This class handles main game computation and logic for expanding feature during free spin
 */
class KnightAndDragons extends Compute {

  /**
   * [Handles game click event for knight and dragons]
   * @param  gameId   knight and dragons game id
   * @param  token    user token
   * @param  clientID client id generated on the client side
   * @param  data     user related data fetched from whow
   * @param  gameData game configuratin related data
   */
  handleGameClick(gameId, token, clientID, data, gameData) {
    bucket.get(`${data.userDetails.user.id}::${gameId}`, (err, result) => {
      if (!err) {
        if (result == null) {
          gameSession.createGameSessionObject(gameId, clientID, data, gameData);
        } else {
          result.value.clientID = clientID;
          gameSession.buildUserSessionObject(result, data, gameData);
          this.updateSession(result.value, gameId);
        }
      } else {
        gameSession.createGameSessionObject(gameId, clientID, data, gameData);
      }
    });
    gameSession.createUserDataAndSessionRequest(data, gameId, token);
  }


  /**
  * This function is responsible to calculate result for a spin event
  * taking into account
  * view zone, pay lines, pay table
  */
  compute(game, gameId, token, session) {
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));

    // start calculating view zone
    let arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);

    // random numbers are fetched from reel configuration
    const randomNumber = rng.generateRandomNumber(game.default.gameConfig.reelConfig);

    // view zone is calculated below
    const expanding = [];
    const scatter = [];
    super.generateViewZone(randomNumber, noofReels, columnSize, expanding, scatter, game, arrays, session);

    // if expanding wild is detected complete reel is replaced with non expaning symbols each
    const deepCopyArray = arrays.clone();

    // load pay arrays
    let payArray = game.default.gameConfig.payLines.normalizedPayArray;
    const copyPayArray = JSON.parse(JSON.stringify(payArray));

    // result based on view zone is calculated below
    const results = [];

    return new Promise((resolve, reject) => {
      const currentLines = session.eventData.currentLines;
      const lineArrays = game.default.gameConfig.selectablePaylines[currentLines];
      payArray.forEach((pay) => {
        const object = _.find(lineArrays, o => o === pay._id);
        if (!object) {
          _.remove(copyPayArray, arr => arr._id === pay._id);
        }
      });
      payArray = copyPayArray;
      arrays = deepCopyArray.clone();


      // check if user wins a bonus game
      const bonusSymbolId = _.findKey(game.default.gameConfig.symbols, { SymbolType: 'Bonus' });
      const bonusWon = this.checkIfBonusWon(game, arrays, columnSize, noofReels, bonusSymbolId);


      // check if adjacent expected symbols are present
      const adjacents = this.checkIfAdjacentKnights(arrays, noofReels, columnSize, game);
      if (adjacents && adjacents.length > 0) {
        // there are adjacent good knight or damsel symbols
        this.fetchAdjacentWinnings(adjacents, session.eventData.currentBet, game.default.gameConfig.specialFeature);
      }


      this.freeSpinCompute(scatter, results, game)
      .then((results) => {
        if (results.freeSpins) {
          // free spins are awarded
          if (_.isEmpty(session.eventData.freeSpinData)) {
            // there is no active free spin that is going on

            resolve(this.computeForNoActiveFreeSpins(game, arrays, deepCopyArray, results, payArray, columnSize, adjacents, bonusWon));
          } else {
            // there are active free spins and free spin is awarded

            // check for damsel in free spins
            const damselPositions = this.damselInFreeSpins(arrays, noofReels, columnSize, game);
            let outcomes = [];
            if (damselPositions && damselPositions.length > 0) {
              outcomes = this.damselFreeSpinOutput(damselPositions, game, session.eventData.currentBet);
            }
            resolve(this.computeForFreeSpin(game, session, arrays, deepCopyArray, results, payArray, noofReels, columnSize, outcomes, bonusWon, adjacents));
          }
        } else if (!_.isEmpty(session.eventData.freeSpinData)) {
          // free spins are active but free spin is not awarded

          // check for damsel in free spins
          const damselPositions = this.damselInFreeSpins(arrays, noofReels, columnSize, game);
          let outcomes = [];
          if (damselPositions && damselPositions.length > 0) {
            outcomes = this.damselFreeSpinOutput(damselPositions, game, session.eventData.currentBet);
          }

          resolve(this.computeForFreeSpin(game, session, arrays, deepCopyArray, results, payArray, noofReels, columnSize, outcomes, bonusWon, adjacents));
        } else {
          // free spin is not active and free spins are not awarded
          // normal proceedings

          resolve(this.computeForNoActiveFreeSpins(game, arrays, deepCopyArray, results, payArray, columnSize, adjacents, bonusWon));
        }
      });
    });
  }


  /**
  * This function is responsible to generate spin response
  * it includes view zone, wins, user details like wallet, bet array etc
  * necessary render view for the user
  */
  generateSpinResponse(obj, computeData, token, sessionRequestKey, sessionRequest, session) {
    // determine view port
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));

    // create reels for view zone
    const array = computeData.viewZone;
    const viewZone = {};

    for (let i = 0; i < noofReels; i++) {
      const line = [];
      for (let j = 0; j < columnSize; j++) {
        line.push({ symbolId: array.get(j, i), symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType, symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName });
      }
      viewZone[`Reel${i + 1}`] = line;
    }
    viewZone.viewZone = obj.game.default.gameConfig.viewZone;

    // create win document
    const result = {};
    result.viewZone = viewZone;
    result.wins = {};
    result.freeSpinData = {}, result.levelData = {};


    // special feature adjacent knight outcome
    let adjacentTotal = 0;
    if (computeData.adjacents && computeData.adjacents.length > 0) {
      const adjacents = computeData.adjacents;
      const adjacentWinnings = [];
      adjacents.forEach((adjacent) => {
        const reelNumber = adjacent.currentPosition.i + 1;
        const columnNumber = adjacent.currentPosition.j;

        if (adjacent.hasGoodKnightWon) {
          adjacentWinnings.push({ goodKnightPosition: { reelNumber, columnNumber }, appearinPosition: adjacent.position, symbolName: adjacent.symbolName, hasGoodKnightWon: true, winAmount: adjacent.winAmount });
          adjacentTotal += adjacent.winAmount;
        } else {
          adjacentWinnings.push({ goodKnightPosition: { reelNumber, columnNumber }, appearinPosition: adjacent.position, symbolName: adjacent.symbolName, hasGoodKnightWon: false });
        }
      });
      result.adjacentGoodKnightWinnings = adjacentWinnings;
    }

    // special feature damsel in free spin outcome
    let damselTotal = 0;
    let freeSpinWon = 0;
    if (computeData.outcomes && computeData.outcomes.length > 0) {
      const outcomes = computeData.outcomes;
      const damselFeatureWinnings = [];
      outcomes.forEach((outcome) => {
        const reelNumber = outcome.damselPosition.i + 1;
        const columnNumber = outcome.damselPosition.j;

        if (outcome.winType === 'freeSpin') {
          damselFeatureWinnings.push({ damselPoistion: { reelNumber, columnNumber }, winType: outcome.winType, noofFreeSpinsWon: outcome.noofFreeSpinWon });
          freeSpinWon = outcome.noofFreeSpinWon;
        } else {
          damselFeatureWinnings.push({ damselPoistion: { reelNumber, columnNumber }, winType: outcome.winType, winCoins: outcome.winCoins });
          damselTotal += outcome.winCoins;
        }
      });
      result.damselFeatureWinnings = damselFeatureWinnings;
    }

    // develop bonus object
    if (computeData.bonusWon && computeData.bonusWon.bonusWon) {
      const bonus = this.initiateBonus(computeData.bonusWon, obj);
      result.bonus = bonus;
    }

    return new Promise((resolve, reject) => {
        // if free spin is present
      if (computeData.results.freeSpins) {
        if (!_.isEmpty(session)) {
          if (_.isEmpty(session.eventData.freeSpinData)) {
            result.freeSpinData.freeSpins = computeData.results.freeSpins;
            result.freeSpinData.pickedSymbol = computeData.pickedSymbol;
            result.freeSpinData.currentFreeSpin = 0;
            result.freeSpinData.winAmount = 0;

            session.eventData.freeSpinData = result.freeSpinData;
          } else {
            result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;

            session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
            session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
          }
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
          if (session.eventData.freeSpinData.isFreeSpinAwarded) {
            delete session.eventData.freeSpinData.isFreeSpinAwarded;
            delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
          }
          session.event = 'spin';
          session._updatedTimestamp = moment.utc();


          // if bonus is won construct bonus object for user session
          if (computeData.bonusWon && computeData.bonusWon.bonusWon) {
            session.eventData.bonus = result.bonus.bonusSession.bonus;
          }

          session.eventData.freeSpinData.freeSpins += freeSpinWon;
          super.updateSessionAndSessionRequest(session, obj._id, sessionRequestKey, sessionRequest);
        }
        result.isSessionUpdated = true;
      }

        // total win amount calculated based on wins from pay line
      let total = 0;
      computeData.results.forEach((result) => {
        result.winAmount = result.multiplier * session.eventData.currentBet;

        total += result.winAmount;
        delete result.multiplier;
      });

      // add damsel and adjacent symbol winnings
      total += adjacentTotal + damselTotal;

      // add free spins
      if (session.eventData.freeSpinData && session.eventData.freeSpinData.freeSpins) {
        session.eventData.freeSpinData.freeSpins += freeSpinWon;
      }

      result.wins.winCoins = total;
      result.wins.winLinesCount = computeData.results.length;
      result.wins.winLines = JSON.parse(JSON.stringify(computeData.results));

      if (result.wins.winLines.freeSpins) {
        delete result.wins.winLines.freeSpins;
      }

        // put user session data
      result.session = {};
      result.session.sessionRequestKey = sessionRequestKey;
      result.session.sessionRequest = sessionRequest;
      /**
       * notify of all the symbols present in view zone
       */
      gameEvents.notifyGameEvents(obj, token, session, result, computeData.results.freeSpins);

      // mask view zone here
      result.viewZone = super.maskViewZone(result.viewZone, obj);
      if (_.isEmpty(session.eventData.freeSpinData)) {
          // no free spins
        resolve(this.callWhow(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
          // some free spins remaining
        result.freeSpinData = session.eventData.freeSpinData;

        if (computeData.results.freeSpins) {
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
        }

        if (!computeData.results.freeSpins) {
          result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
        }

        if (result.wins.winCoins > 0) {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
          session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
        } else {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
        }
          // call whow api
        if (session.eventData.freeSpinData.currentFreeSpin === 0) {
          // free spins just awarded
          result.isSessionUpdated = false;
          session.eventData.freeSpinData.winAmount = 0;
          resolve(this.callWhowForFirstFreeSpin(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
        } else {
          if (computeData.results.freeSpins && !_.isEmpty(session.eventData.freeSpinData)) {
            result.isSessionUpdated = false;
          }
          resolve(this.callBet(token, 0, total, 10, result, session, sessionRequestKey, sessionRequest, obj._id));
        }
      } else {
          // free spin object exists and there are no free spins remaining
        resolve(this.callWhow(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      }
    });
  }


  /**
   * This function computes various data points
   * It checks for wins after frozen wild has expanded
   * and computes wins for scatter and wild symbols if they pay
   */
  computeForFreeSpin(game, session, arrays, deepCopyArray, results, payArray, noofReels, columnSize, outcomes, bonusWon, adjacents) {
    return new Promise((resolve, reject) => {
      Promise.all(
        [
          super.computeWins(arrays, columnSize, payArray, game, results),
          super.scatterPayCompute(arrays, columnSize, payArray, game, results),
          super.wildPayCompute(deepCopyArray, columnSize, payArray, game, results)
        ]).then((values) => {
          resolve({ viewZone: deepCopyArray, results, outcomes, bonusWon, adjacents });
        }).catch((err) => {
          resolve({ error: 'error in view zone' });
        });
    });
  }


  /**
   * This function computes results when free spins are not active
   * Wild and scatter symbols are considered when computing
   */
  computeForNoActiveFreeSpins(game, arrays, deepCopyArray, results, payArray, columnSize, adjacents, bonusWon) {
    return new Promise((resolve, reject) => {
      Promise.all(
        [
          super.computeWins(arrays, columnSize, payArray, game, results),
          super.scatterPayCompute(arrays, columnSize, payArray, game, results),
          super.wildPayCompute(deepCopyArray, columnSize, payArray, game, results)
        ]).then((values) => {
          resolve({ viewZone: deepCopyArray, results, adjacents, bonusWon });
        }).catch((err) => {
          resolve({ error: 'error in view zone' });
        });
    });
  }


  /**
  * This function checks if there are any free spins to be awarded based on view zone
  * If there are free spins than the number of free spins are determined based on game configuration
  */
  freeSpinCompute(scatter, results, game) {
    return new Promise((resolve, reject) => {
      if (scatter.length >= game.default.gameConfig.freeSpin.minimumRequiredCount) {
        if (!game.default.gameConfig.freeSpin.freeSpinWins[scatter.length]) {
          results.freeSpins = game.default.gameConfig.freeSpin.freeSpinWins[game.default.gameConfig.freeSpin.keys[game.default.gameConfig.freeSpin.keys.length - 1]].noofFS;
        } else {
          results.freeSpins = game.default.gameConfig.freeSpin.freeSpinWins[scatter.length].noofFS;
        }
      }
      resolve(results);
    });
  }

  /**
  * This function models the spin result document based on the analytics requirement
  * it deletes the fields not required and fetches the field required from database
  * logging for secret of amun is customized
  */
  logSpinResult(result, token, gameId) {
    if (result && token && gameId) {
      delete result.levelData;
      delete result.game;
      delete result.jackpot;
      delete result.session;
      delete result.viewZone;
      result.gameId = gameId;
      const gameData = Game.getGame(gameId);
      result.baseGameId = gameData.game.default.baseGameId;
      result.gameVersion = gameData.game.default.versionName;
      result.stage = gameData.game.default.stage;

      /**
      * removes un necessary data from while logging spin results
      */
      const symbols = {};
      const types = {};
      if (result.wins.winLines.length > 0) {
        result.wins.winLines.forEach((line) => {
          if (!types[line.winType]) {
            types[line.winType] = 0;
          }
          if (!symbols[line.symbolId]) {
            symbols[line.symbolId] = 0;
          }
          types[line.winType]++;
          symbols[line.symbolId]++;
        });
        result.wins.types = types;
        result.wins.symbols = symbols;
      }
      if (result.wins.expandedWins && result.wins.expandedWins.expandedWinLines && result.wins.expandedWins.expandedWinLines.length > 0) {
        result.wins.expandedWins.expandedWinLines.forEach((line) => {
          if (!types[line.winType]) {
            types[line.winType] = 0;
          }
          if (!symbols[line.symbolId]) {
            symbols[line.symbolId] = 0;
          }
          types[line.winType]++;
          symbols[line.symbolId]++;
        });
        result.wins.types = types;
        result.wins.symbols = symbols;
      }
      gameSession.getUserSession(token, gameId)
      .then((session) => {
        result.user = {};
        result.user.currentBet = session.eventData.currentBet;
        result.user.currentLines = session.eventData.currentLines;
        result.user.totalBet = session.eventData.currentBet * session.eventData.currentLines;
        result.user.token = token;
        result.user.userId = session.eventData.userDetails.id;

        if (result.expandData) {
          delete result.expandData;
        }
        if (result.wins.expandedWins) {
          delete result.wins.expandedWins;
        }

        logger.info(result);
      }).catch((err) => {
        console.log(err);
      });
    }
  }


  /**
  * updates user game session with details of a spin result
  * it may include free spin if won in game spin
  * and updated wallet chips as a result of whow api call bet/play
  */
  gameSpin(gameId, token, docDetails) {
    docDetails._updatedTimestamp = moment.utc();
    gameSession.getUserSession(token, gameId)
    .then((session) => {
      session.game = docDetails.game;
      session.eventData.userDetails.wallet = docDetails.wallet;
      session.gamble = docDetails.gamble;
      if (docDetails.expandingPositions) {
        session.eventData.expandingPositions = docDetails.expandingPositions;
      } else {
        session.eventData.expandingPositions = [];
      }

      if (!_.isEmpty(session)) {
        session.event = 'spin';
        if (!_.isEmpty(docDetails.freeSpinData) && docDetails.freeSpinData.isFreeSpinAwarded) {
          delete docDetails.freeSpinData.isFreeSpinAwarded;
          delete docDetails.freeSpinData.noofFreeSpinsAwarded;
        }
        session.eventData.freeSpinData = docDetails.freeSpinData;
        if (!_.isEmpty(session.eventData.freeSpinData) && (session.eventData.freeSpinData.currentFreeSpin == session.eventData.freeSpinData.freeSpins)) {
          const totalWin = session.eventData.freeSpinData.winAmount;
          session.gamble.winAmount = totalWin;
          session.gamble.initialWin = totalWin;
          session.gamble.wasFreeSpin = true;
          session.eventData.freeSpinData = {};
        } else if (session.gamble) {
          session.gamble.wasFreeSpin = false;
        }
      }

      const sessionRequestKey = docDetails.session.sessionRequestKey;
      const sessionRequest = docDetails.session.sessionRequest;
      sessionRequest.status = 'ready',
      sessionRequest.spinCompletedTimestamp = moment().utc();

      if (docDetails.bonus) {
        session.eventData.bonus = docDetails.bonus.bonusSession.bonus;
      }
      this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);
      this.updateSession(session, gameId);
    }).catch((err) => {
      console.log(err);
    });
  }


  /**
  * updates user's session with free spin data, wallet chips etc.
  */
  updateSession(session, gameId) {
    bucket.upsert(`${session.eventData.userDetails.id}::${gameId}`, session, (err, result) => {
      if (err) {
        console.log(err);
      }
    });
  }


  /**
  * updates user's session and session requests
  * created when game click took place
  */
  updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest) {
    bucket.upsert(`${session.eventData.userDetails.id}::${gameId}`, session, (err, result) => {
      if (!err) {
        this.updateSessionRequestDoc(sessionRequestKey, sessionRequest);
      }
    });
  }


  /**
  * updates user's session request document with provided document
  * no optimistic database concurreny is used in this update
  */
  updateSessionRequestDoc(sessionRequestKey, sessionRequestDoc) {
    bucket.upsert(sessionRequestKey, sessionRequestDoc, (err, result) => {
      if (err) {
        console.log(err);
      }
    });
  }


  /**
   * This function hadles the gamble request for secret of amun
   * Validations related gamble is done and gamble winings are determined
   */
  handleGambleRequest(token, gameId, clientId, pickedCard) {
    return new Promise((resolve, reject) => {
      if (!pickedCard && pickedCard != null && pickedCard.toLowerCase() !== constants.GAMBLE.RED && pickedCard.toLowerCase() !== constants.GAMBLE.BLACK) {
        resolve({ error: constants.RES_MESSAGES.INVALID_GAMBLE_CARD, errorCode: constants.RES_ERROR_CODES.INVALID_GAMBLE_CARD });
      } else {
        /**
        * Step 1: Check if free spin is not active
        */
        gameSession.getUserSession(token, gameId)
        .then((session) => {
          if (session.clientID === clientId) {
            if (!_.isEmpty(session.eventData.freeSpinData)) {
              // free spin is active
              resolve({ error: constants.RES_MESSAGES.GAMBLE_NOT_ACTIVE, errorCode: constants.RES_ERROR_CODES.GAMBLE_NOT_ACTIVE });
            } else if (!session.gamble.winAmount > 0) {
              /**
              * Step 2: get the last win stored in session
              * if there is no last win, gamble cannot be availed
              */
              resolve({ error: constants.RES_MESSAGES.GAMBLE_NOT_ACTIVE, errorCode: constants.RES_ERROR_CODES.GAMBLE_NOT_ACTIVE });
            } else {
              const pickedNumber = Math.floor(Math.random() * constants.GAMBLE.SECRET_OF_AMUN_RANDOM_RANGE);
              let isRed = false;
              let card = constants.GAMBLE.BLACK;
              if ((pickedNumber >= 0 && pickedNumber <= 13) || (pickedNumber >= 27 && pickedNumber <= 39)) {
                isRed = true;
                card = constants.GAMBLE.RED;
              }
              /**
              * Step 3: User either wins or looses based on randomness in server
              * - If user wins, winning is doubled
              * - If user looses, he looses even the last win amount
              */
              session.gamble.history.push(card);
              if (session.gamble.history.length > 5) {
                session.gamble.history.shift();
              }

              if ((pickedCard.toLowerCase() === constants.GAMBLE.RED && isRed) || (pickedCard.toLowerCase() === constants.GAMBLE.BLACK && !isRed)) {
                // user wins
                session.gamble.winAmount = session.gamble.winAmount * 2;
                session.gamble.betAmount = session.gamble.winAmount;
                session.gamble.count += 1;
                // call bet whow api
                this.whowBetApi(session.gamble.id, token, 0, session.gamble.winAmount, session.gamble.betAmount, session)
                .then((response) => {
                  if (session.gamble.count === constants.GAMBLE.SECRET_OF_AMUN_MAX_GAMBLE) {
                    // max gabmle
                    this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
                    .then((data) => {
                      session.gamble.count = 0;
                      session.gamble.winAmount = 0;
                      this.updateSession(session, gameId);
                      resolve({ gambleCount: session.gamble.count, gambleWon: true, gambleTotalAmount: session.gamble.winAmount, gambleHistory: session.gamble.history, wallet: session.eventData.userDetails.wallet, isGambleAvailable: false });
                    });
                  } else {
                    this.updateSession(session, gameId);
                    resolve({ gambleCount: session.gamble.count, gambleWon: true, gambleTotalAmount: session.gamble.winAmount, gambleBetAmount: session.gamble.betAmount, gamblePotentialWinAmount: session.gamble.betAmount * 2, gambleHistory: session.gamble.history, wallet: session.eventData.userDetails.wallet, isGambleAvailable: true });
                  }
                });
              } else {
                // user looses
                // call close whow api
                this.whowCloseApi(session.gamble.id, token, 0, session)
                .then((data) => {
                  session.gamble.count = 0;
                  session.gamble.winAmount = 0;
                  this.updateSession(session, gameId);
                  resolve({ gambleCount: session.gamble.count, gambleWon: false, gambleTotalAmount: session.gamble.winAmount, gambleHistory: session.gamble.history, wallet: session.eventData.userDetails.wallet, isGambleAvailable: false });
                });
              }
            }
          } else {
            resolve({ error: constants.RES_MESSAGES.INVALID_CLIENT, errorCode: constants.RES_ERROR_CODES.INVALID_CLIENT });
          }
        })
        .catch((err) => {
          console.log(err);
          resolve({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
        });
      }
    });
  }


  /**
   * Whow bet api is called in case user wins
   */
  whowBetApi(roundId, token, betAmount, total, virtualAmount, session) {
    return new Promise((resolve, reject) => {
      whowapi.bet(token, {
        roundId,
        betAmount,
        winAmount: total,
        virtualAmount
      }).then((response) => {
        response = JSON.parse(response);
        session.eventData.userDetails.wallet = response.payload.user.wallet;
        session.game = response.payload.game;
        resolve(response);
      }).catch((err) => {
        resolve(err);
      });
    });
  }


  /**
   * Whow close api is called in case when user looses
   * when user looses previous round is close
   */
  whowCloseApi(roundId, token, total, session) {
    return new Promise((resolve, reject) => {
      whowapi.close(token, {
        winAmount: total,
        roundId
      })
      .then((response) => {
        response = JSON.parse(response);
        session.eventData.userDetails.wallet = response.payload.user.wallet;
        session.game = response.payload.game;

        resolve(response);
      })
      .catch((err) => {
        console.log(err);
        resolve(err);
      });
    });
  }


  /**
   * This function closes previous active round and calls a new bet api
   * This function is used when user wins in main game
   */
  callWhowBet(token, result, computeData, betAmount, virtualAmount, total, session) {
    return new Promise((resolve, reject) => {
      this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
      .then((response) => {
        resolve(this.callBet(token, betAmount, total, virtualAmount, result, session));
      });
    });
  }


  /**
   * This function is overriden from the super class
   * Whow api is called at the end of each spin
   * informing the wins to whow
   */
  callWhow(token, result, computeData, betAmount, virtualAmount, total, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve, reject) => {
      if (computeData.results.length > 0) {
        if (session.gamble && session.gamble.id) {
          this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
          .then((response) => {
            resolve(this.callBet(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
          });
        } else {
          resolve(this.callBet(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
        }
      } else if (session.gamble && session.gamble.id) {
        this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
        .then((response) => {
          resolve(this.callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
        });
      } else {
        resolve(this.callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
      }
    });
  }


  /**
   * This function is called when the first free spin is played
   * Purpose of this function is to close any winnings that was awarded before free spin was triggered
   */
  callWhowForFirstFreeSpin(token, result, computeData, betAmount, virtualAmount, total, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve, reject) => {
      if (computeData.results.length > 0) {
        this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
        .then((data) => {
          session.gamble.winAmount = 0;
          this.callBet(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId)
          .then((results) => {
            whowapi.close(token, {
              winAmount: total,
              roundId: results.gamble.id
            });
            resolve(results);
          });
        });
      } else {
        resolve(this.callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
      }
    });
  }


  /**
   * This is the actual whow api call function
   * bet api is called when user wins
   */
  callBet(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve, reject) => {
      whowapi.bet(token, {
        betAmount,
        winAmount: total,
        virtualAmount
      }).then((response) => {
        response = JSON.parse(response);

        if (response.status && response.status === 110) {
          sessionRequest.status = 'ready';
          session.gamble.winAmount = 0;
          session.gamble.count = 0;
          this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);
          resolve({ error: constants.RES_MESSAGES.NOT_ENOUGH_BALANCE, errorCode: constants.RES_ERROR_CODES.NOT_ENOUGH_BALANCE });
        } else {
          result.gamble = response.payload.round;
          if (session.gamble === undefined || session.gamble === 'undefined' || session.gamble.history === undefined || session.gamble.history === 'undefined') {
            result.gamble.history = [];
          } else {
            result.gamble.history = session.gamble.history;
          }
          result.gamble.winAmount = total;
          result.gamble.count = 0;
          result.levelData.level = response.payload.user.level;
          result.levelData.levelProgress = response.payload.user.levelProgress;
          result.wallet = response.payload.user.wallet;
          result.game = response.payload.game;
          result.game.betAmount = (session.eventData.currentBet * session.eventData.currentLines);
          result.jackpot = response.payload.user.jackpotData;
          resolve(result);
        }
      }).catch((err) => {
        console.log(err);
        resolve(result);
      });
    });
  }


  /**
   * This is the actual whow api call function
   * play api is called when user does not win
   */
  callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve, reject) => {
      whowapi.play(token, {
        betAmount,
        winAmount: total,
        virtualAmount
      }).then((response) => {
        response = JSON.parse(response);

        if (response.status && response.status === 110) {
          sessionRequest.status = 'ready';
          if (session.gamble && !_.isEmpty(session.gamble)) {
            session.gamble.winAmount = 0;
            session.gamble.count = 0;
          }
          this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);
          resolve({ error: constants.RES_MESSAGES.NOT_ENOUGH_BALANCE, errorCode: constants.RES_ERROR_CODES.NOT_ENOUGH_BALANCE });
        } else {
          result.gamble = response.payload.round;
          if (session.gamble === undefined || session.gamble === 'undefined' || session.gamble.history === undefined || session.gamble.history === 'undefined') {
            result.gamble.history = [];
          } else {
            result.gamble.history = session.gamble.history;
          }
          result.gamble.count = 0;
          result.levelData.level = response.payload.user.level;
          result.levelData.levelProgress = response.payload.user.levelProgress;
          result.wallet = response.payload.user.wallet;
          result.game = response.payload.game;
          result.game.betAmount = (session.eventData.currentBet * session.eventData.currentLines);
          result.jackpot = response.payload.user.jackpotData;
          resolve(result);
        }
      }).catch((err) => {
        console.log(err);
        resolve(result);
      });
    });
  }


  /**
   * This function collects the win amount of current spin
   * and also the current win amount when gamble is active
   */
  pickGamble(token, gameId, clientId) {
    return new Promise((resolve, reject) => {
      gameSession.getUserSession(token, gameId)
      .then((session) => {
        if (session.clientID === clientId) {
          if (!_.isEmpty(session.eventData.freeSpinData)) {
            resolve({ error: constants.RES_MESSAGES.GAMBLE_NOT_ACTIVE, errorCode: constants.RES_ERROR_CODES.GAMBLE_NOT_ACTIVE });
          } else if (!session.gamble.winAmount > 0) {
            resolve({ error: constants.RES_MESSAGES.NO_WINS_TO_COLLECT, errorCode: constants.RES_ERROR_CODES.NO_WINS_TO_COLLECT });
          } else {
            this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
            .then((data) => {
              if (session.gamble.count > 0) {
                session.gamble.count = 0;
                session.gamble.winAmount = 0;
                this.updateSession(session, gameId);
                resolve({ gambleCount: session.gamble.count, gambleWon: true, gambleTotalAmount: session.gamble.winAmount, gambleHistory: session.gamble.history, wallet: session.eventData.userDetails.wallet, isGambleAvailable: false });
              } else {
                const totalWin = session.gamble.winAmount;
                session.gamble.winAmount = 0;
                this.updateSession(session, gameId);
                resolve({ collectAmount: totalWin, wallet: session.eventData.userDetails.wallet });
              }
            });
          }
        } else {
          resolve({ error: constants.RES_MESSAGES.INVALID_CLIENT, errorCode: constants.RES_ERROR_CODES.INVALID_CLIENT });
        }
      }).catch((err) => {
        console.log(err);
        resolve({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
      });
    });
  }


  /**
   * Whow api call to update bonus winnings
   */
  updateBonusWins(token, betAmount, total, virtualAmount) {
    whowapi.bet(token, {
      betAmount,
      winAmount: total,
      virtualAmount
    }).then((response) => {
      response = JSON.parse(response);
      whowapi.close(token, {
        winAmount: total,
        roundId: response.payload.round.id
      });
    });
  }


  /**
  * This function updates user's session request doument allowing a spin to take place
  * game session helper is used to update session data
  */
  updateSessionRequestDocForBonus(sessionRequestKey, sessionRequestDoc, cas, bonusPlay) {
    sessionRequestDoc.bonusPlay = bonusPlay || 'playing';
    sessionRequestDoc.bonusIniTimestamp = moment().utc();
    return this.updateSessionRequestDocWithCas(sessionRequestKey, sessionRequestDoc, cas);
  }


  /**
  * This updates user's session request document with provided document
  * Optimistic concurreny is handled with couchbase cas document
  * to ensure parallel spins for same game for a user does not happen unless previous spin is not finished
  */
  updateSessionRequestDocWithCas(sessionRequestKey, sessionRequestDoc, cas) {
    return new Promise((resolve, reject) => {
      bucket.upsert(sessionRequestKey, sessionRequestDoc, { cas }, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result.cas);
        }
      });
    });
  }


  /**
   * This function checks if there is evil symbol or damsel adjacent to good knight
   * Immediate left and immediate right positions are determined to check symbol
   */
  checkIfAdjacentKnights(arrays, noofReels, columnSize, game) {
    const adjacents = [];
    for (let i = 0; i < noofReels; i += 1) {
      for (let j = 0; j < columnSize; j += 1) {
        if (game.default.gameConfig.symbols[arrays.get(j, i)].SymbolName === 'Good Knight') {
          // check for adjacent left symbol if there is damsel or bad knight
          if (i > 0 && (game.default.gameConfig.symbols[arrays.get(j, (i - 1))].SymbolName === 'Damsel' || game.default.gameConfig.symbols[arrays.get(j, (i - 1))].SymbolName === 'Evil Knight')) {
            const symbolX = i - 1;
            adjacents.push({ currentPosition: { i, j }, currentSymbol: 'Good Knight', position: 'left', symbolPoistion: { i: symbolX, j }, symbolName: game.default.gameConfig.symbols[arrays.get(j, (i - 1))].SymbolName });
          }

          // check for adjacent right symbol if there is damsel or bad knight
          if (i < (noofReels - 1) && (game.default.gameConfig.symbols[arrays.get(j, (i + 1))].SymbolName === 'Damsel' || game.default.gameConfig.symbols[arrays.get(j, (i + 1))].SymbolName === 'Evil Knight')) {
            const symbolX = i + 1;
            adjacents.push({ currentPosition: { i, j }, currentSymbol: 'Good Knight', position: 'right', symbolPoistion: { i: symbolX, j }, symbolName: game.default.gameConfig.symbols[arrays.get(j, (i + 1))].SymbolName });
          }
        }
      }
    }
    return adjacents;
  }


  /**
   * determine winnings for adjacent symbols
   */
  fetchAdjacentWinnings(adjacents, currentBet, specialFeature) {
    adjacents.forEach((adjacent) => {
      let hasGoodKnightWon = Math.random() >= 0.5;
      if (adjacent.symbolName === 'Damsel') {
        hasGoodKnightWon = true;
      }
      adjacent.hasGoodKnightWon = hasGoodKnightWon;
      if (hasGoodKnightWon) {
        adjacent.winAmount = currentBet * specialFeature.mainGame.adjacentWinnings.betMultiplier;
      }
    });
    return adjacents;
  }


  /**
   * This function determines if damsel is present in view zone
   * It determines position of damsel
   */
  damselInFreeSpins(arrays, noofReels, columnSize, game) {
    const damsel = [];
    for (let i = 0; i < noofReels; i += 1) {
      for (let j = 0; j < columnSize; j += 1) {
        if (game.default.gameConfig.symbols[arrays.get(j, i)].SymbolName === 'Damsel') {
          damsel.push({ i, j });
        }
      }
    }
    return damsel;
  }


  /**
   * Determines result of a damsel symbol
   * Damsel symbol either provides a free spin or win coins
   */
  damselFreeSpinOutput(damselPositions, game, currentBet) {
    const outcomes = [];
    damselPositions.forEach((damsel) => {
      const isWinPrize = Math.random() >= 0.5;
      if (isWinPrize) {
        // user wins coins
        const multiplier = game.default.gameConfig.specialFeature.freeSpin.damselOutcomeWinPrizeBetArray[Math.floor(Math.random() * game.default.gameConfig.specialFeature.freeSpin.damselOutcomeWinPrizeBetArray.length)];
        outcomes.push({ damselPosition: { i: damsel.j, j: damsel.j }, winType: 'winCoins', winCoins: multiplier * currentBet });
      } else {
        // user wins free spin
        outcomes.push({ damselPosition: { i: damsel.j, j: damsel.j }, winType: 'freeSpin', noofFreeSpinWon: game.default.gameConfig.specialFeature.freeSpin.damselOutcomeFreeSpins });
      }
    });
    return outcomes;
  }


  /**
   * This function determines if user has won bonus game
   * Bonus symbols in view zone is determined and checked with game configuration
   */
  checkIfBonusWon(game, arrays, columnSize, noofReels, bonusSymbolId) {
    let bonusCount = 0;
    let bonusDetected = false;
    for (let i = 0; i < noofReels; i += 1) {
      for (let j = 0; j < columnSize; j += 1) {
        if (game.default.gameConfig.symbols[arrays.get(j, i)].SymbolType == 'Bonus') {
          bonusCount += 1;
          bonusDetected = true;
          break;
        } else {
          bonusDetected = false;
        }
      }
      if (bonusDetected) {
        continue;
      }
    }
    if (game.default.gameConfig.bonus && game.default.gameConfig.bonus !== undefined && game.default.gameConfig.bonus !== 'undefined') {
      if (game.default.gameConfig.bonus.bonusWins[bonusCount] != null) {
        return { bonusWon: true, bonusDoc: game.default.gameConfig.bonus.bonusWins[bonusCount] };
      }
    }
    return { bonusWon: false };
  }


  initiateBonus(bonusDoc, obj) {
    const bonusSession = {};
    const bonusSessionCards = {};
    const bonusResultDoc = {};
    let bonusResultCards = [];
    const cards = obj.game.default.gameConfig.bonus.pickDragons.cards;
    cards.forEach((card) => {
      const id = uuid();
      bonusSessionCards[id] = {};
      bonusSessionCards[id].cardType = card.cardType;
      bonusSessionCards[id].noofDragons = card.noofDragons;

      bonusResultCards.push({ cardId: id });
    });
    bonusResultCards = shuffle(bonusResultCards);
    bonusSession.bonus = {};
    bonusSession.bonus.isActive = true;
    bonusSession.bonus.stage = {};
    bonusSession.bonus.stage.currentStage = 'pickDragons';
    bonusSession.bonus.stage.bonusSessionCards = bonusSessionCards;

    bonusResultDoc.bonus = {};
    bonusResultDoc.bonus.stage = {};
    bonusResultDoc.bonus.stage.currentStage = 'pickDragons';
    bonusResultDoc.bonus.stage.cards = bonusResultCards;

    return { bonusSession, bonusResultDoc };
  }


  /**
   * Handles bonus request from client
   * it checks if bonus is active and makes decision accordingly
   * bonus outcomes are determined by it
   */
  handleBonusRequest(token, gameId, cardId) {
    return new Promise(async (resolve, reject) => {
      try {
        const sessionCas = await gameSession.getUserSessionAndSessionRequest(token, gameId);
        if (sessionCas.sessionRequestKey && sessionCas.sessionKey) {
          const sessionRequest = sessionCas[sessionCas.sessionRequestKey].value;
          if (sessionRequest.bonusPlay === 'init' || sessionRequest.bonusPlay === 'ready') {
            const session = sessionCas[sessionCas.sessionKey].value;
            if (!session.eventData.bonus || !session.eventData.bonus.isActive) {
              resolve({ error: constants.RES_MESSAGES.BONUS_NOT_ACTIVE });
            } else {
              const cas = await this.updateSessionRequestDocForBonus(sessionCas.sessionRequestKey, sessionRequest, sessionCas[sessionCas.sessionRequestKey].cas);

              const sessionRequestKey = sessionCas.sessionRequestKey;
              const sessionRequectDoc = sessionCas[sessionCas.sessionRequestKey].value;

              const gameData = Game.getGame(gameId);
              const bonus = gameData.game.default.gameConfig.bonus;
              const card = session.eventData.bonus.stage.bonusSessionCards[cardId];
              if (!card || card === null || card === undefined || card === 'undefined') {
                this.updateSessionRequestDocForBonus(sessionCas.sessionRequestKey, sessionRequest, cas, 'ready');
                resolve({ error: constants.RES_MESSAGES.INVALID_CARD_ID });
              } else if (session.eventData.bonus.stage.currentStage === 'pickDragons') {
                // first stage
                const response = await this.moveBonusToNextStage(session, gameId, bonus, card, sessionRequestKey, sessionRequectDoc, cas);
                resolve(response);
              } else if (card.isOpened) {
                this.updateSessionRequestDocForBonus(sessionCas.sessionRequestKey, sessionRequest, cas, 'ready');
                resolve({ error: constants.RES_MESSAGES.KAD_BONUS_ALREADY_OPEN });
              } else {
                // picked from second/last stage
                const response = await this.handlePickDamsel(session, gameId, bonus, cardId, sessionRequestKey, sessionRequectDoc, cas, token);
                resolve(response);
              }
            }
          } else {
              // not ready
            resolve({ error: constants.RES_MESSAGES.BONUS_CANNOT_BE_PLAYED });
          }
        } else {
            // no game click
          resolve({ error: constants.RES_MESSAGES.BONUS_NOT_ALLOWED_NO_GAME_CLICK });
        }
      } catch (e) {
        console.log(e);
        resolve({ error: constants.RES_MESSAGES.BONUS_CANNOT_BE_PLAYED });
      }
    });
  }


  moveBonusToNextStage(session, gameId, bonus, pickedCard, sessionRequestKey, sessionRequectDoc, cas) {
    session.eventData.bonus.stage.currentStage = 'pickDamsel';
    const noofCards = bonus.pickDamsel.noofCards;
    const noofDragons = bonus.pickDamsel.noofDragonCards;

    // build cards
    const cards = {};
    const cardIds = [];
    for (let i = 0; i < noofCards; i += 1) {
      const id = uuid();
      cards[id] = {};
      cards[id].isOpened = false;
      cards[id].isDragon = false;
      cards[id].prizeWon = 0;
      cardIds.push(id);
    }

    // build dragons
    for (let i = 0; i < noofDragons; i += 1) {
      cards[cardIds[i]].isDragon = true;
    }


    // build order
    const shuffledCards = shuffle(cardIds);
    session.eventData.bonus.stage.bonusSessionCards = cards;
    session.eventData.bonus.stage.shuffledCards = shuffledCards;
    session.eventData.bonus.stage.pickedCard = pickedCard;
    session.eventData.bonus.stage.currentPick = 0;
    session.eventData.bonus.stage.dragonPicks = 0;


    return new Promise(async (resolve, reject) => {
      try {
        await this.updateSessionRequestDocForBonus(sessionRequestKey, sessionRequectDoc, cas, 'ready');
        this.updateSession(session, gameId);
        const bonusResultDoc = {};

        bonusResultDoc.result = pickedCard;
        bonusResultDoc.bonus = {};
        bonusResultDoc.bonus.stage = {};
        bonusResultDoc.bonus.stage.currentStage = 'pickDamsel';
        bonusResultDoc.bonus.stage.currentPick = 0;
        bonusResultDoc.bonus.stage.dragonPicks = 0;
        bonusResultDoc.bonus.stage.totalPicks = noofCards;
        bonusResultDoc.bonus.stage.cards = shuffledCards;
        resolve(bonusResultDoc);
      } catch (e) {
        reject(e);
      }
    });
  }


  handlePickDamsel(session, gameId, bonus, cardId, sessionRequestKey, sessionRequectDoc, cas, token) {
    return new Promise(async (resolve, reject) => {
      const response = {};
      response.isBonusAvailable = true;
      try {
        if (session.eventData.bonus.stage.bonusSessionCards[cardId].isDragon) {
          response.cardType = 'Dragon';
          session.eventData.bonus.stage.bonusSessionCards[cardId].isOpened = true;
          session.eventData.bonus.stage.dragonPicks = session.eventData.bonus.stage.dragonPicks + 1;
          session.eventData.bonus.stage.currentPick = session.eventData.bonus.stage.currentPick + 1;
          if (session.eventData.bonus && session.eventData.bonus.stage.dragonPicks >= session.eventData.bonus.stage.pickedCard.noofDragons) {
            // end bonus
            response.isBonusAvailable = false;
          }
          if (session.eventData.bonus && session.eventData.bonus.stage.currentPick >= bonus.pickDamsel.noofDamselCards) {
            // end bonus
            response.isBonusAvailable = false;
          }
        } else {
          response.cardType = 'Damsel';
          const multiplier = bonus.pickDamsel.damselWinMultiplierBetArray[Math.floor(Math.random() * bonus.pickDamsel.damselWinMultiplierBetArray.length)];
          const winCoins = session.eventData.currentBet * multiplier;

          response.winCoins = winCoins;
          session.eventData.bonus.stage.bonusSessionCards[cardId].isOpened = true;
          session.eventData.bonus.stage.bonusSessionCards[cardId].prizeWon = winCoins;
          session.eventData.bonus.stage.currentPick = session.eventData.bonus.stage.currentPick + 1;
          if (session.eventData.bonus.stage.currentPick >= bonus.pickDamsel.noofDamselCards) {
            // end bonus
            response.isBonusAvailable = false;
          }
          this.updateBonusWins(token, 0, winCoins, 10);
        }

        // build response here
        response.currentDragonPicks = session.eventData.bonus.stage.dragonPicks;
        response.currentPick = session.eventData.bonus.stage.currentPick;
        response.dragonsAvailable = session.eventData.bonus.stage.pickedCard.noofDragons;
        response.toalPicks = bonus.pickDamsel.noofDamselCards;

        if (response.isBonusAvailable === false) {
          session.eventData.bonus = null;
        }

        // update session here
        await this.updateSessionRequestDocForBonus(sessionRequestKey, sessionRequectDoc, cas, 'ready');
        this.updateSession(session, gameId);

        resolve(response);
      } catch (e) {
        console.log(e);
        resolve({ error: constants.RES_MESSAGES.SOMETHING_WENT_WRONG, erroCode: constants.RES_ERROR_CODES.SOMETHING_WENT_WRONG });
      }
    });
  }


  /**
  * This function is used to build response for game click event for kad
  * game data along with user data for subsequent  game events calls are passed to client
  */
  generateGameClickResponse(data, token, gameId) {
    const result = {};
    result.game = {};
    result.game.gameId = data._id;
    result.game.viewZone = data.game.default.gameConfig.viewZone;
    result.game.reels = {};
    result.game.reels.noofReels = data.game.default.gameConfig.reelConfig.NumberOfReels;

    // do not give away reel configuration, mask initiail view
    const symbolIndices = Object.keys(data.game.default.gameConfig.symbols);
    for (let i = 0; i < data.game.default.gameConfig.reelConfig.NumberOfReels; i++) {
      const columnSize = parseInt(data.game.default.gameConfig.viewZone.toString().charAt(0));
      const reelArray = [];
      for (let j = 0; j < columnSize; j += 1) {
        reelArray.push(symbolIndices[Math.floor(Math.random() * symbolIndices.length)]);
      }
      result.game.reels[`Reel${i + 1}`] = reelArray;
    }
    result.game.payArray = data.game.default.gameConfig.payLines.PayArray;

    result.game.payTable = data.game.default.gameConfig.payTable;

    result.game.bigWins = data.game.default.gameConfig.bigWins;
    result.game.symbols = data.game.default.gameConfig.symbols;
    result.game.stage = data.game.default.gameConfig.stage;
    result.game.version = data.game.default.gameConfig.versionName;
    result.userDetails = data.userDetails;
    result.userDetails.selectablePaylines = data.game.default.gameConfig.selectablePaylines.options;

    result.userDetails.currentBet = data.userDetails.game.settings.bets[0] || 5,
    result.userDetails.currentLines = parseInt(data.game.default.gameConfig.selectablePaylines.options[data.game.default.gameConfig.selectablePaylines.options.length - 1]) || 20;
    result.userDetails.totalBet = (result.userDetails.currentBet * result.userDetails.currentLines);

    return new Promise((resolve, reject) => {
      gameSession.getUserSession(token, gameId)
      .then((session) => {
        result.userDetails.currentBet = session.eventData.currentBet;
        result.userDetails.currentLines = session.eventData.currentLines;
        result.userDetails.freeSpinData = session.eventData.freeSpinData;
        result.userDetails.totalBet = (result.userDetails.currentBet * result.userDetails.currentLines);

        // build for bonus if available
        if (session.eventData.bonus && !_.isEmpty(session.eventData.bonus)) {
          result.bonus = {};
          result.bonus.stage = {};
          result.bonus.stage.currentStage = session.eventData.bonus.stage.currentStage;

          if (session.eventData.bonus.stage.currentStage === 'pickDragons') {
            const cardsData = [];
            const cards = Object.keys(session.eventData.bonus.stage.bonusSessionCards);
            cards.forEach((cardId) => {
              cardsData.push({ cardId });
            });
            result.bonus.stage.cards = cardsData;
          } else {
            result.bonus.stage.currentPick = session.eventData.bonus.stage.currentPick;
            result.bonus.stage.dragonPicks = session.eventData.bonus.stage.dragonPicks;
            result.bonus.stage.pickedCard = session.eventData.bonus.stage.pickedCard;
            result.bonus.stage.shuffledCards = session.eventData.bonus.stage.shuffledCards;
            result.bonus.stage.cardsStatus = {};

            session.eventData.bonus.stage.shuffledCards.forEach((card) => {
              result.bonus.stage.cardsStatus[card] = {};
              result.bonus.stage.cardsStatus[card].isOpened = session.eventData.bonus.stage.bonusSessionCards[card].isOpened;
              result.bonus.stage.cardsStatus[card].prizeWon = session.eventData.bonus.stage.bonusSessionCards[card].prizeWon;
              if (session.eventData.bonus.stage.bonusSessionCards[card].isOpened) {
                result.bonus.stage.cardsStatus[card].isDragon = session.eventData.bonus.stage.bonusSessionCards[card].isDragon;
              }
            });
          }
        }
        resolve(result);
      }).catch(() => {
        result.userDetails.currentBet = data.userDetails.game.settings.bets[0] || 5,
        result.userDetails.currentLines = parseInt(data.game.default.gameConfig.selectablePaylines.options[data.game.default.gameConfig.selectablePaylines.options.length - 1]) || 20;
        result.userDetails.freeSpinData = {};
        result.userDetails.totalBet = (result.userDetails.currentBet * result.userDetails.currentLines);
        resolve(result);
      });
    });
  }
}

module.exports = new KnightAndDragons();
