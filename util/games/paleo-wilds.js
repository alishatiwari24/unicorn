const { Compute } = require('../compute');
const nj = require('numjs');
const rng = require('../rng');
const _ = require('lodash');
const moment = require('moment');
const gameSession = require('../gamesession');
const logger = require('../../helpers/firehose');
const Game = require('../../helpers/gamefunction');
const { bucket } = require('../../helpers/couchbase');
const gameEvents = require('../../helpers/game-events');
const { TTL } = require('../../response/constants');

/**
 * Special features related to paleo wilds is implemented in this class
 * This class handles main game computation and logic for expanding feature during free spin
 */
class PaleoWilds extends Compute {


  /**
   * [Handles game click event for paleo wilds]
   * @param  gameId   paleo wilds game id
   * @param  token    user token
   * @param  clientID client id generated on the client side
   * @param  data     user related data fetched from whow
   * @param  gameData game configuratin related data
   */
  handleGameClick(gameId, token, clientID, data, gameData) {
    bucket.get(`${data.userDetails.user.id}::${gameId}`, (err, result) => {
      if (!err) {
        if (result === null) {
          this.createPaleoWildSession(gameId, clientID, data, gameData);
        } else {
          result.value.clientID = clientID;
          if (result.value.eventData.showBetWarning === undefined) {
            result.value.eventData.showBetWarning = true;
          }
          gameSession.buildUserSessionObject(result, data, gameData);
          super.updateSession(result.value, gameId);
        }
      } else {
        this.createPaleoWildSession(gameId, clientID, data, gameData);
      }
    });
    this.createUserDataAndSessionRequest(data, gameId, token);
  }

  /**
   * This function cerates userData object and sesion request object
   * user data contains user's data, retrieved from whow
   */
  createUserDataAndSessionRequest(data, gameId, token) {
    const userData = JSON.parse(JSON.stringify(data.userDetails.user));
    userData.created_at = new Date().getTime();
    userData.updated_at = new Date().getTime();
    userData._type = 'UserData';
    bucket.upsert(token, userData, { expiry: TTL.USER_TOKEN }, (err) => {
      if (err) {
        console.log(err);
      }
    });

    const requestDoc = { _type: 'sessionRequest', status: 'init' };
    bucket.upsert(`${data.userDetails.user.id}::${gameId}::sessionRequest`, requestDoc, (err) => {
      if (err) {
        console.log(err);
      }
    });
  }


  /**
   * This function cerates user session in database
   * it initializes bet array, bet lines forming initial bet
   * for knight and dragons game
   */
  createPaleoWildSession(gameId, clientID, data, gameData) {
    const doc = {
      currentBet: data.userDetails.game.settings.bets[0] || 5,
      currentLines: parseInt(gameData.game.default.gameConfig.selectablePaylines.options[gameData.game.default.gameConfig.selectablePaylines.options.length - 1]) || 20,
      currentReelSet: 0,
      userDetails: data.userDetails.user,
      freeSpinData: {},
      showBetWarning: true
    };
    bucket.upsert(`${data.userDetails.user.id}::${gameId}`, {
      _type: 'userSession',
      event: 'gameClick',
      clientID,
      eventData: doc,
      game: data.userDetails.game,
      _updatedTimestamp: moment.utc()
    }, (err, result) => {
      if (err) {
        console.log(err);
      }
    });
  }


  /**
   * This function fetches user's current session for a game
   * session data includes current bet amount, current pay lines
   * and user profile related informations
   * @param  token  user token
   * @param  gameId paleo wilds game id
   * @return        a user session schema
   */
  getUserSession(token, gameId) {
    return new Promise((resolve, reject) => {
      bucket.get(token, (err, result) => {
        if (!err) {
          bucket.get(`${result.value.id}::${gameId}`, result, (in_err, get_result) => {
            if (!in_err) {
              resolve(get_result.value);
            } else {
              reject();
            }
          });
        } else {
          reject(err);
        }
      });
    });
  }

  /**
  * updates user's current bet in database
  */
  updateUserCurrentBet(result, bet, gameId) {
    return new Promise((resolve, reject) => {
      let reelSet = 5;
      if (result.eventData.previousBet === bet) {
        reelSet = result.eventData.previousReelSet;
      }
      result.eventData.currentBet = bet;
      result.eventData.currentReelSet = reelSet;
      bucket.upsert(`${result.eventData.userDetails.id}::${gameId}`, result, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }


  /**
  * This function is responsible to update showBetWarning flag for paleo wild game
  */
  doNotShowPopUp(result, gameId) {
    return new Promise((resolve, reject) => {
      result.eventData.showBetWarning = false;
      bucket.upsert(`${result.eventData.userDetails.id}::${gameId}`, result, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }


  /**
  * This function is responsible to calculate result for a spin event
  * taking into account
  * view zone, pay lines, pay table
  */
  compute(game, gameId, token, session) {
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));

    // start calculating view zone
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);

    // random numbers are fetched from reel configuration
    const randomNumber = rng.generateRandomNumber(game.default.gameConfig.reelConfig);
    // view zone is calculated below
    const expanding = [];
    const scatter = [];
    super.generateViewZone(randomNumber, noofReels, columnSize, expanding, scatter, game, arrays, session);
    const deepCopyArray = arrays.clone();

    // load pay arrays
    const payArray = game.default.gameConfig.payLines.normalizedPayArray;

    // result based on view zone is calculated below
    const results = [];

    return new Promise((resolve, reject) => {
      super.freeSpinCompute(scatter, results, game)
      .then((results) => {
        resolve(this.computeSpinWins(game, arrays, deepCopyArray, results, payArray, columnSize));
      });
    });
  }


  /**
   * This function decides if respin is triggered or not
   * pick a random value from reSpinWeight array, id it's a 1, trigger respin
   */
  makeReSpinTriggerDecision(reSpinWeight) {
    return (reSpinWeight[Math.floor(Math.random() * reSpinWeight.length)] === 1);
  }


  /**
   * This function computes results when free spins are not active
   * Wild and scatter symbols are considered when computing
   */
  computeSpinWins(game, arrays, deepCopyArray, results, payArray, columnSize) {
    return new Promise((resolve, ) => {
      Promise.all([
        super.computeWins(arrays, columnSize, payArray, game, results),
        super.scatterPayCompute(arrays, columnSize, payArray, game, results),
        super.wildPayCompute(deepCopyArray, columnSize, payArray, game, results)
      ]).then(() => {
        resolve({ viewZone: arrays, results });
      }).catch(() => {
        resolve({ error: 'error in view zone' });
      });
    });
  }


  /**
  * updates user game session with details of a spin result
  * it may include free spin if won in game spin
  * and updated wallet chips as a result of whow api call bet/play
  */
  gameSpin(gameId, token, docDetails, gameData) {
    docDetails._updatedTimestamp = moment.utc();
    this.getUserSession(token, gameId)
    .then((session) => {
      session.gamble = docDetails.gamble;
      session.game = docDetails.game;
      session.eventData.userDetails.wallet = docDetails.wallet;

      const updateReelSet = true;
      if (!_.isEmpty(session)) {
        session.event = 'spin';
        if (!_.isEmpty(docDetails.freeSpinData) && docDetails.freeSpinData.isFreeSpinAwarded) {
          delete docDetails.freeSpinData.isFreeSpinAwarded;
          delete docDetails.freeSpinData.noofFreeSpinsAwarded;
        }
        session.eventData.freeSpinData = docDetails.freeSpinData;
        if (session.eventData.freeSpinData && session.eventData.freeSpinData.currentFreeSpin && session.eventData.freeSpinData.currentFreeSpin == session.eventData.freeSpinData.freeSpins) {
          const totalWin = session.eventData.freeSpinData.winAmount;
          session.gamble.winAmount = totalWin;
          session.eventData.freeSpinData = {};
        }
      }

      if (updateReelSet) {
        if (_.isEmpty(session.eventData.freeSpinData)) {
          session.eventData.currentReelSet = this.getMainGameReelSet(session, gameData);
        } else {
          session.eventData.freeSpinData.currentReelSet = this.getFreeGameReelSet(session.eventData.freeSpinData.currentReelSet, gameData);
        }
      }

      session.eventData.previousBet = session.eventData.currentBet;
      session.eventData.previousReelSet = session.eventData.currentReelSet;
      const sessionRequestKey = docDetails.session.sessionRequestKey;
      const sessionRequest = docDetails.session.sessionRequest;
      sessionRequest.status = 'ready';
      sessionRequest.spinCompletedTimestamp = moment().utc();
      this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);
      super.updateSession(session, gameId);
    }).catch((err) => {
      console.log(err);
    });
  }


  /**
 * getting reel configuration when spin is done on paleo wilds
   * with every spin a new reel configuration is picked from game schema
   * @param  session sessoin schema
   * @param  game    game configuration schema
   * @return         reel configuration to spin on
   */
  getSpinReelConfiguration(session, game) {
    if (_.isEmpty(session.eventData.freeSpinData)) {
      return this.getMainGameSpinReelConfiguration(session, game);
    }
    return this.getFreeGameSpinReelConfiguration(session, game);
  }


  /**
   * fetches next index from game schema which is to be considered for spin when there are no free spins
   * user's current session is to be considered here
   * @param  session  session schema
   * @param  gameData game schema
   * @return          next index of game schema
   */
  getMainGameReelSet(session, gameData) {
    let nextConfig = session.eventData.currentReelSet - 1;
    if (nextConfig < 0) {
      nextConfig = gameData.game.default.gameConfig.reelConfig.NumberOfReels - 1;
    }
    return nextConfig;
  }


  /**
   * fetches next index from game schema which is to be considered for spin when free spin is active
   * user's current session is to be considered here
   * @param  session  session schema
   * @param  gameData game schema
   * @return          next index of game schema
   */
  getFreeGameReelSet(reelSet, gameData) {
    let nextConfig = reelSet - 1;
    if (nextConfig < 0) {
      nextConfig = gameData.game.default.gameConfig.reelConfig.NumberOfReels - 1;
    }
    return nextConfig;
  }


  /**
   * fetches the reel configuration for the spin when free spin are not active
   * @param  session  session schema
   * @param  gameData game schema
   * @return          reel configuration to bet on in a spin
   */
  getMainGameSpinReelConfiguration(session, game) {
    let nextConfig = session.eventData.currentReelSet - 1;
    if (nextConfig < 0) {
      nextConfig = game.game.default.gameConfig.reelConfig.NumberOfReels - 1;
    }
    const reelConfig = game.game.default.gameConfig.reelConfig;
    reelConfig[`Reel${nextConfig + 1}`] = game.game.default.gameConfig.specialFeatures.data.Reel;

    return { reelConfig, wildReel: `reel${nextConfig + 1}` };
  }


  /**
   * fetches the reel configuration for the spin when free spin are active
   * @param  session  session schema
   * @param  gameData game schema
   * @return          reel configuration to bet on in a spin
   */
  getFreeGameSpinReelConfiguration(session, game) {
    // reel set 1
    let nextConfig = session.eventData.freeSpinData.currentReelSet - 1;
    if (nextConfig < 0) {
      nextConfig = game.game.default.gameConfig.reelConfig.NumberOfReels - 1;
    }
    const reelConfig = game.game.default.gameConfig.freeSpin.reelConfig;
    reelConfig[`Reel${nextConfig + 1}`] = game.game.default.gameConfig.specialFeatures.data.Reel;

    return { reelConfig, wildReel: `reel${nextConfig + 1}` };
  }


  /**
  * This function is responsible to generate spin response
  * it includes view zone, wins, user details like wallet, bet array etc
  * necessary render view for the user
  */
  generateSpinResponse(obj, computeData, token, sessionRequestKey, sessionRequest, session) {
    // determine view port
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));

    // create reels for view zone
    const array = computeData.viewZone;
    const viewZone = {};

    for (let i = 0; i < noofReels; i += 1) {
      const line = [];
      for (let j = 0; j < columnSize; j += 1) {
        line.push({ symbolId: array.get(j, i), symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType, symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName });
      }
      viewZone[`Reel${i + 1}`] = line;
    }
    viewZone.viewZone = obj.game.default.gameConfig.viewZone;


    // create win document
    const result = {};
    result.viewZone = viewZone;
    result.wins = {};
    result.freeSpinData = {};
    result.levelData = {};
    result.wasReSpin = computeData.wasReSpin;
    return new Promise((resolve) => {
        // if free spin is present
      if (computeData.results.freeSpins) {
        if (!_.isEmpty(session)) {
          if (_.isEmpty(session.eventData.freeSpinData)) {
            result.freeSpinData.freeSpins = computeData.results.freeSpins;
            result.freeSpinData.pickedSymbol = computeData.pickedSymbol;
            result.freeSpinData.currentFreeSpin = 0;
            result.freeSpinData.winAmount = 0;

            session.eventData.freeSpinData = result.freeSpinData;
            session.eventData.freeSpinData.currentReelSet = 5;
            result.isSessionUpdated = true;
          } else {
            result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;

            session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
            session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
          }
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
          if (session.eventData.freeSpinData.isFreeSpinAwarded) {
            delete session.eventData.freeSpinData.isFreeSpinAwarded;
            delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
          }
          session.event = 'spin';
          session._updatedTimestamp = moment.utc();

          // const cas = super.map.get(`${session.eventData.userDetails.id}::${obj._id}`) || null;
          super.updateSession(session, obj._id);
        }
        // result.isSessionUpdated = true;
      }

      result.wildReel = computeData.wildReel;
        // total win amount calculated based on wins from pay line
      let total = 0;
      computeData.results.forEach((resultObj) => {
        resultObj.winAmount = resultObj.multiplier * session.eventData.currentBet;
        if (!_.isEmpty(session.eventData.freeSpinData) && session.eventData.freeSpinData.currentFreeSpin && session.eventData.freeSpinData.currentFreeSpin > 0) {
          resultObj.winAmount *= obj.game.default.gameConfig.specialFeatures.freeSpinMultiplier;
        }
        total += (resultObj.multiplier * session.eventData.currentBet);
        delete resultObj.multiplier;
      });


      result.wins.winCoins = total;
      result.wins.winLinesCount = computeData.results.length;
      result.wins.winLines = JSON.parse(JSON.stringify(computeData.results));

      if (result.wins.winLines.freeSpins) {
        delete result.wins.winLines.freeSpins;
      }

        // put user session data
      result.session = {};
      result.session.sessionRequestKey = sessionRequestKey;
      result.session.sessionRequest = sessionRequest;
      /**
       * notify of all the symbols present in view zone
       */
      gameEvents.notifyGameEvents(obj, token, session, result, computeData.results.freeSpins);

      // mask view zone here
      result.viewZone = super.maskViewZone(result.viewZone, obj);
      if (_.isEmpty(session.eventData.freeSpinData)) {
          // no free spins
        resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
          // some free spins remaining
        result.freeSpinData = session.eventData.freeSpinData;

        if (computeData.results.freeSpins) {
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
        }

        if (!computeData.results.freeSpins) {
          result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
        }

        if (result.wins.winCoins > 0) {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
          session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
        } else {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
        }
          // call whow api
        if (session.eventData.freeSpinData.currentFreeSpin === 0) {
          // free spins just awarded
          resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
        } else {
          resolve(super.callWhowBluehare(token, result, computeData, 0, 10, total, session, sessionRequestKey, sessionRequest, obj._id));
        }
      } else {
          // free spin object exists and there are no free spins remaining
        resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      }
    });
  }


  /**
  * This function models the spin result document based on the analytics requirement
  * it deletes the fields not required and fetches the field required from database
  * logging for secret of amun is customized
  */
  logSpinResult(result, token, gameId) {
    if (result && token && gameId) {
      delete result.levelData;
      delete result.game;
      delete result.jackpot;
      delete result.session;
      delete result.viewZone;
      result.gameId = gameId;
      const gameData = Game.getGame(gameId);
      result.baseGameId = gameData.game.default.baseGameId;
      result.gameVersion = gameData.game.default.versionName;
      result.stage = gameData.game.default.stage;

      /**
      * removes un necessary data from while logging spin results
      */
      const symbols = {};
      const types = {};
      if (result.wins.winLines.length > 0) {
        result.wins.winLines.forEach((line) => {
          if (!types[line.winType]) {
            types[line.winType] = 0;
          }
          if (!symbols[line.symbolId]) {
            symbols[line.symbolId] = 0;
          }
          types[line.winType]++;
          symbols[line.symbolId]++;
        });
        result.wins.types = types;
        result.wins.symbols = symbols;
      }
      if (result.wins.expandedWins && result.wins.expandedWins.expandedWinLines && result.wins.expandedWins.expandedWinLines.length > 0) {
        result.wins.expandedWins.expandedWinLines.forEach((line) => {
          if (!types[line.winType]) {
            types[line.winType] = 0;
          }
          if (!symbols[line.symbolId]) {
            symbols[line.symbolId] = 0;
          }
          types[line.winType]++;
          symbols[line.symbolId]++;
        });
        result.wins.types = types;
        result.wins.symbols = symbols;
      }
      this.getUserSession(token, gameId)
      .then((session) => {
        result.user = {};
        result.user.currentBet = session.eventData.currentBet;
        result.user.currentLines = session.eventData.currentLines;
        result.user.totalBet = session.eventData.currentBet * session.eventData.currentLines;
        result.user.token = token;
        result.user.userId = session.eventData.userDetails.id;

        logger.info(result);
      }).catch((err) => {
        console.log(err);
      });
    }
  }

}

module.exports = new PaleoWilds();
