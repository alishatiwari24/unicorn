const nj = require('numjs');
const rng = require('../rng');
const { bucket } = require('../../helpers/couchbase');
const moment = require('moment');
const _ = require('lodash');
const whowapi = require('../../api/whowapi');
const constants = require('../../response/constants');
const { Compute } = require('../compute');
const gameSession = require('../gamesession');
const reelMask = require('../../config/reel-mask-config');
const gameEvents = require('../../helpers/game-events');

/**
 * Qin Dynasty specific features are implemented in this class
 * This class would be reponsible to update game related user sessions
 * This class would be reponsible to trigger re-spin feature of qin dynasty feature
 */
class QinDynasty extends Compute {

  /**
  * This function is responsible to update showBetWarning flag for paleo wild game
  */
  doNotShowPopUp(result, gameId) {
    return new Promise((resolve, reject) => {
      result.eventData.showBetWarning = false;
      bucket.upsert(`${result.eventData.userDetails.id}::${gameId}`, result, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }


  /**
  * updates user's current bet in database
  */
  updateUserCurrentBet(result, bet, gameId) {
    return new Promise((resolve, reject) => {
      let reelSet = 5;
      if (result.eventData.previousBet === bet) {
        reelSet = result.eventData.previousReelSet;
      }
      result.eventData.currentBet = bet;
      result.eventData.currentReelSet = reelSet;
      bucket.upsert(`${result.eventData.userDetails.id}::${gameId}`, result, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }


  /**
  * This function is responsible to get the current reel set for roaming reel
  */
  getCurrentReelSet(session, noofReels) {
    if (!_.isEmpty(session.eventData.freeSpinData)) {
      // is current reel set attribute is not there in fs session, set it as 5 (number of reels)
      if (!session.eventData.freeSpinData.currentReelSet) {
        session.eventData.freeSpinData.currentReelSet = noofReels;
      }
      return session.eventData.freeSpinData.currentReelSet;
    }
    // is current reel set attribute is not there in session, set it as 5 (number of reels)
    if (!session.eventData.currentReelSet) {
      session.eventData.currentReelSet = noofReels;
    }
    // return current reel set for main game
    return session.eventData.currentReelSet;
  }


  /**
  * This function is responsible to update the reel sets, for the next spin
  * @param session - user session object, including eventData (which contains freespin data)
  * @param noofReels - number of reel in the game
  */
  updateReelSetForNextSpin(session, noofReels) {
    if (_.isEmpty(session.eventData.freeSpinData)) {
      session.eventData.currentReelSet -= 1;
      if (session.eventData.currentReelSet === 0) {
        session.eventData.currentReelSet = noofReels;
      }
    } else {
      session.eventData.freeSpinData.currentReelSet -= 1;
      if (session.eventData.freeSpinData.currentReelSet == 0) {
        session.eventData.freeSpinData.currentReelSet = noofReels;
      }
    }
  }


  /*
  * replace reel - currentReelSet as wild reels
  * @param game - game object
  * @param eventData - user session data, containing free spin information
  * @param currentReelSet - currently selected reel set number
  */
  addRoamingReel(game, eventData, currentReelSet) {
    if (_.isEmpty(eventData.freeSpinData)) {
      game.default.gameConfig.reelConfig[`Reel${currentReelSet}`] = game.default.gameConfig.specialFeatures.data.Reel;
    } else {
      game.default.gameConfig.freeSpin.reelConfig[`Reel${currentReelSet}`] = game.default.gameConfig.specialFeatures.data.Reel;
    }
  }


  /**
   * This function decides if respin is triggered or not
   * pick a random value from reSpinWeight array, id it's a 1, trigger respin
   */
  makeReSpinTriggerDecision(reSpinWeight) {
    return (reSpinWeight[Math.floor(Math.random() * reSpinWeight.length)] === 1);
  }


  /**
  * This function is responsible to calculate result for a spin event
  * taking into account
  * view zone, pay lines, pay table
  */
  compute(game, gameId, token, session, updateReel = true) {
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const currentReelSet = this.getCurrentReelSet(session, noofReels);
    if (updateReel) {
      this.updateReelSetForNextSpin(session, noofReels);
      this.addRoamingReel(game, session.eventData, currentReelSet);
    }
    /*
    * start calculating view zone
    */
    let arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    // random numbers are fetched from reel configuration
    const randomNumber = rng.generateRandomNumber(game.default.gameConfig.reelConfig);
    // view zone is calculated below
    let expanding = [];
    const scatter = [];
    super.generateViewZone(randomNumber, noofReels, columnSize, expanding, scatter, game, arrays, session);
    // if expanding wild is detected complete reel is replaced with non expaning symbols each
    const deepCopyArray = arrays.clone();
    expanding = _.uniq(expanding);
    // load pay arrays
    let payArray = game.default.gameConfig.payLines.normalizedPayArray;
    const copyPayArray = JSON.parse(JSON.stringify(payArray));
    // result based on view zone is calculated below
    const results = [];
    return new Promise((resolve) => {
      const lineArrays = game.default.gameConfig.selectablePaylines[session.eventData.currentLines];
      payArray.forEach((pay) => {
        const object = _.find(lineArrays, o => o === pay._id);
        if (!object) {
          _.remove(copyPayArray, arr => arr._id === pay._id);
        }
      });
      payArray = copyPayArray;
      arrays = deepCopyArray.clone();
      Promise.all([
        this.computeWins(arrays, columnSize, payArray, game, results),
        super.freeSpinCompute(scatter, results, game, session),
        this.wildPayCompute(deepCopyArray, columnSize, payArray, game, results)
      ]).then(() => {
        resolve({ viewZone: arrays, results, currentReelSet });
      }).catch((err) => {
        console.log(err);
        resolve({ error: 'error in view zone' });
      });
    });
  }


  /**
  * @wildPayCompute - This function checks if wild present in view zone pays or not
  * if wilds are present and wild pays then wins are calculated subsequentaly
  */
  wildPayCompute(arrays, columnSize, payArray, game, results) {
    return new Promise((resolve, reject) => {
      const wildSymbolId = _.findKey(game.default.gameConfig.symbols, { SymbolType: 'Wild' });
      const scatterSymbolId = _.findKey(game.default.gameConfig.symbols, { SymbolType: 'Scatter' });
      let lastSymbolId;
      const j = 0;
      for (let i = 0; i < columnSize; i += 1) {
        const symbolId = arrays.get(i, j);
        lastSymbolId = symbolId;
        if (symbolId !== wildSymbolId) {
          break;
        }
        for (let k = 0; k < payArray.length; k += 1) {
          const payLine = payArray[k].data;
          let ofAKind = 0;
          if (payLine[0] === i) {
            ofAKind += 1;
            for (let l = 1; l < payLine.length; l += 1) {
              if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType !== 'Wild') {
                lastSymbolId = arrays.get(payLine[l], l);
                break;
              }
              ofAKind += 1;
            }
            if (
              ofAKind > 0
              && (game.default.gameConfig.payTable[symbolId][ofAKind] || game.default.gameConfig.payTable[wildSymbolId][ofAKind])
            ) {
              if (
                (lastSymbolId === wildSymbolId || lastSymbolId === scatterSymbolId)
                && game.default.gameConfig.payTable[wildSymbolId][ofAKind]
              ) {
                results.push({ symbolId: wildSymbolId, winType: `${ofAKind} of a kind`, winLineId: payArray[k]._id, multiplier: game.default.gameConfig.payTable[symbolId][ofAKind].Multiplier });
              } else {
                results.push({ symbolId: lastSymbolId, winType: `${ofAKind + 1} of a kind`, winLineId: payArray[k]._id, multiplier: game.default.gameConfig.payTable[lastSymbolId][ofAKind + 1].Multiplier });
              }
            }
          }
        }
      }
      resolve(results);
    });
  }


  /**
  * This function computes wins based on view zone
  * win is calculated starting from left i.e. from 1st reel
  * Wilds and scatter are taken into consideration while computing wins
  */
  computeWins(arrays, columnSize, payArray, game, results) {
    return new Promise((resolve, reject) => {
      const j = 0;
      for (let i = 0; i < columnSize; i += 1) {
        for (let k = 0; k < payArray.length; k += 1) {
          let symbolId = arrays.get(i, j);
          const payLine = payArray[k].data;
          let ofAKind = 0;
          if (payLine[0] === i) {
            ofAKind += 1;
            for (let l = 1; l < payLine.length; l += 1) {
              if (game.default.gameConfig.symbols[symbolId].SymbolType === 'Wild' && game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType !== 'Scatter' && !game.default.gameConfig.payTable[symbolId][ofAKind]) {
                symbolId = arrays.get(payLine[l], l);
                ofAKind += 1;
                continue;
              }
              if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType !== 'Wild' && arrays.get(payLine[l], l) !== symbolId) {
                break;
              }
              ofAKind += 1;
            }
            if (ofAKind > 0 && game.default.gameConfig.payTable[symbolId][ofAKind] && game.default.gameConfig.symbols[symbolId].SymbolType !== 'Wild' && game.default.gameConfig.symbols[symbolId].SymbolType !== 'Scatter') {
              results.push({ symbolId, winType: `${ofAKind} of a kind`, winLineId: payArray[k]._id, multiplier: game.default.gameConfig.payTable[symbolId][ofAKind].Multiplier });
            }
          }
          ofAKind = [];
        }
      }
      resolve(results);
    });
  }


  /**
   * update user session data
   * @param session - user's session data
   * @param docDetails - generated winning data, from spin result
   * it returns sessionRequest, which will be updated to database after execution
   */
  updateSessionData(session, docDetails) {
    session.gamble = docDetails.gamble;
    session.game = docDetails.game;
    session.eventData.userDetails.wallet = docDetails.wallet;
    if (!_.isEmpty(session)) {
      session.eventData.currentReelSet = docDetails.currentReelSet;
      session.event = 'spin';
      if (!_.isEmpty(docDetails.freeSpinData) && docDetails.freeSpinData.isFreeSpinAwarded) {
        delete docDetails.freeSpinData.isFreeSpinAwarded;
        delete docDetails.freeSpinData.noofFreeSpinsAwarded;
      }
      session.eventData.freeSpinData = docDetails.freeSpinData;
      if (session.eventData.freeSpinData.currentFreeSpin && session.eventData.freeSpinData.currentFreeSpin == session.eventData.freeSpinData.freeSpins) {
        session.eventData.freeSpinData = {};
      }
    }
    session.eventData.previousBet = session.eventData.currentBet;
    session.eventData.previousReelSet = session.eventData.currentReelSet;
    const sessionRequest = docDetails.session.sessionRequest;
    sessionRequest.status = 'ready';
    sessionRequest.spinCompletedTimestamp = moment().utc();
    return sessionRequest;
  }


  /**
   * update user session when there are no free spins in qin dynasty
   * @param  gameId     qin dynasty game id
   * @param  token      user token
   * @param  docDetails user session schema
   * @param  gameData   game configuration schema
   */
  gameSessionUpdateMainGame(gameId, token, docDetails) {
    docDetails._updatedTimestamp = moment.utc();
    gameSession.getUserSession(token, gameId)
    .then((session) => {
      const sessionRequest = this.updateSessionData(session, docDetails);
      this.updateSessionAndSessionRequest(session, gameId, docDetails.session.sessionRequestKey, sessionRequest);
    }).catch((err) => {
      console.log(err);
    });
  }


  /**
  * This function is responsible to generate spin response
  * it includes view zone, wins, user details like wallet, bet array etc
  * necessary render view for the user
  */
  generateSpinResponse(obj, computeData, token, sessionRequestKey, sessionRequest, session) {
    // determine view port
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));

    // create reels for view zone
    const array = computeData.viewZone;
    const viewZone = {};

    for (let i = 0; i < noofReels; i += 1) {
      const line = [];
      for (let j = 0; j < columnSize; j += 1) {
        line.push({ symbolId: array.get(j, i), symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType, symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName });
      }
      viewZone[`Reel${i + 1}`] = line;
    }
    viewZone.viewZone = obj.game.default.gameConfig.viewZone;


    // create win document
    const result = {};
    result.viewZone = viewZone;
    result.wins = {};
    result.freeSpinData = {};
    result.levelData = {};
    result.noofReels = noofReels;
    result.currentReelSet = session.eventData.currentReelSet;
    result.wasReSpin = computeData.wasReSpin;
    return new Promise((resolve, reject) => {
      // if free spin is present
      if (computeData.results.freeSpins) {
        if (!_.isEmpty(session)) {
          if (_.isEmpty(session.eventData.freeSpinData)) {
            result.freeSpinData.freeSpins = computeData.results.freeSpins;
            result.freeSpinData.currentFreeSpin = 0;
            result.freeSpinData.winAmount = 0;
            result.freeSpinData.currentReelSet = noofReels;
            session.eventData.freeSpinData = result.freeSpinData;
          } else {
            result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
            session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
            result.freeSpinData.currentReelSet = session.eventData.freeSpinData.currentReelSet;
            session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
            computeData.results.forEach((winLineResult) => {
              session.eventData.freeSpinData.winAmount += (winLineResult.multiplier * (session.eventData.currentBet));
            });
          }
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
          if (session.eventData.freeSpinData.isFreeSpinAwarded) {
            delete session.eventData.freeSpinData.isFreeSpinAwarded;
            delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
          }
          session.event = 'spin';
          session._updatedTimestamp = moment.utc();
          super.updateSession(session, obj._id);
        }
        result.isSessionUpdated = true;
      }

      // total win amount calculated based on wins from pay line
      let total = 0;
      computeData.results.forEach((result) => {
        result.winAmount = result.multiplier * (session.eventData.currentBet);
        if (!_.isEmpty(session.eventData.freeSpinData) && session.eventData.freeSpinData.currentFreeSpin > 0) {
          result.winAmount *= (obj.game.default.gameConfig.specialFeatures.freeSpinMultiplier || 1);
        }
        total += result.winAmount;
        delete result.multiplier;
      });
      result.wins.winCoins = total;
      result.wins.winLinesCount = computeData.results.length;
      result.wins.winLines = [];

      computeData.results.forEach(row => result.wins.winLines.push(row));

      // put user session data
      result.session = {};
      result.session.sessionRequestKey = sessionRequestKey;
      result.session.sessionRequest = sessionRequest;
      /**
       * notify of all the symbols present in view zone
       */
      gameEvents.notifyGameEvents(obj, token, session, result, computeData.results.freeSpins);

      // mask view zone here
      result.viewZone = this.maskViewZone(result.viewZone, obj, _.isEmpty(session.eventData.freeSpinData));
      if (_.isEmpty(session.eventData.freeSpinData)) {
        resolve(this.callPlay(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
        // some free spins remaining
        result.freeSpinData = session.eventData.freeSpinData;
        result.freeSpinData.currentReelSet = session.eventData.freeSpinData.currentReelSet;
        if (computeData.results.freeSpins) {
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
        }

        if (!computeData.results.freeSpins) {
          result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
        }

        if (result.wins.winCoins > 0) {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
          session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
        } else {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
        }

        // call whow api
        if (session.eventData.freeSpinData.currentFreeSpin !== 0) {
          resolve(this.callPlay(token, result, computeData, 0, 10, total, session, sessionRequestKey, sessionRequest, obj._id));
        } else {
          resolve(this.callPlay(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
        }
      } else {
        result.freeSpinData.currentReelSet = session.eventData.freeSpinData.currentReelSet;
        // free spin object exists and there are no free spins remaining
        resolve(this.callPlay(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      }
    });
  }


  /**
   * This function is reponsilbe to mask the view zone for animation and security purpose
   * A initial configuration data is read and all reels are masked randomly from initial configuration
   */
  maskViewZone(viewZone, obj, isMainGame) {
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));
    const maskedReels = {};
    const reelConfig = (isMainGame) ? obj.game.default.gameConfig.reelConfig : obj.game.default.gameConfig.freeSpin.reelConfig;

    // construct fake reel with random symbols
    let reelSize = reelMask.INITIAL_REEL_SIZE;
    for (let i = 0; i < noofReels; i += 1) {
      if (i > 0) {
        reelSize += reelMask.REEL_SIZE_INCREMENT;
      }
      const symbolIndices = Object.keys(reelConfig[`Reel${i + 1}`].SymbolDistribution);
      const reelArray = [];
      for (let j = 0; j < reelSize; j += 1) {
        reelArray.push(reelConfig[`Reel${i + 1}`].SymbolDistribution[Math.floor(Math.random() * symbolIndices.length)].SymbolId);
      }

      // put view zone inside masked reel array
      const randomReelLocation = Math.floor(Math.random() * reelSize);
      for (let j = 0; j < columnSize; j += 1) {
        const reel = viewZone[`Reel${i + 1}`];
        const actualReelLocation = (randomReelLocation + j) % reelSize;
        reelArray[actualReelLocation] = reel[j].symbolId;
      }
      maskedReels[`Reel${i + 1}`] = { symbols: reelArray, stopAt: (randomReelLocation % reelSize) };
    }
    maskedReels.viewZone = viewZone.viewZone;
    return maskedReels;
  }


  /**
   * This is the actual whow api call function
   * play api is called in each spin, informing whow, win amount of spin
   * virtualAmount is specified as non-zero, when the free spins are going on
   */
  callPlay(token, result, computeData, betAmount, virtualAmount, total, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve, reject) => {
      whowapi.play(token, {
        betAmount,
        winAmount: total,
        virtualAmount
      }).then((response) => {
        response = JSON.parse(response);
        if (response.status && response.status === 110) {
          sessionRequest.status = 'ready';
          if (session.gamble && !_.isEmpty(session.gamble)) {
            session.gamble.winAmount = 0;
            session.gamble.count = 0;
          }
          session.eventData.freeSpinData = {};
          session.eventData.currentReelSet += 1;
          if (session.eventData.currentReelSet > result.noofReels) {
            session.eventData.currentReelSet = 1;
          }
          this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);
          resolve({ error: constants.RES_MESSAGES.NOT_ENOUGH_BALANCE, errorCode: constants.RES_ERROR_CODES.NOT_ENOUGH_BALANCE });
        } else {
          result.gamble = response.payload.round || {};
          if (!session.gamble || !session.gamble.history) {
            result.gamble.history = [];
          } else {
            result.gamble.history = session.gamble.history;
          }
          result.gamble.count = 0;
          result.levelData.level = response.payload.user.level;
          result.levelData.levelProgress = response.payload.user.levelProgress;
          result.wallet = response.payload.user.wallet;
          result.game = response.payload.game;
          result.game.betAmount = (session.eventData.currentBet * session.eventData.currentLines);
          result.jackpot = response.payload.user.jackpotData;
          resolve(result);
        }
      }).catch((err) => {
        console.log(err);
        resolve(result);
      });
    });
  }
}

module.exports = new QinDynasty();
