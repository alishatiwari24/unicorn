/* eslint-disable max-statements, max-params, prefer-destructuring, no-ternary, multiline-ternary, no-console */
const nj = require('numjs');
const rng = require('../rng');
const { bucket } = require('../../helpers/couchbase');
const moment = require('moment');
const _ = require('lodash');
const whowapi = require('../../api/whowapi');
const constants = require('../../response/constants');
const { Compute } = require('../compute');
const gameSession = require('../gamesession');
const gameEvents = require('../../helpers/game-events');

/**
 * Noble Musketeers specific features are implemented in this class
 * This class would be reponsible to update game related user sessions
 * This class would be reponsible to compute for anyway win
 */
class NobleMusketeers extends Compute {

  /**
   * [Handles game click event for noble musketeers]
   * @param  gameId   noble musketeers game id
   * @param  token    user token
   * @param  clientID client id generated on the client side
   * @param  data     user related data fetched from whow
   * @param  gameData game configuratin related data
   */
  handleGameClick(gameId, token, clientID, data, gameData) {
    bucket.get(`${data.userDetails.user.id}::${gameId}`, (err, result) => {
      if (err || !result) {
        gameSession.createGameSessionObject(gameId, clientID, data, gameData, constants.NOBLE_MUSKETEERS.NOOF_LINES);
      } else {
        result.value.clientID = clientID;
        gameSession.buildUserSessionObject(result, data, gameData, constants.NOBLE_MUSKETEERS.NOOF_LINES);
        super.updateSession(result.value, gameId);
      }
    });
    gameSession.createUserDataAndSessionRequest(data, gameId, token);
  }

  /**
  * This function is used to build response for game click event
  * game data along with user data for subsequent  game events calls are passed to client
  */
  generateGameClickResponse(data, token, gameId) {
    const result = {};
    result.game = super.buildGameObjForGameClick(data);
    result.game.freeSpinWins = data.game.default.gameConfig.freeSpin.freeSpinWins;
    result.userDetails = super.buildUserDetailsForGameClick(data);

    return new Promise((resolve) => {
      gameSession.getUserSession(token, gameId)
      .then((session) => {
        result.userDetails = super.buildGameSessionGameClick(result.userDetails, data, session);
        resolve(result);
      }).catch(() => {
        result.userDetails = super.buildGameSessionGameClick(result.userDetails, data);
        resolve(result);
      });
    });
  }


  /**
  * This function is responsible to calculate result for a spin event
  * taking into account
  * view zone, pay lines, pay table
  */
  compute(game, gameId, token, session) {
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    /*
    * start calculating view zone
    */
    let arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);

    // random numbers are fetched from reel configuration
    const randomNumber = rng.generateRandomNumber(game.default.gameConfig.reelConfig);
    // view zone is calculated below
    const scatter = [];
    super.generateViewZone(randomNumber, noofReels, columnSize, [], scatter, game, arrays, session);

    // if expanding wild is detected complete reel is replaced with non expaning symbols each
    const deepCopyArray = arrays.clone();
    // result based on view zone is calculated below
    const results = [];

    return new Promise((resolve) => {
      arrays = deepCopyArray.clone();
      Promise.all([
        this.computeWins(arrays, columnSize, game, results),
        this.freeSpinCompute(scatter, results, game)
      ]).then(() => {
        resolve({ viewZone: arrays, results });
      }).catch((err) => {
        console.log(err);
        resolve({ error: 'error in view zone' });
      });
    });
  }


  /**
  * This function checks if there are any free spins to be awarded based on view zone
  * If there are free spins than the number of free spins are determined based on game configuration
  */
  freeSpinCompute(scatter, results, game) {
    return new Promise((resolve) => {
      if (scatter.length >= game.default.gameConfig.freeSpin.minimumRequiredCount && game.default.gameConfig.freeSpin.freeSpinWins[scatter.length]) {
        results.freeSpins = game.default.gameConfig.freeSpin.freeSpinWins[scatter.length].noofFS;
        // shuffle the multipliers, and add them to session (in bonusData)
        results.freeSpinMultipliers = _.shuffle(JSON.parse(JSON.stringify(game.default.gameConfig.playerPicks.multipliers)));
        results.isSymbolPicked = false;
      } else if (scatter.length === 2 && scatter[0] === 0 && scatter[1] === (game.default.gameConfig.reelConfig.NumberOfReels - 1)) {
        // pick one table, from given table list
        const tableIndex = Math.floor(Math.random() * game.default.gameConfig.playerPicks.tables.length);
        // and get options to be selected, shuffle them and store them in bonusData inside session
        results.freeSpinMultipliers = _.shuffle(game.default.gameConfig.playerPicks.tables[tableIndex].values);
        results.isSymbolPicked = false;
      }
      resolve(results);
    });
  }


  /**
  * This function computes wins based on view zone
  * win is calculated starting from left i.e. from 1st reel
  * Wilds and scatter are taken into consideration while computing wins
  */
  computeWins(arrays, columnSize, game, results) {
    return new Promise((resolve) => {
      // maintain an object for storing all matching symbols
      const allMatchingSymbols = {};
      const allMatchingpaylines = {};
      // first of all, set all symbols from first column, as 1 of a kind
      for (let j = 0; j < columnSize; j += 1) {
        const symbolId = arrays.get(j, 0);
        // a symbol can be matched against other symbols in other reels, only is it's not a scatter
        if (game.default.gameConfig.symbols[symbolId].SymbolType !== 'Scatter') {
          if (allMatchingSymbols[symbolId]) {
            allMatchingSymbols[symbolId].push({ position: [[j, 0]] });
          } else {
            allMatchingSymbols[arrays.get(j, 0)] = [{ position: [[j, 0]] }];
          }
        }
      }
      let allSymbolIds = Object.keys(allMatchingSymbols);
      for (let i = 1; i < game.default.gameConfig.reelConfig.NumberOfReels; i += 1) {
        for (let j = 0; j < columnSize; j += 1) {
          const symbolId = arrays.get(j, i);
          // if current symbol is wild, check for all symbols in the allMatchingSymbols list, for a match
          // else, check for just the current symbol
          allSymbolIds = (game.default.gameConfig.symbols[symbolId].SymbolType === 'Wild') ? Object.keys(allMatchingSymbols) : [symbolId];
          if (allMatchingSymbols[symbolId] || game.default.gameConfig.symbols[symbolId].SymbolType === 'Wild') {
            allSymbolIds.forEach((selectedSymbolId) => {
              for (let k = 0; k < allMatchingSymbols[selectedSymbolId].length; k += 1) {
                const lastPos = allMatchingSymbols[selectedSymbolId][k].position[allMatchingSymbols[selectedSymbolId][k].position.length - 1];
                // if last element contains column number as one less than the current one, add current position to the set
                let payline = _.map(allMatchingSymbols[selectedSymbolId][k].position, '0').toString();
                payline = `${payline.substring(0, payline.length - 1)}${j}`;
                if (lastPos[1] + 1 === i) {
                  allMatchingSymbols[selectedSymbolId][k].position.push([j, i]);
                } else if (lastPos[1] === i && lastPos[0] !== j && !allMatchingpaylines[payline]) {
                  // if one symbol from current reel is already added to win combination, clone it from first reel to one less than the current reel,
                  // and add last element of combination as the current position
                  allMatchingSymbols[selectedSymbolId].push({
                    position: JSON.parse(
                      JSON.stringify(allMatchingSymbols[selectedSymbolId][k].position)
                    )
                  });
                  const lastSymbolPosition = allMatchingSymbols[selectedSymbolId].length - 1;
                  const lastCombinationPosition = allMatchingSymbols[selectedSymbolId][allMatchingSymbols[selectedSymbolId].length - 1].position.length - 1;
                  allMatchingSymbols[selectedSymbolId][lastSymbolPosition].position[lastCombinationPosition] = [j, i];
                  allMatchingpaylines[_.map(allMatchingSymbols[selectedSymbolId][lastSymbolPosition].position, '0')] = selectedSymbolId;
                }
              }
            });
          }
        }
      }
      for (const symbolId in allMatchingSymbols) {
        if (allMatchingSymbols[symbolId]) {
          allMatchingSymbols[symbolId].forEach((allPositions) => {
            if (game.default.gameConfig.payTable[symbolId] && game.default.gameConfig.payTable[symbolId][allPositions.position.length]) {
              results.push({ symbolId, winType: `${allPositions.position.length} of a kind`, winPosition: allPositions.position, multiplier: game.default.gameConfig.payTable[symbolId][allPositions.position.length].Multiplier });
            }
          });
        }
      }
      resolve(results);
    });
  }


  /**
   * update user session data
   * @param session - user's session data
   * @param docDetails - generated winning data, from spin result
   * it returns sessionRequest, which will be updated to database after execution
   */
  updateSessionData(session, docDetails) {
    session.gamble = docDetails.gamble;
    session.game = docDetails.game;
    session.eventData.userDetails.wallet = docDetails.wallet;
    session.eventData.bonusData = docDetails.bonusData;
    if (!_.isEmpty(session)) {
      session.event = 'spin';
      if (!_.isEmpty(docDetails.freeSpinData) && docDetails.freeSpinData.isFreeSpinAwarded) {
        delete docDetails.freeSpinData.isFreeSpinAwarded;
        delete docDetails.freeSpinData.noofFreeSpinsAwarded;
      }
      session.eventData.freeSpinData = docDetails.freeSpinData;
      if (session.eventData.bonusData && session.eventData.bonusData.isSymbolPicked === false) {
        session.pickStatus = 'ready';
      }
      // if symbol pick is not remaining, and free spin data is empty, remove bonusData, as there would be no use of it
      if (_.isEmpty(session.eventData.freeSpinData) && session.eventData.bonusData.isSymbolPicked === undefined) {
        session.eventData.bonusData = {};
      }
      // if free spins are completed, and symbol pick is not remaining (i.e., user didn't get 2 or 3 musketeers in the last fs)
      // then, remove freeSpin and bonus data
      if (session.eventData.freeSpinData.currentFreeSpin && session.eventData.freeSpinData.currentFreeSpin == session.eventData.freeSpinData.freeSpins && session.eventData.bonusData.isSymbolPicked === undefined) {
        session.eventData.freeSpinData = {};
        session.eventData.bonusData = {};
      }
    }

    const sessionRequest = docDetails.session.sessionRequest;
    sessionRequest.status = 'ready';
    sessionRequest.spinCompletedTimestamp = moment().utc();
    return sessionRequest;
  }


  /**
   * update user session when there are no free spins in noble musketeers
   * @param  gameId     noble musketeers game id
   * @param  token      user token
   * @param  docDetails user session schema
   * @param  gameData   game configuration schema
   */
  gameSessionUpdateMainGame(gameId, token, docDetails) {
    docDetails._updatedTimestamp = moment.utc();
    gameSession.getUserSession(token, gameId)
    .then((session) => {
      const sessionRequest = this.updateSessionData(session, docDetails);
      this.updateSessionAndSessionRequest(session, gameId, docDetails.session.sessionRequestKey, sessionRequest);
    }).catch((err) => {
      console.log(err);
    });
  }


  /**
  * This function is responsible to generate spin response
  * it includes view zone, wins, user details like wallet, bet array etc
  * necessary render view for the user
  */
  generateSpinResponse(obj, computeData, token, sessionRequestKey, sessionRequest, session) {
    // determine view port
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));

    // create reels for view zone
    const array = computeData.viewZone;
    const viewZone = {};

    for (let i = 0; i < noofReels; i += 1) {
      const line = [];
      for (let j = 0; j < columnSize; j += 1) {
        line.push({ symbolId: array.get(j, i), symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType, symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName });
      }
      viewZone[`Reel${i + 1}`] = line;
    }
    viewZone.viewZone = obj.game.default.gameConfig.viewZone;


    // create win document
    const result = {};
    result.viewZone = viewZone;
    result.wins = {};
    result.freeSpinData = {};
    result.levelData = {};
    result.noofReels = noofReels;
    let isFsWinAdded = false;
    return new Promise((resolve, reject) => {
      if (!session.eventData.bonusData) {
        session.eventData.bonusData = {};
      }

      // if user gets bonus, add bonusData
      if (computeData.results.freeSpinMultipliers) {
        session.eventData.bonusData.freeSpinMultipliers = computeData.results.freeSpinMultipliers;
        session.eventData.bonusData.totalPicks = session.eventData.bonusData.freeSpinMultipliers.length;
        session.eventData.bonusData.isSymbolPicked = computeData.results.isSymbolPicked;
      }

      // if free spin is present
      if (computeData.results.freeSpins) {
        if (!_.isEmpty(session)) {
          if (_.isEmpty(session.eventData.freeSpinData)) {
            result.freeSpinData.freeSpins = computeData.results.freeSpins;
            result.freeSpinData.currentFreeSpin = 0;
            result.freeSpinData.winAmount = 0;
            session.eventData.freeSpinData = JSON.parse(JSON.stringify(result.freeSpinData));
          } else {
            result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
            session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
            session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
            computeData.results.forEach((winLineResult) => {
              session.eventData.freeSpinData.winAmount += (winLineResult.multiplier * (session.eventData.currentBet) * (session.eventData.bonusData.multipliers).reduce((a, b) => a * b) || 1);
            });
            isFsWinAdded = true;
          }

          result.freeSpinData.totalPicks = session.eventData.freeSpinData.totalPicks;
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;

          if (session.eventData.freeSpinData.isFreeSpinAwarded) {
            delete session.eventData.freeSpinData.isFreeSpinAwarded;
            delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
          }
          session.event = 'spin';
          session._updatedTimestamp = moment.utc();
          super.updateSession(session, obj._id);
        }
        result.isSessionUpdated = true;
      }

      // total win amount calculated based on wins from pay line
      let total = 0;
      computeData.results.forEach((result) => {
        result.winAmount = result.multiplier * (session.eventData.currentBet);
        total += result.winAmount;
        delete result.multiplier;
      });
      let isWinUpdated = false;
      if (!_.isEmpty(session.eventData.freeSpinData) && session.eventData.freeSpinData.currentFreeSpin !== 0 && session.eventData.bonusData.multipliers && session.eventData.bonusData.multipliers.length && total > 0) {
        // multiply total winCoins with the multipliers
        total *= (session.eventData.bonusData.multipliers.reduce((a, b) => a * b) || 1);
        isWinUpdated = true;
      }
      result.wins.winCoins = total;
      result.wins.winLinesCount = computeData.results.length;
      result.wins.winLines = [];

      computeData.results.forEach(row => result.wins.winLines.push(row));

      // put user session data
      result.session = {};
      result.session.sessionRequestKey = sessionRequestKey;
      result.session.sessionRequest = sessionRequest;
      /**
       * notify of all the symbols present in view zone
       */
      gameEvents.notifyGameEvents(obj, token, session, result, computeData.results.freeSpins);
      // mask view zone here
      result.viewZone = super.maskViewZone(result.viewZone, obj, _.isEmpty(session.eventData.freeSpinData));
      result.bonusData = session.eventData.bonusData;
      if (_.isEmpty(session.eventData.freeSpinData)) {
        resolve(this.callWhow(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
        // some free spins remaining
        result.freeSpinData = session.eventData.freeSpinData;
        if (computeData.results.freeSpins) {
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
        }

        if (!computeData.results.freeSpins) {
          result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
        }
        if (!isWinUpdated && session.eventData.bonusData.multipliers && session.eventData.bonusData.multipliers.length && total > 0) {
          total *= (session.eventData.bonusData.multipliers.reduce((a, b) => a * b) || 1);
          result.wins.winCoins = total;
        }

        if (result.wins.winCoins > 0) {
          if (!isFsWinAdded) {
            result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
          }
          session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
        } else {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
        }

        // call whow api
        if (session.eventData.freeSpinData.currentFreeSpin !== 0) {
          resolve(this.callWhow(token, result, computeData, 0, 10, total, session, sessionRequestKey, sessionRequest, obj._id));
        } else {
          resolve(this.callWhow(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
        }
      } else {
        // free spin object exists and there are no free spins remaining
        resolve(this.callWhow(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      }
    });
  }


  /**
  * updates user's session with pickStatus
  */
  updateSessionWithCas(session, gameId, cas) {
    return new Promise((resolve, reject) => {
      bucket.upsert(`${session.eventData.userDetails.id}::${gameId}`, session, { cas }, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }


  /**
   * This function is reponsilbe to handle bonus pick of the game
   */
  async handleBonusRequest(token, gameId, cardId, session) {
    const actualMultipliers = session.eventData.bonusData.freeSpinMultipliers;
    let bonusObj = {};
    let pickedMultiplier = actualMultipliers[cardId];
    let wallet;
    let currentMultipliers = (session.eventData.bonusData || {}).multipliers || [];
    if (session.eventData.bonusData.freeSpinMultipliers.length === 3) {
      session.eventData.bonusData.multipliers = session.eventData.bonusData.multipliers || [];
      session.eventData.bonusData.multipliers.push(pickedMultiplier);
      currentMultipliers = session.eventData.bonusData.multipliers;
    } else if (pickedMultiplier.toString().length === 1) {
      let bonusWin = (session.eventData.currentBet * session.eventData.currentLines) * pickedMultiplier;
      if (currentMultipliers.length) {
        bonusWin *= (currentMultipliers.reduce((a, b) => a * b) || 1);
      }
      wallet = await this.callWhowForBonus(token, 0, bonusWin, 10);
      bonusObj.isNobelMusketeerBonus = true;
      bonusObj.isNobelMusketeerBonusAwarded = true;
      if (!_.isEmpty(session.eventData.freeSpinData)) {
        session.eventData.freeSpinData.winAmount += bonusWin;
      }
      bonusObj.freeSpinData = JSON.parse(JSON.stringify(session.eventData.freeSpinData));
      bonusObj.wins = { winCoins: bonusWin };
    } else {
      bonusObj.isNobelMusketeerBonus = true;
      bonusObj.isNobelMusketeerBonusAwarded = false;
      const multiplier = parseInt(pickedMultiplier.toString()[1]);
      const freeSpins = parseInt(pickedMultiplier.toString()[0]);
      session.eventData.bonusData.multipliers = session.eventData.bonusData.multipliers || [];
      session.eventData.bonusData.multipliers.push(multiplier);
      currentMultipliers = session.eventData.bonusData.multipliers;
      if (_.isEmpty(session.eventData.freeSpinData)) {
        session.eventData.freeSpinData = {
          isFreeSpinAwarded: true,
          freeSpins,
          currentFreeSpin: 0,
          noofFreeSpinsAwarded: freeSpins,
          winAmount: 0
        };
      } else {
        session.eventData.freeSpinData.noofFreeSpinsAwarded = freeSpins;
        session.eventData.freeSpinData.freeSpins += freeSpins;
        session.eventData.freeSpinData.isFreeSpinAwarded = true;
      }
      bonusObj.freeSpinData = JSON.parse(JSON.stringify(session.eventData.freeSpinData));
      if (session.eventData.freeSpinData.isFreeSpinAwarded) {
        delete session.eventData.freeSpinData.isFreeSpinAwarded;
        delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
      }
      bonusObj.wins = { winCoins: 0 };
      pickedMultiplier = multiplier;
    }
    delete session.eventData.bonusData.freeSpinMultipliers;
    delete session.eventData.bonusData.isSymbolPicked;
    delete session.eventData.bonusData.totalPicks;
    // if it was last freeSpin, remove freeSpin data
    if (session.eventData.freeSpinData.currentFreeSpin && session.eventData.freeSpinData.currentFreeSpin == session.eventData.freeSpinData.freeSpins) {
      session.eventData.freeSpinData = {};
    }
    if (_.isEmpty(session.eventData.freeSpinData)) {
      session.eventData.bonusData = {};
    }
    delete session.pickStatus;
    super.updateSession(session, gameId);
    if (!_.isEmpty(bonusObj)) {
      bonusObj = this.getBonusObj(bonusObj, gameId, session, token);
    }
    return {
      response: {
        multiplier: pickedMultiplier,
        currentMultipliers,
        wallet,
        actualMultipliers,
        wins: bonusObj.wins,
        freeSpinData: bonusObj.freeSpinData
      },
      bonusObj
    };
  }


  /*
  * build bonus object, to be logged to elasticsearch
  */
  getBonusObj(bonusObj, gameId, session, token) {
    bonusObj.gameId = gameId;
    bonusObj.user = {};
    bonusObj.user.currentBet = session.eventData.currentBet;
    bonusObj.user.currentLines = constants.NOBLE_MUSKETEERS.NOOF_LINES;
    bonusObj.user.totalBet = 0;
    bonusObj.user.token = token;
    bonusObj.user.userId = session.eventData.userDetails.id;
    return bonusObj;
  }


  /**
   * This function calls whow API for updating the wallet, when user has won bonus
   * Whow api is called at the end of each bonus
   * informing the wins to whow
   */
  callWhow(token, result, computeData, betAmount, virtualAmount, total, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve, reject) => {
      resolve(this.callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
    });
  }


  /**
   * This function calls whow API for updating the wallet
   * Whow api is called at the end of each spin
   * informing the wins to whow, and to deduct bet from the wallet
   */
  callWhowForBonus(token, betAmount, winAmount, virtualAmount) {
    return new Promise((resolve) => {
      whowapi.play(token, {
        betAmount,
        winAmount,
        virtualAmount
      }).then((response) => {
        response = JSON.parse(response);
        if (response.status === 200) {
          resolve(response.payload.user.wallet);
        } else {
          console.log(response, 'invalid result');
          resolve(0);
        }
      });
    });
  }


  /**
   * This is the actual whow api call function
   * play api is called in each spin, informing whow, win amount of spin
   * virtualAmount is specified as non-zero, when the free spins are going on
   */
  callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve) => {
      whowapi.play(token, {
        betAmount,
        winAmount: total,
        virtualAmount
      }).then((response) => {
        response = JSON.parse(response);
        if (response.status && response.status === 110) {
          sessionRequest.status = 'ready';
          if (session.gamble && !_.isEmpty(session.gamble)) {
            session.gamble.winAmount = 0;
            session.gamble.count = 0;
          }
          session.eventData.freeSpinData = {};
          session.eventData.bonusData = {};
          this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);
          resolve({ error: constants.RES_MESSAGES.NOT_ENOUGH_BALANCE, errorCode: constants.RES_ERROR_CODES.NOT_ENOUGH_BALANCE });
        } else {
          result.gamble = response.payload.round;
          if (!session.gamble || !session.gamble.history) {
            result.gamble.history = [];
          } else {
            result.gamble.history = session.gamble.history;
          }
          result.gamble.count = 0;
          result.levelData.level = response.payload.user.level;
          result.levelData.levelProgress = response.payload.user.levelProgress;
          result.wallet = response.payload.user.wallet;
          result.game = response.payload.game;
          result.game.betAmount = (session.eventData.currentBet * session.eventData.currentLines);
          result.jackpot = response.payload.user.jackpotData;
          resolve(result);
        }
      }).catch((err) => {
        console.log(err);
        resolve(result);
      });
    });
  }
}

module.exports = new NobleMusketeers();
