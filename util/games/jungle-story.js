const nj = require('numjs');
const rng = require('../rng');
const moment = require('moment');
const _ = require('lodash');
const { Compute } = require('../compute');
const gameEvents = require('../../helpers/game-events');

/**
 * Jungle Story specific features are implemented in this class
 * This class would be reponsible to update game related user sessions
 * This class would be reponsible to compute for special feature trigger
 */
class JungleStory extends Compute {
  /**
  * This function is responsible to calculate result for a spin event
  * taking into account
  * view zone, pay lines, pay table
  */
  compute(game, gameId, token, session) {
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    // random numbers are fetched from reel configuration
    const randomNumber = rng.generateRandomNumber(game.default.gameConfig.reelConfig);
    // view zone is calculated below
    const scatter = [];
    const specialSymbolPositions = {};
    this.generateViewZone(randomNumber, noofReels, columnSize, scatter, game, arrays, specialSymbolPositions);
    const deepCopyArray = arrays.clone();
    let featureData;
    if (this.checkIfFeatureTriggered(session, game.default.gameConfig.specialFeature)) {
      featureData = this.calculateSpecialWinning(arrays, session, game.default.gameConfig.specialFeature, columnSize, specialSymbolPositions, scatter);
    }
    // result based on view zone is calculated below
    let results = [];
    const payArray = game.default.gameConfig.payLines.normalizedPayArray;
    return new Promise((resolve) => {
      Promise.all([
        super.computeWins(arrays, columnSize, payArray, game, results),
        super.freeSpinCompute(scatter, results, game, arrays),
        super.wildPayCompute(arrays, columnSize, payArray, game, results)
      ]).then(() => {
        results = super.refineResults(results);
        resolve({ viewZone: deepCopyArray, results, featureData });
      }).catch((err) => {
        console.log(err);
        resolve({ error: 'error in view zone' });
      });
    });
  }


  /*
  * check if feature is triggered
  * pick a value from weight table
  * if value is *1*, trigger the feature
  */
  checkIfFeatureTriggered(session, specialFeature) {
    let gameType = 'freeGame';
    if (_.isEmpty(session.eventData.freeSpinData)) {
      gameType = 'mainGame';
    }
    if (specialFeature && specialFeature.specialFeatureWeight) {
      const randomNumber = Math.floor(Math.random() * specialFeature.specialFeatureWeight[`${gameType}Length`]);
      return specialFeature.specialFeatureWeight[gameType][randomNumber];
    }
    return false;
  }


  /*
  * to calculate the winning, after applying special feature
  * A - expandingWild - wild expands, wherever appears
  * B - 5X - all wins are multiplied by 5
  * C - ReplaceWithWild - symbol - H1 is replaced with wild symbol, whereverit appears
  * D - ReplaceWithScatter - symbol - H1=2 is replaced with scatter symbol, whereverit appears
  */
  calculateSpecialWinning(arrays, session, specialFeature, columnSize, specialSymbolPositions, scatter) {
    const randomNumber = Math.floor(Math.random() * specialFeature.specialReel.length);
    const specialReel = [];
    for (let i = 0; i < columnSize - 1; i += 1) {
      specialReel.push(specialFeature.specialReel[(randomNumber + i) % specialFeature.specialReel.length]);
    }
    const selectedPosition = Math.floor(Math.random() * specialReel.length);
    const featureData = specialFeature.featureData[specialReel[selectedPosition]];
    const reels = [];
    switch (featureData.type) {
      case 'expandingWild':
        if (specialSymbolPositions[featureData.symbolId]) {
          specialSymbolPositions[featureData.symbolId].forEach((position) => {
            if (!reels.includes(position.column + 1)) {
              reels.push(position.column);
              for (let i = 0; i < columnSize; i += 1) {
                arrays.set(i, position.column, featureData.symbolId);
              }
            }
          });
        }
        return { type: featureData.type, reels, specialReel, selectedPosition };
      case '5X':
        return { type: '5X', multiplier: 5, specialReel, selectedPosition };
      case 'ReplaceWithWild':
        if (specialSymbolPositions[featureData.symbolId]) {
          specialSymbolPositions[featureData.symbolId].forEach((position) => {
            arrays.set(position.row, position.column, featureData.replaceWith);
          });
        }
        return { type: featureData.type, positions: specialSymbolPositions[featureData.symbolId] || [], specialReel, selectedPosition };
      case 'ReplaceWithScatter':
        if (specialSymbolPositions[featureData.symbolId]) {
          specialSymbolPositions[featureData.symbolId].forEach((position) => {
            scatter.push(position);
          });
        }
        return { type: featureData.type, positions: specialSymbolPositions[featureData.symbolId] || [], specialReel, selectedPosition };
      default:
        return null;
    }
  }


  /**
  * This function generates view zone base on random numbers
  * view zone is calculated to be shown to user
  */
  generateViewZone(randomNumber, noofReels, columnSize, scatter, game, arrays, specialSymbolPositions) {
    const symbolIds = _.filter(_.map(game.default.gameConfig.specialFeature.featureData, 'symbolId'), symbolId => symbolId !== undefined);
    for (let i = 0; i < noofReels; i += 1) {
      for (let j = 0; j < columnSize; j += 1) {
        const symbolId = super.getSymbol(j, i, game.default.gameConfig.reelConfig, randomNumber);
        if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Scatter') {
          scatter.push({ column: i, row: j });
        }
        if (symbolIds.includes(symbolId)) {
          if (!specialSymbolPositions[symbolId]) {
            specialSymbolPositions[symbolId] = [];
          }
          specialSymbolPositions[symbolId].push({ column: i, row: j });
        }
        arrays.set(j, i, symbolId);
      }
    }
  }


  /**
  * This function is responsible to generate spin response
  * it includes view zone, wins, user details like wallet, bet array etc
  * necessary render view for the user
  */
  generateSpinResponse(obj, computeData, token, sessionRequestKey, sessionRequest, session) {
    // determine view port
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));

    // create reels for view zone
    const array = computeData.viewZone;
    const viewZone = {};

    for (let i = 0; i < noofReels; i += 1) {
      const line = [];
      for (let j = 0; j < columnSize; j += 1) {
        line.push({ symbolId: array.get(j, i), symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType, symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName });
      }
      viewZone[`Reel${i + 1}`] = line;
    }
    viewZone.viewZone = obj.game.default.gameConfig.viewZone;


    // create win document
    const result = {};
    result.viewZone = viewZone;
    result.wins = {};
    result.freeSpinData = {};
    result.levelData = {};
    result.noofReels = noofReels;
    if (computeData.featureData) {
      delete computeData.featureData.arrays;
      result.specialFeature = computeData.featureData;
    }
    let isFsWinAdded = false;
    return new Promise((resolve) => {
      // if free spin is present
      if (computeData.results.freeSpins) {
        if (!_.isEmpty(session)) {
          if (_.isEmpty(session.eventData.freeSpinData)) {
            result.freeSpinData.freeSpins = computeData.results.freeSpins;
            result.freeSpinData.currentFreeSpin = 0;
            result.freeSpinData.winAmount = 0;
            session.eventData.freeSpinData = JSON.parse(JSON.stringify(result.freeSpinData));
          } else {
            result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
            session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
            session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
            let total = 0;
            computeData.results.forEach((winLineResult) => {
              total += (winLineResult.multiplier * (session.eventData.currentBet));
            });
            if (computeData.featureData && computeData.featureData.multiplier) {
              total *= computeData.featureData.multiplier;
            }
            session.eventData.freeSpinData.winAmount += total;
            isFsWinAdded = true;
          }
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
          if (session.eventData.freeSpinData.isFreeSpinAwarded) {
            delete session.eventData.freeSpinData.isFreeSpinAwarded;
            delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
          }
          session.event = 'spin';
          session._updatedTimestamp = moment.utc();
          super.updateSession(session, obj._id);
        }
        result.isSessionUpdated = true;
      }

      // total win amount calculated based on wins from pay line
      let total = 0;
      computeData.results.forEach((resultObj) => {
        resultObj.winAmount = resultObj.multiplier * (session.eventData.currentBet);
        total += resultObj.winAmount;
        delete resultObj.multiplier;
      });
      if (computeData.featureData && computeData.featureData.multiplier) {
        total *= computeData.featureData.multiplier;
      }
      result.wins.winCoins = total;
      result.wins.winLinesCount = computeData.results.length;
      result.wins.winLines = [];

      computeData.results.forEach(row => result.wins.winLines.push(row));

      // put user session data
      result.session = {};
      result.session.sessionRequestKey = sessionRequestKey;
      result.session.sessionRequest = sessionRequest;
      /**
       * notify of all the symbols present in view zone
       */
      gameEvents.notifyGameEvents(obj, token, session, result, computeData.results.freeSpins);

      // mask view zone here
      result.viewZone = super.maskViewZone(result.viewZone, obj, _.isEmpty(session.eventData.freeSpinData));
      if (_.isEmpty(session.eventData.freeSpinData)) {
        resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
        // some free spins remaining
        result.freeSpinData = session.eventData.freeSpinData;
        if (computeData.results.freeSpins) {
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
        }

        if (!computeData.results.freeSpins) {
          result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
        }

        if (result.wins.winCoins > 0) {
          if (!isFsWinAdded) {
            result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
          }
          session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
        } else {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
        }

        // call whow api
        if (session.eventData.freeSpinData.currentFreeSpin !== 0) {
          resolve(super.callWhowBluehare(token, result, computeData, 0, 10, total, session, sessionRequestKey, sessionRequest, obj._id));
        } else {
          resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
        }
      } else {
        // free spin object exists and there are no free spins remaining
        resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      }
    });
  }
}

module.exports = new JungleStory();
