
const { Compute } = require('../compute');
const nj = require('numjs');
const rng = require('../rng');
const _ = require('lodash');
const moment = require('moment');
const gameSession = require('../gamesession');
const logger = require('../../helpers/firehose');
const Game = require('../../helpers/gamefunction');
const constants = require('../../response/constants');
const whowapi = require('../../api/whowapi');
const { bucket } = require('../../helpers/couchbase');
const gameEvents = require('../../helpers/game-events');

/**
 * Special features related to cinderella is implemented in this class
 * This class handles main game computation and logic for expanding feature during free spin
 */
class Cinderella extends Compute {


  /**
   * [Handles game click event for knight and dragons]
   * @param  gameId   knight and dragons game id
   * @param  token    user token
   * @param  clientID client id generated on the client side
   * @param  data     user related data fetched from whow
   * @param  gameData game configuratin related data
   */
  handleGameClick(gameId, token, clientID, data, gameData) {
    bucket.get(`${data.userDetails.user.id}::${gameId}`, (err, result) => {
      if (!err) {
        if (result == null) {
          gameSession.createGameSessionObject(gameId, clientID, data, gameData, constants.CINDERELLA.NOOF_LINES);
        } else {
          result.value.clientID = clientID;
          gameSession.buildUserSessionObject(result, data, gameData, constants.CINDERELLA.NOOF_LINES);
          super.updateSession(result.value, gameId);
        }
      } else {
        gameSession.createGameSessionObject(gameId, clientID, data, gameData, constants.CINDERELLA.NOOF_LINES);
      }
    });
    gameSession.createUserDataAndSessionRequest(data, gameId, token);
  }


  /**
  * This function is used to build response for game click event
  * game data along with user data for subsequent  game events calls are passed to client
  */
  generateGameClickResponse(data, token, gameId) {
    const result = {};
    result.game = {};
    result.game.gameId = data._id;
    result.game.viewZone = data.game.default.gameConfig.viewZone;
    result.game.reels = {};
    result.game.reels.noofReels = data.game.default.gameConfig.reelConfig.NumberOfReels;

    for (let i = 0; i < data.game.default.gameConfig.reelConfig.NumberOfReels; i++) {
      const columnSize = parseInt(data.game.default.gameConfig.viewZone.toString().charAt(0));
      const reelArray = [];
      const randomNumber = Math.floor(Math.random() * data.game.default.gameConfig.reelConfig[`Reel${i + 1}`].NumberOfRows);
      for (let j = 0; j < columnSize; j += 1) {
        // do not give away reel configuration, mask initiail view
        reelArray.push(data.game.default.gameConfig.reelConfig[`Reel${i + 1}`].SymbolDistribution[randomNumber % data.game.default.gameConfig.reelConfig[`Reel${i + 1}`].NumberOfRows].SymbolId);
      }
      result.game.reels[`Reel${i + 1}`] = reelArray;
    }
    result.game.payArray = data.game.default.gameConfig.payLines.PayArray;
    result.game.payTable = data.game.default.gameConfig.payTable;
    result.game.bigWins = data.game.default.gameConfig.bigWins;
    result.game.symbols = data.game.default.gameConfig.symbols;
    result.game.stage = data.game.default.gameConfig.stage;
    result.game.version = data.game.default.gameConfig.versionName;
    result.userDetails = data.userDetails;
    result.userDetails.selectablePaylines = data.game.default.gameConfig.selectablePaylines.options;
    result.userDetails.currentBet = data.userDetails.game.settings.bets[0] || 5;
    result.userDetails.currentLines = constants.CINDERELLA.NOOF_LINES;
    result.userDetails.totalBet = (result.userDetails.currentBet * result.userDetails.currentLines);

    return new Promise((resolve) => {
      gameSession.getUserSession(token, gameId)
      .then((session) => {
        result.userDetails.currentBet = session.eventData.currentBet;
        result.userDetails.currentLines = session.eventData.currentLines;
        result.userDetails.freeSpinData = session.eventData.freeSpinData;
        result.userDetails.totalBet = (result.userDetails.currentBet * result.userDetails.currentLines);
        resolve(result);
      }).catch(() => {
        result.userDetails.currentBet = data.userDetails.game.settings.bets[0] || 5,
        result.userDetails.currentLines = parseInt(data.game.default.gameConfig.selectablePaylines.options[data.game.default.gameConfig.selectablePaylines.options.length - 1]) || 20;
        result.userDetails.freeSpinData = {};
        result.userDetails.totalBet = (result.userDetails.currentBet * result.userDetails.currentLines);
        resolve(result);
      });
    });
  }


  /**
  * This function is responsible to calculate result for a spin event
  * taking into account
  * view zone, pay lines, pay table
  */
  compute(game, gameId, token, session) {
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));

    // start calculating view zone
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);

    // random numbers are fetched from reel configuration
    const randomNumber = rng.generateRandomNumber(game.default.gameConfig.reelConfig);

    // view zone is calculated below
    const expanding = [];
    this.generateViewZone(randomNumber, noofReels, columnSize, expanding, game, arrays, session);

    // load pay arrays
    const payArray = game.default.gameConfig.payLines.normalizedPayArray;

    // result based on view zone is calculated below
    const results = [];

    return new Promise((resolve) => {
      const wildReels = this.getWildReels(session, game);
      let pickedWildPositions;
      if (wildReels) {
        const wildSymbolId = _.findKey(game.default.gameConfig.symbols, { SymbolType: 'Wild' });
        pickedWildPositions = this.replaceWildsInReel(arrays, wildReels, columnSize, wildSymbolId);
      }

      // determine if glass show pays
      const bonus = this.determineBonus(noofReels, columnSize, game, arrays);

      // check for free spins
      const scatter = this.determineScatters(arrays, noofReels, columnSize, game);

      this.freeSpinCompute(scatter, results, game)
      .then((results) => {
        resolve(this.computeSpinWins(game, arrays, arrays, results, payArray, columnSize, bonus, pickedWildPositions));
      });
    });
  }


  /**
  * This function generates view zone base on random numbers
  * view zone is calculated to be shown to user
  * In Cinderella the first symbol of each reel is replace in all of other symbol places in same reel
  */
  generateViewZone(randomNumber, noofReels, columnSize, expanding, game, arrays, session) {
    if (!_.isEmpty(session.eventData.freeSpinData) && (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0)) {
      let symbolId;
      for (let i = 0; i < noofReels; i += 1) {
        for (let j = 0; j < columnSize; j += 1) {
          if (j === 0) {
            symbolId = this.getSymbol(j, i, game.default.gameConfig.freeSpin.reelConfig, randomNumber);
          }
          arrays.set(j, i, symbolId);
        }
      }
    } else {
      let symbolId;
      for (let i = 0; i < noofReels; i += 1) {
        for (let j = 0; j < columnSize; j += 1) {
          if (j === 0) {
            symbolId = this.getSymbol(j, i, game.default.gameConfig.reelConfig, randomNumber);
          }
          arrays.set(j, i, symbolId);
        }
      }
    }
  }


  /**
  * This function generates view zone base on random numbers
  * view zone is calculated to be shown to user
  * In Cinderella the first symbol of each reel is replace in all of other symbol places in same reel
  */
  determineBonus(noofReels, columnSize, game, arrays) {
    const bonus = [];
    for (let i = 0; i < noofReels; i += 1) {
      for (let j = 0; j < columnSize; j += 1) {
        if (game.default.gameConfig.symbols[arrays.get(j, i)].SymbolType === 'Bonus') {
          bonus.push(i);
        }
      }
    }
    return bonus;
  }


  /**
   * This function computes results when free spins are not active
   * Wild and scatter symbols are considered when computing
   */
  computeSpinWins(game, arrays, deepCopyArray, results, payArray, columnSize, bonus, pickedWildPositions) {
    return new Promise((resolve, reject) => {
      Promise.all(
        [
          this.computeWins(arrays, columnSize, payArray, game, results),
          this.scatterPayCompute(arrays, columnSize, payArray, game, results, bonus),
          this.wildPayCompute(deepCopyArray, columnSize, payArray, game, results)
        ]).then((values) => {
          const refinedResults = [];
          let highestResults = [];
          results.forEach((result) => {
            if (result.symbolId) {
              refinedResults.push(result);
            } else {
              highestResults.push(result);
            }
          });
          if (refinedResults.length > 0) {
            highestResults.push(_.maxBy(refinedResults, 'multiplier'));
            if (results.freeSpins) {
              highestResults.freeSpins = results.freeSpins;
            }

          } else {
            highestResults = results;
          }
          resolve({ viewZone: arrays, results: highestResults, pickedWildPositions });
        }).catch((err) => {
          resolve({ error: 'error in view zone' });
        });
    });
  }


  /**
  * This function computes wins based on view zone
  * win is calculated starting from left i.e. from 1st reel
  * Wilds and scatter are taken into consideration while computing wins
  */
  computeWins(arrays, columnSize, payArray, game, results) {
    return new Promise((resolve, reject) => {
      const bonusSymbolId = _.findKey(game.default.gameConfig.symbols, { SymbolType: 'Bonus' });
      const j = 0;
      for (let i = 0; i < columnSize; i += 1) {
        for (let k = 0; k < payArray.length; k += 1) {
          let symbolId = arrays.get(i, j);
          if (symbolId === bonusSymbolId) {
            break;
          }
          const payLine = payArray[k].data;
          let ofAKind = [];
          if (payLine[0] === i) {
            ofAKind.push('test');
            for (let l = 1; l < payLine.length; l += 1) {
              if (game.default.gameConfig.symbols[symbolId].SymbolType === 'Wild' && game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType !== 'Scatter' && arrays.get(payLine[l], l) !== bonusSymbolId) {
                symbolId = arrays.get(payLine[l], l);
                ofAKind.push('test');
                continue;
              }
              if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType !== 'Wild' && arrays.get(payLine[l], l) !== symbolId) {
                break;
              }
              ofAKind.push('test');
            }
            if (ofAKind.length > 0 && game.default.gameConfig.payTable[symbolId][ofAKind.length] && game.default.gameConfig.symbols[symbolId].SymbolType !== 'Wild') {
              results.push({ symbolId, winType: `${ofAKind.length} of a kind`, winLineId: payArray[k]._id, multiplier: game.default.gameConfig.payTable[symbolId][ofAKind.length].Multiplier });
            }
          }
          ofAKind = [];
        }
      }
      resolve(results);
    });
  }


  /**
  * This function checks if wild present in view zone pays or not
  * if wilds are present and wild pays then wins are calculated subsequentaly
  */
  wildPayCompute(arrays, columnSize, payArray, game, results) {
    return new Promise((resolve, reject) => {
      const bonusSymbolId = _.findKey(game.default.gameConfig.symbols, { SymbolType: 'Bonus' });
      const wildSymbolId = _.findKey(game.default.gameConfig.symbols, { SymbolType: 'Wild' });
      const scatteSymbolId = _.findKey(game.default.gameConfig.symbols, { SymbolType: 'Scatter' });
      let lastSymbolId;
      const j = 0;
      for (let i = 0; i < columnSize; i += 1) {
        const symbolId = arrays.get(i, j);
        lastSymbolId = symbolId;
        if (symbolId === bonusSymbolId) {
          break;
        }
        if (symbolId !== wildSymbolId) {
          break;
        }
        for (let k = 0; k < payArray.length; k += 1) {
          const payLine = payArray[k].data;
          const ofAKind = [];
          if (payLine[0] === i) {
            ofAKind.push('test');
            for (let l = 1; l < payLine.length; l += 1) {
              if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType !== 'Wild') {
                lastSymbolId = arrays.get(payLine[l], l);
                break;
              }
              ofAKind.push('test');
            }
            if (ofAKind.length > 0 && game.default.gameConfig.payTable[symbolId][ofAKind.length]) {
              results.push({ symbolId, winType: `${ofAKind.length} of a kind`, winLineId: payArray[k]._id, multiplier: game.default.gameConfig.payTable[symbolId][ofAKind.length].Multiplier });
            }
          }
        }
      }
      resolve(results);
    });
  }


  /**
  * This function checks if scatter is present in view zone pays or not
  * if scatters are present and scatter pays then wins are calculated subsequentaly
  */
  scatterPayCompute(arrays, columnSize, payArray, game, results, bonus) {
    return new Promise((resolve, reject) => {
      const bonusSymbolId = _.findKey(game.default.gameConfig.symbols, { SymbolType: 'Bonus' });
      if (bonus.length > 0 && game.default.gameConfig.payTable[bonusSymbolId][bonus.length]) {
        results.push({ bonusWin: { multiplier: game.default.gameConfig.payTable[bonusSymbolId][bonus.length].Multiplier, winType: `${bonus.length}` } });
      }
      resolve(results);
    });
  }


  /**
  * updates user game session with details of a spin result
  * it may include free spin if won in game spin
  * and updated wallet chips as a result of whow api call bet/play
  */
  gameSpin(gameId, token, docDetails) {
    docDetails._updatedTimestamp = moment.utc();
    gameSession.getUserSession(token, gameId)
    .then((session) => {
      session.game = docDetails.game;
      session.eventData.userDetails.wallet = docDetails.wallet;
      session.gamble = docDetails.gamble;

      if (!_.isEmpty(session)) {
        session.event = 'spin';
        if (!_.isEmpty(docDetails.freeSpinData) && docDetails.freeSpinData.isFreeSpinAwarded) {
          delete docDetails.freeSpinData.isFreeSpinAwarded;
          delete docDetails.freeSpinData.noofFreeSpinsAwarded;
        }
        session.eventData.freeSpinData = docDetails.freeSpinData;
        if (!_.isEmpty(session.eventData.freeSpinData) && (session.eventData.freeSpinData.currentFreeSpin == session.eventData.freeSpinData.freeSpins)) {
          const totalWin = session.eventData.freeSpinData.winAmount;
          session.gamble.winAmount = totalWin;
          session.gamble.initialWin = totalWin;
          session.gamble.wasFreeSpin = true;
          session.eventData.freeSpinData = {};
        } else if (session.gamble) {
          session.gamble.wasFreeSpin = false;
        }
      }

      const sessionRequestKey = docDetails.session.sessionRequestKey;
      const sessionRequest = docDetails.session.sessionRequest;
      sessionRequest.status = 'ready';
      sessionRequest.spinCompletedTimestamp = moment().utc();
      this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);
      super.updateSession(session, gameId);
    }).catch((err) => {
      console.log(err);
    });
  }


  /**
  * This function is responsible to generate spin response
  * it includes view zone, wins, user details like wallet, bet array etc
  * necessary render view for the user
  */
  generateSpinResponse(obj, computeData, token, sessionRequestKey, sessionRequest, session) {
    // determine view port
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));

    // create reels for view zone
    const array = computeData.viewZone;
    const viewZone = {};

    for (let i = 0; i < noofReels; i += 1) {
      const line = [];
      for (let j = 0; j < columnSize; j += 1) {
        line.push({ symbolId: array.get(j, i), symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType, symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName });
      }
      viewZone[`Reel${i + 1}`] = line;
    }
    viewZone.viewZone = obj.game.default.gameConfig.viewZone;


    // create win document
    const result = {};
    result.viewZone = viewZone;
    result.wins = {};
    result.freeSpinData = {};
    result.levelData = {};

    return new Promise((resolve) => {
        // if free spin is present
      if (computeData.results.freeSpins) {
        if (!_.isEmpty(session)) {
          if (_.isEmpty(session.eventData.freeSpinData)) {
            result.freeSpinData.freeSpins = computeData.results.freeSpins;
            result.freeSpinData.pickedSymbol = computeData.pickedSymbol;
            result.freeSpinData.currentFreeSpin = 0;
            result.freeSpinData.winAmount = 0;

            session.eventData.freeSpinData = result.freeSpinData;
          } else {
            result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;

            session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
            session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
          }
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
          if (session.eventData.freeSpinData.isFreeSpinAwarded) {
            delete session.eventData.freeSpinData.isFreeSpinAwarded;
            delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
          }
          session.event = 'spin';
          session._updatedTimestamp = moment.utc();

          super.updateSession(session, obj._id);
        }
        result.isSessionUpdated = true;
      }

        // total win amount calculated based on wins from pay line
      let total = 0;
      let bonusWins;
      const comResults = [];
      computeData.results.forEach((resultObj) => {
        if (resultObj.bonusWin) {
          bonusWins = resultObj.bonusWin;
        } else {
          resultObj.winAmount = resultObj.multiplier * session.eventData.currentBet * session.eventData.currentLines;
          total += (resultObj.multiplier * session.eventData.currentBet * session.eventData.currentLines);
          delete resultObj.multiplier;
          comResults.push(resultObj);
        }
      });
      let updateFreeSpins = false;
      let fs;
      if (computeData.results.freeSpins) {
        updateFreeSpins = true;
        fs = computeData.results.freeSpins;
      }
      computeData.results = comResults;

      if (updateFreeSpins) {
        computeData.results.freeSpins = fs;
      }
      if (bonusWins) {
        computeData.results.bonusWins = bonusWins;
        total += (bonusWins.multiplier * session.eventData.currentBet * session.eventData.currentLines);
        result.wins.bonusWins = {};
        result.wins.bonusWins.winCoins = bonusWins.multiplier * session.eventData.currentBet * session.eventData.currentLines;
        result.wins.bonusWins.winType = bonusWins.winType;
      }

      result.wins.winCoins = total;
      result.wins.winLinesCount = computeData.results.length;
      result.wins.winLines = JSON.parse(JSON.stringify(computeData.results));

      if (result.wins.winLines.freeSpins) {
        delete result.wins.winLines.freeSpins;
      }

      // check if there was a picked wild in view zone
      if (computeData.pickedWildPositions) {
        result.pickedWildPositions = {};
        const pickedWildPositions = computeData.pickedWildPositions;
        pickedWildPositions.forEach((position) => {
          const reelNumber = position.i + 1;
          const columnNumber = position.j;
          result.pickedWildPositions[`reel${reelNumber}`] = { columnNumber, isWildPicked: true };
        });
      }

        // put user session data
      result.session = {};
      result.session.sessionRequestKey = sessionRequestKey;
      result.session.sessionRequest = sessionRequest;

      /**
       * notify of all the symbols present in view zone
       */
      gameEvents.notifyGameEvents(obj, token, session, result, computeData.results.freeSpins);

      // mask view zone here
      result.viewZone = super.maskViewZone(result.viewZone, obj);
      if (_.isEmpty(session.eventData.freeSpinData)) {
          // no free spins
        resolve(this.callWhow(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
          // some free spins remaining
        result.freeSpinData = session.eventData.freeSpinData;

        if (computeData.results.freeSpins) {
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
        }


        if (!computeData.results.freeSpins) {
          result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
        }

        if (result.wins.winCoins > 0) {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
          session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
        } else {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
        }
          // call whow api
        if (session.eventData.freeSpinData.currentFreeSpin === 0) {
          // free spins just awarded
          session.eventData.freeSpinData.winAmount = 0;
          resolve(this.callWhow(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
        } else {
          resolve(this.callWhow(token, result, computeData, 0, 10, total, session, sessionRequestKey, sessionRequest, obj._id));
          // resolve(this.callBet(token, 0, total, 10, result, session, sessionRequestKey, sessionRequest, obj._id));
        }
      } else {
          // free spin object exists and there are no free spins remaining
        resolve(this.callWhow(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      }
    });
  }


  /**
   * This function is overriden from the super class
   * Whow api is called at the end of each spin
   * informing the wins to whow
   */
  callWhow(token, result, computeData, betAmount, virtualAmount, total, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve) => {
      resolve(this.callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
    });
  }


  /**
   * This is the actual whow api call function
   * play api is called when user does not win
   */
  callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve) => {
      whowapi.play(token, {
        betAmount,
        winAmount: total,
        virtualAmount
      }).then((response) => {
        response = JSON.parse(response);

        if (response.status && response.status === 110) {
          sessionRequest.status = 'ready';
          if (session.gamble && !_.isEmpty(session.gamble)) {
            session.gamble.winAmount = 0;
            session.gamble.count = 0;
          }
          session.eventData.freeSpinData = {};
          this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);
          resolve({ error: constants.RES_MESSAGES.NOT_ENOUGH_BALANCE, errorCode: constants.RES_ERROR_CODES.NOT_ENOUGH_BALANCE });
        } else {
          result.gamble = response.payload.round;
          if (!session.gamble || !session.gamble.history) {
            result.gamble.history = [];
          } else {
            result.gamble.history = session.gamble.history;
          }
          result.gamble.count = 0;
          result.levelData.level = response.payload.user.level;
          result.levelData.levelProgress = response.payload.user.levelProgress;
          result.wallet = response.payload.user.wallet;
          result.game = response.payload.game;
          result.game.betAmount = (session.eventData.currentBet * session.eventData.currentLines);
          result.jackpot = response.payload.user.jackpotData;
          resolve(result);
        }
      }).catch((err) => {
        console.log(err);
        resolve(result);
      });
    });
  }


  /**
  * This function models the spin result document based on the analytics requirement
  * it deletes the fields not required and fetches the field required from database
  * logging for secret of amun is customized
  */
  logSpinResult(result, token, gameId) {
    if (result && token && gameId) {
      delete result.levelData;
      delete result.game;
      delete result.jackpot;
      delete result.session;
      delete result.viewZone;
      result.gameId = gameId;
      const gameData = Game.getGame(gameId);
      result.baseGameId = gameData.game.default.baseGameId;
      result.gameVersion = gameData.game.default.versionName;
      result.stage = gameData.game.default.stage;
      if (result.pickedWildPositions) {
        result.isPickedWildPresent = true;
      }

      /**
      * removes un necessary data from while logging spin results
      */
      const symbols = {};
      const types = {};
      if (result.wins.winLines.length > 0) {
        result.wins.winLines.forEach((line) => {
          if (!types[line.winType]) {
            types[line.winType] = 0;
          }
          if (!symbols[line.symbolId]) {
            symbols[line.symbolId] = 0;
          }
          types[line.winType]++;
          symbols[line.symbolId]++;
        });
        result.wins.types = types;
        result.wins.symbols = symbols;
      }
      if (result.wins.expandedWins && result.wins.expandedWins.expandedWinLines && result.wins.expandedWins.expandedWinLines.length > 0) {
        result.wins.expandedWins.expandedWinLines.forEach((line) => {
          if (!types[line.winType]) {
            types[line.winType] = 0;
          }
          if (!symbols[line.symbolId]) {
            symbols[line.symbolId] = 0;
          }
          types[line.winType]++;
          symbols[line.symbolId]++;
        });
        result.wins.types = types;
        result.wins.symbols = symbols;
      }
      gameSession.getUserSession(token, gameId)
      .then((session) => {
        result.user = {};
        result.user.currentBet = session.eventData.currentBet;
        result.user.currentLines = session.eventData.currentLines;
        result.user.totalBet = session.eventData.currentBet * session.eventData.currentLines;
        result.user.token = token;
        result.user.userId = session.eventData.userDetails.id;

        logger.info(result);
      }).catch((err) => {
        console.log(err);
      });
    }
  }


  /**
  * Checks for wild reel
  * Determines wild reels from wild table in game config
  */
  getWildReels(session, game) {
    if (_.isEmpty(session.eventData.freeSpinData)) {
      // main game
      const mainGameLength = game.default.gameConfig.wildReels.mainGameLength;
      const index = Math.floor(Math.random() * mainGameLength);
      const wildReel = game.default.gameConfig.wildReels.mainGame[index];
      if (wildReel) {
        return wildReel;
      }
      return null;
    }
      // free game
    const freeGameLength = game.default.gameConfig.wildReels.freeGameLength;
    const index = Math.floor(Math.random() * freeGameLength);
    const wildReel = game.default.gameConfig.wildReels.freeGame[index];
    if (wildReel) {
      return wildReel;
    }
    return null;
  }


  /**
  * Replaces wilds in given reels determined from wild table
  */
  replaceWildsInReel(arrays, wildReels, columnSize, wildSymbolId) {
    const pickedWildPositions = [];
    for (let i = 0; i < wildReels.length; i += 1) {
      if (wildReels[i] === '1') {
        for (let j = 0; j < columnSize; j += 1) {
          arrays.set(j, i, wildSymbolId);
        }
        pickedWildPositions.push({ i, j: 0 });
      }
    }
    return pickedWildPositions;
  }


  /**
   * Determines the number of free spin symbols in the view zone
   */
  determineScatters(arrays, noofReels, columnSize, game) {
    const bonus = [];
    for (let i = 0; i < noofReels; i += 1) {
      for (let j = 0; j < columnSize; j += 1) {
        if (game.default.gameConfig.symbols[arrays.get(j, i)].SymbolType === 'Scatter') {
          bonus.push(i);
        }
      }
    }
    return bonus;
  }
}

module.exports = new Cinderella();
