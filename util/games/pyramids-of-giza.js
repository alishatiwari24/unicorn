/* eslint-disable max-statements, max-params, prefer-destructuring, no-ternary, multiline-ternary, no-console */
const nj = require('numjs');
const rng = require('../rng');
const moment = require('moment');
const _ = require('lodash');
const { Compute } = require('../compute');
const gameEvents = require('../../helpers/game-events');
const gameFunction = require('../../helpers/gamefunction');
const whowapi = require('../../api/whowapi');
const constants = require('../../response/constants');

class PyramidsOfGiza extends Compute {

  /**
  * This function handles game click event from controller
  * WHOW API is called to get user data from token
  * relevant user data along with game sessions are created using compute helper
  */
  handleGameClick(gameId, token) {
    return new Promise((resolve, reject) => {
      whowapi.get(token)
      .then((response) => {
        let obj = gameFunction.getGame(gameId);
        obj = JSON.parse(JSON.stringify(obj));
        obj.game.default.gameConfig.reelConfig = obj.game.default.gameConfig.reelConfigSet[obj.game.default.gameConfig.specialFeature.reelSwitching.mainGame[0]];
        if (!obj) {
          resolve({ error: constants.RES_MESSAGES.GAME_NOT_FOUND, erroCode: constants.RES_ERROR_CODES.GAME_NOT_FOUND });
        } else if (response.status == 200) {
          obj.userDetails = response.payload;
          resolve(this.generateGameClickResponse(obj, token, gameId));
        } else {
          resolve({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
        }
      }).catch((err) => {
        console.log(err);
        reject({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
      });
    });
  }


  /**
   * getReelConfiguration method is used to get reelConfig for free game or main game
   * @param  {[game]} game    game contains game object
   * @param  {[session]} session session object user session and games session details
   */
  getReelConfiguration(game, session) {
    let reelConfigProbability = 0;
    if (!_.isEmpty(session.eventData.freeSpinData) && (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0)) {
      reelConfigProbability = this.getRandomProbability(game, 'freeGame');
      const reelConf = game.default.gameConfig.freeSpin.reelConfigSet[reelConfigProbability];
      game.default.gameConfig.freeSpin.reelConfig = reelConf;
      game.default.gameConfig.freeSpin.reelConfig.NumberOfReels = game.default.gameConfig.reelConfigSet[0].NumberOfReels;
      game.default.gameConfig.reelConfig = reelConf;

      return game.default.gameConfig;
    }
      reelConfigProbability = this.getRandomProbability(game, 'mainGame');
      const reelConf = game.default.gameConfig.reelConfigSet[reelConfigProbability];
      game.default.gameConfig.reelConfig = reelConf;
      return game.default.gameConfig;
  }


  /**
   * getRandomProbability method is used to get probability of reelWeight
   * which is used to select reelConfig from any two
   * @param  {[game]} game object contains game data
   * @param  {[gameType]} gameType method is used to get freeGame or mainGame
   */
  getRandomProbability(game, gameType) {
    return game.default.gameConfig.specialFeature.reelSwitching[gameType][Math.floor(Math.random() * (game.default.gameConfig.specialFeature.reelSwitching[gameType].length))];
  }


  /**
   * This function is responsible to calculate result for a spin event
   * taking into account
   * view zone, pay lines, pay table
   */
   compute(game, gameId, token, session) {
    game.default.gameConfig = this.getReelConfiguration(game, session);
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    // start calculating view zone
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    // random numbers are fetched from reel configuration
    const randomNumber = rng.generateRandomNumber(game.default.gameConfig.reelConfig);
    // view zone is calculated below
    const scatter = [];
    const nudgeReel = [];
    const pos = {};
    this.generateViewZone(randomNumber, noofReels, columnSize, scatter, game, arrays, session, pos);
    // if expanding wild is detected complete reel is replaced with non expaning symbols each
    const deepCopyArray = arrays.clone();
    if (Object.keys(pos).length > 0) {
      const wildSymbolId = _.findKey(game.default.gameConfig.symbols, {
        SymbolType: 'Wild',
        isExpandingWild: true
      });
      const wildMoveMent = {
        '1 - 0': { currentWildPosition: 0, moveTo: 2 },
        '2 - 1': { currentWildPosition: 1, moveTo: 1 },
        '1 - 2': { currentWildPosition: 2, moveTo: 2 },
        '2 - 2': { currentWildPosition: 1, moveTo: 1 }
      };
      Object.keys(pos).forEach((reel) => {
        if (wildMoveMent[`${pos[reel].wildCount} - ${pos[reel].lastWild}`]) {
          const nudgePos = JSON.parse(JSON.stringify(wildMoveMent[`${pos[reel].wildCount} - ${pos[reel].lastWild}`]));
          nudgePos.isReelNudge = true;
          nudgePos.reel = reel;
          nudgePos.direction = (pos[reel].lastWild < 2) ? 'down' : 'up';
          nudgeReel.push(nudgePos);
        }
      });
      nudgeReel.forEach((expandingPosition) => {
        for (let j = 0; j < columnSize; j += 1) {
          arrays.set(j, (parseInt(expandingPosition.reel) - 1), wildSymbolId);
        }
      });
    }

    // load pay arrays
    const payArray = game.default.gameConfig.payLines.normalizedPayArray;
    // result based on view zone is calculated below
    let results = [];
    return new Promise((resolve) => {
      Promise.all([
        super.computeWins(arrays, columnSize, payArray, game, results),
        super.freeSpinCompute(scatter, results, game),
        super.wildPayCompute(arrays, columnSize, payArray, game, results)
      ]).then(() => {
        results = super.refineResults(results);
        resolve({
          viewZone: deepCopyArray,
          results,
          scatter,
          nudgeReel
        });
      }).catch((err) => {
        console.log(err, 'err in cmpute');
        resolve({ error: 'error in view zone' });
      });
    });
  }


  /**
   * This function generates view zone base on random numbers
   * view zone is calculated to be shown to user
   */
  generateViewZone(randomNumber, noofReels, columnSize, scatter, game, arrays, session, pos) {
    for (let i = 0; i < noofReels; i += 1) {
      for (let j = 0; j < columnSize; j += 1) {
        const symbolId = super.getSymbol(j, i, game.default.gameConfig.reelConfig, randomNumber);
        if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Wild' && game.default.gameConfig.symbols[symbolId].isExpandingWild == true && !_.isEmpty(session.eventData.freeSpinData)
          && (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0)) {
          if (!pos[i + 1]) {
            pos[i + 1] = { wildCount: 0 };
          }
          pos[i + 1].lastWild = j;
          pos[i + 1].wildCount += 1;
        }
        if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Scatter') {
          scatter.push(i);
        }
        arrays.set(j, i, symbolId);
      }
    }
  }


  /**
   * generateSpinResponse method is used to generate spin response on every spin
   * and update user session and call callWhowBluehare api
   * @param obj                 obj  contains game object
   * @param computeData computeData contains data that is computed on every spin
   * @param token              token contains user token
   * @param sessionRequestKey  its contains that sessionRequestKey such as cas
   * @param sessionRequest sessionRequest contains game session request
   */
  generateSpinResponse(obj, computeData, token, sessionRequestKey, sessionRequest, session) {
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));
    const array = computeData.viewZone;
    const viewZone = {};
    for (let i = 0; i < noofReels; i += 1) {
      const line = [];
      for (let j = 0; j < columnSize; j += 1) {
        line.push({
          symbolId: array.get(j, i),
          symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType,
          symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName
        });
      }
      viewZone[`Reel${i + 1}`] = line;
    }
    viewZone.viewZone = obj.game.default.gameConfig.viewZone;

    const result = {};
    result.viewZone = viewZone;
    if (computeData.nudgeReel.length) {
      result.nudgeReel = computeData.nudgeReel;
    }
    result.wins = {};
    result.freeSpinData = {};
    result.levelData = {};
    result.noofReels = noofReels;
    return new Promise((resolve) => {
      if (computeData.results.freeSpins) {
        if (!_.isEmpty(session)) {
          if (_.isEmpty(session.eventData.freeSpinData)) {
            result.freeSpinData.freeSpins = computeData.results.freeSpins;
            result.freeSpinData.currentFreeSpin = 0;
            result.freeSpinData.winAmount = 0;
            session.eventData.freeSpinData = result.freeSpinData;
          } else {
            result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
            session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
            session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
            computeData.results.forEach((winLineResult) => {
              session.eventData.freeSpinData.winAmount += (winLineResult.multiplier * (session.eventData.currentBet));
            });
          }
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
          if (session.eventData.freeSpinData.isFreeSpinAwarded) {
            delete session.eventData.freeSpinData.isFreeSpinAwarded;
            delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
          }
          session.event = 'spin';
          session._updatedTimestamp = moment.utc();
          super.updateSession(session, obj._id);
        }
        result.isSessionUpdated = true;
      }
      let total = 0;
      computeData.results.forEach((resultObj) => {
        resultObj.winAmount = resultObj.multiplier * (session.eventData.currentBet);
        total += resultObj.winAmount;
        delete resultObj.multiplier;
      });
      result.wins.winCoins = total;
      result.wins.winLinesCount = computeData.results.length;
      result.wins.winLines = [];
      computeData.results.forEach(row => result.wins.winLines.push(row));
      result.session = {};
      result.session.sessionRequestKey = sessionRequestKey;
      result.session.sessionRequest = sessionRequest;
      gameEvents.notifyGameEvents(obj, token, session, result, computeData.results.freeSpins);
      // mask view zone here
      result.viewZone = super.maskViewZone(result.viewZone, obj, _.isEmpty(session.eventData.freeSpinData));
      if (_.isEmpty(session.eventData.freeSpinData)) {
        resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
        result.freeSpinData = session.eventData.freeSpinData;
        if (computeData.results.freeSpins) {
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
        }
        if (!computeData.results.freeSpins) {
          result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
        }

        if (result.wins.winCoins > 0) {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
          session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
        } else {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
        }
        // call whow api
        if (session.eventData.freeSpinData.currentFreeSpin !== 0) {
          resolve(super.callWhowBluehare(token, result, computeData, 0, 10, total, session, sessionRequestKey, sessionRequest, obj._id));
        } else {
          resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
        }
      } else {
        resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      }
    });
  }
}
module.exports = new PyramidsOfGiza();
