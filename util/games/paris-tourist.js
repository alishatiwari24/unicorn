const { Compute } = require('../compute');
const nj = require('numjs');
const moment = require('moment');
const rng = require('../rng');
const _ = require('lodash');
const gameSession = require('../gamesession');
const { bucket } = require('../../helpers/couchbase');
const gameFunction = require('../../helpers/gamefunction');
const constants = require('../../response/constants');
const whowapi = require('../../api/whowapi');
const gameEvents = require('../../helpers/game-events');

class ParisTourist extends Compute {

  /**
   * [Handles game click event for paris tourist]
   * @param  gameId   unicron slot game id
   * @param  token    user token
   * @param  clientID client id generated on the client side
   * @param  data     user related data fetched from whow
   * @param  gameData game configuratin related data
   */
  handleGameClick(gameId, token, clientID, data, gameData) {
    bucket.get(`${data.userDetails.user.id}::${gameId}`, (err, result) => {
      if (!err) {
        if (result == null) {
          this.createParisTouristUserSession(gameId, clientID, data, gameData);
        } else {
          result.value.clientID = clientID;
          gameSession.buildUserSessionObject(result, data, gameData);
          this.updateSession(result.value, gameId);
        }
      } else {
        this.createParisTouristUserSession(gameId, clientID, data, gameData);
      }
    });
    gameSession.createUserDataAndSessionRequest(data, gameId, token);
  }


  /**
   * This function cerates user session in database
   * it initializes bet array, bet lines forming initial bet
   * for paris tourist game
   */
  createParisTouristUserSession(gameId, clientID, data, gameData) {
    const doc = {
      currentBet: data.userDetails.game.settings.bets[0] || 5,
      currentLines: parseInt(gameData.game.default.gameConfig.selectablePaylines.options[gameData.game.default.gameConfig.selectablePaylines.options.length - 1]) || 20,
      freeSymbolCounts: 0,
      freeSpinBet: 0,
      userDetails: data.userDetails.user,
      freeSpinData: {}
    };
    bucket.upsert(`${data.userDetails.user.id}::${gameId}`, {
      _type: 'userSession',
      event: 'gameClick',
      clientID,
      eventData: doc,
      game: data.userDetails.game,
      _updatedTimestamp: moment.utc()
    }, (err, result) => {
      if (err) {
        console.log(err);
      }
    });
  }

  /**
  * This function is responsible to calculate result for a spin event
  * taking into account
  * view zone, pay lines, pay table
  */
  compute(game, gameId, token, session) {
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));

     // start calculating view zone
    let arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);

     // random numbers are fetched from reel configuration
    const randomNumber = rng.generateRandomNumber(game.default.gameConfig.reelConfig);

     // view zone is calculated below
    const expanding = [];
    const scatter = [];
    super.generateViewZone(randomNumber, noofReels, columnSize, expanding, scatter, game, arrays, session);

    const deepCopyArray = arrays.clone();

     // load pay arrays
    let payArray = game.default.gameConfig.payLines.normalizedPayArray;
    const copyPayArray = JSON.parse(JSON.stringify(payArray));


     // result based on view zone is calculated below
    const results = [];

    return new Promise((resolve, reject) => {
      const currentLines = session.eventData.currentLines;
      const lineArrays = game.default.gameConfig.selectablePaylines[currentLines];
      payArray.forEach((pay) => {
        const object = _.find(lineArrays, o => o === pay._id);
        if (!object) {
          _.remove(copyPayArray, arr => arr._id === pay._id);
        }
      });
      payArray = copyPayArray;
      arrays = deepCopyArray.clone();

      this.freeSpinCompute(scatter, results, game, session)
       .then((results) => {
         if (results.freeSpins) {
           if (_.isEmpty(session.eventData.freeSpinData)) {
             resolve(this.computeForMainGame(session, game, arrays, noofReels, columnSize, payArray, results, deepCopyArray, scatter));
           } else {
             resolve(this.computeForActiveFreeSpins(session, game, arrays, noofReels, columnSize, payArray, results, deepCopyArray, scatter));
           }
         } else if (_.isEmpty(session.eventData.freeSpinData)) {
           // free spin is not active and free spins are not awarded
           // normal proceedings
           resolve(this.computeForMainGame(session, game, arrays, noofReels, columnSize, payArray, results, deepCopyArray, scatter));
         } else {
           resolve(this.computeForActiveFreeSpins(session, game, arrays, noofReels, columnSize, payArray, results, deepCopyArray, scatter));
         }
       });
    });
  }


  computeForActiveFreeSpins(session, game, arrays, noofReels, columnSize, payArray, results, deepCopyArray, scatter) {
    return new Promise((resolve, reject) => {
      const stickingSymbolId = this.pickStickingSymbol(session, game);

      // over ride if sticking positions are already there
      const stickingPositions = session.eventData.freeSpinData.stickingPositions;
      arrays = this.overrideStickingPositions(arrays, stickingPositions, stickingSymbolId);

      // check for new sticking positions
      const currentStickPositions = this.stickingSymbolInViewZone(arrays, stickingSymbolId, noofReels, columnSize);

      // check for scatter
      const scatterLength = this.checkScatterSymbols(arrays, noofReels, columnSize, game);

      Promise.all(
        [
          this.computeWins(arrays, columnSize, payArray, game, results),
          this.scatterPayCompute(arrays, columnSize, payArray, game, results),
          this.wildPayCompute(deepCopyArray, columnSize, payArray, game, results)
        ]).then((values) => {
          resolve({ viewZone: arrays, scatterLength, stickingSymbolId, stickingPositions: currentStickPositions, results });
        }).catch((err) => {
          resolve({ error: 'error in view zone' });
        });
    });
  }


  computeForMainGame(session, game, arrays, noofReels, columnSize, payArray, results, deepCopyArray, scatter) {
    return new Promise((resolve, reject) => {
      Promise.all(
        [
          this.computeWins(arrays, columnSize, payArray, game, results),
          this.scatterPayCompute(arrays, columnSize, payArray, game, results),
          this.wildPayCompute(deepCopyArray, columnSize, payArray, game, results)
        ]).then((values) => {
          resolve({ viewZone: arrays, scatterLength: scatter.length, results });
        }).catch((err) => {
          resolve({ error: 'error in view zone' });
        });
    });
  }


  overrideStickingPositions(arrays, stickingPositions, stickingSymbolId) {
    if (stickingPositions) {
      stickingPositions.forEach((position) => {
        arrays.set(position.j, position.i, stickingSymbolId);
      });
    }
    return arrays;
  }


  stickingSymbolInViewZone(array, stickingSymbolId, noofReels, columnSize) {
    const stickingPositions = [];
    for (let i = 0; i < noofReels; i += 1) {
      for (let j = 0; j < columnSize; j += 1) {
        if (array.get(j, i) === stickingSymbolId) {
          stickingPositions.push({ i, j });
        }
      }
    }
    return stickingPositions;
  }


  /**
  * This function checks if scatter is present in view zone pays or not
  * if scatters are present and scatter pays then wins are calculated subsequentaly
  */
  scatterPayCompute(arrays, columnSize, payArray, game, results) {
    return new Promise((resolve, reject) => {
      const j = 0;
      for (let i = 0; i < columnSize; i++) {
        const symbolId = arrays.get(i, j);
        for (let k = 0; k < payArray.length; k++) {
          const payLine = payArray[k].data;
          const ofAKind = [];
          if (payLine[0] == i) {
            ofAKind.push('test');
            for (let l = 1; l < payLine.length; l++) {
              if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType != 'Scatter' || arrays.get(payLine[l], l) != symbolId) {
                break;
              }
              ofAKind.push('test');
            }
            if (ofAKind.length > 0 && game.default.gameConfig.payTable[symbolId][ofAKind.length]) {
              results.push({ symbolId, winType: `${ofAKind.length} of a kind`, winLineId: payArray[k]._id, multiplier: game.default.gameConfig.payTable[symbolId][ofAKind.length].Multiplier });
            }
          }
        }
      }
      resolve(results);
    });
  }


  /**
  * This function checks if wild present in view zone pays or not
  * if wilds are present and wild pays then wins are calculated subsequentaly
  */
  wildPayCompute(arrays, columnSize, payArray, game, results) {
    return new Promise((resolve, reject) => {
      const j = 0;
      for (let i = 0; i < columnSize; i++) {
        const symbolId = arrays.get(i, j);
        for (let k = 0; k < payArray.length; k++) {
          const payLine = payArray[k].data;
          const ofAKind = [];
          if (payLine[0] == i) {
            ofAKind.push('test');
            for (let l = 1; l < payLine.length; l++) {
              if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType != 'Wild' || arrays.get(payLine[l], l) != symbolId) {
                break;
              }
              ofAKind.push('test');
            }
            if (ofAKind.length > 0 && game.default.gameConfig.payTable[symbolId][ofAKind.length]) {
              results.push({ symbolId, winType: `${ofAKind.length} of a kind`, winLineId: payArray[k]._id, multiplier: game.default.gameConfig.payTable[symbolId][ofAKind.length].Multiplier });
            }
          }
        }
      }
      resolve(results);
    });
  }


  /**
  * This function computes wins based on view zone
  * win is calculated starting from left i.e. from 1st reel
  * Wilds and scatter are taken into consideration while computing wins
  */
  computeWins(arrays, columnSize, payArray, game, results) {
    return new Promise((resolve, reject) => {
      const j = 0;
      for (let i = 0; i < columnSize; i++) {
        for (let k = 0; k < payArray.length; k++) {
          let symbolId = arrays.get(i, j);
          let wildMultiplierCount = 0;
          if (game.default.gameConfig.symbols[symbolId].SymbolType === 'Wild') {
            wildMultiplierCount += game.default.gameConfig.symbols[symbolId].multiplier;
          }
          const payLine = payArray[k].data;
          let ofAKind = [];
          if (payLine[0] === i) {
            ofAKind.push('test');
            for (let l = 1; l < payLine.length; l++) {
              if (game.default.gameConfig.symbols[symbolId].SymbolType === 'Wild' && game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType !== 'Scatter') {
                symbolId = arrays.get(payLine[l], l);
                ofAKind.push('test');
                continue;
              }
              if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType === 'Wild') {
                wildMultiplierCount += game.default.gameConfig.symbols[arrays.get(payLine[l], l)].multiplier;
              }
              if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType !== 'Wild' && arrays.get(payLine[l], l) !== symbolId) {
                break;
              }
              ofAKind.push('test');
            }
            if (ofAKind.length > 0 && game.default.gameConfig.payTable[symbolId][ofAKind.length]) {
              results.push({ symbolId, winType: `${ofAKind.length} of a kind`, winLineId: payArray[k]._id, multiplier: game.default.gameConfig.payTable[symbolId][ofAKind.length].Multiplier, wildMultiplier: wildMultiplierCount });
            }
          }
          ofAKind = [];
        }
      }
      resolve(results);
    });
  }


  freeSpinCompute(scatter, results, game, session) {
    return new Promise((resolve, reject) => {
      if (scatter.length + session.eventData.freeSymbolCounts >= game.default.gameConfig.freeSpin.freeSymbolCounts) {
        results.freeSpins = game.default.gameConfig.freeSpin.noofFS;
      }
      resolve(results);
    });
  }


  spinFortuneWheel(token, gameId) {
    return new Promise((resolve, reject) => {
      gameSession.getUserSession(token, gameId)
      .then((session) => {
        const game = gameFunction.getGame(gameId);
        const pickedSymbolId = this.pickStickingSymbol(session, game.game);
        session.eventData.freeSpinData.stickingSymbolId = pickedSymbolId;
        this.updateSession(session, gameId);
        resolve(pickedSymbolId);
      })
      .catch((err) => {
        console.log(err);
        reject(constants.RES_MESSAGES.UNRECOGNIZED_USER);
      });
    });
  }


  /**
   * This function is responsible to pick a sticking symbol during free games
   * A wheel of 3 high and 2 low symbols must be picked from session
   * A sticking symbol picked randomly out out of 5 symbols that sticks in free spins
   */
  pickStickingSymbol(session, game) {
    if (session.eventData.freeSpinData.stickingSymbolId) {
      return session.eventData.freeSpinData.stickingSymbolId;
    }
    return game.default.gameConfig.freeSpin.freeSpinWheel[Math.floor(Math.random() * game.default.gameConfig.freeSpin.freeSpinWheel.length)];
  }


  /**
   * TODO
   * This function returns 3 high and 2 low paying symbols
   * On user interaction, a symbol must be choosen to stick during free spins
   */
  getWheelForFreeSpin(game, token) {

  }

  /**
  * This function is responsible to generate spin response
  * it includes view zone, wins, user details like wallet, bet array etc
  * necessary render view for the user
  */
  generateSpinResponse(obj, computeData, token, sessionRequestKey, sessionRequest, session) {
    // determine view port
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));

    // create reels for view zone
    const array = computeData.viewZone;
    const viewZone = {};

    for (let i = 0; i < noofReels; i++) {
      const line = [];
      for (let j = 0; j < columnSize; j++) {
        line.push({ symbolId: array.get(j, i), symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType, symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName });
      }
      viewZone[`Reel${i + 1}`] = line;
    }
    viewZone.viewZone = obj.game.default.gameConfig.viewZone;


    // create win document
    const result = {};
    result.viewZone = viewZone;
    result.wins = {};
    result.freeSpinData = {}, result.levelData = {};

    return new Promise((resolve, reject) => {
        // if free spin is present
      if (computeData.results.freeSpins) {
        session.eventData.actualFreeSpinBet = session.eventData.freeSpinBet;
        if (!_.isEmpty(session)) {
          if (_.isEmpty(session.eventData.freeSpinData)) {
            result.freeSpinData.freeSpins = computeData.results.freeSpins;
            result.freeSpinData.currentFreeSpin = 0;
            result.freeSpinData.winAmount = 0;

            session.eventData.freeSpinData = result.freeSpinData;
            session.eventData.freeSpinData.stickingPositions = [];

            session.eventData.actualFreeSpinBet += (obj.game.default.gameConfig.freeSpin.freeSymbolCounts - session.eventData.freeSymbolCounts) * ((session.eventData.currentBet * session.eventData.currentLines) / obj.game.default.gameConfig.freeSpin.freeSymbolCounts);
            session.eventData.freeSymbolCounts = Math.abs(obj.game.default.gameConfig.freeSpin.freeSymbolCounts - session.eventData.freeSymbolCounts - computeData.scatterLength);
            if (session.eventData.freeSymbolCounts === 0) {
              session.eventData.freeSpinBet = 0;
            }
          } else {
            result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;

            session.eventData.freeSymbolCounts += computeData.scatterLength;

            session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
            session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
          }
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
          if (session.eventData.freeSpinData.isFreeSpinAwarded) {
            delete session.eventData.freeSpinData.isFreeSpinAwarded;
            delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
          }

          if (computeData.scatterLength && computeData.scatterLength > 0) {
            if (session.eventData.freeSymbolCounts > 0) {
              session.eventData.freeSpinBet = parseInt(session.eventData.freeSymbolCounts * (session.eventData.actualFreeSpinBet / obj.game.default.gameConfig.freeSpin.freeSymbolCounts));
            }
          }

          session.event = 'spin';
          session._updatedTimestamp = moment.utc();

          super.updateSessionAndSessionRequest(session, obj._id, sessionRequestKey, sessionRequest);
        }
        result.isSessionUpdated = true;
      }

      if (!_.isEmpty(session.eventData.freeSpinData)) {
        result.freeSpinData.stickingSymbolId = computeData.stickingSymbolId;
        // console.log(computeData.stickingSymbolId, 'sticking symbol id');
        // console.log(computeData.stickingPositions, 'computeData.stickingPositions');
        session.eventData.freeSpinData.stickingSymbolId = computeData.stickingSymbolId;
        session.eventData.freeSpinData.stickingPositions = computeData.stickingPositions;
      }

        // total win amount calculated based on wins from pay line
      let total = 0;
      computeData.results.forEach((result) => {
        if (!_.isEmpty(session.eventData.freeSpinData)) {
          result.winAmount = result.multiplier * session.eventData.actualFreeSpinBet / obj.game.default.gameConfig.payLines.PayArray.length;
        } else {
          result.winAmount = result.multiplier * session.eventData.currentBet;
        }
        if (result.wildMultiplier !== 0) {
          result.winAmount *= result.wildMultiplier;
        }
        total += result.winAmount;
        delete result.multiplier;
        delete result.wildMultiplier;
      });
      result.wins.winCoins = total;
      result.wins.winLinesCount = computeData.results.length;
      result.wins.winLines = computeData.results;
      result.wins.freeSymbolCounts = computeData.scatterLength;
      result.wins.currentFreeSymbols = session.eventData.freeSymbolCounts;
      result.wins.totalFreeSymbolCounts = session.eventData.freeSymbolCounts + computeData.scatterLength;

      // Temp code here
      result.actualFreeSpinBet = session.eventData.actualFreeSpinBet;

      //
      if (!computeData.results.freeSpins && !_.isEmpty(session.eventData.freeSpinData) && computeData.scatterLength && computeData.scatterLength > 0) {
        session.eventData.freeSpinBet += parseInt(computeData.scatterLength * (session.eventData.actualFreeSpinBet / obj.game.default.gameConfig.freeSpin.freeSymbolCounts));
      } else if (computeData.scatterLength && computeData.scatterLength > 0) {
        session.eventData.freeSpinBet += parseInt(computeData.scatterLength * ((session.eventData.currentBet * session.eventData.currentLines) / obj.game.default.gameConfig.freeSpin.freeSymbolCounts));
      }
      result.wins.totalFreeSpinBet = session.eventData.freeSpinBet;
        // put user session data
      result.session = {};
      result.session.sessionRequestKey = sessionRequestKey;
      result.session.sessionRequest = sessionRequest;
      /**
       * notify of all the symbols present in view zone
       */
      gameEvents.notifyGameEvents(obj, token, session, result, computeData.results.freeSpins);

      // mask view zone here
      result.viewZone = super.maskViewZone(result.viewZone, obj);
      if (_.isEmpty(session.eventData.freeSpinData)) {
          // no free spins
        resolve(this.callWhow(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
          // some free spins remaining
        result.freeSpinData = session.eventData.freeSpinData;

        if (computeData.results.freeSpins) {
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
        }

        if (!computeData.results.freeSpins) {
          result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
        }

        if (result.wins.winCoins > 0) {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
          session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
        } else {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
        }
          // call whow api
        if (computeData.stickingPositions && computeData.stickingPositions.length > 0) {
          const sticks = JSON.parse(JSON.stringify(computeData.stickingPositions));
          result.stickingPositions = [];

          sticks.forEach((stick) => {
            const reelNumber = stick.i + 1;
            const columnNumber = stick.j;

            result.stickingPositions.push({ reel: `reel${reelNumber}`, columnNumber, isSticking: true });
          });
        }
        // call whow api
        if (session.eventData.freeSpinData.currentFreeSpin === 0) {
        // free spins just awarded
          // result.isSessionUpdated = false;
          session.eventData.freeSpinData.winAmount = 0;
          resolve(this.callWhowForFirstFreeSpin(token, result, computeData, session.eventData.actualFreeSpinBet, 0, total, session, sessionRequestKey, sessionRequest, obj._id));
        } else {
          resolve(this.callBet(token, 0, total, 10, result, session, sessionRequestKey, sessionRequest, obj._id));
        }
      } else {
          // free spin object exists and there are no free spins remaining
        resolve(this.callWhow(token, result, computeData, session.eventData.actualFreeSpinBet, 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      }
    });
  }

  /**
   * updates user's session with free spin data, wallet chips etc.
   * @param  session session schema
   * @param  gameId  unicorn slot game id
   */
  updateSession(session, gameId) {
    bucket.upsert(`${session.eventData.userDetails.id}::${gameId}`, session, (err, result) => {
      if (err) {
        console.log(err);
      }
    });
  }

  /**
  * updates user game session with details of a spin result
  * it may include free spin if won in game spin
  * and updated wallet chips as a result of whow api call bet/play
  */
  gameSpin(gameId, token, docDetails) {
    docDetails._updatedTimestamp = moment.utc();
    gameSession.getUserSession(token, gameId)
        .then((session) => {
          session.game = docDetails.game;
          session.eventData.userDetails.wallet = docDetails.wallet;
          session.gamble = docDetails.gamble;
          session.eventData.freeSymbolCounts += docDetails.wins.freeSymbolCounts;
          session.eventData.freeSpinBet = docDetails.wins.totalFreeSpinBet;

          if (!_.isEmpty(session)) {
            session.event = 'spin';
            if (!_.isEmpty(docDetails.freeSpinData) && docDetails.freeSpinData.isFreeSpinAwarded) {
              delete docDetails.freeSpinData.isFreeSpinAwarded;
              delete docDetails.freeSpinData.noofFreeSpinsAwarded;
            }
            session.eventData.freeSpinData = docDetails.freeSpinData;
            if (!_.isEmpty(session.eventData.freeSpinData) && session.eventData.freeSpinData.currentFreeSpin == session.eventData.freeSpinData.freeSpins) {
              const totalWin = session.eventData.freeSpinData.winAmount;
              session.gamble.winAmount = totalWin;
              session.eventData.freeSpinData = {};
            }
          }


          const sessionRequestKey = docDetails.session.sessionRequestKey;
          const sessionRequest = docDetails.session.sessionRequest;
          sessionRequest.status = 'ready',
          sessionRequest.spinCompletedTimestamp = moment().utc();
          this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);
          this.updateSession(session, gameId);
        }).catch((err) => {
          console.log(err);
        });
  }


  /**
  * updates user's session and session requests
  * created when game click took place
  */
  updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest) {
    bucket.upsert(`${session.eventData.userDetails.id}::${gameId}`, session, (err, result) => {
      if (!err) {
        this.updateSessionRequestDoc(sessionRequestKey, sessionRequest);
      }
    });
  }


  /**
  * updates user's session request document with provided document
  * no otimistic database concurreny is used in this update
  */
  updateSessionRequestDoc(sessionRequestKey, sessionRequestDoc) {
    bucket.upsert(sessionRequestKey, sessionRequestDoc, (err, result) => {
      if (err) {
        console.log(err);
      }
    });
  }


  /**
   * Whow bet api is called in case user wins
   */
  whowBetApi(roundId, token, betAmount, total, virtualAmount, session) {
    return new Promise((resolve, reject) => {
      whowapi.bet(token, {
        roundId,
        betAmount,
        winAmount: total,
        virtualAmount
      }).then((response) => {
        response = JSON.parse(response);
        session.eventData.userDetails.wallet = response.payload.user.wallet;
        session.game = response.payload.game;
        resolve(response);
      }).catch((err) => {
        resolve(err);
      });
    });
  }


  /**
   * Whow close api is called in case when user looses
   * when user looses previous round is close
   */
  whowCloseApi(roundId, token, total, session) {
    return new Promise((resolve, reject) => {
      whowapi.close(token, {
        winAmount: total,
        roundId
      })
      .then((response) => {
        response = JSON.parse(response);
        session.eventData.userDetails.wallet = response.payload.user.wallet;
        session.game = response.payload.game;

        resolve(response);
      })
      .catch((err) => {
        console.log(err);
        resolve(err);
      });
    });
  }


  /**
   * This function closes previous active round and calls a new bet api
   * This function is used when user wins in main game
   */
  callWhowBet(token, result, computeData, betAmount, virtualAmount, total, session) {
    return new Promise((resolve, reject) => {
      this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
      .then((response) => {
        resolve(this.callBet(token, betAmount, total, virtualAmount, result, session));
      });
    });
  }


  /**
   * This function is overriden from the super class
   * Whow api is called at the end of each spin
   * informing the wins to whow
   */
  callWhow(token, result, computeData, betAmount, virtualAmount, total, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve, reject) => {
      if (computeData.results.length > 0) {
        if (session.gamble && session.gamble.id) {
          this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
          .then((response) => {
            resolve(this.callBet(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
          });
        } else {
          resolve(this.callBet(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
        }
      } else if (session.gamble && session.gamble.id) {
        this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
        .then((response) => {
          resolve(this.callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
        });
      } else {
        resolve(this.callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
      }
    });
  }


  /**
   * This function is called when the first free spin is played
   * Purpose of this function is to close any winnings that was awarded before free spin was triggered
   */
  callWhowForFirstFreeSpin(token, result, computeData, betAmount, virtualAmount, total, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve, reject) => {
      if (computeData.results.length > 0) {
        this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
        .then((data) => {
          session.gamble.winAmount = 0;
          this.callBet(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId)
          .then((results) => {
            whowapi.close(token, {
              winAmount: total,
              roundId: results.gamble.id
            });
            resolve(results);
          });
        });
      } else {
        resolve(this.callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
      }
    });
  }


  /**
   * This is the actual whow api call function
   * bet api is called when user wins
   */
  callBet(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve, reject) => {
      whowapi.bet(token, {
        betAmount,
        winAmount: total,
        virtualAmount
      }).then((response) => {
        response = JSON.parse(response);

        if (response.status && response.status === 110) {
          sessionRequest.status = 'ready';
          session.gamble.winAmount = 0;
          session.gamble.count = 0;
          this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);
          resolve({ error: constants.RES_MESSAGES.NOT_ENOUGH_BALANCE, errorCode: constants.RES_ERROR_CODES.NOT_ENOUGH_BALANCE });
        } else {
          result.gamble = response.payload.round;
          if (!session.gamble || !session.gamble.history) {
            result.gamble.history = [];
          } else {
            result.gamble.history = session.gamble.history;
          }
          result.gamble.winAmount = total;
          result.gamble.count = 0;
          result.levelData.level = response.payload.user.level;
          result.levelData.levelProgress = response.payload.user.levelProgress;
          result.wallet = response.payload.user.wallet;
          result.game = response.payload.game;
          result.game.betAmount = (session.eventData.currentBet * session.eventData.currentLines);
          result.jackpot = response.payload.user.jackpotData;
          resolve(result);
        }
      }).catch((err) => {
        console.log(err);
        resolve(result);
      });
    });
  }


  /**
   * This is the actual whow api call function
   * play api is called when user does not win
   */
  callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve, reject) => {
      whowapi.play(token, {
        betAmount,
        winAmount: total,
        virtualAmount
      }).then((response) => {
        response = JSON.parse(response);

        if (response.status && response.status === 110) {
          sessionRequest.status = 'ready';
          if (session.gamble && !_.isEmpty(session.gamble)) {
            session.gamble.winAmount = 0;
            session.gamble.count = 0;
          }
          this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);
          resolve({ error: constants.RES_MESSAGES.NOT_ENOUGH_BALANCE, errorCode: constants.RES_ERROR_CODES.NOT_ENOUGH_BALANCE });
        } else {
          result.gamble = response.payload.round;
          if (!session.gamble || !session.gamble.history) {
            result.gamble.history = [];
          } else {
            result.gamble.history = session.gamble.history;
          }
          result.gamble.count = 0;
          result.levelData.level = response.payload.user.level;
          result.levelData.levelProgress = response.payload.user.levelProgress;
          result.wallet = response.payload.user.wallet;
          result.game = response.payload.game;
          result.game.betAmount = (session.eventData.currentBet * session.eventData.currentLines);
          result.jackpot = response.payload.user.jackpotData;
          resolve(result);
        }
      }).catch((err) => {
        console.log(err);
        resolve(result);
      });
    });
  }


  /**
   * This function hadles the gamble request for unicorn slot
   * Validations related gamble is done and gamble winings are determined
   */
  handleGambleRequest(token, gameId, clientId, pickedCard) {
    return new Promise((resolve, reject) => {
      if (!pickedCard && pickedCard != null && pickedCard.toLowerCase() !== constants.GAMBLE.RED && pickedCard.toLowerCase() !== constants.GAMBLE.BLACK) {
        resolve({ error: constants.RES_MESSAGES.INVALID_GAMBLE_CARD, errorCode: constants.RES_ERROR_CODES.INVALID_GAMBLE_CARD });
      } else {
        /**
        * Step 1: Check if free spin is not active
        */
        gameSession.getUserSession(token, gameId)
        .then((session) => {
          if (session.clientID === clientId) {
            if (!_.isEmpty(session.eventData.freeSpinData)) {
              // free spin is active
              resolve({ error: constants.RES_MESSAGES.GAMBLE_NOT_ACTIVE, errorCode: constants.RES_ERROR_CODES.GAMBLE_NOT_ACTIVE });
            } else if (!session.gamble.winAmount > 0) {
              /**
              * Step 2: get the last win stored in session
              * if there is no last win, gamble cannot be availed
              */
              resolve({ error: constants.RES_MESSAGES.GAMBLE_NOT_ACTIVE, errorCode: constants.RES_ERROR_CODES.GAMBLE_NOT_ACTIVE });
            } else {
              const pickedNumber = Math.floor(Math.random() * constants.GAMBLE.SECRET_OF_AMUN_RANDOM_RANGE);
              let isRed = false;
              let card = constants.GAMBLE.BLACK;
              if ((pickedNumber >= 0 && pickedNumber <= 13) || (pickedNumber >= 27 && pickedNumber <= 39)) {
                isRed = true;
                card = constants.GAMBLE.RED;
              }
              /**
              * Step 3: User either wins or looses based on randomness in server
              * - If user wins, winning is doubled
              * - If user looses, he looses even the last win amount
              */
              session.gamble.history.push(card);
              if (session.gamble.history.length > 5) {
                session.gamble.history.shift();
              }

              if ((pickedCard.toLowerCase() === constants.GAMBLE.RED && isRed) || (pickedCard.toLowerCase() === constants.GAMBLE.BLACK && !isRed)) {
                // user wins
                session.gamble.winAmount = session.gamble.winAmount * 2;
                session.gamble.betAmount = session.gamble.winAmount;
                session.gamble.count += 1;
                // call bet whow api
                this.whowBetApi(session.gamble.id, token, 0, session.gamble.winAmount, session.gamble.betAmount, session)
                .then((response) => {
                  if (session.gamble.count === constants.GAMBLE.SECRET_OF_AMUN_MAX_GAMBLE) {
                    // max gabmle
                    this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
                    .then((data) => {
                      session.gamble.count = 0;
                      session.gamble.winAmount = 0;
                      this.updateSession(session, gameId);
                      resolve({ gambleCount: session.gamble.count, gambleWon: true, gambleTotalAmount: session.gamble.winAmount, gambleHistory: session.gamble.history, wallet: session.eventData.userDetails.wallet, isGambleAvailable: false });
                    });
                  } else {
                    this.updateSession(session, gameId);
                    resolve({ gambleCount: session.gamble.count, gambleWon: true, gambleTotalAmount: session.gamble.winAmount, gambleBetAmount: session.gamble.betAmount, gamblePotentialWinAmount: session.gamble.betAmount * 2, gambleHistory: session.gamble.history, wallet: session.eventData.userDetails.wallet, isGambleAvailable: true });
                  }
                });
              } else {
                // user looses
                // call close whow api
                this.whowCloseApi(session.gamble.id, token, 0, session)
                .then((data) => {
                  session.gamble.count = 0;
                  session.gamble.winAmount = 0;
                  this.updateSession(session, gameId);
                  resolve({ gambleCount: session.gamble.count, gambleWon: false, gambleTotalAmount: session.gamble.winAmount, gambleHistory: session.gamble.history, wallet: session.eventData.userDetails.wallet, isGambleAvailable: false });
                });
              }
            }
          } else {
            resolve({ error: constants.RES_MESSAGES.INVALID_CLIENT, errorCode: constants.RES_ERROR_CODES.INVALID_CLIENT });
          }
        })
        .catch((err) => {
          console.log(err);
          resolve({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
        });
      }
    });
  }


  /**
   * This function collects the win amount of current spin
   * and also the current win amount when gamble is active
   */
  pickGamble(token, gameId, clientId) {
    return new Promise((resolve, reject) => {
      gameSession.getUserSession(token, gameId)
      .then((session) => {
        if (session.clientID === clientId) {
          if (!_.isEmpty(session.eventData.freeSpinData)) {
            resolve({ error: constants.RES_MESSAGES.GAMBLE_NOT_ACTIVE, errorCode: constants.RES_ERROR_CODES.GAMBLE_NOT_ACTIVE });
          } else if (!session.gamble.winAmount > 0) {
            resolve({ error: constants.RES_MESSAGES.NO_WINS_TO_COLLECT, errorCode: constants.RES_ERROR_CODES.NO_WINS_TO_COLLECT });
          } else {
            this.whowCloseApi(session.gamble.id, token, session.gamble.winAmount, session)
            .then((data) => {
              if (session.gamble.count > 0) {
                session.gamble.count = 0;
                session.gamble.winAmount = 0;
                this.updateSession(session, gameId);
                resolve({ gambleCount: session.gamble.count, gambleWon: true, gambleTotalAmount: session.gamble.winAmount, gambleHistory: session.gamble.history, wallet: session.eventData.userDetails.wallet, isGambleAvailable: false });
              } else {
                const totalWin = session.gamble.winAmount;
                session.gamble.winAmount = 0;
                this.updateSession(session, gameId);
                resolve({ collectAmount: totalWin, wallet: session.eventData.userDetails.wallet });
              }
            });
          }
        } else {
          resolve({ error: constants.RES_MESSAGES.INVALID_CLIENT, errorCode: constants.RES_ERROR_CODES.INVALID_CLIENT });
        }
      }).catch((err) => {
        console.log(err);
        resolve({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
      });
    });
  }


  checkScatterSymbols(arrays, noofReels, columnSize, game) {
    let len = 0;
    for (let i = 0; i < noofReels; i += 1) {
      for (let j = 0; j < columnSize; j += 1) {
        if (game.default.gameConfig.symbols[arrays.get(j, i)].SymbolType == 'Scatter') {
          len += 1;
        }
      }
    }
    return len;
  }
}
module.exports = new ParisTourist();
