const { bucket } = require('../../helpers/couchbase');
const moment = require('moment');
const _ = require('lodash');
const { Compute } = require('../compute');
const gameSession = require('../gamesession');
const gameEvents = require('../../helpers/game-events');

/**
 * Unicorn Slot specific features are implemented in this class
 * saving current reel in session that is to be used is consfigured in this class
 * This class would also be reponsible to update game related user sessions
 */
class UnicornSlot extends Compute {

  /**
   * [Handles game click event for unicorn slot]
   * @param  gameId   unicorn slot game id
   * @param  token    user token
   * @param  clientID client id generated on the client side
   * @param  data     user related data fetched from whow
   * @param  gameData game configuratin related data
   */
  handleGameClick(gameId, token, clientID, data, gameData) {
    bucket.get(`${data.userDetails.user.id}::${gameId}`, (err, result) => {
      if (!err) {
        if (result === null) {
          this.createUnicornSlotSession(gameId, clientID, data, gameData);
        } else {
          result.value.clientID = clientID;
          if (result.value.eventData.showBetWarning === undefined) {
            result.value.eventData.showBetWarning = true;
          }
          gameSession.buildUserSessionObject(result, data, gameData);
          super.updateSession(result.value, gameId);
        }
      } else {
        this.createUnicornSlotSession(gameId, clientID, data, gameData);
      }
    });
    gameSession.createUserDataAndSessionRequest(data, gameId, token);
  }


  /**
   * This function cerates user session in database
   * it initializes bet array, bet lines forming initial bet
   * for knight and dragons game
   */
  createUnicornSlotSession(gameId, clientID, data, gameData) {
    const doc = {
      currentBet: data.userDetails.game.settings.bets[0] || 5,
      currentLines: parseInt(gameData.game.default.gameConfig.selectablePaylines.options[gameData.game.default.gameConfig.selectablePaylines.options.length - 1]) || 20,
      currentReelSet: 0,
      userDetails: data.userDetails.user,
      freeSpinData: {},
      showBetWarning: true
    };
    bucket.upsert(`${data.userDetails.user.id}::${gameId}`, {
      _type: 'userSession',
      event: 'gameClick',
      clientID,
      eventData: doc,
      game: data.userDetails.game,
      _updatedTimestamp: moment.utc()
    }, (err) => {
      if (err) {
        console.log(err);
      }
    });
  }


  /**
   * This function decides if respin is triggered or not
   * pick a random value from reSpinWeight array, id it's a 1, trigger respin
   */
  makeReSpinTriggerDecision(reSpinWeight) {
    return (reSpinWeight[Math.floor(Math.random() * reSpinWeight.length)] === 1);
  }


  /**
  * updates user's current bet in database
  */
  updateUserCurrentBet(result, bet, gameId) {
    return new Promise((resolve, reject) => {
      let reelSet = 5;
      if (result.eventData.previousBet === bet) {
        reelSet = result.eventData.previousReelSet;
      }
      result.eventData.currentBet = bet;
      result.eventData.currentReelSet = reelSet;
      bucket.upsert(`${result.eventData.userDetails.id}::${gameId}`, result, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }


  /**
  * This function is responsible to update showBetWarning flag for unicorn slot game
  */
  doNotShowPopUp(result, gameId) {
    return new Promise((resolve, reject) => {
      result.eventData.showBetWarning = false;
      bucket.upsert(`${result.eventData.userDetails.id}::${gameId}`, result, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }


  /**
   * update user session when there are no free spins in unicorn slot
   * @param  gameId     unicorn slot game id
   * @param  token      user token
   * @param  docDetails user session schema
   * @param  gameData   game configuration schema
   */
  gameSessionUpdateMainGame(gameId, token, docDetails, gameData) {
    docDetails._updatedTimestamp = moment.utc();
    gameSession.getUserSession(token, gameId)
    .then((session) => {
      session.gamble = docDetails.gamble;
      session.game = docDetails.game;
      session.eventData.userDetails.wallet = docDetails.wallet;

      const updateReelSet = true;
      if (!_.isEmpty(session)) {
        session.event = 'spin';
        if (!_.isEmpty(docDetails.freeSpinData) && docDetails.freeSpinData.isFreeSpinAwarded) {
          delete docDetails.freeSpinData.isFreeSpinAwarded;
          delete docDetails.freeSpinData.noofFreeSpinsAwarded;
        }
        session.eventData.freeSpinData = docDetails.freeSpinData;
        if (session.eventData.freeSpinData.currentFreeSpin && session.eventData.freeSpinData.currentFreeSpin == session.eventData.freeSpinData.freeSpins) {
          const totalWin = session.eventData.freeSpinData.winAmount;
          session.gamble.winAmount = totalWin;
          session.eventData.freeSpinData = {};
        }
      }

      if (updateReelSet) {
        if (_.isEmpty(session.eventData.freeSpinData)) {
          session.eventData.currentReelSet = this.getReelSet(session.eventData.currentReelSet, gameData.game.default.gameConfig.reelConfig);
        } else {
          session.eventData.freeSpinData.currentReelSet = this.getReelSet(session.eventData.freeSpinData.currentReelSet, gameData.game.default.gameConfig.reelConfig);
        }
      }

      session.eventData.previousBet = session.eventData.currentBet;
      session.eventData.previousReelSet = session.eventData.currentReelSet;
      const sessionRequestKey = docDetails.session.sessionRequestKey;
      const sessionRequest = docDetails.session.sessionRequest;
      sessionRequest.status = 'ready';
      sessionRequest.spinCompletedTimestamp = moment().utc();
      this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);
      this.updateSession(session, gameId);
    }).catch((err) => {
      console.log(err);
    });
  }


  /**
   * getting reel configuration when spin is done on unicorn slot
   * with every spin a new reel configuration is picked from game schema
   * @param  session sessoin schema
   * @param  game    game configuration schema
   * @return         reel configuration to spin on
   */
  getSpinReelConfiguration(session, game) {
    let reelConf = game.game.default.gameConfig.reelConfig;
    let nextConfig = session.eventData.currentReelSet - 1;
    if (!_.isEmpty(session.eventData.freeSpinData) && (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0)) {
      nextConfig = session.eventData.freeSpinData.currentReelSet - 1;
      game.game.default.gameConfig.freeSpin.reelConfig.NumberOfReels = game.game.default.gameConfig.reelConfig.NumberOfReels;
      reelConf = game.game.default.gameConfig.freeSpin.reelConfig;
    }
    if (nextConfig < 0) {
      nextConfig = reelConf.NumberOfReels - 1;
    }
    reelConf[`Reel${nextConfig + 1}`] = game.game.default.gameConfig.specialFeatures.data.Reel;
    reelConf.wildReel = `reel${nextConfig + 1}`;
    return reelConf;
  }


  /**
   * fetches next index from game schema which is to be considered for spin when there are no free spins
   * user's current session is to be considered here
   * @param  session  session schema
   * @param  gameData game schema
   * @return          next index of game schema
   */
  getReelSet(reelSet, reelConfig) {
    let nextConfig = reelSet - 1;
    if (nextConfig < 0) {
      nextConfig = reelConfig.NumberOfReels - 1;
    }
    return nextConfig;
  }


  /**
  * This function is responsible to generate spin response
  * it includes view zone, wins, user details like wallet, bet array etc
  * necessary render view for the user
  */
  generateSpinResponse(obj, computeData, token, sessionRequestKey, sessionRequest, session) {
    // determine view port
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));

    // create reels for view zone
    const array = computeData.viewZone;
    const viewZone = {};

    for (let i = 0; i < noofReels; i += 1) {
      const line = [];
      for (let j = 0; j < columnSize; j += 1) {
        line.push({ symbolId: array.get(j, i), symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType, symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName });
      }
      viewZone[`Reel${i + 1}`] = line;
    }
    viewZone.viewZone = obj.game.default.gameConfig.viewZone;


    // create win document
    const result = {};
    result.viewZone = viewZone;
    result.wins = {};
    result.freeSpinData = {};
    result.levelData = {};
    result.wasReSpin = computeData.wasReSpin;
    result.wildReel = computeData.wildReel;
    return new Promise((resolve) => {
        // if free spin is present
      if (computeData.results.freeSpins) {
        if (!_.isEmpty(session)) {
          if (_.isEmpty(session.eventData.freeSpinData)) {
            result.freeSpinData.freeSpins = computeData.results.freeSpins;
            result.freeSpinData.pickedSymbol = computeData.pickedSymbol;
            result.freeSpinData.currentFreeSpin = 0;
            result.freeSpinData.winAmount = 0;

            session.eventData.freeSpinData = result.freeSpinData;
            session.eventData.freeSpinData.currentReelSet = 0;
            result.isSessionUpdated = true;
          } else {
            result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;

            session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
            session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
            session.eventData.freeSpinData.currentReelSet = session.eventData.freeSpinData.currentReelSet - 1;
            if (session.eventData.freeSpinData.currentReelSet < 0) {
              session.eventData.freeSpinData.currentReelSet = noofReels - 1;
            }
          }
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
          if (session.eventData.freeSpinData.isFreeSpinAwarded) {
            delete session.eventData.freeSpinData.isFreeSpinAwarded;
            delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
          }
          session.event = 'spin';
          session._updatedTimestamp = moment.utc();
          super.updateSession(session, obj._id);
        }
        result.isSessionUpdated = true;
      }
      // total win amount calculated based on wins from pay line
      let total = 0;
      computeData.results.forEach((resultObj) => {
        resultObj.winAmount = resultObj.multiplier * session.eventData.currentBet;
        total += (resultObj.multiplier * session.eventData.currentBet);
        delete resultObj.multiplier;
      });

      result.wins.winCoins = total;
      result.wins.winLinesCount = computeData.results.length;
      result.wins.winLines = JSON.parse(JSON.stringify(computeData.results));

      if (result.wins.winLines.freeSpins) {
        delete result.wins.winLines.freeSpins;
      }

        // put user session data
      result.session = {};
      result.session.sessionRequestKey = sessionRequestKey;
      result.session.sessionRequest = sessionRequest;
      /**
       * notify of all the symbols present in view zone
       */
      gameEvents.notifyGameEvents(obj, token, session, result, computeData.results.freeSpins);

      // mask view zone here
      result.viewZone = super.maskViewZone(result.viewZone, obj);
      if (_.isEmpty(session.eventData.freeSpinData)) {
          // no free spins
        resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
          // some free spins remaining
        result.freeSpinData = session.eventData.freeSpinData;

        if (computeData.results.freeSpins) {
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
        }

        if (!computeData.results.freeSpins) {
          result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
        }

        if (result.wins.winCoins > 0) {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
          session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
        } else {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
        }
          // call whow api
        if (session.eventData.freeSpinData.currentFreeSpin === 0) {
          // free spins just awarded
          resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
        } else {
          resolve(super.callWhowBluehare(token, result, computeData, 0, 10, total, session, sessionRequestKey, sessionRequest, obj._id));
        }
      } else {
          // free spin object exists and there are no free spins remaining
        resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      }
    });
  }
}
module.exports = new UnicornSlot();
