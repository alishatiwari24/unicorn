/* eslint-disable max-statements, max-params, prefer-destructuring, no-ternary, multiline-ternary, no-console */
const nj = require('numjs');
const rng = require('../rng');
const { bucket } = require('../../helpers/couchbase');
const moment = require('moment');
const _ = require('lodash');
const constants = require('../../response/constants');
const { Compute } = require('../compute');
const gameSession = require('../gamesession');
const reelMask = require('../../config/reel-mask-config');
const gameEvents = require('../../helpers/game-events-deep-seas');

/**
 * DeepSeas specific features are implemented in this class
 * This class would be reponsible to update game related user sessions
 * This class would be reponsible to trigger special feature of DeepSeas (viewZone combine feature)
 * it generates 2 viewZones on each spin, and calculates for win on both viewZones independently
 * and it checks if the feature is triggered, if yes, it combines both viewzones, and calculates for win for both viewzones
 */
class DeepSeas extends Compute {

  /**
  * This function handles game click event from controller
  * @param  gameId   deep seas game id
  * @param  token    user token
  * @param  clientID client id generated on the client side
  * @param  data     user related data fetched from whow
  * @param  gameData game configuratin related data
  */
  handleGameClick(gameId, token, clientID, data, gameData) {
    bucket.get(`${data.userDetails.user.id}::${gameId}`, (err, result) => {
      if (!err) {
        if (result == null) {
          gameSession.createGameSessionObject(gameId, clientID, data, gameData, constants.DEEP_SEAS.NOOF_LINES);
        } else {
          result.value.clientID = clientID;
          gameSession.buildUserSessionObject(result, data, gameData, constants.DEEP_SEAS.NOOF_LINES);
          super.updateSession(result.value, gameId);
        }
      } else {
        gameSession.createGameSessionObject(gameId, clientID, data, gameData, constants.DEEP_SEAS.NOOF_LINES);
      }
    });
    gameSession.createUserDataAndSessionRequest(data, gameId, token);
  }


  /**
   * This function cerates user session in database
   * it initializes bet array, bet lines forming initial bet
   * for deep seas game
   */
  createDeepSeasSession(gameId, clientID, data) {
    const doc = {
      currentBet: data.userDetails.game.settings.bets[0] || 5,
      currentLines: constants.DEEP_SEAS.NOOF_LINES,
      userDetails: data.userDetails.user,
      freeSpinData: {}
    };
    bucket.upsert(`${data.userDetails.user.id}::${gameId}`, {
      _type: 'userSession',
      event: 'gameClick',
      clientID,
      eventData: doc,
      game: data.userDetails.game,
      _updatedTimestamp: moment.utc()
    }, (err, result) => {
      if (err) {
        console.log(err);
      }
    });
  }


  /**
  * This function is used to build response for game click event
  * game data along with user data for subsequent  game events calls are passed to client
  */
  generateGameClickResponse(data, token, gameId) {
    const result = {};
    result.game = {};
    result.game.gameId = data._id;
    result.game.viewZone = data.game.default.gameConfig.viewZone;

    // there will be two viewZones, so reels and reels2 would be there in response
    result.game.reels = {};
    result.game.reels2 = {};
    result.game.reels.noofReels = data.game.default.gameConfig.reelConfigSet.ReelL.NumberOfReels;
    result.game.reels2.noofReels = data.game.default.gameConfig.reelConfigSet.ReelR.NumberOfReels;

    // calculate left viewzone
    for (let i = 0; i < data.game.default.gameConfig.reelConfigSet.ReelL.NumberOfReels; i += 1) {
      const columnSize = parseInt(data.game.default.gameConfig.viewZone.toString().charAt(0));
      const reelArray = [];
      const randomNumber = Math.floor(Math.random() * data.game.default.gameConfig.reelConfigSet.ReelL[`Reel${i + 1}`].NumberOfRows);
      for (let j = 0; j < columnSize; j += 1) {
        // do not give away reel configuration, mask initiail view
        reelArray.push(data.game.default.gameConfig.reelConfigSet.ReelL[`Reel${i + 1}`].SymbolDistribution[(randomNumber + (j * Math.floor(Math.random() * 10))) % data.game.default.gameConfig.reelConfigSet.ReelL[`Reel${i + 1}`].NumberOfRows].SymbolId);
      }
      result.game.reels[`Reel${i + 1}`] = reelArray;
    }

    // calculate right viewzone
    for (let i = 0; i < data.game.default.gameConfig.reelConfigSet.ReelR.NumberOfReels; i += 1) {
      const columnSize = parseInt(data.game.default.gameConfig.viewZone.toString().charAt(0));
      const reelArray = [];
      const randomNumber = Math.floor(Math.random() * data.game.default.gameConfig.reelConfigSet.ReelR[`Reel${i + 1}`].NumberOfRows);
      for (let j = 0; j < columnSize; j += 1) {
        // do not give away reel configuration, mask initiail view
        reelArray.push(data.game.default.gameConfig.reelConfigSet.ReelR[`Reel${i + 1}`].SymbolDistribution[(randomNumber + (j * Math.floor(Math.random() * 10))) % data.game.default.gameConfig.reelConfigSet.ReelR[`Reel${i + 1}`].NumberOfRows].SymbolId);
      }
      result.game.reels2[`Reel${i + 1}`] = reelArray;
    }

    // add payArray66 and payArray36 in response
    result.game.payArray = data.game.default.gameConfig.payLines.PayArray;
    result.game.payArray66 = data.game.default.gameConfig.payLines66.PayArray;
    result.game.payTable = data.game.default.gameConfig.payTable;
    result.game.bigWins = data.game.default.gameConfig.bigWins;
    result.game.symbols = data.game.default.gameConfig.symbols;
    result.game.stage = data.game.default.gameConfig.stage;
    result.game.version = data.game.default.gameConfig.versionName;
    result.userDetails = data.userDetails;
    result.userDetails.selectablePaylines = data.game.default.gameConfig.selectablePaylines.options;
    result.userDetails.currentBet = data.userDetails.game.settings.bets[0] || 5;
    result.userDetails.currentLines = constants.DEEP_SEAS.NOOF_LINES;
    result.userDetails.totalBet = (result.userDetails.currentBet * result.userDetails.currentLines);
    return new Promise((resolve) => {
      gameSession.getUserSession(token, gameId)
      .then((session) => {
        result.userDetails.currentBet = session.eventData.currentBet;
        result.userDetails.currentLines = session.eventData.currentLines;
        result.userDetails.freeSpinData = session.eventData.freeSpinData;
        result.userDetails.totalBet = (result.userDetails.currentBet * result.userDetails.currentLines);
        resolve(result);
      }).catch(() => {
        result.userDetails.currentBet = data.userDetails.game.settings.bets[0] || 5;
        result.userDetails.currentLines = constants.DEEP_SEAS.NOOF_LINES;
        result.userDetails.freeSpinData = {};
        result.userDetails.totalBet = (result.userDetails.currentBet * result.userDetails.currentLines);
        resolve(result);
      });
    });
  }


  /**
  * This function is responsible to calculate result for a spin event
  * taking into account
  * view zone, pay lines, pay table
  * there will be 2 sets of viewzone, and 2 sets of results as well
  */
  compute(game, gameId, token, session) {
    return new Promise((resolve) => {
      if (_.isEmpty(session.eventData.freeSpinData)) {
        const noofReelsL = game.default.gameConfig.reelConfigSet.ReelL.NumberOfReels;
        const noofReelsR = game.default.gameConfig.reelConfigSet.ReelR.NumberOfReels;
        const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
        // start calculating view zone
        const arraysL = nj.arange(noofReelsL * columnSize).reshape(columnSize, noofReelsL);
        const arraysR = nj.arange(noofReelsR * columnSize).reshape(columnSize, noofReelsR);
        // random numbers are fetched from reel configuration
        const randomNumberL = rng.generateRandomNumberOld(game.default.gameConfig.reelConfigSet.ReelL);
        const randomNumberR = rng.generateRandomNumberOld(game.default.gameConfig.reelConfigSet.ReelR);
        // view zone is calculated below
        const scatter = [];
        this.generateViewZone(randomNumberL, noofReelsL, columnSize, scatter, game, arraysL, session, 'ReelL');
        this.generateViewZone(randomNumberR, noofReelsR, columnSize, scatter, game, arraysR, session, 'ReelR');
        const deepCopyArrayL = arraysL.clone();
        const deepCopyArrayR = arraysR.clone();
        // load pay arrays
        const payArray = game.default.gameConfig.payLines.normalizedPayArray;
        // result based on view zone is calculated below
        let results = [];
        let resultsR = [];
        Promise.all([
          super.computeWins(arraysL, columnSize, payArray, game, results),
          super.computeWins(arraysR, columnSize, payArray, game, resultsR),
          super.freeSpinCompute(scatter, results, game),
          super.wildPayCompute(deepCopyArrayL, columnSize, payArray, game, results),
          super.wildPayCompute(deepCopyArrayR, columnSize, payArray, game, resultsR)
        ]).then(() => {
          results = super.refineResults(results);
          resultsR = super.refineResults(resultsR);
          const combinedResults = [];
          if (results && results.length) {
            const triggerWeightIndex = Math.floor(Math.random() * game.default.gameConfig.specialFeature.triggerWeight.mainGameLength);
            const triggerWeightValue = game.default.gameConfig.specialFeature.triggerWeight.mainGame[triggerWeightIndex];
            // if feature is triggered, combine viewZone, and calculate win for 6X6 viewZone with 81 paylines
            if (triggerWeightValue && triggerWeightValue.includes('111')) {
              this.calculateWinForCombinedViewZone(randomNumberL, randomNumberR, game, combinedResults, columnSize, session)
              .then(() => {
                resolve({ viewZone: arraysL, viewZoneR: arraysR, results, resultsR, combinedResults });
              });
            } else {
              resolve({ viewZone: arraysL, viewZoneR: arraysR, results, resultsR, combinedResults });
            }
          } else {
            resolve({ viewZone: arraysL, viewZoneR: arraysR, results, resultsR, combinedResults });
          }
        }).catch(() => {
          resolve({ error: 'error in view zone' });
        });
      } else {
        // if free spins are not going on, calculate only 1 viewzone, instead of 2 viewzones
        // and apply 81 paylines
        const noofReels = game.default.gameConfig.freeSpin.reelConfigSet.ReelL.NumberOfReels + game.default.gameConfig.freeSpin.reelConfigSet.ReelR.NumberOfReels;
        const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
        const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
        game.default.gameConfig.reelConfig = this.generateCombinedReelConfig(game.default.gameConfig.freeSpin.reelConfigSet);
        const randomNumber = rng.generateRandomNumberOld(game.default.gameConfig.reelConfig);
        const scatter = [];
        this.generateViewZone(randomNumber, noofReels, columnSize, scatter, game, arrays, session);
        const deepCopyArray = arrays.clone();
        // use 66 payArray (payArray for viewzone 6X6)
        const payArray = game.default.gameConfig.payLines66.normalizedPayArray;
        const results = [];
        Promise.all([
          super.computeWins(arrays, columnSize, payArray, game, results),
          super.freeSpinCompute(scatter, results, game),
          super.wildPayCompute(deepCopyArray, columnSize, payArray, game, results)
        ]).then(() => {
          resolve({ viewZone: arrays, results });
        }).catch((error) => {
          console.log(error, 'error viewzone');
          resolve({ error: 'error in view zone' });
        });
      }
    });
  }


  /**
  * generateCombinedReelConfig - generate combined reel configuration
  * get both viewZone, NumberOfReels in both config, and generate a combines reel config for a 3X6 viewZone
  */
  generateCombinedReelConfig(reelConfigSet) {
    const reelConfig = JSON.parse(JSON.stringify(reelConfigSet.ReelL));
    reelConfig.NumberOfReels += reelConfigSet.ReelR.NumberOfReels;
    for (
      let i = reelConfigSet.ReelL.NumberOfReels + 1;
      i < reelConfigSet.ReelR.NumberOfReels + reelConfigSet.ReelL.NumberOfReels + 1;
      i += 1
    ) {
      reelConfig[`Reel${i}`] = reelConfigSet.ReelR[`Reel${i - reelConfigSet.ReelL.NumberOfReels}`];
    }
    return reelConfig;
  }


  /**
  * calculateWinForCombinedViewZone - if the features is triggered, calculate win for combined viewzone
  * it combines both randomNumber arrays, and calculates combines viewzone
  */
  calculateWinForCombinedViewZone(randomNumberL, randomNumberR, game, results, columnSize, session) {
    return new Promise((resolve) => {
      const randomNumber = _.concat(randomNumberL, randomNumberR);
      game.default.gameConfig.reelConfig = this.generateCombinedReelConfig(game.default.gameConfig.reelConfigSet);
      const arrays = nj.arange(randomNumber.length * columnSize).reshape(columnSize, randomNumber.length);
      this.generateViewZone(randomNumber, randomNumber.length, columnSize, [], game, arrays, session);
      const deepCopyArray = arrays.clone();
      const payArray = game.default.gameConfig.payLines66.normalizedPayArray;
      Promise.all([
        super.computeWins(arrays, columnSize, payArray, game, results),
        super.wildPayCompute(deepCopyArray, columnSize, payArray, game, results)
      ]).then(() => {
        results = super.refineResults(results);
        resolve({ viewZone: arrays, results });
      }).catch(() => {
        resolve({ error: 'error in view zone' });
      });
    });
  }


  /**
  * This function generates view zone base on random numbers
  * view zone is calculated to be shown to user
  */
  generateViewZone(randomNumber, noofReels, columnSize, scatter, game, arrays, session, reelConfigType) {
    let reelConfiguration = game.default.gameConfig.reelConfig;
    if (reelConfigType) {
      reelConfiguration = game.default.gameConfig.reelConfigSet[reelConfigType];
    }
    for (let i = 0; i < noofReels; i += 1) {
      for (let j = 0; j < columnSize; j += 1) {
        const symbolId = super.getSymbol(j, i, reelConfiguration, randomNumber);
        if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Scatter') {
          scatter.push(i);
        }
        arrays.set(j, i, symbolId);
      }
    }
  }


  /**
  * This function is responsible to generate spin response
  * it includes view zone, wins, user details like wallet, bet array etc
  * necessary render view for the user
  * this function adds both viewZones, and both results to response
  * additionally, if the feature is triggered, it also adds combined result to the response
  */
  generateSpinResponse(obj, computeData, token, sessionRequestKey, sessionRequest, session) {
    // determine view port
    const noofReelsL = obj.game.default.gameConfig.reelConfigSet.ReelL.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));
    // create win document
    const result = {};
    // create reels for view zone
    const array = computeData.viewZone;
    const viewZoneL = {};
    for (let i = 0; i < noofReelsL; i += 1) {
      const line = [];
      for (let j = 0; j < columnSize; j += 1) {
        line.push({ symbolId: array.get(j, i), symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType, symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName });
      }
      viewZoneL[`Reel${i + 1}`] = line;
    }
    viewZoneL.viewZone = obj.game.default.gameConfig.viewZone;
    result.viewZoneL = viewZoneL;

    const noofReelsR = obj.game.default.gameConfig.reelConfigSet.ReelR.NumberOfReels;
    let firstReelOfViewZoneR = noofReelsL;
    let lastReelOfViewZoneR = noofReelsL + noofReelsR;
    let viewZoneRArray = array;
    if (computeData.viewZoneR) {
      firstReelOfViewZoneR = 0;
      lastReelOfViewZoneR = noofReelsR;
      viewZoneRArray = computeData.viewZoneR;
    }
    const viewZoneR = {};
    for (let i = firstReelOfViewZoneR; i < lastReelOfViewZoneR; i += 1) {
      const line = [];
      for (let j = 0; j < columnSize; j += 1) {
        line.push({ symbolId: viewZoneRArray.get(j, i), symbolType: obj.game.default.gameConfig.symbols[viewZoneRArray.get(j, i)].SymbolType, symbolName: obj.game.default.gameConfig.symbols[viewZoneRArray.get(j, i)].SymbolName });
      }
      viewZoneR[`Reel${(firstReelOfViewZoneR === 0) ? (i + 1) : (i - 2)}`] = line;
    }

    // add 2nd viewZone to spin response
    viewZoneR.viewZone = obj.game.default.gameConfig.viewZone;
    result.viewZoneR = viewZoneR;
    result.wins = {};
    result.freeSpinData = {};
    result.levelData = {};

    return new Promise((resolve, reject) => {
        // if free spin is present
      if (computeData.results.freeSpins) {
        if (!_.isEmpty(session)) {
          if (_.isEmpty(session.eventData.freeSpinData)) {
            result.freeSpinData.freeSpins = computeData.results.freeSpins;
            result.freeSpinData.currentFreeSpin = 0;
            result.freeSpinData.winAmount = 0;
            session.eventData.freeSpinData = result.freeSpinData;
          } else {
            result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
            session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
            session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
          }
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
          if (session.eventData.freeSpinData.isFreeSpinAwarded) {
            delete session.eventData.freeSpinData.isFreeSpinAwarded;
            delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
          }
          session.event = 'spin';
          session._updatedTimestamp = moment.utc();
          super.updateSession(session, obj._id);
        }
        result.isSessionUpdated = true;
      }
      const total = this.generateWinLines(result, computeData, session);

      // put user session data
      result.session = {};
      result.session.sessionRequestKey = sessionRequestKey;
      result.session.sessionRequest = sessionRequest;
      /**
       * notify of all the symbols present in view zone
       */
      gameEvents.notifyGameEvents(obj, token, session, result, computeData.results.freeSpins);

      // mask view zone here
      result.viewZoneL = this.maskViewZone(result.viewZoneL, obj, 'ReelL');
      result.viewZoneR = this.maskViewZone(result.viewZoneR, obj, 'ReelR');

      if (_.isEmpty(session.eventData.freeSpinData)) {
        // no free spins
        resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
        // some free spins remaining
        result.freeSpinData = session.eventData.freeSpinData;

        if (computeData.results.freeSpins) {
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
        }

        if (!computeData.results.freeSpins) {
          result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
        }

        if (result.wins.winCoins > 0) {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
          session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
        } else {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
        }
        // call whow api
        if (session.eventData.freeSpinData.currentFreeSpin !== 0) {
          resolve(super.callWhowBluehare(token, result, computeData, 0, 10, total, session, sessionRequestKey, sessionRequest, obj._id));
        } else {
          resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
        }
      } else {
        // free spin object exists and there are no free spins remaining
        resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      }
    });
  }


  /*
  * this function adds winLines of left and right, and combined winlines to result as well
  * it also adds all winLines to a common winLine object
  */
  generateWinLines(result, computeData, session) {
    // total win amount calculated based on wins from pay line
    let total = 0;
    // if left result contains data, compute wins for left viewZone
    computeData.results.forEach((resultObj) => {
      resultObj.winAmount = resultObj.multiplier * (session.eventData.currentBet);
      total += (resultObj.multiplier * (session.eventData.currentBet));
      delete resultObj.multiplier;
    });

    // if resultR contains data, compute wins for right viewZone
    if (computeData.resultsR && computeData.resultsR.length) {
      computeData.resultsR.forEach((resultObj) => {
        resultObj.winAmount = resultObj.multiplier * (session.eventData.currentBet);
        total += (resultObj.multiplier * (session.eventData.currentBet));
        delete resultObj.multiplier;
      });
    }

    let combinedWin = 0;
    // if combined result contains data, compute wins for combined viewZone
    if (computeData.combinedResults && computeData.combinedResults.length) {
      computeData.combinedResults.forEach((resultObj) => {
        resultObj.winAmount = resultObj.multiplier * (session.eventData.currentBet);
        total += (resultObj.multiplier * (session.eventData.currentBet));
        combinedWin += (resultObj.multiplier * (session.eventData.currentBet));
        delete resultObj.multiplier;
      });
    }
    result.wins.winCoins = total;
    // all winlines, must contain winline result for both viewZones, and combined as well
    result.wins.winLines = JSON.parse(JSON.stringify(computeData.results));
    if (result.wins.winLines.freeSpins) {
      delete result.wins.winLines.freeSpins;
    }
    result.wins.winLinesCount = computeData.results.length;
    // add right winlines to result
    if (computeData.resultsR) {
      result.wins.winLinesCountL = computeData.results.length;
      result.wins.winLinesL = JSON.parse(JSON.stringify(computeData.results));
      if (result.wins.winLinesL.freeSpins) {
        delete result.wins.winLinesL.freeSpins;
      }
      // add right viewzone winLines to the overall result
      result.wins.winLines = result.wins.winLines.concat(computeData.resultsR);
      result.wins.winLinesCount += computeData.resultsR.length;
      result.wins.winLinesR = computeData.resultsR;
      result.wins.winLinesCountR = computeData.resultsR.length;
    } else {
      result.isViewzoneCombined = true;
      result.wins.combinedWinLines = computeData.results;
      result.wins.combinedWinLinesCount = computeData.results.length;
      result.wins.winLinesL = [];
      result.wins.winLinesCountL = 0;
      result.wins.winLinesR = [];
      result.wins.winLinesCountR = 0;
    }

    if (computeData.combinedResults && computeData.combinedResults.length) {
      result.wins.combinedWinLines = computeData.combinedResults;
      result.wins.combinedWinLinesCount = computeData.combinedResults.length;
      result.wins.triggerWin = combinedWin;
      result.isViewzoneCombined = true;
      // add combined viewzone winLines to the overall result
      result.wins.winLines = result.wins.winLines.concat(computeData.combinedResults);
      result.wins.winLinesCount += computeData.combinedResults.length;
    }
    return total;
  }


  /**
   * This function is reponsilbe to mask the view zone for animation and security purpose
   * A initial configuration data is read and all reels are masked randomly from initial configuration
   */
  maskViewZone(viewZone, obj, reelConfigType) {
    const reelConfiguration = (reelConfigType) ? obj.game.default.gameConfig.reelConfigSet[reelConfigType] : obj.game.default.gameConfig.reelConfig;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));
    const maskedReels = {};

    // construct fake reel with random symbols
    let reelSize = reelMask.INITIAL_REEL_SIZE;
    for (let i = 0; i < reelConfiguration.NumberOfReels; i += 1) {
      if (i > 0) {
        reelSize += reelMask.REEL_SIZE_INCREMENT;
      }
      const symbolIndices = Object.keys(reelConfiguration[`Reel${i + 1}`].SymbolDistribution);
      const reelArray = [];
      for (let j = 0; j < reelSize; j += 1) {
        reelArray.push(reelConfiguration[`Reel${i + 1}`].SymbolDistribution[Math.floor(Math.random() * symbolIndices.length)].SymbolId);
      }
      // put view zone inside masked reel array
      const randomReelLocation = Math.floor(Math.random() * reelSize);
      for (let j = 0; j < columnSize; j += 1) {
        const reel = viewZone[`Reel${i + 1}`];
        const actualReelLocation = (randomReelLocation + j) % reelSize;
        reelArray[actualReelLocation] = reel[j].symbolId;
      }
      maskedReels[`Reel${i + 1}`] = { symbols: reelArray, stopAt: (randomReelLocation % reelSize) };
    }
    maskedReels.viewZone = viewZone.viewZone;
    return maskedReels;
  }


  /*
  * remove unnecessary parameters from response, when logging the result to elasticsearch
  */
  refineLogResult(logResult) {
    delete logResult.viewZoneL;
    delete logResult.viewZoneR;
    if (logResult.wins.winLinesL) {
      delete logResult.wins.winLinesL;
      delete logResult.wins.winLinesCountL;
    }
    if (logResult.wins.winLinesR) {
      delete logResult.wins.winLinesR;
      delete logResult.wins.winLinesCountR;
    }
    if (logResult.wins.combinedWinLines) {
      delete logResult.wins.combinedWinLines;
      delete logResult.wins.combinedWinLinesCount;
    }
    return logResult;
  }

}

module.exports = new DeepSeas();
