const nj = require('numjs');
const rng = require('../rng');
const { bucket } = require('../../helpers/couchbase');
const moment = require('moment');
const _ = require('lodash');
const constants = require('../../response/constants');
const { Compute } = require('../compute');
const gameSession = require('../gamesession');
const gameEvents = require('../../helpers/game-events');

/**
 * Dragon & Phoenix specific features are implemented in this class
 * This class would be reponsible to update game related user sessions
 * This class would be reponsible to compute for respin feature
 */
class DragonPhoenix extends Compute {
  /**
   * [Handles game click event for dragon & phoenix]
   * @param  gameId   dragon & phoenix game id
   * @param  token    user token
   * @param  clientID client id generated on the client side
   * @param  data     user related data fetched from whow
   * @param  gameData game configuratin related data
   */
  handleGameClick(gameId, token, clientID, data, gameData) {
    bucket.get(`${data.userDetails.user.id}::${gameId}`, (err, result) => {
      if (err || result === null) {
        gameSession.createGameSessionObject(gameId, clientID, data, gameData, constants.DRAGON_PHOENIX.NOOF_LINES);
      } else {
        result.value.clientID = clientID;
        gameSession.buildUserSessionObject(result, data, gameData, constants.DRAGON_PHOENIX.NOOF_LINES);
        super.updateSession(result.value, gameId);
      }
    });
    gameSession.createUserDataAndSessionRequest(data, gameId, token);
  }


  /**
   * This function cerates user session in database
   * it initializes bet array, bet lines forming initial bet
   * for dragon & phoenix game
   */
  createDragonPhoenixUserSession(gameId, clientID, data) {
    const doc = {
      currentBet: data.userDetails.game.settings.bets[0] || 5,
      currentLines: constants.DRAGON_PHOENIX.NOOF_LINES,
      userDetails: data.userDetails.user,
      freeSpinData: {}
    };
    bucket.upsert(`${data.userDetails.user.id}::${gameId}`, {
      _type: 'userSession',
      event: 'gameClick',
      clientID,
      eventData: doc,
      game: data.userDetails.game,
      _updatedTimestamp: moment.utc()
    }, (err) => {
      if (err) {
        console.log(err);
      }
    });
  }


  /*
  * selectReelConfig - select reel config to be used, either main game or free game, based on user session
  */
  selectReelConfig(session, obj) {
    if (!_.isEmpty(session.eventData.freeSpinData) && (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0)) {
      obj.game.default.gameConfig.freeSpin.reelConfig.NumberOfReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
      obj.game.default.gameConfig.reelConfig = obj.game.default.gameConfig.freeSpin.reelConfig;
      obj.game.default.gameConfig.specialFeature.reelConfig = obj.game.default.gameConfig.specialFeature.reelConfig.freeGame;
    } else {
      obj.game.default.gameConfig.specialFeature.reelConfig = obj.game.default.gameConfig.specialFeature.reelConfig.mainGame;
    }
    return obj;
  }


  /**
  * This function is responsible to calculate result for a spin event
  * taking into account
  * view zone, pay lines, pay table
  */
  compute(game, gameId, token, session) {
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    /*
    * start calculating view zone
    */
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    let reelConfig;
    if (session.eventData.bonusData && !_.isEmpty(session.eventData.bonusData)) {
      reelConfig = JSON.parse(JSON.stringify(game.default.gameConfig.reelConfig));
      game.default.gameConfig.reelConfig = game.default.gameConfig.specialFeature.reelConfig;
      game.default.gameConfig.reelConfig.NumberOfReels = noofReels;
    }
    // random numbers are fetched from reel configuration
    const randomNumber = rng.generateRandomNumber(game.default.gameConfig.reelConfig);
    // restore reel config, in order to mask reels
    const scatter = [];
    const specialSymbols = [];
    let heldSymbols = { symbolList: [], positions: {} };
    let currentReSpin = 0;
    // is isReSpinTriggered - get currentReSpin, and heldSymbols from session
    if (session.eventData.bonusData && session.eventData.bonusData.isReSpinTriggered) {
      currentReSpin = session.eventData.bonusData.currentReSpin;
      heldSymbols = JSON.parse(JSON.stringify(session.eventData.bonusData.heldSymbols));
    }
    // view zone is calculated below
    // it also decides, if the special feature will be triggered or not
    const isFeatureTriggered = this.generateViewZone(
      randomNumber, noofReels, columnSize, scatter,
      game, arrays, specialSymbols, heldSymbols,
      currentReSpin,
      game.default.gameConfig.specialFeature.numberOfReSpins
    );
    // restore reel configuratin
    if (reelConfig) {
      game.default.gameConfig.reelConfig = reelConfig;
    }
    let bonusData = null;
    if (isFeatureTriggered) {
      bonusData = {
        isReSpinTriggered: true,
        heldSymbols,
        currentReSpin,
        numberOfReSpins: game.default.gameConfig.specialFeature.numberOfReSpins
      };
    }
    // if all symbols on viewzone are either wild or dragon, set bonus data as null, as the current spin would be the last spin of feature
    if (bonusData && bonusData.heldSymbols && bonusData.heldSymbols.symbolList && bonusData.heldSymbols.symbolList.length === noofReels * columnSize) {
      session.eventData.bonusData.currentReSpin = session.eventData.bonusData.numberOfReSpins;
      bonusData = null;
    }
    const wasReSpin = (session.eventData.bonusData !== undefined && session.eventData.bonusData.currentReSpin === session.eventData.bonusData.numberOfReSpins);
    // result based on view zone is calculated below
    return new Promise((resolve) => {
      let results = [];
      if (bonusData) {
        resolve({ viewZone: arrays, results, bonusData });
      } else {
        const payArray = game.default.gameConfig.payLines.normalizedPayArray;
        Promise.all([
          super.computeWins(arrays, columnSize, payArray, game, results),
          super.freeSpinCompute(scatter, results, game),
          super.wildPayCompute(arrays, columnSize, payArray, game, results)
        ]).then(() => {
          results = super.refineResults(results);
          resolve({
            viewZone: arrays,
            results,
            wasReSpin
          });
        }).catch((err) => {
          console.log(err);
          resolve({ error: 'error in view zone' });
        });
      }
    });
  }


  /**
  * This function generates view zone base on random numbers
  * view zone is calculated to be shown to user
  */
  generateViewZone(
    randomNumber, noofReels, columnSize, scatter, game, arrays,
    specialSymbols, heldSymbols, currentReSpin, numberOfReSpins
  ) {
    for (let i = 0; i < noofReels; i += 1) {
      for (let j = 0; j < columnSize; j += 1) {
        // if held symbol is already there (i.e., spin is a re-spin), assign it to symbol-id, else generate it
        const symbolId = heldSymbols.positions[`${j}${i}`] || super.getSymbol(j, i, game.default.gameConfig.reelConfig, randomNumber);
        // set symbol is in array
        arrays.set(j, i, symbolId);
        // if symbol is on triggering reel, and symbol is triggering symbol, push it to specialSymbols-list
        if (
          currentReSpin !== numberOfReSpins
          && i === (game.default.gameConfig.specialFeature.triggeringReel - 1)
          && symbolId === game.default.gameConfig.specialFeature.triggeringSymbolId
        ) {
          specialSymbols.push({ j, i, symbolId });
        }
        // add symbol to held-symbols, if already does not exist
        if (game.default.gameConfig.specialFeature.heldSymbols[symbolId] && !heldSymbols.positions[`${j}${i}`]) {
          heldSymbols.symbolList.push({ row: j, column: i, symbolId });
          heldSymbols.positions[`${j}${i}`] = symbolId;
        }
        // check for scatter (free spins)
        if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Scatter') {
          scatter.push(i);
        }
      }
    }
    // if whole triggering reel is covered by special-symbol (i.e., triggering symbol), return true
    if (specialSymbols.length === columnSize) {
      return true;
    }
    return false;
  }


  /**
   * update user session data
   * @param session - user's session data
   * @param docDetails - generated winning data, from spin result
   * it returns sessionRequest, which will be updated to database after execution
   */
  updateSessionData(session, docDetails) {
    session.game = docDetails.game;
    session.eventData.userDetails.wallet = docDetails.wallet;
    if (!_.isEmpty(session)) {
      session.event = 'spin';
      if (!_.isEmpty(docDetails.freeSpinData) && docDetails.freeSpinData.isFreeSpinAwarded) {
        delete docDetails.freeSpinData.isFreeSpinAwarded;
        delete docDetails.freeSpinData.noofFreeSpinsAwarded;
      }
      session.eventData.freeSpinData = docDetails.freeSpinData;
      if (session.eventData.freeSpinData.currentFreeSpin && session.eventData.freeSpinData.currentFreeSpin == session.eventData.freeSpinData.freeSpins) {
        session.eventData.freeSpinData = {};
      }
      if (docDetails.bonusData) {
        docDetails.bonusData.currentReSpin += 1;
      }
      session.eventData.bonusData = docDetails.bonusData;
    }
    docDetails.session.sessionRequest.status = 'ready';
    docDetails.session.sessionRequest.spinCompletedTimestamp = moment().utc();
    return docDetails.session.sessionRequest;
  }


  /**
   * update user session when there are no free spins in The Great Gatsby
   * @param  gameId     dragon & phoenix game id
   * @param  token      user token
   * @param  docDetails user session schema
   * @param  gameData   game configuration schema
   */
  gameSessionUpdateMainGame(gameId, token, docDetails) {
    docDetails._updatedTimestamp = moment.utc();
    gameSession.getUserSession(token, gameId)
    .then((session) => {
      const sessionRequest = this.updateSessionData(session, docDetails);
      this.updateSessionAndSessionRequest(session, gameId, docDetails.session.sessionRequestKey, sessionRequest);
    }).catch((err) => {
      console.log(err);
    });
  }


  /*
  * generateViewZoneResponse - generate viewzone to be sent in response
  */
  generateViewZoneResponse(noofReels, columnSize, array, obj) {
    const viewZone = {};
    for (let i = 0; i < noofReels; i += 1) {
      const line = [];
      for (let j = 0; j < columnSize; j += 1) {
        line.push({ symbolId: array.get(j, i), symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType, symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName });
      }
      viewZone[`Reel${i + 1}`] = line;
    }
    return viewZone;
  }


  /**
  * This function is responsible to generate spin response
  * it includes view zone, wins, user details like wallet, bet array etc
  * necessary render view for the user
  */
  generateSpinResponse(obj, computeData, token, sessionRequestKey, sessionRequest, session) {
    // determine view port
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));

    // create reels for view zone
    const array = computeData.viewZone;
    const viewZone = this.generateViewZoneResponse(noofReels, columnSize, array, obj);
    viewZone.viewZone = obj.game.default.gameConfig.viewZone;

    // create win document
    const result = {};
    if (computeData.bonusData) {
      result.bonusData = computeData.bonusData;
    }
    if (computeData.wasReSpin) {
      result.wasReSpin = computeData.wasReSpin;
    }
    result.viewZone = viewZone;
    result.wins = {};
    result.freeSpinData = {};
    result.levelData = {};
    result.noofReels = noofReels;
    let isFsWinAdded = false;
    return new Promise((resolve) => {
      // if free spin is present
      if (computeData.results.freeSpins) {
        if (!_.isEmpty(session)) {
          if (_.isEmpty(session.eventData.freeSpinData)) {
            result.freeSpinData.freeSpins = computeData.results.freeSpins;
            result.freeSpinData.currentFreeSpin = 0;
            result.freeSpinData.winAmount = 0;
            session.eventData.freeSpinData = JSON.parse(JSON.stringify(result.freeSpinData));
          } else {
            result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
            if (!computeData.bonusData) {
              result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
            }
            session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
            session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
            computeData.results.forEach((winLineResult) => {
              session.eventData.freeSpinData.winAmount += (winLineResult.multiplier * session.eventData.currentBet);
            });
            isFsWinAdded = true;
          }
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
          if (session.eventData.freeSpinData.isFreeSpinAwarded) {
            delete session.eventData.freeSpinData.isFreeSpinAwarded;
            delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
          }
          session.event = 'spin';
          session._updatedTimestamp = moment.utc();
          super.updateSession(session, obj._id);
        }
        result.isSessionUpdated = true;
      }
      // total win amount calculated based on wins from pay line
      let total = 0;
      computeData.results.forEach((resultObj) => {
        resultObj.winAmount = resultObj.multiplier * (session.eventData.currentBet);
        total += resultObj.winAmount;
        delete resultObj.multiplier;
      });
      result.wins.winCoins = total;
      result.wins.winLinesCount = computeData.results.length;
      result.wins.winLines = [];
      computeData.results.forEach(row => result.wins.winLines.push(row));
      // put user session data
      result.session = {};
      result.session.sessionRequestKey = sessionRequestKey;
      result.session.sessionRequest = sessionRequest;
      if (!computeData.bonusData) {
        /**
         * notify of all the symbols present in view zone
         */
       gameEvents.notifyGameEvents(obj, token, session, result, computeData.results.freeSpins);
      }
      // mask view zone here
      result.viewZone = super.maskViewZone(result.viewZone, obj, _.isEmpty(session.eventData.freeSpinData));
      if (_.isEmpty(session.eventData.freeSpinData)) {
        resolve(this.callWhow(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
        // some free spins remaining
        result.freeSpinData = session.eventData.freeSpinData;
        if (computeData.results.freeSpins) {
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
        }
        if (!computeData.results.freeSpins && !computeData.bonusData) {
          result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
        }
        if (result.wins.winCoins > 0) {
          if (!isFsWinAdded) {
            result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
          }
          session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
        } else {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
        }
        // call whow api
        if (session.eventData.freeSpinData.currentFreeSpin !== 0) {
          resolve(this.callWhow(token, result, computeData, 0, 10, total, session, sessionRequestKey, sessionRequest, obj._id));
        } else {
          resolve(this.callWhow(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
        }
      } else {
        // free spin object exists and there are no free spins remaining
        resolve(this.callWhow(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      }
    });
  }


  /**
   * This function calls whow API for updating the wallet
   * if user has won a res-pin, it just gets balance from whow, else it updated wallet
   */
  callWhow(token, result, computeData, betAmount, virtualAmount, total, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve) => {
      if (result.bonusData) {
        if (result.bonusData.currentReSpin === 0) {
          resolve(super.callWhowBluehare(token, result, computeData, betAmount, virtualAmount, total, session, sessionRequestKey, sessionRequest, gameId));
        } else {
          result.wallet = session.eventData.userDetails.wallet;
          result.game = session.game;
          result.game.betAmount = (session.eventData.currentBet * session.eventData.currentLines);
          resolve(result);
        }
      } else if (result.wasReSpin) {
        resolve(super.callWhowBluehare(token, result, computeData, 0, 10, total, session, sessionRequestKey, sessionRequest, gameId));
      } else {
        resolve(super.callWhowBluehare(token, result, computeData, betAmount, virtualAmount, total, session, sessionRequestKey, sessionRequest, gameId));
      }
    });
  }
}

module.exports = new DragonPhoenix();
