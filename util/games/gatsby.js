/* eslint-disable max-statements, max-params, prefer-destructuring, no-ternary, multiline-ternary */
const nj = require('numjs');
const rng = require('../rng');
const moment = require('moment');
const _ = require('lodash');
const whowapi = require('../../api/whowapi');
const constants = require('../../response/constants');
const { Compute } = require('../compute');
const gameSession = require('../gamesession');
const gameEvents = require('../../helpers/game-events');

/**
 * The Great Gatsby specific features are implemented in this class
 * This class would be reponsible to update game related user sessions
 * This class would be reponsible to trigger special feature of The Great Gatsby (wild replacement feature)
 */
class Gatsby extends Compute {

  /**
  * This function handles game click event from controller
  * WHOW API is called to get user data from token
  * relevant user data along with game sessions are created using compute helper
  */
  handleGameClick(gameId, token, obj) {
    return new Promise((resolve, reject) => {
      whowapi.get(token)
      .then((response) => {
        if (!obj) {
          resolve({ error: constants.RES_MESSAGES.GAME_NOT_FOUND, erroCode: constants.RES_ERROR_CODES.GAME_NOT_FOUND });
        } else if (response.status == 200) {
          obj.userDetails = response.payload;
          resolve(this.generateGameClickResponse(obj, token, gameId));
        } else {
          resolve({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
        }
      }).catch(() => {
        reject({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
      });
    });
  }


  /**
  * This function is used to build response for game click event
  * game data along with user data for subsequent  game events calls are passed to client
  */
  generateGameClickResponse(data, token, gameId) {
    const result = {};
    data.game.default.gameConfig.reelConfig = data.game.default.gameConfig.reelConfigSet.Reel15;
    result.game = super.buildGameObjForGameClick(data);
    result.userDetails = super.buildUserDetailsForGameClick(data);
    return new Promise((resolve) => {
      gameSession.getUserSession(token, gameId)
      .then((session) => {
        result.userDetails = this.buildGameSessionGameClick(result.userDetails, data, session);
        resolve(result);
      }).catch(() => {
        result.userDetails = this.buildGameSessionGameClick(result.userDetails, data);
        resolve(result);
      });
    });
  }


  /**
  * This function is responsible to get reel configuration
  * from given configurations - reel15, and reel24
  */
  getReelConfig(gameConfig, session) {
    // check if spin is in main game or free game
    const gameType = (_.isEmpty(session.eventData.freeSpinData)) ? 'mainGame' : 'freeGame';
    const randomNumber = Math.floor(Math.random() * (gameConfig.specialFeature.reelSwitching[`${gameType}Length`]));
    const value = gameConfig.specialFeature.reelSwitching[gameType][randomNumber];
    // if value is "00000", select reel15, select select reel24, for current spin
    if (!value || value === '00000') {
      // replace game reel configuration, with reel15 config
      gameConfig.freeSpin.reelConfig = gameConfig.freeSpin.reelConfigSet.Reel15;
      gameConfig.reelConfig = (gameType === 'freeGame') ? gameConfig.freeSpin.reelConfigSet.Reel15 : gameConfig.reelConfigSet.Reel15;
      return '15';
    }
    // replace game reel configuration, with reel24 config
    gameConfig.freeSpin.reelConfig = gameConfig.freeSpin.reelConfigSet.Reel24;
    gameConfig.reelConfig = (gameType === 'freeGame') ? gameConfig.freeSpin.reelConfigSet.Reel24 : gameConfig.reelConfigSet.Reel24;
    return '24';
  }


  /**
  * This function is responsible to calculate positions, which will be replaced by wild symbol, if special feature is triggered
  * after calculating, it also replaces those positions, with wild symbol
  */
  calculateWildPath(wildTrigger, game, arrays, wildSymbols, reelSet) {
    wildTrigger.wildSymbols = [];
    wildTrigger.replacingSymbol = game.default.gameConfig.specialFeature.replacingWild;
    const pathOptions = game.default.gameConfig.specialFeature.wildTrigger[`Codes_${reelSet}_${wildSymbols.join('')}`];
    if (pathOptions && pathOptions.length) {
      const selectedOption = pathOptions[Math.floor(Math.random() * pathOptions.length)];
      if (selectedOption) {
        const path = selectedOption.split(';').map(opt => opt.split(''));
        const startingReel = parseInt(reelSet.substring(0, 1));
        arrays.set(wildSymbols[0] - 1, startingReel - 1, game.default.gameConfig.specialFeature.replacingWild);
        wildTrigger.wildSymbols.push([wildSymbols[0] - 1, startingReel - 1]);
        for (let i = startingReel; i < startingReel + path.length; i += 1) {
          path[i - startingReel].forEach((position) => {
            arrays.set(position - 1, i, game.default.gameConfig.specialFeature.replacingWild);
            wildTrigger.wildSymbols.push([position - 1, i]);
          });
        }
        wildTrigger.wildSymbols.push([wildSymbols[1] - 1, startingReel + path.length]);
        arrays.set(wildSymbols[1] - 1, startingReel + path.length, game.default.gameConfig.specialFeature.replacingWild);
      } else {
        console.log(pathOptions, 'wrong options');
      }
    } else {
      console.log(reelSet, wildSymbols, 'wrong options');
    }
  }


  /**
  * This function is responsible to calculate overlay feature wins
  * it checks if overlay data contains 5 of a ku=ind of any gem, and adds winCoins accordingly
  */
  calculateOverlayWins(overlayData, gameConfig, session) {
    if (!Object.keys(overlayData.gems).length) {
      return {};
    }
    for (const symbolId in overlayData.gems) {
      if (overlayData.gems[symbolId]) {
        if (gameConfig.payTable[symbolId] && gameConfig.payTable[symbolId][overlayData.gems[symbolId].winType]) {
          overlayData.gems[symbolId].winCoins = gameConfig.payTable[symbolId][overlayData.gems[symbolId].winType].Multiplier * (session.eventData.currentBet * session.eventData.currentLines);
          overlayData.winCoins += overlayData.gems[symbolId].winCoins;
        }
      }
    }
    return overlayData;
  }


  /**
  * This function is responsible to calculate result for a spin event
  * taking into account
  * view zone, pay lines, pay table
  */
  compute(game, gameId, token, session) {
    const reelSet = this.getReelConfig(game.default.gameConfig, session);
    if (!game.default.gameConfig.reelConfig.NumberOfReels) {
      game.default.gameConfig.reelConfig.NumberOfReels = Object.keys(game.default.gameConfig.reelConfig).length;
    }
    if (!game.default.gameConfig.freeSpin.reelConfig.NumberOfReels) {
      game.default.gameConfig.freeSpin.reelConfig.NumberOfReels = Object.keys(game.default.gameConfig.freeSpin.reelConfig).length;
    }
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    /*
    * start calculating view zone
    */
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    // random numbers are fetched using number of reels, and size of reel
    const randomNumber = rng.generateRandomNumber(game.default.gameConfig.reelConfig);
    // view zone is calculated below
    const scatter = [];
    const wildSymbols = [];
    let overlayData = { winCoins: 0, gems: {} };
    this.generateViewZone(randomNumber, noofReels, columnSize, scatter, game, arrays, session, wildSymbols, overlayData);
    overlayData = this.calculateOverlayWins(overlayData, game.default.gameConfig, session);

    // if expanding wild is detected complete reel is replaced with non expanding symbols each
    const deepCopyArray = arrays.clone();
    const wildTrigger = {};
    if (wildSymbols.length === 2) {
      this.calculateWildPath(wildTrigger, game, arrays, wildSymbols, reelSet);
    }

    // load pay arrays
    let payArray = game.default.gameConfig.payLines.normalizedPayArray;
    const copyPayArray = JSON.parse(JSON.stringify(payArray));


    // result based on view zone is calculated below
    const results = [];

    return new Promise((resolve, reject) => {
      const lineArrays = game.default.gameConfig.selectablePaylines[session.eventData.currentLines];
      payArray.forEach((pay) => {
        const object = _.find(lineArrays, o => o === pay._id);
        if (!object) {
          _.remove(copyPayArray, arr => arr._id === pay._id);
        }
      });
      payArray = copyPayArray;
      Promise.all(
        [
          this.computeWins(arrays, columnSize, payArray, game, results),
          this.freeSpinCompute(scatter, results, game, arrays),
          this.wildPayCompute(arrays, columnSize, payArray, game, results)
        ]
      ).then(() => {
        const winLines = {};
        const highestResults = [];
        if (results.freeSpins) {
          highestResults.freeSpins = results.freeSpins;
        }
        results.forEach((result) => {
          if (winLines[result.winLineId]) {
            highestResults[winLines[result.winLineId].index] = _.maxBy([winLines[result.winLineId].result, result], 'multiplier');
          } else {
            winLines[result.winLineId] = { result, index: highestResults.length };
            highestResults.push(result);
          }
        });
        resolve({ viewZone: deepCopyArray, results: highestResults, wildTrigger, overlayData });
      }).catch((err) => {
        console.log(err);
        resolve({ error: 'error in view zone' });
      });
    });
  }


  /**
  * This function generates view zone base on random numbers
  * view zone is calculated to be shown to user
  */
  generateViewZone(randomNumber, noofReels, columnSize, scatter, game, arrays, session, wildSymbols, overlayData) {
    if (!_.isEmpty(session.eventData.freeSpinData) && (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0)) {
      for (let i = 0; i < noofReels; i += 1) {
        for (let j = 0; j < columnSize; j += 1) {
          const symbolId = this.getSymbol(j, i, game.default.gameConfig.freeSpin.reelConfig, randomNumber);
          if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Scatter') {
            scatter.push([j, i]);
          }
          arrays.set(j, i, symbolId);
          // if reel number is less than 2, i.e., if reel is either reel1 or reel2, and symbol is gatsby wild (s1-wild), add it to wildSymbols array
          // if reel number is greater than 2, i.e., if reel is either reel4 or reel5, and symbol is daisy wild (s2-wild), add it to wildSymbols array
          if (
            (i < 2 && symbolId === game.default.gameConfig.specialFeature.symbolS1)
            || (i > 2 && symbolId === game.default.gameConfig.specialFeature.symbolS2)
          ) {
            wildSymbols.push(j + 1);
          }
        }
      }
    } else {
      for (let i = 0; i < noofReels; i += 1) {
        for (let j = 0; j < columnSize; j += 1) {
          const symbolId = this.getSymbol(j, i, game.default.gameConfig.reelConfig, randomNumber);
          // check if gemId is present, at given rowNumber
          const gemId = this.getGemId(j, i, game.default.gameConfig.reelConfig, randomNumber);
          // add gems to overlay data
          if (gemId && !overlayData.gems[gemId]) {
            overlayData.gems[gemId] = { winType: 0, gemPositions: [], winCoins: 0, gemId };
          }
          if (gemId) {
            overlayData.gems[gemId].winType += 1;
            overlayData.gems[gemId].gemPositions.push([j, i]);
          }
          if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Scatter') {
            scatter.push([j, i]);
          }
          arrays.set(j, i, symbolId);
          // if reel number is less than 2, i.e., if reel is either reel1 or reel2, and symbol is gatsby wild (s1-wild), add it to wildSymbols array
          // if reel number is greater than 2, i.e., if reel is either reel4 or reel5, and symbol is daisy wild (s2-wild), add it to wildSymbols array
          if (
            (i < 2 && symbolId === game.default.gameConfig.specialFeature.symbolS1)
            || (i > 2 && symbolId === game.default.gameConfig.specialFeature.symbolS2)
          ) {
            wildSymbols.push(j + 1);
          }
          arrays.set(j, i, symbolId);
        }
      }
    }
  }


  /**
  * This function determines gem to be shown on particular line on a reel
  */
  getGemId(j, i, reelConfig, randomNumber) {
    return reelConfig[`Reel${i + 1}`].SymbolDistribution[(randomNumber[i] + j) % reelConfig[`Reel${i + 1}`].NumberOfRows].GemId;
  }


  /**
  * This function checks if there are any free spins to be awarded based on view zone
  * If there are free spins than the number of free spins are determined based on game configuration
  */
  freeSpinCompute(scatter, results, game, arrays) {
    return new Promise((resolve) => {
      // remove scatter, from scatter array, if the position is replaced by wild, when special feature triggered
      const removeScatter = [];
      scatter.forEach((position, i) => {
        if (arrays.get(position[0], position[1]) !== game.default.gameConfig.freeSpin.symbolId) {
          removeScatter.push(i);
        }
      });
      removeScatter.forEach(val => scatter.splice(val, 1));
      if (scatter.length >= game.default.gameConfig.freeSpin.minimumRequiredCount) {
        if (game.default.gameConfig.freeSpin.freeSpinWins[scatter.length]) {
          results.freeSpins = game.default.gameConfig.freeSpin.freeSpinWins[scatter.length].noofFS;
        }
      }
      resolve(results);
    });
  }


  /**
  * @wildPayCompute - This function checks if wild present in view zone pays or not
  * if wilds are present and wild pays then wins are calculated subsequentaly
  * it checks for other symbol after wild, and checks if ofAKind + 1 of that symbol pays more than ofAKind wild
  */
  wildPayCompute(arrays, columnSize, payArray, game, results) {
    return new Promise((resolve, reject) => {
      const wildSymbolId = game.default.gameConfig.specialFeature.replacingWild;
      const wildSymbolIdS1 = game.default.gameConfig.specialFeature.symbolS1;
      const wildSymbolIdS2 = game.default.gameConfig.specialFeature.symbolS2;
      let lastSymbolId;
      const j = 0;
      for (let i = 0; i < columnSize; i += 1) {
        let symbolId = arrays.get(i, j);
        if (symbolId === wildSymbolIdS1 || symbolId === wildSymbolIdS2) {
          symbolId = wildSymbolId;
        }
        lastSymbolId = symbolId;
        if (symbolId !== wildSymbolId) {
          continue;
        }
        for (let k = 0; k < payArray.length; k += 1) {
          const payLine = payArray[k].data;
          let ofAKind = 0;
          if (payLine[0] === i) {
            ofAKind += 1;
            for (let l = 1; l < payLine.length; l += 1) {
              if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType !== 'Wild') {
                lastSymbolId = arrays.get(payLine[l], l);
                break;
              }
              ofAKind += 1;
            }
            if (ofAKind > 0 && game.default.gameConfig.payTable[symbolId][ofAKind]) {
              results.push({ symbolId, winType: `${ofAKind} of a kind`, winLineId: payArray[k]._id, multiplier: game.default.gameConfig.payTable[symbolId][ofAKind].Multiplier });
            }
          }
        }
      }
      resolve(results);
    });
  }


  /**
  * This function computes wins based on view zone
  * win is calculated starting from left i.e. from 1st reel
  * Wilds and scatter are taken into consideration while computing wins
  */
  computeWins(arrays, columnSize, payArray, game, results) {
    return new Promise((resolve) => {
      const bonusSymbolId = _.findKey(game.default.gameConfig.symbols, { SymbolType: 'Bonus' });
      const j = 0;
      for (let i = 0; i < columnSize; i += 1) {
        for (let k = 0; k < payArray.length; k += 1) {
          let symbolId = arrays.get(i, j);
          if (symbolId === bonusSymbolId) {
            break;
          }
          const payLine = payArray[k].data;
          let ofAKind = 0;
          if (payLine[0] === i) {
            ofAKind += 1;
            for (let l = 1; l < payLine.length; l += 1) {
              if (game.default.gameConfig.symbols[symbolId].SymbolType === 'Wild' && game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType !== 'Scatter' && arrays.get(payLine[l], l) !== bonusSymbolId) {
                symbolId = arrays.get(payLine[l], l);
                ofAKind += 1;
                continue;
              }
              if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType !== 'Wild' && arrays.get(payLine[l], l) !== symbolId) {
                break;
              }
              ofAKind += 1;
            }
            if (ofAKind > 0 && game.default.gameConfig.payTable[symbolId][ofAKind] && game.default.gameConfig.symbols[symbolId].SymbolType !== 'Wild') {
              results.push({ symbolId, winType: `${ofAKind} of a kind`, winLineId: payArray[k]._id, multiplier: game.default.gameConfig.payTable[symbolId][ofAKind].Multiplier });
            }
          }
          ofAKind = 0;
        }
      }
      resolve(results);
    });
  }


  /**
  * This function is responsible to generate spin response
  * it includes view zone, wins, user details like wallet, bet array etc
  * necessary render view for the user
  */
  generateSpinResponse(obj, computeData, token, sessionRequestKey, sessionRequest, session) {
    // determine view port
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));

    // create reels for view zone
    const array = computeData.viewZone;
    const viewZone = {};

    for (let i = 0; i < noofReels; i += 1) {
      const line = [];
      for (let j = 0; j < columnSize; j += 1) {
        line.push({ symbolId: array.get(j, i), symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType, symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName });
      }
      viewZone[`Reel${i + 1}`] = line;
    }
    viewZone.viewZone = obj.game.default.gameConfig.viewZone;


    // create win document
    const result = {};
    result.viewZone = viewZone;
    result.wins = {};
    result.freeSpinData = {};
    result.levelData = {};
    result.noofReels = noofReels;
    if (!_.isEmpty(computeData.wildTrigger)) {
      result.wildTrigger = computeData.wildTrigger;
    }
    return new Promise((resolve) => {
      // if free spin is present
      if (computeData.results.freeSpins) {
        if (!_.isEmpty(session)) {
          if (_.isEmpty(session.eventData.freeSpinData)) {
            result.freeSpinData.freeSpins = computeData.results.freeSpins;
            result.freeSpinData.currentFreeSpin = 0;
            result.freeSpinData.winAmount = 0;
            session.eventData.freeSpinData = result.freeSpinData;
          } else {
            result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
            session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
            session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
            computeData.results.forEach((winLineResult) => {
              session.eventData.freeSpinData.winAmount += (winLineResult.multiplier * (session.eventData.currentBet));
            });
          }
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
          if (session.eventData.freeSpinData.isFreeSpinAwarded) {
            delete session.eventData.freeSpinData.isFreeSpinAwarded;
            delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
          }
          session.event = 'spin';
          session._updatedTimestamp = moment.utc();
          super.updateSession(session, obj._id);
        }
        result.isSessionUpdated = true;
      }

      // total win amount calculated based on wins from pay line
      let total = 0;
      computeData.results.forEach((result) => {
        result.winAmount = result.multiplier * (session.eventData.currentBet);
        total += result.winAmount;
        delete result.multiplier;
      });
      result.overlayWins = {};
      if (!_.isEmpty(computeData.overlayData)) {
        result.overlayWins = {
          winCoins: computeData.overlayData.winCoins,
          gems: []
        };
        for (const gemId in computeData.overlayData.gems) {
          if (computeData.overlayData.gems[gemId]) {
            result.overlayWins.gems.push(computeData.overlayData.gems[gemId]);
          }
        }
        total += computeData.overlayData.winCoins;
      }
      result.wins.winCoins = total;
      result.wins.winLinesCount = computeData.results.length;
      result.wins.winLines = [];

      computeData.results.forEach(row => result.wins.winLines.push(row));

      // put user session data
      result.session = {};
      result.session.sessionRequestKey = sessionRequestKey;
      result.session.sessionRequest = sessionRequest;
      /**
       * notify of all the symbols present in view zone
       */
      gameEvents.notifyGameEvents(obj, token, session, result, computeData.results.freeSpins);

      // mask view zone here
      result.viewZone = super.maskViewZone(result.viewZone, obj, _.isEmpty(session.eventData.freeSpinData));
      if (_.isEmpty(session.eventData.freeSpinData)) {
        resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
        // some free spins remaining
        result.freeSpinData = session.eventData.freeSpinData;
        if (computeData.results.freeSpins) {
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
        }

        if (!computeData.results.freeSpins) {
          result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
        }

        if (result.wins.winCoins > 0) {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
          session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
        } else {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
        }

        // call whow api
        if (session.eventData.freeSpinData.currentFreeSpin !== 0) {
          resolve(super.callWhowBluehare(token, result, computeData, 0, 10, total, session, sessionRequestKey, sessionRequest, obj._id));
        } else {
          resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
        }
      } else {
        // free spin object exists and there are no free spins remaining
        resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      }
    });
  }
}

module.exports = new Gatsby();
