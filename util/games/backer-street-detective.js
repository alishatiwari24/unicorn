/* eslint-disable max-statements, max-params, prefer-destructuring, no-ternary, multiline-ternary, no-console */
const { Compute } = require('../compute');
const moment = require('moment');
const _ = require('lodash');
const nj = require('numjs');
const rng = require('../rng');
const constants = require('../../response/constants');
const whowapi = require('../../api/whowapi');
const gameEvents = require('../../helpers/game-events');

/**
 * This class is responsible for all the
 * compute, response, bonus handling and other features of
 * Backer Street Detective game
 */
class BackerStreetDetective extends Compute {


  /**
  * This function is responsible to calculate result for a spin event
  * taking into account
  * view zone, pay lines, pay table
  */
  compute(game, gameId, token, session) {
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    const randomNumber = rng.generateRandomNumber(game.default.gameConfig.reelConfig);
    const scatter = [];
    super.generateViewZone(randomNumber, noofReels, columnSize, [], scatter, game, arrays, session);
    const deepCopyArray = arrays.clone();
    const { roamingWildsPositions, wildPositions } = this.getRoamingWildPositions(session, game.default.gameConfig.specialFeature, columnSize);
    let results = [];
    const wildSymbolId = _.findKey(game.default.gameConfig.symbols, { SymbolType: 'Wild' });
    roamingWildsPositions.forEach((position) => {
      arrays.set(position.row, position.column, wildSymbolId);
    });
    const payArray = game.default.gameConfig.payLines.normalizedPayArray;
    return new Promise((resolve) => {
      Promise.all([
        super.freeSpinCompute(scatter, results, game),
        super.computeWins(arrays, columnSize, payArray, game, results),
        super.wildPayCompute(arrays, columnSize, payArray, game, results)
      ]).then(() => {
        results = super.refineResults(results);
        resolve({ viewZone: deepCopyArray, results, roamingWildsPositions, wildPositions });
      }).catch((err) => {
        console.log(err, 'errrr in BSD');
        resolve({ error: 'error in view zone' });
      });
    });
  }


  /**
   * It gets roaming wild positions
   * Roaming wild positions are picked randomly with every spin
   * during free spins there are additional roaming wilds
   */
  getRoamingWildPositions(session, specialFeature, columnSize) {
    let noofWilds = specialFeature.roamingWild.noofRoamingWilds;
    if (!_.isEmpty(session.eventData.freeSpinData) && session.eventData.freeSpinData.noofRoamingWilds) {
      noofWilds = session.eventData.freeSpinData.noofRoamingWilds;
    }
    const allWildPositionsArray = specialFeature.roamingWild.wildPositions[noofWilds];
    const wildIndex = Math.floor(Math.random() * allWildPositionsArray.length);
    const wildPositions = allWildPositionsArray[wildIndex].split('').map(val => parseInt(val));
    const roamingWildsPositions = [];
    for (let i = 0; i < wildPositions.length; i += 1) {
      if (wildPositions[i] > 0) {
        // make an array like - [0, 1, 2, 3]
        const reelPositions = new Array(columnSize).fill(0).map((value, index) => index);
        for (let j = 0; j < wildPositions[i]; j += 1) {
          const rowNumber = Math.floor(Math.random() * reelPositions.length);
          roamingWildsPositions.push({ row: reelPositions[rowNumber], column: i });
          reelPositions.splice(rowNumber, 1);
        }
      }
    }
    return { roamingWildsPositions, wildPositions: wildPositions.join('') };
  }


  /**
  * This function is responsible to generate spin response
  * it includes view zone, wins, user details like wallet, bet array etc
  * necessary render view for the user
  */
  generateSpinResponse(obj, computeData, token, sessionRequestKey, sessionRequest, session) {
    // determine view port
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));
    // create reels for view zone
    const array = computeData.viewZone;
    const viewZone = {};
    for (let i = 0; i < noofReels; i += 1) {
      const line = [];
      for (let j = 0; j < columnSize; j += 1) {
        line.push({ symbolId: array.get(j, i), symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType, symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName });
      }
      viewZone[`Reel${i + 1}`] = line;
    }
    viewZone.viewZone = obj.game.default.gameConfig.viewZone;
    // create win document
    const result = {};
    result.viewZone = viewZone;
    result.wins = {};
    result.freeSpinData = {};
    result.levelData = {};
    result.noofReels = noofReels;
    result.roamingWildsPositions = computeData.roamingWildsPositions;
    result.wildPositions = computeData.wildPositions;
    return new Promise((resolve) => {
      // if free spin is present
      if (computeData.results.freeSpins) {
        if (!_.isEmpty(session)) {
          if (_.isEmpty(session.eventData.freeSpinData)) {
            result.freeSpinData.freeSpins = computeData.results.freeSpins;
            result.freeSpinData.currentFreeSpin = 0;
            result.freeSpinData.winAmount = 0;
            result.freeSpinData.totalPicks = obj.game.default.gameConfig.specialFeature.roamingWild.picks.length;
            result.freeSpinData.isCardPicked = false;
            session.eventData.cards = _.shuffle(JSON.parse(JSON.stringify(obj.game.default.gameConfig.specialFeature.roamingWild.picks)));
            session.eventData.freeSpinData = result.freeSpinData;
          } else {
            result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
            session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
            session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
            computeData.results.forEach((winLineResult) => {
              session.eventData.freeSpinData.winAmount += (winLineResult.multiplier * (session.eventData.currentBet));
            });
          }
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
          if (session.eventData.freeSpinData.isFreeSpinAwarded) {
            delete session.eventData.freeSpinData.isFreeSpinAwarded;
            delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
          }
          session.event = 'spin';
          session._updatedTimestamp = moment.utc();
          super.updateSession(session, obj._id);
        }
        result.isSessionUpdated = true;
      }
      // total win amount calculated based on wins from pay line
      let total = 0;
      computeData.results.forEach((resultObj) => {
        resultObj.winAmount = resultObj.multiplier * (session.eventData.currentBet);
        total += resultObj.winAmount;
        delete resultObj.multiplier;
      });
      result.wins.winCoins = total;
      result.wins.winLinesCount = computeData.results.length;
      result.wins.winLines = [];
      computeData.results.forEach(row => result.wins.winLines.push(row));
      // put user session data
      result.session = {};
      result.session.sessionRequestKey = sessionRequestKey;
      result.session.sessionRequest = sessionRequest;
      /**
       * notify of all the symbols present in view zone
       */
      gameEvents.notifyGameEvents(obj, token, session, result, computeData.results.freeSpins);

      // mask view zone here
      result.viewZone = super.maskViewZone(result.viewZone, obj, _.isEmpty(session.eventData.freeSpinData));
      if (_.isEmpty(session.eventData.freeSpinData)) {
        resolve(this.callWhow(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
        // some free spins remaining
        result.freeSpinData = session.eventData.freeSpinData;
        if (computeData.results.freeSpins) {
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
        }

        if (!computeData.results.freeSpins) {
          result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
        }

        if (result.wins.winCoins > 0) {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
          session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
        } else {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
        }

        // call whow api
        if (session.eventData.freeSpinData.currentFreeSpin !== 0) {
          resolve(this.callWhow(token, result, computeData, 0, 10, total, session, sessionRequestKey, sessionRequest, obj._id));
        } else {
          resolve(this.callWhow(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
        }
      } else {
        // free spin object exists and there are no free spins remaining
        resolve(this.callWhow(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      }
    });
  }


  /**
   * This function calls whow API for updating the wallet
   * Whow api is called at the end of each spin
   * informing the wins to whow, and to deduct bet from the wallet
   */
  callWhow(token, result, computeData, betAmount, virtualAmount, total, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve, reject) => {
      resolve(this.callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId));
    });
  }


  /**
   * This is the actual whow api call function
   * play api is called in each spin, informing whow, win amount of spin
   * virtualAmount is specified as non-zero, when the free spins are going on
   */
  callPlay(token, betAmount, total, virtualAmount, result, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve) => {
      whowapi.play(token, {
        betAmount,
        winAmount: total,
        virtualAmount
      }).then((response) => {
        response = JSON.parse(response);
        if (response.status && response.status === 110) {
          sessionRequest.status = 'ready';
          session.eventData.freeSpinData = {};
          delete session.eventData.cards;
          this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);
          resolve({ error: constants.RES_MESSAGES.NOT_ENOUGH_BALANCE, errorCode: constants.RES_ERROR_CODES.NOT_ENOUGH_BALANCE });
        } else {
          result.levelData.level = response.payload.user.level;
          result.levelData.levelProgress = response.payload.user.levelProgress;
          result.wallet = response.payload.user.wallet;
          result.game = response.payload.game;
          result.game.betAmount = (session.eventData.currentBet * session.eventData.currentLines);
          result.jackpot = response.payload.user.jackpotData;
          resolve(result);
        }
      }).catch((err) => {
        console.log(err);
        resolve(result);
      });
    });
  }


  /**
   * Handles bonus request from client
   * it selects wild counts for free game
   */
  async handleBonusRequest(gameId, cardId, session) {
    const actualCards = JSON.parse(JSON.stringify(session.eventData.cards));
    delete session.eventData.cards;
    delete session.eventData.freeSpinData.isCardPicked;
    delete session.eventData.freeSpinData.totalPicks;
    session.eventData.freeSpinData.noofRoamingWilds = actualCards[cardId];
    await super.updateSession(session, gameId);
    return { response: { actualCards, selectedCard: actualCards[cardId] } };
  }
}

module.exports = new BackerStreetDetective();
