const nj = require('numjs');
const rng = require('../rng');
const moment = require('moment');
const _ = require('lodash');
const { Compute } = require('../compute');
const gameEvents = require('../../helpers/game-events');

class Carnevale extends Compute {
/**
 * getReelConfiguration method is used to get reelConfig for free game or main game
 * @param  {[game]} game    game contains game object
 * @param  {[session]} session session object user session and games session details
 */
  getReelConfiguration(game, session) {
    let reelConfigProbability = 0;
    if (!_.isEmpty(session.eventData.freeSpinData) && (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0)) {
      const reelConf = game.default.gameConfig.freeSpin.reelConfigSet[0];
      if (!reelConf) {
        throw new Error('picked up random number of reel set is not in reelConfigSet array of freeGame');
      }
      game.default.gameConfig.freeSpin.reelConfig = reelConf;
      game.default.gameConfig.freeSpin.reelConfig.NumberOfReels = game.default.gameConfig.reelConfigSet[0].NumberOfReels;
      game.default.gameConfig.reelConfig = reelConf;

      return game.default.gameConfig;
    }
    reelConfigProbability = this.getRandomProbability(game, 'mainGame');
    const reelConf = game.default.gameConfig.reelConfigSet[reelConfigProbability];
    if (!reelConf) {
      throw new Error('picked up random number of reel set is not in reelConfigSet array of mainGame');
    }
    game.default.gameConfig.reelConfig = reelConf;
    return game.default.gameConfig;
  }


/**
 * getRandomProbability method is used to get probability of reelWeight
 * which is used to select reelConfig from any two
 * @param  {[game]} game object contains game data
 * @param  {[gameType]} gameType method is used to get freeGame or mainGame
 */
  getRandomProbability(game, gameType) {
    return game.default.gameConfig.specialFeature.reelSwitching[gameType][Math.floor(Math.random() * (game.default.gameConfig.specialFeature.reelSwitching[gameType].length))];
  }


/**
 * This function is responsible to calculate result for a spin event
 * taking into account
 * view zone, pay lines, pay table
 */
  compute(game, gameId, token, session) {
    game.default.gameConfig = this.getReelConfiguration(game, session);
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));
    // start calculating view zone
    const arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);
    // random numbers are fetched from reel configuration
    const randomNumber = rng.generateRandomNumber(game.default.gameConfig.reelConfig);
    // view zone is calculated below
    const scatter = [];
    const wildOfKind = {};
    this.generateViewZone(randomNumber, noofReels, columnSize, scatter, game, arrays, session, wildOfKind);
    // if expanding wild is detected complete reel is replaced with non expaning symbols each
    const deepCopyArray = arrays.clone();
    // load pay arrays
    const payArray = game.default.gameConfig.payLines.normalizedPayArray;
    // result based on view zone is calculated below
    const results = [];
    return new Promise((resolve) => {
      Promise.all([
        this.computeWins(arrays, columnSize, payArray, game, results, session, wildOfKind),
        super.freeSpinCompute(scatter, results, game)
        // super.wildPayCompute(arrays, columnSize, payArray, game, results)
      ]).then(() => {
        // results = super.refineResults(results);
        resolve({
          viewZone: deepCopyArray,
          results,
          scatter,
          wildMultiplier: wildOfKind
        });
      }).catch((err) => {
        console.log(err, 'err in cmpute');
        resolve({ error: 'error in view zone' });
      });
    });
  }


/**
 * This function computes wins based on view zone
 * win is calculated starting from left i.e. from 1st reel
 * Wilds and scatter are taken into consideration while computing wins
 */
  computeWins(arrays, columnSize, payArray, game, results, session, wildFromViewZone) {
    return new Promise((resolve) => {
      const j = 0;
      for (let i = 0; i < columnSize; i += 1) {
        for (let k = 0; k < payArray.length; k += 1) {
          let symbolId = arrays.get(i, j);
          const payLine = payArray[k].data;
          let ofAKind = [];
          let totalWildMultiplier = 0;
          const wildPositionMultiplier = {};
          if (payLine[0] === i) {
            ofAKind.push('test');
            for (let l = 1; l < payLine.length; l += 1) {
              if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType === 'Wild') {
                const currentWildMultiplier = wildFromViewZone[`${l + 1}-${payLine[l]}`];
                totalWildMultiplier += currentWildMultiplier.multiplier;
                wildPositionMultiplier[`reel${l + 1}`] = { row: payLine[l], multiplier: currentWildMultiplier.multiplier };
              }
              if (game.default.gameConfig.symbols[symbolId].SymbolType === 'Wild' && game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType !== 'Scatter') {
                symbolId = arrays.get(payLine[l], l);
                ofAKind.push('test');
                continue;
              }
              if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType !== 'Wild' && arrays.get(payLine[l], l) != symbolId) {
                break;
              }
              ofAKind.push('test');
            }
            if (ofAKind.length > 0 && game.default.gameConfig.payTable[symbolId][ofAKind.length]) {
              results.push({ symbolId, winType: `${ofAKind.length} of a kind`, winLineId: payArray[k]._id, multiplier: game.default.gameConfig.payTable[symbolId][ofAKind.length].Multiplier, totalWildMultiplier, wildMultiplierPosition: wildPositionMultiplier });
            }
          }
          ofAKind = [];
        }
      }
      resolve(results);
    });
  }


/**
* getWildMultiplier method is used to get randomNumber wild multiplier
 * @param  {[type]} session   it used to get that there is freeGame or not
 * @param  {[type]} specialFeature it contains specialFeature of game
 * @param  {[type]} line   it contains reel number
 * @return {[type]}  [description]
 */
  getWildMultiplier(session, specialFeature, line) {
    if (!_.isEmpty(session.eventData.freeSpinData) && (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0)) {
      return specialFeature.wildMultiplierProb.freeGame[`reel${line}`][Math.floor(Math.random() * specialFeature.wildMultiplierProb.freeGame[`reel${line}`].length)];
    }
    return specialFeature.wildMultiplierProb.mainGame[`reel${line}`][Math.floor(Math.random() * specialFeature.wildMultiplierProb.mainGame[`reel${line}`].length)];
  }


/**
 * This function generates view zone base on random numbers
 * view zone is calculated to be shown to user
 */
  generateViewZone(randomNumber, noofReels, columnSize, scatter, game, arrays, session, wildOfKind) {
    for (let i = 0; i < noofReels; i += 1) {
      for (let j = 0; j < columnSize; j += 1) {
        const symbolId = super.getSymbol(j, i, game.default.gameConfig.reelConfig, randomNumber);
        if (game.default.gameConfig.symbols[symbolId].SymbolType === 'Scatter') {
          scatter.push(i);
        }
        if (game.default.gameConfig.symbols[symbolId].SymbolType === 'Wild') {
        const wildMultiplier = parseInt(this.getWildMultiplier(session, game.default.gameConfig.specialFeature, i + 1));
        wildOfKind[`${(i + 1)}-${j}`] = { multiplier: wildMultiplier, reel: i + 1, row: j };
        }
        arrays.set(j, i, symbolId);
      }
    }
  }


/**
 * generateSpinResponse method is used to generate spin response on every spin
 * and update user session and call callWhowBluehare api
 * @param obj   obj  contains game object
 * @param computeData computeData contains data that is computed on every spin
 * @param token   token contains user token
 * @param sessionRequestKey  its contains that sessionRequestKey such as cas
 * @param sessionRequest sessionRequest contains game session request
 */
  generateSpinResponse(obj, computeData, token, sessionRequestKey, sessionRequest, session) {
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));
    const array = computeData.viewZone;
    const viewZone = {};
    for (let i = 0; i < noofReels; i += 1) {
      const line = [];
      for (let j = 0; j < columnSize; j += 1) {
        line.push({
          symbolId: array.get(j, i),
          symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType,
          symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName
        });
      }
      viewZone[`Reel${i + 1}`] = line;
    }
    viewZone.viewZone = obj.game.default.gameConfig.viewZone;

    const result = {};
    result.viewZone = viewZone;
    const wildsArray = Object.keys(computeData.wildMultiplier);
    if (wildsArray.length > 0) {
      result.wildMultiplierLocation = [];
      wildsArray.forEach((key) => {
        result.wildMultiplierLocation.push(computeData.wildMultiplier[key]);
      });
    }
    result.wins = {};
    result.freeSpinData = {};
    result.levelData = {};
    result.noofReels = noofReels;
    return new Promise((resolve) => {
      if (computeData.results.freeSpins) {
        if (!_.isEmpty(session)) {
          if (_.isEmpty(session.eventData.freeSpinData)) {
            result.freeSpinData.freeSpins = computeData.results.freeSpins;
            result.freeSpinData.currentFreeSpin = 0;
            result.freeSpinData.winAmount = 0;
            session.eventData.freeSpinData = result.freeSpinData;
          } else {
            result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
            session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
            session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
            computeData.results.forEach((winLineResult) => {
              session.eventData.freeSpinData.winAmount += (winLineResult.multiplier * (session.eventData.currentBet) * (winLineResult.totalWildMultiplier || 1));
            });
          }
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
          if (session.eventData.freeSpinData.isFreeSpinAwarded) {
            delete session.eventData.freeSpinData.isFreeSpinAwarded;
            delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
          }
          session.event = 'spin';
          session._updatedTimestamp = moment.utc();
          super.updateSession(session, obj._id);
        }
        result.isSessionUpdated = true;
      }
      let total = 0;
      computeData.results.forEach((resultObj) => {
        resultObj.winAmount = resultObj.multiplier * (session.eventData.currentBet) * (resultObj.totalWildMultiplier || 1);
        total += resultObj.winAmount;
        delete resultObj.multiplier;
      });
      result.wins.winCoins = total;
      result.wins.winLinesCount = computeData.results.length;
      result.wins.winLines = [];
      computeData.results.forEach(row => result.wins.winLines.push(row));
      result.session = {};
      result.session.sessionRequestKey = sessionRequestKey;
      result.session.sessionRequest = sessionRequest;
      gameEvents.notifyGameEvents(obj, token, session, result, computeData.results.freeSpins);
      // mask view zone here
      result.viewZone = super.maskViewZone(result.viewZone, obj, _.isEmpty(session.eventData.freeSpinData));
      if (_.isEmpty(session.eventData.freeSpinData)) {
        resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
        result.freeSpinData = session.eventData.freeSpinData;
        if (computeData.results.freeSpins) {
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
        }
        if (!computeData.results.freeSpins) {
          result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
        }

        if (result.wins.winCoins > 0) {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
          session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
        } else {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
        }
        // call whow api
        if (session.eventData.freeSpinData.currentFreeSpin !== 0) {
          resolve(super.callWhowBluehare(token, result, computeData, 0, 10, total, session, sessionRequestKey, sessionRequest, obj._id));
        } else {
          resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
        }
      } else {
        resolve(super.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session, sessionRequestKey, sessionRequest, obj._id));
      }
    });
  }

}
module.exports = new Carnevale();
