/* eslint-disable max-statements, max-params, no-ternary, multiline-ternary, no-console */
const nj = require('numjs');
const rng = require('./rng');
const whowapi = require('../api/whowapi');
const _ = require('lodash');
const { bucket } = require('../helpers/couchbase');
const moment = require('moment');
const Promise = require('bluebird');
const reelMask = require('../config/reel-mask-config');
const gameSession = require('./gamesession');
const constants = require('../response/constants');

/**
* ComputeResults is responsible of heavy computation like win computes, scatter win computes, wild win computes
* it is also used to build responses for game click and spin apis
*/
class ComputeResults {

  /**
  * This function is responsible to calculate result for a spin event
  * taking into account
  * view zone, pay lines, pay table
  */
  compute(game, gameId, token, session) {
    const noofReels = game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(game.default.gameConfig.viewZone.toString().charAt(0));

    // start calculating view zone
    let arrays = nj.arange(noofReels * columnSize).reshape(columnSize, noofReels);

    // random numbers are fetched from reel configuration
    const randomNumber = rng.generateRandomNumber(game.default.gameConfig.reelConfig);

    // view zone is calculated below
    let expanding = [];
    const scatter = [];
    this.generateViewZone(randomNumber, noofReels, columnSize, expanding, scatter, game, arrays, session);

    // if expanding wild is detected complete reel is replaced with non expaning symbols each
    const deepCopyArray = arrays.clone();
    expanding = _.uniq(expanding);
    if (expanding.length > 0) {
      const copyArray = arrays.clone();
      const wildSymbolId = _.findKey(game.default.gameConfig.symbols, { SymbolType: 'Wild', isExpandingWild: false });
      expanding.forEach((i) => {
        for (let j = 0; j < columnSize; j += 1) {
          copyArray.set(j, i, wildSymbolId);
        }
      });
      arrays = copyArray.clone();
    }

    // load pay arrays
    let payArray = game.default.gameConfig.payLines.normalizedPayArray;
    const copyPayArray = JSON.parse(JSON.stringify(payArray));


    // result based on view zone is calculated below
    const results = [];

    return new Promise((resolve) => {
      const currentLines = session.eventData.currentLines;
      const lineArrays = game.default.gameConfig.selectablePaylines[currentLines];
      payArray.forEach((pay) => {
        const object = _.find(lineArrays, o => o === pay._id);
        if (!object) {
          _.remove(copyPayArray, arr => arr._id === pay._id);
        }
      });
      payArray = copyPayArray;
      arrays = deepCopyArray.clone();
      /*
      * compute wins, for given viewzone
      * compute free spins,if user got any
      * compute scatter wins, if user got any
      * and compute for wild pay
      */
      Promise.all([
        this.computeWins(arrays, columnSize, payArray, game, results),
        this.freeSpinCompute(scatter, results, game),
        this.scatterPayCompute(arrays, columnSize, payArray, game, results),
        this.wildPayCompute(deepCopyArray, columnSize, payArray, game, results)
      ]).then(() => {
        resolve({ viewZone: arrays, results });
      }).catch((err) => {
        resolve({ error: 'error in view zone' });
      });
    });
  }


  /**
  * refineResults - this function checks for wild pay and normal pay on same line, and considers the highest paying combination
  */
  refineResults(results) {
    const winLines = {};
    const highestResults = [];
    if (results.freeSpins) {
      highestResults.freeSpins = results.freeSpins;
    }
    results.forEach((result) => {
      if (result.winLineId) {
        // if given winLine is there in winLines object, check for maximum of thw wins, and replace it with max result
        if (winLines[result.winLineId]) {
          highestResults[winLines[result.winLineId].index] = _.maxBy([winLines[result.winLineId].result, result], 'multiplier');
        } else {
          // else, add it to winLine list
          winLines[result.winLineId] = { result, index: highestResults.length };
          highestResults.push(result);
        }
      }
    });
    return highestResults;
  }


  /**
  * This function generates view zone base on random numbers
  * view zone is calculated to be shown to user
  */
  generateViewZone(randomNumber, noofReels, columnSize, expanding, scatter, game, arrays, session) {
    if (!_.isEmpty(session.eventData.freeSpinData) && (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0)) {
      for (let i = 0; i < noofReels; i += 1) {
        for (let j = 0; j < columnSize; j += 1) {
          const symbolId = this.getSymbol(j, i, game.default.gameConfig.freeSpin.reelConfig, randomNumber);
          if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Wild' && game.default.gameConfig.symbols[symbolId].isExpandingWild == true) {
            expanding.push(i);
          }
          if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Scatter') {
            scatter.push(i);
          }
          arrays.set(j, i, symbolId);
        }
      }
    } else {
      for (let i = 0; i < noofReels; i += 1) {
        for (let j = 0; j < columnSize; j += 1) {
          const symbolId = this.getSymbol(j, i, game.default.gameConfig.reelConfig, randomNumber);
          if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Wild' && game.default.gameConfig.symbols[symbolId].isExpandingWild == true) {
            expanding.push(i);
          }
          if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Scatter') {
            scatter.push(i);
          }
          arrays.set(j, i, symbolId);
        }
      }
    }
  }


  /**
  * This function determines symbol to be shown on particular line on a reel
  * it uses i,j as reel number and line number along with random number
  * to build viwe zone based on reel configuration
  */
  getSymbol(j, i, reelConfig, randomNumber) {
    return reelConfig[`Reel${i + 1}`].SymbolDistribution[(randomNumber[i] + j) % reelConfig[`Reel${i + 1}`].NumberOfRows].SymbolId;
  }


  /**
  * This function checks if wild present in view zone pays or not
  * if wilds are present and wild pays then wins are calculated subsequentaly
  */
  wildPayCompute(arrays, columnSize, payArray, game, results) {
    return new Promise((resolve) => {
      const j = 0;
      for (let i = 0; i < columnSize; i += 1) {
        const symbolId = arrays.get(i, j);
        for (let k = 0; k < payArray.length; k += 1) {
          const payLine = payArray[k].data;
          const ofAKind = [];
          if (payLine[0] == i) {
            ofAKind.push('test');
            for (let l = 1; l < payLine.length; l += 1) {
              if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType != 'Wild' || arrays.get(payLine[l], l) != symbolId) {
                break;
              }
              ofAKind.push('test');
            }
            if (ofAKind.length > 0 && game.default.gameConfig.payTable[symbolId][ofAKind.length]) {
              results.push({ symbolId, winType: `${ofAKind.length} of a kind`, winLineId: payArray[k]._id, multiplier: game.default.gameConfig.payTable[symbolId][ofAKind.length].Multiplier });
            }
          }
        }
      }
      resolve(results);
    });
  }


  /**
  * This function checks if scatter is present in view zone pays or not
  * if scatters are present and scatter pays then wins are calculated subsequentaly
  */
  scatterPayCompute(arrays, columnSize, payArray, game, results) {
    return new Promise((resolve) => {
      const j = 0;
      for (let i = 0; i < columnSize; i += 1) {
        const symbolId = arrays.get(i, j);
        for (let k = 0; k < payArray.length; k += 1) {
          const payLine = payArray[k].data;
          const ofAKind = [];
          if (payLine[0] == i) {
            ofAKind.push('test');
            for (let l = 1; l < payLine.length; l += 1) {
              if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType != 'Scatter' || arrays.get(payLine[l], l) != symbolId) {
                break;
              }
              ofAKind.push('test');
            }
            if (ofAKind.length > 0 && game.default.gameConfig.payTable[symbolId][ofAKind.length]) {
              results.push({ symbolId, winType: `${ofAKind.length} of a kind`, winLineId: payArray[k]._id, multiplier: game.default.gameConfig.payTable[symbolId][ofAKind.length].Multiplier });
            }
          }
        }
      }
      resolve(results);
    });
  }


  /**
  * This function checks if there are any free spins to be awarded based on view zone
  * If there are free spins than the number of free spins are determined based on game configuration
  */
  freeSpinCompute(scatter, results, game) {
    return new Promise((resolve) => {
      if (scatter.length >= game.default.gameConfig.freeSpin.minimumRequiredCount && game.default.gameConfig.freeSpin.freeSpinWins[scatter.length]) {
        results.freeSpins = game.default.gameConfig.freeSpin.freeSpinWins[scatter.length].noofFS;
      }
      resolve(results);
    });
  }


  /**
  * This function computes wins based on view zone
  * win is calculated starting from left i.e. from 1st reel
  * Wilds and scatter are taken into consideration while computing wins
  */
  computeWins(arrays, columnSize, payArray, game, results) {
    return new Promise((resolve) => {
      const j = 0;
      for (let i = 0; i < columnSize; i += 1) {
        for (let k = 0; k < payArray.length; k += 1) {
          let symbolId = arrays.get(i, j);
          const payLine = payArray[k].data;
          let ofAKind = [];
          if (payLine[0] == i) {
            ofAKind.push('test');
            for (let l = 1; l < payLine.length; l += 1) {
              if (game.default.gameConfig.symbols[symbolId].SymbolType == 'Wild' && game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType != 'Scatter') {
                symbolId = arrays.get(payLine[l], l);
                ofAKind.push('test');
                continue;
              }
              if (game.default.gameConfig.symbols[arrays.get(payLine[l], l)].SymbolType != 'Wild' && arrays.get(payLine[l], l) != symbolId) {
                break;
              }
              ofAKind.push('test');
            }
            if (ofAKind.length > 0 && game.default.gameConfig.payTable[symbolId][ofAKind.length]) {
              results.push({ symbolId, winType: `${ofAKind.length} of a kind`, winLineId: payArray[k]._id, multiplier: game.default.gameConfig.payTable[symbolId][ofAKind.length].Multiplier });
            }
          }
          ofAKind = [];
        }
      }
      resolve(results);
    });
  }


  /**
  * This function is responsible to generate spin response
  * it includes view zone, wins, user details like wallet, bet array etc
  * necessary render view for the user
  */
  generateSpinResponse(obj, computeData, token, sessionRequestKey, sessionRequest, session) {
    // determine view port
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));
    // create reels for view zone
    const array = computeData.viewZone;
    const viewZone = {};

    for (let i = 0; i < noofReels; i += 1) {
      const line = [];
      for (let j = 0; j < columnSize; j += 1) {
        line.push({ symbolId: array.get(j, i), symbolType: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolType, symbolName: obj.game.default.gameConfig.symbols[array.get(j, i)].SymbolName });
      }
      viewZone[`Reel${i + 1}`] = line;
    }
    viewZone.viewZone = obj.game.default.gameConfig.viewZone;


    // create win document
    const result = {};
    result.viewZone = viewZone;
    result.wins = {};
    result.freeSpinData = {};
    result.levelData = {};

    // result.wildReel = computeData.wildReel;
    // session.eventData.wildReel = result.wildReel;
    return new Promise((resolve) => {
        // if free spin is present
      if (computeData.results.freeSpins) {
        if (!_.isEmpty(session)) {
          if (_.isEmpty(session.eventData.freeSpinData)) {
            result.freeSpinData.freeSpins = computeData.results.freeSpins;
            result.freeSpinData.currentFreeSpin = 0;
            result.freeSpinData.winAmount = 0;

            session.eventData.freeSpinData = result.freeSpinData;
          } else {
            result.freeSpinData.freeSpins = computeData.results.freeSpins + session.eventData.freeSpinData.freeSpins;
            result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;

            session.eventData.freeSpinData.currentFreeSpin = result.freeSpinData.currentFreeSpin;
            session.eventData.freeSpinData.freeSpins = result.freeSpinData.freeSpins;
          }
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
          if (session.eventData.freeSpinData.isFreeSpinAwarded) {
            delete session.eventData.freeSpinData.isFreeSpinAwarded;
            delete session.eventData.freeSpinData.noofFreeSpinsAwarded;
          }
          session.event = 'spin';
          session._updatedTimestamp = moment.utc();
          this.updateSessionAndSessionRequest(session, obj._id, sessionRequestKey, sessionRequest);
        }
        result.isSessionUpdated = true;
      }

        // total win amount calculated based on wins from pay line
      let total = 0;
      computeData.results.forEach((resultObj) => {
        resultObj.winAmount = resultObj.multiplier * (session.eventData.currentBet);
        total += (resultObj.multiplier * (session.eventData.currentBet));
        delete resultObj.multiplier;
      });
      result.wins.winCoins = total;
      result.wins.winLinesCount = computeData.results.length;
      result.wins.winLines = computeData.results;

        // put user session data
      result.session = {};
      result.session.sessionRequestKey = sessionRequestKey;
      result.session.sessionRequest = sessionRequest;

      // mask view zone here
      result.viewZone = this.maskViewZone(result.viewZone, obj);
      if (_.isEmpty(session.eventData.freeSpinData)) {
          // no free spins
        resolve(this.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session));
      } else if (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0) {
          // some free spins remaining
        result.freeSpinData = session.eventData.freeSpinData;

        if (computeData.results.freeSpins) {
          result.freeSpinData.isFreeSpinAwarded = true;
          result.freeSpinData.noofFreeSpinsAwarded = computeData.results.freeSpins;
        }

        if (!computeData.results.freeSpins) {
          result.freeSpinData.currentFreeSpin = session.eventData.freeSpinData.currentFreeSpin + 1;
        }

        if (result.wins.winCoins > 0) {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount + result.wins.winCoins;
          session.eventData.freeSpinData.winAmount = result.freeSpinData.winAmount;
        } else {
          result.freeSpinData.winAmount = session.eventData.freeSpinData.winAmount;
        }
          // call whow api
        resolve(this.callWhowBluehare(token, result, computeData, 0, 10, total, session));
      } else {
          // free spin object exists and there are no free spins remaining
        resolve(this.callWhowBluehare(token, result, computeData, (session.eventData.currentBet * session.eventData.currentLines), 0, total, session));
      }
    });
  }


  /**
   * This function is reponsilbe to mask the view zone for animation and security purpose
   * A initial configuration data is read and all reels are masked randomly from initial configuration
   */
  maskViewZone(viewZone, obj) {
    const noofReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
    const columnSize = parseInt(obj.game.default.gameConfig.viewZone.toString().charAt(0));
    const maskedReels = {};

    // construct fake reel with random symbols
    let reelSize = reelMask.INITIAL_REEL_SIZE;
    for (let i = 0; i < noofReels; i += 1) {
      if (i > 0) {
        reelSize += reelMask.REEL_SIZE_INCREMENT;
      }
      const symbolIndices = Object.keys(obj.game.default.gameConfig.reelConfig[`Reel${i + 1}`].SymbolDistribution);
      const reelArray = [];
      for (let j = 0; j < reelSize; j += 1) {
        reelArray.push(obj.game.default.gameConfig.reelConfig[`Reel${i + 1}`].SymbolDistribution[Math.floor(Math.random() * symbolIndices.length)].SymbolId);
      }

      // put view zone inside masked reel array
      const randomReelLocation = Math.floor(Math.random() * reelSize);
      for (let j = 0; j < columnSize; j += 1) {
        const reel = viewZone[`Reel${i + 1}`];
        const actualReelLocation = (randomReelLocation + j) % reelSize;
        reelArray[actualReelLocation] = reel[j].symbolId;
      }
      maskedReels[`Reel${i + 1}`] = { symbols: reelArray, stopAt: (randomReelLocation % reelSize) };
    }
    maskedReels.viewZone = viewZone.viewZone;
    return maskedReels;
  }


  /**
   * updates user's session with free spin data, wallet chips etc.
   * @param  session session schema
   * @param  gameId  game id
   */
  updateSession(session, gameId) {
    bucket.upsert(`${session.eventData.userDetails.id}::${gameId}`, session, (err) => {
      if (err) {
        console.log(err);
      }
    });
  }


  /**
   * update user session data
   * @param session - user's session data
   * @param docDetails - generated winning data, from spin result
   * it returns sessionRequest, which will be updated to database after execution
   */
  updateSessionData(session, docDetails) {
    session.gamble = docDetails.gamble;
    session.game = docDetails.game;
    session.eventData.userDetails.wallet = docDetails.wallet;
    if (docDetails.wildReel) {
      session.eventData.wildReel = docDetails.wildReel;
    }
    if (!_.isEmpty(session)) {
      session.event = 'spin';
      if (!_.isEmpty(docDetails.freeSpinData) && docDetails.freeSpinData.isFreeSpinAwarded) {
        delete docDetails.freeSpinData.isFreeSpinAwarded;
        delete docDetails.freeSpinData.noofFreeSpinsAwarded;
      }
      session.eventData.freeSpinData = docDetails.freeSpinData;
      // if free spins are completed, and symbol pick is not remaining (i.e., user didn't get 2 or 3 musketeers in the last fs)
      // then, remove freeSpin data
      if (session.eventData.freeSpinData.currentFreeSpin && session.eventData.freeSpinData.currentFreeSpin == session.eventData.freeSpinData.freeSpins) {
        session.eventData.freeSpinData = {};
      }
    }
    const sessionRequest = docDetails.session.sessionRequest;
    sessionRequest.status = 'ready';
    sessionRequest.spinCompletedTimestamp = moment().utc();
    return sessionRequest;
  }


  /**
   * updates user's session request document with provided document
   * no otimistic database concurreny is used in this update
   * @param  sessionRequestKey session request key
   * @param  sessionRequestDoc session request schema
   */
  updateSessionRequestDoc(sessionRequestKey, sessionRequestDoc) {
    bucket.upsert(sessionRequestKey, sessionRequestDoc, (err) => {
      if (err) {
        console.log(err);
      }
    });
  }


  /**
   * updates user's session and session requests
   * created when game click took place
   * @param  session           user session schema
   * @param  gameId            noble musketeers game id
   * @param  sessionRequestKey session request key
   * @param  sessionRequest    session request schema
   */
  updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest) {
    sessionRequest.status = 'ready';
    bucket.upsert(`${session.eventData.userDetails.id}::${gameId}`, session, (err) => {
      if (!err) {
        this.updateSessionRequestDoc(sessionRequestKey, sessionRequest);
      }
    });
  }


  /**
   * update user session when there are no free spins in poseidon
   * @param  gameId     poseidon game id
   * @param  token      user token
   * @param  docDetails user session schema
   * @param  gameData   game configuration schema
   */
  gameSessionUpdateMainGame(gameId, token, docDetails) {
    docDetails._updatedTimestamp = moment.utc();
    gameSession.getUserSession(token, gameId)
    .then((session) => {
      const sessionRequest = this.updateSessionData(session, docDetails);
      this.updateSessionAndSessionRequest(session, gameId, docDetails.session.sessionRequestKey, sessionRequest);
    }).catch((err) => {
      console.log(err);
    });
  }


  /**
  * This function is used to build game object for game click event
  */
  buildGameObjForGameClick(data) {
    const game = {};
    game.gameId = data._id;
    game.viewZone = data.game.default.gameConfig.viewZone;
    game.reels = {};
    game.reels.noofReels = data.game.default.gameConfig.reelConfig.NumberOfReels;

    for (let i = 0; i < data.game.default.gameConfig.reelConfig.NumberOfReels; i += 1) {
      const columnSize = parseInt(data.game.default.gameConfig.viewZone.toString().charAt(0));
      const reelArray = [];
      const randomNumber = Math.floor(Math.random() * data.game.default.gameConfig.reelConfig[`Reel${i + 1}`].NumberOfRows);
      for (let j = 0; j < columnSize; j += 1) {
        // do not give away reel configuration, mask initiail view
        reelArray.push(data.game.default.gameConfig.reelConfig[`Reel${i + 1}`].SymbolDistribution[(randomNumber + (j * Math.floor(Math.random() * 10))) % data.game.default.gameConfig.reelConfig[`Reel${i + 1}`].NumberOfRows].SymbolId);
      }
      game.reels[`Reel${i + 1}`] = reelArray;
    }
    game.payArray = data.game.default.gameConfig.payLines.PayArray;
    game.payTable = data.game.default.gameConfig.payTable;
    game.bigWins = data.game.default.gameConfig.bigWins;
    game.symbols = data.game.default.gameConfig.symbols;
    game.stage = data.game.default.gameConfig.stage;
    game.version = data.game.default.gameConfig.versionName;
    return game;
  }


  /**
  * This function is used to build user details object for game click event
  */
  buildUserDetailsForGameClick(data, currentLines) {
    const userDetails = data.userDetails;
    userDetails.selectablePaylines = data.game.default.gameConfig.selectablePaylines.options;
    userDetails.currentBet = data.userDetails.game.settings.bets[0] || 5;
    userDetails.currentLines = currentLines || parseInt(data.game.default.gameConfig.selectablePaylines.options[data.game.default.gameConfig.selectablePaylines.options.length - 1]) || 20;
    userDetails.totalBet = (userDetails.currentBet * userDetails.currentLines);
    return userDetails;
  }


  /**
  * This function is used to build game session data during game click
  */
  buildGameSessionGameClick(user, data, session) {
    const userDetails = JSON.parse(JSON.stringify(user));
    if (session) {
      userDetails.currentBet = session.eventData.currentBet;
      userDetails.currentLines = session.eventData.currentLines;
      userDetails.freeSpinData = session.eventData.freeSpinData;
      userDetails.totalBet = (userDetails.currentBet * userDetails.currentLines);
    } else {
      userDetails.currentBet = data.userDetails.game.settings.bets[0] || 5;
      userDetails.currentLines = parseInt(data.game.default.gameConfig.selectablePaylines.options[data.game.default.gameConfig.selectablePaylines.options.length - 1]) || 20;
      userDetails.freeSpinData = {};
      userDetails.totalBet = (userDetails.currentBet * userDetails.currentLines);
    }
    return userDetails;
  }


  /**
  * This function is used to build response for game click event
  * game data along with user data for subsequent  game events calls are passed to client
  */
  generateGameClickResponse(data, token, gameId) {
    const result = {};
    result.game = this.buildGameObjForGameClick(data);
    result.userDetails = this.buildUserDetailsForGameClick(data);

    return new Promise((resolve) => {
      gameSession.getUserSession(token, gameId)
      .then((session) => {
        result.userDetails = this.buildGameSessionGameClick(result.userDetails, data, session);
        resolve(result);
      }).catch(() => {
        result.userDetails = this.buildGameSessionGameClick(result.userDetails, data);
        resolve(result);
      });
    });
  }


  /**
  * This function builds a response in case where a client calls backend api with invalid client Id
  * this is necessary to ensure single session for a user for a game
  */
  invalidClientIdResponse() {
    return Promise.resolve({ error: constants.RES_MESSAGES.INVALID_CLIENT, errorCode: constants.RES_ERROR_CODES.INVALID_CLIENT });
  }


  /**
   * This is the actual whow api call function for bluehare games
   * bet api is called in each spin, informing whow, win amount of spin
   * virtualAmount is specified as non-zero, when the free spins are going on
   */
  callWhowBluehare(token, result, computeData, betAmount, virtualAmount, total, session, sessionRequestKey, sessionRequest, gameId) {
    return new Promise((resolve) => {
      whowapi.play(token, {
        betAmount,
        winAmount: total,
        virtualAmount
      }).then((response) => {
        response = JSON.parse(response);
        if (response.status && response.status === 110) {
          sessionRequest.status = 'ready';
          if (session.gamble && !_.isEmpty(session.gamble)) {
            session.gamble.winAmount = 0;
            session.gamble.count = 0;
          }
          session.eventData.freeSpinData = {};
          this.updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest);
          resolve({ error: constants.RES_MESSAGES.NOT_ENOUGH_BALANCE, errorCode: constants.RES_ERROR_CODES.NOT_ENOUGH_BALANCE });
        } else {
          if (!response.payload.user) {
            console.log(response.payload, 'payload');
          }
          if (response.payload.user) {
            result.wallet = response.payload.user.wallet;
            if (!result.levelData) {
              result.levelData = {};
            }
            result.levelData.level = response.payload.user.level;
            result.levelData.levelProgress = response.payload.user.levelProgress;
            result.jackpot = response.payload.user.jackpotData;
          } else if (session.eventData.userDetails && session.eventData.userDetails.wallet) {
            result.wallet = session.eventData.userDetails.wallet;
          }
          result.game = response.payload.game || {};
          result.game.betAmount = (session.eventData.currentBet * session.eventData.currentLines);
          result.gamble = (response.payload || {}).round || {};
          if (!session.gamble || !session.gamble.history) {
            result.gamble.history = [];
          } else {
            result.gamble.history = session.gamble.history;
          }
          result.gamble.count = 0;
          resolve(result);
        }
      }).catch((err) => {
        console.log(err);
        resolve(result);
      });
    });
  }


  /**
  * This function is used call whow api with relevant results of spin
  * if user wins then play api is called otherwise bet api is called
  */
  callWhow(token, result, computeData, betAmount, virtualAmount, total, session) {
    return new Promise((resolve) => {
      if (computeData.results.length > 0) {
        whowapi.bet(token, {
          betAmount,
          winAmount: total,
          virtualAmount
        }).then((response) => {
          response = JSON.parse(response);
          whowapi.close(token, {
            winAmount: total,
            roundId: response.payload.round.id
          });
          if (response.status && response.status === 110) {
            resolve({ error: constants.RES_MESSAGES.NOT_ENOUGH_BALANCE, errorCode: constants.RES_ERROR_CODES.NOT_ENOUGH_BALANCE });
          } else {
            result.gamble = response.payload.round;
            if (!session.gamble || !session.gamble.history) {
              result.gamble.history = [];
            } else {
              result.gamble.history = session.gamble.history;
            }
            result.gamble.winAmount = total;
            result.gamble.count = 0;
            result.levelData.level = response.payload.user.level;
            result.levelData.levelProgress = response.payload.user.levelProgress;
            result.wallet = response.payload.user.wallet;
            result.game = response.payload.game;
            result.game.betAmount = (session.eventData.currentBet * session.eventData.currentLines);
            result.jackpot = response.payload.user.jackpotData;
            resolve(result);
          }
        }).catch((err) => {
          console.log(err, 'err in whow api compute');
          resolve(result);
        });
      } else {
        // if user does not win whow api call to play is made to play a complete round and response is returned
        whowapi.play(token, {
          betAmount,
          winAmount: total,
          virtualAmount
        }).then((response) => {
          response = JSON.parse(response);

          if (response.status && response.status === 110) {
            resolve({ error: constants.RES_MESSAGES.NOT_ENOUGH_BALANCE, errorCode: constants.RES_ERROR_CODES.NOT_ENOUGH_BALANCE });
          } else {
            result.gamble = response.payload.round;
            if (!session.gamble || !session.gamble.history) {
              result.gamble.history = [];
            } else {
              result.gamble.history = session.gamble.history;
            }
            result.gamble.count = 0;
            result.levelData.level = response.payload.user.level;
            result.levelData.levelProgress = response.payload.user.levelProgress;
            result.wallet = response.payload.user.wallet;
            result.game = response.payload.game;
            result.game.betAmount = (session.eventData.currentBet * session.eventData.currentLines);
            result.jackpot = response.payload.user.jackpotData;
            resolve(result);
          }
        }).catch((err) => {
          resolve(result);
        });
      }
    });
  }
}

module.exports = new ComputeResults();
module.exports.Compute = ComputeResults;
