module.exports = {
  /**
  * generate random number based on game configuratoin
  * takes into account reel size and number of reels to generate random numbers within range
  */
  generateRandomNumber: (reelConfig) => {
    try {
      const noofReels = reelConfig.NumberOfReels;
      const randomNumbers = [];
      for (let i = 0; i < noofReels; i += 1) {
        const max = reelConfig[`Reel${i + 1}`].NumberOfRows;
        randomNumbers.push(Math.floor(Math.random() * max));
      }
      return randomNumbers;
    } catch (e) {
      return null;
    }
  },

  /**
  * generate random number based on game configuratoin
  * takes into account reel size and number of reels to generate random numbers within range
  * for SOA and Deep Seas
  */
  generateRandomNumberOld: (reelConfig) => {
    try {
      const noofReels = reelConfig.NumberOfReels;
      const randomNumbers = [];
      for (let i = 0; i < noofReels; i += 1) {
        const max = reelConfig[`Reel${i + 1}`].NumberOfRows;
        randomNumbers.push(Math.floor(Math.random() * (max + 1)));
      }
      return randomNumbers;
    } catch (e) {
      return null;
    }
  }
};
