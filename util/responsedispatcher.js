const constants = require('../response/constants');
const jsonResponse = require('../response/jsonResponse');

/**
* ResponseDispatch is a single point response dispatcher
* error and success results are dispatched
*/
class ResponseDispatch {

  /**
  * This function dispatches api responses
  * if business logic is executed then success response is passed with 200 response code
  */
  dispatch(res, data) {
    res.setHeader('Content-Type', constants.RES_HEADER.CONTENT_TYPE);
    res.setHeader('charset', constants.RES_HEADER.CHAR_SET);
    res.writeHead(constants.HTTP_STATUS.OK);
    res.write(jsonResponse(false, constants.HTTP_STATUS.USER_LOGIN, toCamel(data)));
    res.end();
  }

  /**
  * This function dispatches error api responses
  * in case of business validation fails error flag and message in response body is used to describe the result
  * 400 response code is passed to indicate error
  */
  dispatchError(res, data) {
    res.setHeader('Content-Type', constants.RES_HEADER.CONTENT_TYPE);
    res.setHeader('charset', constants.RES_HEADER.CHAR_SET);
    res.writeHead(constants.HTTP_STATUS.ERROR);
    res.write(jsonResponse(true, constants.HTTP_STATUS.USER_LOGIN, data));
    res.end();
  }


  /**
  * This function dispatches error api responses
  * a 404 response code is passed in case where a resource could not be found
  * utilized in case of invalid api call or http method
  */
  dispatch404(res) {
    res.setHeader('Content-Type', constants.RES_HEADER.CONTENT_TYPE);
    res.setHeader('charset', constants.RES_HEADER.CHAR_SET);
    res.writeHead(constants.HTTP_STATUS.NOT_FOUND);
    res.write(jsonResponse(true, constants.RES_MESSAGES.NOT_FOUND));
    res.end();
  }


  /**
  * This function dispatches health check response
  * a 200 response code is passed in case of health check api call
  * utilized to check if backend server is functioning and is available
  */
  dispatchPing(res) {
    res.setHeader('Content-Type', constants.RES_HEADER.CONTENT_TYPE);
    res.setHeader('charset', constants.RES_HEADER.CHAR_SET);
    res.writeHead(constants.HTTP_STATUS.OK);
    res.write(jsonResponse(false, constants.RES_MESSAGES.PONG));
    res.end();
  }

  /**
  * This function dispatches parameter validation error responses
  * a 422 response code is passed in case where parameter is either invalid or not present
  */
  dispatchValidationError(res, data) {
    res.setHeader('Content-Type', constants.RES_HEADER.CONTENT_TYPE);
    res.setHeader('charset', constants.RES_HEADER.CHAR_SET);
    res.writeHead(constants.HTTP_STATUS.UNPROCESSABLE_ENTITY);
    res.write(jsonResponse(true, constants.HTTP_STATUS.USER_LOGIN, data));
    res.end();
  }
}


/**
* this function is used to camel case the json in reponse objects
* it only camel cases keys
*/
function toCamel(o) {
  let newO,
    origKey,
    newKey,
    value;
  if (o instanceof Array) {
    newO = [];
    for (origKey in o) {
      value = o[origKey];
      if (typeof value === 'object') {
        value = toCamel(value);
      }
      newO.push(value);
    }
  } else {
    newO = {};
    for (origKey in o) {
      if (o.hasOwnProperty(origKey)) {
        newKey = (origKey.charAt(0).toLowerCase() + origKey.slice(1) || origKey).toString();
        value = o[origKey];
        if ((typeof value !== 'undefined' && value !== null && value.constructor === Object) || value instanceof Array) {
          value = toCamel(value);
        }
        newO[newKey] = value;
      }
    }
  }
  return newO;
}

module.exports = new ResponseDispatch();
