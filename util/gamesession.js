const { bucket } = require('../helpers/couchbase');
const moment = require('moment');
const Promise = require('bluebird');
const _ = require('lodash');

/**
* GameSessionOps handles user session related database operations
* this class utilizes primary database bucket
* to store user session and game data
*/
class GameSessionOps {

  /**
  * This function updates user session with game click event like
  * user details, current bet, current bet lines
  * this forms the basis of user and subsequent spin api calls can only happen once game click is successfull
  */
  gameClick(gameId, token, clientID, data, gameData) {
    bucket.get(`${data.userDetails.user.id}::${gameId}`, (err, result) => {
      if (err || !result) {
        this.createGameSessionObject(gameId, clientID, data, gameData);
      } else {
        result.value.clientID = clientID;
        this.buildUserSessionObject(result, data, gameData);
        this.updateSession(result.value, gameId);
      }
    });
    this.createUserDataAndSessionRequest(data, gameId, token);
  }


  /**
   * This function cerates userData object and sesion request object
   * user data contains user's data, retrieved from whow
   */
  createUserDataAndSessionRequest(data, gameId, token) {
    const userData = JSON.parse(JSON.stringify(data.userDetails.user));
    userData.created_at = new Date().getTime();
    userData.updated_at = new Date().getTime();
    userData._type = 'UserData';
    bucket.upsert(token, userData, (err) => {
      if (err) {
        console.log(err);
      }
    });

    const requestDoc = { _type: 'sessionRequest', status: 'init' };
    bucket.upsert(`${data.userDetails.user.id}::${gameId}::sessionRequest`, requestDoc, (err) => {
      if (err) {
        console.log(err);
      }
    });
  }


  /**
   * This function cerates user session in database
   * it initializes bet array, bet lines forming initial bet
   * for everyg= game
   */
  createGameSessionObject(gameId, clientID, data, gameData, lines) {
    let currentLines = lines;
    // if number of lines are not suppylied, assign from game
    if (!currentLines) {
      currentLines = parseInt(gameData.game.default.gameConfig.selectablePaylines.options[gameData.game.default.gameConfig.selectablePaylines.options.length - 1]) || 20;
    }
    const doc = {
      currentBet: data.userDetails.game.settings.bets[0] || 5,
      currentLines,
      userDetails: data.userDetails.user,
      freeSpinData: {}
    };
    bucket.upsert(`${data.userDetails.user.id}::${gameId}`, {
      _type: 'userSession',
      event: 'gameClick',
      clientID,
      eventData: doc,
      game: data.userDetails.game,
      _updatedTimestamp: moment.utc()
    }, (err) => {
      if (err) {
        console.log(err);
      }
    });
  }

  /**
   * updates user's session with free spin data, wallet chips etc.
   * @param  session session schema
   * @param  gameId  game id
   */
  updateSession(session, gameId) {
    bucket.upsert(`${session.eventData.userDetails.id}::${gameId}`, session, (err) => {
      if (err) {
        console.log(err);
      }
    });
  }


  /**
  * This function fetches user's current session for a game
  * session data includes current bet amount, current pay lines
  * and user profile related informations
  */
  getUserSession(token, gameId) {
    return new Promise((resolve, reject) => {
      bucket.get(token, (err, result) => {
        if (err) {
          reject(err);
        } else {
          bucket.get(`${result.value.id}::${gameId}`, result, (getSessionErr, session) => {
            if (getSessionErr) {
              reject(getSessionErr);
            } else {
              resolve(session.value);
            }
          });
        }
      });
    });
  }


  /**
  * gets user's session and session request documents
  * session request document would be used to manage concurrent requests
  */
  getUserSessionAndSessionRequest(token, gameId) {
    return new Promise((resolve, reject) => {
      bucket.get(token, (err, result) => {
        if (err) {
          reject(err);
        } else {
          bucket.getMulti([`${result.value.id}::${gameId}`, `${result.value.id}::${gameId}::sessionRequest`], (sessionError, session) => {
            if (sessionError) {
              reject(sessionError);
            } else {
              session.sessionKey = `${result.value.id}::${gameId}`;
              session.sessionRequestKey = `${result.value.id}::${gameId}::sessionRequest`;
              resolve(session);
            }
          });
        }
      });
    });
  }


  /**
  * updates user's session request document with provided document
  * no otimistic database concurreny is used in this update
  */
  updateSessionRequestDoc(sessionRequestKey, sessionRequestDoc) {
    bucket.upsert(sessionRequestKey, sessionRequestDoc, (err, result) => {
      if (err) {
        console.log(err);
      }
    });
  }


  /**
  * This updates user's session request document with provided document
  * Optimistic concurreny is handled with couchbase cas document
  * to ensure parallel spins for same game for a user does not happen unless previous spin is not finished
  */
  updateSessionRequestDocWithCas(sessionRequestKey, sessionRequestDoc, cas) {
    return new Promise((resolve, reject) => {
      bucket.upsert(sessionRequestKey, sessionRequestDoc, { cas }, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }


  /**
  * updates user game session with details of a spin result
  * it may include free spin if won in game spin
  * and updated wallet chips as a result of whow api call bet/play
  */
  gameSpin(gameId, token, docDetails) {
    docDetails._updatedTimestamp = moment.utc();
    this.getUserSession(token, gameId)
    .then((session) => {
      if (!_.isEmpty(session)) {
        session.event = 'spin';
        if (!_.isEmpty(docDetails.freeSpinData) && docDetails.freeSpinData.isFreeSpinAwarded) {
          delete docDetails.freeSpinData.isFreeSpinAwarded;
          delete docDetails.freeSpinData.noofFreeSpinsAwarded;
        }
        session.eventData.freeSpinData = docDetails.freeSpinData;
        if (session.eventData.freeSpinData.currentFreeSpin == session.eventData.freeSpinData.freeSpins) {
          session.eventData.freeSpinData = {};
        }
      }
      session.game = docDetails.game;
      session.eventData.userDetails.wallet = docDetails.wallet;
      docDetails.session.sessionRequest.status = 'ready';
      docDetails.session.sessionRequest.spinCompletedTimestamp = moment().utc();
      this.updateSessionAndSessionRequest(session, gameId, docDetails.session.sessionRequestKey, docDetails.session.sessionRequest);
      this.updateSession(session, gameId);
    }).catch((err) => {
      console.log(err);
    });
  }


  /**
  * updates user's current bet in database
  */
  updateUserCurrentBet(session, gameId, betAmount) {
    session.eventData.currentBet = betAmount;
    bucket.upsert(`${session.eventData.userDetails.id}::${gameId}`, session, (err) => {
      if (err) {
        console.log(err, 'er in updating user bet');
      }
    });
  }


  /**
  * updates user's current bet and lines in database
  */
  updateUserCurrentBetAndLines(session, gameId, betAmount, lines) {
    session.eventData.currentBet = betAmount;
    session.eventData.currentLines = lines;
    bucket.upsert(`${session.eventData.userDetails.id}::${gameId}`, session, (err) => {
      if (err) {
        console.log(err, 'er in updating user bet');
      }
    });
  }

  /**
  * updates user's bet lines in database
  */
  updateBetLines(session, gameId, betLines) {
    session.eventData.currentLines = betLines;
    bucket.upsert(`${session.eventData.userDetails.id}::${gameId}`, session, (err) => {
      if (err) {
        console.log(err, 'er in updating user bet');
      }
    });
  }


  /**
  * updates user's session and session requests
  * created when game click took place
  */
  updateSessionAndSessionRequest(session, gameId, sessionRequestKey, sessionRequest) {
    bucket.upsert(`${session.eventData.userDetails.id}::${gameId}`, session, (err) => {
      if (!err) {
        this.updateSessionRequestDoc(sessionRequestKey, sessionRequest);
      }
    });
  }


  /**
   * Would fetch the closests bet when user enters the game
   * It fetches closests match to a bet amount if the provided bet is not found
   */
  getClosestBet(currentBet, betArray) {
    if (!currentBet || !betArray) {
      return 5;
    }
    let distance = Math.abs(betArray[0] - currentBet);
    let idx = 0;
    for (let c = 1; c < betArray.length; c += 1) {
      const compDistance = Math.abs(betArray[c] - currentBet);
      if (compDistance < distance) {
          idx = c;
          distance = compDistance;
      }
    }
    return betArray[idx];
  }


  /**
   * Would build the user session object, from the user data stored  in database,
   * and user data fetched from whow
   * if user session object does not contain user details, game settings,
   * then it will be added from the data fetched from whow
   */
   buildUserSessionObject(result, data, gameData, currentLines) {
     if (result.value.eventData) {
       if (!result.value.eventData.currentBet) {
         result.value.eventData.currentBet = data.userDetails.game.settings.bets[0] || 5;
       }
       if (currentLines) {
         result.value.eventData.currentLines = currentLines;
       } else if (!result.value.eventData.currentLines) {
         result.value.eventData.currentLines = parseInt(gameData.game.default.gameConfig.selectablePaylines.options[gameData.game.default.gameConfig.selectablePaylines.options.length - 1]) || 20;
       }
     }
     if (!result.value.game) {
       result.value.game = data.userDetails.game;
     }
     if (data.userDetails && data.userDetails.user) {
       result.value.eventData.userDetails = data.userDetails.user;
     }
     if (result.value.game && result.value.game.settings && result.value.game.settings.bets && data.userDetails && data.userDetails.game && data.userDetails.game.settings && data.userDetails.game.settings.bets) {
       result.value.game.settings.bets = data.userDetails.game.settings.bets;
       if (
         (!result.value.eventData.freeSpinData || _.isEmpty(result.value.eventData.freeSpinData))
         && (!result.value.eventData.bonusData || _.isEmpty(result.value.eventData.bonusData))
       ) {
         result.value.eventData.currentBet = this.getClosestBet(result.value.eventData.currentBet, data.userDetails.game.settings.bets);
       }
     }
   }
}

module.exports = new GameSessionOps();
