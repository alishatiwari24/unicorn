/**
* export config
*/
module.exports = {
/**
* Application configuration
*/
  application: {
    env: 'development',
    hostName: 'localhost',
    httpPort: 9000
  },
/**
* bugsnag APi Key configuration
*/
  bugsnag: { apiKey: 'a6a618ca38d5cde04069144f654b99a6' }
};
