/**
* reel masking initial configuration is defined here
*/
const reelMask = {
  INITIAL_REEL_SIZE: 8,
  REEL_SIZE_INCREMENT: 4
};

module.exports = reelMask;
