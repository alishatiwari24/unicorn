# Slot Backend Server
This repository provides backend services for Slot clients.

This project is build with [NodeJs](https://github.com/angular/angular-cli) version 8.8.1.

# deployment steps
slot backend is a dockerized application
so, to deploy it, you just have to install **docker**, and **git**.
No extra dependency is needed to be installed!!

#### STEP-1 : Install Docker

Follow the articles to install docker

* [How to install docker on ubuntu](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/)
* [How to install docker on windows](https://docs.docker.com/toolbox/toolbox_install_windows/)
* [How to install docker on mac](https://docs.docker.com/docker-for-mac/install/)

#### STEP-2 : Install Git

Follow the article to install git

[How to install git](https://www.atlassian.com/git/tutorials/install-git)

#### STEP-3 : Clone the project

open a terminal and type

**git clone https://github.com/whowgames/snb-http-server.git**

this will copy project to your local machine
#### STEP-4 : Docker Build

to build the docker, go inside the project folder and type

**docker build . -t image-name**

provide appropriate **image-name**.
For example,*docker build . -t slot-backend* will build an image of *slot-backend* with a tag - *slot-backend:latest*

docker build will take a few minutes, when building for the first time.

#### STEP-5 : Docker Run
after the docker build is complete, start docker container

you can do it using,

**docker run -d -p 9000:9000 --name backend slot-backend**

* **-d** : to run docker daemon in background
* **-p** : to provide container and host ports. slot backend runs on port 9000 on container. you can provide host and container ports by *-p host-port:container-port*. here, both are 9000.
* **--name** : to provide running container an appropriate name


By following above steps, slot backend can be easily deployed, without installing any external dependencies.

When redeploying it, you have to first remove currently running container by following command:

**docker rm -f backend**

after that, build docker container again, and run it!

## Running unit tests

Unit tests can be executed via [Node Package Manager](https://www.npmjs.com), execute `npm run tests` from project root to see test results

## APIs

Currently the application is hosted on Amazon ECS, [ping](http://ecs-first-run-alb-1992293428.eu-central-1.elb.amazonaws.com/ping) to check the status of the application.

Api docs can be found at [apidoc](http://ecs-first-run-alb-1992293428.eu-central-1.elb.amazonaws.com/apidoc).
