const Game = require('../helpers/gamefunction');
const responseDispatcher = require('../util/responsedispatcher');
const url = require('url');

/**
 * This class handles all mobile client apis
 * Version, asset bundle are some of the apis that are implemented
 */
class MobileController {

/**
 * This function returns the current version of the assessts that are and must be in production
 * it returns assesst bundle that client would use to download new assessts
 */
  getVersion(req, res) {
    Game.getVersion(req.body.gameId, req.body.token, url.parse(req.url, true).query.platform)
    .then((data) => {
      responseDispatcher.dispatch(res, data);
      return data;
    }).catch((err) => {
      responseDispatcher.dispatchError(res, err);
      console.log(err);
    });
  }
}
module.exports = new MobileController();
