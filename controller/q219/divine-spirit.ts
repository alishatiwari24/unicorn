import { GameFunction } from '../../helpers/gamefunction';
import constants = require('../../response/constants');
import compute = require('../../util/compute.js');
import responseDispatcher = require('../../util/responsedispatcher');
import { GameDetail, SessionObject, ComputeData, SessionResult, SessionRequestValue, SpinFinalResult } from 'interface/q219/divine-spirit';
import logger = require('../../util/logger.js');
import ClientRequest from 'interface/client-request';
import divineSpirit from '../../util/games/q219/divine-spirit';
import http from 'http';

/**
 * DivineSpirit class contains controller method such as spin method
 */
class DivineSpirit extends GameFunction {


    /**
     * Overrides handle spin function of divineSpirit game
     * Spin handles game spin event on divineSpirit, it checks, which reel set is selected
     */
    public async spin(req: ClientRequest, res: http.ServerResponse) {
        try {
            const result: SpinFinalResult = await new DivineSpirit().handleGameSpin(req.body.gameId,
                req.body.token, req.body.clientID);
            const resultCopy: SpinFinalResult = JSON.parse(JSON.stringify(result));
            delete result.session;
            if (result.error) {
                responseDispatcher.dispatchError(res, result);
            } else {
                responseDispatcher.dispatch(res, result);
            }
            if (!resultCopy.error) {
                try {
                    const logResult: SpinFinalResult = JSON.parse(JSON.stringify(resultCopy));
                    /*
                    * update user session, if not updated yet
                    */
                    if (!result.isSessionUpdated) {
                        compute.gameSessionUpdateMainGame(req.body.gameId, req.body.token, resultCopy);
                    } else {
                        const sessionRequest: SessionRequestValue = resultCopy.session.sessionRequest;
                        sessionRequest.status = 'ready';
                        compute.updateSessionRequestDoc(resultCopy.session.sessionRequestKey, sessionRequest);
                    }
                    logger.logSpinResult(logResult, req.body.token, req.body.gameId);
                } catch (e) {
                    console.log(e);
                }
            }
        } catch (e) {
            responseDispatcher.dispatch(res, {
                error: constants.RES_MESSAGES.SPIN_FAILED,
                errorCode: constants.RES_ERROR_CODES.SPIN_FAILED,
            });
        }
    }


    /**
     * Handles game spin event from controller
     * @param  {[type]}  gameId   [divineSpirit game id]
     * @param  {[type]}  token    [playing user token]
     * @param  {[type]}  clientID [unique client id used to track sessions]
     * @return {Promise}          [description]
     */
    public async handleGameSpin(gameId: string, token: string, clientID: string): Promise<SpinFinalResult> {
        try {
            const game: GameDetail = super.getGame(gameId);
            if (!game) {
                const error: SpinFinalResult = {
                    error: constants.RES_MESSAGES.GAME_NOT_FOUND,
                    erroCode: constants.RES_ERROR_CODES.GAME_NOT_FOUND,
                };
                return error;
            }

            const results: SessionResult = await super.getUserSessionAndSessionRequest(token, gameId);
            if (!results.sessionRequestKey || !results.sessionKey) {
                const error: SpinFinalResult = {
                    error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK,
                    erroCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK,
                };
                return error;
            }

            const sessionRequest: SessionRequestValue = results[results.sessionRequestKey].value;
            if (sessionRequest.status !== 'init' && sessionRequest.status !== 'ready') {
                const error: SpinFinalResult = {
                    error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED,
                    erroCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED,
                };
                return error;
            }

            const session: SessionObject = results[results.sessionKey].value;
            if (session.clientID !== clientID) {
                const error: SpinFinalResult = {
                    error: constants.RES_MESSAGES.INVALID_CLIENT,
                    erroCode: constants.RES_ERROR_CODES.INVALID_CLIENT,
                };
                return error;
            }

            try {
                await super.updateSessionRequestDoc(results.sessionRequestKey, sessionRequest, results[results.sessionRequestKey].cas);
                try {
                    const obj: GameDetail = JSON.parse(JSON.stringify(game));
                    const computeData: ComputeData = await divineSpirit.compute(obj.game, session);
                    return divineSpirit.generateSpinResponse(obj, computeData, token, results.sessionRequestKey, sessionRequest, session);
                } catch (e) {
                    const error: SpinFinalResult = {
                        error: constants.RES_MESSAGES.SPIN_FAILED,
                        erroCode: constants.RES_ERROR_CODES.SPIN_FAILED,
                    };
                    return error;
                }
            } catch (e) {
                const error: SpinFinalResult = {
                    error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED,
                    erroCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED,
                };
                return error;
            }
        } catch (e) {
            const error: SpinFinalResult = {
                error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK,
                erroCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK,
            };
            return error;
        }
    }

}
export default new DivineSpirit();
