/*eslint no-var: "off"*/
import { GameFunction } from '../../helpers/gamefunction';
import constants = require('../../response/constants');
import compute = require('../../util/compute.js');
import responseDispatcher = require('../../util/responsedispatcher');
import logger = require('../../util/logger.js');
import ClientRequest from 'interface/client-request';
import casanova from '../../util/games/q219/casanova';
import http from 'http';
import { ComputeData, CasanovaGame, SessionStore, Session, SessionRequest, SessionData, Result } from 'interface/q219/cassanova-type';
import { ErrorMessage } from 'interface/Game';
import * as _ from 'lodash';

/**
 * Handles game events for Casanova
 * @extends GameFunction
 */
class Casanova extends GameFunction {


    /**
     * Overrides handle spin function of casanova game
     * Spin handles game spin event on casanova, it checks, which reel set is selected
     */
    public async spin(req: ClientRequest, res: http.ServerResponse) {
        try {
            const result = await new Casanova().handleGameSpin(req.body.gameId as string,
                req.body.token as string, req.body.clientID as string);
            const resultCopy: Result = _.cloneDeep(result);
            delete result.session;
            if (result.error) {
                responseDispatcher.dispatchError(res, result);
            } else {
                responseDispatcher.dispatch(res, result);
            }
            if (!resultCopy.error) {
                try {
                    const logResult = _.cloneDeep(resultCopy);
                    /*
                    * update user session, if not updated yet
                    */
                    if (!result.isSessionUpdated) {
                        compute.gameSessionUpdateMainGame(req.body.gameId, req.body.token, resultCopy);
                    } else {
                        const sessionRequest = resultCopy.session.sessionRequest;
                        sessionRequest.status = 'ready';
                        compute.updateSessionRequestDoc(resultCopy.session.sessionRequestKey, sessionRequest);
                    }
                    logger.logSpinResult(logResult, req.body.token, req.body.gameId);

                } catch (e) {
                    console.log(e);
                }
            }
        } catch (e) {
            const error: ErrorMessage = {
                error: constants.RES_MESSAGES.SPIN_FAILED,
                errorCode: constants.RES_ERROR_CODES.SPIN_FAILED,
            };

            return error;
        }
    }


    /**
     * Handles game spin event from controller
     * @param  {[type]}  gameId   [casanova game id]
     * @param  {[type]}  token    [playing user token]
     * @param  {[type]}  clientID [unique client id used to track sessions]
     * @return {Promise}          [description]
     */
    public async handleGameSpin(gameId: string, token: string, clientID: string): Promise<Result> {
        try {
            const game: CasanovaGame = super.getGame(gameId);

            if (!game) {
                const error: Result = {
                    error: constants.RES_MESSAGES.GAME_NOT_FOUND,
                    errorCode: constants.RES_ERROR_CODES.GAME_NOT_FOUND,
                };
                return error;
            }

            const results: SessionStore = await super.getUserSessionAndSessionRequest(token, gameId);

            if (!results.sessionRequestKey || !results.sessionKey) {
                const error: Result = {
                    error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK,
                    errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK,
                };
                return error;
            }

            const sessionRequest: SessionRequest = (results[results.sessionRequestKey as string] as SessionData).value as SessionRequest;
            if (sessionRequest.status as string !== 'init' && sessionRequest.status !== 'ready') {
                const error: Result = {
                    error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED,
                    errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED,
                };
                return error;
            }

            const session: Session = (results[results.sessionKey as string] as SessionData).value as Session;
            if (session.clientID !== clientID) {
                const error: Result = {
                    error: constants.RES_MESSAGES.INVALID_CLIENT,
                    errorCode: constants.RES_ERROR_CODES.INVALID_CLIENT,
                };
                return error;
            }

            try {
                await super.updateSessionRequestDoc(results.sessionRequestKey, sessionRequest,
                    (results[results.sessionRequestKey as string] as SessionData).cas as string);
                try {
                    const obj = _.cloneDeep(game) as CasanovaGame;
                    const computeData: ComputeData = await casanova.compute(obj.game, session);
                    return casanova.generateSpinResponse(obj, computeData, token, results.sessionRequestKey as string,
                        sessionRequest, session);
                } catch (e) {
                    console.log(e);
                    const error: Result = {
                        error: constants.RES_MESSAGES.SPIN_FAILED as string,
                        errorCode: constants.RES_ERROR_CODES.SPIN_FAILED as number,
                    };
                    return error;
                }
            } catch (e) {
                const error: Result = {
                    error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED as string,
                    errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED as number,
                };
                return error;
            }
        } catch (e) {
            const error: Result = {
                error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK as string,
                errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK as number,
            };
            return error;
        }
    }
}

export default new Casanova();
