const gameFunction = require('../helpers/gamefunction');
const zombieApoc = require('../util/games/zombie-apocalypse');
const responseDispatcher = require('../util/responsedispatcher');
const constants = require('../response/constants');
const logger = require('../util/logger');
const compute = require('../util/compute');

/**
 * Special features and Bonus feature is handled by this class
 * it also handles gamble request from clients
 */
class ZombieApocalypseController {


  /**
  * This function handles game click event and receives data from node router
  * Game click logic in game function is invoked in this method
  * and game session invoked would store game click data
  * request and response objects are passed to it from node router
  * @param game_id
  * @param token
  * @param clientID
  * are utilized for game click
  */
  gameClick(req, res) {
    zombieApoc.handleGameClickZA(req.body.gameId, req.body.token)
    .then((data) => {
      responseDispatcher.dispatch(res, data);
      return data;
    })
    .then((data) => {
      if (data.userDetails) {
        const gameData = gameFunction.getGame(req.body.gameId);
        zombieApoc.handleGameClick(req.body.gameId, req.body.token, req.body.clientID, data, gameData);
      }
    }).catch((err) => {
      responseDispatcher.dispatchError(res, err);
      console.log(err);
    });
  }


  /**
   * Spin handles game spin event on secret of amun
   * Since unicorn slot contains roaming reel feature
   * reel set on each spin is managed in this function
   */
  spin(req, res) {
    try {
      new ZombieApocalypseController().handleSpin(req.body.gameId, req.body.token, req.body.clientID)
      .then((result) => {
        const resultCopy = JSON.parse(JSON.stringify(result));
        delete result.session;
        if (result.gamble) {
          delete result.gamble.id;
          delete result.gamble.betAmount;
          delete result.gamble.winAmount;
          delete result.gamble.timestamp;
        }
        if (result.expandingPositions) {
          delete result.expandingPositions;
        }
        if (result.bonus) {
          result = zombieApoc.filterBonusResponse(result);
        }
        if (result.error) {
          responseDispatcher.dispatchError(res, result);
        } else {
          responseDispatcher.dispatch(res, result);
        }
        return resultCopy;
      })
      .then((result) => {
        if (!result.error) {
          const logResult = JSON.parse(JSON.stringify(result));
          if (!result.isSessionUpdated) {
            zombieApoc.gameSpin(req.body.gameId, req.body.token, result);
          }
          zombieApoc.logSpinResult(logResult, req.body.token, req.body.gameId);
        }
      }).catch((err) => {
        console.log(err);
        responseDispatcher.dispatch(res, { error: constants.RES_MESSAGES.SPIN_FAILED, errorCode: constants.RES_ERROR_CODES.SPIN_FAILED });
      });
    } catch (e) {
      console.log(e);
    }
  }


  /*
  * This function handles spin request for secret of amun
  * generates a random number based on game configuration
  * computes the result with the help of compute helper and generates the response
  * fallbacks in case of user spin being active
  * and checks if spin request is valid.
  */
  handleSpin(gameId, token, clientID) {
    return new Promise((resolve, reject) => {
      const obj = gameFunction.getGame(gameId);
      if (!obj) {
        resolve({ error: constants.RES_MESSAGES.GAME_NOT_FOUND, erroCode: constants.RES_ERROR_CODES.GAME_NOT_FOUND });
      } else {
        gameFunction.getUserSessionAndSessionRequest(token, gameId)
        .then((results) => {
          if (results.sessionRequestKey && results.sessionKey) {
            const sessionRequest = results[results.sessionRequestKey].value;
            if (sessionRequest.status == 'init' || sessionRequest.status == 'ready') {
              const session = results[results.sessionKey].value;

              // allow spin
              if (session.eventData.bonus && session.eventData.bonus !== undefined && session.eventData.bonus.isActive) {
                resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_BONUS_ACTIVE });
              } else if (session.clientID === clientID) {
                // update session request doc
                gameFunction.updateSessionRequestDoc(results.sessionRequestKey, sessionRequest, results[results.sessionRequestKey].cas)
                .then(() => {
                  zombieApoc.compute(obj.game, gameId, token, session)
                  .then((computeData) => {
                    resolve(zombieApoc.generateSpinResponse(obj, computeData, token, results.sessionRequestKey, sessionRequest, session));
                  }).catch((err) => {
                    console.log(err);
                  });
                })
                .catch((error) => {
                  console.log(error);
                  resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED });
                });
              } else {
                resolve(compute.invalidClientIdResponse());
              }
            } else {
              resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED });
            }
          } else {
            // reject spin as no session request document is present
            resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK });
          }
        }).catch(() => {
          resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK });
        });
      }
    });
  }


  /**
   * handles bonus events from client
   * request is forwarded ZA helper
   * where validations and outcomes are determined
   */
  handleBonusRequest(req, res) {
    zombieApoc.handleBonusRequest(req.body.token, req.body.gameId, req.body.zombieId)
    .then((result) => {
      if (result.error) {
        responseDispatcher.dispatchError(res, result);
      } else {
        responseDispatcher.dispatch(res, result);
      }
    })
    .catch((err) => {
      responseDispatcher.dispatchError(res, err);
    });
  }


  /**
   * This function handles game request coming from client
   * it utilizes ZA helper to determine outcomes for gamble request
   * and for logical validtions
   */
  gamble(req, res) {
    zombieApoc.handleGambleRequest(req.body.token, req.body.gameId, req.body.clientID, req.body.pickedCard)
      .then((result) => {
        if (result.error) {
          responseDispatcher.dispatchError(res, result);
        } else {
          responseDispatcher.dispatch(res, result);
        }
      }).catch((err) => {
        console.log(err);
        responseDispatcher.dispatch(res, err);
      });
  }


  /**
   * This function is used to collect gamble winnings
   * it utilizes ZA helper to pick existing wins
   * and do all logical validations
   */
  pickGamble(req, res) {
    zombieApoc.pickGamble(req.body.token, req.body.gameId, req.body.clientID)
      .then((result) => {
        if (result.error) {
          responseDispatcher.dispatchError(res, result);
        } else {
          responseDispatcher.dispatch(res, result);
        }
      }).catch((err) => {
        console.log(err);
        responseDispatcher.dispatch(res, err);
      });
  }

}
module.exports = new ZombieApocalypseController();
