const gameSession = require('../util/gamesession');
const gameFunction = require('../helpers/gamefunction');
const nobleMusketeersSession = require('../util/games/noble-musketeers');
const responseDispatcher = require('../util/responsedispatcher');
const constants = require('../response/constants');
const logger = require('../util/logger');
const firehoseLogger = require('../helpers/firehose');
const compute = require('../util/compute');
const _ = require('lodash');
const whowapi = require('../api/whowapi');

/**
 * NobleMusketeersController - Handles spin event for noble musketeers game
 * noble musketeers contains an anyway-win feature
 * i.e., payout doesn't depend on payline, and pays for all left to right combinations
 */
class NobleMusketeersController {

  /**
  * This function handles game click event and receives data from node router
  * Game click logic in noble-musketeers helper is invoked in this method
  * and game session invoked would store game click data
  * request and response objects are passed to it from node router
  * @param game_id
  * @param token
  * @param clientID
  * are utilized for game click
  */
  gameClick(req, res) {
    new NobleMusketeersController().handleGameClick(req.body.gameId, req.body.token)
    .then((data) => {
      if (data.error) {
        responseDispatcher.dispatchError(res, data);
      } else {
        data.userDetails.currentLines = constants.NOBLE_MUSKETEERS.NOOF_LINES;
        responseDispatcher.dispatch(res, data);
      }
      return data;
    })
    .then((data) => {
      if (data.userDetails) {
        const gameData = gameFunction.getGame(req.body.gameId);
        nobleMusketeersSession.handleGameClick(req.body.gameId, req.body.token, req.body.clientID, data, gameData);
      }
    }).catch((err) => {
      responseDispatcher.dispatchError(res, err);
      console.log(err);
    });
  }


  /**
  * This function handles game click event from controller
  * WHOW API is called to get user data from token
  * relevant user data along with game sessions are created using compute helper
  */
  handleGameClick(gameId, token) {
    return new Promise((resolve, reject) => {
      whowapi.get(token)
      .then((response) => {
        const obj = gameFunction.getGame(gameId);
        if (!obj) {
          resolve({ error: constants.RES_MESSAGES.GAME_NOT_FOUND, erroCode: constants.RES_ERROR_CODES.GAME_NOT_FOUND });
        } else if (response.status === 200) {
          obj.userDetails = response.payload;
          resolve(nobleMusketeersSession.generateGameClickResponse(obj, token, gameId));
        } else {
          resolve({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
        }
      }).catch((err) => {
        console.log(err);
        reject({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
      });
    });
  }


  /**
   * Overrides handle spin function of noble musketeers game
   * this ia an anyway game, i.e., it doesn't depend on payline, and pays for all left to right combinations
   */
  spin(req, res) {
    try {
      new NobleMusketeersController().handleGameSpin(req.body.gameId, req.body.token, req.body.clientID).then((result) => {
        const resultCopy = JSON.parse(JSON.stringify(result));
        delete result.session;
        if (result.error) {
          responseDispatcher.dispatchError(res, result);
        } else {
          // if bonus data does not contain symbol pick response, delete it from the response
          if (result.bonusData && result.bonusData.isSymbolPicked == undefined) {
            delete result.bonusData;
          } else {
            // delete freeSpinMultipliers and multipliers from bonusData, in order to not to expose it to the user
            delete result.bonusData.freeSpinMultipliers;
            delete result.bonusData.multipliers;
          }
          delete result.isSessionUpdated;
          responseDispatcher.dispatch(res, result);
        }
        return resultCopy;
      })
      .then((result) => {
        if (!result.error) {
          const logResult = JSON.parse(JSON.stringify(result));
          /*
          * update user session, if not updated yet
          */
          if (!result.isSessionUpdated) {
            nobleMusketeersSession.gameSessionUpdateMainGame(req.body.gameId, req.body.token, result);
          } else {
            const sessionRequest = result.session.sessionRequest;
            sessionRequest.status = 'ready';
            nobleMusketeersSession.updateSessionRequestDoc(result.session.sessionRequestKey, sessionRequest);
          }
          // if bonus data is empty, remove it from the result getting logged
          if (_.isEmpty(logResult.bonusData)) {
            delete logResult.bonusData;
          }
          logger.logSpinResult(logResult, req.body.token, req.body.gameId);
        }
      }).catch((err) => {
        console.log(err);
        responseDispatcher.dispatch(res, { error: constants.RES_MESSAGES.SPIN_FAILED, errorCode: constants.RES_ERROR_CODES.SPIN_FAILED });
      });
    } catch (e) {
      console.log(e);
    }
  }


  /**
   * Overrides handle spin function of base game features
   * @param  {[type]} gameId   game id of noble musketeers
   * @param  {[type]} token    user token
   * @param  {[type]} clientID generated client id
   */
  handleGameSpin(gameId, token, clientID) {
    return new Promise((resolve, reject) => {
      const game = gameFunction.getGame(gameId);
      if (!game) {
        resolve({ error: constants.RES_MESSAGES.GAME_NOT_FOUND, erroCode: constants.RES_ERROR_CODES.GAME_NOT_FOUND });
      } else {
        gameFunction.getUserSessionAndSessionRequest(token, gameId)
        .then((results) => {
          if (results.sessionRequestKey && results.sessionKey) {
            const sessionRequest = results[results.sessionRequestKey].value;
            if (sessionRequest.status == 'init' || sessionRequest.status == 'ready') {
              const session = results[results.sessionKey].value;
              // if symbol pick is remaining (after the bonus of 2 or 3 musketeers), send error
              if (session.eventData.bonusData && session.eventData.bonusData.isSymbolPicked === false) {
                resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_BONUS_ACTIVE, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_BONUS_ACTIVE });
              } else {
                const obj = JSON.parse(JSON.stringify(game));
                if (!_.isEmpty(session.eventData.freeSpinData) && (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0)) {
                  obj.game.default.gameConfig.freeSpin.reelConfig.NumberOfReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
                  obj.game.default.gameConfig.reelConfig = obj.game.default.gameConfig.freeSpin.reelConfig;
                }
                // allow spin
                if (session.clientID === clientID) {
                  // update session request doc
                  gameFunction.updateSessionRequestDoc(results.sessionRequestKey, sessionRequest, results[results.sessionRequestKey].cas)
                  .then(() => {
                    nobleMusketeersSession.compute(obj.game, gameId, token, session)
                    .then((computeData) => {
                      resolve(nobleMusketeersSession.generateSpinResponse(obj, computeData, token, results.sessionRequestKey, sessionRequest, session));
                    }).catch((err) => {
                      console.log(err);
                    });
                  })
                  .catch((error) => {
                    console.log(error, 'error');
                    resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED });
                  });
                } else {
                  resolve(compute.invalidClientIdResponse());
                }
              }
            } else {
              resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED });
            }
          } else {
            // reject spin as no session request document is present
            resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK });
          }
        }).catch((err) => {
          console.log(err);
          resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK });
        });
      }
    });
  }


  /**
   * handles bonus events from client
   * request is forwarded KAD helper
   * where validations and outcomes are determined
   */
  handleBonusRequest(req, res) {
    gameFunction.getUserSessionAndSessionRequest(req.body.token, req.body.gameId)
    .then((sessionCas) => {
      const session = sessionCas[sessionCas.sessionKey].value;
      let response = {};
      if (session.clientID !== req.body.clientID) {
        response = { error: constants.RES_MESSAGES.INVALID_CLIENT, errorCode: constants.RES_ERROR_CODES.INVALID_CLIENT };
      } else if (
        !session.eventData.bonusData || session.eventData.bonusData.isSymbolPicked !== false || !session.eventData.bonusData.freeSpinMultipliers
      ) {
        response = { error: constants.RES_MESSAGES.BONUS_NOT_ACTIVE, errorCode: constants.RES_ERROR_CODES.BONUS_NOT_ACTIVE };
      } else if (session.eventData.bonusData && session.eventData.bonusData.freeSpinMultipliers && session.eventData.bonusData.freeSpinMultipliers[req.body.cardId] === undefined) {
        response = { error: constants.RES_MESSAGES.PICK_NOT_FOUND, errorCode: constants.RES_ERROR_CODES.PICK_NOT_FOUND };
      } else if (session.pickStatus === 'locked') {
        response = { error: constants.RES_MESSAGES.CARD_BEING_PICKED, errorCode: constants.RES_ERROR_CODES.GAMBLE_BEING_PICKED };
      }
      if (response.error) {
        responseDispatcher.dispatchError(res, response);
      } else {
        session.pickStatus = 'locked';
        nobleMusketeersSession.updateSessionWithCas(session, req.body.gameId, sessionCas[sessionCas.sessionKey].cas).then(() => {
          nobleMusketeersSession.handleBonusRequest(req.body.token, req.body.gameId, req.body.cardId, session)
          .then((result) => {
            if (result.error) {
              responseDispatcher.dispatchError(res, result);
            } else {
              if (!_.isEmpty(result.bonusObj)) {
                result.bonusObj.baseGameId = req.body.baseGameId;
                firehoseLogger.info(result.bonusObj);
              }
              responseDispatcher.dispatch(res, result.response);
            }
          })
          .catch((err) => {
            responseDispatcher.dispatchError(res, err);
          });
        }).catch(() => {
          responseDispatcher.dispatchError(res, { error: constants.RES_MESSAGES.CARD_BEING_PICKED, errorCode: constants.RES_ERROR_CODES.GAMBLE_BEING_PICKED });
        });
      }
    })
    .catch((err) => {
      responseDispatcher.dispatchError(res, err);
    });
  }

  /**
  * This function gets user session data, such as current bet, current bet lines,
  * free spin status if any for agame
  * it receives request and response objects from node router
  * @param game_id
  * @param token
  * are used to query game session for user's session data
  */
  getSession(req, res) {
    gameSession.getUserSession(req.body.token, req.body.gameId)
    .then((result) => {
      if (result.clientID === req.body.clientID) {
        const response = gameFunction.generateUserSessionResponse(result);
        response.eventData.bonusData = result.eventData.bonusData;
        if (!_.isEmpty(result.eventData.bonusData)) {
          delete response.eventData.bonusData.freeSpinMultipliers;
        }
        result.eventData.gameName = 'NobleMusketeers';
        responseDispatcher.dispatch(res, response);
      } else {
        responseDispatcher.dispatchError(res, { error: constants.RES_MESSAGES.INVALID_CLIENT, errorCode: constants.RES_ERROR_CODES.INVALID_CLIENT });
      }
    }).catch((err) => {
      console.log(err);
      responseDispatcher.dispatchError(res, { error: constants.RES_MESSAGES.NO_SESSION_DATA, errorCode: constants.RES_ERROR_CODES.NO_SESSION_DATA });
    });
  }
}
module.exports = new NobleMusketeersController();
