const gameSession = require('../util/gamesession');
const gameFunction = require('../helpers/gamefunction');
const dragonPhoenixSession = require('../util/games/dragon-phoenix');
const responseDispatcher = require('../util/responsedispatcher');
const constants = require('../response/constants');
const logger = require('../util/logger');
const compute = require('../util/compute');
const whowapi = require('../api/whowapi');

/**
 * DragonPhoenixController - Handles spin event for dragon & phoenix game
 * dragon & phoenix contains a re-spin feature
 */
class DragonPhoenixController {
  /**
  * This function handles game click event and receives data from node router
  * Game click logic in dragon & phoenix helper is invoked in this method
  * and game session invoked would store game click data
  * request and response objects are passed to it from node router
  * @param game_id
  * @param token
  * @param clientID
  * are utilized for game click
  */
  gameClick(req, res) {
    new DragonPhoenixController().handleGameClick(req.body.gameId, req.body.token)
    .then((data) => {
      if (data.error) {
        responseDispatcher.dispatchError(res, data);
      } else {
        data.userDetails.currentLines = constants.DRAGON_PHOENIX.NOOF_LINES;
        responseDispatcher.dispatch(res, data);
      }
      return data;
    })
    .then((data) => {
      if (data.userDetails) {
        const gameData = gameFunction.getGame(req.body.gameId);
        dragonPhoenixSession.handleGameClick(req.body.gameId, req.body.token, req.body.clientID, data, gameData);
      }
    }).catch((err) => {
      responseDispatcher.dispatchError(res, err);
      console.log(err);
    });
  }


  /**
  * This function handles game click event from controller
  * WHOW API is called to get user data from token
  * relevant user data along with game sessions are created using compute helper
  */
  handleGameClick(gameId, token) {
    return new Promise((resolve, reject) => {
      whowapi.get(token)
      .then((response) => {
        const obj = gameFunction.getGame(gameId);
        if (!obj) {
          resolve({ error: constants.RES_MESSAGES.GAME_NOT_FOUND, erroCode: constants.RES_ERROR_CODES.GAME_NOT_FOUND });
        } else if (response.status === 200) {
          obj.userDetails = response.payload;
          resolve(dragonPhoenixSession.generateGameClickResponse(obj, token, gameId));
        } else {
          resolve({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
        }
      }).catch((err) => {
        console.log(err);
        reject({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
      });
    });
  }


  /**
   * Overrides handle spin function of dragon & phoenix game
   * this game contains a re-spin feature, so reels respin for 2 times, if feature is triggered
   */
  spin(req, res) {
    try {
      new DragonPhoenixController().handleGameSpin(req.body.gameId, req.body.token, req.body.clientID).then((result) => {
        const resultCopy = JSON.parse(JSON.stringify(result));
        delete result.session;
        if (result.error) {
          responseDispatcher.dispatchError(res, result);
        } else {
          if (result.bonusData) {
            result.bonusData.heldSymbols = result.bonusData.heldSymbols.symbolList;
          }
          delete result.isSessionUpdated;
          responseDispatcher.dispatch(res, result);
        }
        return resultCopy;
      })
      .then((result) => {
        if (!result.error) {
          const logResult = JSON.parse(JSON.stringify(result));
          /*
          * update user session, if not updated yet
          */
          if (!result.isSessionUpdated) {
            dragonPhoenixSession.gameSessionUpdateMainGame(req.body.gameId, req.body.token, result);
          } else {
            const sessionRequest = result.session.sessionRequest;
            sessionRequest.status = 'ready';
            dragonPhoenixSession.updateSessionRequestDoc(result.session.sessionRequestKey, sessionRequest);
          }
          // log result of the last re spin only
          if (!result.bonusData) {
            logger.logSpinResult(logResult, req.body.token, req.body.gameId);
          }
        }
      }).catch((err) => {
        console.log(err);
        responseDispatcher.dispatch(res, { error: constants.RES_MESSAGES.SPIN_FAILED, errorCode: constants.RES_ERROR_CODES.SPIN_FAILED });
      });
    } catch (e) {
      console.log(e);
    }
  }


  /**
   * Overrides handle spin function of base game features
   * @param  {[type]} gameId   game id of dragon & phoenix
   * @param  {[type]} token    user token
   * @param  {[type]} clientID generated client id
   */
  handleGameSpin(gameId, token, clientID) {
    return new Promise((resolve, reject) => {
      const game = gameFunction.getGame(gameId);
      if (!game) {
        resolve({ error: constants.RES_MESSAGES.GAME_NOT_FOUND, erroCode: constants.RES_ERROR_CODES.GAME_NOT_FOUND });
      } else {
        gameFunction.getUserSessionAndSessionRequest(token, gameId)
        .then((results) => {
          if (results.sessionRequestKey && results.sessionKey) {
            const sessionRequest = results[results.sessionRequestKey].value;
            if (sessionRequest.status == 'init' || sessionRequest.status == 'ready') {
              const session = results[results.sessionKey].value;
              const obj = dragonPhoenixSession.selectReelConfig(session, JSON.parse(JSON.stringify(game)));
              // allow spin
              if (session.clientID === clientID) {
                // update session request doc
                gameFunction.updateSessionRequestDoc(results.sessionRequestKey, sessionRequest, results[results.sessionRequestKey].cas)
                .then(() => {
                  dragonPhoenixSession.compute(obj.game, gameId, token, session)
                  .then((computeData) => {
                    resolve(dragonPhoenixSession.generateSpinResponse(obj, computeData, token, results.sessionRequestKey, sessionRequest, session));
                  }).catch((err) => {
                    console.log(err);
                  });
                })
                .catch((error) => {
                  console.log(error, 'error');
                  resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED });
                });
              } else {
                resolve(compute.invalidClientIdResponse());
              }
            } else {
              resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED });
            }
          } else {
            // reject spin as no session request document is present
            resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK });
          }
        }).catch((err) => {
          console.log(err);
          resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK });
        });
      }
    });
  }


  /**
  * This function gets user session data, such as current bet, current bet lines,
  * free spin status if any for agame
  * it receives request and response objects from node router
  * @param game_id
  * @param token
  * are used to query game session for user's session data
  */
  getSession(req, res) {
    gameSession.getUserSession(req.body.token, req.body.gameId)
    .then((result) => {
      const response = gameFunction.generateUserSessionResponse(result);
      if (result.eventData.bonusData) {
        response.eventData.bonusData = result.eventData.bonusData;
        response.eventData.bonusData.heldSymbols = result.eventData.bonusData.heldSymbols.symbolList;
      }
      responseDispatcher.dispatch(res, response);
    }).catch((err) => {
      console.log(err);
      responseDispatcher.dispatchError(res, { error: constants.RES_MESSAGES.NO_SESSION_DATA, errorCode: constants.RES_ERROR_CODES.NO_SESSION_DATA });
    });
  }
}
module.exports = new DragonPhoenixController();
