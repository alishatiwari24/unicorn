const Game = require('../helpers/gamefunction');
const gameSession = require('../util/gamesession');
const url = require('url');
const _ = require('lodash');
const responseDispatcher = require('../util/responsedispatcher');
const constants = require('../response/constants');
const logger = require('../util/logger');

/**
* RequestController acts as controller to various game events.
* - Game click
* - Game spin
* - Increasing and decreasing bet lines
* - Increasing and decreasing bet amount
* - Getting max bet
* - are some of the events this controller handles
* Game events are triggered as an action of user interaction
*/
class RequestController {
  /**
  * This function handles game click event and receives data from node router
  * Game click logic in game function is invoked in this method
  * and game session invoked would store game click data
  * request and response objects are passed to it from node router
  * @param game_id
  * @param token
  * @param clientID
  * are utilized for game click
  */
  gameClick(req, res) {
    Game.handleGameClick(req.body.gameId, req.body.token)
    .then((data) => {
      if (data.error) {
        responseDispatcher.dispatchError(res, data);
      } else {
        responseDispatcher.dispatch(res, data);
      }
      return data;
    })
    .then((data) => {
      if (data.userDetails) {
        const gameData = Game.getGame(req.body.gameId);
        gameSession.gameClick(req.body.gameId, req.body.token, req.body.clientID, data, gameData);
      }
    }).catch((err) => {
      responseDispatcher.dispatchError(res, err);
      console.log(err);
    });
  }

  /**
  * This function handles various game events such as
  * bet increase, bet descrease, increase bet line,
  * decrease bet line, setting bet line, max bet
  * it receives request and response objects from node router
  * relevant game function method is invoked to actual perform the events
  * @param game_id
  * @param token
  * are used to perform the events
  */
  gameEvent(req, res) {
    switch (url.parse(req.url, true).query.event) {
      case constants.GAME_EVENT.INCREASE_BET:
        new RequestController().increaseBet(req, res);
        break;

      case constants.GAME_EVENT.DECREASE_BET:
        new RequestController().decreaseBet(req, res);
        break;

      case constants.GAME_EVENT.MAX_BET:
        new RequestController().maxBet(req, res);
        break;

      case constants.GAME_EVENT.INCREASE_BET_LINE:
        new RequestController().increaseBetLine(req, res);
        break;

      case constants.GAME_EVENT.DECREASE_BET_LINE:
        new RequestController().decreaseBetLine(req, res);
        break;

      case constants.GAME_EVENT.SET_BET_LINE:
        new RequestController().setBetLine(req, res);
        break;
      default:
        responseDispatcher.dispatch(res, { error: constants.RES_MESSAGES.INVALID_GAME_EVENT, erroCode: constants.RES_ERROR_CODES.INVALID_GAME_EVENT });
    }
  }


  /**
  * This function handles spin event triggered when user presses spin button
  * game function hanldes spining based on game configuration and user session
  * a logger function than exports the spin result in elastic search for further analysis
  * it receives request and response objects from node router
  * @param game_id
  * @param token
  * are utilized to spin on a game
  */
  spin(req, res) {
    Game.handleSpin(req.body.gameId, req.body.token, req.body.clientID).then((result) => {
      const resultCopy = JSON.parse(JSON.stringify(result));
      delete result.session;
      if (result.error) {
        responseDispatcher.dispatchError(res, result);
      } else {
        responseDispatcher.dispatch(res, result);
      }
      return resultCopy;
    })
    .then((result) => {
      if (!result.error) {
        const logResult = JSON.parse(JSON.stringify(result));
        if (!result.isSessionUpdated) {
          gameSession.gameSpin(req.body.gameId, req.body.token, result);
        }
        logger.logSpinResult(logResult, req.body.token, req.body.gameId);
      }
    }).catch((err) => {
      console.log(err);
      responseDispatcher.dispatch(res, { error: constants.RES_MESSAGES.SPIN_FAILED, errorCode: constants.RES_ERROR_CODES.SPIN_FAILED });
    });
  }


  /**
  * This function gets user session data, such as current bet, current bet lines,
  * free spin status if any for agame
  * it receives request and response objects from node router
  * @param game_id
  * @param token
  * are used to query game session for user's session data
  */
  getSession(req, res) {
    gameSession.getUserSession(req.body.token, req.body.gameId)
    .then((result) => {
      const response = Game.generateUserSessionResponse(result);
      responseDispatcher.dispatch(res, response);
    }).catch((err) => {
      console.log(err);
      responseDispatcher.dispatchError(res, { error: constants.RES_MESSAGES.NO_SESSION_DATA, errorCode: constants.RES_ERROR_CODES.NO_SESSION_DATA });
    });
  }


  /**
  * This function gets pay table based on game session of the user
  * game function fetches the data of paytable
  * it receives request and response objects from node router
  * @param game_id
  * @param token
  * are used to fetch paytable for a game
  */
  getPayTable(req, res) {
    Game.getPaytble(req.body.token, req.body.gameId)
    .then((result) => {
      responseDispatcher.dispatch(res, result);
    }).catch((err) => {
      responseDispatcher.dispatchError(res, { error: constants.RES_MESSAGES.NO_PAYTABLE });
    });
  }


  /**
  * This function increases user bet based on current session data of user
  * game session is used to updatednew bet
  * it receives request and response objects from node router
  * @param gameId
  * @param token
  * are used to increase user bet
  */
  increaseBet(req, res) {
    gameSession.getUserSession(req.body.token, req.body.gameId)
    .then((result) => {
      if (!_.isEmpty(result.eventData.freeSpinData) || !_.isEmpty(result.eventData.bonus)) {
        throw new Error(constants.RES_MESSAGES.BET_ALTER_DENIED_FREE_SPIN_ACTIVE);
      } else if (result.eventData.bonusData && (result.eventData.bonusData.isSymbolPicked === false || result.eventData.bonusData.isReSpinTriggered === true)) {
        throw new Error(constants.RES_MESSAGES.BET_ALTER_DENIED_BONUS_ACTIVE);
      } else if (result.clientID === req.body.clientID) {
        const closestBet = gameSession.getClosestBet(result.eventData.currentBet, result.game.settings.bets);
        const key = _.findIndex(result.game.settings.bets, item => item == closestBet);

        // call below api if whow needs it and evaluates it in their backend
        // Game.increaseBet(req.body.token, result.game.settings.bets[(key + 1) % result.game.settings.bets.length]);

        const bet = result.game.settings.bets[(key + 1) % result.game.settings.bets.length];
        responseDispatcher.dispatch(res, { currentLines: result.eventData.currentLines, currentBet: bet, totalBet: (result.eventData.currentLines * bet) });
        result.currentBet = result.game.settings.bets[(key + 1) % result.game.settings.bets.length];
        return result;
      }

      throw new Error(constants.RES_MESSAGES.INVALID_CLIENT);
    })
    .then((result) => {
      gameSession.updateUserCurrentBet(result, req.body.gameId, result.currentBet);
    }).catch((err) => {
      responseDispatcher.dispatchError(res, { error: err.message });
    });
  }


  /**
  * This function decreases user bet based on current session data of user
  * game session is used to update new bet
  * it receives request and response objects from node router
  * @param gameId
  * @param token
  * are used to decrease user bet
  */
  decreaseBet(req, res) {
    gameSession.getUserSession(req.body.token, req.body.gameId)
      .then((result) => {
        if (!_.isEmpty(result.eventData.freeSpinData) || !_.isEmpty(result.eventData.bonus)) {
          throw new Error(constants.RES_MESSAGES.BET_ALTER_DENIED_FREE_SPIN_ACTIVE);
        } else if (result.eventData.bonusData && (result.eventData.bonusData.isSymbolPicked === false || result.eventData.bonusData.isReSpinTriggered === true)) {
          throw new Error(constants.RES_MESSAGES.BET_ALTER_DENIED_BONUS_ACTIVE);
        } else if (result.clientID === req.body.clientID) {
          const closestBet = gameSession.getClosestBet(result.eventData.currentBet, result.game.settings.bets);
          let key = _.findIndex(result.game.settings.bets, item => item == closestBet);
          if (key == 0) {
            key = result.game.settings.bets.length;
          }

          // call below api if whow needs it and evaluates it in their backend
          const bet = result.game.settings.bets[(key - 1) % result.game.settings.bets.length];
          responseDispatcher.dispatch(res, { currentLines: result.eventData.currentLines, currentBet: bet, totalBet: (result.eventData.currentLines * bet) });
          result.currentBet = result.game.settings.bets[(key - 1) % result.game.settings.bets.length];
          return result;
        }
        throw new Error(constants.RES_MESSAGES.INVALID_CLIENT);
      })
      .then((result) => {
        gameSession.updateUserCurrentBet(result, req.body.gameId, result.currentBet);
      }).catch((err) => {
        responseDispatcher.dispatchError(res, { error: err.message });
      });
  }


  /**
  * Thus function sets a maximum possible bet that a user can make on a game
  * game session is used to update maximum possible bet for a user
  * it receives request and response objects from node router
  * @param gameId
  * @param token
  * are used to compute max bet for user
  */
  maxBet(req, res) {
    gameSession.getUserSession(req.body.token, req.body.gameId)
    .then((result) => {
      if (!_.isEmpty(result.eventData.freeSpinData) || !_.isEmpty(result.eventData.bonus)) {
        throw new Error(constants.RES_MESSAGES.BET_ALTER_DENIED_FREE_SPIN_ACTIVE);
      } else if (result.eventData.bonusData && (result.eventData.bonusData.isSymbolPicked === false || result.eventData.bonusData.isReSpinTriggered === true)) {
        throw new Error(constants.RES_MESSAGES.BET_ALTER_DENIED_BONUS_ACTIVE);
      } else if (result.clientID === req.body.clientID) {
        const bet = result.game.settings.bets[result.game.settings.bets.length - 1];

        const gameData = Game.getGame(req.body.gameId);
        let lines = parseInt(gameData.game.default.gameConfig.selectablePaylines.options[gameData.game.default.gameConfig.selectablePaylines.options.length - 1]);
        if (
          req.body.baseGameId === constants.GAME_IDS.CINDERELLA || req.body.baseGameId === constants.GAME_IDS.NOBLE_MUSKETEERS
          || req.body.baseGameId === constants.GAME_IDS.DEEP_SEAS || req.body.baseGameId === constants.GAME_IDS.DRAGON_PHOENIX
        ) {
          lines = result.eventData.currentLines;
        }
        responseDispatcher.dispatch(res, { currentLines: lines, currentBet: bet, totalBet: (result.eventData.currentLines * bet) });
        result.currentBet = bet;
        result.currentLines = lines;
        return result;
      }

      throw new Error(constants.RES_MESSAGES.INVALID_CLIENT);
    })
    .then((result) => {
      gameSession.updateUserCurrentBetAndLines(result, req.body.gameId, result.currentBet, result.currentLines);
    }).catch((err) => {
      responseDispatcher.dispatchError(res, { error: err.message });
    });
  }


  /**
  * This function increases no of lines to bet on based on available options according to game configuration
  * game function is used to compute and update bet lines
  * it receives request and response objects from node router
  * @param gameId
  * @param token
  * are used to increase bet lines
  */
  increaseBetLine(req, res) {
    if (req.body.baseGameId === constants.GAME_IDS.SECRET_OF_AMUN
      || req.body.baseGameId === constants.GAME_IDS.UNICORN_SLOT
      || req.body.baseGameId === constants.GAME_IDS.PARIS_TOURIST
      || req.body.baseGameId === constants.GAME_IDS.ZOMBIE_APOCALYPSE
      || req.body.baseGameId === constants.GAME_IDS.KNIGHT_DRAGONS) {
      responseDispatcher.dispatchError(res, { error: constants.RES_MESSAGES.CANNOT_INCREASE_BET_LINE });
    } else {
      Game.increaseBetLine(req.body.token, req.body.gameId, req.body.clientID)
      .then((result) => {
        responseDispatcher.dispatch(res, result);
      }).catch((err) => {
        console.log(err);
        responseDispatcher.dispatchError(res, { error: err });
      });
    }
  }


  /**
  * This function decreases no of lines to bet on based on available options according to game configuration
  * game function is used to compute and update bet lines
  * it receives request and response objects from node router
  * @param gameId
  * @param token
  * are used to decrease bet lines
  */
  decreaseBetLine(req, res) {
    if (req.body.baseGameId === constants.GAME_IDS.SECRET_OF_AMUN
      || req.body.baseGameId === constants.GAME_IDS.UNICORN_SLOT
      || req.body.baseGameId === constants.GAME_IDS.PARIS_TOURIST
      || req.body.baseGameId === constants.GAME_IDS.ZOMBIE_APOCALYPSE
      || req.body.baseGameId === constants.GAME_IDS.KNIGHT_DRAGONS) {
      responseDispatcher.dispatchError(res, { error: constants.RES_MESSAGES.CANNOT_DECREASE_BET_LINE });
    } else {
      Game.decreaseBetLine(req.body.token, req.body.gameId, req.body.clientID)
      .then((result) => {
        responseDispatcher.dispatch(res, result);
      }).catch((err) => {
        console.log(err);
        responseDispatcher.dispatchError(res, { error: err });
      });
    }
  }


  /**
  * This function sets no of lines to bet on based on available options according to game configuration
  * game function is used to set and update bet lines
  * it receives request and response objects from node router
  * @param gameId
  * @param token
  * are used to set bet lines
  */
  setBetLine(req, res) {
    if (req.body.baseGameId === constants.GAME_IDS.SECRET_OF_AMUN
      || req.body.baseGameId === constants.GAME_IDS.UNICORN_SLOT
      || req.body.baseGameId === constants.GAME_IDS.PARIS_TOURIST
      || req.body.baseGameId === constants.GAME_IDS.ZOMBIE_APOCALYPSE
      || req.body.baseGameId === constants.GAME_IDS.KNIGHT_DRAGONS) {
      responseDispatcher.dispatchError(res, { error: constants.RES_MESSAGES.CANNOT_SET_BET_LINE });
    } else {
      Game.setBetLine(req.body.token, req.body.gameId, req.body.betLine, req.body.clientID)
        .then((result) => {
          responseDispatcher.dispatch(res, result);
        }).catch((err) => {
          console.log(err);
          responseDispatcher.dispatchError(res, { error: err });
        });
    }
  }


/**
 * This function fetches all the playable games that the end user can play
 * game function is used to poll such data from database
 */
  getAllPlayableGames(req, res) {
    Game.getAllPlayableGames()
    .then((result) => {
      responseDispatcher.dispatch(res, { playableGames: result });
    })
    .catch((err) => {
      responseDispatcher.dispatchError(res, { error: constants.RES_MESSAGES.SPIN_FAILED, errorCode: constants.RES_ERROR_CODES.SPIN_FAILED });
      console.log(err);
    });
  }


  /**
   * Contoller function that gets symbol pay of the symbol id provided
   * In order to get symbol pay, a user must be playing the game for which symbolId is queried
   */
  getSymbolPay(req, res) {
    Game.getSymbolPay(req.body.token, req.body.gameId, req.body.symbolId)
    .then((result) => {
      responseDispatcher.dispatch(res, { symbolPay: result });
    })
    .catch((err) => {
      responseDispatcher.dispatchError(res, { error: err });
    });
  }
}

module.exports = new RequestController();
