const gameFunction = require('../helpers/gamefunction');
const responseDispatcher = require('../util/responsedispatcher');
const constants = require('../response/constants');
const compute = require('../util/compute');
const backStreet = require('../util/games/backer-street-detective');
const _ = require('lodash');
const logger = require('../util/logger');

/**
 * Controller to backer street Detective
 * Responsible for handling spin, gamble and bonus requests
 */
class BackerStreetDetective {

  /**
   * Spin handles game spin event on backer street detective
   * spin results are logged to a centralized datastore
   * and user session is updated accordingly
   */
  spin(req, res) {
    try {
      new BackerStreetDetective().handleSpin(req.body.gameId, req.body.token, req.body.clientID)
      .then((result) => {
        const resultCopy = JSON.parse(JSON.stringify(result));
        delete result.session;
        if (result.error) {
          responseDispatcher.dispatchError(res, result);
        } else {
          responseDispatcher.dispatch(res, result);
        }
        return resultCopy;
      })
      .then((result) => {
        if (!result.error) {
          const logResult = JSON.parse(JSON.stringify(result));
          if (result.isSessionUpdated) {
            result.session.sessionRequest.status = 'ready';
            backStreet.updateSessionRequestDoc(result.session.sessionRequestKey, result.session.sessionRequest);
          } else {
            backStreet.gameSessionUpdateMainGame(req.body.gameId, req.body.token, result);
          }
          logger.logSpinResult(logResult, req.body.token, req.body.gameId);
        }
      }).catch(() => {
        responseDispatcher.dispatchError(res, { error: constants.RES_MESSAGES.SPIN_FAILED, errorCode: constants.RES_ERROR_CODES.SPIN_FAILED });
      });
    } catch (e) {
      console.log(e);
    }
  }


  /*
  * This function handles spin request for secret of amun
  * generates a random number based on game configuration
  * computes the result with the help of compute helper and generates the response
  * fallbacks in case of user spin being active
  * and checks if spin request is valid.
  */
  handleSpin(gameId, token, clientID) {
    return new Promise((resolve) => {
      const game = gameFunction.getGame(gameId);
      if (!game) {
        resolve({ error: constants.RES_MESSAGES.GAME_NOT_FOUND, erroCode: constants.RES_ERROR_CODES.GAME_NOT_FOUND });
      } else {
        gameFunction.getUserSessionAndSessionRequest(token, gameId)
        .then((results) => {
          if (results.sessionRequestKey && results.sessionKey) {
            const sessionRequest = results[results.sessionRequestKey].value;
            if (sessionRequest.status === 'init' || sessionRequest.status === 'ready') {
              const session = results[results.sessionKey].value;
              // allow spin
              if (session.eventData.freeSpinData && session.eventData.freeSpinData.isCardPicked === false) {
                resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_BONUS_ACTIVE, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_BONUS_ACTIVE });
              } else if (session.clientID === clientID) {
                const obj = JSON.parse(JSON.stringify(game));
                if (!_.isEmpty(session.eventData.freeSpinData) && (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0)) {
                  obj.game.default.gameConfig.freeSpin.reelConfig.NumberOfReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
                  obj.game.default.gameConfig.reelConfig = obj.game.default.gameConfig.freeSpin.reelConfig;
                }
                // update session request doc
                gameFunction.updateSessionRequestDoc(results.sessionRequestKey, sessionRequest, results[results.sessionRequestKey].cas)
                .then(() => {
                  backStreet.compute(obj.game, gameId, token, session)
                  .then((computeData) => {
                    resolve(backStreet.generateSpinResponse(obj, computeData, token, results.sessionRequestKey, sessionRequest, session));
                  }).catch((err) => {
                    console.log(err, 'err88');
                  });
                })
                .catch((error) => {
                  console.log(error, 'error92');
                  resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED });
                });
              } else {
                resolve(compute.invalidClientIdResponse());
              }
            } else {
              console.log(sessionRequest, 'sessionRequest');
              resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED });
            }
          } else {
            // reject spin as no session request document is present
            resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK });
          }
        }).catch(() => {
          resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK });
        });
      }
    });
  }


  /**
   * handles bonus events from client
   * request is forwarded BSD helper
   * where validations and outcomes are determined
   */
  handleBonusRequest(req, res) {
    gameFunction.getUserSessionAndSessionRequest(req.body.token, req.body.gameId)
    .then((sessionCas) => {
      const session = sessionCas[sessionCas.sessionKey].value;
      let response = {};
      if (session.clientID !== req.body.clientID) {
        response = { error: constants.RES_MESSAGES.INVALID_CLIENT, errorCode: constants.RES_ERROR_CODES.INVALID_CLIENT };
      } else if (
        !session.eventData.freeSpinData || session.eventData.freeSpinData.isCardPicked !== false || !session.eventData.cards
      ) {
        response = { error: constants.RES_MESSAGES.BONUS_NOT_ACTIVE, errorCode: constants.RES_ERROR_CODES.BONUS_NOT_ACTIVE };
      } else if (session.eventData.freeSpinData && session.eventData.cards && session.eventData.cards[req.body.cardId] === undefined) {
        response = { error: constants.RES_MESSAGES.PICK_NOT_FOUND, errorCode: constants.RES_ERROR_CODES.PICK_NOT_FOUND };
      }
      if (response.error) {
        responseDispatcher.dispatchError(res, response);
      } else {
        backStreet.handleBonusRequest(req.body.gameId, req.body.cardId, session)
        .then((result) => {
          if (result.error) {
            responseDispatcher.dispatchError(res, result);
          } else {
            responseDispatcher.dispatch(res, result.response);
          }
        })
        .catch((err) => {
          console.log(err, 'eee');
          responseDispatcher.dispatchError(res, err);
        });
      }
    })
    .catch((err) => {
      console.log(err, 'err');
      responseDispatcher.dispatchError(res, err);
    });
  }
}
module.exports = new BackerStreetDetective();
