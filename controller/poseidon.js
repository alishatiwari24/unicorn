const gameFunction = require('../helpers/gamefunction');
const poseidonSession = require('../util/games/poseidon');
const responseDispatcher = require('../util/responsedispatcher');
const constants = require('../response/constants');
const logger = require('../util/logger');
const compute = require('../util/compute');
const _ = require('lodash');

/**
 * PoseidonController - Handles spin event for poseidon game
 */
class PoseidonController {
  /**
   * Overrides handle spin function of poseidon game
   */
  spin(req, res) {
    try {
      new PoseidonController().handleGameSpin(req.body.gameId, req.body.token, req.body.clientID).then((result) => {
        const resultCopy = JSON.parse(JSON.stringify(result));
        delete result.session;
        if (result.error) {
          responseDispatcher.dispatchError(res, result);
        } else {
          delete result.isSessionUpdated;
          responseDispatcher.dispatch(res, result);
        }
        return resultCopy;
      })
      .then((result) => {
        if (!result.error) {
          const logResult = JSON.parse(JSON.stringify(result));
          /*
          * update user session, if not updated yet
          */
          if (!result.isSessionUpdated) {
            poseidonSession.gameSessionUpdateMainGame(req.body.gameId, req.body.token, result);
          } else {
            const sessionRequest = result.session.sessionRequest;
            sessionRequest.status = 'ready';
            poseidonSession.updateSessionRequestDoc(result.session.sessionRequestKey, sessionRequest);
          }
          logger.logSpinResult(logResult, req.body.token, req.body.gameId);
        }
      }).catch((err) => {
        console.log(err);
        responseDispatcher.dispatch(res, { error: constants.RES_MESSAGES.SPIN_FAILED, errorCode: constants.RES_ERROR_CODES.SPIN_FAILED });
      });
    } catch (e) {
      console.log(e);
    }
  }


  /**
   * Overrides handle spin function of base game features
   * @param  {[type]} gameId   game id of poseidon
   * @param  {[type]} token    user token
   * @param  {[type]} clientID generated client id
   */
  handleGameSpin(gameId, token, clientID) {
    return new Promise((resolve, reject) => {
      const game = gameFunction.getGame(gameId);
      if (!game) {
        resolve({ error: constants.RES_MESSAGES.GAME_NOT_FOUND, errorCode: constants.RES_ERROR_CODES.GAME_NOT_FOUND });
      } else {
        gameFunction.getUserSessionAndSessionRequest(token, gameId)
        .then((results) => {
          if (results.sessionRequestKey && results.sessionKey) {
            const sessionRequest = results[results.sessionRequestKey].value;
            if (sessionRequest.status == 'init' || sessionRequest.status == 'ready') {
              const session = results[results.sessionKey].value;
              const obj = JSON.parse(JSON.stringify(game));
              if (!_.isEmpty(session.eventData.freeSpinData) && (session.eventData.freeSpinData.freeSpins - session.eventData.freeSpinData.currentFreeSpin > 0)) {
                obj.game.default.gameConfig.freeSpin.reelConfig.NumberOfReels = obj.game.default.gameConfig.reelConfig.NumberOfReels;
                obj.game.default.gameConfig.reelConfig = obj.game.default.gameConfig.freeSpin.reelConfig;
              }
              // allow spin
              if (session.clientID === clientID) {
                // update session request doc
                gameFunction.updateSessionRequestDoc(results.sessionRequestKey, sessionRequest, results[results.sessionRequestKey].cas)
                .then(() => {
                  poseidonSession.compute(obj.game, gameId, token, session)
                  .then((computeData) => {
                    resolve(poseidonSession.generateSpinResponse(obj, computeData, token, results.sessionRequestKey, sessionRequest, session));
                  }).catch((err) => {
                    console.log(err);
                  });
                })
                .catch((error) => {
                  console.log(error, 'error');
                  resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED });
                });
              } else {
                resolve(compute.invalidClientIdResponse());
              }
            } else {
              resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED });
            }
          } else {
            // reject spin as no session request document is present
            resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK });
          }
        }).catch((err) => {
          console.log(err);
          resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK });
        });
      }
    });
  }
}
module.exports = new PoseidonController();
