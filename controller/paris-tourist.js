const gameFunction = require('../helpers/gamefunction');
const responseDispatcher = require('../util/responsedispatcher');
const constants = require('../response/constants');
const compute = require('../util/compute');
const GameFunction = require('../helpers/gamefunction').GameFunction;
const parisTouristHelper = require('../util/games/paris-tourist');
const logger = require('../util/logger');

/**
 * Paris tourist related game controls are transfered in this class.
 * It takes care of spin event on the game
 */
class ParisTourist extends GameFunction {


  /**
   * Handles game click for unicorn slot game
   * as unicorn slot contains roaming reel feature, user session needs additional details
   * that additional detail is managed by this function
   */
  gameClick(req, res) {
    new GameFunction().handleGameClick(req.body.gameId, req.body.token)
    .then((data) => {
      responseDispatcher.dispatch(res, data);
      return data;
    })
    .then((data) => {
      if (data.userDetails) {
        const gameData = super.getGame(req.body.gameId);
        parisTouristHelper.handleGameClick(req.body.gameId, req.body.token, req.body.clientID, data, gameData);
      }
    }).catch((err) => {
      responseDispatcher.dispatchError(res, err);
      console.log(err);
    });
  }


  spinFortuneWheel(req, res) {
    parisTouristHelper.spinFortuneWheel(req.body.token, req.body.gameId)
    .then((data) => {
      responseDispatcher.dispatch(res, { pickedSymbol: data });
    })
    .catch((err) => {
      responseDispatcher.dispatchError(res, err);
    });
  }


  spin(req, res) {
    try {
      new ParisTourist().handleSpin(req.body.gameId, req.body.token, req.body.clientID)
      .then((result) => {
        const resultCopy = JSON.parse(JSON.stringify(result));
        delete result.session;
        if (result.error) {
          responseDispatcher.dispatchError(res, result);
        } else {
          responseDispatcher.dispatch(res, result);
        }
        return resultCopy;
      })
      .then((result) => {
        if (!result.error) {
          const logResult = JSON.parse(JSON.stringify(result));
          if (!result.isSessionUpdated) {
            parisTouristHelper.gameSpin(req.body.gameId, req.body.token, result);
          }
          // TODO log results here, make necessary changes to the document if required
          logger.logSpinResult(logResult, req.body.token, req.body.gameId);
        }
      }).catch((err) => {
        responseDispatcher.dispatch(res, { error: constants.RES_MESSAGES.SPIN_FAILED, errorCode: constants.RES_ERROR_CODES.SPIN_FAILED });
      });
    } catch (e) {
      console.log(e);
    }
  }


  /*
  * This function handles spin request for secret of amun
  * generates a random number based on game configuration
  * computes the result with the help of compute helper and generates the response
  * fallbacks in case of user spin being active
  * and checks if spin request is valid.
  */
  handleSpin(gameId, token, clientID) {
    return new Promise((resolve, reject) => {
      const obj = gameFunction.getGame(gameId);
      if (!obj) {
        resolve({ error: constants.RES_MESSAGES.GAME_NOT_FOUND, erroCode: constants.RES_ERROR_CODES.GAME_NOT_FOUND });
      } else {
        gameFunction.getUserSessionAndSessionRequest(token, gameId)
        .then((results) => {
          if (results.sessionRequestKey && results.sessionKey) {
            const sessionRequest = results[results.sessionRequestKey].value;
            if (sessionRequest.status == 'init' || sessionRequest.status == 'ready') {
              const session = results[results.sessionKey].value;

              // allow spin
              if (session.clientID === clientID) {
                // update session request doc
                gameFunction.updateSessionRequestDoc(results.sessionRequestKey, sessionRequest, results[results.sessionRequestKey].cas)
                // TODO Compute must be overriden here to satisfy special features
                .then(() => {
                  parisTouristHelper.compute(obj.game, gameId, token, session)
                  .then((computeData) => {
                    resolve(parisTouristHelper.generateSpinResponse(obj, computeData, token, results.sessionRequestKey, sessionRequest, session));
                  }).catch((err) => {
                    console.log(err);
                  });
                })
                .catch((error) => {
                  console.log(error);
                  resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED });
                });
              } else {
                resolve(compute.invalidClientIdResponse());
              }
            } else {
              resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED });
            }
          } else {
            // reject spin as no session request document is present
            resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK });
          }
        }).catch(() => {
          resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK });
        });
      }
    });
  }


  /**
   * This function handles game request coming from client
   * it utilizes SOA helper to determine outcomes for gamble request
   * and for logical validtions
   */
  gamble(req, res) {
    parisTouristHelper.handleGambleRequest(req.body.token, req.body.gameId, req.body.clientID, req.body.pickedCard)
      .then((result) => {
        if (result.error) {
          responseDispatcher.dispatchError(res, result);
        } else {
          responseDispatcher.dispatch(res, result);
        }
      }).catch((err) => {
        console.log(err);
        responseDispatcher.dispatch(res, err);
      });
  }


  /**
   * This function is used to collect gamble winnings
   * it utilizes SOA helper to pick existing wins
   * and do all logical validations
   */
  pickGamble(req, res) {
    parisTouristHelper.pickGamble(req.body.token, req.body.gameId, req.body.clientID)
      .then((result) => {
        if (result.error) {
          responseDispatcher.dispatchError(res, result);
        } else {
          responseDispatcher.dispatch(res, result);
        }
      }).catch((err) => {
        console.log(err);
        responseDispatcher.dispatch(res, err);
      });
  }
}
module.exports = new ParisTourist();
