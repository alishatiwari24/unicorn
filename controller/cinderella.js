const gameFunction = require('../helpers/gamefunction');
const responseDispatcher = require('../util/responsedispatcher');
const constants = require('../response/constants');
const compute = require('../util/compute');
const cinderella = require('../util/games/cinderella');
const whowapi = require('../api/whowapi');
const moment = require('moment');

/**
 * Special features for Cinderella are implemented in this class
 * it handles spin and gamble requests from client
 */
class Cinderella {

  /**
  * This function handles game click event and receives data from node router
  * Game click logic in cinderella helper is invoked in this method
  * and game session invoked would store game click data
  * request and response objects are passed to it from node router
  * @param game_id
  * @param token
  * @param clientID
  * are utilized for game click
  */
  gameClick(req, res) {
    new Cinderella().handleGameClick(req.body.gameId, req.body.token)
    .then((data) => {
      if (data.error) {
        responseDispatcher.dispatchError(res, data);
      } else {
        responseDispatcher.dispatch(res, data);
      }
      return data;
    })
    .then((data) => {
      if (data.userDetails) {
        const gameData = gameFunction.getGame(req.body.gameId);
        cinderella.handleGameClick(req.body.gameId, req.body.token, req.body.clientID, data, gameData);
      }
    }).catch((err) => {
      responseDispatcher.dispatchError(res, err);
      console.log(err);
    });
  }


  /**
  * This function handles game click event from controller
  * WHOW API is called to get user data from token
  * relevant user data along with game sessions are created using compute helper
  */
  handleGameClick(gameId, token) {
    return new Promise((resolve, reject) => {
      whowapi.get(token)
      .then((response) => {
        const obj = gameFunction.getGame(gameId);
        if (!obj) {
          resolve({ error: constants.RES_MESSAGES.GAME_NOT_FOUND, erroCode: constants.RES_ERROR_CODES.GAME_NOT_FOUND });
        } else if (response.status === 200) {
          obj.userDetails = response.payload;
          resolve(cinderella.generateGameClickResponse(obj, token, gameId));
        } else {
          resolve({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
        }
      }).catch((err) => {
        console.log(err);
        reject({ error: constants.RES_MESSAGES.UNRECOGNIZED_USER, errorCode: constants.RES_ERROR_CODES.UNRECOGNIZED_USER });
      });
    });
  }


  /**
   * Spin handles game spin event on cinderella
   * A wild reel is choosen randomly on top of view zone
   */
  spin(req, res) {
    try {
      new Cinderella().handleSpin(req.body.gameId, req.body.token, req.body.clientID)
      .then((result) => {
        const resultCopy = JSON.parse(JSON.stringify(result));
        delete result.session;
        if (result.gamble) {
          delete result.gamble.id;
          delete result.gamble.betAmount;
          delete result.gamble.winAmount;
          delete result.gamble.timestamp;
        }
        if (result.error) {
          responseDispatcher.dispatchError(res, result);
        } else {
          responseDispatcher.dispatch(res, result);
        }
        return resultCopy;
      })
      .then((result) => {
        if (!result.error) {
          const logResult = JSON.parse(JSON.stringify(result));
          if (!result.isSessionUpdated) {
            cinderella.gameSpin(req.body.gameId, req.body.token, result);
          } else {
            const sessionRequest = result.session.sessionRequest;
            sessionRequest.status = 'ready';
            sessionRequest.spinCompletedTimestamp = moment().utc();
            cinderella.updateSessionRequestDoc(result.session.sessionRequestKey, sessionRequest);
          }
          cinderella.logSpinResult(logResult, req.body.token, req.body.gameId);
        }
      }).catch((err) => {
        console.log(err);
        responseDispatcher.dispatch(res, { error: constants.RES_MESSAGES.SPIN_FAILED, errorCode: constants.RES_ERROR_CODES.SPIN_FAILED });
      });
    } catch (e) {
      console.log(e);
    }
  }


  /*
  * This function handles spin request for cinderella
  * generates a random number based on game configuration
  * computes the result with the help of compute helper and generates the response
  * fallbacks in case of user spin being active
  * and checks if spin request is valid.
  */
  handleSpin(gameId, token, clientID) {
    return new Promise((resolve, reject) => {
      const obj = gameFunction.getGame(gameId);
      if (!obj) {
        resolve({ error: constants.RES_MESSAGES.GAME_NOT_FOUND, erroCode: constants.RES_ERROR_CODES.GAME_NOT_FOUND });
      } else {
        gameFunction.getUserSessionAndSessionRequest(token, gameId)
        .then((results) => {
          if (results.sessionRequestKey && results.sessionKey) {
            const sessionRequest = results[results.sessionRequestKey].value;
            if (sessionRequest.status == 'init' || sessionRequest.status == 'ready') {
              const session = results[results.sessionKey].value;

              // allow spin
              if (session.clientID === clientID) {
                // update session request doc
                gameFunction.updateSessionRequestDoc(results.sessionRequestKey, sessionRequest, results[results.sessionRequestKey].cas)
                .then(() => {
                  cinderella.compute(obj.game, gameId, token, session)
                  .then((computeData) => {
                    resolve(cinderella.generateSpinResponse(obj, computeData, token, results.sessionRequestKey, sessionRequest, session));
                  }).catch((err) => {
                    console.log(err);
                  });
                })
                .catch((error) => {
                  console.log(error);
                  resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED });
                });
              } else {
                resolve(compute.invalidClientIdResponse());
              }
            } else {
              resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED });
            }
          } else {
            // reject spin as no session request document is present
            resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK });
          }
        }).catch(() => {
          resolve({ error: constants.RES_MESSAGES.SPIN_NOT_ALLOWED_NO_GAME_CLICK, errorCode: constants.RES_ERROR_CODES.SPIN_NOT_ALLOWED_NO_GAME_CLICK });
        });
      }
    });
  }
}

module.exports = new Cinderella();
