import http from 'http';

interface BodyParams {
    gameId?: string;
    token?: string;
    clientID?: string;
    baseGameId?: string;
}

export default interface Reqesut extends http.ClientRequest {
    body?: BodyParams;
}
