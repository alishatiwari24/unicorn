export interface User {
    id: string;
    name: string;
    gender: string;
    wallet: { chips: number };
    locale: string;
    birthday: string;
    level: number;
    levelProgress: number;
    multiplier: number;
}


export interface BetArray {
    settings: { bets: number[] };
    betAmount?: number;
}


export interface UserDetails {
    user: User;
    game: BetArray;
    selectablePaylines: number[];
    currentBet: number;
    currentLines: number;
    totalBet: number;
}


export interface Game {
    _id: string;
    game: { [key: string]: {} | object };
    userDetails: UserDetails;
}

export interface ErrorMessage {
    error: string;
    errorCode: number;
}

export interface WildMultiplierPosition {
    [key: string]: {
        row: number;
        multiplier: number;
    };
}

export interface WinLines {
    symbolId?: string;
    totalWildMultiplier?: number;
    wildMultiplierPosition?: WildMultiplierPosition;
    winAmount?: number;
    winLineId?: string;
    winType?: string;
    multiplier?: number;
}

export interface Wild {
    multiplier?: number;
    reel?: number;
    row?: number;
}

export interface WildOfKind {
    [key: string]: Wild;
}

export interface Win {
    winCoins: number;
    winLinesCount: number;
    winLines: WinLines[];
}

export interface FreeSpinData {
    freeSpins?: number;
    currentFreeSpin?: number;
    isFreeSpinAwarded?: boolean;
    noofFreeSpinsAwarded?: number;
    winAmount?: number;
}

export interface LevelData {
    level: number;
    levelProgress: number;
}
