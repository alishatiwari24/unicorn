import ndarray from './../nd-impl';
import { Game, User, BetArray, FreeSpinData, WinLines, LevelData } from './../Game';
import moment from 'moment';

export interface ViewZone {
    [key: string]: SymbolsDesc[] | number;
}

export interface Wins {
    winCoins: number;
    winLines: WinLines[];
    winLinesCount: number;
}

export interface Result {
    viewZone?: ViewZone;
    wins?: Wins;
    freeSpinData?: FreeSpinData;
    levelData?: LevelData;
    noofReels?: number;
    wallet?: { chips: number };
    isSessionUpdated?: boolean;
    session?: ResultSession;
    error?: string;
    errorCode?: number;
}

export interface ResultSession {
    sessionRequestKey: string;
    sessionRequest: SessionRequest;

}

export interface ComputeDataResult {
    mainGameResult: WinLines[];
    freeSpins?: number;
}

export interface ComputeData {
    viewZone?: ndarray<number>;
    results?: ComputeDataResult;
    scatter?: number[];
    error?: string;
}


interface MaxMinMulti {
    maxBetMultiplier: number;
    minBetMultiplier: number;
}


interface BigWins {
    bigWin: MaxMinMulti;
    legendaryWin: MaxMinMulti;
    monsterWin: MaxMinMulti;
}


interface FreeSpinWins {
    noofFS: number;
    winMultiplier: number;
}


interface PayArray {
    [key: string]: string | number | string[];
}


export interface NormalizedPayArray {
    _id: string;
    data: number[];
}


interface SpecialFeature {
    reelSwitching: { freeGame: string[]; mainGame: string[]; };
    wildMultiplier?: { freeGame: { [key: string]: number }; mainGame: { [key: string]: number }; };
}


export interface SymbolsDesc {
    RowNumber?: number;
    SymbolId?: string;
    SymbolName?: string;
    SymbolNumber?: number;
    SymbolType?: string;
    SymbolUrl?: string;
    symbolId?: string;
    symbolType?: string;
    symbolName?: string;
}


interface ReelArray {
    [key: string]: { NumberOfRows: number; SymbolDistribution: SymbolsDesc[]; } | number;
}


interface FreeSpin {
    SymbolName: string;
    SymbolType: string;
    symbolId: string;
    freeSpinWins: { [key: string]: FreeSpinWins };
    isFSReelDiff: boolean;
    keys: string[];
    minimumRequiredCount: number;
    reelConfigSet: ReelArray[];
}


export interface CasanovaGameConfig {
    bigWins: { [key: string]: BigWins };
    freeSpin: FreeSpin;
    payLines: { PayArray: PayArray[]; normalizedPayArray: NormalizedPayArray[] };
    payTable: { [key: string]: { [key: string]: { Amount: number; Multiplier: number; }; }; };
    reelConfigSet: ReelArray[];
    selectablePaylines: { [key: string]: string[] };
    specialFeature: SpecialFeature;
    symbols: { [key: string]: SymbolsDesc };
    viewZone: number;
    reelConfig: ReelArray;
}


/**
 * [casanova: here is shown a type definition for casanova game which provide type interface]
 */
export interface CasanovaGame extends Game {
    game: {
        default: {
            _type: string;
            baseGameId: string;
            createdAt: Date | number;
            environment: string;
            gameConfig: CasanovaGameConfig;
            isDeleted: boolean;
            status: string;
            updateAt: number | Date;
            version: number;
            versionCode: string;
            versionName: string;
        };
    };
}

interface EventData {
    currentBet: number;
    currentLines: number;
    userDetails: User;
    freeSpinData: FreeSpinData;
}


export interface Session {
    _type: string;
    event: string;
    clientID: string;
    eventData: EventData;
    game: BetArray;
    _updatedTimestamp: string | moment.Moment;
    gamble: {};
}


export interface SessionRequest {
    _type: string;
    status: string;
}


export interface SessionData {
    cas: string;
    value: Session | SessionRequest;
}


/**
 * type of session object in casanova
 */
export interface SessionStore {
    [key: string]: SessionData | string;
}
