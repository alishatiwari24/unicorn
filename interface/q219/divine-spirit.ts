import * as BaseNdArray from './index';
import * as moment from 'moment';

/**
 * GameDetail contains GameDetail with config and version
 */
export interface GameDetail {
    _id: string;
    game: GameObject;
}


/**
 * GameObject contains game base details with GameConfig
 */
export interface GameObject {
    default: {
        _type: string,
        baseGameId: string,
        createdAt: number,
        environment: string,
        gameConfig: GameConfig,
        isDeleted: boolean,
        status: string,
        updatedAt: number,
        version: number,
        versionCode: string;
        versionName: string;
    };
}


/**
 * GameConfig contains game configuration
 */
export interface GameConfig {
    bigWins: BigWins;
    freeSpin: FreeSpin;
    payLines: {
        PayArray: Paylines[],
        normalizedPayArray: NormalizedPayArray[],
    };
    payTable: PayTableData;
    reelConfig: MainGameReelConfig;
    selectablePaylines: SelectablePaylines;
    symbols: {
        [key: string]: SymbolSchema,
    };
    viewZone: number;
}


/**
 * WinsOnMultiplier contains winsMultiplier
 */
export interface WinsOnMultiplier {
    [key: string]: {
        maxBetMultiplier: number;
        minBetMultiplier: number;
    };
}


/**
 * BigWins contains Big Win Multiplier
 */
export interface BigWins {
    bigWin: WinsOnMultiplier;
    legendaryWin: WinsOnMultiplier;
    monsterWin: WinsOnMultiplier;
}


/**
 * FreeSpin contains FreeSpin details
 */
export interface FreeSpin {
    SymbolName: string;
    SymbolType: string;
    freeSpinWins: FreeSpinWins;
    isFSReelDiff: boolean;
    keys: Array<string | number>;
    minimumRequiredCount: number;
    reelConfig: Reels;
    symbolId: string;
}


/**
 * FreeSpinWins contains wining details
 */
export interface FreeSpinWins {
    [key: string]: {
        noofFS: number,
        winMultiplier: number,
    };
}


/**
 * ReelS Contains Reel configuration data
 */
export interface Reels {
    [key: string]: ReelData;
}


/**
 * SymbolDistribution contains Symbols Data
 */
export interface SymbolDistribution {
    RowNumber: number;
    SymbolId: string;
    SymbolName: string;
    SymbolType: string;
    SymbolUrl: string;
}


/**
 * ReelData contains Symbols in Particular Reel
 */
export interface ReelData {
    NumberOfRows: number;
    SymbolDistribution: SymbolDistribution[];
}


/**
 * Paylines contains paylines and keys in Array
 */
export interface Paylines {
    [key: string]: number | string | string[];
}


/**
 * PayTable Data contains paytable data from gameConfig
 */
export interface PayTableData {
    [key: string]: PayTableMultiplierAmount;
}


/**
 * PayTableMultiplierAmount contains multip and amount to be paid of a kind
 */
export interface PayTableMultiplierAmount {
    [key: string]: {
        Amount: number,
        Multiplier: number,
    };
}


/**
 * PayArrayObject contains Paylines Array position
 */
export interface PayArrayObject {
    _id: string;
    data: number[];
}


/**
 * MainGameReelConfig contains reelConfiguration for mainGame
 */
export interface MainGameReelConfig {
    [key: string]: ReelData & number;
}


/**
 * SelectablePaylines contains all paylines data
 */
export interface SelectablePaylines {
    [key: string]: string[] | string;
}


/**
 * SymbolSchema contains particular symbol details
 */
export interface SymbolSchema {
    SymbolId: string;
    SymbolName: string;
    SymbolType: string;
    SymbolUrl: string;
    isExpandingWild: boolean;
    doesWildPay: boolean;
    multiplier: number;
}


/**
 * NormalizedPayArray pay array contains number of postion of paylines
 */
export interface NormalizedPayArray {
    [key: string]: string | number[];
}


/**
 * ViewZone contains viewZone for  spin response and viewZone number
 */
export interface ViewZone {
    [key: string]: SpinResponseViewZone[] | number;
}


/**
 * Result contains result for spin
 */
export interface Result {
    viewZone?: ViewZone;
    wins?: SpinResponseWins;
    freeSpinData?: FreeSpinDataSchema;
    levelData?: LevelData;
    noofReels?: number;
    isSessionUpdated?: boolean;
    session?: SpinResultSessionObject;
    wildReel?: number;
}


/**
 * SpinResultSessionObject contains session object for spin response
 */
export interface SpinResultSessionObject {
    sessionRequestKey?: string;
    sessionRequest?: SessionRequestValue;
}


/**
 * ComputeData contains comuputaion result of every spin
 */
export interface ComputeData {
    viewZone?: BaseNdArray<number | string>;
    results?: ComputeDataResult;
    scatter?: number[];
    error?: string;
}

/**
 * SpinResponseViewZone contains viewZone after every spin in spin response
 */
export interface SpinResponseViewZone {
    symbolId: string;
    symbolType: string;
    symbolName: string;
}


/**
 * SpinResponseWins contains data for wins in spins
 */
export interface SpinResponseWins {
    winCoins: number;
    winLinesCount: number;
    winLines: SpinResponseWinLines[];
    freeSpinTriggerWin: number;
}


/**
 * SpinResponseWinLines contains data for every win lines
 */
export interface SpinResponseWinLines {
    symbolId: string;
    winType: string;
    winLineId: string;
    winAmount: number;
}


/**
 * ComputedResult contains data in compute function
 */
export interface ComputedResult {
    symbolId: string;
    winType: string;
    winLineId: string;
    multiplier: number;
    winAmount: number;
}


/**
 * SpinResponseResult contains wins and viewZone
 */
export interface SpinResponseResult {
    wins: SpinResponseWins;
    viewZone: ViewZone;
}


/**
 * SessionResult contains object of user session and sessionRequest and other keys for session
 */
export interface SessionResult {
    [key: string]: UserSession & SessionRequest & string;
}


/**
 * contains user session
 */
export interface UserSession {
    cas: string;
    value: SessionObject;
}


/**
 * SessionRequest contains session Request which is used to
 */
export interface SessionRequest {
    cas: string;
    value: SessionRequestValue;
}


/**
 * SessionRequestValue contains value for SessionRequest
 */
export interface SessionRequestValue {
    _type: string;
    status: string;
    spinIniTimestamp: string;
    spinCompletedTimestamp: string;
}


/**
 * SessionObject contains session for every spin of game
 */
export interface SessionObject {
    _type: string;
    event: string;
    clientID: string;
    eventData: EventData;
    game: GameSettingSchema;
    _updatedTimestamp: string | moment.Moment;
    gamble: GambleSchema;
}


/**
 * GameSettingSchema contains game settings such as bet Array
 */
export interface GameSettingSchema {
    settings: {
        bets: number[],
    };
    betAmount: number;
}


/**
 * GambleSchema contains gamble data structure
 */
export interface GambleSchema {
    id: number;
    betAmount: number;
    winAmount: number;
    timestamp: {
        sec: string,
        usec: string,
    };
    history: number[];
    count: number;
}


/**
 * EventData contains session EventData
 */
export interface EventData {
    currentBet: number;
    currentLines: number;
    userDetails: UserDetailsSchema;
    freeSpinData: FreeSpinDataSchema;
    wildReel?: number;
}


/**
 * UserDetailsSchema contains particular user details
 */
export interface UserDetailsSchema {
    id: string;
    name: string;
    gender: string;
    wallet: {
        chips: number,
    };
    locale: string;
    birthday: string;
    level: string;
    levelProgress: string;
    multiplier: number;
}


/**
 * FreeSpinDataSchema contains FreeSpinDataSchema for game and session also
 */
export interface FreeSpinDataSchema {
    freeSpins: number;
    currentFreeSpin: number;
    winAmount: number;
    isFreeSpinAwarded?: boolean;
    noofFreeSpinsAwarded?: number;
}


export interface ComputeDataResult {
    mainGameResult: ComputedResult[];
    freeSpins?: number;
}

/**
 * LevelData contains level Record structure
 */
export interface LevelData {
    level: number;
    levelProgress: number;
}


/**
 * SpinFinalResult contains spin final result structure
 */
export interface SpinFinalResult extends Result, SpinResultSessionObject {
    gamble?: GambleSchema;
    game?: GameSettingSchema;
    wallet?: {
        chips: number,
    };
    error?: string;
    erroCode?: number;
}
