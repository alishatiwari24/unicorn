FROM node:dubnium

WORKDIR /server

COPY . /server
RUN npm install
RUN npm run compile
RUN npm install apidoc -g

RUN apidoc -i ./ -e node_modules/ -o apidoc/

EXPOSE 9000

CMD ["node","index.js"]
