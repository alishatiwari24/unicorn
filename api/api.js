const crypto = require('crypto');
const http = require('http');

class Api {
  /**
  * intialize host, port, key and secret
  */
  constructor(host, port, key, secret) {
    this.host = host;
    this.port = port;
    this.key = key;
    this.secret = secret;
  }

  /**
  * builds hash from headers and request payload, and call whowapi
  */
  call(method, uri, payload = '', callback) {
    // date
    const date = new Date().toISOString().replace(/\.\d{3}Z$/, 'Z').replace(/[:\-]|\.\d{3}/g, '');

    // headers
    const signingHeaders = [];
    signingHeaders.push('Content-Type:application/json; charset: utf-8');
    signingHeaders.push(`Host:${this.host}${this.port != 80 ? `:${this.port}` : ''}`);
    signingHeaders.push(`X-Whow-Date:${date}`);

    const headers = {};
    headers['Content-Type'] = 'application/json; charset: utf-8';
    headers.Host = this.host + (this.port != 80 ? `:${this.port}` : '');
    headers['X-Whow-Date'] = date;

    // sort headers
    signingHeaders.sort((a, b) => a[0].toLowerCase() < b[0].toLowerCase() ? -1 : 1);

    for (const i in signingHeaders) {
      signingHeaders[i] = signingHeaders[i].toLowerCase();
    }

    // signed headers
    const signedHeaders = 'content-type;host;x-whow-date';

    // canonical headers
    const canonicalHeaders = `${signingHeaders.join('\n')}\n`;

    // payload
    if (typeof payload === 'array' || typeof payload === 'object' || typeof payload === 'Map') {
      payload = JSON.stringify(payload);
    }

    // hashedPayload
    let hash = crypto.createHash('sha256');
    hash.update(payload);

    const hashedPayload = hash.digest('hex');

    // canonical form
    const canonicalForm = `${method}\n${uri}\n${canonicalHeaders}\n${signedHeaders}\n${hashedPayload}`;

    hash = crypto.createHash('sha256');
    hash.update(canonicalForm);

    const canonicalFormHash = hash.digest('hex');

    // string to sign
    const stringToSign = `SHA256\n${date}\n${canonicalFormHash}`;

    // pad helper..
    const pad = (n) => {
      if (n < 10) {
        return `0${n}`;
      }
      return n;
    };

    // signingKeyDate
    let hmac = crypto.createHmac('sha256', `whow${this.secret}`);
    hmac.update(`${new Date().getUTCFullYear()}${pad(new Date().getUTCMonth() + 1)}${pad(new Date().getUTCDate())}`);

    const signingKeyDate = hmac.digest('hex');

    // derivedSigningKey
    hmac = crypto.createHmac('sha256', signingKeyDate);
    hmac.update('whow_request');

    const derivedSigningKey = hmac.digest('hex');

    // signature
    hmac = crypto.createHmac('sha256', derivedSigningKey);
    hmac.update(stringToSign);

    const signature = hmac.digest('hex');

    // add sign information to headers
    const authorization = `SHA256 Credential=${this.key}, SignedHeaders=${signedHeaders}, Signature=${signature}`;

    headers.Authorization = authorization;
    headers['Content-Length'] = payload.length;
    headers.Accept = 'application/json';

    // options
    const options = {
      hostname: this.host,
      port: this.port,
      path: uri,
      method,
      headers
    };

    // request..
    let response = '';

    const request = http.request(options, (res) => {
      res.setEncoding('utf8');

      res.on('data', (chunk) => {
        response += chunk;
      });

      res.on('end', () => {
        if (typeof callback === 'function') {
          callback(response, false);
        }
      });
    });

    request.on('error', (e) => {
      if (typeof callback === 'function') {
        callback(false, e);
      }
    });

    request.write(payload);
    request.end();
  }
}

module.exports = Api;
