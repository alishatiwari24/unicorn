const whowApiConfig = require('../config/whowapi');
const Api = require('./api');
const Promise = require('bluebird');
const client = require('./prometheus');
const histogram = new client.Histogram({
  name: 'whow_response_times',
  help: 'whow_response_times',
  labelNames: ['apiEndpoint', 'statusCode'],
  buckets: [0.5, 1, 1.5, 2, 2.5, 3]
});
/**
* Whow builds api calls to be made to WHOW api server
* it registers various routes, used to fetch user details and notify of game events
* game events include play, bet, close etc.
*/
class Whow {

  /**
  * Initializes api with secret and access keys
  * this keys are used to sign, request to be made
  */
  constructor() {
    this.api = new Api('staging-3-api.jackpot.de', 80, whowApiConfig.API_ACCESS_KEY, whowApiConfig.API_SECRET_KEY);
  }


  /**
  * This api call does not inclide any payload
  * it is used to fetch user details from whow api using a user token
  */
  get(token) {
    return new Promise((resolve, reject) => {
      if (!token || token === '') {
        resolve({});
      } else {
        const end = histogram.startTimer({ apiEndpoint: '/session_get' });
        this.api.call('POST', `/game_sessions/action/get/${token}`, [], (response, bool) => {
          const parsedResponse = JSON.parse(response);
          end({ statusCode: parsedResponse.status });
          resolve(parsedResponse);
        });
      }
    });
  }


  /**
  * This api call does not inclide any payload
  * it is used to fetch user wallet chips from whow api using a user token
  */
  wallet(token) {
    return new Promise((resolve, reject) => {
      if (!token || token === '') {
        resolve({});
      } else {
        const end = histogram.startTimer({ apiEndpoint: '/session_wallet' });
        this.api.call('POST', `/game_sessions/action/wallet/${token}`, [], (response, bool) => {
          const parsedResponse = JSON.parse(response);
          end({ statusCode: parsedResponse.status });
          resolve(parsedResponse);
        });
      }
    });
  }

	/*
	 * payload arguments
	 * Name: betAmount, Type: Float, Example Value: 1250, Description: amount to bet in this round
	 * Name: virtualAmount, Type: Float, Example Value: 0, Description: amount to bet in case of free spins; important: this parameter is optional and only used in special cases for freespins!
	 * Name: winAmount, Type: Float	, Example Value: 0, Description: amount the user will win in this round
	 */
  play(token, payload) {
    return new Promise((resolve, reject) => {
      if (!token || token === '' || !payload || payload === '') {
        resolve({});
      } else {
        const end = histogram.startTimer({ apiEndpoint: '/play' });
        this.api.call('POST', `/game_sessions/action/play/${token}`, payload, (response, bool) => {
          end({ statusCode: JSON.parse(response).status });
          resolve(response);
        });
      }
    });
  }

	/*
	* payload arguments
	* Name: betAmount, Type: Float, Example Value: 1250, Description: amount to bet in this round
	* Name: virtualAmount, Type: Float, Example Value: 0, Description: amount to bet in case of free spins; important: this parameter is optional and only used in special cases for freespins!
	* Name: roundId, Type: String, Example Value: "ca5217a205e148ba…", Description: round id to bet on; important: this parameter is optional! if it is not provided the backend will start a new game round
	*/
  bet(token, payload) {
    return new Promise((resolve, reject) => {
      if (!token || token === '' || !payload || payload === '') {
        resolve({});
      } else {
        const end = histogram.startTimer({ apiEndpoint: '/bet' });
        this.api.call('POST', `/game_sessions/action/bet/${token}`, payload, (response, bool) => {
          end({ statusCode: JSON.parse(response).status });
          resolve(response);
        });
      }
    });
  }

	/*
	 * payload arguments
	 * Name: winAmount, Type: Float, Example Value: 2500, Description: closing win amount for this round
	 * Name: gameData, Type: Object, Example Value: {"spinResult":[…]}, Description: gameData the game wants to track to the casino
	 * Name: roundId, Type: String, Example Value: "ca5217a205e148ba…", Description: round id to close
	 */
  close(token, payload) {
    return new Promise((resolve, reject) => {
      if (!token || token === '' || !payload || payload === '') {
        resolve({});
      } else {
        const end = histogram.startTimer({ apiEndpoint: '/close' });
        this.api.call('POST', `/game_sessions/action/close/${token}`, payload, (response, bool) => {
          end({ statusCode: JSON.parse(response).status });
          resolve(response);
        });
      }
    });
  }

	/*
	 * payload arguments
	 * Name: gameData, Type: Object, Example Value: {"spinResult":[…]}, Description: gameData the game wants to track to the casino
	 * Name: roundId, Type: String, Example Value: "ca5217a205e148ba…", Description: round id to close
	 */
  cancel(token, payload) {
    return new Promise((resolve, reject) => {
      if (!token || token === '' || !payload || payload === '') {
        resolve({});
      } else {
        const end = histogram.startTimer({ apiEndpoint: '/cancel' });
        this.api.call('POST', `/game_sessions/action/cancel/${token}`, payload, (response, bool) => {
          end({ statusCode: JSON.parse(response).status });
          resolve(response);
        });
      }
    });
  }

	/*
	 * payload arguments
	 * Name: ids, Type: Array, Example Value: ["ca5217a205e148ba85f73bc37ad7e0a0"], Description: free spin ids to validate
	 */
  validateFreesSpins(token, payload) {
    return new Promise((resolve, reject) => {
      if (!token || token === '' || !payload || payload === '') {
        resolve({});
      } else {
        const end = histogram.startTimer({ apiEndpoint: '/validateFreespins' });
        this.api.call('POST', `/game_sessions/action/validateFreespins/${token}`, payload, (response, bool) => {
          const parsedResponse = JSON.parse(response);
          end({ statusCode: parsedResponse.status });
          resolve(parsedResponse);
        });
      }
    });
  }

  /**
  * This api call is used to notify WHOW server of a big win
  * any further events can be added if needed
  */
  triggerEvents(token, payload) {
    return new Promise((resolve, reject) => {
      if (!token || token === '' || !payload || payload === '') {
        resolve({});
      } else {
        const end = histogram.startTimer({ apiEndpoint: '/event_trigger' });
        this.api.call('POST', `/game_events/action/trigger/${token}`, payload, (response, bool) => {
          const parsedResponse = JSON.parse(response);
          end({ statusCode: parsedResponse.status });
          resolve(parsedResponse);
        });
      }
    });
  }

  /**
  * This api call is used to notify WHOW server of game session
  * Validating free spins is one of the session call
  */
  sessionCalls(token, payload) {
    return new Promise((resolve, reject) => {
      if (!token || token === '' || !payload || payload === '') {
        resolve({});
      } else {
        const end = histogram.startTimer({ apiEndpoint: '/validateFreespins' });
        this.api.call('POST', `/game_sessions/action/validateFreespins/${token}`, payload, (response, bool) => {
          const parsedResponse = JSON.parse(response);
          end({ statusCode: parsedResponse.status });
          resolve(parsedResponse);
        });
      }
    });
  }
}

module.exports = new Whow();
